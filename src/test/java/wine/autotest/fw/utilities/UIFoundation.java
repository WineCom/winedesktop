package wine.autotest.fw.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.MediaEntityBuilder;


public class UIFoundation extends GlobalVariables {
	static int z = 0;
	static ArrayList<String> pageNameDetails = new ArrayList<String>();
	static Object repeatedPagename = null;
	static ArrayList obj1 = new ArrayList();
	public static String screenshotFolderName=null;
	public static int rannum = UIFoundation.randomNumber(0 - 9);
	/***************************************************************************
	 * Method Name : property() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to load .properties
	 * file
	 ****************************************************************************
	 */
	public static Properties property(String FileName) {
		FileInputStream fin = null;
		Properties prop = null;
		try {
			fin = new FileInputStream(FileName);
			prop = new Properties();
			prop.load(fin);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	/***************************************************************************
	 * Method Name : property() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to load .properties
	 * file
	 ****************************************************************************
	 */
	public static Properties updatePropertiesFile(String FileName) {
		FileInputStream fin = null;
		Properties prop = null;
		try {
			fin = new FileInputStream(FileName);
			prop = new Properties();
			prop.load(fin);
			
			fin.close();

			FileOutputStream out = new FileOutputStream(FileName);
			prop.setProperty("orderNumSplit", "america");
			prop.store(out, null);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}

	/***************************************************************************
	 * Method Name : captureScreenShot() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to capture the
	 * screenshot of failed method
	 ****************************************************************************
	 */

	public static void captureScreenShot(String FileName, String strObjectName) {
		File srcFile;
		try {
			srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(FileName));
			UIFoundation.waitFor(4L);
			ReportUtil.addTestStepsDetails(strObjectName.toString(), "Fail", FileName);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : captureScreenShot() Created By : Ramesh S Reviewed
	 * By :  Purpose : The purpose of this method is to capture the
	 * screenshot of failed method
	 ****************************************************************************
	 */

	public static String ExtendcaptureScreenShot(String FileName) {
		File srcFile;
		try {
			srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(FileName));
			UIFoundation.waitFor(4L);
//			ReportUtil.addTestStepsDetails(strObjectName.toString(), "Fail", FileName);
//			logger.fail(strObjectName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return FileName;
	}
	/***************************************************************************
	 * Method Name : captureScreenShot() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to capture the
	 * screenshot of failed method
	 ****************************************************************************
	 */

	public static void screenshotFolderName(String folderName) {
		
		try {
			screenshotFolderName=folderName;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***************************************************************************
	 * Method Name : isObjectExists() Created By : Vishwanath Chavan Reviewed By
	 * : Ramesh,KB Purpose : The purpose of this method is to verify existence
	 * of object
	 ****************************************************************************
	 */

	public static boolean isObjectExists(WebDriver driver, By by) {
		try {
			driver.findElement(by);
			return true;

		} catch (Exception e) {
			return false;
		}

	}

	/***************************************************************************
	 * Method Name : isAlertExists() Created By : Vishwanath Chavan Reviewed By
	 * : Ramesh,KB Purpose : The purpose of this method is to handle alert popup
	 ****************************************************************************
	 */

	public static boolean isAlertExists(WebDriver driver) {
		try {
			driver.switchTo().alert();
			return true;

		} catch (Exception e) {
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : waitFor() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to put wait in script
	 ****************************************************************************
	 */

	public static void waitFor(long seconds) {
		long miliseconds;
		try {
			miliseconds = seconds * 1000;
			Thread.sleep(miliseconds);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***************************************************************************
	 * Method Name : refresh() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static void refresh() {
		WebDriver driver = null;
		for (int i = 0; i < 5; i++) {
			driver.navigate().refresh();
			UIFoundation.waitFor(2L);
		}

	}
	
	/***************************************************************************
	 * Method Name : getText() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static int listSize(By strObjectName) {
		int elementSize = 0;
		try {
		List<WebElement>	oEle = driver.findElements(strObjectName);
			if (oEle.size()!=0) {
				elementSize = oEle.size();
				UIFoundation.waitFor(1L);

			} else {

				return 0;
			}
			return elementSize;
		} catch (Exception e) {
			return 0;
		}
	}

	/***************************************************************************
	 * Method Name : getDateTime() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to get the current date
	 ****************************************************************************
	 */

	public static String getDateTime(String sDateFormat) {
		String sDateTime = null;
		try {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat);
			sDateTime = sdf.format(cal.getTime());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return sDateTime;
	}

	/***************************************************************************
	 * Method Name : randomNumber() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to generate the random
	 * number
	 ****************************************************************************
	 */

	public static int randomNumber(int randomNumber) {
		int randomNum = 0;

		try {
			Random rand = new Random();
			randomNum = rand.nextInt(100000000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return randomNum;
	}
	
	/***************************************************************************
	 * Method Name : randomNumber() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to generate the random
	 * number
	 ****************************************************************************
	 */

	public static long randomNumberTenDigit(long randomNumber) {
		long randomNum = 0;

		try {
			Random rand = new Random();
			randomNum = rand.nextInt(1000000000);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return randomNum;
	}

	/***************************************************************************
	 * Method Name : getEmail() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to capture newly
	 * created email-id
	 ****************************************************************************
	 */

	public static String getEmail(String userDetails) {
		String userDet = null;
		BufferedWriter bw = null;
		userDet = userDetails;
		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\userEmail.txt";
		try {
			FileWriter fileWritter = new FileWriter(filePath, true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			bufferWritter.write(userDet);
			bufferWritter.newLine();
			bufferWritter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userDet;
	}

	/***************************************************************************
	 * Method Name : waitFor() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to put dynamic wait in
	 * script
	 ****************************************************************************
	 */

	public static boolean waitFor(By by, String strWaitFor, String strValue, int timeOut) {
		WebDriverWait wait = null;
		try {
			wait = new WebDriverWait(driver, timeOut);
			if (strWaitFor.equalsIgnoreCase("title")) {
				wait.until(ExpectedConditions.titleContains(strValue));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("text")) {
				wait.until(ExpectedConditions.textToBePresentInElementLocated(by, strValue));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("element")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(by));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("valueattr")) {
				wait.until(ExpectedConditions.textToBePresentInElementValue(by, strValue));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("Invisible")) {
				wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("Clickable")) {
				wait.until(ExpectedConditions.elementToBeClickable(by));
				return true;
			}

		} catch (Exception e) {
			System.out.println("INFO :" + e.getMessage());
			return false;
		}
		return true;
	}

	/***************************************************************************
	 * Method Name : setObject() Created By : Ramesh S Reviewed By :
	 * Ramesh,KB Purpose :
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static boolean setObject(By strObjectName, String strData) throws IOException {
		String screenshotName = rannum+ "_Screenshot.jpeg";
		WebElement ele = null;
		String objDetail=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				value=value.trim();
				ele.sendKeys(value);
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "RecipientExistAddress");
				logger.pass(objDetail);
				return true;
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
			}
			

		} catch (Exception e) {
			objDetail="Exception: Not able to Enter text in text field: "+strObjectName;
			logger.fail(objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : setObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean JsEnterText(By strObjectName, String strData) {
		String screenshotName = rannum+"_Screenshot.jpeg";
		WebElement ele = null;
		String objDetail=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				JavascriptExecutor js=(JavascriptExecutor)driver;
				
				testScriptXMLTestDataFileName=System.getProperty("user.dir")+"\\src\\main\\resources\\TestData.xml";
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				value=value.trim();
				//ele.sendKeys(value);
				js.executeScript("arguments[1].value = arguments[0]; ",value, ele);   
				UIFoundation.waitFor(1L);
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
			}
			return true;

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name : eventFiringWebDriver() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean eventFiringWebDriver(By strObjectName, String strData) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;
		String objDetail=null;
		EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
		try {
			WebElement element3;
			//element3 = eventDriver.findElement(By.xpath("(//form[@class='paymentForm_form']//fieldset/div/fieldset/label/input)[1]"));
			ele = eventDriver.findElement(strObjectName);
			if (ele != null) {
				JavascriptExecutor js=(JavascriptExecutor)driver;
				
				testScriptXMLTestDataFileName=System.getProperty("user.dir")+"\\src\\main\\resources\\TestData.xml";
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				value=value.trim();
				ele.sendKeys(value);
				UIFoundation.waitFor(1L);
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
			}
			return true;

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return false;
		}
	}
	/***************************************************************************
	 * Method Name : clickObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static boolean clickObject(By strObjectName) throws IOException {
		String screenshotName =rannum+ "_Screenshot.jpeg";
		WebElement ele = null;
		String objDetail=null;
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
			
				ele.click();
				UIFoundation.waitFor(2L);
				objDetail="Clicked on element: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return true;
			} else {
				objDetail="Not able to click on element: "+strObjectName;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
				}
			}
			 catch (Exception e) {
				 objDetail="Catch Exception, Not able to click on element: "+strObjectName;
				 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		
				return false;
		}
	}

	/***************************************************************************
	 * Method Name : setObjectCreateAccount() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @return
	 ****************************************************************************
	 */
	public static boolean setObjectCreateAccount(By strObjectName, String strData) {
		String screenshotName = rannum+ "_Screenshot.jpeg";
		int randomnum = 0;
		String objDetail=null;
		WebElement ele = null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				randomnum = UIFoundation.randomNumber(0 - 9);
				// String value =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				ele.sendKeys(randomnum + "" + value);
				UIFoundation.waitFor(2L);
				String email = randomnum + "" + value;
				UIFoundation.getEmail(email);
				userEmail = email;
				System.out.println("New user email-id: " + email);
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return true;
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				logger.fail(objDetail);
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			//	driver.quit();
				return false;
			}

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return false;

		}
	}
	
	/***************************************************************************
	 * Method Name : setObjectCreateAccount() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @return
	 ****************************************************************************
	 */
	public static String setObjectEditShippingAddress(By strObjectName, String strData) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		int randomnum = 0;
		String objDetail=null;
		WebElement ele = null;
		String editedStrretAddress=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				randomnum = UIFoundation.randomNumber(0 - 9);
				// String value =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				UIFoundation.waitFor(1L);
				ele.sendKeys(randomnum + "" + value);
				UIFoundation.waitFor(2L);
				editedStrretAddress = randomnum + "" + value;
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return editedStrretAddress;
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				logger.fail(objDetail);
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			//	driver.quit();
				return editedStrretAddress;
			}

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return editedStrretAddress;

		}
	}
	
	public static String setObjectEditNameOnCard(By strObjectName, String strData) {
		String screenshotName = rannum+ "_Screenshot.jpeg";
		int randomnum = 0;
		String objDetail=null;
		WebElement ele = null;
		String editNameOnCard=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				randomnum = UIFoundation.randomNumber(0 - 9);
				// String value =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				UIFoundation.waitFor(1L);
				ele.sendKeys(randomnum + "" + value);
				UIFoundation.waitFor(2L);
				editNameOnCard = randomnum + "" + value;
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return editNameOnCard;
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			//	driver.quit();
				return editNameOnCard;
			}

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return editNameOnCard;

		}
	}

	/***************************************************************************
	 * Method Name : scrollDown() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static boolean scrollDown(WebDriver driver) {

		try {
			String getUrl = driver.getCurrentUrl();
			Actions action = new Actions(driver);
			for (int j = 0; j < 4; j++) {
				action.sendKeys(Keys.END).build().perform();
				UIFoundation.waitFor(3L);
			}
			String getUrl1 = driver.getCurrentUrl();
			if (!getUrl.equalsIgnoreCase(getUrl1)) {
				System.out.println("pagination number is changed");
				System.out.println(getUrl1);
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : scrollUp() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static boolean scrollUp(WebDriver driver) {

		try {
			Actions action = new Actions(driver);
			action.sendKeys(Keys.HOME).build().perform();
			UIFoundation.waitFor(3L);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : VerifyText() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean VerifyText(By strObjectName, String strExpected, String strObjType) {
		WebElement oEle = null;
		Select oSelect = null;
		String strText = null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {

				if (strObjType.equalsIgnoreCase("label")) {
					strText = oEle.getText();
				} else if (strObjType.equalsIgnoreCase("edit")) {
					strText = oEle.getAttribute("value");
				} else if (strObjType.equalsIgnoreCase("button")) {
					strText = oEle.getAttribute("value");
				} else {
					oSelect = new Select(oEle);
					strText = oSelect.getFirstSelectedOption().getText();
				}
				if (strText.equalsIgnoreCase(strExpected)) {

					return true;
				} else {
					return false;
				}
			} else {

				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : SelectObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean SelectObject(By strObjectName, String strData) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement oEle = null;
		Select oSelect = null;
		String objDetail=null;
		String selectData=null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				selectData = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				oSelect = new Select(oEle);
				oSelect.selectByVisibleText(selectData);
				objDetail="Selected value from dropdown: "+selectData;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return true;

			} else {
				objDetail="Not able to select value from dropdown : "+strObjectName;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
			}
			
		} catch (Exception e) {
			objDetail="Not able to select value from dropdown is: "+strObjectName;
//			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : getFirstSelectedValue() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String getFirstSelectedValue(By strObjectName) {
		String screenshotName = rannum+ "_Screenshot.jpeg";
		WebElement oEle = null;
		Select oSelect = null;
		String selectedValue = null;
		String objDetail=null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle!=null) {
				oSelect = new Select(oEle);
				selectedValue = oSelect.getFirstSelectedOption().getText();
				objDetail="Selected first value in dropdown is: "+selectedValue;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);

			} else {
				objDetail="Not able to select first value from dropdown is: "+strObjectName;
				logger.fail(objDetail);
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			return selectedValue;
		} catch (Exception e) {
			objDetail="Not able to select first value from dropdown is: "+strObjectName;
			logger.fail(objDetail);
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : getText() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String getText(By strObjectName) {
		WebElement oEle = null;
		String txt = null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				txt = oEle.getText();
				UIFoundation.waitFor(1L);

			} else {
				String objDetail = "Not able to get text";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			}
			return txt;
		} catch (Exception e) {
			
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : getText() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static WebElement webelemnt(By strObjectName) {
		WebElement oEle = null;
		String txt = null;
		try {
			oEle = driver.findElement(strObjectName);
			return  oEle;
		} catch (Exception e) {
			return  oEle;
		}
	}

	/***************************************************************************
	 * Method Name : getAttribute() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String getAttribute(By strObjectName) {
		WebElement oEle = null;
		String txt = null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle!=null) {
				txt = oEle.getAttribute("value");

			} else {

				return "Fail";
			}
			return txt;
		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : getOrderNumber() Created By : Vishwanath Chavan Reviewed By
	 * : Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static String getOrderNumber(String orderNum) {
		String orderNumber = null;
		BufferedWriter bw = null;
		orderNumber = orderNum;
		try {
			FileWriter fileWritter = new FileWriter(OrderFilePath, true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			String DateandTime = UIFoundation.getDateTime("MM/dd/yyyy HH:mm:ss");
			bufferWritter.write(DateandTime+" : "+orderNumber);
			bufferWritter.newLine();
			bufferWritter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return orderNumber;
	}

	/***************************************************************************
	 * Method Name : clickObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean clckObject(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
			WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				ele.click();
				UIFoundation.waitFor(2L);
				String objDetail="Clicked on element: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return true;
			} else {
				String objDetail="Not able Clicked on element: "+strObjectName;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
				
			}
		} catch (Exception e) {
			String objDetail="Not able Clicked on element: "+strObjectName;
//			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : ClearBrowserCache() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static void clearBrowserCache() {
		try {
			driver.manage().deleteAllCookies(); // delete all cookies
			Thread.sleep(5000); // wait 5 seconds to clear cookies.
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/***************************************************************************
	 * Method Name : ClearBrowserCache() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static void launchReport() {
		try {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\main\\resources\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		//	driver.navigate().to(ReportFileName);
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/***************************************************************************
	 * Method Name : isCheckboxSelected() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static void isCheckboxSelected(By strObjectName) {
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (!ele.isSelected()) {
				ele.click();
				UIFoundation.waitFor(4L);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/***************************************************************************
	 * Method Name : setObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean setLocatorObject(By strObjectName, int strData) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;
		String orderNum = String.valueOf(strData);
		String objDetail=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				// String value =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);
				ele.sendKeys(orderNum);
				UIFoundation.waitFor(1L);
				objDetail="Enterd text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				// ReportUtil.addTestStepsDetails(strObjectName,
				// "Fail",screenshotpath);
				return false;
			}
			return true;

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : setObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean newUsersetObject(By strObjectName, String strData) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;
		String objDetail=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				String value = strData;
				ele.sendKeys(value);
				UIFoundation.waitFor(1L);
				objDetail="Enterd text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
				return false;
			}
			return true;

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return false;
		}
	}

	/***************************************************************************
	 * Method Name : setObjectCreateAccount() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @return
	 ****************************************************************************
	 */
	public static String updateEmailInAccountInfo(By strObjectName, String strData) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		int randomnum = 0;
		String objDetail=null;
		WebElement ele = null;
		String email=null;
		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				randomnum = UIFoundation.randomNumber(0 - 9);
				// String value =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);
				String value = XMLData.getTestData(testScriptXMLTestDataFileName, strData, 1);
				ele.sendKeys(randomnum + "" + value);
				UIFoundation.waitFor(2L);
				email = randomnum + "" + value;
				UIFoundation.getEmail(email);
				userEmail = email;
				XMLData.updateTestData(testScriptXMLTestDataFileName, "updateEmailInAccountInfo", 1, userEmail);
				System.out.println("New user email-id: " + email);
				objDetail="Entered text in text field: "+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return email;
			} else {
				objDetail="Not able to Enter text in text field: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			//	driver.quit();
				return email;
			}

		} catch (Exception e) {
			objDetail="Not able to Enter text in text field: "+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return email;

		}
	}
	/***************************************************************************
	 * Method Name : setObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String saveConsoleOutput(WebDriver driver, String pageNameTag) {
		String pageName = pageNameTag;
		BufferedReader br = null;
		java.io.FileReader fr = null;
		String sline = null;
		String x = null;
		FileWriter writer = null;
		BufferedWriter bufWriter = null;
		String expctdPageTag = System.getProperty("user.dir") + "\\src\\test\\resources\\TagValidation\\" + pageName
				+ "Tag.txt";
		String actaulPageTag = System.getProperty("user.dir") + "\\src\\test\\resources\\ActualTagName\\Actual"
				+ pageName + ".txt";
		try {
			fr = new java.io.FileReader(expctdPageTag);
			br = new BufferedReader(fr);
			writer = new FileWriter(actaulPageTag);
			bufWriter = new BufferedWriter(writer);
			while ((sline = br.readLine()) != null) {
				if (sline.contains("Events") || sline.contains("eVar") || sline.contains("prop")
						|| sline.contains("Page Name") || sline.contains("Current URL")) {

					x = sline;
					bufWriter.write(x);
					bufWriter.newLine();
				}

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		} finally {
			try {
				bufWriter.flush();
				bufWriter.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/***************************************************************************
	 * Method Name : countPageNameAppearance() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String countPageNameAppearance(WebDriver driver, String pageNameTag) {
		String pageName = pageNameTag;

		BufferedReader br1 = null;
		java.io.FileReader fr1 = null;
		String sline = null;
		String actaulHomePageTag = System.getProperty("user.dir") + "\\src\\test\\resources\\ActualTagName\\Actual"
				+ pageName + ".txt";
		try {

			fr1 = new java.io.FileReader(actaulHomePageTag);
			br1 = new BufferedReader(fr1);
			while ((sline = br1.readLine()) != null) {
				if (sline.contains("Page Name")) {
					int s = sline.lastIndexOf(":");
					sline = sline.substring(s + 1, sline.length());
					pageNameDetails.add(sline);
					// z++;
				}

			}

			Iterator iterator = pageNameDetails.iterator();
			while (iterator.hasNext()) {
				Object o = iterator.next();
				if (!obj1.contains(o)) {
					obj1.add(o);
					// System.out.println(o);

				} else {
					repeatedPagename = o;
					// System.out.println("duplicate element is:"+o);
				}
			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : giftCertificateNumber() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String giftCertificateNumber() {
		String giftCertificateNum="";
		  try
	        {
			  String path=GiftCertificate;

	                File f1=new File(path);
	                FileReader fr=new FileReader(f1);
	                BufferedReader br=new BufferedReader(fr);
	                ArrayList<String> giftCert=new ArrayList<String>();
	                // Writing data
	                String s = br.readLine();
	 
	                while(s!=null)
	                {
	                	
	                	giftCert.add(s);
	                	 s=br.readLine();

	                }
	               // System.out.println(a);
	                giftCertificateNum=giftCert.get(0);
	                		giftCert.remove(0);
	                FileWriter writer1 = new FileWriter(path);
	                BufferedWriter bufferedWriter1 = new BufferedWriter(writer1);
	                FileWriter writer = new FileWriter(path, true);
	                BufferedWriter bufferedWriter = new BufferedWriter(writer);
	             //   bufferedWriter.newLine();
	                for(int i=0;i<giftCert.size();i++){
	                	String writeGiftCert=giftCert.get(i);
	                	
		                bufferedWriter.write(writeGiftCert);
		                bufferedWriter.newLine();
	                }
	               
	           
	                bufferedWriter.close();
	                return giftCertificateNum;
	        }
	        catch (Exception e)
	        {
	                System.out.println("Problem reading file.");
	                return giftCertificateNum;
	        }
		
		
	}


	

	/***************************************************************************
	 * Method Name : countPageNameAppearance() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String compareTwoFileForTagValidation(WebDriver driver, String pageNameTag) {
		String pageName = pageNameTag;
		BufferedReader br1 = null;
		java.io.FileReader fr1 = null;
		BufferedReader br2 = null;
		java.io.FileReader fr2 = null;
		String sline1 = null;
		String sline2 = null;
		String actaulHomePageTag = System.getProperty("user.dir") + "\\src\\test\\resources\\ActualTagName\\Actual"
				+ pageName + ".txt";
		String expectedHomePageTag = System.getProperty("user.dir")
				+ "\\src\\main\\resources\\ExpectedTagName\\Expected" + pageName + ".txt";
		try {

			fr1 = new java.io.FileReader(actaulHomePageTag);
			br1 = new BufferedReader(fr1);
			fr2 = new java.io.FileReader(expectedHomePageTag);
			br2 = new BufferedReader(fr2);
			if (pageNameDetails.size() != obj1.size()) {
				System.err.println("Adobe Analytics server call made more than one time for " + repeatedPagename);
				return "Fail";

			} else if (pageNameDetails.isEmpty()) {
				System.err.println("No Adobe Analytics server call made for ");
				return "Fail";
			} else {
				while ((sline1 = br1.readLine()) != null && (sline2 = br2.readLine()) != null) {

					if (sline1.replace(" ", "").equalsIgnoreCase(sline2.replace(" ", ""))) {
						System.out.println("Tag name matched :" + "Tag name is  " + sline2);
						return "Pass";
					} else {
						System.err.println("Tag name is not matched :" + "Missing tag is " + sline1);
						return "Fail";
					}

				}
			}

		} catch (Exception e) {
			return "Fail";
		}
		return expectedHomePageTag;

	}

	/***************************************************************************
	 * Method Name : getDouble() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static Double getDouble(By strObjectName) {
		WebElement oEle = null;
		Double txt = null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				txt = Double.parseDouble(oEle.getText());

			}
			return txt;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return txt;
	}

	/***************************************************************************
	 * Method Name : getDouble() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static Long getLong(By strObjectName) {
		WebElement oEle = null;
		Long txt = null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				txt = Long.parseLong(oEle.getText());

			}
			return txt;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return txt;
	}

	/***************************************************************************
	 * Method Name : getText() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String getColor(By strObjectName) {
		WebElement oEle = null;
		String color = null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				color = oEle.getCssValue("color");
				System.out.println("color :"+color);
				String[] hexValue = color.replace("rgba(", "").replace(")", "").split(",");

				int hexValue1 = Integer.parseInt(hexValue[0]);
				hexValue[1] = hexValue[1].trim();
				int hexValue2 = Integer.parseInt(hexValue[1]);
				hexValue[2] = hexValue[2].trim();
				int hexValue3 = Integer.parseInt(hexValue[2]);

				color = String.format("#%02x%02x%02x", hexValue1, hexValue2, hexValue3);
				UIFoundation.waitFor(1L);

			} else {

				return "Fail";
			}
			return color;
		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean isDisplayed(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
			WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele.isDisplayed()) {

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}
	
	
	/***************************************************************************
	 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean isStrDisplayed(String strObjectName) {
		String screenshotName = "Scenarios_" + strObjectName + "_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\"+screenshotFolderName+"\\Screenshots\\"
				+ screenshotName;
		WebElement ele = null;

		try {
			ele = driver.findElement(ObjectMap.getLocator(strObjectName));
			if (ele.isDisplayed()) {

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}
	/***************************************************************************
	 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean isElementDisplayed(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;
		String objDetail=null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele.isDisplayed()) {
				UIFoundation.waitFor(2L);
				objDetail=strObjectName+ ":Element is displayed ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return true;
	
			} else {

				objDetail=strObjectName+ ":Element is not displayed ";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;

			}

		} catch (Exception e) {

			objDetail=strObjectName+ ":Element is not displayed";
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return false;
		}
	}
	
	

	/***************************************************************************
	 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean scrollDownOrUpToParticularElement(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement element = null;

		try {
			element = driver.findElement(strObjectName);
			if (element!=null) {

				UIFoundation.waitFor(2L);
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].scrollIntoView();", element);
				UIFoundation.waitFor(2L);

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}
	/***************************************************************************
	 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean javaSriptClick(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement element = null;

		try {
			element = driver.findElement(strObjectName);
			if (element!=null) {

				UIFoundation.waitFor(1L);
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);
				UIFoundation.waitFor(2L);

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}

	/***************************************************************************
	 * Method Name : verifyAbsentElement() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static String verifyAbsentElement(WebDriver driver, String locator) {
		WebElement oEle = null;
		String txt = null;
		try {
			oEle = driver.findElement(By.xpath(locator));
			if (oEle.isDisplayed()) {
				txt = oEle.getText();
				UIFoundation.waitFor(1L);

			} else {

				return "Fail";
			}
			return txt;
		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : getLastOptionFromDropDown() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static int getLastOptionFromDropDown(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement oEle = null;
		Select oSelect = null;
		int productLimit = 0;
		String objDetail=null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				// String selectData =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);

				oSelect = new Select(oEle);
				int selectOptions = oSelect.getOptions().size();
				oSelect.selectByIndex(selectOptions - 1);
				WebElement option = oSelect.getFirstSelectedOption();
				productLimit = Integer.parseInt(option.getText());
				objDetail="selected last value from dropdown :"+productLimit;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return productLimit;

			} else {
				objDetail="Not able to select last value from dropdown :"+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return 0;
			}
		} catch (Exception e) {
			objDetail="Not able to select last value from dropdown :"+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return 0;
		}
	}
	/***************************************************************************
	 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean isEnabled(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele.isEnabled()) {

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name : isCheckBoxSelected() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean isCheckBoxSelected(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele.isSelected()) {

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name : isCheckBoxSelected() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean isSelected( By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele.isSelected()) {

				return true;
			} else {

				return false;

			}

		} catch (Exception e) {

			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name : SelectObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean selectLastValueFromDropdown(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement oEle = null;
		Select oSelect = null;
		String objDetail=null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {

				Select select = new Select(oEle);
				select.selectByIndex(select.getOptions().size()-1);
				objDetail="selected last value from dropdown :"+strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return true;

			} else {
				objDetail="Not able to select last value from dropdown :"+strObjectName;
				logger.fail(objDetail);
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			//	driver.quit();
				return false;
			}
			
		} catch (Exception e) {
			objDetail="Not able to select last value from dropdown :"+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return false;
		}
	}
	/***************************************************************************
	 * Method Name : SelectObject() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean selectValueByValue(By strObjectName, String indexValue) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement oEle = null;
		Select oSelect = null;
		String objDetail=null;
		try {
			oEle = driver.findElement(strObjectName);
			if (oEle.isDisplayed()) {
				// String selectData =
				// XMLData.getElementData(testScriptXMLTestDataFileName,strData,
				// 1);
				
				oSelect = new Select(oEle);
				oSelect.selectByValue(indexValue);
				objDetail="selected index value from the dropdown :"+indexValue;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return true;

			} else {
				objDetail="Not able to select value from the dropdown :"+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			//	driver.quit();
				return false;
			}
			
		} catch (Exception e) {
			objDetail="Not able to select value from the dropdown :"+strObjectName;
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		//	driver.quit();
			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name : clearField() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static void clearField(By strObjectName) {
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				ele.clear();
				UIFoundation.waitFor(1L);
			} 

		} catch (Exception e) {
			
		}
	}
	
/*	*//***************************************************************************
	 * Method Name : verifyLink() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 *//*
	public static void verifyLink(By strObjectName) {
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				ele.clear();
				ApplicationIndependent.waitFor(1L);
			} 

		} catch (Exception e) {
			
		}
	}*/
	
	/***************************************************************************
	 * Method Name : hitEnter() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static void hitEnter(By strObjectName) {
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				ele.sendKeys(Keys.ENTER);;
				UIFoundation.waitFor(1L);
			} 

		} catch (Exception e) {
			
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name : getEmail() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to capture newly
	 * created email-id
	 ****************************************************************************
	 */

	public static String storePasswordVerificationLink(String emailID,String verificationLink) {
		String userDet = null;
		BufferedWriter bw = null;
		String filePath = System.getProperty("user.dir") + "\\src\\test\\resources\\PasswordVerificationLinkDetails.txt";
		try {
			String timeStamp=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:Ss z");
			FileWriter fileWritter = new FileWriter(filePath, true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			bufferWritter.write(emailID+" || "+verificationLink+"|| "+timeStamp);
			bufferWritter.newLine();
			bufferWritter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userDet;
	}

	/***************************************************************************
		 * Method Name : isDisplayed() Created By : Vishwanath Chavan Reviewed By :
		 * Ramesh,KB Purpose :
		 ****************************************************************************/
		 
		public static boolean javaScriptClick(By strObjectName) {
			String screenshotName = rannum+ "_Screenshot.jpeg";
			WebElement element = null;
			String objDetail=null;

			try {
				element = driver.findElement(strObjectName);
				if (element != null) {

					UIFoundation.waitFor(1L);
					JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].click();", element);
					UIFoundation.waitFor(2L);
					objDetail="Clicked on element: "+strObjectName;
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
					return true;
				} else {
					objDetail="Not able to click on element: "+strObjectName;
					logger.fail(objDetail);
					UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					return false;
				}

			} catch (Exception e) {
				objDetail="Not able to click on element: "+strObjectName;
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				return false;
			}
		} 

	/********************************************************...

	/***************************************************************************
	 * Method Name : mouseHover() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean mouseHover(By strObjectName) {
		String screenshotName = rannum + "_Screenshot.jpeg";
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				Actions action = new Actions(driver);
				 
			    action.moveToElement(ele).build().perform();
				UIFoundation.waitFor(2L);
				String objDetail="Mouse Hover operation is performed on element" +strObjectName;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return true;

			} else {
				ReportUtil.addTestStepsDetails("Mouse Hover operation not performed on element :" +strObjectName, "", "");
				logger.fail("Mouse Hover operation not performed on element :" +strObjectName);
				return false;
			}
		} catch (Exception e) {
//			logger.fail("Mouse Hover operation not performed on element", MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			ReportUtil.addTestStepsDetails("Mouse Hover operation not performed on element :" +strObjectName, "", "");
			return false;
		}
	}
	/***************************************************************************
	 * Method Name : escapeBtn() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean escapeBtn(WebDriver driver) {
		String screenshotName = "Scenarios_escapeBtn_Screenshot.jpeg";
		try{
			Actions action = new Actions(driver);
		    action.sendKeys(Keys.ESCAPE).build().perform();
			return true;
		}catch(Exception e){
			ReportUtil.addTestStepsDetails("Cannot perform Escape action of Keyboard :", "", "");
			logger.fail("Cannot perform Escape action of Keyboard");
			return false;
	}}
	
	/***************************************************************************
	 * Method Name : webDriverWaitForElement() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static boolean webDriverWaitForElement(By strObjectName, String strWaitFor, String strValue, int timeOut) {
		WebDriverWait wait = null;
		WebElement ele = null;
		try {
			wait = new WebDriverWait(driver, timeOut);
			//ele = driver.findElement(strObjectName);
			if (strWaitFor.equalsIgnoreCase("title")) {
				wait.until(ExpectedConditions.titleContains(strValue));
				return true;
			}
			
			

			if (strWaitFor.equalsIgnoreCase("text")) {
				wait.until(ExpectedConditions.textToBePresentInElementLocated(strObjectName, strValue));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("element")) {
				wait.until(ExpectedConditions.presenceOfElementLocated(strObjectName));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("valueattr")) {
				wait.until(ExpectedConditions.textToBePresentInElementValue(strObjectName, strValue));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("Invisible")) {
				wait.until(ExpectedConditions.invisibilityOfElementLocated(strObjectName));
				return true;
			}

			if (strWaitFor.equalsIgnoreCase("Clickable")) {
				wait.until(ExpectedConditions.elementToBeClickable(strObjectName));
				return true;
			}
			
		   

		} catch (Exception e) {
			System.out.println("INFO :" + e.getMessage());
			return false;
		}
		return true;
	}
	
	/***************************************************************************
	 * Method Name : hitEnter() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose :
	 ****************************************************************************
	 */
	public static void hitTab(By strObjectName) {
		WebElement ele = null;

		try {
			ele = driver.findElement(strObjectName);
			if (ele != null) {
				ele.sendKeys(Keys.TAB);;
				UIFoundation.waitFor(1L);
			} 

		} catch (Exception e) {
			
		}
	}
	/***************************************************************************
	 * Method Name : getText() Created By : Chandra shekhar:
	 * Ramesh, Purpose :
	 ****************************************************************************
	 */
	public static List<WebElement> webelements(By strObjectName) {
		List<WebElement> oEle = null;
		String txt = null;
		try {
			oEle = driver.findElements(strObjectName);
			/*for(int i=0; i<=oEle.size();i++)
			{
				//txt=oEle.get(i).toString();
				txt=oEle.get(i).getText();
			}*/
			return  oEle;
		} catch (Exception e) {
			return  oEle;
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: productPrice()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static double productPrice(double d)
	{
		String strStatus=null;
		int cartConut=0;
		double prodPrice=0;
		
		try
		{
			prodPrice=d;
			int x = 2; // Or 2, or whatever
			BigDecimal unscaled = new BigDecimal(prodPrice);
			BigDecimal scaled = unscaled.scaleByPowerOfTen(-x);
			prodPrice=scaled.doubleValue();
			return prodPrice;
		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cartConut;
	}
	/***************************************************************************
	 * Method Name : getMethodName() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to capture newly
	 * created email-id
	 ****************************************************************************
	 */

	public static String[] getMethodName() {
		String Method = new Object() {} .getClass().getEnclosingMethod() .getName(); 
        System.out.println("Name of current method: "+ Method); 
        
       String[] m1=  Method.split("_");
       String TestCaseID = m1[0];
       String methodName = m1[1];
       System.out.println("TestCaseID :"+TestCaseID+" MethodName :"+methodName);
		return m1;
	}

	
}
