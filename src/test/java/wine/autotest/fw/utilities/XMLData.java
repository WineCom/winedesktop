package wine.autotest.fw.utilities;

import java.io.File;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import wine.autotest.desktop.test.Desktop;

public class XMLData extends Desktop {
static String testDataState=null;
	/***************************************************************************
	 * Method Name			: nodeCount()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to get number of node 
	 * 							present in XML file
	 ****************************************************************************
	 */
	
	
	public int nodeCount(String FileName,String tagname)
	{
		int nc=0;
		try
		{
			File f1=new File(FileName);
			DocumentBuilderFactory dbfactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder=dbfactory.newDocumentBuilder();
			Document doc=dbuilder.parse(f1);
			//normalize the file
			doc.getDocumentElement().normalize();
			NodeList nList=doc.getElementsByTagName(tagname);
			nc=nList.getLength();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return nc;
	}
	/***************************************************************************
	 * Method Name			: getElementData()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Read Test data 
	 * 							present in XML file
	 ****************************************************************************
	 */
	
	public static  String getElementData(String FileName,String tagname,int nodenum)
	{ //static
		String ElementData = null;
		try
		{
			File f1=new File(FileName);
			DocumentBuilderFactory dbfactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder=dbfactory.newDocumentBuilder();
			Document doc=dbuilder.parse(f1);
			//normalize the file
			doc.getDocumentElement().normalize();
			NodeList nList=doc.getElementsByTagName("Record");
			Node nNode=nList.item(nodenum-1);
		
			if (nNode.getNodeType()==Node.ELEMENT_NODE)
			{
				Element ele=(Element) nNode;
				
				ElementData=ele.getElementsByTagName(tagname).item(0).getTextContent();

			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return ElementData;
	}
	/***************************************************************************
	 * Method Name			: getElementData()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Read Test data 
	 * 							present in XML file
	 ****************************************************************************
	 */
	
	public static  boolean updateTestData(String FileName,String tagname,int nodenum,String newEmail)
	{ //static
		
		try
		{
			File f1=new File(FileName);
			DocumentBuilderFactory dbfactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder=dbfactory.newDocumentBuilder();
			Document doc=dbuilder.parse(f1);
			//normalize the file
			doc.getDocumentElement().normalize();
			NodeList nList=doc.getElementsByTagName(testDataState);
		//	NodeList nList=doc.getElementsByTagName("CARecord");
			Node nNode=nList.item(nodenum-1);
		
			if (nNode.getNodeType()==Node.ELEMENT_NODE)
			{
				Element ele=(Element) nNode;
				
				ele.getElementsByTagName(tagname).item(0).setTextContent(newEmail);
				
				 TransformerFactory transformerFactory = TransformerFactory.newInstance();
			        Transformer transformer = transformerFactory.newTransformer();
			        DOMSource source = new DOMSource(doc);
			        StreamResult result = new StreamResult(new File(FileName));
			        transformer.transform(source, result);

				return true;

			}else{
				return false;
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		
	}
	
	/***************************************************************************
	 * Method Name			: getElementData()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Read Test data 
	 * 							present in XML file
	 ****************************************************************************
	 */
	
	public static  String getTestData(String FileName,String tagname,int nodenum)
	{ //static
		String ElementData = null;
		try
		{
			File f1=new File(FileName);
			DocumentBuilderFactory dbfactory=DocumentBuilderFactory.newInstance();
			DocumentBuilder dbuilder=dbfactory.newDocumentBuilder();
			Document doc=dbuilder.parse(f1);
			//normalize the file
			doc.getDocumentElement().normalize();
			NodeList nList=doc.getElementsByTagName(testDataState);
		//	NodeList nList=doc.getElementsByTagName("CARecord");
			Node nNode=nList.item(nodenum-1);
		
			if (nNode.getNodeType()==Node.ELEMENT_NODE)
			{
				Element ele=(Element) nNode;
				
				ElementData=ele.getElementsByTagName(tagname).item(0).getTextContent();

			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return ElementData;
	}
	/***************************************************************************
	 * Method Name			: getElementData()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Read Test data 
	 * 							present in XML file
	 ****************************************************************************
	 */
	public static void tagName(String testDataTagName)
	{
		testDataState=testDataTagName;
	}
}
