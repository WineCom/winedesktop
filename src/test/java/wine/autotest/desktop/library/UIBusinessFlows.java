package wine.autotest.desktop.library;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;

import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;

public class UIBusinessFlows extends Desktop {
	static int expectedItemCount[]={1519,253,217,1623,271,63};
	static int actualItemCount[]=new int[expectedItemCount.length];
	
	/***************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean isObjectExistForSignOut(WebDriver driver)
	{
		String strStatus=null;
		
		try
		{
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.obj_ForgotPasowrd));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.obj_NewToWine));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.lnkSignInFacebook));
			if(strStatus.contains("false"))
			{
				return false;
			} 
			else
			{

				return true;
			}
			
		}catch(Exception e)
		{
			return false;
		}
	}
	
	/****************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to handle alert popup
	 ****************************************************************************
	 */
	public static boolean isObjectExistForSignIn()
	{
		String strStatus=null;
		   String screenshotName = "Scenarios_createUser.jpeg";
//			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  				+ screenshotName;
		try
		{
			strStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.OrdersHistory));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.AddressBook));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.PaymentMethods));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.EmailPreferences));
		//	strStatus+=String.valueOf(UIFoundation.isDisplayed(driver, "SignoutLink"));
			if(strStatus.contains("false"))
			{
				strStatus+=false;
				String objDetail="Failed to create user";
				ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return false;
			}
			else
			{
				strStatus+=true;
				String objDetail="User is logged in successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			logger.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			logger.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String Desktoplogin()
	{
		String objStatus=null;
		
		try
		{
			
			logger.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Deskusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			logger.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: Stewardshiplogin()
	 * Created By			: Ramesh S 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String Stewardshiplogin()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: logout()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Logout from the Wine.com 
	 * 						  application
	 ****************************************************************************
	 */

	public static String logout()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method logout started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(5l);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSignout));
//			UIBusinessFlows.isObjectExistForSignOut(driver);
			
			log.info("The execution of the method logout ended here ...");	
			if (objStatus.contains("false"))
			{
				System.out.println("Logout test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Logout test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method logout "+ e);
			return "Fail";
		}
	}
	
	/*
	*//***************************************************************************
	 * Method Name			: recipientEdit()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static boolean recipientEdit()
	{
		String objStatus=null;
		try
		{
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}				
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnshipState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnshipState);
			UIFoundation.waitFor(8L);
			UIFoundation.clickObject(FinalReviewPage.txtZipCode);
			UIFoundation.clearField(FinalReviewPage.txtZipCode);
			UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCode");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
			UIFoundation.waitFor(20L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			UIFoundation.waitFor(10L);
			if(objStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	/*
	*//***************************************************************************
	 * Method Name			: clickObject()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	public static String ClickObjectItems(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		WebElement totalItemsBefore,ItemsAfterSelShowOutIfStock=null;
		   String screenshotName = "Scenarios_ProductsFilteration_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
			String objStatus=null;
		//int numberOfItems=0;
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
			if(ele!=null)
			{
				ele.click();
				UIFoundation.waitFor(2L);
				totalItemsBefore=driver.findElement(object.getLocator("TotalNoOfItems"));
				String totalItem=totalItemsBefore.getText();
				int totalItemBefore=Integer.parseInt(totalItem.replaceAll("[^0-9]", ""));
			//	int totalItemBefore=Integer.parseInt(totalItemsBefore.getText());
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "ShowOutOfStock"));
				UIFoundation.waitFor(2L);
				ItemsAfterSelShowOutIfStock=driver.findElement(object.getLocator("TotalNoOfItems"));
				String totalItemAftr=ItemsAfterSelShowOutIfStock.getText();
			//	double totalItemAfter=Integer.parseInt(ItemsAfterSelShowOutIfStock.getText());
				int totalItemAfter=Integer.parseInt(totalItemAftr.replaceAll("[^0-9]", ""));
				if(totalItemAfter>=totalItemBefore)
				{
					objStatus+=true;
					System.out.println("total no of items are:"+totalItemBefore);
				}
				else
				{
					objStatus+=false;
					String objDetail="products are not filtering properly";
					//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
					UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
					System.out.println("products are not filtering properly");
				}
			}
			UIFoundation.clickObject(driver, "ShowOutOfStock");
			return objStatus;
			
		}
		catch(Exception e)
		{
			String objDetail="products are not filtering properly";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			System.out.println("products are not filtering properly");
			return objStatus;
		}
	}
	*//***************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean isObjectExistForList()
	{
		String strStatus=null;
		
		try
		{
			strStatus+=String.valueOf(UIFoundation.VerifyText(ListPage.ChkShowOutOfStock,"Show out of stock", "label"));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
			
		}catch(Exception e)
		{
			return false;
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: recommendedProductsAddToCartBtn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static boolean recommendedProductsAddToCartBtn()
	{
		String strStatus=null;
		
		try
		{
			strStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.lnkrecommendedProductsFirstAddToCart));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.lnkrecommendedProductsSecondAddToCart));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.lnkrecommendedProductsThirdAddToCart));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name			: recommendedProductsAddToCartBtn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String stewardshipSection()
	{
		String stewardshipText=null;
				
		try
		{
			if(UIFoundation.isDisplayed(CartPage.spngetFreeShippingAll)){
				stewardshipText=UIFoundation.getText(CartPage.spnStewardtextR);
				if(UIFoundation.isDisplayed(CartPage.lnkgetFreeShipping)) {
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkgetFreeShipping));
			}
				else {
					
					objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkstewardAdd2R));	
				}
			}
				//objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkstewardAdd2R));	
				UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(CartPage.spnstewardShipUpsellOptionDefault)){
				stewardshipText=UIFoundation.getText(CartPage.spnStewardtextR);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkstewardShipUpsellOption));
				UIFoundation.waitFor(2L);
			}
			if(UIFoundation.isDisplayed(CartPage.dwnstewardShipUpsellOptionA)){
				stewardshipText=UIFoundation.getText(CartPage.spnStewardtextR);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkstewardShipUpsellOption));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(CartPage.spnwantFreeShiping)){
				stewardshipText=UIFoundation.getText(CartPage.spnStewardtextR);
				if(UIFoundation.isDisplayed(CartPage.lnkgetFreeShipping)) {
					objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkgetFreeShipping));
				}
					else {
						
						objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkstewardAdd2R));	
					}
				}
				
				UIFoundation.waitFor(2L);
			
			
				return stewardshipText;
		
		}catch(Exception e)
		{
			e.printStackTrace();
			return stewardshipText;
		}
		
		
	}
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 *//*
	public static String addprodTocrt(WebDriver driver) {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkSecondProductToCart);
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkThirdProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFourthProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFifthProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	*//***************************************************************************
	 * Method Name			: isElementExistForList()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	
	public static boolean isElementExist(WebDriver driver)
	{
		String strStatus=null;
		
		try
		{
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address1'])[3]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address2'])[3]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='city'])[3]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("//select[@name='stateOrProvince']"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='zipOrPostalCode'])[1]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='phone'])[3]"), "element", "",5));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	
	*//***************************************************************************
	 * Method Name			: isElementNotExist()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	
	public static boolean isElementNotExist(WebDriver driver)
	{
		String strStatus=null;
		
		try
		{
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address1'])[3]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address2'])[3]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='city'])[3]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("//select[@name='stateOrProvince']"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='zipOrPostalCode'])[1]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='phone'])[3]"), "Invisible", "",5));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	
	*//***************************************************************************
	 * Method Name			: isElementExistForList()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static void deleteFile(WebDriver driver)
	{
		
		
		try {
			String path=System.getProperty("user.dir")+"\\src\\test\\resources\\Results\\DetailedReport.html";
            File file = new File(path);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String validationForSaveForLaterWithSignIn()
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		ArrayList<String> missingProductName=new ArrayList<String>();
		ArrayList<String> allProductName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		try {
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			String totalItemsBeforeSaveForLater=UIFoundation.getText(ListPage.btnCartCount);
			System.out.println("Number of Products Added to the CART :"+totalItemsBeforeSaveForLater);
			System.out.println("Number of Products in Saved For Later Section :"+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			
			if(!UIFoundation.getText(CartPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			
			if(!UIFoundation.getText(CartPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(CartPage.lnkFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(CartPage.lnkFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			String saveForLaterCnt=UIFoundation.getText(CartPage.spnSaveForLaterCount);
			int saveForLaterCount=Integer.parseInt(saveForLaterCnt.replaceAll("\\(|\\)", ""));
			boolean saveForLaterTitle=UIFoundation.isDisplayed(CartPage.spnSaveForLaterHeadline);
			boolean noItemsInSaveForLater=UIFoundation.isDisplayed(CartPage.spnNoItemsInSaveForLater);
			boolean saveForLaterAfterCheckout=UIFoundation.isDisplayed(CartPage.spnSaveForLaterAfterCheckout);
			if(saveForLaterTitle==true && noItemsInSaveForLater==true && saveForLaterAfterCheckout==true)
			{
				System.out.println("==========Verify save for later section, if the list empty===============");
				System.out.println("Title 'Saved for Later (0)' is displayed");
				System.out.println("'You have no items saved' text is available");
				System.out.println("Save for later section is available below the Checkout button");
				
			}
			System.out.println("=======================================================================");
			System.out.println("=======================CART Order Summary Before Moving to Saved for Later =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items before moving :"+totalPriceBeforeSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Name moving from CART to �Saved For Later�:"+expectedItemsName);
			if(!UIFoundation.getText(CartPage.lnkThirdProductSaveForLater).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkThirdProductSaveForLater));
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProductSaveForLater).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkSecondProductSaveForLater));
			}
			UIFoundation.waitFor(2L);
			String totalItemsAfterSaveForLater=UIFoundation.getText(ListPage.btnCartCount);
			String totalPriceAfterSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInSaveFor).contains("Fail"))
			{
				String actualProducts1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor).contains("Fail"))
			{
				String actualProducts2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}
			String actualProducts1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
			String actualProducts2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
			actualItemsName.add(actualProducts1);
			actualItemsName.add(actualProducts2);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
		//	System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
		//	System.out.println("1) "+actualProducts1);
		//	System.out.println("2) "+actualProducts2);
			System.out.println("=======================================================================");
			System.out.println("Product Names in �Saved for Later�: "+actualItemsName);
			System.out.println("=======================Order Summary After Moving =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items after moving :"+totalPriceAfterSaveForLater);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			System.out.println("=======================================================================");
			int totalItemsInCartBeforeSaveForLater=Integer.parseInt(totalItemsBeforeSaveForLater);
			int totalItemsInCartAfterSaveForLater=Integer.parseInt(totalItemsAfterSaveForLater);
			double totPriceBefrSaveForLater=Double.parseDouble(totalPriceBeforeSaveForLater.substring(1));
			double totPriceAftrSaveForLater=Double.parseDouble(totalPriceAfterSaveForLater.substring(1));
			for(int i=0;i<expectedItemsName.size();i++)
			{
				
				if(actualItemsName.contains(expectedItemsName.get(i)))
				{
					allProductName.add(expectedItemsName.get(i));
				}
				else
				{
					missingProductName.add(expectedItemsName.get(i));
				}
			}
			if(strStatus.contains("false"))
			{
				System.out.println("Product Name "+missingProductName+"is not matched or missing.");
				return "Fail";
				
			}
			else
			{
				System.out.println("Product Names are matched before moving and after moving to the �Saved For Later�");
				return "Pass";
			}
			
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}
	
	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String validationForSaveForLaterWithoutSignIn()
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		ArrayList<String> missingProductName=new ArrayList<String>();
		ArrayList<String> allProductName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		   String screenshotName = "Scenarios_SaveForLater_Screenshot.jpeg";
			
		try {
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			String totalItemsBeforeSaveForLater=UIFoundation.getText(ListPage.btnCartCount);
			System.out.println("Number of Products Added to the CART :"+totalItemsBeforeSaveForLater);
			System.out.println("Number of Products in Saved For Later Section :"+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			
			if(!UIFoundation.getText(CartPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			
			if(!UIFoundation.getText(CartPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(CartPage.lnkFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(CartPage.lnkFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			String saveForLaterCnt=UIFoundation.getText(CartPage.spnSaveForLaterCount);
			int saveForLaterCount=Integer.parseInt(saveForLaterCnt.replaceAll("\\(|\\)", ""));
			boolean saveForLaterTitle=UIFoundation.isDisplayed(CartPage.spnSaveForLaterHeadline);
			boolean noItemsInSaveForLater=UIFoundation.isDisplayed(CartPage.spnNoItemsInSaveForLater);
			boolean saveForLaterAfterCheckout=UIFoundation.isDisplayed(CartPage.spnSaveForLaterAfterCheckout);
			if(saveForLaterTitle==true && noItemsInSaveForLater==true && saveForLaterAfterCheckout==true)
			{
				System.out.println("==========Verify save for later section, if the list empty===============");
				System.out.println("Title 'Saved for Later (0)' is displayed");
				System.out.println("'You have no items saved' text is available");
				System.out.println("Save for later section is available below the Checkout button");
				
				strStatus+=true;
			      String objDetail="Verified  save for later section, if the list empty ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				System.out.println("Verified save for later section, if the list empty");
				
			}else{
					strStatus+=false;
			       String objDetail="Verify save for later section, if the list empty is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.out.println("Verify save for later section, if the list empty is failed");
			}
			System.out.println("=======================================================================");
			System.out.println("=======================CART Order Summary Before Moving to Saved for Later =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items before moving :"+totalPriceBeforeSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Names moving from CART to �Saved For Later�:"+expectedItemsName);
			if(!UIFoundation.getText(CartPage.lnkThirdProductSaveForLater).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkThirdProductSaveForLater));
				
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProductSaveForLater).contains("Fail"))
			{
				String saveForLaterLink=String.valueOf(UIFoundation.clickObject(CartPage.lnkSecondProductSaveForLater));
				strStatus+=saveForLaterLink;
				boolean saveForLaterLnk =String.valueOf(saveForLaterLink) != null;
				if(saveForLaterLnk){
						strStatus+=true;
				      String objDetail="Verified the functionality of 'Save for later' link in cart section";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("Verified the functionality of 'Save for later' link in cart section");
				}else{
						strStatus+=false;
				       String objDetail="Verify the functionality of 'Save for later' link in cart section is failed";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
						  
					   System.out.println("Verify the functionality of 'Save for later' link in cart section is failed");
				}
				
			}
			UIFoundation.waitFor(2L);
			String totalItemsAfterSaveForLater=UIFoundation.getText(ListPage.btnCartCount);
			String totalPriceAfterSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInSaveFor).contains("Fail"))
			{
				String actualProducts1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor).contains("Fail"))
			{
				String actualProducts2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}
			String actualProducts1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
			String actualProducts2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
			actualItemsName.add(actualProducts1);
			actualItemsName.add(actualProducts2);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
		//	System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
		//	System.out.println("1) "+actualProducts1);
		//	System.out.println("2) "+actualProducts2);
			System.out.println("=======================================================================");
			System.out.println("Product Names in �Saved for Later�: "+actualItemsName);
			System.out.println("=======================Order Summary After Moving =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items after moving :"+totalPriceAfterSaveForLater);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			System.out.println("=======================================================================");
			if(UIFoundation.isDisplayed(CartPage.spnSaveForLaterCount))
			{
					strStatus+=true;
			      String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			}else{
					strStatus+=false;
			       String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount);;
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.out.println("Verify save for later section, if the list non empty is failed");
			}
			
			int cartCountBeforeMoveToCart=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkmoveToCartFirstLink));
			UIFoundation.waitFor(2L);
			int cartCountAfterMoveToCart=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			if(cartCountAfterMoveToCart>cartCountBeforeMoveToCart){
				strStatus+=true;
				ReportUtil.addTestStepsDetails("Products are moved to cart on clicking 'Move To cart'", "Pass", "");
				logger.pass("Products are moved to cart on clicking 'Move To cart'");
			}else{
				strStatus+=false;
				String objDetail=("Products are not moved to cart on clicking 'Move To cart'");
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					
			}
			int totalItemsInCartBeforeSaveForLater=Integer.parseInt(totalItemsBeforeSaveForLater);
			int totalItemsInCartAfterSaveForLater=Integer.parseInt(totalItemsAfterSaveForLater);
			double totPriceBefrSaveForLater=Double.parseDouble(totalPriceBeforeSaveForLater.substring(1));
			double totPriceAftrSaveForLater=Double.parseDouble(totalPriceAfterSaveForLater.substring(1));
			for(int i=0;i<expectedItemsName.size();i++)
			{
				
				if(actualItemsName.contains(expectedItemsName.get(i)))
				{
					allProductName.add(expectedItemsName.get(i));
				}
				else
				{
					missingProductName.add(expectedItemsName.get(i));
				}
			}
			if(strStatus.contains("false"))
			{
				System.out.println("Product Name "+missingProductName+"is not matched or missing.");
				return "Fail";
				
			}
			else
			{
				System.out.println("Product Names are matched before moving and after moving to the �Saved For Later�");
				return "Pass";
			}
			
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}
	
	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String validationForRemoveProductsSaveForLater()
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		String actualProducts1=null;
		String actualProducts2=null;
		String actualProducts3=null;
		String actualProducts4=null;
		String actualProducts5=null;
		boolean isSorted = true;
		try {
			if(!UIFoundation.getText(CartPage.lnkFifthProductSaveForLater).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkFifthProductSaveForLater));
				UIFoundation.waitFor(2L);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFourthProductSaveForLater).contains("Fail"))
			{
				
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkFourthProductSaveForLater));
				UIFoundation.waitFor(2L);
			}
			
			if(!UIFoundation.getText(CartPage.lnkThirdProductSaveForLater).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkThirdProductSaveForLater));
				UIFoundation.waitFor(2L);
			}
			
			if(!UIFoundation.getText(CartPage.lnkSecondProductSaveForLater).contains("Fail"))
			{


				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkSecondProductSaveForLater));
				UIFoundation.waitFor(2L);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFirstProductSaveForLater).contains("Fail"))
			{
				UIFoundation.waitFor(3L);
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkFirstProductSaveForLater));
			}

			driver.navigate().refresh();

			UIFoundation.waitFor(2L);

			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInSaveFor).contains("Fail"))
			{
				actualProducts1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}
			
			if(!UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor).contains("Fail"))
			{
				actualProducts2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}
			
			if(!UIFoundation.getText(CartPage.lnkThirdProductInSaveFor).contains("Fail"))
			{
				actualProducts3=UIFoundation.getText(CartPage.lnkThirdProductInSaveFor);
				System.out.println("3) "+actualProducts3);
				actualItemsName.add(actualProducts3);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFourthProdcutInSaveFor).contains("Fail"))
			{
				actualProducts4=UIFoundation.getText(CartPage.lnkFourthProdcutInSaveFor);
				System.out.println("4) "+actualProducts4);
				actualItemsName.add(actualProducts4);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFifthProductInSaveFor).contains("Fail"))
			{
				actualProducts5=UIFoundation.getText(CartPage.lnkFifthProductInSaveFor);
				System.out.println("5) "+actualProducts5);
				actualItemsName.add(actualProducts5);
			}
			 for(int i = 0; i < actualItemsName.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((actualItemsName.get(i).compareToIgnoreCase(actualItemsName.get(i + 1)) > 0) && (actualItemsName.get(i).compareToIgnoreCase(expectedItemsName.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Verify products are listed in Alphabatical order in save for later section");
					
			    }
			    else
			    {
			    	System.err.println("Prooducts are not listed in Alphabatical order in save for later section");
					
			    }
			System.out.println("=======================================================================");
			if(strStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}
	
	/***************************************************************************
	 * Method Name			: discountCalculator()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static void discountCalculator(String subTotal,String promoCode)
	{
		
		String sub_Total=null;
		String prome_Code=null;
		try {
			sub_Total=subTotal;
			sub_Total = sub_Total.replaceAll("[$,]","");
			prome_Code=promoCode;
			prome_Code = prome_Code.replaceAll("[$,]","");
			double sub_total=Double.parseDouble(sub_Total);
		//	double promo_code=Double.parseDouble(prome_Code);
			//promo_code=(promo_code*10/100);
		//	promo_code=Math.round(promo_code*100);
			//promo_code=promo_code/100;
			double discountPrice=(sub_total*10/100);
			discountPrice=Math.round(discountPrice*100);
			discountPrice=discountPrice/100;
			String discountedPrice = Double.toString(discountPrice);
			UIFoundation.waitFor(3L);
			//double promoCode;
			if(promoCode==discountedPrice)
				
			{
				System.out.println("Thanx for using promocde,10% of amount is discounted from SubTotal");
			
				
			}
			else
			{
				System.out.println("Sorry unable to discount 10% of amount from SubTotal");
				
			}
		
        } catch (Exception e) {
        	e.printStackTrace();
            
        }
	}
	
	/***************************************************************************
	 * Method Name			: removerProductPrice()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static boolean removerProductPrice(WebDriver driver,String subTotalBefore,String subTotalAfter,String secondProductPrice, String thirdProductPrice)
	{
		
		String sub_Total_Before=null;
		String sub_Total_After=null;
		String second_Product_Price=null;
		String third_Product_Price=null;

		try {
			sub_Total_Before=subTotalBefore;
			sub_Total_After=subTotalAfter;
			second_Product_Price=secondProductPrice;
			third_Product_Price=thirdProductPrice;
			double subTotal_Before=Double.parseDouble(sub_Total_Before.substring(1));
			double subTotal_After=Double.parseDouble(sub_Total_After.substring(1));
			double secondProduct_Price=Double.parseDouble(second_Product_Price.substring(1));
			double thirdProduct_Price=Double.parseDouble(third_Product_Price.substring(1));
			double removedProductsPrice=secondProduct_Price+thirdProduct_Price;
			double finalSubtotal=subTotal_Before-removedProductsPrice;
			finalSubtotal=Math.round(finalSubtotal*100);
			finalSubtotal=finalSubtotal/100;
			if(finalSubtotal==subTotal_After)
			{
				System.out.println("Two products are removed from the cart");
				return true;
				
			}
			else
			{
				System.out.println("Unable to remove products from the cart");
				return false;
			}
		
        } catch (Exception e) {
        	return false;
            
        }
	}
	/***************************************************************************
	 * Method Name			: isCheckboxSelected()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	public static boolean isCheckboxSelected(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		String objStatus=null;
		//WebElement emailField=null;
		//String emailValue=null;
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtReceipientEmail))
			{
				ele.click();
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "RecipientEmail"));
				UIFoundation.waitFor(2L);
				recEmail=UIFoundation.getAttribute(FinalReviewPage.txtReceipientEmail);
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "RecipientGiftMessage", "RecipientMessage"));
				UIFoundation.waitFor(2L);
				recGiftMsg=UIFoundation.getText(driver, "RecipientGiftMessage");
				return true;
			}
			else
			{
				UIFoundation.waitFor(1L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(!emailValue.isEmpty())
				{
					driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']")).clear();
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "RecipientEmail"));
				}
				UIFoundation.waitFor(1L);
				recEmail=UIFoundation.getAttribute(FinalReviewPage.txtReceipientEmail);
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "RecipientGiftMessage", "RecipientMessage"));
				recGiftMsg=UIFoundation.getAttribute(driver, "RecipientGiftMessage");
				return true;
			}
			
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	
	*//***************************************************************************
	 * Method Name			: isCheckboxSelected()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	public static void isCheckboxSelectedAWU(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		WebElement emailField=null;
		String emailValue=null;
		
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
			
			if(!ele.isSelected())
			{
				ele.click();
				emailField=driver.findElement(By.xpath("//fieldset[@class='formWrap_group checkoutForm_textareaGroup']/input"));
				emailValue=emailField.getAttribute("value");
				if(!emailValue.isEmpty())
				{
					emailField.clear();
				}
				
				
				UIFoundation.waitFor(1L);
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			
		}
	
	}
	
	*//***************************************************************************
	 * Method Name			: isCheckboxSelected()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static void isGiftCheckboxSelected(By strObjectName)
	{
		
		WebElement emailField=null;
		String emailValue=null;
		
		try
		{
//			ele=driver.findElement(object.getLocator(strObjectName));
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtReceipientEmail))
			{
				WebElement ele1=driver.findElement(By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label"));
				ele1.click();
				UIFoundation.waitFor(2L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(emailValue.isEmpty())
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "RecipientEmail"));
				}
				else
				{
					emailField.clear();
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "RecipientEmail"));
					recEmail=UIFoundation.getAttribute(FinalReviewPage.txtReceipientEmail);
				}
				
				
			}
			else
			{
				UIFoundation.waitFor(1L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(emailValue.isEmpty())
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "RecipientEmail"));
				}
				else
				{
					emailField.clear();
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "RecipientEmail"));
					recEmail=UIFoundation.getAttribute(FinalReviewPage.txtReceipientEmail);
				}
			}
			
		}catch(Exception e)
		{
			e.printStackTrace();
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: validationForSortZtoA()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForSorting()
	{
		
		ArrayList<String> arrayList=new ArrayList<String>();
		
		   boolean isSorted = true;
			String screenshotName = "Scenarios_ZtoA_Screenshot.jpeg";
			
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.lnkFrstProductName);
			String products2=UIFoundation.getText(ListPage.lnkSecProductName);
			String products3=UIFoundation.getText(ListPage.lnkThrdProductName);
			String products4=UIFoundation.getText(ListPage.lnkFrthProductName);
			String products5=UIFoundation.getText(ListPage.lnkFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0 ) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) == 0 ))
			    		   { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	  objStatus+=true;
				      String objDetail="Products names are sorted in the order of Z-A";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	  System.out.println("Products names are sorted in the order of Z-A");
					return true;
			    }
			    else
			    {
			    		objStatus+=false;
				       String objDetail="Products names are not sorted in the order of Z-A";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail);
			    	System.err.println("Products names are not sorted in the order of Z-A");
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: validationForSortAtoZ()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForSortingAtoZ()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
			String screenshotName = "Scenarios_AtoZ_Screenshot.jpeg";
			
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.lnkFrstProductName);
			String products2=UIFoundation.getText(ListPage.lnkSecProductName);
			String products3=UIFoundation.getText(ListPage.lnkThrdProductName);
			String products4=UIFoundation.getText(ListPage.lnkFrthProductName);
			String products5=UIFoundation.getText(ListPage.lnkFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			arrayListYr.add(products1.replaceAll("[^0-9]", ""));
			arrayListYr.add(products2.replaceAll("[^0-9]", ""));
			arrayListYr.add(products3.replaceAll("[^0-9]", ""));
			arrayListYr.add(products4.replaceAll("[^0-9]", ""));
			arrayListYr.add(products5.replaceAll("[^0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	 objStatus+=true;
				      String objDetail="Products names are sorted in the order of A-Z";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	  System.out.println("Products names are sorted in the order of A-Z");
					return true;
			    }
			    else
			    {
			    	objStatus+=false;
				       String objDetail="Products names are not sorted in the order of A-Z";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail);
			    	System.err.println("Products names are not sorted in the order of Z-A");
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	

	
	/***************************************************************************
	 * Method Name			: validationForSortingPrice()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForSortingPriceLtoH()
	{
		
		 List<Double> arrayList = new ArrayList<Double>();;
			Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
		boolean isSorted=true;
		String screenshotName = "Scenarios_LtoH_Screenshot.jpeg";
		
		try
		{
		//	driver.navigate().refresh();
			if(UIFoundation.isDisplayed(ListPage.lnkFrstProductPrice))
			{
				String products1=UIFoundation.getText(ListPage.lnkFrstProductPrice);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlows.productPrice(product1);
				arrayList.add(product1);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkFrstProductPriceSale))
				{
					String products1=UIFoundation.getText(ListPage.lnkFrstProductPriceSale);
					products1=products1.replaceAll("[ ]","");
					product1=Double.parseDouble(products1);
					product1=UIBusinessFlows.productPrice(product1);
					arrayList.add(product1);
				}
				
			}
			
			
			if(UIFoundation.isDisplayed(ListPage.lnkSecndProductPrice))
			{
				String products2=UIFoundation.getText(ListPage.lnkSecndProductPrice);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlows.productPrice(product2);
				arrayList.add(product2);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkSecondProductPriceSale))
				{
					String products2=UIFoundation.getText(ListPage.lnkSecondProductPriceSale);
					products2=products2.replaceAll("[ ]","");
					product2=Double.parseDouble(products2);
					product2=UIBusinessFlows.productPrice(product2);
					arrayList.add(product2);
				}
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkThrdProductPrice))
			{
				String products3=UIFoundation.getText(ListPage.lnkThrdProductPrice);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlows.productPrice(product3);
				arrayList.add(product3);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkThirdProductPriceSale))
				{
					String products3=UIFoundation.getText(ListPage.lnkThirdProductPriceSale);
					products3=products3.replaceAll("[ ]","");
					product3=Double.parseDouble(products3);
					product3=UIBusinessFlows.productPrice(product3);
					arrayList.add(product3);
				}
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkFrthProductPrice))
			{
				String products4=UIFoundation.getText(ListPage.lnkFrthProductPrice);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlows.productPrice(product4);
				arrayList.add(product4);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkFourthProductPriceSale))
				{
					String products4=UIFoundation.getText(ListPage.lnkFourthProductPriceSale);
					products4=products4.replaceAll("[ ]","");
					product4=Double.parseDouble(products4);
					product4=UIBusinessFlows.productPrice(product4);
					arrayList.add(product4);
				}
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkFifhtProductPrice))
			{
				String products5=UIFoundation.getText(ListPage.lnkFifhtProductPrice);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlows.productPrice(product5);
				arrayList.add(product5);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkFifthProductPriceSale))
				{
					String products5=UIFoundation.getText(ListPage.lnkFifthProductPriceSale);
					products5=products5.replaceAll("[ ]","");
					product5=Double.parseDouble(products5);
					product5=UIBusinessFlows.productPrice(product5);
					arrayList.add(product5);
				}
				
			}
			if(!UIFoundation.getText(ListPage.lnkFrstProductPrice).contains("Fail"))
			{	String products1=UIFoundation.getText(ListPage.lnkFrstProductPrice);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlows.productPrice(product1);
			}
			if(!UIFoundation.getText(ListPage.lnkSecndProductPrice).contains("Fail"))
			{
				String products2=UIFoundation.getText(ListPage.lnkSecndProductPrice);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlows.productPrice(product2);
			}
			if(!UIFoundation.getText(ListPage.lnkThrdProductPrice).contains("Fail"))
			{
				String products3=UIFoundation.getText(ListPage.lnkThrdProductPrice);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlows.productPrice(product3);
			}
			if(!UIFoundation.getText(ListPage.lnkFrthProductPrice).contains("Fail"))
			{
				String products4=UIFoundation.getText(ListPage.lnkFrthProductPrice);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlows.productPrice(product4);
			}
			if(!UIFoundation.getText(ListPage.lnkFrthProductPrice).contains("Fail"))
			{
				String products5=UIFoundation.getText(ListPage.lnkFifhtProductPrice);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlows.productPrice(product5);
			}
			Double products1=UIFoundation.getDouble(ListPage.lnkFrstProductPrice);
			Double products2=UIFoundation.getDouble(ListPage.lnkSecndProductPrice);
			Double products3=UIFoundation.getDouble(ListPage.lnkThrdProductPrice);
			Double products4=UIFoundation.getDouble(ListPage.lnkFrthProductPrice);
			Double products5=UIFoundation.getDouble(ListPage.lnkFifhtProductPrice);
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)>(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	 objStatus+=true;
				      String objDetail="Products are sorted from low to high price";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	System.out.println("Products are sorted from low to high price");
					return true;
			    }
			    else
			    {
			    	objStatus+=false;
				       String objDetail="Products are not sorted from low to high price";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail);
			    	System.err.println("Products are not sorted from low to high price");
					return false;
			    }
			
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: validationForSortingPrice()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForSortingPriceHtoL()
	{

		 List<Double> arrayList = new ArrayList<Double>();;
		boolean isSorted=true;
		Double product1=0.0;
		Double product2=0.0;
		Double product3=0.0;
		Double product4=0.0;
		Double product5=0.0;
		String objStatus=null;
		String screenshotName = "Scenarios_HtoL_Screenshot.jpeg";
		
		try
		{
			
			
		//	driver.navigate().refresh();
			if(UIFoundation.isDisplayed(ListPage.lnkFrstProductPrice))
			{
				String products1=UIFoundation.getText(ListPage.lnkFrstProductPrice);
				products1=products1.replaceAll("[,]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlows.productPrice(product1);
				arrayList.add(product1);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkFrstProductPriceSale))
				{
					String products1=UIFoundation.getText(ListPage.lnkFrstProductPriceSale);
					products1=products1.replaceAll("[,]","");
					product1=Double.parseDouble(products1);
					product1=UIBusinessFlows.productPrice(product1);
					arrayList.add(product1);
				}
				
			}
			
			
			if(UIFoundation.isDisplayed(ListPage.lnkSecndProductPrice))
			{
				String products2=UIFoundation.getText(ListPage.lnkSecndProductPrice);
				products2=products2.replaceAll("[,]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlows.productPrice(product2);
				arrayList.add(product2);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkSecondProductPriceSale))
				{
					String products2=UIFoundation.getText(ListPage.lnkSecondProductPriceSale);
					products2=products2.replaceAll("[,]","");
					product2=Double.parseDouble(products2);
					product2=UIBusinessFlows.productPrice(product2);
					arrayList.add(product2);
				}
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkThrdProductPrice))
			{
				String products3=UIFoundation.getText(ListPage.lnkThrdProductPrice);
				products3=products3.replaceAll("[,]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlows.productPrice(product3);
				arrayList.add(product3);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkThirdProductPriceSale))
				{
					String products3=UIFoundation.getText(ListPage.lnkThirdProductPriceSale);
					products3=products3.replaceAll("[,]","");
					product3=Double.parseDouble(products3);
					product3=UIBusinessFlows.productPrice(product3);
					arrayList.add(product3);
				}
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkFrthProductPrice))
			{
				String products4=UIFoundation.getText(ListPage.lnkFrthProductPrice);
				products4=products4.replaceAll("[,]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlows.productPrice(product4);
				arrayList.add(product4);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkFourthProductPriceSale))
				{
					String products4=UIFoundation.getText(ListPage.lnkFourthProductPriceSale);
					products4=products4.replaceAll("[,]","");
					product4=Double.parseDouble(products4);
					product4=UIBusinessFlows.productPrice(product4);
					arrayList.add(product4);
				}
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkFifhtProductPrice))
			{
				String products5=UIFoundation.getText(ListPage.lnkFifhtProductPrice);
				products5=products5.replaceAll("[,]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlows.productPrice(product5);
				arrayList.add(product5);
				
			}else{
				if(UIFoundation.isDisplayed(ListPage.lnkFifthProductPriceSale))
				{
					String products5=UIFoundation.getText(ListPage.lnkFifthProductPriceSale);
					products5=products5.replaceAll("[,]","");
					product5=Double.parseDouble(products5);
					product5=UIBusinessFlows.productPrice(product5);
					arrayList.add(product5);
				}
				
			}
			if(!UIFoundation.getText(ListPage.lnkFrstProductPrice).contains("Fail"))
			{	String products1=UIFoundation.getText(ListPage.lnkFrstProductPrice);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlows.productPrice(product1);
			}
			if(!UIFoundation.getText(ListPage.lnkSecndProductPrice).contains("Fail"))
			{
				String products2=UIFoundation.getText(ListPage.lnkSecndProductPrice);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlows.productPrice(product2);
			}
			if(!UIFoundation.getText(ListPage.lnkThrdProductPrice).contains("Fail"))
			{
				String products3=UIFoundation.getText(ListPage.lnkThrdProductPrice);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlows.productPrice(product3);
			}
			if(!UIFoundation.getText(ListPage.lnkFrthProductPrice).contains("Fail"))
			{
				String products4=UIFoundation.getText(ListPage.lnkFrthProductPrice);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlows.productPrice(product4);
			}
			if(!UIFoundation.getText(ListPage.lnkFrthProductPrice).contains("Fail"))
			{
				String products5=UIFoundation.getText(ListPage.lnkFifhtProductPrice);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlows.productPrice(product5);
			}
			String products1=UIFoundation.getText(ListPage.lnkFrstProductPrice);
			Double product01 = Double.parseDouble(products1.replaceAll("[,]",""));
			String products2=UIFoundation.getText(ListPage.lnkSecndProductPrice);
			Double product02 = Double.parseDouble(products2.replaceAll("[,]",""));
			String products3=UIFoundation.getText(ListPage.lnkThrdProductPrice);
			Double product03 = Double.parseDouble(products3.replaceAll("[,]",""));
			String products4=UIFoundation.getText(ListPage.lnkFrthProductPrice);
			Double product04 = Double.parseDouble(products4.replaceAll("[,]",""));
			String products5=UIFoundation.getText(ListPage.lnkFifhtProductPrice);
			Double product05 = Double.parseDouble(products5.replaceAll("[,]",""));
			arrayList.add(product01);
			arrayList.add(product02);
			arrayList.add(product03);
			arrayList.add(product04);
			arrayList.add(product05);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)<(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	 objStatus+=true;
				      String objDetail="Products are sorted from High to Low price";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	System.out.println("Products are sorted from High to Low price");
					return true;
			    }
			    else
			    {
			    	objStatus+=false;
				       String objDetail="Products are not sorted from High to Low price";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail);
			    	System.err.println("Products are not sorted from High to Low price");
					return false;
			    }
			
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	/***************************************************************************
	 * Method Name			: userProfileCreation()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String userProfileCreation() {
		String objStatus=null;
		try {
			log.info("The execution of method create Account started here");
			
			// driver.findElement(By.xpath("//a[@id='full-view-button']")).click();
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "accPassword"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
			UIFoundation.webDriverWaitForElement(LoginPage.btnCreateAccount, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement( LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			objStatus+= String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			System.out.println("userProfileCreation :"+objStatus);
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: validationForMyWineSortingPurchaseDateNewToOld()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForMyWineSortingPurchaseDateNewToOld(WebDriver driver)
	{
		String product1 = null;
		String product2 = null;
		String product3 = null;
		String product4 = null;
		String product5 = null;
		String product6 = null;
		String product7 = null;
		ArrayList<String> dates=new ArrayList<String>();
		String screenshotName = "Scenarios_PurchaseDateHtoL_Screenshot.jpeg";
		
		String objStatus="";
		try
		{	if(UIFoundation.isDisplayed(ListPage.lnkmyWineFirstProdPrice)){
			product1 = UIFoundation.getText(ListPage.lnkmyWineFirstProdPrice);
			product1=product1.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product1);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSecProdPrice)){
			product2 = UIFoundation.getText(ListPage.lnkmyWineSecProdPrice);
			product2=product2.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product2);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineThirdProdPrice)){
			product3 = UIFoundation.getText(ListPage.lnkmyWineThirdProdPrice);
			product3=product3.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product3);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineFourtProdPrice)){
			product4 = UIFoundation.getText(ListPage.lnkmyWineFourtProdPrice);
			product4=product4.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product4);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineFifthProdPrice)){
			product5 = UIFoundation.getText(ListPage.lnkmyWineFifthProdPrice);
			product5=product5.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product5);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSixthProdPrice)){
			product6 = UIFoundation.getText(ListPage.lnkmyWineSixthProdPrice);
			product6=product6.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product6);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSevenProdPrice)){
			product7 = UIFoundation.getText(ListPage.lnkmyWineSevenProdPrice);
			product7=product7.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product7);
			
		}	
		System.out.println(dates);
		for(int i=0;i<dates.size();i++){
			Date d1=new Date(dates.get(i));
			for(int j=i+1;j<dates.size();j++)
			{
				Date d2=new Date(dates.get(j));
				if(d1.before(d2) || d1.equals(d2)){
					objStatus+=true;
					
				}else{
					objStatus+=false;
				}
			}
			if(objStatus.contains("false")){
				 objStatus+=true;
			      String objDetail="Product's purchased rate from new to old are displayed with the respective selected sort option.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		    	System.out.println("Product's purchased rate from new to old are displayed with the respective selected sort option.");
				//return true;
			}else{
				 objStatus+=false;
			       String objDetail="Product's purchased rate from new to old are not displayed with the respective selected sort option.";
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			       System.err.println("Product's purchased rate from new to old are not displayed with the respective selected sort option.");
			      // return false;
			}
		}
		return objStatus;
		}catch(Exception e)
		{
			return objStatus;
			
		}
		
		
	
	}
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 *//*
	
	public static String login(WebDriver driver)
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			//objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			//objStatus+=String.valueOf(UIFoundation.clIckObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(driver, "signInLinkAccount")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "signInLinkAccount"));
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(driver, "QAUserPopUp")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "QAUserPopUp"));
				UIFoundation.webDriverWaitForElement(driver, "QAUserPopUp", "Invisible", "", 50);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}*/
	/***************************************************************************
	 * Method Name			: loginR()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	
	public static String loginR()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "usrID"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "passR"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String loginPrefferedAddress()
	{
		String objStatus=null;
		
		try
		{
			
			UIFoundation.waitFor(5L);
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			//objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.lnkAccSignIn)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "preferenceAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				logger.fail("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				logger.pass("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: validationForOnlyPreSaleProducts()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static boolean validationForOnlyPreSaleProducts()
	{
		String objStatus=null;
		String preSaleProducts1 = null;
		String preSaleProducts2 = null;
		String preSaleProducts3 = null;
		
		try
		{
			log.info("The execution of the method validationForOnlyPreSaleProducts started here ...");
			
				
				if (UIFoundation.isDisplayed(ListPage.txtPreSaleProduct)) {
					System.out.println("Pre-sale text is Displayed for first product");
					return true;
				} else {
					System.out.println("Product is not pre-sale product");
					return false;
				}

			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method validationForOnlyPreSaleProducts "+ e);
			
			return false;
		}
		
		
	}
	
	/***************************************************************************
	 * Method Name			: verifyClockIconPresent()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 *//*
	
	public static void verifyClockIconPresent(WebDriver driver)
	{

		
		try
		{
			log.info("The execution of the method validationForOnlyPreSaleProducts started here ...");
			if (UIFoundation.isDisplayed(driver, "FirstClockIcon")) {
				System.out.println("Clock icon is present for the first product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "SecondClockIcon")) {
				System.out.println("Clock icon is present for the Second product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "ThirdClockIcon")) {
				System.out.println("Clock icon is present for the Third product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "FourthClockIcon")) {
				System.out.println("Clock icon is present for the fourth product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "FifthClockIcon")) {
				System.out.println("Clock icon is present for the fifth product");
			}
			log.info("The execution of the method validationForOnlyPreSaleProducts ended here ...");

			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method validationForOnlyPreSaleProducts "+ e);
			
			
		}
	}
	
	*//***************************************************************************
	 * Method Name			: validationForGiftSortingZtoA()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForGiftSortingZtoA()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		   boolean isSorted = true;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.lnkFrstProductName);
			String products2=UIFoundation.getText(ListPage.lnkSecProductName);
			String products3=UIFoundation.getText(ListPage.lnkThrdProductName);
			String products4=UIFoundation.getText(ListPage.lnkFrthProductName);
			String products5=UIFoundation.getText(ListPage.lnkFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted in the order of Z-A");
					return true;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted in the order of Z-A");
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	
	
	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingAtoZ()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForGiftSortingAtoZ()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.lnkFrstProductName);
			String products2=UIFoundation.getText(ListPage.lnkSecProductName);
			String products3=UIFoundation.getText(ListPage.lnkThrdProductName);
			String products4=UIFoundation.getText(ListPage.lnkFrthProductName);
			String products5=UIFoundation.getText(ListPage.lnkFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			arrayListYr.add(products1.replaceAll("[^0-9]", ""));
			arrayListYr.add(products2.replaceAll("[^0-9]", ""));
			arrayListYr.add(products3.replaceAll("[^0-9]", ""));
			arrayListYr.add(products4.replaceAll("[^0-9]", ""));
			arrayListYr.add(products5.replaceAll("[^0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted in the order of A-Z");
					return true;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted in the order of A-Z");
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	

	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingPriceLtoH()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForGiftSortingPriceLtoH()
	{
		WebElement ele=null;
		String objStatus=null;
		 List<Double> arrayList = new ArrayList<Double>();;
		 Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
		boolean isSorted=true;
		try
		{
			driver.navigate().refresh();
			if(!UIFoundation.getText(ListPage.spnGiftSPriceFirstProduct).contains("Fail"))
			{	String products1=UIFoundation.getText(ListPage.spnGiftSPriceFirstProduct);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlows.productPrice(product1);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceSecondProduct).contains("Fail"))
			{
				String products2=UIFoundation.getText(ListPage.spnGiftSPriceSecondProduct);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlows.productPrice(product2);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceThirdProduct).contains("Fail"))
			{
				String products3=UIFoundation.getText(ListPage.spnGiftSPriceThirdProduct);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlows.productPrice(product3);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceFourthProduct).contains("Fail"))
			{
				String products4=UIFoundation.getText(ListPage.spnGiftSPriceFourthProduct);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlows.productPrice(product4);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceFifthProduct).contains("Fail"))
			{
				String products5=UIFoundation.getText(ListPage.spnGiftSPriceFifthProduct);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlows.productPrice(product5);
			}
			driver.navigate().refresh();
			Double products1=UIFoundation.getDouble(ListPage.spnGiftSPriceFirstProduct);
			Double products2=UIFoundation.getDouble(ListPage.spnGiftSPriceSecondProduct);
			Double products3=UIFoundation.getDouble(ListPage.spnGiftSPriceThirdProduct);
			Double products4=UIFoundation.getDouble(ListPage.spnGiftSPriceFourthProduct);
			Double products5=UIFoundation.getDouble(ListPage.spnGiftSPriceFifthProduct);
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)>(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted from low to high price");
					return true;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted from low to high price");
					return false;
			    }
			
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	/*====================================================================
	 * Method Name			: validationForGiftSortingAtoZ()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************/
	 
	public static String validationForResponseHeader(WebDriver driver, String prodName)
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		   String screenshotName = "Scenarios_listViewPr_Screenshot.jpeg";
			
		try
		{
			String products1=UIFoundation.getText(ListPage.listPageFirstProdName);
			String products2=UIFoundation.getText(ListPage.listPageSecProdName);
			String products3=UIFoundation.getText(ListPage.listPageThirdProdName);
			String products4=UIFoundation.getText(ListPage.listPageFourthProdName);
			String products5=UIFoundation.getText(ListPage.listPageFifthProdName);
			arrayList.add(products1);
			arrayList.add(products2);
			arrayList.add(products3);
			arrayList.add(products4);
			arrayList.add(products5);
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).contains(prodName))) { 
			    	   objStatus+=true;
			       }else{
			    	   objStatus+=false;
			       }
			      
			    }
				if (objStatus.contains("false"))
				{
					 String objDetail="Verified the content of the list test case if failed";
					System.out.println("Verified the content of the list test case if failed");
					 UIFoundation.captureScreenShot(screenshotpath+"fet", objDetail);
					return objStatus;
				}
				else
				{
					System.out.println("Verified the content of the list and data is dispalyed");
					return objStatus;
				}
	
		}catch(Exception e)
		{
			return objStatus;
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: validationForMyWineSortingRatingDateNewToOld()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForMyWineSortingRatingDateOldToNew()
	{
		String product1 = null;
		String product2 = null;
		String product3 = null;
		String product4 = null;
		String product5 = null;
		String product6 = null;
		String product7 = null;
		ArrayList<String> dates=new ArrayList<String>();
		String screenshotName = "Scenarios_RatingDateLtoN_Screenshot.jpeg";
		
		String objStatus="";
		try
		{	
				if(UIFoundation.isDisplayed(ListPage.lnkmyWineFirstProdPrice)){
			product1 = UIFoundation.getText(ListPage.lnkmyWineFirstProdPrice);
			product1=product1.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product1);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSecProdPrice)){
			product2 = UIFoundation.getText(ListPage.lnkmyWineSecProdPrice);
			product2=product2.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product2);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineThirdProdPrice)){
			product3 = UIFoundation.getText(ListPage.lnkmyWineThirdProdPrice);
			product3=product3.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product3);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineFourtProdPrice)){
			product4 = UIFoundation.getText(ListPage.lnkmyWineFourtProdPrice);
			product4=product4.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product4);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineFifthProdPrice)){
			product5 = UIFoundation.getText(ListPage.lnkmyWineFifthProdPrice);
			product5=product5.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product5);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSixthProdPrice)){
			product6 = UIFoundation.getText(ListPage.lnkmyWineSixthProdPrice);
			product6=product6.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product6);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSevenProdPrice)){
			product7 = UIFoundation.getText(ListPage.lnkmyWineSevenProdPrice);
			product7=product7.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product7);
			
		}	
		
		System.out.println(dates);
		for(int i=0;i<dates.size();i++){
			Date d1=new Date(dates.get(i));
		
			for(int j=i+1;j<dates.size();j++)
			{
				Date d2=new Date(dates.get(j));
				if(d1.before(d2) || d1.equals(d2)){
					objStatus+=true;
					
				}else{
					objStatus+=false;
				}
			}
		}
		if(objStatus.contains("false")){
			objStatus+=false;
			  String objDetail="Rating date from old to new are not displayed with the respective selected sort option.";
		       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
		       System.err.println("Rating date from old to new  are not displayed with the respective selected sort option.");
		    
			//return true;
		}else{
			 
			 objStatus+=true;
			  String objDetail="Rating date from old to new  are displayed with the respective selected sort option.";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      logger.pass(objDetail);
	    	System.out.println("Rating date from old to new  are displayed with the respective selected sort option.");
		      // return false;
		}
		return objStatus;
		}catch(Exception e)
		{
			return objStatus;
			
		}
		
	}
	
	/***************************************************************************
	 * Method Name			: validationForMyWineSortingRatingDateNewToOld()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	@SuppressWarnings("deprecation")
	public static String validationForMyWineSortingRatingDateNewToOld()
	{
		String product1 = null;
		String product2 = null;
		String product3 = null;
		String product4 = null;
		String product5 = null;
		String product6 = null;
		String product7 = null;
		ArrayList<String> dates=new ArrayList<String>();
		String screenshotName = "Scenarios_RatingDateNtoO_Screenshot.jpeg";
		
		String objStatus="";
		try {
			if(UIFoundation.isDisplayed(ListPage.lnkmyWineFirstProdPrice)){
			product1 = UIFoundation.getText(ListPage.lnkmyWineFirstProdPrice);
			product1=product1.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product1);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSecProdPrice)){
			product2 = UIFoundation.getText(ListPage.lnkmyWineSecProdPrice);
			product2=product2.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product2);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineThirdProdPrice)){
			product3 = UIFoundation.getText(ListPage.lnkmyWineThirdProdPrice);
			product3=product3.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product3);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineFourtProdPrice)){
			product4 = UIFoundation.getText(ListPage.lnkmyWineFourtProdPrice);
			product4=product4.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product4);
			
		}
	if(UIFoundation.isDisplayed(ListPage.lnkmyWineFifthProdPrice)){
			product5 = UIFoundation.getText(ListPage.lnkmyWineFifthProdPrice);
			product5=product5.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product5);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSixthProdPrice)){
			product6 = UIFoundation.getText(ListPage.lnkmyWineSixthProdPrice);
			product6=product6.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product6);
			
		}
		if(UIFoundation.isDisplayed(ListPage.lnkmyWineSevenProdPrice)){
			product7 = UIFoundation.getText(ListPage.lnkmyWineSevenProdPrice);
			product7=product7.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product7);
			
		}	
		
		System.out.println(dates);
		for(int i=0;i<dates.size();i++){
			Date d1=new Date(dates.get(i));
			for(int j=i+1;j<dates.size();j++)
			{
				Date d2=new Date(dates.get(j));
				if(d1.after(d2) || d1.equals(d2)){
					objStatus+=true;
					
				}else{
					objStatus+=false;
				}
			}
		}
		if(objStatus.contains("false")){
			objStatus+=false;
			  String objDetail="Rating date from new to old are not displayed with the respective selected sort option.";
		       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
		       System.err.println("Rating date from new to old  are not displayed with the respective selected sort option.");
		    
			//return true;
		}else{
			 
			 objStatus+=true;
			  String objDetail="Rating date from new to old  are displayed with the respective selected sort option.";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      logger.pass(objDetail);
	    	System.out.println("Rating date from new to old  are displayed with the respective selected sort option.");
		      // return false;
		}
		return objStatus;
		}catch(Exception e)
		{
			return objStatus;
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingPriceHtoL()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForGiftSortingPriceHtoL()
	{

		 List<Double> arrayList = new ArrayList<Double>();;
		boolean isSorted=true;
		 Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
		try
		{
			driver.navigate().refresh();
			if(!UIFoundation.getText(ListPage.spnGiftSPriceFirstProduct).contains("Fail"))
			{	String products1=UIFoundation.getText(ListPage.spnGiftSPriceFirstProduct);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlows.productPrice(product1);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceSecondProduct).contains("Fail"))
			{
				String products2=UIFoundation.getText(ListPage.spnGiftSPriceSecondProduct);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlows.productPrice(product2);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceThirdProduct).contains("Fail"))
			{
				String products3=UIFoundation.getText(ListPage.spnGiftSPriceThirdProduct);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlows.productPrice(product3);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceFourthProduct).contains("Fail"))
			{
				String products4=UIFoundation.getText(ListPage.spnGiftSPriceFourthProduct);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlows.productPrice(product4);
			}
			if(!UIFoundation.getText(ListPage.spnGiftSPriceFifthProduct).contains("Fail"))
			{
				String products5=UIFoundation.getText(ListPage.spnGiftSPriceFifthProduct);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlows.productPrice(product5);
			}
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)<(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted from High to Low price");
					return true;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted from High to Low price");
					return false;
			    }
			
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static int cartCount()
	{
		String strStatus=null;
		int cartConut=0;
		
		try
		{
			String num=UIFoundation.getText(FinalReviewPage.spnCartHeadLineCount);
			cartConut = Integer.parseInt(num.replaceAll("[^0-9?!\\.]",""));
			return cartConut;
		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cartConut;
	}
	/***************************************************************************
	 * Method Name			: productPrice()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static double productPrice(double d)
	{
		String strStatus=null;
		int cartConut=0;
		double prodPrice=0;
		
		try
		{
			prodPrice=d;
			int x = 2; // Or 2, or whatever
			BigDecimal unscaled = new BigDecimal(prodPrice);
			BigDecimal scaled = unscaled.scaleByPowerOfTen(-x);
			prodPrice=scaled.doubleValue();
			return prodPrice;
		
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cartConut;
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String loginOtherState(String State)
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(8L);
			UIFoundation.SelectObject(LoginPage.dwnSelectState, State);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: recipientEditDryState()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static boolean recipientEditDryState()
	{
		String objStatus=null;
		try
		{
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}				
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnshipState, "UTWineState"));
			UIFoundation.clickObject(FinalReviewPage.dwnshipState);
			UIFoundation.clickObject(FinalReviewPage.txtZipCode);
			UIFoundation.clearField(FinalReviewPage.txtZipCode);
			UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCodeDryState");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			UIFoundation.waitFor(10L);
			if(objStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addSpiritsprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
				UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkVodka));
			
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: addTocrtWithDiffProd()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addTocrtWithDiffSpiritsProd() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));
			
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
//				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkVodka));
			UIFoundation.waitFor(5L);
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkTequila));
			
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: addTocrtFromPIPpage()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addTocrtFromPIPpage() {
		String objStatus = null;
		String addToCart1 = null;
		try {
			UIFoundation.waitFor(10L);
			/*objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "ItemPIP510898"));
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}*/
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "SPItemPIP23753"));
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.webDriverWaitForElement(ListPage.lnkFirstProductToCart, "Invisible", "", 50);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "SPItemPIP23754"));
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.webDriverWaitForElement(ListPage.lnkFirstProductToCart, "Invisible", "", 50);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: addTocrtFromPIPpage()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addTocrtFromMyWinePage() {
		String objStatus = null;
		String addToCart1 = null;
		
		try {
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "SPItemPIP23753"));
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(2L);
			
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "SPItemPIP23754"));
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(2L);
			
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "SPItemPIP23792"));
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(2L);
			
			
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: addGiftCertTocrt()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addGiftCertTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		String addToCart6 = null;
		try {
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftCards));
			
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.webDriverWaitForElement(ListPage.lnkFirstProductToCart, "Clickable", "", 50);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			addToCart6 = UIFoundation.getText(ListPage.lnkSixthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkSixthProductToCart);
			}
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));
			
			UIFoundation.waitFor(10L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.webDriverWaitForElement(ListPage.lnkFirstProductToCart, "Clickable", "", 50);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "listPageContainer");
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: addGiftBasketTocrt()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addGiftBasketTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		String addToCart6 = null;
		try {
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftBasketsFoodandChoco));
			
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.webDriverWaitForElement(ListPage.lnkFirstProductToCart, "Clickable", "", 50);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));
			
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.webDriverWaitForElement(ListPage.lnkFirstProductToCart, "Clickable", "", 50);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);				
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: Walgreenslogin()
	 * Created By			: Ramesh S 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Walgreenslogin into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String Walgreenslogin()
	{
		String objStatus=null;
		
		try
		{
			
			logger.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "walgreenusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			logger.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
}


