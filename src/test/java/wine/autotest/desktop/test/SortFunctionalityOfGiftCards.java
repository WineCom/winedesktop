package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class SortFunctionalityOfGiftCards extends Desktop {
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchGiftCard() {
		String objStatus=null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			//objStatus+=String.valueOf(UIFoundation.setObject(driver, "TxtsearchProduct", "sortGiftCard"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftCards));
			UIFoundation.waitFor(2L);
		//	System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
		//	UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(UIFoundation.clickObject(driver, "obj_AddToCart"));
		//	System.out.println(UIFoundation.getText(driver,"AddAgain"));
		//	objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CartBtnCount"));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name : sortingOptionsAtoZ() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsAtoZ() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsAtoZ started here ...");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.lnkSortOptions,"giftSortAtoZ"));
		//	objStatus += String.valueOf(UIFoundation.clickObject(driver, "GiftSortAtoZ"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlows.validationForGiftSortingAtoZ());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsAtoZ ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsAtoZ "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsZtoA() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsZtoA() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsZtoA started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.lnkSortOptions,"giftSortZtoA"));
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "GiftSortZtoA"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlows.validationForGiftSortingZtoA());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsZtoA ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsZtoA "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsLtoH() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsLtoH() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsLtoH started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			 objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.lnkSortOptions,"giftSortLtoH"));
		//	objStatus += String.valueOf(UIFoundation.clickObject(driver, "GiftSortLtoH"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlows.validationForGiftSortingPriceLtoH());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsLtoH ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsLtoH "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsHtoL() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsHtoL() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsHtoL started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.lnkSortOptions,"giftSortHtoL"));
		//	objStatus += String.valueOf(UIFoundation.clickObject(driver, "GiftSortHtoL"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlows.validationForGiftSortingPriceHtoL());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsHtoL ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsHtoL "
					+ e);
			return "Fail";
		}
	}

}
