package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyAddGiftWrappingToCartFunctionality extends Desktop {
	
	

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "gftvalueUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			logger.info("The execution of the method login ended here ...");
			
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : verifyGiftWrapping() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String verifyGiftWrapping() {
		String objStatus = null;
		String finalsubTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		int productCountInCart=0;
		int productCountInShoppingCart=0;
		   String screenshotName = "Scenarios_VerifyGiftWrapping_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method verifyGiftWrapping started here ...");
			UIFoundation.waitFor(12L);
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			String cartSubTotal=UIFoundation.getText(CartPage.spnSubtotal);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			UIFoundation.waitFor(3L);
			cartSubTotal = cartSubTotal.replaceAll("[$,]","");
			double cartSubTot=Double.parseDouble(cartSubTotal);
			productCountInCart=Integer.parseInt(UIFoundation.getText(CartPage.btnCartCount));
			System.out.println("Cart count:"+productCountInCart);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShipToThisAddress));
			UIFoundation.waitFor(2L);
			UIBusinessFlows.isGiftCheckboxSelected(FinalReviewPage.chkRecipientGift);
			
			/*WebElement gift = driver.findElement(By.xpath("(//main/section[@class='checkoutMainContent']//span[@class='formWrap_checkboxSpan'])[2]"));
			UIFoundation.scrollDownOrUpToParticularElement(driver, "RecipientGiftCheckbox");
			if(!gift.isSelected()){
				UIFoundation.clickObject(driver, "RecipientGiftCheckbox");
			}*/
		
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
			UIFoundation.waitFor(6L);
			if(UIFoundation.isDisplayed(FinalReviewPage.giftWrappingSection)){
				String section=UIFoundation.getText(FinalReviewPage.giftWrappingSection);
				 objStatus+=true;
			      String objDetail="Gift wrap section is added to the product";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Gift wrap section is not added to the product.";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radNogiftBag));
			UIFoundation.waitFor(6L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.giftWrappingSection)){
				objStatus+=true;
			      String objDetail="Gift wrap sections is removed removed.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Gift wrap sections is not removed removed.";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
			UIFoundation.waitFor(6L);
			String giftBag=UIFoundation.getText(FinalReviewPage.spnGiftBag);
			UIFoundation.waitFor(3L);
			giftBag=giftBag.replace(" ", ".");
			double giftBagValue=Double.parseDouble(giftBag.substring(0,4));
			System.out.println("GiftBag value for each products:"+giftBagValue);
			UIFoundation.waitFor(3L);
			String giftWrapCnt=UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			int giftWrapCount= Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap Count:"+giftWrapCount);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(13L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");

			finalsubTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+finalsubTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			//finalsubTotal=UIFoundation.getText(driver, "Subtotal");
			finalsubTotal = finalsubTotal.replaceAll("[$,]","");
			double finalSubTot=Double.parseDouble(finalsubTotal);
			finalSubTot=(finalSubTot*10/100);
			finalSubTot=Math.round(finalSubTot*100);
			finalSubTot=finalSubTot/100;//should divide by 10
			double finalSubtotalPrice=(giftBagValue*giftWrapCount)+cartSubTot;
			finalSubtotalPrice=(finalSubtotalPrice*10/100);
			finalSubtotalPrice=Math.round(finalSubtotalPrice*100);
			finalSubtotalPrice=finalSubtotalPrice/100;
			productCountInShoppingCart=Integer.parseInt(UIFoundation.getText(FinalReviewPage.spnCartHeadLineCount));
			if((finalSubtotalPrice==finalSubTot) && (productCountInShoppingCart==productCountInCart ))
			{
				 	objStatus+=true;
			      String objDetail="Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are matched";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are matched ");
			}
			else
			{
					objStatus+=false;
				   String objDetail="Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are not matched";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail);
				  System.err.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are not matched ");
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);

			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(10L);
			
			if(UIFoundation.isDisplayed(OrderDetailsPage.GiftBagInOrderHistory))
			{
				  objStatus+=true;
			      String objDetail="Gift wrap is listed in Order Summary as a separate line item";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Gift wrap is listed in Order Summary as a separate line item");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Gift wrap is not listed in Order Summary as a separate line item";
			       UIFoundation.captureScreenShot(screenshotpath+"GiftWrapOrderHistory", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Gift wrap is not listed in Order Summary as a separate line item");
			}
			System.out.println(objStatus);
			log.info("The execution of the method verifyGiftWrapping ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftWrapping "
					+ e);
			return "Fail";
		}
	}

}
