package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;


public class OrderCreationWithNewAccountBillingAddress extends Desktop {
	
	

	/***************************************************************************
	 * Method Name			: shippingDetails()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus=null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			//
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
//			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Clickable", "", 20);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			/*if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
//				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(4L);
			}*/
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails "
					+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name			: addNewCreditCard()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to add the new credit 
	 * 						  card details for billing process
	 ****************************************************************************
	 */
		public static String addNewCreditCard() {
			String objStatus=null;
			String subTotal=null;
			String shippingAndHandling=null;
			String total=null;
			String salesTax=null;
			   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
				
			try {
				log.info("The execution of the method addNewCreditCard started here ...");
				
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
					
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddPymtMethod));
					UIFoundation.waitFor(1L);
					
				}
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
//				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(2L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingZip, "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(10L);
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
				UIFoundation.waitFor(2L);
				
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(10L);
				String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				if(orderNum!="Fail")
				{ objStatus+=true;
			      String objDetail="Order is placed successfully";
			      UIFoundation.getOrderNumber(orderNum);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("orderCreation Wih NewAccount Billing Address test case executed succesfully");  
				}else
				{ 	objStatus+=false;
			       String objDetail="Order is null and Order cannot placed";
			       System.out.println("orderCreation Wih NewAccount Billing Address test case failed"); 
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				log.info("The execution of the method addNewCreditCard ended here ...");	
				if (objStatus.contains("false")) {
					
					return "Fail";
				} else {
					
					return "Pass";
				}
			} catch (Exception e) {
				log.error("there is an exception arised during the execution of the method addNewCreditCard "
						+ e);
				return "Fail";
			}
			
		}

}
