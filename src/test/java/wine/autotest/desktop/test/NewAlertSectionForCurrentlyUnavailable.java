package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class NewAlertSectionForCurrentlyUnavailable extends Desktop {
	
	/***************************************************************************
	 * Method Name			: NewAlertSectionForCurrentlyUnavailable()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String newAlertSectionForCurrentlyUnavailable()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_newArrival.jpeg";
			
		try
		{
			log.info("The execution of the method newAlertSectionForCurrentlyUnavailable started here ...");
			driver.get("https://qwww.wine.com/product/wind-gap-nellessen-vineyard-syrah-2014/156665");
			if(UIFoundation.isDisplayed(LoginPage.lnktoolTipComponet))
			{
				UIFoundation.clckObject(LoginPage.lnktoolTipComponet);
				UIFoundation.waitFor(1L);
			}
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(ListPage.spnprodAlert))
			{
				objStatus+=true;
				String objDetail="New Arrival Alert section is expanded by default when landing on a Currently Unavailable PIP";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="New Arrival Alert section is not expanded by default when landing on a Currently Unavailable PIP";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method newAlertSectionForCurrentlyUnavailable ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method newAlertSectionForCurrentlyUnavailable "+ e);
			return "Fail";
			
		}
	}

}
