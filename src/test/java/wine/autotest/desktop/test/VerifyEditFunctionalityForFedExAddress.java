package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyEditFunctionalityForFedExAddress extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clckObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "editFedexEmaill"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				logger.fail("Login test case is failed", MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				logger.pass("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: editFedexAddress()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String editFedexAddress() {
	String objStatus=null;
	   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method editHomeAddress started here ...");
		
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
			UIFoundation.clearField(FinalReviewPage.txtFedExAddEditName);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtPhoneNumAddress));
		//	driver.findElement(By.xpath("(//fieldset[@class='formWrap_group'])[1]")).click();
			String expectedEditedAddress=UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtFedExAddEditName, "firstName");
			expectedEditedAddress=expectedEditedAddress.replaceAll(" ", "");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnhomeAddressSave));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}
			String actualEditedAddress=UIFoundation.getText(FinalReviewPage.txtexpectedEditedName);
			actualEditedAddress=actualEditedAddress.replaceAll(" ", "");
			if(actualEditedAddress.equalsIgnoreCase(expectedEditedAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified  the Edit functionality for FedEx address in Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified the Edit functionality for FedEx address in Recipient page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the Edit functionality for FedEx address in Recipient page  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"PaymentSection", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify the Edit functionality for FedEx address in Recipient page is failed ");
			}
			log.info("The execution of the method editHomeAddress ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the Edit functionality for FedEx address in Recipient page test case is failed");
				logger.fail("Verify the Edit functionality for FedEx address in Recipient page test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the Edit functionality for FedEx address in Recipient page test case is executed successfully");
				logger.pass("Verify the Edit functionality for FedEx address in Recipient page test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method editHomeAddress "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
