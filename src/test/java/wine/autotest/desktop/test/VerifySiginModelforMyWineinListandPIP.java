package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifySiginModelforMyWineinListandPIP extends Desktop {
	
	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By : Ramesh S Reviewed By : Purpose : TM-4132
	 ****************************************************************************
	 */

	public static String signInModalForMyWinePageinListandPIP() {

		String objStatus = null;
		String ScreenshotName = "signInModalForMyWinePage.jpeg";
		
		try {
			log.info("sign In Modal For My Wine Page method started here........");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			if (UIFoundation.isElementDisplayed(LoginPage.spnsignInTextR)
					&& UIFoundation.isElementDisplayed(LoginPage.btnSignIn)) {
				objStatus += true;
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.winmodalWindowClose));
				String objDetail = "Sign modal is displayed when clicks on Mywine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywine";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			
			driver.get("https://qwww.wine.com/product/hess-allomi-cabernet-sauvignon-2017/525938");
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			if (UIFoundation.isElementDisplayed(LoginPage.spnsignInTextR)
					&& UIFoundation.isElementDisplayed(LoginPage.btnSignIn)) {
				objStatus += true;
				String objDetail = "Sign modal is displayed when clicks on Mywine badge in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywine badge in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			log.info("sign In Modal For My Wine Page method ended here........");

			if (objStatus.contains("false")) {

				System.out.println("signInModalForMyWinePage test case is failed");

				return "Fail";
			} else {
				System.out.println("signInModalForMyWinePage test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}


}
