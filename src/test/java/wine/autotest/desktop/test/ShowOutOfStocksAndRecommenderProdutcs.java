package wine.autotest.desktop.test;



import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class ShowOutOfStocksAndRecommenderProdutcs extends Desktop {
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String showOutOfStocksAndRecommenderProdutcs() {
		String objStatus=null;
		   String screenshotName = "Scenarios__showOutOfStock.jpeg";
					
					//WebDriverWait wait=null;
		try {
			
		
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.ChkShowOutOfStock));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(ListPage.spncurrentlyUnavailableProduct))
			{	
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.spncurrentlyUnavailableProductName));
				UIFoundation.waitFor(5L);
				if(UIFoundation.isDisplayed(ListPage.spncurrentlyUnavailableProductInPIP))
				{
					objStatus+=true;
					ReportUtil.addTestStepsDetails("Product is currnetly unavailable", "Pass", "");
					logger.pass("Product is currnetly unavailable");
					
				}else{
					objStatus+=false;
					String objDetail="Product is currnetly available";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					
				}
			}else{
				objStatus+=false;
				String objDetail="Product is currnetly available in List page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			//
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkrecommendedProductsList);
			UIFoundation.waitFor(2L);
			String status=String.valueOf(UIBusinessFlows.recommendedProductsAddToCartBtn());
			if(status.contains("false")){
				objStatus+=false;
				String objDetail="'Add to Cart button' is not displayed for Recommended Products'";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			UIFoundation.captureScreenShot(screenshotpath+"recommended", objDetail);
			}else{

			objStatus+=true;
			ReportUtil.addTestStepsDetails("'Add to Cart button' is displayed for Recommended Products'", "Pass", "");
			logger.pass("'Add to Cart button' is displayed for Recommended Products'");
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkrecommendedProdRightArrow));
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(ListPage.lnkrecommendedProdLeftArrow))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Next page of recommendations is displayed", "Pass", "");
				logger.pass("Next page of recommendations is displayed");
				
			}else{
				objStatus+=false;
				String objDetail="Next page of recommendations is not  displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"recommende", objDetail);
				
			}
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}

}
