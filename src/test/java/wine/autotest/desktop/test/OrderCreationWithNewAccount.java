package wine.autotest.desktop.test;

import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.test.Desktop;

public class OrderCreationWithNewAccount extends Desktop {
	

	static boolean isObjectPresent=true;
	/***************************************************************************
	 * Method Name : searchProductWithProdName() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to search for
	 * product with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName(WebDriver driver) {
		String objStatus = null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "vinye"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(12L);
			// System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
			// UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			// System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-2197,
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		   String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 70);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 70);
				
			}
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.spnlocalPickUp)){
				objStatus+=true;
				logger.pass("Local pickup icon is displayed next to pickup location address.");
				ReportUtil.addTestStepsDetails("Local pickup icon is displayed next to pickup location address.", "Pass", "");
			}else{
				objStatus+=false;
					String objDetail="Local pickup icon is not displayed next to pickup location address.";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
			
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
				objStatus+=true;
				logger.pass("DOB format is in'MM / DD / YYYY'.");
				ReportUtil.addTestStepsDetails("DOB format is in'MM / DD / YYYY'.", "Pass", "");
			}else{
				objStatus+=false;
					String objDetail="DOB is not in format 'MM / DD / YYYY'.";
					 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(5L);
			
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 70);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }
			if(UIFoundation.isDisplayed(ThankYouPage.spnTickSymbol) && UIFoundation.isDisplayed(ThankYouPage.spnOrderConfirmation))
			{
				  objStatus+=true;
			      String objDetail="Verified  the contents in the 'Thank you' page ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified  the contents in the 'Thank you' page ");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the contents in the 'Thank you' page  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.err.println("Verify the contents in the 'Thank you' page ");
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println(" Order creation with new account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with new account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}

}
