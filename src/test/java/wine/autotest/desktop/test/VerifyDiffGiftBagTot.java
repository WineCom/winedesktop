package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyDiffGiftBagTot extends Desktop {

	/***************************************************************************
	 * Method Name : verifyDiffGiftBagTotInOrderSummary() Created By : Vishwanath
	 * Chavan Reviewed By : Purpose :
	 ****************************************************************************
	 */
	public static String verifyDiffGiftBagTotInOrderSummary() {
		String objStatus = null;
		String addToCart1, addToCart2;
		String screenshotName = "VerifyDiffGiftBagTotInOrderSummary.jpeg";
		
		try {
			log.info("Verify Diff GiftBag Tot In Order Summarymethos started here");
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSangviovese));
			UIFoundation.waitFor(3L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				// UIFoundation.scrollDownOrUpToParticularElement(driver,
				// "FirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);

			} else {
				addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
				if (addToCart2.contains("Add to Cart")) {
					// UIFoundation.scrollDownOrUpToParticularElement(driver,
					// "SecondProductToCart");
					UIFoundation.waitFor(1L);
					UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
				}
			}
			// driver.navigate().to("https://qwww.wine.com/product/avalon-napa-cabernet-sauvignon-2014/159588");
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressR"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipR"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			objStatus += UIFoundation.clickObject(FinalReviewPage.lnkNogiftselected);
			WebElement gift = driver.findElement(By.xpath(
					"//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if (!gift.isSelected()) {
				UIFoundation.clickObject(FinalReviewPage.chkRecipientGift);
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
			if ((UIFoundation.isElementDisplayed(FinalReviewPage.lnkprodItemgiftIcon))&& UIFoundation.isElementDisplayed(FinalReviewPage.giftWrappingSection)) {
				objStatus += true;
				String objDetail = "Gift order summary and gift bag icon is displayed succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("Gift bag is added to cart and order summary details");
			} else {
				objStatus += false;
				String objDetail = "Gift order summary or gift bag icon is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.err.println("Gift order summary or gift bag icon is not added");
			}
			UIFoundation.waitFor(5L);
			String giftBag = UIFoundation.getText(FinalReviewPage.spnGiftBag);
			giftBag = giftBag.replace(" ", ".");
			double giftBagValue = Double.parseDouble(giftBag.substring(0, 4));
			System.out.println("GiftBag value for each products is :" + giftBagValue);
			String giftWrapCnt = UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			int giftWrapCount = Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: " + giftWrapCount + " item");
			UIFoundation.waitFor(5L);
			String giftbagsumminrecipient = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			double giftbagprice = Double.parseDouble(giftbagsumminrecipient.substring(1, 5));
			double giftbagtotal = giftWrapCount * giftBagValue;
			if (giftbagtotal == giftbagprice) {
				objStatus += true;
				String objDetail = "Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("GiftBag total for " + giftWrapCount + " item is " + giftbagprice);
			} else {
				objStatus += false;
				String objDetail = "Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEdit));
			UIFoundation.waitFor(4L);
			String giftbagsummincart = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			if (giftbagsumminrecipient.equals(giftbagsummincart)) {
				objStatus += true;
				String objDetail = "Gift bag summary in Recipient and Shopping Cart page are same";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("Gift bag summary in recipient and shopping cart page are same");
			} else {
				objStatus += false;
				String objDetail = "Gift bag summary in recipient and shopping cart page are not same";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Gift bag summary in recipient and shopping cart page are not same");
			}
			objStatus += String.valueOf(UIFoundation.SelectObject(CartPage.spnProductQuantity, "quantity"));
			UIFoundation.waitFor(2L);
			System.out.println(
					"============Gift Wrapping Summary after increasing the quantity in Shopping cart====================");
			System.out.println("Gift Wrapping : " + UIFoundation.getText(FinalReviewPage.giftWrappingSection));
			UIFoundation.waitFor(1L);

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkNogiftselected));
			giftWrapCnt = UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			giftWrapCount = Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: " + giftWrapCount + " item");
			UIFoundation.waitFor(5L);
			giftbagsumminrecipient = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			giftbagprice = Double.parseDouble(giftbagsumminrecipient.substring(1, 5));
			giftbagtotal = giftWrapCount * giftBagValue;
			if (giftbagtotal == giftbagprice) {
				objStatus += true;
				String objDetail = "Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("GiftBag total for " + giftWrapCount + " item is " + giftbagprice);
			} else {
				objStatus += false;
				String objDetail = "Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			System.out.println(
					"============Gift Wrapping Summary after increasing the quantity in Recipient page====================");
			System.out.println("Gift Wrapping : " + UIFoundation.getText(FinalReviewPage.giftWrappingSection));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag6_99));
			UIFoundation.waitFor(5L);
			giftBag = UIFoundation.getText(FinalReviewPage.rdogiftbag2);
			giftBag = giftBag.replace(" ", ".");
			giftBagValue = Double.parseDouble(giftBag.substring(0, 4));
			System.out.println("GiftBag value for each products is :" + giftBagValue);
			UIFoundation.waitFor(5L);
			giftWrapCnt = UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			giftWrapCount = Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: " + giftWrapCount + " item");
			UIFoundation.waitFor(5L);
			giftbagsumminrecipient = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			giftbagprice = Double.parseDouble(giftbagsumminrecipient.substring(1, 6));
			giftbagtotal = giftWrapCount * giftBagValue;
			if (giftbagtotal == giftbagprice) {
				objStatus += true;
				String objDetail = "Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("GiftBag total for " + giftWrapCount + " item is " + giftbagprice);
			} else {
				objStatus += false;
				String objDetail = "Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEdit));
			UIFoundation.waitFor(4L);
			giftbagsummincart = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			if (giftbagsumminrecipient.equals(giftbagsummincart)) {
				objStatus += true;
				String objDetail = "Gift bag summary in Recipient and Shopping Cart page are same";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("Gift bag summary in Recipient and shopping cart page are same");
			} else {
				objStatus += false;
				String objDetail = "gift bag summary in recipient and shopping cart page are not same";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Gift bag summary in recipient and shopping cart page are not same");
			}
			objStatus += String.valueOf(UIFoundation.SelectObject(CartPage.spnProductQuantity, "quantity1"));
			UIFoundation.waitFor(2L);
			System.out.println(
					"============Gift Wrapping Summary after increasing the quantity in Shopping cart====================");
			System.out.println("Gift Wrapping : " + UIFoundation.getText(FinalReviewPage.giftWrappingSection));
			UIFoundation.waitFor(1L);

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkNogiftselected));
			UIFoundation.waitFor(5L);
			giftWrapCnt = UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			giftWrapCount = Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: " + giftWrapCount + " item");
			giftbagsumminrecipient = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			giftbagprice = Double.parseDouble(giftbagsumminrecipient.substring(1, 6));
			giftbagtotal = giftWrapCount * giftBagValue;
			if (giftbagtotal == giftbagprice) {
				objStatus += true;
				String objDetail = "Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("GiftBag total for " + giftWrapCount + " item is " + giftbagprice);
			} else {
				objStatus += false;
				String objDetail = "Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			System.out.println(
					"============Gift Wrapping Summary after increasing the quantity in Recipient page====================");
			System.out.println("Gift Wrapping : " + UIFoundation.getText(FinalReviewPage.giftWrappingSection));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag9_99));
			UIFoundation.waitFor(5L);
			giftBag = UIFoundation.getText(FinalReviewPage.rdogiftbag3);
			giftBag = giftBag.replace(" ", ".");
			giftBagValue = Double.parseDouble(giftBag.substring(0, 4));
			System.out.println("GiftBag value for each products is :" + giftBagValue);
			UIFoundation.waitFor(5L);
			giftWrapCnt = UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			giftWrapCount = Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: " + giftWrapCount + " item");
			UIFoundation.waitFor(5L);
			giftbagsumminrecipient = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			giftbagprice = Double.parseDouble(giftbagsumminrecipient.substring(1, 6));
			giftbagtotal = giftWrapCount * giftBagValue;
			if (giftbagtotal == giftbagprice) {
				objStatus += true;
				String objDetail = "Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("GiftBag total for " + giftWrapCount + " item is " + giftbagprice);
			} else {
				objStatus += false;
				String objDetail = "Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEdit));
			UIFoundation.waitFor(4L);
			giftbagsummincart = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			if (giftbagsumminrecipient.equals(giftbagsummincart)) {
				objStatus += true;
				String objDetail = "Gift bag summary in Recipient and Shopping Cart page are same";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("gift bag summary in recipient and shopping cart page are same");
			} else {
				objStatus += false;
				String objDetail = "gift bag summary in recipient and shopping cart page are not same";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("gift bag summary in recipient and shopping cart page are not same");
			}
			objStatus += String.valueOf(UIFoundation.SelectObject(CartPage.spnProductQuantity, "quantity2"));
			UIFoundation.waitFor(2L);
			System.out.println(
					"============Gift Wrapping Summary after increasing the quantity in Shopping cart====================");
			System.out.println("Gift Wrapping : " + UIFoundation.getText(FinalReviewPage.giftWrappingSection));
			UIFoundation.waitFor(1L);

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkNogiftselected));
			UIFoundation.waitFor(5L);
			giftWrapCnt = UIFoundation.getText(FinalReviewPage.GiftWrapOptionCount);
			giftWrapCount = Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: " + giftWrapCount + " item");
			UIFoundation.waitFor(5L);
			giftbagsumminrecipient = UIFoundation.getText(FinalReviewPage.giftWrappingSection);
			giftbagprice = Double.parseDouble(giftbagsumminrecipient.substring(1, 6));
			giftbagtotal = giftWrapCount * giftBagValue;
			if (giftbagtotal == giftbagprice) {
				objStatus += true;
				String objDetail = "Gift bag added for N items is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("GiftBag total for " + giftWrapCount + " item is " + giftbagprice);
			} else {
				objStatus += false;
				String objDetail = "Gift bag added for N items is Incorrect";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			System.out.println("============Gift Wrapping Summary after increasing the quantity in Recipient page====================");
			System.out.println("Gift Wrapping : " + UIFoundation.getText(FinalReviewPage.giftWrappingSection));
			UIFoundation.waitFor(1L);
			log.info("The execution of the method verifyGiftWrapping ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftWrapping " + e);
			return "Fail";
		}

	}
}
