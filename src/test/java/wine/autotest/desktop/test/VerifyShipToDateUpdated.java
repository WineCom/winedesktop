package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyShipToDateUpdated extends Desktop {

	/***************************************************************************
	 * Method Name : VerifyShipToDateUpdated() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static String verifyShipToDateUpdated() {
		String objStatus = null;
				
		try {

			log.info("The execution of the method verifyShipToDateUpdated started here ...");
			//UIFoundation.SelectObject(driver, "SelectState", "shipDateUpdatedState");
			UIFoundation.waitFor(10L);
			driver.get("https://qwww.wine.com/product/the-federalist-lodi-cabernet-sauvignon-2017/630205");
			UIFoundation.waitFor(3L);
			String shipDateBeforIncQuantity = UIFoundation.getText(ListPage.spnfirstProdName);
			System.out.println("shipDateBeforIncQuantity : "+shipDateBeforIncQuantity);
			objStatus += String.valueOf(UIFoundation.selectLastValueFromDropdown(ListPage.dwnFirstProdQuantitySelect));
			UIFoundation.waitFor(2L);
			String shipDateAfterIncQuantity = UIFoundation.getText(ListPage.spnfirstProdName);
			UIFoundation.waitFor(2L);
			if (!shipDateBeforIncQuantity.equalsIgnoreCase(shipDateAfterIncQuantity)) {
				objStatus+=true;
				String objDetail="Ship to date is updated when user modifies quantity to add to cart in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				logger.pass(objDetail);
				System.out.println("Ship to date is updated when user modifies quantity to add to cart in PIP");
			} else {
				objStatus+=false;
				String objDetail="Ship to date is not updated when user modifies quantity to add to cart in PIP";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+"PIPDate", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Ship to date is not updated when user modifies quantity to add to cart in PIP");
			}
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkviewAllWine));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkviewAllWine));
			UIFoundation.waitFor(2L);
			String shipDateBeforIncQuantityInList = UIFoundation.getText(ListPage.spnfirstProdName);
			System.out.println("shipDateBeforIncQuantityInList : "+shipDateBeforIncQuantityInList);
			objStatus += String
					.valueOf(UIFoundation.selectLastValueFromDropdown(ListPage.dwnFirstProdQuantitySelect));
			UIFoundation.waitFor(2L);
			String shipDateAfterIncQuantityInList = UIFoundation.getText(ListPage.spnfirstProdName);
			UIFoundation.waitFor(2L);
			if (!shipDateBeforIncQuantityInList.equalsIgnoreCase(shipDateAfterIncQuantityInList)) {
				objStatus+=true;
				String objDetail="Ship to date is updated when user modifies quantity to add to cart in List Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				logger.pass(objDetail);
				System.out.println("Ship to date is updated when user modifies quantity to add to cart in List Page");
			} else {
				objStatus+=false;
				String objDetail="Ship to date is not updated when user modifies quantity to add to cart in List Page";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+"ListDate", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Ship to date is not updated when user modifies quantity to add to cart in List Page");
			}
			log.info("The execution of the method verifyShipToDateUpdated ended here ...");
			if (objStatus.contains("false")) {
				
				System.out.println(
						"Verify ship to date is updated when user modifies quantity to add to cart in PIP and List page test case is failed");
				return "Fail";
			
			} else {
				System.out.println(
						"Verify ship to date is updated when user modifies quantity to add to cart in PIP and List page test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error(
					"there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "
							+ e);
			return "Fail";

		}
	}

}
