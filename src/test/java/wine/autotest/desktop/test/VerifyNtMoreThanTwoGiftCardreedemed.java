package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyNtMoreThanTwoGiftCardreedemed extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "giftCertificateUn"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyNtMoreThanTwoGiftCardreedemed()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String verifyNtMoreThanTwoGiftCardreedemed()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_twoGiftCert_Screenshot.jpeg";
			
		try
		{
			log.info("The execution of the method enterGiftCertificate started here ...");
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(10L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(5L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(10L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(5L);
				
			}
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Verified multiple gift cards can be redeemed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Gift certificate has been expired.Please enter new gift certificate";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			
			String accountCreditLabelColor=UIFoundation.getColor(CartPage.spnaccountCreditLabelColor);
			if(accountCreditLabelColor.equalsIgnoreCase("#d61d2c"))
			{
				objStatus+=true;
				String objDetail="'Account credit' label and the discount amount is displayed in red color";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("'Account credit' label and the discount amount is displayed in red color");
			}
			else
			{
				objStatus+=false;
				String objDetail="'Account credit' label and the discount amount is not displayed in red color";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("'Account credit' label and the discount amount is not displayed in red");
			}
//			String Subtotal = UIFoundation.getText(CartPage.spnSubtotal);
//			Subtotal = Subtotal.replaceAll("[$,]", "");
//			double SubtotalAmount = Double.parseDouble(Subtotal);
//			String shippingAndHandling = UIFoundation.getText(CartPage.spnShippingHandling);
//			shippingAndHandling = shippingAndHandling.replaceAll("[$,]", "");
//			double shippingAndHandlingAmount = Double.parseDouble(shippingAndHandling);
//			String accountCreditLabelAmunt = UIFoundation.getText(CartPage.spnaccountCreditLabelAmount);
//			accountCreditLabelAmunt = accountCreditLabelAmunt.replaceAll("[$,]", "");
//			double accountCreditLabelAmount = Double.parseDouble(accountCreditLabelAmunt);
//			double totalAmount=SubtotalAmount+shippingAndHandlingAmount;
//			if(totalAmount==accountCreditLabelAmount){
//				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spntotalAmountZero));
//				objStatus+=String.valueOf(UIFoundation.isElementDisplayed(CartPage.spncreditRemainingHeader));
//			}
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify not more than 2 gift cards can be redeemed test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify not more than 2 gift cards can be redeemed test case executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}

}
