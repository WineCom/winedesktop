package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyTheFourAddressAndCreditcards extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyTheFourAddressAndCreditcards()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */	
	public static String verifyTheFourAddressAndCreditcards() {
		String objStatus=null;
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkRecipientShipToaddress);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientShipToaddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(5L);
			int AddressCount = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName, "AddressCreditCardCount", 1));
			int getAddressCount=UIFoundation.listSize(FinalReviewPage.lnkaddressCountInRecipient);
			if(AddressCount==getAddressCount){
				  objStatus+=true;
			      String objDetail="4 address cards are displayed by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="4 address cards are not displayed by default";
				UIFoundation.captureScreenShot(screenshotpath+"addressCard", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkviewAllAllAddress));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkmodalWindowIcon)){
				  objStatus+=true;
			      String objDetail="All the available address cards  are displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="All the available address cards  are not displayed";
				UIFoundation.captureScreenShot(screenshotpath+"allCreditCard", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkmodalWindowIcon));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			}
			int creditCardCount = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName, "AddressCreditCardCount", 1));
			int getcreditCard=UIFoundation.listSize(FinalReviewPage.lnkcreditCardCountInRecipient);
			if(creditCardCount==getcreditCard){
				  objStatus+=true;
			      String objDetail="4 credit cards are displayed by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="4 credit cards are not displayed by default";
				UIFoundation.captureScreenShot(screenshotpath+"creditCard", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkviewAllPaymentMethods));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkmodalWindowIcon)){
				  objStatus+=true;
			      String objDetail="All the available credit cards are displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="All the available credit cards are not displayed";
				UIFoundation.captureScreenShot(screenshotpath+"allCreditCard", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the 4 Address/Credit cards is displayed by default in Recipient/Payment methods page during checkout test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the 4 Address/Credit cards is displayed by default in Recipient/Payment methods page during checkout test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			return "Fail";
		}
	}

}
