package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyCollapsedPanelInShippingForm extends Desktop {
	
	/***************************************************************************
	 * Method Name : verifyCollapsedPanel() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String verifyCollapsedPanel() {
		String objStatus = null;
		   String screenshotName = "Scenarios__CollapseShippingScreenshot.jpeg";
					
		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(16L);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			boolean isDeliveryContinuePresent=UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue);
			
			boolean isPaymentContinuePresent=UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue);
			if(isDeliveryContinuePresent && isPaymentContinuePresent )
			{
				objStatus+=false;
				String objDetail="Verify the collapsed panel in shipping form test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
				
			}else
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verify the collapsed panel in shipping form test case is executed successfully.", "Pass", "");
				
		
			}
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify the collapsed panel in shipping form test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the collapsed panel in shipping form test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	

}
