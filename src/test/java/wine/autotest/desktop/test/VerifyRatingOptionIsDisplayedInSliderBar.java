package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyRatingOptionIsDisplayedInSliderBar extends Desktop {
	
	/***************************************************************************
	 * Method Name			: VerifyRatingOptionIsDisplayedInSliderBar()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String verifyRatingOptionIsDisplayedInSliderBar() {
		String objStatus=null;
		   String screenshotName = "Scenarios_SlidingBar_Screenshot.jpeg";
			
		try {
			log.info("The execution of method verifyRatingOptionIsDisplayedInSliderBar started here");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkRatingAndPrice));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spnsliderBarRating) && UIFoundation.isDisplayed(ListPage.spnsliderBarPricing))
			{
				  objStatus+=true;
			      String objDetail="Rating option is displayed in slider bar under filter section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				 String objDetail="Rating option is not displayed in slider bar under filter section";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method verifyRatingOptionIsDisplayedInSliderBar ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify Rating option is displayed in slider bar under filter section  test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Rating option is displayed in slider bar under filter section test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Existing customer should be a link in new customer register page  test case is failed");
			log.error("there is an exception arised during the execution of the method verifyRatingOptionIsDisplayedInSliderBar "
					+ e);
			return "Fail";
		}
	}

}
