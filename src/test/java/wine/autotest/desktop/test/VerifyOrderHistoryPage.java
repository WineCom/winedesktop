package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyOrderHistoryPage extends Desktop {


	/***************************************************************************
	 * Method Name : OrderHistoryProcessBarValidation() Created By : Ramesh S
	 * Reviewed By :
	 * Purpose : TM-4143
	 ****************************************************************************
	 */

	public static String OrderHistoryProcessBarValidation() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderHistoryProcessBarValidation_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method OrderHistoryProcessBarValidation started here ...");
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			
			UIFoundation.waitFor(10L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnContinuetostate)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnContinuetostate));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
		
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(20L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: " + orderNum);
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Order number is null.Order not placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			
			}
			
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(20L);
			if(UIFoundation.isElementDisplayed(OrderDetailsPage.spnProgressBarOrderHis)) {
				objStatus += true;
				String objDetail = "Order Progress Bar in Order History Page is Displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				}else {
					objStatus += false;
					String objDetail = "Order Progress Bar in Order History page is not Displayed";
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
					UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
				    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				
				}
			log.info("The execution of the method OrderHistoryProcessBarValidation ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("OrderHistoryProcessBarValidation test case is failed");
				return "Fail";
			} else {
				System.out.println("OrderHistoryProcessBarValidation test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method OrderHistoryProcessBarValidation " + e);
			objStatus += false;
			String objDetail = "Order number is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : OrderHistoryShippingAmountValidation() Created By : Ramesh S Reviewed By :
	 *Purpose : TM-4190
	 ****************************************************************************
	 */

	public static String OrderHistoryShippingAmountValidation() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String OrderHistotalShipping = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderHistoryShippingAmountValidation_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method OrderHistoryTotalAmountValidation started here ...");
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			
			UIFoundation.waitFor(10L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnContinuetostate)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnContinuetostate));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
		
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
	          UIFoundation.waitFor(4L);
	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
	          UIFoundation.waitFor(4L);
	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightPM));
	          UIFoundation.waitFor(4L);
	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
	          UIFoundation.waitFor(6L);
	          
	          if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
	  		{
	  			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
	  			UIFoundation.waitFor(3L);
	  		}
	  		
	          
	  		if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
	          {
	   			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
	          }
	            if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
	            {
	            	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);	               
	              WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
	  			if(!ele.isSelected())
	  			{
	  			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);	
	  		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
	  		    UIFoundation.waitFor(3L);
	  		    UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
	  	          UIFoundation.waitFor(1L);
	  	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
	  	          UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
	  			}
	  			
	  			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
	  			{
	  				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
	  				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
	  				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
	  			}
	  			
	  			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
	  			{	
	  			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
	  			UIFoundation.waitFor(1L);
	  			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
	  			UIFoundation.waitFor(14L);
	  			}
	                   
	            }
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(20L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: " + orderNum);
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Order number is null.Order not placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			
			}
			
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(20L);
			OrderHistotalShipping = UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			System.out.println("Order History Total:                 " + OrderHistotalShipping);
			if(OrderHistotalShipping.equalsIgnoreCase(shippingAndHandling)) {
				objStatus += true;
				System.out.println("Order History Shipping Amount Matches the Final Review Page Shipping Amount");
				String objDetail = "Order History Shipping Amount Matches the Final Review Page Shipping Amount";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			
				}else {
					objStatus += false;
					String objDetail = "Order History Shipping Amount not Matches the Final Review Page Shipping Amount";
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
					UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
				    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				
				}
			
			log.info("The execution of the method OrderHistoryShippingAmountValidation ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("OrderHistoryShippingAmountValidation test case is failed");
				return "Fail";
			} else {
				System.out.println("OrderHistoryShippingAmountValidation test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method OrderHistoryShippingAmountValidation " + e);
			objStatus += false;
			String objDetail = "Order History Shipping Amount Matches the Final Review Page Shipping Amount";
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
			return "Fail";
		}
	}
}
