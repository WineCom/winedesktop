package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyAddAntherTextAndCorporateGiftsLink extends Desktop {

	/***************************************************************************
	 * Method Name : addaAnotherTextInVariousPages() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : TM-4133
	 ****************************************************************************
	 */
	public static String addAnotherTextInVariousPages() {

		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String ScreenshotName = "addaAnotherTextInVariousPages.jpeg";
		
		try {
			log.info("Add Another Text In Various Pages method started here......");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkprodInHomePage));
			UIFoundation.waitFor(2L);
			String prodText = UIFoundation.getText(ListPage.lnkprodInHomePage);
			if (prodText.contains("Add Another")) {
				objStatus += true;
				String objDetail = "Add Another text is verified in Home Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Add Another text is  not displayed for Home page Products";
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			UIFoundation.waitFor(1L);
		    driver.navigate().to("https://qwww.wine.com/search/bonarda/0");
		    UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(2L);
			prodText = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (prodText.contains("Add Another")) {
				objStatus += true;
				String objDetail = "Add Another text is verified in List Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Add Another text is  not displayed for List Page";
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkmerlotFirstProd));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(2L);
			prodText = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (prodText.contains("Add Another")) {
				objStatus += true;
				String objDetail = "Add Another text is verified in PIP Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Add Another text is  not displayed for PIP Page";
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(2L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			else {
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			prodText = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (prodText.contains("Add Another")) {
				objStatus += true;
				String objDetail = "Add Another text is verified in MyWine Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Add Another text is  not displayed for MyWine Page";
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			log.info("Add Another Text In Various Pages method started here......");
			if (objStatus.contains("false")) {
				System.err.println("Add Another Text In Various Pages test case is failed");
				return "Fail";
			} else {
				System.out.println("Add Another Text In Various Pages test case is Passed");
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println("Add Another Text In Various Pages test case is failed");
			log.error("there is an exception arised during the execution of the method" + e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : VerifyCorperateLinksInCustomerCareSection() Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose : TM-4127
	 ****************************************************************************
	 */
	public static String verifyCorperateLinksInCustomerCareSection() {
		String objStatus = null;
		String ScreenshotName = "VerifyCorperateLinksInCustomerCareSection.jpeg";
		
		try {
			log.info("Verify Corperate Link In Customer Section methos started here....");

			driver.navigate().to("https://qwww.wine.com/");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkcustomerCareLink);
			if (UIFoundation.isElementDisplayed(ListPage.lnkCorperateGifts)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCorperateGifts));
				UIFoundation.waitFor(2L);
				String corperateGift = driver.getCurrentUrl();
				// String actcorperateGift =objExpectedRes.getProperty("corperateGiftUrl");
				if (corperateGift.contains("https://qwww.wine.com/content/landing/corporate-gifting")) {
					objStatus += true;
					String objDetail = "Corperate Gifts link is displayed in customer care footer";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
				} else {
					objStatus += false;
					String objDetail1 = "Corperate Gifts link is displayed in customer care footer";
					UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail1);
					logger.fail(objDetail1, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
				}
			}
			log.info("Verify Corperate Link In Customer Section methos started here....");
			if (objStatus.contains("False")) {
				System.out.println("Verify Corperate Links In and Customer CareSection test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Corperate Links In and Customer Care Section test case is Pass");
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println("there is an exception arised during the execution of the method" + e);
			return "fail";
		}
	}
}