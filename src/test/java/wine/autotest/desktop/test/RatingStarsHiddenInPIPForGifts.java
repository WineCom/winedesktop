package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;


public class RatingStarsHiddenInPIPForGifts extends Desktop {
	
	/***************************************************************************
	 * Method Name			: VerifyPromoBarInWashington()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * TM-3287
	 ****************************************************************************
	 */
	
	public static String VerifyRatingStarsHiddenInPIPForGifts()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__RatingAndShare.jpeg";
			
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkGiftsBottles));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkgiftProdutcAddToPIP));
			UIFoundation.waitFor(2L);
			if(!UIFoundation.isDisplayed(ListPage.imgstarsRating) && !UIFoundation.isDisplayed(ListPage.imgshare) ){
				objStatus+=true;
				logger.pass("Verified the 'share' & 'rating stars' are hidden in PIP of Gift products");
				ReportUtil.addTestStepsDetails("Verified the 'share' & 'rating stars' are hidden in PIP of Gift products", "Pass", "");
				
			}else{
				objStatus+=false;
				String objDetail="Verify the 'share' & 'rating stars' are hidden in PIP of Gift products is failed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+"washington", objDetail);
			}
/*			objStatus += String.valueOf(!UIFoundation.isDisplayed(driver, "starsRating"));
			objStatus += String.valueOf(!UIFoundation.isDisplayed(driver, "share"));*/
		//	objStatus+= String.valueOf(ApplicationDependent.isObjectExistForList(driver));
			if (objStatus.contains("false"))
			{
				
				System.out.println("Verify the 'share' & 'rating stars' are hidden in PIP of Gift products test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'share' & 'rating stars' are hidden in PIP of Gift products test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method VerifyRatingStarsHiddenInPIPForGifts "+e);
			return "Fail";
		}
	}

}
