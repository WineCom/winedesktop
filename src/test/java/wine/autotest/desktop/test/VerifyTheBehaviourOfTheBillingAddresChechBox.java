package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyTheBehaviourOfTheBillingAddresChechBox extends Desktop {
	

	static boolean isObjectPresent=true;
	

	/***************************************************************************
	 * Method Name : verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : The purpose of this method is to validate the error message displayed
	 * TM-625,
	 ****************************************************************************
	 */
	
	public static String verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected() {


		String objStatus = null;
		String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected started here ...");
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnRecipientContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 10);				
			}			
			if(UIFoundation.isDisplayed(FinalReviewPage.spnmustSelectShippingAddress))
			 {
			      objStatus+=true;
			      String objDetail="Must select address before proceeding displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			      logger.pass(objDetail);
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Must select address before proceeding not displayed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			 }
			
			log.info("The execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected ended here ...");

			if (objStatus.contains("false")) {				
				
				 String objDetail="behaviour of the Continue button, if address is not selected in Recipient page is failed";
			      ReportUtil.addTestStepsDetails(objDetail, "fail", "");
			      logger.fail(objDetail);

				return "Fail";
			} else {
				 String objDetail="behaviour of the Continue button, if address is not selected in Recipient page excecuted succesfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			      logger.pass(objDetail);
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected " + e);
			return "Fail";
		}
	}

	
	
	public static String checkBoxSelectedDefault() {
		String objStatus=null;
		String screenshotName = "Scenarios_checkBoxSelectedDefault_Screenshot.jpeg";

			try {
				log.info("The execution of the method checkBoxSelectedDefault started here ...");
				UIFoundation.waitFor(6L);
				
				if(UIFoundation.isDisplayed(CartPage.btnCheckout))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				}
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
	          
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkchangePayment);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddPymtMethod));
					UIFoundation.waitFor(1L);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				}
				if(UIFoundation.isSelected(FinalReviewPage.chkBillingAndShipping))	
				{					
				
					objStatus+=true;
					String objDetail="Billing and shipping CheckBox is selected by default";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
					UIFoundation.waitFor(1L);
				}
				else
				{
					objStatus+=false;
					String objDetail="Billing and shipping CheckBox is not selected by default";
				    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
				
				
				log.info("The execution of the method checkBoxSelectedDefault ended here ...");
				if ( objStatus.contains("false")) {
					objStatus+=false;
					String objDetail="Billing and shipping CheckBox is selected by default is excecuted succesfully";
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
					logger.fail(objDetail);
					System.out.println(" Order creation with existing account test case is failed");
					return "Fail";
				} else {
					
					objStatus+=true;
					String objDetail="Billing and shipping CheckBox is not selected by default and test excecution failed";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method checkBoxSelectedDefault "
						+ e);
				return "Fail";
			}
	
	}
	
	public static String unCheckBoxSelected() {
		String objStatus=null;
		String objDetail=null;
		String screenshotName = "Scenarios_unCheckBoxSelected.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
			try {
				log.info("The execution of the method unCheckBoxSelected started here ...");
				UIFoundation.waitFor(6L);
				
				if(UIFoundation.isDisplayed(CartPage.btnCheckout))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				}
				UIFoundation.waitFor(5L);
	
				objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
	           				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkchangePayment);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddPymtMethod));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
				}
			//	if(UIFoundation.isSelected(FinalReviewPage.chkBillingAndShipping))	
					if(!UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingAddress))
						
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingAddress));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingSuite));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingCity));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.dwnBillinState));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingZip));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingPhone));
					UIFoundation.waitFor(1L);
					objStatus+=true;
					objDetail="Billing address form is displayed succesfully,on unchecking the 'Billing same as Shipping' checkbox in the payment section";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
				      }
					 else
					 {
					     objStatus+=false;
					     objDetail="Billing address form is not displayed,on unchecking the 'Billing same as Shipping' checkbox in the payment section";
					     UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
						 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					 }
				log.info("The execution of the method unCheckBoxSelected ended here ...");
				if ( objStatus.contains("false")) {				
					
					return "Fail";
				} else {
					
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method unCheckBoxSelected "
						+ e);
				return "Fail";
			}
	
	}
}
	
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 *//*
	public static String addNewCreditCard(WebDriver driver) {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
		
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(5L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(1L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(1L);
			}
			
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthMonth","birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthDate","birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "birthYear", "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
			UIFoundation.webDriverWaitForElement(driver, "PaymentContinue", "Invisible", "", 50);
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "LearnMore"));
			if (UIFoundation.isDisplayed(driver, "AboutStewardship")) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the header  in the 'About Stewardship' popup in Final Review section when the user has not enrolled stewardship.", "Pass", "");
				
				
			} else {
				objStatus+=false;
			//	System.out.println("Verify the header  in the 'About Stewardship' popup in Final Review section when the user has not enrolled stewardship.");
				String objDetail="Verify the header  in the 'About Stewardship' popup in Final Review section when the user has not enrolled stewardship test case is failed";
				UIFoundation.captureScreenShot(driver, screenshotpath+"StewardshipFnl", objDetail);
				
			}
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "stewardshipClose"));
			
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(driver, "Subtotal");
			shippingAndHandling=UIFoundation.getText(driver, "obj_Shipping&Hnadling");
			total=UIFoundation.getText(driver, "TotalBeforeTax");
			salesTax=UIFoundation.getText(driver, "OrderSummaryTaxTotal");
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentSaveButton"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "PlaceOrderButton"));
			UIFoundation.webDriverWaitForElement(driver, "PlaceOrderButton", "Invisible", "", 60);
			String orderNum=UIFoundation.getText(driver,"OrderNum");
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			 }
			if(UIFoundation.isDisplayed(driver, "TickSymbol") && UIFoundation.isDisplayed(driver, "OrderConfirmation"))
			{
				  objStatus+=true;
			      String objDetail="Verified  the contents in the 'Thank you' page ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified  the contents in the 'Thank you' page ");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the contents in the 'Thank you' page  is failed";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				   System.err.println("Verify the contents in the 'Thank you' page ");
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println(" Order creation with new account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with new account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}*/


