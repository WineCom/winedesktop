package wine.autotest.desktop.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VintageRefinementsAreOrderedInChronologicalOrder extends Desktop {
	
	/***************************************************************************
	 * Method Name			: vintageRefinementProduct()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String vintageRefinementProduct() {
		String objStatus = null;
		 List<Double> arrayList = new ArrayList<Double>();;
		boolean isSorted=true;
		Double product1=0.0;
		Double product2=0.0;
		Double product3=0.0;
		Double product4=0.0;
		Double product5=0.0;
		String screenshotName = "Scenarios_VintageHtoL_Screenshot.jpeg";
		
		try {
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
		//	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet"));
		//	UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkcabernetMore));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkmerlot);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkmerlot));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkselectSortOption));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkvinatageNtoO));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(ListPage.lnkmerlotFirstProd))
			{
				String products1=UIFoundation.getText(ListPage.lnkmerlotFirstProd);
				products1=products1.replaceAll("[^0-9]","");
				product1=Double.parseDouble(products1);
				arrayList.add(product1);
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkmerlotSecProd))
			{
				String products2=UIFoundation.getText(ListPage.lnkmerlotSecProd);
				products2=products2.replaceAll("[^0-9]","");
				product2=Double.parseDouble(products2);
				arrayList.add(product2);
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkmerlotThirdProd))
			{
				String products3=UIFoundation.getText(ListPage.lnkmerlotThirdProd);
				products3=products3.replaceAll("[^0-9]","");
				product3=Double.parseDouble(products3);
				arrayList.add(product3);
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkmerlotFourthProd))
			{
				String products4=UIFoundation.getText(ListPage.lnkmerlotFourthProd);
				products4=products4.replaceAll("[^0-9]","");
				product4=Double.parseDouble(products4);
				arrayList.add(product4);
				
			}
			if(UIFoundation.isDisplayed(ListPage.lnkmerlotFifthProd))
			{
				String products5=UIFoundation.getText(ListPage.lnkmerlotFifthProd);
				products5=products5.replaceAll("[^0-9]","");
				product5=Double.parseDouble(products5);
				arrayList.add(product5);
				
			}	
			UIFoundation.waitFor(3L);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)<(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			    }
			    if(isSorted)
			    {
			    	 objStatus+=true;
				      String objDetail="all the vintages are arranged in the DSC order";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	System.out.println("all the vintages are arranged in the DSC order");
			    }
			    else
			    {
			    	objStatus+=false;
				       String objDetail="all the vintages are not arranged in the DSC order";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			    	System.err.println("all the vintages are not arranged in the DSC order");
			    }
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
