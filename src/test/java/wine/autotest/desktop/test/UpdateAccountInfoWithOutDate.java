package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class UpdateAccountInfoWithOutDate extends Desktop {
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clckObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "usAccWithoutinfo"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(6L);
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
			//	System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
			//	System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: updateAccountInfoWithOutDate()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String updateAccountInfoWithOutDate()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_Stewardship.jpeg";
			
		try
		{
			log.info("The execution of the method updateAccountInfoWithOutDate started here ...");

			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAccountInfo));
			UIFoundation.waitFor(3L);
//			UIFoundation.clearField(UserProfilePage.txtclearUpdateAccountLastName);
//			UIFoundation.waitFor(1L);
//			UIFoundation.clckObject(UserProfilePage.txtupdateAccountemail);
//			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtenterUpdateAccountLastName, "lastName"));
//			UIFoundation.waitFor(6L);			
			UIFoundation.clearField(UserProfilePage.txtBrthMonth);
			UIFoundation.clearField(UserProfilePage.txtBrthDay);
			UIFoundation.clearField(UserProfilePage.txtBrthYear);
			if(UIFoundation.isEnabled(UserProfilePage.btnSaveAccountInfo))
			{
				objStatus+=true;
				String objDetail="User is not able to update Account info with out Date ";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Verify user is not able to update Account info with out Date test case is failed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}

			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify user is not able to update Account info with out Date test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify user is not able to update Account info with out Date test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method updateAccountInfoWithOutDate "+ e);
			return "Fail";
			
		}
	}	

}
