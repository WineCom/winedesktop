package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyErrorMsgForAddingNewCreditCard extends Desktop {
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 * TM-2190,2192,686
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
			
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(3L);
			}
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.spnnameRequiredErrorMsg) && UIFoundation.isDisplayed(FinalReviewPage.spncreditCardRequiredErrorMsg) && UIFoundation.isDisplayed(FinalReviewPage.spnexpirtDateRequiredErrorMsg) && UIFoundation.isDisplayed(FinalReviewPage.spncvvRequiredErrorMsg))
			{
				  objStatus+=true;
			      String objDetail="Verified appropriate error messages are displayed in Payment section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified appropriate error messages are displayed in Payment section");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify appropriate error messages are displayed in Payment section  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"PaymentSection", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify Verified appropriate error messages are displayed in Payment section is failed ");
			}
			
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "inValidCreditCardNum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.spnpleaseSpeciftCardProvider))
			{
				  objStatus+=true;
			      String objDetail="Verified error message is displayed when invalid credit card values are entered.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified  error message is displayed when invalid credit card values are entered.");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify error message is displayed when invalid credit card values are entered is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"inValidCredit", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify error message is displayed when invalid credit card values are entered is failed ");
			}
			UIFoundation.clearField(FinalReviewPage.txtCardNumber);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			//WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.spnstreetAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.spncityRequired) && UIFoundation.isDisplayed(FinalReviewPage.spnstateAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.spnZipRequired))
			{
				  objStatus+=true;
			      String objDetail="Verified appropriate error messages are displayed in Billing address fields in Payment section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified appropriate error messages are displayed in Billing address fields in Payment section");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify appropriate error messages are displayed in Billing address fields in Payment section is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"BillingAddress", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify Verified appropriate error messages are displayed in Billing address fields in Payment section is failed ");
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				//System.out.println(" Order creation with new account test case is failed");
				return "Fail";
			} else {
				//System.out.println("Order creation with new account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
}
