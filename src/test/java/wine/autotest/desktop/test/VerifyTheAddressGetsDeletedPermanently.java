package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyTheAddressGetsDeletedPermanently extends Desktop {
	

	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-2197,
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		   String screenshotName = "Scenarios_DeleteAddress.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(13L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.imgWineLogo));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkuserDataCardEdit));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkremoveThisAddress));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.lnkcontactUs);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnYesRemove));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnyouDoNotHaveAnyAddress)){
				  objStatus+=true;
			      String objDetail="Address is deleted permanently from the user profile.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				 objStatus+=false;
				   String objDetail="Address is not deleted permanently from the user profile.";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
