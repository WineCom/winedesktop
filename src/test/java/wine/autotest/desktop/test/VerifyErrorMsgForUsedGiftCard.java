package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

/*import com.gargoylesoftware.htmlunit.html.applets.AppletClassLoader;*/

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class VerifyErrorMsgForUsedGiftCard extends Desktop {
	
	/***************************************************************************
	 * Method Name			: usedGiftCardValidation()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String usedGiftCardValidation()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedErrorMsg=null;
		String actualErrorMsg=null;
		String ScreenshotName = "usedGiftCardValidation.jpeg";
		
		try
		{
			log.info("The execution of the method usedGiftCardValidation started here ...");
			System.out.println("============Order summary ===============");
			expectedErrorMsg=verifyexpectedresult.inValidGiftCardmsg;
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
//                UIFoundation.webDriverWaitForElement(CartPage.lnkGiftApply, "Clickable", "", 50);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtGiftNumber));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
//				UIFoundation.webDriverWaitForElement(CartPage.lnkGiftApply, "Clickable", "", 50);
			}

			actualErrorMsg=UIFoundation.getText(CartPage.spnGiftErrorCode);
			UIFoundation.waitFor(5L);
			if(actualErrorMsg.equalsIgnoreCase(expectedErrorMsg))
			{
				objStatus+=true;
				String objDetail = "Verified error message for used GiftCard";
				System.out.println(expectedErrorMsg);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			
			else
			{
				objStatus+=false;
				String objDetail = "Error message does not match";
				System.err.println(actualErrorMsg);
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
			}
			log.info("The execution of the method usedGiftCardValidation ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify Error Message for used GiftCard test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify Error Message for used GiftCard test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method usedGiftCardValidation "+ e);
			return "Fail";
			
		}
	}


}
