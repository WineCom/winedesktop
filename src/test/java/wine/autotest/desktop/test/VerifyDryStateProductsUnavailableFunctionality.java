package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;


//import com.gargoylesoftware.htmlunit.html.applets.AppletClassLoader;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class VerifyDryStateProductsUnavailableFunctionality extends Desktop {
	

	/***************************************************************************
	 * Method Name : orderSummaryForNewUser() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyproductUnavailabileFunctionality() {
/*		String products1 = null;
		String products2 = null;
		String products3 = null;
		String products4 = null;
		String products5 = null;*/
		String objStatus = null;
		   String screenshotName = "Scenarios_currentlyUnavailable_Screenshot.jpeg";
			
		try {
			
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "dryState");
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkvarietal));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spncurrentlyUnavailableProd)){
				
				  objStatus+=true;
			      String objDetail="currently unavailable text is displayed for the products in List Page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="currently unavailable text is not displayed for the products in List Page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spncurrentlyUnavailableProd)){
				
				  objStatus+=true;
			      String objDetail="currently unavailable text is displayed for the products in PIP Page";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="currently unavailable text is not displayed for the products in PIP Page";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
		
			if (objStatus.contains("false")) {
				System.out.println("Check 'Currently Unavailable' text is displayed for products under dry states in the PIP/List page test case is failed.");
				return "Fail";
			} else {
				System.out.println("Check 'Currently Unavailable' text is displayed for products under dry states in the PIP/List page test case executed successfully.");
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}

}
