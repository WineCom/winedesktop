package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.OrderDetailsPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;



public class SearchProductByCharacter extends Desktop {
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		   String screenshotName = "Scenarios_VerifyGiftWrapping_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "cay"));
			UIFoundation.waitFor(6L);
			if(!UIFoundation.getText(ListPage.lnkFirstProductInList).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductInList);
				if(products1.toLowerCase().contains("cay"))
				{
					  objStatus+=true;
				      String objDetail="First product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("First product contains" + "'Cay'"+ "character , Product name is:"+products1);
				}
				else
				{
					objStatus+=false;
					String objDetail="First product not contains" + "'Cay'"+ "character";
				    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				    logger.fail(objDetail);
					System.out.println("First product not contains" + "'Cay'"+ "character , Product name is:"+products1);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.lnkSecondProductInList).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductInList);
				if(products2.toLowerCase().contains("cay"))
				{
					  objStatus+=true;
				      String objDetail="Second product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("Second product contains" + "'Cay'"+ "character , Product name is:"+products2);
				}
				else
				{
					objStatus+=false;
					String objDetail="Second product not contains" + "'Cay'"+ "character";
				    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				    logger.fail(objDetail);
					System.out.println("Second product not contains" + "'Cay'"+ "character , Product name is:"+products2);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.lnkThirdProductInList).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductInList);
				if(products3.toLowerCase().contains("cay"))
				{
					 objStatus+=true;
				      String objDetail="Third product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("Third product contains" + "'Cay'"+ "character , Product name is:"+products3);
				}
				else
				{
					objStatus+=false;
					String objDetail="Third product not contains" + "'Cay'"+ "character";
				    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				    logger.fail(objDetail);
					System.out.println("Third product not contains" + "'Cay'"+ "character , Product name is:"+products3);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.lnkFourthProductInList).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductInList);
				if(products4.toLowerCase().contains("cay"))
				{
					objStatus+=true;
				      String objDetail="Fourth product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("Fourth product contains" + "'Cay'"+ "character , Product name is:"+products4);
				}
				else
				{
					objStatus+=false;
					String objDetail="Fourth product not contains" + "'Cay'"+ "character";
				    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				    logger.fail(objDetail);
					System.out.println("Fourth product contains" + "'Cay'"+ "character , Product name is:"+products4);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.lnkFifthProductInList).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductInList);
				if(products5.toLowerCase().contains("cay"))
				{
					objStatus+=true;
				      String objDetail="Fifth product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("Fifth product contains" + "'Cay'"+ "character , Product name is:"+products5);
				}
				else
				{
					objStatus+=false;
					String objDetail="Fifth product not contains" + "'Cay'"+ "character";
				    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				    logger.fail(objDetail);
					System.out.println("Fifth product contains" + "'Cay'"+ "character , Product name is:"+products5);
				}
			
				
			}
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}


}
