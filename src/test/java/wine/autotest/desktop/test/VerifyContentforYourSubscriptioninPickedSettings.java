package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.PickedPage;

public class VerifyContentforYourSubscriptioninPickedSettings extends Desktop{

	public static String strRedBottleCount ;
	public static String strWhiteBottleCount ;
	public static String strTargetPrice ;
	public static String strAdvenTypeVery ;
	public static String strFrequency3Month ;
	
	/***********************************************************************************************************
	 * Method Name    : RedWhiteWineSubscriptionflow() 
	 * Created By     : Ramesh S
	 * Reviewed By    : Chandrashekhar
	 * Purpose        : The purpose of this method is to do RedWhite wine subscription flow' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String RedWhiteWineSubscriptionflow() {
		String objStatus = null;
		String screenshotName = "Scenarios_RedWhiteWineSubscriptionflow_Screenshot.jpeg";
		try {
			log.info("Execution of the method RedWhiteWineSubscriptionflow started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkRedandWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWhitewinePagenNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));
			strAdvenTypeVery = UIFoundation.getText(PickedPage.spnAdventurTypeVery);
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) 
			{
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				logger.pass(objDetail);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoFrequency3Months));
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			strFrequency3Month = UIFoundation.getText(PickedPage.rdoFrequency3Months);
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAddRedBottleCount));
			UIFoundation.waitFor(1L);
			strRedBottleCount = UIFoundation.getText(PickedPage.spnRedCount);
			if(strRedBottleCount!=null) 
			{
			ReportUtil.addTestStepsDetails("Increased Red Bottle Count"+strRedBottleCount, "Pass", "");
			}
			else 
			{
				ReportUtil.addTestStepsDetails("Red Bottle Count Not Increased"+strRedBottleCount, "Fail", "");
			}
			//White Wine calculation
			UIFoundation.waitFor(1L);
			strWhiteBottleCount = UIFoundation.getText(PickedPage.spnWhiteCount);
			if(strWhiteBottleCount!=null) 
			{
			ReportUtil.addTestStepsDetails("Decreased  White Bottle Count "+strWhiteBottleCount, "Pass", "");
			}
			else 
			{
				ReportUtil.addTestStepsDetails("White Bottle Count Not Decreased "+strWhiteBottleCount, "Fail", "");
			}	
			strTargetPrice = UIFoundation.getText(PickedPage.spnTargetPrice);
			System.out.println("strRedBottleCount :"+strRedBottleCount);
			System.out.println("strWhiteBottleCount :"+strWhiteBottleCount);
			System.out.println("strTargetPrice :"+strTargetPrice);
			System.out.println("strAdvenTypeVery :"+strAdvenTypeVery);
			System.out.println("strFrequency3Month :"+strFrequency3Month);
			if(strTargetPrice!=null) {
				ReportUtil.addTestStepsDetails("Target Price Exist in Quantity Selection page "+strTargetPrice, "Pass", "");
				}
			else 
			{
					ReportUtil.addTestStepsDetails("Target Price Not-Exist in Quantity Selection page "+strTargetPrice, "Fail", "");
				}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedWhiteQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				String objDetail="Subscription flow for red/White wine test case failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Subscription flow for red/White wine test case executed successfully";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	
	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyLocalPickUpForPickedUpWine() {
			
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.txtRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkAcceptTermsandConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
//				UIFoundation.webDriverWaitForElement(OMSPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}
				else
				{
					objStatus+=false;
					String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					UIFoundation.captureScreenShot(screenshotpath, objDetail);
				}
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
	
	/***********************************************************************************************************
	 * Method Name : ValidateContentforYourSubscriptioninPickedSettings() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the Your Subscription Content in picked settings' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String ValidateContentforYourSubscriptioninPickedSettings() {
		String objStatus = null;
		String screenshotName = "Scenarios_ValidateContentforYourSubscriptioninPickedSettings_Screenshot.jpeg";
		try {
			log.info("Execution of the method ValidateContentforYourSubscriptioninPickedSettings started here .........");
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.imgPickedLogo));
			UIFoundation.waitFor(10L);
			String strYourSubTitle = UIFoundation.getText(PickedPage.spnPickedSetYourSubscription);
			ReportUtil.addTestStepsDetails("Your Subscription Title : "+strYourSubTitle, "Pass", "");	
			String strTargetPrice = UIFoundation.getText(PickedPage.spnpickedsetTargetPrice);
			ReportUtil.addTestStepsDetails("Target Price : "+strTargetPrice, "Pass", "");
			String strMinPrice = UIFoundation.getText(PickedPage.lnkpickedsetMinPrice);
			ReportUtil.addTestStepsDetails("Minimum Price : "+strMinPrice, "Pass", "");
			String strMaxPrice = UIFoundation.getText(PickedPage.lnkpickedsetMaxPrice);
			ReportUtil.addTestStepsDetails("Maximum Price : "+strMaxPrice, "Pass", "");
			String strRedBottleQty = UIFoundation.getText(PickedPage.spnpickedsetRedQty);
			ReportUtil.addTestStepsDetails("Red Bottle Quantity : "+strRedBottleQty, "Pass", "");
			String strRedBottlePrice = UIFoundation.getText(PickedPage.spnpickedsetRedPrice);
			ReportUtil.addTestStepsDetails("Red Bottle Price : "+strRedBottlePrice, "Pass", "");
			String strWhiteBottleQty = UIFoundation.getText(PickedPage.spnpickedsetWhiteQty);
			ReportUtil.addTestStepsDetails("White Bottle Quantity : "+strWhiteBottleQty, "Pass", "");
			String strWhiteBottlePrice = UIFoundation.getText(PickedPage.spnpickedsetWhitePrice);
			ReportUtil.addTestStepsDetails("White Bottle Price  : "+strWhiteBottlePrice, "Pass", "");
			String strFreqDesc = UIFoundation.getText(PickedPage.spnpickedsetFreqDesc);
			ReportUtil.addTestStepsDetails("Frequency Description : "+strFreqDesc, "Pass", "");
			String strAdvenLevelDesc = UIFoundation.getText(PickedPage.spnpickedsetDiscDesc);
			ReportUtil.addTestStepsDetails("Discovery Level : "+strAdvenLevelDesc, "Pass", "");
			String strFreeShipDesc = UIFoundation.getText(PickedPage.spnpickedsetFreeShip);
			ReportUtil.addTestStepsDetails("Free Shipping : "+strFreeShipDesc, "Pass", "");
			String strSatisfactGuatantDesc = UIFoundation.getText(PickedPage.spnpickedsetSatisfactGuarant);
			ReportUtil.addTestStepsDetails("Satisfaction guaranteed : "+strSatisfactGuatantDesc, "Pass", "");
			if(strYourSubTitle!=null){
				objStatus+=true;
				String objDetail="Your Subscription Content Found : "+strYourSubTitle;
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Your Subscription Content Not Found : "+strYourSubTitle;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			System.out.println("strRedBottleQty : "+strRedBottleQty);
			System.out.println("strWhiteBottleQty : "+strWhiteBottleQty);
			System.out.println("strTargetPrice : "+strTargetPrice);
			System.out.println("strFreqDesc : "+strFreqDesc);
			System.out.println("strDiscLevelDesc : "+strAdvenLevelDesc);		
			if(strTargetPrice.contains(strTargetPrice)){
				objStatus+=true;
				String objDetail="The Picked Setting Target Price matched the selected Price : "+strTargetPrice;
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="The Picked Setting Target Price Not matched the selected Price : "+strTargetPrice;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(strRedBottleQty.contains(strRedBottleCount)){
				objStatus+=true;
				String objDetail="The Picked Setting Red Bottle count matched the selected Count : "+strRedBottleQty;
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="The Picked Setting Red Bottle count Not matched the selected Count : "+strRedBottleQty;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(strWhiteBottleQty.contains(strWhiteBottleCount)){
				objStatus+=true;
				String objDetail="The Picked Setting White Bottle count matched the selected Count : "+strWhiteBottleQty;
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="The Picked Setting White Bottle count Not matched the selected Count : "+strWhiteBottleQty;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(strFreqDesc.contains(strFrequency3Month)){
				objStatus+=true;
				String objDetail="The Picked Setting Frequemcy matched the selected Frequency : "+strFreqDesc;
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="The Picked Setting Frequemcy matched the selected Frequency : "+strFreqDesc;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(strAdvenLevelDesc.contains(strAdvenTypeVery)){
				objStatus+=true;
				String objDetail="The Picked Setting Adventurous Level matched the selected Adventure Level : "+strAdvenLevelDesc;
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="The Picked Setting Frequemcy matched the selected Frequency : "+strAdvenLevelDesc;
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verification of content for Your Subscription in Picked Settings page is not completed successfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verification of content for Your Subscription in Picked Settings page is completed successfully";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
