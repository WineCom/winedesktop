package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.UIFoundation;

public class RedWhiteButtonPresentinPriceQuantityWidget extends Desktop {
	
	/***********************************************************************************************************
	 * Method Name : FunctionalityRedorWhiteButtoninPriceQtyPage() 
	 * Created By  : Ramesh S
	 * Reviewed By : Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the functionality of White Wine'/'Red Wine' button present in Price & Quantity widget page' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String FunctionalityRedorWhiteButtoninPriceQtyPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_FunctionalityRedorWhiteButtoninPriceQtyPage_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkRedOnly));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);		
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNextThirdPage));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lblNotVery));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNextThirdPage));
			UIFoundation.waitFor(4L);
			String strLiketoMixQuest = verifyexpectedresult.LiketoMixQuest;
			String strexistLiketoMixQuest = UIFoundation.getText(PickedPage.spnLiketoMixQuest);
			System.out.println("strLiketoMixQuest : "+strLiketoMixQuest);
			System.out.println("strexistLiketoMixQuest : "+strexistLiketoMixQuest);
			if(strexistLiketoMixQuest.contains(strLiketoMixQuest)) 
			{
				String objDetail="Would you like to add to the mix? - Question displayed";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}
			else 
			{
				String objDetail="Would you like to add to the mix? - Question Not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				objStatus+=false;
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMixRedWhite));
			UIFoundation.waitFor(4L);
			String strpopupLiketoMixWhite = verifyexpectedresult.HeaderMixWhiteWine;
			String strPopupexistLiketoMixWhite = UIFoundation.getText(PickedPage.spnHeaderRedWhitePopup);
			System.out.println("strpopupLiketoMixWhite : "+strpopupLiketoMixWhite);
			System.out.println("strPopupexistLiketoMixWhite : "+strPopupexistLiketoMixWhite);
			if(strPopupexistLiketoMixWhite.contains(strpopupLiketoMixWhite)) 
			{
				String objDetail="How do you feel about these white wines? - Model window displayed";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}
			else 
			{
				String objDetail="How do you feel about these white wines? - Model window Not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				objStatus+=false;
			}
			if(UIFoundation.isDisplayed(PickedPage.btnCancelMixPopup) && UIFoundation.isDisplayed(PickedPage.btnConfirmMixPopup)) 
			{
				String objDetail="Varietal Preference model displayed with Confirme button and Cancel Link";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}
			else 
			{
				String objDetail="Varietal Preference model Not displayed with Confirme button and Cancel Link";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				objStatus+=false;
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMixQuestChardonnay));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnConfirmMixPopup));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.spnWhiteCount)) 
			{
				String objDetail="Varietal selected in model PopUP is displayed in  Quantity section";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}
			else 
			{
				String objDetail="Varietal selected in model PopUp is not displayed in Quantity section";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				objStatus+=false;
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verify  Functionality of White Wine'/'Red Wine' button present in Price & Quantity widget page test case failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verify  Functionality of White Wine'/'Red Wine' button present in Price & Quantity widget page test case executed successfully";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
