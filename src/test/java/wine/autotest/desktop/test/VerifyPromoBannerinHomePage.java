package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyPromoBannerinHomePage extends Desktop {
	
	/***************************************************************************
	 * Method Name			: VerifyPromoBannerinHomePage()
	 * Created By			: Ramesh S 
	 * Reviewed By			: 
	 * Purpose				: TM-405
	 ****************************************************************************
	 */

public static String VerifypromoBanner() {
		
		String objStatus = null;
		String screenshotName = "promoBannerHomePage.jpeg";
			
			try {
				log.info("Promo Banner In Home Page method started here......");
				
				UIFoundation.waitFor(3L);
				UIFoundation.SelectObject(LoginPage.dwnSelectState, "State");
				UIFoundation.waitFor(5L);
				if(UIFoundation.isDisplayed(LoginPage.lnkpromoBarDisp)){
				System.out.println("Promo Banner is displayed in Home  page ");
				objStatus += true;
				String objDetail = "Promo Banner is displayed in Home  page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Promo Banner is not displayed in Home  page";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
						
			if (objStatus.contains("false")) {

					System.out.println("Promo Banner In Home Page test case is failed");

					return "Fail";
				} else {
					System.out.println("Promo Banner In Home Page test case executed succesfully");

					return "Pass";
				}
			} catch (Exception e) {
				return "Fail";

			}
		}
}
