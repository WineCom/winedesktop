package wine.autotest.desktop.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.test.Desktop;


public class OrderCreationWithFedexAddress extends Desktop {
	
static boolean isObjectPresent=true;



/***************************************************************************
 * Method Name			: shippingDetails()
 * Created By			: Vishwanath Chavan
 * Reviewed By			: Ramesh,KB
 * Purpose				:  The purpose of this method is to fill the shipping 
 * 						  address of the customer
 ****************************************************************************
 */

public static String shippingDetails() {
	String objStatus=null;
	   String screenshotName = "Scenarios_fedexPickUpLocation_Screenshot.jpeg";
		
	try {
		log.info("The execution of the method shippingDetails started here ...");
		//
		objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
		UIFoundation.waitFor(3L);
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
		UIFoundation.waitFor(10L);
		//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "VerifyFedexAddress"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btngoBack));
		UIFoundation.waitFor(1L);
		if(UIFoundation.isDisplayed(FinalReviewPage.VerifyFindPickupLoactionear))
		{
			objStatus+=true;
		      String objDetail="User is navigated back to the list of local pickup locations on clicking 'go back' button";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      logger.pass(objDetail);
			  System.out.println("User is navigated back to the list of local pickup locations on clicking 'go back' button");
		}else{
			objStatus+=false;
			   String objDetail="User is not able navigat back to the list of local pickup locations on clicking 'go back' button";
		       UIFoundation.captureScreenShot(screenshotpath+"PaymentSection", objDetail);
		       logger.fail(objDetail);
			   System.err.println("User is not able navigat back to the list of local pickup locations on clicking 'go back' button");
		}
		UIFoundation.waitFor(1L);
		//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "VerifyFedexAddress"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
		UIFoundation.waitFor(15L);

		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexContinue));
//		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnFedexContinue, "Invisible", "", 50);
		if(!UIFoundation.isDisplayed(FinalReviewPage.spnlocalPickUp)){
			objStatus+=true;
			logger.pass("Local pickup icon is not displayed next to pickup location address.");
			ReportUtil.addTestStepsDetails("Local pickup icon is not displayed next to pickup location address.", "Pass", "");
		}else{
			objStatus+=false;
			//	System.out.println("Verify the header  in the 'About Stewardship' popup in Final Review section when the user has not enrolled stewardship.");
				String objDetail="Local pickup icon is displayed next to pickup location address.";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+"localPickUp", objDetail);
		}
/*		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
		UIFoundation.waitFor(5L);*/
		log.info("The execution of the method shippingDetails ended here ...");

		if (objStatus.contains("false")) {
			
			return "Fail";
		} else {
			
			return "Pass";
		}
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method shippingDetails "
				+ e);
		return "Fail";
	}

}


/***************************************************************************
 * Method Name			: addNewCreditCard()
 * Created By			: Vishwanath Chavan
 * Reviewed By			: Ramesh,KB
 * Purpose				: The purpose of this method is to add the new credit 
 * 						  card details for billing process
 ****************************************************************************
 */
	
	public static String addNewCreditCard() {
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
		
		//if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
            if(UIFoundation.isDisplayed(FinalReviewPage.btnRecipientContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnRecipientContinue));				
				UIFoundation.waitFor(5L);
			}
		 if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnDeliveryContinue));				
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);

			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(2L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				
/*				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(driver, "BillingAndShippingCheckbox");
			    UIFoundation.clickObject(driver, "BillingAndShippingCheckbox");
			    UIFoundation.waitFor(3L);
				}else{*/
				if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
				//	objStatus+=String.valueOf(UIFoundation.setObject(driver,"NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
					
				//}
/*				objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthMonth","birthMonth"));
				objStatus+=String.valueOf(UIFoundation.eventFiringWebDriver(driver,"birthDate","birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "birthYear", "birthYear"));
				UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
				UIFoundation.waitFor(1L);*/
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
            {
                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
                  UIFoundation.waitFor(5L);
                  
            }
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
				logger.pass("Order Number :"+orderNum);
		    	objStatus+=true;			
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null.Order not placed successfully";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			
			}
			log.info("The execution of the method addNewCreditCard ended here ...");	
			if (objStatus.contains("false")) {
				System.out.println("Order creation with Fedex address test case is failed");
				logger.fail("Order creation with Fedex address test case is failed");
				return "Fail";
				
			} else {
				System.out.println("Order creation with Fedex address test case is executed successfully");
				logger.pass("Order creation with Fedex address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard "
					+ e);
			return "Fail";
		}
	}

}
