package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class HandleSoldInIncrementOfXProducts extends Desktop {
	
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addIncrementProdTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		   String screenshotName = "Scenarios_IncrementProd_Screenshot.jpeg";
			
		try {
			UIFoundation.waitFor(3L);
			driver.get("https://qwww.wine.com/product/tour-saint-christophe-futures-pre-sale-2018/520689");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			int prodLimit=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			WebElement drop_down =driver.findElement(By.xpath("//select[@class='prodItemStock_quantitySelect']"));
			Select se = new Select(drop_down);
			for(int i=1 ;i<se.getOptions().size(); i++)
			{
				int prodLimitInDropDown=Integer.parseInt(se.getOptions().get(i).getAttribute("value"));
				if(prodLimit*(i)==prodLimitInDropDown)
				{
					objStatus+=true;
/*				    String objDetail="Order number is placed successfully";
				     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println(se.getOptions().get(i).getAttribute("value"));*/
				}
				else
				{
					objStatus+=false;
					String objDetail="product limit test case is failed";
					//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					break;
				}
				
			//System.out.println(se.getOptions().get(i).getAttribute("value"));
			}

			if (objStatus.contains("false")) {
				System.err.println("Product limit test case is failed");
				return "Fail";
			} else {
				System.out.println("Product limit test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}
