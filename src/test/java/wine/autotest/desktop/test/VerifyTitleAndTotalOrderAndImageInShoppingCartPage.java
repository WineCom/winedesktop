package wine.autotest.desktop.test;

import java.io.IOException;
import java.text.DecimalFormat;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyTitleAndTotalOrderAndImageInShoppingCartPage extends Desktop {

	

	/***************************************************************************
	 * Method Name : verifyTotalOrderSummary() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyTotalOrderSummary() {
		String firstProductPrice = null;
		String secondProductPrice = null;
		String thirdProductPrice = null;
		String fourthProductPrice = null;
		//String fifthProductPrice = null;
		double totalProdPrice = 0;
		double firstProdPrice = 0;
		double secondProdPrice = 0;
		double thirdProdPrice = 0;
		double fourthProdPrice = 0;
		//double fifthProdPrice = 0;
		String expectedShoppingCartPageTitle = null;
		String screenshotName = "Scenarios__summaryImageScreenshot.jpeg";
		
		try {

			
			if (!UIFoundation.getText(ListPage.spnFirstProductPrice).contains("Fail")) {
				firstProductPrice = UIFoundation.getText(ListPage.spnFirstProductPrice);
				firstProductPrice = firstProductPrice.replaceAll("[ ]", "");
				firstProdPrice = Double.parseDouble(firstProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(firstProdPrice);

			}
			/*	if (UIFoundation.isDisplayed(driver, "SecondProductStrike")) {
				thirdProductPrice = UIFoundation.getText(driver, "SecondProductSalePrice");
				thirdProductPrice = thirdProductPrice.replaceAll("[ ]", "");
				thirdProdPrice = Double.parseDouble(thirdProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(driver, thirdProdPrice);
			}*/
			if (!UIFoundation.getText(ListPage.spnSecondProductPrice).contains("Fail")) {
				secondProductPrice = UIFoundation.getText(ListPage.spnSecondProductPrice);
				secondProductPrice = secondProductPrice.replaceAll("[ ]", "");
				secondProdPrice = Double.parseDouble(secondProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(secondProdPrice);

			}

			/*if (UIFoundation.isDisplayed(driver, "ThirdProductStrike")) {
				thirdProductPrice = UIFoundation.getText(driver, "ThirdProductSalePrice");
				thirdProductPrice = thirdProductPrice.replaceAll("[ ]", "");
				thirdProdPrice = Double.parseDouble(thirdProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(driver, thirdProdPrice);
			}*/
			if (!UIFoundation.getText(ListPage.spnThirdProductPrice).contains("Fail")) {
				thirdProductPrice = UIFoundation.getText(ListPage.spnThirdProductPrice);
				thirdProductPrice = thirdProductPrice.replaceAll("[ ]", "");
				thirdProdPrice = Double.parseDouble(thirdProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(thirdProdPrice);

			}

			/*if (UIFoundation.isDisplayed(driver, "FourthProductStrike")) {
				fourthProductPrice = UIFoundation.getText(driver, "FourthProductSalePrice");
				fourthProductPrice = fourthProductPrice.replaceAll("[ ]", "");
				fourthProdPrice = Double.parseDouble(fourthProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(driver, fourthProdPrice);
			}
			if (!UIFoundation.getText(driver, "FourthProductPrice").contains("Fail")) {
				fourthProductPrice = UIFoundation.getText(driver, "FourthProductPrice");
				fourthProductPrice = fourthProductPrice.replaceAll("[ ]", "");
				fourthProdPrice = Double.parseDouble(fourthProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(driver, fourthProdPrice);
			}*/

			/*if (UIFoundation.isDisplayed(driver, "FifthProductStrike")) {
				fifthProductPrice = UIFoundation.getText(driver, "FifthProductSalePrice");
				fifthProductPrice = fifthProductPrice.replaceAll("[ ]", "");
				fifthProdPrice = Double.parseDouble(fifthProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(driver, fifthProdPrice);
			}

			if (!UIFoundation.getText(driver, "FifthProductPrice").contains("Fail")) {
				fifthProductPrice = UIFoundation.getText(driver, "FifthProductPrice");
				fifthProductPrice = fifthProductPrice.replaceAll("[ ]", "");
				fifthProdPrice = Double.parseDouble(fifthProductPrice);
				totalProdPrice += UIBusinessFlows.productPrice(driver, fifthProdPrice);
			}*/

			expectedShoppingCartPageTitle = verifyexpectedresult.shoppingCartPageTitle;
			String actaulTitle = driver.getTitle();
			if (expectedShoppingCartPageTitle.equalsIgnoreCase(actaulTitle)) {
				System.out.println(actaulTitle);
			}
			UIFoundation.waitFor(3L);
			String totalCount = UIFoundation.getText(ListPage.btnCartCount);
			System.out.println("Total no of items in the cart are:" + totalCount);
			UIFoundation.waitFor(2L);
			String subTotal = UIFoundation.getText(CartPage.spnSubtotal);
			String shippingAndHandling = UIFoundation.getText(CartPage.spnShippingHandling);
			shippingAndHandling = shippingAndHandling.replaceAll("[$,]", "");
			double shipAndHandling = Double.parseDouble(shippingAndHandling);
			totalProdPrice += shipAndHandling;
			String totalPriceBeforeTaxr = UIFoundation.getText(CartPage.spnTotalBeforeTax);
			totalPriceBeforeTaxr = totalPriceBeforeTaxr.replaceAll("[$,]", "");
			double totPriceBeforeTax = Double.parseDouble(totalPriceBeforeTaxr);
			totalProdPrice = Double.parseDouble(new DecimalFormat("##.##").format(totalProdPrice));
			System.out.println("Total price of all products with shipping and handling charge:" + totalProdPrice);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : " + subTotal);
			System.out.println("Shipping and handling : " + shippingAndHandling);
			System.out.println("Total Before Tax :" + totalPriceBeforeTaxr);

			if (totalProdPrice == totPriceBeforeTax) {
				ReportUtil.addTestStepsDetails(
						"Verify the total of the order in order summary widget test case is executed successfully",
						"Pass", "");
				System.out.println(
						"Verify the total of the order in order summary widget test case is executed successfully");
				return "Pass";

			} else {
				String objDetail = "Verify the total of the order in order summary widget test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Verify the total of the order in order summary widget test case is failed");
				return "Fail";

			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : verifyImageIsAvailable() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyImageIsAvailable() {
		try {

			if (!UIFoundation.getText(CartPage.imgFirstImageInCart).contains("Fail")) {
				System.out.println("Item Image is available for first product");

			}

			if (!UIFoundation.getText(CartPage.imgSecondImageInCart).contains("Fail")) {
				System.out.println("Item Image is available for second product");
			}

			if (!UIFoundation.getText(CartPage.imgThirdImageInCart).contains("Fail")) {
				System.out.println("Item Image is available for third product");
			}

			if (!UIFoundation.getText(CartPage.imgFourthImageInCart).contains("Fail")) {
				System.out.println("Item Image is available for fourth product");
			}

			/*if (!UIFoundation.getText(driver, "FifthImageInCart").contains("Fail")) {
				System.out.println("Item Image is available for fifth product");

			}*/
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}

}
