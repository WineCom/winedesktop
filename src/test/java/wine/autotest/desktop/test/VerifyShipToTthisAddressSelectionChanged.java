package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyShipToTthisAddressSelectionChanged extends Desktop {

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */

	public static String loginPrefferedAddress()
	{
		String objStatus=null;

		try
		{

			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "preferenceAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name			: preferredAdresSelChanged()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				:
	 ****************************************************************************
	 */

	public static String preferredAdresSelChanged() {


		String objStatus=null;
		try {
			log.info("The execution of the method preferredAdresSelChanged started here ...");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkEditShippingAddress));
			String userFullName=UIFoundation.getAttribute(FinalReviewPage.txtshippingAddressFullName);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkshipAdrPreferredCard));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshipAdrSave));
			UIFoundation.waitFor(12L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			String shippingAddressExpectedName=UIFoundation.getText(FinalReviewPage.spnshipAdrExpFullName);
			if(UIFoundation.isSelected(FinalReviewPage.btnshipTothisAdrRadion) && (userFullName.equalsIgnoreCase(shippingAddressExpectedName)))
			{
				objStatus+=true;
				String objDetail="Preferred address is updated,It is set as 'ship to this address'";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Preferred address is not updated,It is not set as 'ship to this address'";
				UIFoundation.captureScreenShot(screenshotpath+"preferredSetAddr", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method preferredAdresSelChanged ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify ship to this address selection is changed on updating the preferred address. test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify ship to this address selection is changed on updating the preferred address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method preferredAdresSelChanged "
					+ e);
			return "Fail";
		}
	}

}
