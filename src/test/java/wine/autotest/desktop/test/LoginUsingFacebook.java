package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class LoginUsingFacebook extends Desktop {
	
	/***************************************************************************
	 * Method Name			: loginUsingFacebook()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String loginUsingFacebook()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_facebookLogin_Screenshot.jpeg";	
		try
		{
			log.info("The execution of the method loginUsingFacebook started here ...");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btncontinueWithFacebook));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtfacebookEmail, "usernameFacebook"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtfacebookPass, "passwordFacebook"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnfacebookLogin));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)){
				  objStatus+=true;
			      String objDetail="Customer is navigated to Recipient section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Customer is unable to navigated to Recipient section";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method loginUsingFacebook ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the user is able to login using Facebook account test case failed");
				return "Fail";
				
			}
			else
			{
				System.out.println("Verify the user is able to login using Facebook account test case executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}

}
