package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.test.Desktop;


public class InvalidPromoCode extends Desktop {
	
	
	/***************************************************************************
	 * Method Name			: invalidPromoCode()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String invalidPromoCode()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedPromocodeMsg=null;
		String actualPromocodeMsg=null;
		String ScreenshotName = "invalidPromoCode.jpeg";
		
		try
		{
			log.info("The execution of the method invalidPromoCode started here ...");
			System.out.println("============Order summary ===============");
			expectedPromocodeMsg=verifyexpectedresult.inValidPromoCodemsg;
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{
				
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "inValidPromoCode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(15L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "inValidPromoCode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(15L);
			}

			actualPromocodeMsg=UIFoundation.getText(CartPage.spnInvalidPromoCode);
			UIFoundation.waitFor(5L);
			System.out.println("expectedPromocodeMsg : "+expectedPromocodeMsg);
			System.out.println("actualPromocodeMsg : "+actualPromocodeMsg);
			if(actualPromocodeMsg.equalsIgnoreCase(expectedPromocodeMsg))
			{
				objStatus+=true;
				String objDetail = "Verified using Invaid promocode";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				 System.out.println(expectedPromocodeMsg);
			}
			
			else
			{
				 objStatus+=false;
				 String objDetail="Verification of invalid promo code test case failed";
			     ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			     logger.fail(objDetail);
			     UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);		 
				 System.err.println(actualPromocodeMsg);
			}
			log.info("The execution of the method invalidPromoCode ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method invalidPromoCode "+ e);
			return "Fail";
			
		}
	}
	

}
