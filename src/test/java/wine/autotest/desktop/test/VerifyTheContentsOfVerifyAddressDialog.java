package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyTheContentsOfVerifyAddressDialog extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-645,649,646
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		   String screenshotName = "Scenarios_VerifySuggested_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			
/*			if(UIFoundation.isDisplayed(driver, "SuggestedAddressLabel") && UIFoundation.isDisplayed(driver, "OriginalAddressLabel"))
			{
				  objStatus+=true;
			      String objDetail="Verified the contents of Verify Address Dialog";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the contents of Verify Address Dialog");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the contents of Verify Address Dialog test case is failed";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				   System.err.println("Verify the contents of Verify Address Dialog");
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.rdoOriginalAddress) && UIFoundation.isDisplayed(driver, "OriginalAddressEditButton"))
			{
				  objStatus+=true;
			      String objDetail="Verified  the Original address content in Address Dialog";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified  the Original address content in Address Dialog");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the Original address content in Address Dialog is failed";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				   System.err.println("Verify the Original address content in Address Dialog");
			}*/
			if(!UIFoundation.isSelected(FinalReviewPage.rdoOriginalAddress))
			{
				  objStatus+=true;
			      String objDetail="'Use this Shipping Address' checkbox should be available and should be unmarked by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("'Use this Shipping Address' checkbox should be available and should be unmarked by default");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Use this Shipping Address' checkbox should be available and not unmarked by default";
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			       logger.fail(objDetail);
				   System.err.println("'Use this Shipping Address' checkbox should be available and not unmarked by default");
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified  the Continue button functionality in Verify Address dialog";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified  the Continue button functionality in Verify Address dialog");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the Continue button functionality in Verify Address dialog test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"ContinueButtonVerifyAddress", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify the Continue button functionality in Verify Address dialog");
			}


			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
			//	System.out.println("Verify the contents of Verify Address Dialog test case is failed");
				return "Fail";
			} else {
				//System.out.println("Verify the contents of Verify Address Dialog test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	

}
