package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyTheElementsDisplayedOverTheHeroImage extends Desktop{
	
	/***************************************************************************
	 * Method Name			: VerifyTheElementsDisplayedOverTheHeroImage()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyTheElementsDisplayedOverTheHeroImage()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_heroImage.jpeg";
			
		try
		{
			log.info("The execution of the method verifyTheElementsDisplayedOverTheHeroImage started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkRegion));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.cmbCARegion));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.cmbCARegSonomaCounty));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.titheroImageTitleSonoma))
			{
				objStatus+=true;
				String objDetail="Varietal name Sonoma County  is displayed ";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Varietal name Sonoma County is not displayed ";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			if(UIFoundation.isDisplayed(ListPage.heroImagePlusSymbol))
			{
				objStatus+=true;
				String objDetail="Plus symbol is displayed over the hero image ";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Plus symbol is not displayed over the hero image";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.heroImagePlusSymbol));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.ContheroImageTextSonoma))
			{
				objStatus+=true;
				String objDetail="Preview text Content for Sonoma County is displayed ";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Preview text Content for Sonoma County is not displayed ";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyTheElementsDisplayedOverTheHeroImage ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifyTheElementsDisplayedOverTheHeroImage "+ e);
			return "Fail";
			
		}
	}	

}
