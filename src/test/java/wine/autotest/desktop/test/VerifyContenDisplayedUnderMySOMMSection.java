package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyContenDisplayedUnderMySOMMSection extends Desktop{	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method login started here ...");
		    //logger.pass("Execution of Test case verifyContentDisplayedUnderMySOMMSection started (inside)");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(2L);		
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "compassuser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnSignIn));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);		
			String actualtile=driver.getTitle();
			String expectedTile=verifyexpectedresult.shoppingCartPageTitle;
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
				logger.pass("Passed Login");
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
		}
		catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: VerifySubscriptions
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method to verifySubscriptions
	 ****************************************************************************
	 */
	public static String verifySubscriptions()
	{
		String objStatus=null;			
		String screenshotName = "Scenarios_verifySubscriptions_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;					
		try
		{			
			log.info("The execution of the method verifySubscriptions started here ...");
		    //logger.info("The execution of the method verifySubscriptions started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.lnkPickedSetting));
		    UIFoundation.waitFor(4L);
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyWelcomPicked));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.cmdVerifyRedWine));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.cmdVerifyWhiteWine));
		    UIFoundation.waitFor(1L);  		    		    
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtOrderStatus));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtSubcreption));		    
		    UIFoundation.waitFor(1L);
		    UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtRecipient);
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtPayment));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtChangeAdd));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtChangePay));
		    UIFoundation.waitFor(1L);		    
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtCancleSub));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtTermAndCon));
		    UIFoundation.waitFor(1L);		  		    		    
			log.info("The execution of the method verifySubscriptions ended here ...");		
			if (objStatus.contains("false"))
			{
				objStatus+=false;			
		    	String objDetail="All the contents are not displayed in the MySOMM page";
		    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());    	
		    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				    objStatus+=true;				
					String objDetail = "All the contents are displayed in the MySOMM page";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
					return "Pass";
			}
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to verifySubscriptions";
	    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			log.error("there is an exception arised during the execution of the method verifySubscriptions "+ e);
			return "Fail";	
		}
	}		
}