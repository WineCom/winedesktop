package wine.autotest.desktop.test;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyBillingPhoneNumberIsRequiredField extends Desktop {
	/***************************************************************************************
	 * Method Name : AddnewcreditCardWithoutPhoneNumberInPaymentSection Created By :
	 * Rakesh Reviewed By : TM-4033
	 * 
	 * @throws IOException
	 ***************************************************************************************
	 */

	public static String addnewcreditCardWithoutPhoneNumberInPaymentSection() {

		String ScreenshotName = "AddnewcreditCardWithoutPhoneNumber.jpeg";
		
		String objStatus = null;
		String ActualErrorMesage = null;
		try {
			log.info("Execution adding new credit card withput phone number started here");
			
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkChangeAddress))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkAddNewAddress)) {
				objStatus += String.valueOf(UIFoundation.clckObject(FinalReviewPage.lnkRecipientShipToaddress));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			}

			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.btnstateContinueButton)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnstateContinueButton));
			//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnstateContinueButton, "Invisible", "", 50);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkAddPymtMethod))
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			UIFoundation.clckObject(FinalReviewPage.btnsaveCreditCard);
			WebElement ele = driver.findElement(By.xpath(
					"//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if (ele.isSelected()) {
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "militaryStateAA"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingZip, "militaryStateAACode"));
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnPhoneNumberReq);
			ActualErrorMesage = UIFoundation.getText(FinalReviewPage.spnPhoneNumberReq);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.spnPhoneNumberReq)) {
				System.out.println("Phone number required validation message is displayed :" + ActualErrorMesage);
				objStatus += true;
				ReportUtil.addTestStepsDetails("Verified the error mesage validation for phonenumber in billing and info section", "Pass", "");
				logger.pass("Verified the error mesage validation for phonenumber in billing and info section");
			} else {
				objStatus += false;
				String objDetail = "Phonenumber validation message is not displayed in billing and shipping info section.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());

			}
			log.info("Execution method ended here");
			if (objStatus.contains("false")) {
				System.out.println("Verification of error message failed in payment section");
				return "fail";

			} else {
				System.out.println("Verification of error message in payment section executed succesfully");
				return "pass";

			}
		} catch (Exception e) {
			log.error("Execution method aborted due to some error " + e);
			return "fail";

		}

	}

	/***************************************************************************************
	 * Method Name : AddnewcreditCardWithoutPhoneNumberInPaymentSection Created By :
	 * Rakesh Reviewed By :
	 * 
	 * @throws IOException
	 ***************************************************************************************
	 */

	public static String addnewcreditCardWithoutPhoneNumberInPaymentMethod() {

		String objStatus = null;
		String ScreenshotName = "AddnewcreditCardWithoutPhoneNumberInPaymentMethod.jpeg";
		String ActualErrorMesageInPayemntSection = null;
		try {
			log.info("Excution method started here");
			// objStatus+=String.valueOf(UIFoundation.clickObject(Driver,
			// "cancelpaymentR"));
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.imgWineLogo));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.imgWineLogo));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPaymentMethods));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewCard));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtExpiryYear, "Yr"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingAddress, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingSuite, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingCity, "BillingCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnNewBillinState, "militaryStateAA"));
			objStatus += String
					.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingZip, "militaryStateAACode"));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnBillingSave));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.spnPhoneNumberP);
			ActualErrorMesageInPayemntSection = UIFoundation.getText(UserProfilePage.spnPhoneNumberP);
			if (UIFoundation.isElementDisplayed(UserProfilePage.spnPhoneNumberP)) {
				System.out.println("Phone number required validation message :" + ActualErrorMesageInPayemntSection);
				objStatus += true;
				ReportUtil.addTestStepsDetails("Verified the error mesage validation for phonenumber in PayemntMethod","Pass", "");
				logger.pass("Verified the error mesage validation for phonenumber in PayemntMethod");
			} else {
				objStatus += false;
				String objDetail = "Phonenumber validation message is not displayed in PayemntMethod section.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());

			}
			log.info("Execution method ended here");
			if (objStatus.contains("fail")) {
				System.out.println("Verification of error message failed in paymentmethod section failed");
				return "fail";

			} else {
				System.out.println("Verification of error message in paymentmethod section executed succesfully");
				return "pass";

			}
		} catch (Exception e) {
			log.error("Execution method aborted due to some error " + e);
			return "fail";

		}

	}

}
