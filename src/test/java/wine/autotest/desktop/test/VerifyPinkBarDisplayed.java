package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.library.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyPinkBarDisplayed extends Desktop {
	
	/***************************************************************************
	 * Method Name			: VerifyPinkBarDisplayed()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String verifyPinkBarDisplayed()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_verifyPinkBar_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			String giftCert=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Gift card is applied to the cart total and the appropriate message is displayed above the gift certificate field.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Gift certificate has been expired.Please enter new gift certificate";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			if(!UIFoundation.getText(CartPage.lnkRemoveThirdProduct).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveThirdProduct));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
			}
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkOrderStatus));
			UIFoundation.waitFor(3L);
			String expectedBarColor=verifyexpectedresult.verifyPinkBar;
			String actualBarColor=UIFoundation.getColor(UserProfilePage.spnverifyPinkBar);
			if((UIFoundation.isDisplayed(UserProfilePage.spnverifyCreditTotal)) && (expectedBarColor.equalsIgnoreCase(actualBarColor)))
			{ objStatus+=true;
		      String objDetail="Pink bar is displayed, indicating FULL amount of credit";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      logger.pass(objDetail);
			}else
			{
				objStatus+=false;
				String objDetail="pink bar and FULL amount of credit is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"pink", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the customerís account credit in Account section: full credit test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the customerís account credit in Account section: full credit test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}

}
