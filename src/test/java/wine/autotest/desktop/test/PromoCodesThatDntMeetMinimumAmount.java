package wine.autotest.desktop.test;


import java.io.IOException;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class PromoCodesThatDntMeetMinimumAmount extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
	
		try {
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
	
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
        	System.out.println("addToCart1 :"+addToCart1);
        	
			if (addToCart1.equals("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
				UIFoundation.waitFor(1L);
			}
						
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			}
			else {
				
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			}
			UIFoundation.waitFor(3L);
		//	UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
				UIFoundation.waitFor(10L);
				
			}
			System.out.println("addprodTocrt : "+objStatus);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String enterPromoCode()
	{
		String objStatus=null;
		
		   String screenshotName = "Scenarios_PromoCodeMinimum_Screenshot.jpeg";
			
		
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{	
				objStatus += String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(10L);
			}else{
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus += String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(15L);
			}

			if(UIFoundation.isDisplayed(CartPage.spnminimumOfferAmount)){
				objStatus+=true;
			      String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed.";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectLastValueFromDropdown(CartPage.dwnincreaseQuantity);
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.spnPromoCodePrice)){
				objStatus+=true;
			      String objDetail="Promo code is immediately applied and displayed in the Order Summary";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="promo code is not applied ";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectValueByValue(CartPage.dwnincreaseQuantity, "1");
			UIFoundation.waitFor(13L);
			if(UIFoundation.isDisplayed(CartPage.spnminimumOfferAmount) && (!UIFoundation.isDisplayed(CartPage.spnPromoCodePrice))){
				objStatus+=true;
			      String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed and promo credit isn't applied.";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}

	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,
	 * Jira Id              : TM - 3921
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String validatePromoCodeMessageDisplayed()
	{
		String objStatus=null;
		
		   String screenshotName = "Scenarios_PromoCodeMinimum_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method validatePromoCodeMessageDisplayed started here ...");
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{	
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
//				UIFoundation.webDriverWaitForElement(CartPage.lnkRemovePromoCode, "Clickable", "", 50);
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
//				UIFoundation.webDriverWaitForElement(CartPage.lnkRemovePromoCode, "Clickable", "", 50);
			}

			if(UIFoundation.isDisplayed(CartPage.spnminimumOfferAmount)){
				objStatus+=true;
				 String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed.";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				 String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectLastValueFromDropdown(CartPage.dwnincreaseQuantity);
			UIFoundation.waitFor(9L);
			if(UIFoundation.isDisplayed(CartPage.spnPromoCodePrice)){
				objStatus+=true;
			      String objDetail="Promo code is immediately applied and displayed in the Order Summary";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="promo code is not applied ";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectValueByValue(CartPage.dwnincreaseQuantity, "1");
			UIFoundation.waitFor(9L);
			if(UIFoundation.isDisplayed(CartPage.spnminimumOfferAmount) && (!UIFoundation.isDisplayed(CartPage.spnPromoCodePrice))){
				objStatus+=true;
				   String objDetail="Promo code applied' message is displayed and discount is added in the order summary succesfully";
				      System.out.println(objDetail);
				      logger.pass(objDetail);
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Promo code applied' message is not displayed and discount is not added in the order summary";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method validatePromoCodeMessageDisplayed ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
}
