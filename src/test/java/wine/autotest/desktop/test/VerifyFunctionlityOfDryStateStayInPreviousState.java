package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class VerifyFunctionlityOfDryStateStayInPreviousState extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String dryStateShippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(10L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);  
/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);
			}

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(3L);
			
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtgiftRecipientEmail);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(20L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(20L);
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkStayInPreviousState));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientHeader));
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
				System.out.println("Verify the functionality of �Stay in previous state� test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the functionality of �Stay in previous state� test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}


}
