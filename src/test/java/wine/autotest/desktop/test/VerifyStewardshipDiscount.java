package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyStewardshipDiscount extends Desktop {
	
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String stewardLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "StewrdshipRenewalUsername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String stewardshipSaving=null;
		String shippingHandling=null;
		String stewardshipSave=null;
		   String screenshotName = "Scenarios_Stewardship.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying Stewardship code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			objStatus+=String.valueOf(stewardshipSaving=UIFoundation.getText(CartPage.spnStewardshipDiscountPrice));
			stewardshipSave=stewardshipSaving.replaceAll("[^0-9]", "");
			shippingHandling=shippingAndHandling.replaceAll("[^0-9]", "");
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			System.out.println("shippingHandling : "+shippingHandling);
			System.out.println("stewardshipSave : "+stewardshipSave);
			if(shippingHandling.equals(stewardshipSave))
			{
				System.out.println("Stewardship Savings:      "+stewardshipSaving);
				objStatus+=true;
				String objDetail="Stewardship amount is discounted successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Stewardship amount is discounted successfully");
			}
			else
			{
				objStatus+=false;
				String objDetail="Stewardship amount is not discounted";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}

}
