package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;


public class ForgotPassword extends Desktop {
	
	/***************************************************************************
	 * Method Name			: forgotPassword()
	 * Created By			: Ramesh S
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String forgotPassword()
	{
		String objStatus=null;
		String actual=null;
		String expected=null;
		try
		{
			log.info("The execution of the method Forgot Password started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkForgotPassword));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtForgotEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotContinue));
			UIFoundation.waitFor(3L);
			expected=verifyexpectedresult.plzCheckUrMail;
			actual=UIFoundation.getText(LoginPage.spnCheckUrEmail);
			log.info("The execution of the method Forgot Password ended here ...");
			if (objStatus.contains("false") && !(expected.equalsIgnoreCase(actual)))
			{
				System.out.println("Forgot Password test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Forgot Password test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method Forgot Password "+ e);
			return "Fail";
			
		}
	}

}
