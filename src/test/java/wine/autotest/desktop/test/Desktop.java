package wine.autotest.desktop.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import org.testng.annotations.Parameters; 
import wine.autotest.desktop.test.BaseTest;
import wine.autotest.desktop.test.ProductListFilteration;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.GlobalVariables;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.desktop.library.Initialize;

public class Desktop extends GlobalVariables {

	
	static int numberOfTestCasePassed=0;
	static int numberOfTestCaseFailed=0;
	public static String startTime=null;
	public static String endTime=null;
	public static String objStatus = null;
	public static String teststarttime=null;
	
	public static String TestScriptStatus="";
	
	/***************************************************************************
	 * Method Name			: ExtendReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method  is to generate extend report
	 ****************************************************************************
	 */
	@BeforeTest
	public void ExtendReport()
	{
		try
		{
			//Extend Report
			ExtentHtmlReporter reporter =new ExtentHtmlReporter(ExtendDesktopReportFileName);
	        report =new ExtentReports();
	        report.attachReporter(reporter);
	        reporter.config().setDocumentTitle("Platform Automation");
	        reporter.config().setReportName("Desktop Execution Report");
	       
	     	
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	}
	
	/***************************************************************************
	 * Method Name			: loadFiles()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method  is to load .properties 
	 * 						   files.
	 ****************************************************************************
	 */
	
	@BeforeClass
	@Parameters({"browser", "testEnvironment", "teststate"})
	public static void loadFiles(String browser, String testEnvironment, String teststate)
	{ 
		String teststarttime=null;
		try
		{
				log=Logger.getLogger("wine Automation ...");
				teststarttime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				environmentUrl=testEnvironment;
				state=teststate;
				ReportUtil.createReport(DesktopReportFileName, teststarttime,environmentUrl);
				UIBusinessFlows.deleteFile(driver);
				xmldata=new XMLData();
				url=UIFoundation.property(configurl);
				UIFoundation.screenshotFolderName("screenshots");
				ReportUtil.startScript("Scenarios");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/***************************************************************************
	 * Method Name			: LaunchBrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: This method is used to launch the browser.
	 ****************************************************************************
	 */
	@BeforeMethod
	@Parameters({"browser"})
	public static void Launchbrowser(String browser)
	{ 
			try
		{
				log=Logger.getLogger("Launch Browser ...");
				ReportUtil.deleteDescription();
				driver=Initialize.launchBrowser(browser);
				objStatus = null;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: TC01_Basetest
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: this method is responsible for execution of all the 
	 * 						  scenarios.
	 ****************************************************************************
	 */

	@Test
	public static void TC01_Basetest()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
					{	
						Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                        String[] m1=  Method.split("_");
                        testCaseID = m1[0];
                        methodName = m1[1];
                logger=report.createTest(Method); 
				objStatus += String.valueOf(Initialize.navigate());
				objStatus += String.valueOf(BaseTest.login());
				objStatus += String.valueOf(BaseTest.logout());
				objStatus += String.valueOf(BaseTest.userProfileCreation());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				}
					}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : TC02_productFilteration() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC02_productFilteration() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method);
            System.out.println("Executing "+Method);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ProductListFilteration.verifyOnlyThreeFiltersAreVisible());
			objStatus += String.valueOf(ProductListFilteration.verifyMoreFiltersElementsAreVisible());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/***************************************************************************
	 * Method Name : TC03_orderCreationWithExistingAccount() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC03_orderCreationWithExistingAccount() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(OrderCreationWithExistingAccount.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/***************************************************************************
	 * Method Name : TC04_orderCreationWithNewAccount() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC04_orderCreationWithNewAccount() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/***************************************************************************
	 * Method Name : TC05_saveForLaterWithoutSignIn() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC05_saveForLaterWithoutSignIn() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(UIBusinessFlows.validationForSaveForLaterWithoutSignIn());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/***************************************************************************
	 * Method Name : TC06_saveForLaterWithSignIn() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC06_saveForLaterWithSignIn() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
        	objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(UIBusinessFlows.validationForSaveForLaterWithSignIn());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/***************************************************************************
	 * Method Name : TC07_orderCreationWihNewAccountBillingAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC07_orderCreationWihNewAccountBillingAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            logger.assignCategory("Order_Creation");
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithNewAccountBillingAddress.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithNewAccountBillingAddress.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	/***************************************************************************
	 * Method Name : TC08_orderCreationWihExistingAccountBillingAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC08_orderCreationWihExistingAccountBillingAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(OrderCreationWithExistingAccountBillingAddress.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
 
	}
	
	/***************************************************************************
	 * Method Name : TC09_orderCreationWithPromoCodeForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC09_orderCreationWithPromoCodeForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.captureOrdersummary());
			objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
 
	}
	
	/***************************************************************************
	 * Method Name : TC10_orderCreationWithPromoCodeForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC10_orderCreationWithPromoCodeForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.captureOrdersummary());
			objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
 
	}
	
	/***************************************************************************
	 * Method Name : TC11_removeFewProductsFromTheCart() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC11_removeFewProductsFromTheCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemoveFewProductsFromTheCart.captureOrdersummary());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
 
	}
	
	/***************************************************************************
	 * Method Name : TC12_productsInCartAfterRemove() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC12_productsInCartAfterRemove() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method);
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ProductsInCartAfterRemove.captureOrdersummary());
			objStatus += String.valueOf(ProductsInCartAfterRemove.productsAvailableInTheCart());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
 
	}
	
	/***************************************************************************
	 * Method Name : TC13_removeProductsFromTheSaveForLater() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC13_removeProductsFromTheSaveForLater() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.moveProductsToSaveForLater());
			objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.productsAvailableInSaveForLater());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC14_forgotPassword() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC14_forgotPassword() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ForgotPassword.forgotPassword());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC15_userProfileCreationWithDesiredState() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	@Parameters({"browser"})
	public static void TC15_userProfileCreationWithDesiredState(String browser) {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method);
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserCreationWithDesiredState.userProfileCreation());
			objStatus += String.valueOf(Initialize.closeApplication());
			driver = Initialize.launchBrowser(browser);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserCreationWithDesiredState.login());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : TC16_userProfileCreationWithCheckoutProcess() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	@Parameters({"browser"})
	public static void TC16_userProfileCreationWithCheckoutProcess(String browser) {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(UserCreationWithCheckoutProcess.userProfileCreation());
			objStatus += String.valueOf(Initialize.closeApplication());
			driver = Initialize.launchBrowser(browser);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserCreationWithCheckoutProcess.login());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC17_userProfileCreationMyWine() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	@Parameters({"browser"})
	public static void TC17_userProfileCreationMyWine(String browser) {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserProfileCreationWithMyWine.userProfileCreation());
			objStatus += String.valueOf(BaseTest.logout());
			objStatus += String.valueOf(Initialize.closeApplication());
			driver = Initialize.launchBrowser(browser);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserProfileCreationWithMyWine.login());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC18_orderCreationWithGiftCardForExistinguser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC18_orderCreationWithGiftCertForExistinguser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.captureOrdersummary());
			objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC19_orderCreationWithGiftCardForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC19_orderCreationWithGiftCertForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.captureOrdersummary());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC20_verifyStewardshipDiscount() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC20_verifyStewardshipDiscount() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyStewardshipDiscount.stewardLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipDiscount.captureOrdersummary());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC21_sortingOption() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC21_sortingOption() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsAtoZ());
			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsZtoA());
			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsLtoH());
			objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsHtoL());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC22_verifyDiscountPriceInRedColorAndGrandTotalInBold() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC22_verifyDiscountPriceInRedColorAndGrandTotalInBold() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyDiscountPriceInRedColorAndGrandTotalInBoldColor.captureOrdersummary());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC23_searchProductWithCharacter() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC23_searchProductWithCharacter() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(SearchProductByCharacter.searchProductWithProdName());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC24_removeAllProductsFromTheCartContinueShoppingWithoutSignIn() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC24_removeAllProductsFromTheCartContinueShoppingWithoutSignIn() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithoutSignIn.addprodTocrt());
			objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithoutSignIn.removeProductsFromTheCart());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC25_removeAllProductsFromTheCartContinueShoppingWithSignIn() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC25_removeAllProductsFromTheCartContinueShoppingWithSignIn() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithSignIn.addprodTocrt());
			objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithSignIn.removeProductsFromTheCart());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC26_giftRecipientEmailValidation() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC26_giftRecipientEmailValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
           	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(GiftReceipientEmailValidation.editRecipientPlaceORder());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(GiftReceipientEmailValidation.verifyGiftRecipientEmail());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC27_applyAndRemoveromoCodeForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC27_applyAndRemoveromoCodeForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ApplyAndRemovePromoCodeForExistingUser.captureOrdersummary());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC28_applyAndReovePromoCodeForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC28_applyAndReovePromoCodeForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ApplyAndRemovePromoCodeForNewUser.captureOrdersummary());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC29_quantityPickerInCartPage() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC29_quantityPickerInCartPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(QuantityPicker.quantityPick());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC30_invalidPromoCode() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC30_invalidPromoCode() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(InvalidPromoCode.invalidPromoCode());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC31_verifySalesTaxInCartandFinalReviewPage() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC31_verifySalesTaxInCartandFinalReviewPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifySalesTaxInCartAndFinalReviewPage.verifySalesTaxInCartPage());
			objStatus += String.valueOf(VerifySalesTaxInCartAndFinalReviewPage.verifySalesTaxInFinalReviewPage());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC32_QuantityPickerFromPIP() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC32_QuantityPickerFromPIP() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(QuantityPickerFromPIP.verifyQuantityPickerInCartPage());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC33_finalReviewPageContinueShopping() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC33_finalReviewPageContinueShopping() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(FinalReviewPageContinueShopping.verifyContinueShopping());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC34_invalidGiftCardValidation() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC34_invalidGiftCardValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(InvalidGiftCertificate.invalidGiftCardValidation());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC35_VerifyWarningMsgaddShipTodayProductsAndListProductsToCart() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC35_VerifyWarningMsgaddShipTodayProductsAndListProductsToCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyWarningMessageForShipTodayProductsAndListProducts.addShipTodayProdTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC36_verifyWarningMsgForDryStateShippingAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test
	public static void TC36_verifyWarningMsgForDryStateShippingAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyFunctionlityOfDryStateStayInPreviousState.dryStateShippingDetails());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC37_verifyWarningMsgForOnlyPreSaleProducts() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC37_verifyWarningMsgForOnlyPreSaleProducts() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyWarningMsgForOnlyPreSaleProductsToCart.addPreSaleProdTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC38_verifyErrorMsgForUsedGiftCard() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC38_verifyErrorMsgForUsedGiftCard() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyErrorMsgForUsedGiftCard.usedGiftCardValidation());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC39_verifyWarningMsgForDryStateSaveForLater() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC39_verifyWarningMsgForDryStateSaveForLater() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.dryStateShippingDetails());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC40_verifyDryStateProductsUnavailableFunctionality() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC40_verifyDryStateProductsUnavailableFunctionality() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyDryStateProductsUnavailableFunctionality.verifyproductUnavailabileFunctionality());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC41_verifyAddToCartButtonFunctionlityForDryState() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC41_verifyAddToCartButtonFunctionlityForDryState() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyAddToCartButtonFunctionlityForDryState.verifyAddToCartButtonFunctionality());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC42_verifyWarningMsgForRemoveShipTodayProductsFromCart() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC42_verifyWarningMsgForRemoveShipTodayProductsFromCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyWarningMsgForRemoveShipTodayProductFromCart.addShipTodayProdTocrt());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC43_HandelsXBottelsPerCustomer() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC43_HandelsXBottelsPerCustomer() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(HandelsXBottelsPerCustomer.addLimitProdTocrt());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC44_CreateAccountButtonInActive() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC44_CreateAccountButtonInActive() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(CreateAccountButtonInactive.createButtonInActive());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC45_VerifyInvalidEmailInCreateAccountForm() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC45_VerifyInvalidEmailInCreateAccountForm() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyInValidEmailInCreateAccountForm.inValidEmail());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC46_HandleSoldInIncrementOfXProducts() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC46_HandleSoldInIncrementOfXProducts() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(HandleSoldInIncrementOfXProducts.addIncrementProdTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC47_SortOptionsOfGiftCard() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC47_SortOptionsOfGiftCard() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(SortFunctionalityOfGiftCards.searchGiftCard());
			objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsAtoZ());
			objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsZtoA());
			objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsLtoH());
			objStatus += String.valueOf(SortFunctionalityOfGiftCards.sortingOptionsHtoL());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC48_VerifyOutOFStockCheckboxChecked() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC48_VerifyOutOFStockCheckboxChecked() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyOutOFStockCheckBoxChecked.searchProductWithProdName());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC49_VerifyProductsAreListedInAlphbeticalOrder() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC49_VerifyProductsAreListedInAlphbeticalOrder() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphbeticalOrder.verifyProductsAreListedAlbhabeticalOrder());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC50_PasswordValidationLessThanSixChar() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC50_PasswordValidationLessThanSixChar() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(PasswordValidationLessThanSixChar.passwordValidationLessThanSixChar());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC51_verifyEditFunctionalityofShippingAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC51_verifyEditFunctionalityofShippingAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityofShippingAddress.recipientAddressEdit()); 
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/***************************************************************************
	 * Method Name : TC52_orderCreationWithFedExAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC52_orderCreationWithFedExAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithFedexAddress.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithFedexAddress.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC53_verifyAddGiftWrappingToCartFunctionality() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC53_verifyAddGiftWrappingToCartFunctionality() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
           
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyAddGiftWrappingToCartFunctionality.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyAddGiftWrappingToCartFunctionality.verifyGiftWrapping());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC54_addProductsToCartWithoutSelectingState() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC54_addProductsToCartWithoutSelectingState() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigateWithoutSelectinState());
			objStatus += String.valueOf(VerifyChooseStateDialogPrompted.addprodTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC55_verifyStewardshipProgramAddedToCart() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC55_verifyStewardshipProgramAddedToCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipProgramAddedToCart.addStewardshipToCart());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC56_verifyItemWithoutQuantityPicker() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC56_verifyItemWithoutQuantityPicker() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyItemWithoutQuantityPicker.verifyQuantityPicker());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC57_verifyUnderVarietalFilters() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC57_verifyUnderVarietalFilters() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyMenuVarietalFilters.verifyVarietalMenu());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC58_verifyTitleAndTotalOrderAndImageInShoppingCartPage() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC58_verifyTitleAndTotalOrderAndImageInShoppingCartPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTitleAndTotalOrderAndImageInShoppingCartPage.verifyTotalOrderSummary());
			objStatus += String.valueOf(VerifyTitleAndTotalOrderAndImageInShoppingCartPage.verifyImageIsAvailable());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC59_verifySaveCreditCardInfoCheckboxUncheckedFunctionality() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC59_verifySaveCreditCardInfoCheckboxUncheckedFunctionality() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxUnchecedFunctionality.verifySaveCrdeitCardInfo());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC60_verifySaveCreditCardInfoCheckboxCheckedFunctionality() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC60_verifySaveCreditCardInfoCheckboxCheckedFunctionality() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxCheckedFunctionality.verifySaveCrdeitCardInfo());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC61_verifyCollapsedPanelInShippingForm() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC61_verifyCollapsedPanelInShippingForm() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyCollapsedPanelInShippingForm.verifyCollapsedPanel());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC62_verifyStateChangeDialog() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC62_verifyStateChangeDialog() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStateChangeDialog.verifyStateChangrDialogandLocalPickUp());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC63_verifySuggestedShippingAddressWidget() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC63_verifySuggestedShippingAddressWidget() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifySuggestedShippingAddressWidget.verifySuggestedAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC64_verifyOptionTryVinatageAndRemoveButton() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC64_verifyOptionTryVinatageAndRemoveButton() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(VerifyOptionTryVinatageAndRemoveButton.searchProductWithProdName());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC65_orderCreationWithRegionForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC65_orderCreationWithRegionForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC66_orderCreationWithRegionForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC66_orderCreationWithRegionForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC67_OrderCreationWithFilteringTheProductsForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC67_OrderCreationWithFilteringTheProductsForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(OrderCreationWithFilteringTheProductsForExistingUser.productFilter());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithFilteringTheProductsForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC68_orderCreationWithProductFilterationForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC68_orderCreationWithProductFilterationForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
			objStatus += String.valueOf(orderCreationWithProductFilterationForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC69_orderCreationWithSpecialProductsForExistingUserr() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC69_orderCreationWithSpecialProductsForExistingUserr() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(OrderCreationWithSpecialProductsForExistingUser.specialProducts());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithSpecialProductsForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC70_orderCreationWithSpecialProductsForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC70_orderCreationWithSpecialProductsForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.specialProducts());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC71_orderCreationWithTopRatedProductsForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC71_orderCreationWithTopRatedProductsForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(OrderCreationWithTopRatedForExistingUser.sortingOptionsTopRated());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithTopRatedForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC72_orderCreationWithTopRatedProductsForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC72_orderCreationWithTopRatedProductsForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(OrderCreationWithTopRatedForNewUser.sortingOptionsTopRated());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithTopRatedForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC73_orderCreationWithGiftsForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC73_orderCreationWithGiftsForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.addGiftsToTheCart());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC74_orderCreationWithGiftsForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC74_orderCreationWithGiftsForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
             logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(OrderCreationWithGiftsForNewUser.addGiftsToTheCart());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithGiftsForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC75_orderCreationWithMyWineForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC75_orderCreationWithMyWineForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
             objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.addMyWineProdTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC76_orderCreationWithShippingMethodForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC76_orderCreationWithShippingMethodForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithShippingMethodForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC77_OrderCreationWithShippingMethodForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC77_OrderCreationWithShippingMethodForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC78_OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC78_OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC79_orderCreationWithEditFunctionalityInFinalReviewPageForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC79_orderCreationWithEditFunctionalityInFinalReviewPageForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(orderCreationWithEditFunctionalityInFinalReviewPageForNewUser.shippingDetails());
			objStatus += String.valueOf(orderCreationWithEditFunctionalityInFinalReviewPageForNewUser.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC80_addAddressUsingUserProfileServicesForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC80_addAddressUsingUserProfileServicesForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddAddressUsingUserProfileServicesForExistingUser.addAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC81_addAddressUsingUserProfileServicesForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC81_addAddressUsingUserProfileServicesForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(addAddressUsingUserProfileServicesForNewUser.addAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC82_orderCreationUsingUserProfileServicesWithStewardshipSettings() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC82_orderCreationUsingUserProfileServicesWithStewardshipSettings() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.addprodTocrt());
			objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.shippingDetails());
			objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC83_addPaymentMethodUsingUserProfileServicesForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC83_addPaymentMethodUsingUserProfileServicesForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
           objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddPaymentMethodUsingUserProfileServicesForExistingUser.addPaymentMethod());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC84_addPaymentMethodUsingUserProfileServicesForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC84_addPaymentMethodUsingUserProfileServicesForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(AddPaymentMethodUsingUserProfileServicesForNewUser.addPaymentMethod());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC85_orderCreationWithWhiteWineForNewUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC85_orderCreationWithWhiteWineForNewUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithWhiteWineForNewUSer.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC86_orderCreationWithWhiteWineForExistingUser() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC86_orderCreationWithWhiteWineForExistingUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            logger.assignCategory("Order_Creation");
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithWhiteWineForExistingUser.checkoutProcess());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC87_verifyGiftWrappingSectionNtDisplayed() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC87_verifyGiftWrappingSectionNtDisplayed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
           objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyGiftWrappingSection.addGiftsTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC88_forgotPasswordValidation() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC88_forgotPasswordValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ForgotPasswordValidation.forgotPassword());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC89_forgotPasswordInvalidEmailErrorMsgValidation() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC89_forgotPasswordInvalidEmailErrorMsgValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ForgotPasswordInvalidEmailMsgValidation.forgotPasswordInvalidEmailMsgValidation());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC90_outBoundGreggOverrideLink() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	/*@Test 
	public static void TC90_outBoundGreggOverrideLink() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.addprodTocrt());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.orderSummaryForNewUser());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.shippingDetails());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	
	/***************************************************************************
	 * Method Name : TC91_emailPrefernce() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC91_emailPrefernce() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(EmailPreference.saveButtonFunctionality());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC92_verifyPromoBarInWashingtonRegion() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
/*	@Test 
	public static void TC92_verifyPromoBarInWashingtonRegion() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
//            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
         	objStatus += String.valueOf(WasingtonRegion.VerifyPromoBarInWashington());endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	/***************************************************************************
	 * Method Name : TC93_ratingStarsHiddenInPIPForGifts() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC93_ratingStarsHiddenInPIPForGifts() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(RatingStarsHiddenInPIPForGifts.VerifyRatingStarsHiddenInPIPForGifts());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC94_InvalidEmailGiftRecipientValidation() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC94_InvalidEmailGiftRecipientValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(InvalidEmailAddressInGiftRecipientEmail.editRecipientPlaceORder());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC95_verifyPromoBarIsDisplayedInAccountPages() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC95_verifyPromoBarIsDisplayedInAccountPages() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyPromoBarIsDisplayedInAccountPages.verifyPromoBarIsDisplayedInAccountPages());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC96_verifyTheFunctionalityOfAddAddressLink() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC96_verifyTheFunctionalityOfAddAddressLink() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(VerifyTheFunctionalityOfAddAddressLink.verifyTheFunctionalityOfAddAddressLink());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC97_verifyTheContentsOfVerifyAddressDialog() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC97_verifyTheContentsOfVerifyAddressDialog() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyTheContentsOfVerifyAddressDialog.shippingDetails());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC98_preferredAdressFunctionality() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC98_preferredAdressFunctionality() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.loginPrefferedAddress());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(PreferredAdress.preferredAdressFunctionality());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC99_verifyErrorMsgForAddingNewCreditCard() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC99_verifyErrorMsgForAddingNewCreditCard() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyErrorMsgForAddingNewCreditCard.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC100_verifyEditFunctionalityForHomeAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC100_verifyEditFunctionalityForHomeAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyEditFunctionalityForHomeAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityForHomeAddress.editHomeAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC101_verifyEditFunctionalityForFedexeAddress() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC101_verifyEditFunctionalityForFedexeAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyEditFunctionalityForFedExAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityForFedExAddress.editFedexAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC102_verifyBehaviourOFContinueButtonInRecipientPage() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC102_verifyBehaviourOFContinueButtonInRecipientPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyBehaviourOFContinueButtonInRecipientPage.verifyBehaviourOFContinueButtonInRecipientPage());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC103_verifyTheGiftOptionSummaryRecipientSection() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC103_verifyTheGiftOptionSummaryRecipientSection() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheGiftOptionSummaryRecipientSection.verifyTheGiftOptionSummaryRecipientSection());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC104_newArrivalAlertsInUserProfile() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC104_newArrivalAlertsInUserProfile() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(NewArrivalAlertsInUserProfile.newArrivalAlertsInUserProfile());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC105_updateShippingAddressInAddressBook() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC105_updateShippingAddressInAddressBook() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UpdateShippingAddressInAddressBook.login());
			objStatus += String.valueOf(UpdateShippingAddressInAddressBook.updateShippingAddressInAddressBook());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC106_productsInsaveForLaterNtStayedBackShipToSateChange() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC106_productsInsaveForLaterNtStayedBackShipToSateChange() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ProductsInsaveForLaterNotStayedBackShipToSateChange.productsInsaveForLate());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC106_productsInsaveForLaterNtStayedBackShipToSateChange() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC107_verifyStewardshipUpsellWhenCartIsEmpty() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(StewardshipUpsellWhenCartIsEmpty.stewardshipUpshellCartIsEmpty());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC108_stewardshipUpsellInFinalReviewPAgeForStandardShipping() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC108_stewardshipUpsellInFinalReviewPAgeForStandardShipping() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(StewardshipUpsellInFinalReviewPAgeForStandardShipping.shippingDetails());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			// objStatus +=
			// String.valueOf(OrderCreationWithShippingMethodForNewUser.addNewCreditCard(driver));
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC109_itemsNtEligibleForPromoCodeRedemption() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC109_itemsNtEligibleForPromoCodeRedemption() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ItemsNotEligibleForPromoCodeRedemption.itemsNotEligibleForPromoCodeRedemption());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC110_verifyUserIsNavigatedToAppropriateTextUrl() Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC110_verifyUserIsNavigatedToAppropriateTextUrl() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyUserIsNavigatedToAppropriateTextUrl.verifyUserIsNavigatedToAppropriateTextUrl());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC111_verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC111_verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser(driver));
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping());
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC112_verifyErrorMsgInvalidZipCodeForFedexeAddress()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC112_verifyErrorMsgInvalidZipCodeForFedexeAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyErrorMsgInvalidZipCodeForFedexeAddress.invalidZipCodeInFedexAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC113_verifyProductsAreListedInAlphabeticalOrder()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC113_verifyProductsAreListedInAlphabeticalOrder() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphabeticalOrder.verifyProductsAreListedInAlphabeticalOrder());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
            } catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC114_verifyErrorMessageInTheLoginComponent()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC114_verifyErrorMessageInTheLoginComponent() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyErrorMsgInLoginComponent.verifyErrorMsgInLoginComponent());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC115_updateAccountInfoWithOutDate()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC115_updateAccountInfoWithOutDate() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
//            objStatus += String.valueOf(UpdateAccountInfoWithOutDate.login());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(UpdateAccountInfoWithOutDate.updateAccountInfoWithOutDate());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC116_verifyTheSocialMediaLinks()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC116_verifyTheSocialMediaLinks() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTheSocialMediaLinks.verifyTheSocialMediaLinks());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC117_aboutProffessionalandProductAttributeDescriptionInPIP()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC117_aboutProffessionalandProductAttributeDescriptionInPIP() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AboutProffessionalandProductAttributeDescriptionInPIP.aboutProffessionalandProductAttributeDescriptionInPIP());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC118_errorMessageDisplayedForTheUserBelow21Years()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC118_errorMessageDisplayedForTheUserBelow21Years() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(ErrorMessageDisplayedForTheUserBelow21Years.errorMessageDisplayedForTheUserBelow21Years());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC119_verifyTheElementsDisplayedOverTheHeroImage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC119_verifyTheElementsDisplayedOverTheHeroImage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTheElementsDisplayedOverTheHeroImage.verifyTheElementsDisplayedOverTheHeroImage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC120_stewardshipAutoRenewalOption()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC120_stewardshipAutoRenewalOption() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyStewardshipDiscount.stewardLogin());
			objStatus += String.valueOf(StewardshipAutoRenewalOption.stewardshipAutoRenewalOption());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC121_verifyLinksDispalyedInFooter()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC121_verifyLinksDispalyedInFooter() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyLinksDispalyedInFooter.verifyLinksDispalyedInFooter());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC122_newAlertSectionForCurrentlyUnavailable()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC122_newAlertSectionForCurrentlyUnavailable() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(NewAlertSectionForCurrentlyUnavailable.newAlertSectionForCurrentlyUnavailable());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC123_promoCodesThatDntMeetMinimumAmount()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC123_promoCodesThatDntMeetMinimumAmount() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.addprodTocrt());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.enterPromoCode());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC124_giftMessageRemainingCharacterCountdown()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC124_giftMessageRemainingCharacterCountdown() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
        	objStatus += String.valueOf(GiftMessageRemainingCharacterCountdown.editRecipientEnterGiftMessage());
        	 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC125_productAttributeDescriptionInListPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC125_productAttributeDescriptionInListPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ProductAttributeDescriptionInListPage.productAttributeDescriptionInListPage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC126_viewAllWineForDifferentStates()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC126_viewAllWineForDifferentStates() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ViewAllWineForDifferentStates.viewAllWineForDifferentStates());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC127_superscriptInProductPrice()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC127_superscriptInProductPrice() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(SuperscriptInProductPrice.superscriptInProductPrice());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC128_ABVandFoundLowerPrice()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC128_ABVandFoundLowerPrice() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ABVandFoundLowerPrice.aBVandFoundLowerPrice());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC129_collapsedDeliverySectionWhenThePreSaleItemIsAdded()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC129_collapsedDeliverySectionWhenThePreSaleItemIsAdded() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(CollapsedDeliverySectionWhenThePreSaleItemIsAdded.addPreSaleprodTocrt());
			objStatus += String.valueOf(CollapsedDeliverySectionWhenThePreSaleItemIsAdded.verifyDeliverySection());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC130_removeCreditCard()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC130_removeCreditCard() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemoveCreditCard.removeCreditCard());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC131_promoBannerBehaviourForDryState()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC131_promoBannerBehaviourForDryState() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(PromoBannerBehaviourForDryState.promoBannerBehaviourForDryState());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC132_sortOrderInNewArrivalAlerts()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC132_sortOrderInNewArrivalAlerts() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(SortOrderInNewArrivalAlerts.sortedArrivalLogin());
			objStatus += String.valueOf(SortOrderInNewArrivalAlerts.verifyTheSortingInNewArrival());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC133_fedexLocationForUPSStatesInRecipientSection()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC133_fedexLocationForUPSStatesInRecipientSection() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(FedexLocationForUPSStatesInRecipientSection.fedexLocationForUPSStatesInRecipientSection());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC134_creditCardInfoIsUpdatedInPaymentMethods()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC134_creditCardInfoIsUpdatedInPaymentMethods() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(CreditCardInfoIsUpdatedInPaymentMethods.addPaymentMethod());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC135_showOutOfStocksAndRecommenderProdutcs()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC135_showOutOfStocksAndRecommenderProdutcs() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ShowOutOfStocksAndRecommenderProdutcs.showOutOfStocksAndRecommenderProdutcs());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC136_verifyTheAddressInRecipientPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC136_verifyTheAddressInRecipientPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.verifyTheAddressInRecipientPage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC137_VerifyContinueButtonIfAddressNtSelectedInRecipientPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC137_VerifyContinueButtonIfAddressNtSelectedInRecipientPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC136_verifyTheAddressInRecipientPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC138_verifyErrorDisplayedForInvalidProductId() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String
					.valueOf(VerifyErrorDisplayedForInvalidProductId.verifyErrorDisplayedForInvalidProductId());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC139_filterIsHighlightedInBlueInListPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC139_filterIsHighlightedInBlueInListPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(FilterIsHighlightedInBlueInListPage.filterIsHighlightedInBlueInListPage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC140_verifyTheAddressGetsDeletedPermanently()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC140_verifyTheAddressGetsDeletedPermanently() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheAddressGetsDeletedPermanently.shippingDetails());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC141_addressGetsSavedOnceOnAddingNewAddressInUserProfile()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC141_addressGetsSavedOnceOnAddingNewAddressInUserProfile() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.login());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.addAddressInAddressBook());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC142_deletePreferredCreditCardUserProfile()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC142_deletePreferredCreditCardUserProfile() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(DeletePreferredCreditCardUserProfile.addPaymentMethod());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC143_removePreferredCreditCardDuringCheckout()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC143_removePreferredCreditCardDuringCheckout() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemovePreferredCreditCardDuringCheckout.shippingDetails());
			objStatus += String.valueOf(RemovePreferredCreditCardDuringCheckout.addNewCreditCard());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC144_calenderDispalyedSixMonth()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC144_calenderDispalyedSixMonth() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(CalenderDispalyedSixMonth.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.calenderSixMnthInDeliverySection());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC145_verifyShipToTthisAddressSelectionChanged()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC145_verifyShipToTthisAddressSelectionChanged() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyShipToTthisAddressSelectionChanged.loginPrefferedAddress());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShipToTthisAddressSelectionChanged.preferredAdresSelChanged());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC146_vintageRefinementsAreOrderedInChroNlogicalOrder()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC146_vintageRefinementsAreOrderedInChroNlogicalOrder() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VintageRefinementsAreOrderedInChronologicalOrder.vintageRefinementProduct());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC147_enteredGiftCardGiftCertificateSection()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC147_enteredGiftCardGiftCertificateSection() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(EnteredGiftCardGiftCertificateSection.enterGiftCertificate());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC148_verifyNtMoreThanTwoGiftCardreedemed()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC148_verifyNtMoreThanTwoGiftCardreedemed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyNtMoreThanTwoGiftCardreedemed.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyNtMoreThanTwoGiftCardreedemed.verifyNtMoreThanTwoGiftCardreedemed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC149_userSelectsToPayWithDifferentCard()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC149_userSelectsToPayWithDifferentCard() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UserSelectsToPayWithDifferentCard.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(UserSelectsToPayWithDifferentCard.userSelectsToPayWithDifferentCard());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC150_verifySortFunctionalitiesUnderMyWine()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC150_verifySortFunctionalitiesUnderMyWine() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.addMyWineProdTocrt());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC151_wireUpCalender()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC151_wireUpCalender() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(CalenderDispalyedSixMonth.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(WireUpCalender.wireUpCalender());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC152_multipleTrackingOrderNumber()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC152_multipleTrackingOrderNumber() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(MultipleTrackingOrderNumber.login());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.addprodTocrt());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.multipleTrackingOrderNumber());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/***************************************************************************
	 * Method Name : TC153_creditInAccountSectionFullCredit()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC153_creditInAccountSectionFullCredit() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyNtMoreThanTwoGiftCardreedemed.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyPinkBarDisplayed.verifyPinkBarDisplayed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC154_creditInAccountSectionRemainingCredit()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC154_creditInAccountSectionRemainingCredit() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyNtMoreThanTwoGiftCardreedemed.login());
			objStatus += String.valueOf(CreditInAccountSectionRemainingCredit.addProductToCart());
			objStatus += String.valueOf(CreditInAccountSectionRemainingCredit.enterGiftCertificate());
			objStatus += String.valueOf(CreditInAccountSectionRemainingCredit.checkoutProcess());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC155_giftBagIsDisplayedAsLineItemInShoppingCartPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC155_giftBagIsDisplayedAsLineItemInShoppingCartPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(GiftBagIsDisplayedAsLineItemInShoppingCartPage.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(GiftBagIsDisplayedAsLineItemInShoppingCartPage.addGiftBag());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC156_createNewAccountWhileRatingTheProduct()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC156_createNewAccountWhileRatingTheProduct() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(CreateNewAccountWhileRatingTheProduct.createNewAccountWhileRatingTheProduct());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC157_existingCustomerLinkInNewCustomerRegisterPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC157_existingCustomerLinkInNewCustomerRegisterPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ExistingCustomerLinkInNewCustomerRegisterPage.existingCustomerLinkInNewCustomerRegisterPage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC158_verifyRatingOptionIsDisplayedInSliderBar()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC158_verifyRatingOptionIsDisplayedInSliderBar() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyRatingOptionIsDisplayedInSliderBar.verifyRatingOptionIsDisplayedInSliderBar());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC159_continueWithFacebook()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC159_continueWithFacebook() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ContinueWithFacebook.continueWithFacebook());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC160_verifyPromoBarOfWashingtonRegion()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 *//*
	@Test 
	public static void TC160_verifyPromoBarOfWashingtonRegion() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyPromoBarOfWashingtonRegion.verifyPromoBarOfWashingtonRegion());
             endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	

	/***************************************************************************
	 * Method Name : TC161_verifyTheShipsOnStatementInPIP()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC161_verifyTheShipsOnStatementInPIP() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTheShipsOnStatementInPIP.verifyTheShipsOnStatementInPIP());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC162_modalLightBoxWithXIcon()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC162_modalLightBoxWithXIcon() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
			objStatus += String.valueOf(ModalLightBoxWithXIcon.modalLightBoxWithXIcon());
			objStatus += String.valueOf(ModalLightBoxWithXIcon.lightBoxInRecipientPage());  
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC163_verifyRecommendedProductsOnRating()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC163_verifyRecommendedProductsOnRating() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(VerifyRecommendedProductsOnRating.verifyRecommendedProductsOnRating());   
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC164_limitReached()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC164_limitReached() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(LimitReached.limitReached());   
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC165_alphabeticalOrderMovingFromSaveForLaterToCart()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC165_alphabeticalOrderMovingFromSaveForLaterToCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AlphabeticalOrderMovingFromSaveForLaterToCart.alphabeticalOrderMovingFromSaveForLaterToCart());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphabeticalOrder.verifyProductsAreListedInAlphabeticalOrder());   
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC166_ratingStarsDisplayedProperlyInMyWine()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC166_ratingStarsDisplayedProperlyInMyWine() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(RatingStarsDisplayedProperlyInMyWine.login());
			objStatus += String.valueOf(RatingStarsDisplayedProperlyInMyWine.ratingStarsDisplayedProperlyInMyWine());   
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC167_verifyTheFourAddressAndCreditcards()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC167_verifyTheFourAddressAndCreditcards() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheFourAddressAndCreditcards.verifyTheFourAddressAndCreditcards()); 
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC168_loginUsingFacebook()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC168_loginUsingFacebook() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(LoginUsingFacebook.loginUsingFacebook());  
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC169_DoBFormatInUserProfile()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC169_DoBFormatInUserProfile() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(DoBFormatInUserProfile.doBFormatInUserProfile());  
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC170_verifyMilitaryOfficeBillingAddress()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC170_verifyMilitaryOfficeBillingAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyMilitaryOfficeBillingAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyMilitaryOfficeBillingAddress.addNewCreditCard());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC171_verifyShipToDateUpdated()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC171_verifyShipToDateUpdated() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyShipToDateUpdated.verifyShipToDateUpdated());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC172_verifyOrderOfProductsIconInPIP()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC172_verifyOrderOfProductsIconInPIP() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyOrderOfProductsIconInPIP.verifyOrderOfProductsIconInPIP());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC173_shipSoonCheckBox()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC173_shipSoonCheckBox() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(ShipSoonCheckbox.shipSoonCheckbox());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC174_verifyLocalPickupInformationDisplayedInCollapsedDeliverySection()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC174_verifyLocalPickupInformationDisplayedInCollapsedDeliverySection() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection.addFedExAddress());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC175_verifyFedexDisplaysProperGoogleAdresses()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC175_verifyFedexDisplaysProperGoogleAdresses() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyFedexDisplaysProperGoogleAdresses.verifyFedexAddressInGoogleMaps());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC176_verifyBillingPhoneNumberIsRequiredField()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC176_verifyBillingPhoneNumberIsRequiredField() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.loginR());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyBillingPhoneNumberIsRequiredField.addnewcreditCardWithoutPhoneNumberInPaymentSection());
			objStatus += String.valueOf(VerifyBillingPhoneNumberIsRequiredField.addnewcreditCardWithoutPhoneNumberInPaymentMethod());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC177_verifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC177_verifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated.verifyAddressSuggestioIsDisplayed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC178_verifyAboutProfRatingsHeaderisDispInProf()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC178_verifyAboutProfRatingsHeaderisDispInProf() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyAboutProfRatingsHeaderisDispInProf.verifyprofessionalRating());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC179_passwordExceptsMoreThan15Char()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC179_passwordExceptsMoreThan15Char() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(PasswordExceptsMoreThan15Char.verifyPasswordExceptsMoreThan15Char());
			objStatus += String.valueOf(PasswordExceptsMoreThan15Char.loginusingSamepassword());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC180_verifyDiffGiftBagTot()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC180_verifyDiffGiftBagTot() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyDiffGiftBagTot.verifyDiffGiftBagTotInOrderSummary());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC181_verifyAddToMyWineFunctionality()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC181_verifyAddToMyWineFunctionality() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionality());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC182_verifyProductsAreRemovedInMyWine()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC182_verifyProductsAreRemovedInMyWine() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyProductsAreRemovedInMyWine.login());
			objStatus += String.valueOf(VerifyProductsAreRemovedInMyWine.verifyProductsAreRemovedInMyWine());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC183_verify30LocationsAreDispInFedexAddress()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC183_verify30LocationsAreDispInFedexAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.verify30LocationsAreDispInFedexAddress());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name : TC184_verifyDefaultGiftBagButtonIsUnchecked()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC184_verifyDefaultGiftBagButtonIsUnchecked() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(VerifyDefaultGiftBagButtonIsUnchecked.verifyNoGiftBagButtonIsUnchecked());
			objStatus += String.valueOf(VerifyDefaultGiftBagButtonIsUnchecked.verifyGiftbagName());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name : TC185_verifyMyWineSignInNEditLinkForGiftMessage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC185_verifyMyWineSignInNEditLinkForGiftMessage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyMyWineSignInNEditLinkForGiftMessage.signInModalForMyWinePage());
			objStatus += String.valueOf(VerifyMyWineSignInNEditLinkForGiftMessage.editOptionForGiftMessage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC186_verifyAddAntherTextAndCorporateGiftsLink()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC186_verifyAddAntherTextAndCorporateGiftsLink() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(VerifyAddAntherTextAndCorporateGiftsLink.verifyCorperateLinksInCustomerCareSection());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC187_verifyPromoBarIsDispInSignAndThankYouPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC187_verifyPromoBarIsDispInSignAndThankYouPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promoBarInSignPageandCreateAct());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promobarDispInThankYouPage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC188_verifyShippingCostAppliedToOrderSummary()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC188_verifyShippingCostAppliedToOrderSummary() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingCostAppliedToOrderSummary.shippingCostAppliedToOrderSummary());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC189_VerifyShippingAndHandlingQuestionMarkIconIsDisplayed()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC189_VerifyShippingAndHandlingQuestionMarkIconIsDisplayed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingAndHandlingQuestionMarkIconIsDisplayed.ShippingAndHandlingQuestionMarkIconIsDisplayedInCart());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC190_VerifyPickUpFormIsNtDispForDiffBillingAddr()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC190_VerifyPickUpFormIsNtDispForDiffBillingAddr() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyPickUpFormIsNtDispForDiffBillingAddr.login());
			objStatus += String.valueOf(VerifyPickUpFormIsNtDispForDiffBillingAddr.searchProductWithProdName());
			objStatus += String.valueOf(VerifyPickUpFormIsNtDispForDiffBillingAddr.verificationOfPickupInformationForm());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC191_VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC191_VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.addingNewCardInPaymentSection());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.searchProductWithProdName());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.verifyDOBFiledPresent());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC192_VerifyVareitalPlusRegionIsDisplayedInHeroImage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC192_VerifyVareitalPlusRegionIsDisplayedInHeroImage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyVareitalPlusRegionIsDisplayedInHeroImage.varietalPlusResgionInHeroImage());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/***************************************************************************
	 * Method Name : TC193_VerifyAddToCartAlertIsDispWhenProdIsAdded()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC193_VerifyAddToCartAlertIsDispWhenProdIsAdded() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInHomePage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInListPage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInPiptPage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInMyWinePage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/***************************************************************************
	 * Method Name : TC194_VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC194_VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip.recomendedProdDisplayedForOutOfStock());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/***************************************************************************
	 * Method Name : TC195_VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC195_VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages.varietalNRegionLinkInPip());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	/***************************************************************************
	 * Method Name : TC196_VerifyGiftCardErrorMessageForInvalidCodes()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC196_VerifyGiftCardErrorMessageForInvalidCodes() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyGiftCardErrorMessageForInvalidCodes.giftCodeErrorMessage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC197_VerifyNewTagTitlesForListPages()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC197_VerifyNewTagTitlesForListPages() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyNewTagTitlesForListPages.verifyNewTitleForListpages());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC198_VerifyWetStateandPromoBannerinHomePage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC198_VerifyWetStateandPromoBannerinHomePage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyPromoBannerinHomePage.VerifypromoBanner());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC199_VerifyAddressfromAddresBooktoRecipientPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC199_VerifyAddressfromAddresBooktoRecipientPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddAddressUsingUserProfileServicesForExistingUser.addAddressInAddressBook());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyAddressInRecipientPage.VerifyRecipientAddress());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC200_VerifyStewardshipMessageNtDisplayedinCartPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC200_VerifyStewardshipMessageNtDisplayedinCartPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyStewardshipMember.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipMember.verifyStewardshipMessageNotDisplayed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC201_VerifySaveForLaterOnClickingContinueShipToKY()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC201_VerifySaveForLaterOnClickingContinueShipToKY() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(SaveForLater.verifySaveForLaterOnClickingShipToKY());	
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC202_VerifyProductMovedToCartFromSaveForLaterOnClickingMoveToCart()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC202_VerifyProductMovedToCartFromSaveForLaterOnClickingMoveToCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());		
			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.specialProducts());			
			objStatus += String.valueOf(SaveForLater.VerifyProductMovedToCartFromSaveForLater());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC203_verifyCustomerShipstateRevertedToPreviousStateInsideShoppingCart()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC203_verifyCustomerShipstateRevertedToPreviousStateInsideShoppingCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
			objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateInSideShoppingCart());				
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC204_verifyCustomerShipstateRevertedToPreviousStateOutsideShoppingCart()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC204_verifyCustomerShipstateRevertedToPreviousStateOutsideShoppingCart() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateOutSideShoppingCart());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC205_verifyEditFunctionalityofPreferredAddress()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC205_verifyEditFunctionalityofPreferredAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityofShippingAddress.preferredAddressEdit());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC206_verifyCustomerCareLinkinHeader()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC206_verifyCustomerCareLinkinHeader() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
        	objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyCustomerCareHeader());
        	 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC207_verifyLinkinFooter()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC207_verifyLinkinFooter() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyLinksinFooter());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC208_VerifyStewardshipStdShiping()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC208_VerifyStewardshipStdShiping() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(VerifyStewardshipMember.VerifyDefaultstewardshipStdShipping());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC209_reportingAddressPopUpforNewPickUpLocation()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC209_reportingAddressPopUpforNewPickUpLocation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingaddress.verifyHomePickUpLocationAddress());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC210_VerifyUserNtabletoSelectSaturdayForShippinginAlaska()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC210_VerifyUserNtabletoSelectSaturdayForShippinginAlaska() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.loginOtherState("StateAK"));
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.AlaskaStateSaturdayCalenderSelect());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC211_VerifyUserNtabletoSelectSaturdayForShippinginHawaii()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC211_VerifyUserNtabletoSelectSaturdayForShippinginHawaii() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.loginOtherState("StateHI"));
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.HawaiiStateSaturdayCalenderSelect());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC212_verifyAddToMyWineFunctionalityinPIP()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC212_verifyAddToMyWineFunctionalityinPIP() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionalityinPIP());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC213_verifyAddToMyWineFunctionalityinPIPforrating()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC213_verifyAddToMyWineFunctionalityinPIPforrating() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionalityinPIPforRating());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC214_VerifySiginModelforMyWineinListandPIP()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC214_VerifySiginModelforMyWineinListandPIP() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifySiginModelforMyWineinListandPIP.signInModalForMyWinePageinListandPIP());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC215_orderHistoryProgressBarValidation()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC215_orderHistoryProgressBarValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyOrderHistoryPage.OrderHistoryProcessBarValidation());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC216_OrderHistoryShippingAmountValidation()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC216_OrderHistoryShippingAmountValidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(VerifyOrderHistoryPage.OrderHistoryShippingAmountValidation());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC217_VerifyCopyrightsinFooter()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC217_VerifyCopyrightsinFooter() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyCopyrightsinFooter());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC218_VerifyAddToCartButtonDisplayedForRecommendedProducts()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC218_VerifyAddToCartButtonDisplayedForRecommendedProducts() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddToCartForRecomondedProducts.verifyAddToCartButtonForRecommendedProducts());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name : TC219_VerifyRecommendedProductsAfterRatingTheStars()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC219_VerifyRecommendedProductsAfterRatingTheStars() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddToCartForRecomondedProducts.rateTheStarsAndVerifyRecommendedProducts());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC220_VerifyStewardship()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC220_VerifyStewardship() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyStewardshipMember.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipMember.verifyStewardshipMessageNotDisplayed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC221_verifyTheBehaviorOfTheContinueButtonIfAddressNtSelectedInRecipientPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC221_verifyTheBehaviorOfTheContinueButtonIfAddressNtSelectedInRecipientPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC222_verifyTheBillingAddressCheckboxIsCheckedByDefault()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC222_verifyTheBillingAddressCheckboxIsCheckedByDefault() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.checkBoxSelectedDefault());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name : TC223_verifyBillingAddressDisplayedOnUncheckingTheBillingSameAsShipping()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC223_verifyBillingAddressDisplayedOnUncheckingTheBillingSameAsShipping() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
            objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.unCheckBoxSelected());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC224_verifyAddressGetsDeletPermanentlyFromTheShippingAddress()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC224_verifyAddressGetsDeletPermanentlyFromTheShippingAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.login());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.removeAddressInAddressBook());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC225_verifyContentInTheOrderPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC225_verifyContentInTheOrderPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
			objStatus += String.valueOf(VerifyTheContentsInTheOrderPage.verifyTheContentOntheOrderPage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC226_verifyShippinAndHandalingChargesAppliedToTheNnStewardshipUser()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC226_verifyShippinAndHandalingChargesAppliedToTheNnStewardshipUser() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.verifyShippingAndHandalingChargesAreAppliedToTheNonStewardshipUser());
			objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.addNewCreditCard());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC227_stewardUpsellshouldMatchShippingAndHandalingCharges()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC227_stewardUpsellshouldMatchShippingAndHandalingCharges() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(StewardshipUpsellInFinalReviewPAgeForStandardShipping.shippingDetails());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC228_verifyGoBackButtonIsDispInFedexWindow()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC228_verifyGoBackButtonIsDispInFedexWindow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyFedexDisplaysProperGoogleAdresses.fedexLocationGoBackButtonDisplayed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC229_validatePromoCodeAppliedMessageDisplayed()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC229_validatePromoCodeAppliedMessageDisplayed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.validatePromoCodeMessageDisplayed());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC230_verifyTheRareProductAttributeIconDisplayed()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC230_verifyTheRareProductAttributeIconDisplayed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(SpiritsRareProducts.validateRareProductIcon());	
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC231_verifyPromoCodeAndGiftCardErrorMassage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC231_verifyPromoCodeAndGiftCardErrorMassage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
           	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyInvalidPromoCodeAndGiftCard.invalidPromoCode());
			objStatus += String.valueOf(VerifyInvalidPromoCodeAndGiftCard.invalidGiftCardValidation());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC232_verifyCorporateGiftLinkDisplayedUnderCustomerCareSection()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC232_verifyCorporateGiftLinkDisplayedUnderCustomerCareSection() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.Desktoplogin());
            objStatus += String.valueOf(VerifyAddAntherTextAndCorporateGiftsLink.verifyCorperateLinksInCustomerCareSection());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC233_verifyPromoBarIsDispInSignAndThankYouPage()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC233_verifyPromoBarIsDispInSignAndThankYouPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promoBarInSignPageandCreateAct());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promobarDispInThankYouPage());
			 endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC234_VerifyGftCardBalDispIn_OrdrSummaryUponRedeem()
	 *  Created By : Ramesh S
	 *  Reviewed By: 
	 *  Purpose :
	 ****************************************************************************
	 */
	@Test 
	public static void TC234_VerifyGftCardBalDispIn_OrdrSummaryUponRedeem() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.addProductToCartLessThnGftCrdPrice());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.verifyGftCardBalDispInOrdrSummaryUponRedeem());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***********************************************************************************************
	 * Method Name :  VerifyGftCrdAccIsDispInPymntOpt() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4603
	 ************************************************************************************************
	 */
	@Test 
	public static void TC235_VerifyGftCrdAccIsDispInPymntOpt() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCrdAccIsDispInPymntOpt.verifyGftCrdAccAndBalDispInPaymntOpt());		
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***********************************************************************************************
	 * Method Name :  VerifyGftCrdSucessMsgDispAftrGftCrdApplied() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4605
	 ************************************************************************************************
	 */
	@Test 
	public static void TC236_VerifyGftCrdSucessMsgDispAftrGftCrdApplied() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCrdSucessMsgDispAftrGftCrdApplied.verifyGiftCardAppliedSuccessMsg());			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyTheRecipientSectionUIUnderSubscriptionFlow() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4667,TM-4662
	 ************************************************************************************************
	 */
	@Test 
	public static void TC237_VerifyTheRecipientSectionUIUnderSubscriptionFlow() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
        	objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
            objStatus += String.valueOf(VerifySignUpFunctionalitiesFlowNUI.verifyUserIsAbleToEnrollForSubscription());
		 	objStatus += String.valueOf(VerifyTheRecipientSectionUIUnderSubscriptionFlow.verifyRecipientSectionAfterSubscriptionFlow());	
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  ValidateTheSubscriptionFlowForLocalPickupAddress() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4673
	 ************************************************************************************************
	 */
	@Test 
	public static void TC238_ValidateTheSubscriptionFlowForLocalPickupAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlowNUI.verifyUserIsAbleToEnrollForSubscription());
		    objStatus += String.valueOf(ValidateTheSubscriptionFlowForLocalPickupAddress.verifyLocalPickUpFlowForPickedUpWine());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4669
	 ************************************************************************************************
	 */
	@Test 
	public static void TC239_VerifySubscriptionSettingsSummIsDispInCheckoutNRecipientPaymnt() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(PickedWineGetStarted.enrollForPickedUpWine());
		    objStatus += String.valueOf(VerifySubscriptionSettingsSummIsDispInCheckoutNRecipientPaymnt.verifySubscriptionSettingsInChckout());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4610
	 ************************************************************************************************
	 */
	@Test 
	public static void TC240_VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection() {	
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.addProductToCartLessThnGftCrdPrice());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection.verifyZeroBalGftCrdIsRemovedInPymntOpt());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  ValidateTheSubscriptionFlowForHomeAddress() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  Chandrashekar
	 * Purpose     :  TM-4675
	 ************************************************************************************************
	 */
	@Test 
	public static void TC241_ValidateTheSubscriptionFlowForHomeAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(PickedWineGetStarted.enrollForPickedUpWine());
		    objStatus += String.valueOf(ValidateTheSubscriptionFlowForHomeAddress.verifyHomeAddrFlowForPickedUpWine());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  Chandrashekar
	 * Purpose     :  TM-4666,TM-4665
	 ************************************************************************************************
	 */
	@Test 
	public static void TC242_VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(PickedWineGetStarted.enrollForPickedUpWine());
		    objStatus += String.valueOf(VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk.verifyDetailedTermsDispOnClickingTermsLnk());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyLikedandDisLikedRedWhiteWineinAnythingElseSectionAndTotalPricewithQtyvalidation() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4663, TM-4664
	 ************************************************************************************************
	 */
	@Test 
	public static void TC243_VerifyLikedandDisLikedRedWhiteWineinAnythingElseSectionAndTotalPricewithQtyvalidation() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(ValidateRedWhiteVarietalPreffinAnythingElseSec.VerifyLikeandDislikeinAnythingElseSec());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  ValidateCompassEnrollmentFlowforWhiteWine() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4657
	 ************************************************************************************************
	 */
	@Test 
	public static void TC244_ValidateCompassEnrollmentFlowforWhiteWine() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforWhiteWine.verifyUserIsAbleToEnrollForWhiteWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforWhiteWine.verifyLocalPickUpForPickedUpWine());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  ValidateCompassEnrollmentFlowforRedWine() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4656
	 ************************************************************************************************
	 */
	@Test 
	public static void TC245_ValidateCompassEnrollmentFlowforRedWine() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(ValidateEnrollmentFlowforRedWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateEnrollmentFlowforRedWine.verifyLocalPickUpForPickedUpWine());	    
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  ValidateCompassEnrollmentFlowforBothRedandWhiteWineWithSubscriptionSettingsandPymtMethodEdit() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4658
	 ************************************************************************************************
	 */
	@Test 
	public static void TC246_ValidateCompassEnrollmentFlowforBothRedandWhiteWineWithSubscriptionSettingsandPymtMethodEdit() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.EditSubscriptioninPickedSettings());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.EditPaymentMethodinPickedSettings());		   
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyChangeDelvyDateandRecipientAddressinPickedSettingpage() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4684, TM-4689 
	 ************************************************************************************************
	 */
	@Test 
	public static void TC247_VerifyChangeDelvyDateandRecipientAddressinPickedSettingpage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.ChangedDeliveryDateinPickedSettings());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.ChangeRecipientAddressinPickedSettings());			
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyMySommPageDisplayedthroughPickedSettingNav() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4677
	 ************************************************************************************************
	 */
	@Test 
	public static void TC248_VerifyMySommPageDisplayedthroughPickedSettingNav() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyMySommPageDisplayedthroughNav.login());
			objStatus += String.valueOf(VerifyMySommPageDisplayedthroughNav.VerifyMySommPageDisplayedthroughNavigation());			
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***********************************************************************************************
	 * Method Name :  VerifyContentforYourSubscriptioninPickedSetting() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4687
	 ************************************************************************************************
	 */

	@Test 
	public static void TC249_VerifyContentforYourSubscriptioninPickedSetting() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
        	objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.RedWhiteWineSubscriptionflow());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.ValidateContentforYourSubscriptioninPickedSettings());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress() 
	 * Created By :  Ramesh S
	 * Reviewed By : 
	 * Jijra Id    : TM-4187
	 ****************************************************************************
	 */

	@Test 
	public static void TC250_VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus+=String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress.verifyTheUserAbleToAddGiftAddress());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyTheOfferPageIsDisplayed() 
	 * Created By :  Ramesh S
	 * Reviewed By : 
	 * Jijra Id    : TM-4678
	 ****************************************************************************
	 */
	@Test 
	public static void TC251_verifyTheOfferPageIsDisplayed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus+=String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyTheOfferPageIsDisplayedWhenNewUerClickOnUserProfile.offerPageDisplayed());		
           endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyTheElementsDisplayedInThankyouPage() 
	 * Created By :  Ramesh S
	 * Reviewed By : 
	 * Jijra Id    : TM-4671
	 ****************************************************************************
	 */
	@Test 
	public static void TC252_verifyTheElementsDisplayedInThankyouPage() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.offerPageDisplayed());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.shippingDetails());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.addNewCreditCard());			
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyStewardshipDiscountDisplayedNegetiveInHistorys() 
	 * Created By  : Ramesh S
	 * Reviewed By :  
	 * Purpose     :
	 * Jira id     : TM-4648
	 ****************************************************************************
	 */
	@Test 
	public static void TC253_verifyStewardshipDiscountDisplayedNegetiveInHistory() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.stewardLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.shippingDetails());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.addNewCreditCard());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyGiftCardAccountDisplayedInThePaymentOption() 
	 * Created By :  Ramesh S
	 * Reviewed By : 
	 * Jijra Id    : TM-4604
	 ****************************************************************************
	 */
	@Test 
	public static void TC254_verifyGiftCardAccountDisplayedInThePaymentOption() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus+=String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(verifyTwoGiftCardAccountDisplayedInPaymentOption.verifyTwoGiftCardReedemed());			
			objStatus += String.valueOf(verifyTwoGiftCardAccountDisplayedInPaymentOption.verifyTwoGiftCardAccountsDisplayed());		
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : verifyDeliveryViaEmailDisplayed() 
	 * Created By  : Ramesh S
	 * Reviewed By : 
	 * Purpose     : To Verify the 'Delivered via email' is displayed in delivery section.
	 * Jira id     : TM-4723
	 ****************************************************************************
	 */
	@Test 
	public static void TC255_verifyDeliveryViaEmailDisplayed() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.addGiftsToTheCart());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.shippingDetails());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.addNewCreditCard());	
           endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyCompassUserSubscription() 
	 * Created By  : Ramesh S
	 * Reviewed By : 
	 * Purpose     : To Verify the Compass user subscription.
	 * Jira id     : TM-4691,4744,4752
	 ****************************************************************************
	 */
	@Test 
	public static void TC256_verifyCompassUserSubscription() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(VerifyCompussUsersSubscriptions.login());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.verifySubscriptions());
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyGiftCardRedeemedDisplayedInOrderSummary () 
	 * Created By  : Ramesh S
	 * Reviewed By : Ramesh, 
	 * Purpose     :
	 * Jira id     : TM-4646
	 ****************************************************************************
	 */
	@Test 
	public static void TC257_verifyGiftCardRedeemedDisplayedInOrderSummary() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.verifyGiftCardAmountInOrderSummary());						
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}

	/***********************************************************************************************
	 * Method Name :  VerifyRescheduleSubscriptioninPickedSetting() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4787
	 ************************************************************************************************
	 */
	@Test 
	public static void TC258_VerifyRescheduleSubscriptioninPickedSetting() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.enrollForPickedUpWine());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.ChangedDeliveryDateinPickedSettings());			
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/***********************************************************************************************
	 * Method Name :  VerifyUserAbletoSetRedWhiteBottleCountto0or6() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4808
	 ************************************************************************************************
	 */
	@Test 
	public static void TC259_VerifyUserAbletoSetRedWhiteBottleCountto0or6() {		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyRedWhiteQtyAllowedto0or6withMultipleClasses.VerifyRedWhiteQtySetto0or6());		    
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	/***********************************************************************************************
	 * Method Name :  VerifyRedWhiteButtonPresentinPriceQtyWidgetPage() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4810
	 ************************************************************************************************
	 */
	@Test 
	public static void TC260_VerifyRedWhiteButtonPresentinPriceQtyWidgetPage() {	
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(RedWhiteButtonPresentinPriceQuantityWidget.FunctionalityRedorWhiteButtoninPriceQtyPage());	    
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		 
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
		
	/***********************************************************************************************
	 * Method Name :  VerifyLikeDislikeNotSelectedinVeritalpreferencePage() 
	 * Created By  :  Ramesh S
	 * Reviewed By :  
	 * Purpose     :  TM-4811
	 ************************************************************************************************
	 */
	@Test 
	public static void TC261_VerifyLikeDislikeNtSelectedinVeritalpreferencePage() {		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(VerifyLikeDislikeSectionNotDisplayedinVaritalPreference.LikeDisLikeNotSelectedinVarietalPage());           
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : verifyGiftCardRedeemedDisplayedInOrderSummary () 
	 * Created By  : Ramesh S
	 * Reviewed By :  
	 * Purpose     :
	 * Jira id     : TM-4646
	 ****************************************************************************/
	 
	@Test 
	public static void TC262_verifyGiftCardRedeemedDisplayedInOrderSummary() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());
            objStatus += String.valueOf(UIBusinessFlows.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.verifyGiftCardAmountInOrderSummary());								
            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : verifyGiftCardRedeemedNtDisplayedInTheOrderSummary () 
	 * Created By  : Ramesh S
	 * Reviewed By : 
	 * Purpose     :
	 * Jira id     : TM-4647
	 ****************************************************************************
	 */
	@Test 
	public static void TC263_verifyGiftCardRemainingIsNtDisplayedInTheOrderSummary() {		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());       
            objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary.verifyGiftCardAmountInOrderSummary());								
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : verifyCompassUserNtAbleToEnrollForDryState() 
	 * Created By  : Ramesh S
	 * Reviewed By : 
	 * Purpose     : 
	 * Jira id     : TM-4785
	 ****************************************************************************
	 */
	@Test 
	public static void TC264_verifyCompassUserNtAbleToEnrollForDryState() {		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());   
            objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.offerPageDisplayed());			
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.shippingDetails());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.addNewCreditCard());				
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : verifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup() 
	 * Created By :  Ramesh S
	 * Reviewed By : 
	 * Jijra Id    : TM-4786
	 ****************************************************************************
	 */
	
	@Test 
	public static void TC265_verifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());  
            objStatus += String.valueOf(VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup.offerPageDisplayed());
			objStatus += String.valueOf(VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup.shippingDetails());					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : verifyContentDisplayedUnderMySOMMSection() 
	 * Created By  : Ramesh S
	 * Reviewed By : 
	 * Purpose     : To Verify the Compass user subscription.
	 * Jira id     : TM-4681
	 ****************************************************************************
	 */
	@Test 
	public static void TC266_verifyContentDisplayedUnderMySOMMSection() {
		
		try {
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1];
            logger=report.createTest(Method); 
            System.out.println("Executing "+Method);
            objStatus += String.valueOf(Initialize.navigate());            
			objStatus += String.valueOf(VerifyContenDisplayedUnderMySOMMSection.login());        
			objStatus += String.valueOf(VerifyContenDisplayedUnderMySOMMSection.verifySubscriptions());				
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: Closebrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser.
	 ****************************************************************************
	 */
	@AfterMethod
	public static void Closebrowser()
	{ 
			try
		{
				if (objStatus.contains("Fail"))
				{
				 	numberOfTestCaseFailed++;
				 	ReportUtil.writeTestResults(testCaseID, methodName, "Fail", startTime, endTime);
				}
				else
				{
					numberOfTestCasePassed++;
					ReportUtil.writeTestResults(testCaseID, methodName, "Pass", startTime, endTime);
				}
				log=Logger.getLogger("Close Browser ...");
				Initialize.closeApplication();
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: CloseBrowserAndEndReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser and end the report.
	 * ****************************************************************************
	 */
	 @AfterTest
     public void EndReport()
     {
		 try {
		 
		 report.flush(); 
		 }catch(Exception e)
			{
				e.printStackTrace();
			}
           }

	/***************************************************************************
	 * Method Name			: endScenariosExecution()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: This method responsible for updating the end time to 
	 * 						  customized html report
	 ****************************************************************************
	 */
	
	@AfterSuite
	public static void endScenariosExecution()
	{
		String endTime=null;
		try
		{
			ReportUtil.endScript(numberOfTestCasePassed,numberOfTestCaseFailed);
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			ReportUtil.updateEndTime(endTime);
			UIFoundation.waitFor(3L);
			
			System.out.println("=======Desktop test case details==============");
			System.out.println("Total no test case Passed:"+numberOfTestCasePassed);
			System.out.println("Total no test case Failed:"+numberOfTestCaseFailed);
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
