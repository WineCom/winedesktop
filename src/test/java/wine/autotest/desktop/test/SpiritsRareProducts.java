package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class SpiritsRareProducts extends Desktop {
	
	/***************************************************************************
	 * Method Name			: forgotPassword()
	 * Created By			: Chandrashekha
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is to Validate the rare product attribute icon displayed.
	 * Jira Id         		: TM-4404				  
	 ****************************************************************************
	 */
	
	public static String validateRareProductIcon()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_validateRareProductIcon_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method validateRareProductIcon started here ...");
			
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkrareSpirits));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkmerlotFirstProd));		
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.rareProductAttribute));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.rareProductAttribute));
			log.info("The execution of the method validateRareProductIcon ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
				   String objDetail="'Rare' product attribute icon is not displayed";
				   ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("validateRareProductIcon test case is failed");
				return "Fail";
			}
			else
			{
				 objStatus+=true;
			     String objDetail="'Rare' product attribute icon is displayed successfully";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     logger.pass(objDetail);
				System.out.println("validateRareProductIcon test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method validateRareProductIcon "+ e);
			return "Fail";
			
		}
	}

}
