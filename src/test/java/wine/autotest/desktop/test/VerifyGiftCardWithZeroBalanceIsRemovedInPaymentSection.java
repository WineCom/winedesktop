package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.pages.UserProfilePage;


public class VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection extends Desktop {

	/***************************************************************************
	 * Method Name : verifyGftCrdAccAndBalDispInPaymntOpt() 
	 * Created By  : Rakesh C S
	 * Reviewed By : Chandrashekar 
	 * Purpose     : Verifies gift card applied is displayed in payment option
	 ****************************************************************************
	 */

	public static String verifyZeroBalGftCrdIsRemovedInPymntOpt() {

		String objStatus=null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_verifyGftCrdAccAndBalDispInPaymntOpt_Screenshot.jpeg";
		
		try {
		log.info("The execution of the verifyGiftCardAppliedSuccessMsg method strated here");
		if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
		{
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
			UIFoundation.waitFor(3L);
		}
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
		UIFoundation.waitFor(3L);
		WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
		if(!ele.isSelected())
		{
			UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			UIFoundation.waitFor(3L);
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
			objStatus+=true;
			String objDetail="DOB format is in'MM / DD / YYYY'.";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		}
		else
		{
			objStatus+=false;
			String objDetail="DOB is not in format 'MM / DD / YYYY'.";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+"dobFormat", objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
		objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		UIFoundation.waitFor(5L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(6L);
		UIFoundation.waitFor(2L);
		String giftCert1=UIFoundation.giftCertificateNumber();
		XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
		if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
		{
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
			UIFoundation.webDriverWaitForElement(CartPage.spngiftCertificateSuccessMsg, "element", "", 50);
		}
		else
		{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
			UIFoundation.webDriverWaitForElement(CartPage.spngiftCertificateSuccessMsg, "element", "", 50);
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtGiftCrdRemAmtZero)) {
			objStatus+=true;
			String objDetail="Gift card applied and zero remaining is displayed is order summary";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Invalid message for gift code  is displayed");
		}
		else
		{
			objStatus+=false;
			String objDetail="Gift card applied and zero remaing is not displayed is order summary";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal = UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling = UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		total = UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax = UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              " + subTotal);
		System.out.println("Shipping & Handling:   " + shippingAndHandling);
		System.out.println("Sales Tax:             " + salesTax);
		System.out.println("Total:                 " + total);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.waitFor(20L);
		  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
		            {
		                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
		                  UIFoundation.waitFor(5L);             
		            } 
		orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
		if (orderNum != "Fail") 
		{
			objStatus += true;
			String objDetail = "Order number is placed successfully";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Order number is placed successfully: " + orderNum);
		}
		else
		{
			objStatus += false;
			String objDetail = "Order number is null.Order not placed successfully";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			// ReportUtil.addTestStepsDetails("Order number is null.Order not placed
			// successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
		objStatus+=String.valueOf(UIFoundation.clckObject(LoginPage.PaymentMethods));
		UIFoundation.waitFor(1L);
		if(UIFoundation.isDisplayed(UserProfilePage.spnPaymentMethodsHeader)){
			objStatus+=true;
			ReportUtil.addTestStepsDetails("User is navigated to Credit card info ", "Pass", "");
			logger.pass("User is navigated to Credit card info ");
			System.out.println("Verify user is navigated to Credit card info test case is executed successfully");

		}
		else
		{
			objStatus+=false;
			String objDetail="Verify user is navigated to Credit card info  test case is failed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(!UIFoundation.isDisplayed(FinalReviewPage.txtGiftCardRemZero)){
			objStatus+=true;
			String objDetail="Gift card with ($0 Remaining) is removed from the Payment methods";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Gift card with ($0 Remaining) is removed from the Payment methods");
		}
		else
		{
			objStatus+=false;
			String objDetail="Gift card with ($0 Remaining) is not removed from the Payment methods";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			System.out.println("Gift card with ($0 Remaining) is not removed from the Payment methods");
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		log.info("The execution of the method verifyGiftCardAppliedSuccessMsg ended here ...");
		if (objStatus.contains("false"))
		{
			String objDetail="'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case Failed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			System.out.println("'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case Failed");
			return "Fail";
		}
		else
		{
			String objDetail="'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case excuted succesfully";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case excuted succesfully");
			return "Pass";
		}
	}catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method"+ e);
		return "Fail";
	}
}		

}