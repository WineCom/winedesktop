package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class InvalidEmailAddressInGiftRecipientEmail extends Desktop {
	
	/***************************************************************************
	 * Method Name : editRecipientPlaceORder() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String editRecipientPlaceORder() {
		String objStatus = null;
		 			
		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(18L);  
			String expectedEmailErrorMsg=verifyexpectedresult.InvalidEmailErrorMsgInGiftRecipientEmail;
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientShipToaddress));
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkRecipientGift);
			UIFoundation.waitFor(2L); 
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtReceipientEmail))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkRecipientGift));
				UIFoundation.clearField(FinalReviewPage.txtReceipientEmail);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "InValiRecipientEmail"));
				
				
			}else
			{
				UIFoundation.clearField(FinalReviewPage.txtReceipientEmail);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "InValiRecipientEmail"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
			String actualInValidEmailErrorMsg=UIFoundation.getText(FinalReviewPage.txtInvalidEmailAddInGiftRecipPage);
			if (expectedEmailErrorMsg.equalsIgnoreCase(actualInValidEmailErrorMsg)) {
				
				objStatus+=true;
				logger.pass("Verified warning message for shiptoday products and others products");
				ReportUtil.addTestStepsDetails("Verified warning message for shiptoday products and others products", "Pass", "");
			} else {
			
				objStatus+=false;
				String objDetail="Verified warning message for shiptoday products and others products test case is failed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot( screenshotpath+"InvalidEmailInGiftRecipient", objDetail);
				
			}
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}

}
