package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class MultipleTrackingOrderNumber extends Desktop {
	
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			
			
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "multipleOrderLogin"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(5L);
		
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkFeatured));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexFeatures));
			UIFoundation.waitFor(4L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkFirstProductToCart));
			}

		addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkSecondProductToCart));
			}
			
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkThirdProductToCart));
			}
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
		/***************************************************************************
		 * Method Name			: multipleTrackingOrderNumber()
		 * Created By			: Vishwanath Chavan
		 * Reviewed By			: Ramesh,KB
		 * Purpose				:
		 ****************************************************************************
		 */
		
		public static String multipleTrackingOrderNumber() {
	
		String objStatus=null;
		String orderNum=null;
		   String screenshotName = "Scenarios_OmultipleOrder_Screenshot.jpeg";
			
			try {
				log.info("The execution of the method checkoutProcess started here ...");
				UIFoundation.waitFor(4L);
				
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
					UIFoundation.waitFor(3L);	
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkRecipientShipToaddress);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientShipToaddress));
				if(UIFoundation.isDisplayed(FinalReviewPage.btnRecipientContinue))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
					UIFoundation.waitFor(3L);	
				}
							
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
				}
				if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
					objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
					//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
				}
	               if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
	            	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	       			UIFoundation.waitFor(2L);
	       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
	       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
	       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	       			UIFoundation.waitFor(3L);
					WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
					if(!ele.isSelected())
					{
				    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					}
				if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
				orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				int splitFirstOrderNumebr=Integer.parseInt(orderNum);
//				UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage.lnkOrderNumber);
				UIFoundation.waitFor(10L);
				objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
				UIFoundation.waitFor(15L);
				System.out.println("splitFirstOrderNumebr :"+splitFirstOrderNumebr);
				int splitSecOrderNumebr=splitFirstOrderNumebr;
	/*			objStatus+=String.valueOf(UIFoundation.mouseHover(driver, "accountBtn"));
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "OrdersHistory"));
				UIFoundation.waitFor(3L);*/
				//String xpathSecOrder="//span[text()='"+splitSecOrderNumebr+"']";
				WebElement splitSecondOrderNumebr=driver.findElement(By.xpath("//span[text()='"+splitSecOrderNumebr+"']"));
				if(splitSecondOrderNumebr.isDisplayed())
				{ objStatus+=true;
			      String objDetail="Multiple tracking numbers are displayed under order history";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      //System.out.println("Multiple tracking numbers are displayed under order history "+splitSecondOrderNumebr);   
				}else
				{
					objStatus+=false;
					String objDetail="Multiple tracking numbers are not displayed under order history";
					//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				log.info("The execution of the method checkoutProcess ended here ...");
				if ( objStatus.contains("false")) {
					System.out.println("Verify multiple tracking numbers are displayed under Tracking # column in the 'order History' grid for split orders test case is failed");
					return "Fail";
				} else {
					System.out.println("Verify multiple tracking numbers are displayed under Tracking # column in the 'order History' grid for split orders test case is executed successfully");
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method getOrderDetails "
						+ e);
				objStatus+=false;
				String objDetail="Order number is null.Order not placed successfully";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
		}

}
