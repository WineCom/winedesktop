package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class RemoveCreditCard extends Desktop {
	/***************************************************************************
	 * Method Name			: removeCreditCard()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String removeCreditCard() {

	String objStatus=null;
	   String screenshotName = "Scenarios__removeCreditCard.jpeg";
				
		try {
			log.info("The execution of the method removeCreditCard started here ...");
			UIFoundation.waitFor(4L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		//	driver.navigate().refresh();
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkRecipientShipToaddress);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientShipToaddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(5L);
			

			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkeditCreditCard));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnremoveThisCard));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnyesRemove));
			UIFoundation.waitFor(2L);
			String creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentCount);
			int creditCardCntBefore=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkeditCreditCard));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnremoveThisCard));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnyesRemove));
			UIFoundation.waitFor(2L);
			creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentCount);
			int creditCardCountAfter=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if(((creditCardCntBefore-1)==creditCardCountAfter))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("The credit card is removed from the payment list.", "Pass", "");
				logger.pass("The credit card is removed from the payment list.");
		
			}else
			{
				objStatus+=false;
				String objDetail="The credit card is not removed from the payment list";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method removeCreditCard ended here ...");
			if (objStatus.contains("false")) {
			
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method removeCreditCard "
					+ e);
			return "Fail";
		}
	}

}
