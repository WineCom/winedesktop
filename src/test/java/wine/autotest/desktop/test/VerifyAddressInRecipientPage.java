package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyAddressInRecipientPage extends Desktop{

	/***************************************************************************
	 * Method Name			: addAddress()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-442
	 ****************************************************************************
	 */
	
	public static String VerifyRecipientAddress() {
		String objStatus=null;
		String screenshotName = "Scenarios_VerifyRecipientAddress_Screenshot.jpeg";
				try {
				log.info("The execution of the method recipientAddressEdit started here ...");

				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				UIFoundation.waitFor(4L);
				if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)) {
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				}
//				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "shippingAddressEdit"));
				UIFoundation.waitFor(4L);
				if(UIFoundation.isDisplayed(FinalReviewPage.spnEditShipAddName)) {
					objStatus+=true;
					String objDetail = "The Address added in Addressbook displayed in Recipient Page";		
					System.out.println(objDetail);
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else {
					objStatus+=false;
					String objDetail = "The Address added in Addressbook is not displayed in Recipient Page";
					System.out.println(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					}
				log.info("The execution of the method VerifyRecipientAddress ended here ...");
				if (objStatus.contains("false")) {
					String objDetail = "Verify the Address Book functionality of shipping address test case is failed";
					System.out.println(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					return "Fail";
				} else {
					String objDetail = "Verify the Address Book functionality of shipping address test case is executed successfully";
					System.out.println(objDetail);
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method VerifyAddressInRecipientPage "
						+ e);
				return "Fail";
			}
		}
		
}
