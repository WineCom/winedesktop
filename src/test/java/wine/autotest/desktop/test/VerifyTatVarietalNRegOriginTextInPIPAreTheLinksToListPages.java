package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages extends Desktop {

	/***************************************************************************
	 * Method Name : varietal and region text
	 * (TM-4394) Created By :
	 *  Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */


	public static String varietalNRegionLinkInPip() {

		String objStatus=null;
		String screenshotName = "varietalNRegionLinkInPip.jpeg";
		
		try {
			log.info("varietal N Region Link In Pip method started here");
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkmerlotFirstProd));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.lnkvarietalOrigin)) 
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkvarietalOrigin));
			UIFoundation.waitFor(2L);
			String expVarietalUrl = driver.getCurrentUrl();
			String actVarietalUrl = verifyexpectedresult.orginvarietalUrl;
			if(expVarietalUrl.equals(actVarietalUrl)) {
				objStatus+=true;
				String objDetail = "Varietal origin link is displayed and user is navigated succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail = "Varietal origin link is not displayed  displayed and user is not navigated succesfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			driver.navigate().back();
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.lnkregionOrgigin)) 
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkregionOrgigin));
			UIFoundation.waitFor(2L);
			String expRegionUrl = driver.getCurrentUrl();
			String actRegionUrl = verifyexpectedresult.originRegionUrl;
			if(expRegionUrl.equals(actRegionUrl)) {
				objStatus+=true;
				String objDetail = "Region origin link is displayed and user is navigated succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail = "Region origin link is not displayed  displayed and user is not navigated succesfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(objStatus.contains("false")) {
				System.out.println("Verify Tat Varietal N Region origin Text In PIP Are The Links To ListPages test case failed");
				return "false";
			}
			else {
				System.out.println("Verify Tat Varietal N Region origin Text In PIP Are The Links To ListPages test case passed");
				return "true";
			}
		}catch(Exception e){
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
}