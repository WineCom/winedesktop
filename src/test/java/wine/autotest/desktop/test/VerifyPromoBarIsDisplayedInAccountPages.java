package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyPromoBarIsDisplayedInAccountPages extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyPromoBarIsDisplayedInAccountPages()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String verifyPromoBarIsDisplayedInAccountPages()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_VerifyProMoBarDis_Screenshot.jpeg";
			
		
		try
		{
			log.info("The execution of the method login started here ...");

			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(16L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAccountInfo));
			UIFoundation.waitFor(2L);
			
			
			if(UIFoundation.isDisplayed(UserProfilePage.spnAccountInfoPromoBar))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is displayed on the main User Account page;";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Promo bar is displayed on the main User Account page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not displayed on the main User Account page";
				   logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+"AccountInfo", objDetail);
				   System.err.println("Promo bar is not displayed on the main User Account page");
			}
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkOrderStatus));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnAccountInfoPromoBar) && UIFoundation.isDisplayed(UserProfilePage.spnOrderHistoryPage))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Orders page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Promo bar is present on the Orders page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Orders page";
			       UIFoundation.captureScreenShot(screenshotpath+"orderPage", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Promo bar is not present on the Orders page");
			}
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnAccountInfoPromoBar) && UIFoundation.isDisplayed(UserProfilePage.spnAddressBookHeader))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Addresses page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Promo bar is present on the Addresses page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Addresses page";
			       UIFoundation.captureScreenShot(screenshotpath+"AddressPage", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Promo bar is not present on the Addresses page");
			}
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPaymentMethods));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnAccountInfoPromoBar) && UIFoundation.isDisplayed(UserProfilePage.spnPaymentMethodsHeader))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Payments page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Promo bar is present on the Payments page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Payments page";
			       UIFoundation.captureScreenShot(screenshotpath+"PaymentPage", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Promo bar is not present on the Payments page");
			}
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkEmailPreferences));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnAccountInfoPromoBar) && UIFoundation.isDisplayed(UserProfilePage.spnEmailPreferenceHeader))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Email Preferences page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Promo bar is present on the Email Preferences page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Email Preferences page";
			       UIFoundation.captureScreenShot(screenshotpath+"EmailPreference", objDetail);
			       logger.fail(objDetail);
				   System.err.println("Promo bar is not present on the Email Preferences page");
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println(" Verify the promo bar is displayed in account pages test case is failed");
				logger.fail(" Verify the promo bar is displayed in account pages test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println(" Verify the promo bar is displayed in account pages test case is executed successfully");
				logger.pass(" Verify the promo bar is displayed in account pages test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifyPromoBarIsDisplayedInAccountPages "+ e);
			return "Fail";
			
		}
	}	

}
