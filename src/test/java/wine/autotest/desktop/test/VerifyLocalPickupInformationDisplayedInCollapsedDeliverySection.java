package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection extends Desktop {

	/*****************************************************************************************
	 * Method Name :
	 * VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection() Created By
	 * : Reviewed By : Purpose : TM-4027
	 *******************************************************************************************
	 */

	public static String addFedExAddress() {
		String actualFedexDeliveyMessage = null;
		String expectedFedexDeliveyMessage = null;
		String objStatus = null;
		String ScreenshotName = "VerifyLocalPickupInformationDisplayed.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;
		try {
			log.info("FEDEX Delivery message Execution method started here:");
//			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnRecipientContinue)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));}
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));}
			UIFoundation.waitFor(5L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.spnFedEXShippingDeliveryMessage)) {
				expectedFedexDeliveyMessage = verifyexpectedresult.FedexDeliveryMessageR;
				actualFedexDeliveyMessage = UIFoundation.getText(FinalReviewPage.spnFedEXShippingDeliveryMessage);
			}
			System.out.println("actualFedexDeliveyMessage   : "+actualFedexDeliveyMessage);
			System.out.println("expectedFedexDeliveyMessage : "+expectedFedexDeliveyMessage);
			if (actualFedexDeliveyMessage.contains(expectedFedexDeliveyMessage)) {
				objStatus += true;
				ReportUtil.addTestStepsDetails("Verified the FedexPickup Message in Delivery Section", "Pass", "");
				System.out.println(actualFedexDeliveyMessage);
				logger.pass("Verified the FedexPickup Message in Delivery Section");
			} else {
				objStatus += false;
				String objDetail = "Verified the FedexPickup Message in Delivery Section is not displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(Screenshotpath, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("FEDEX Delivery message Excution method started here:");
			if (objStatus.contains("false")) {

				System.out.println("Verified the FedexPickup Address Message in Delivery Section testcase failed");

				return "Fail";
			} else {

				System.out.println("Verified the FedexPickup Address Message in Delivery Section testcase succesfully");

				return "Pass";

			}

		} catch (Exception e) {
			return "Fail";
		}

	}
}
