package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.desktop.pages.CartPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.OrderDetailsPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.UIFoundation;


public class VerifyWarningMessageForShipTodayProductsAndListProducts extends Desktop {

	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addShipTodayProdTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
/*		String addToCart4 = null;
		String addToCart5 = null;
		String addToCart6 = null;
		String addToCart7 = null;*/
		String expectedWarningMsg = null;
		String actualWarningMsg = null;
		   String screenshotName = "Scenarios__RemoveShipScreenshot.jpeg";
					
		try {
			
			driver.navigate().to("https://qwww.wine.com/search/Chasseur%20Syrah%202013/0");
			UIFoundation.waitFor(10L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			System.out.println("addToCart1 : "+addToCart1);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				String.valueOf(UIFoundation.clckObject(ListPage.lnkFirstProductToCart));		
			}
			
			System.out.println("ShipsSoonestR");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkPinotNoir));
			//objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(driver, "ShipsSoonestR"));
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "ShipsSoonestR"));
			UIFoundation.waitFor(10L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "SecondProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
			//	UIFoundation.scrollDownOrUpToParticularElement(driver, "ThirdProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			UIFoundation.waitFor(2L);
			expectedWarningMsg = verifyexpectedresult.shiptodayandlistproducts;
			actualWarningMsg = UIFoundation.getText(CartPage.spnShipTodayWarningMsg);
			if (actualWarningMsg.contains(expectedWarningMsg)) {
				objStatus+=true;
				String objDetail="Verifiy warning message for shiptoday products and others products test case is Passed";
			} else {
				System.out.println(actualWarningMsg);
				objStatus+=false;
				String objDetail="Verifiy warning message for shiptoday products and others products test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false")) {
				System.err.println(
						"Verify warning message for shiptoday products and others products test case is failed ");
				return "Fail";
			} else {
				System.out.println(
						"Verify warning message for shiptoday products and others products test case is executed successfully ");
				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}
