package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyStateChangeDialog extends Desktop {
	
	/***************************************************************************
	 * Method Name : verifyStateChangrDialog() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : 
	 ****************************************************************************
	 */

	public static String verifyStateChangrDialogandLocalPickUp() {
		String objStatus = null;
		String expectedStateChangeDialogMsg=null;
		String actualStateChangeDialogMsg=null;
		   String screenshotName = "Scenarios__stateChangeScreenshot.jpeg";
				
		try {
			log.info("The execution of the method verifyStateChangrDialog started here ...");
			UIFoundation.waitFor(3L);
			expectedStateChangeDialogMsg=verifyexpectedresult.stateChangeDailog;
//			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.imgWineLogo));
			UIFoundation.waitFor(2L);
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "dryState");
			
			UIFoundation.waitFor(40L);
			if(UIFoundation.isDisplayed(LoginPage.StateChangeDailog))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verify the state change dialog test case is executed successfully", "Pass", "");
			}else
			{
				objStatus+=false;
				String objDetail="Verify the state change dialog test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			actualStateChangeDialogMsg=UIFoundation.getText(LoginPage.StateChangeDailog);
			System.out.println(actualStateChangeDialogMsg);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnStateChangeCancel));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			
			//Verify Local Pickup
				
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(4L);
			}	
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewAddress))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
				UIFoundation.waitFor(4L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexFirstName))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			}
			UIFoundation.waitFor(15L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnFedexContinue, "Invisible", "", 50);
			if(!UIFoundation.isDisplayed(FinalReviewPage.spnlocalPickUp)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("User able to add Local pickup address.", "Pass", "");
			}else{
				objStatus+=false;
					String objDetail="User not able to add Local pickup address.";
					logger.fail(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+"localPickUp", objDetail);
			}
			log.info("The execution of the method verifyStateChangrDialog ended here ...");
			if (objStatus.contains("false") && (!actualStateChangeDialogMsg.contains(expectedStateChangeDialogMsg))) {
				System.out.println("Verify the state change dialog and local pickup test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the state change dialog and local pickup test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyStateChangrDialog "
					+ e);
			return "Fail";
		}
	}

}
