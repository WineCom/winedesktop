package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class ContinueWithFacebook extends Desktop {
	
	/***************************************************************************
	 * Method Name			: ContinueWithFacebook()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String continueWithFacebook() {
		String objStatus=null;
		   String screenshotName = "Scenarios_facebook_Screenshot.jpeg";
			
		try {
			log.info("The execution of method continueWithFacebook started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.lnkSignInFacebook))
			{
				  objStatus+=true;
			      String objDetail="'Continue with Facebook' button is dispalyed in 'Sign-In' pages";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				objStatus+=false;
				 String objDetail="'Continue with Facebook' button is not dispalyed in 'Sign-In' pages";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
			if(UIFoundation.isDisplayed(LoginPage.lnkSignInFacebook))
			{
				  objStatus+=true;
			      String objDetail="'Continue with Facebook' button is dispalyed in 'Register' pages";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			     
			}else{
				objStatus+=false;
				String objDetail="'Continue with Facebook' button is not dispalyed in 'Register' pages";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method continueWithFacebook ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("'Continue with Facebook' button must be in 'Sign-In' and 'Register' pages  test case is failed");
				return "Fail";
			} else {
				System.out.println("'Continue with Facebook' button must be in 'Sign-In' and 'Register' pages test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Existing customer should be a link in new customer register page  test case is failed");
			log.error("there is an exception arised during the execution of the method continueWithFacebook "
					+ e);
			return "Fail";
		}
	}

}
