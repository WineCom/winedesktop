package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyFedexDisplaysProperGoogleAdresses extends Desktop {

	/*****************************************************************************************
	 * Method Name : VerifyFedexDisplaysProperGoogleAdresses Created By : Reviewed
	 * By : Purpose : TM-4027
	 *******************************************************************************************
	 */

	public static String verifyFedexAddressInGoogleMaps() {
		// String AddressonList= null;
		
		String objStatus = null;
		String ScreenshotName = "VerifyFedexDisplaysProperGoogleAdresses.jpeg";
		
		try {
			log.info("Fedex address verification Execution method started here:");
			objStatus += String
					.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(10L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkChangeAddress))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkAddNewAddress))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			// objStatus+=String.valueOf(UIFoundation.clickObject(Driver,
			// "AddNewAddressLink"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeR"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.imgloactionIcon));
			String addrOnlist = UIFoundation.getText(FinalReviewPage.spnactAddrOnList);
			String oddrOnMap = UIFoundation.getText(FinalReviewPage.spnactAddrOnMap);
			if (addrOnlist.equals(oddrOnMap)) {
				objStatus += true;
				String objDetail = "Address displayed on PickupLoaction and Map are Same:";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Address displayed on PickupLoaction and Map are not Same";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("FedEx address verification mehtod ended here:");
			if (objStatus.contains("false")) {
				System.out.println("Verified the FedexPickup Address on pickup loacation and map failed");

				return "Fail";
			} else {
				System.out.println("Verified the FedexPickup Address on pickup loacation and map executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: fedexLocationGoBackButtonDisplayed()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: TM-3373
	 ****************************************************************************
	 */

	public static String fedexLocationGoBackButtonDisplayed() {
		String objStatus=null;
		   
			try {
			log.info("The execution of the method fedexLocationForUPSStatesInRecipientSection started here ...");
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
            UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "fedExUPSZipcode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshipToThisLocation));
			UIFoundation.waitFor(2L);	
			if(UIFoundation.isDisplayed(FinalReviewPage.btngoBack))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btngoBack));
				 objStatus+=true;
			      String objDetail="User is navigated back to the list of pickup locations on clicking Go Back in local pickup form.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("User is navigated back to the list of pickup locations on clicking Go Back in local pickup form.");
			}else{ 				
				 	objStatus+=false;
				   String objDetail="User is not navigated back to the list of pickup locations on clicking Go Back in local pickup form.";
			       UIFoundation.captureScreenShot(screenshotpath+"PaymentSection", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   System.err.println("User is not navigated back to the list of pickup locations on clicking Go Back in local pickup form.");
			}
		
			log.info("The execution of the method fedex  location go back button displayed ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method fedex  location go back button displayed  "
					+ e);
			return "Fail";
		}

	}
}
