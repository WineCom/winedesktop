package wine.autotest.desktop.test;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyFunctionalityOfDryStateSaveItemsForLater extends Desktop {

	static ArrayList<String> productsInCart = new ArrayList<String>();
	static ArrayList<String> productsInSaveForLater = new ArrayList<String>();
	
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String dryStateShippingDetails() {
		String objStatus = null;
		String prodInSaveForLater = null;
		int productCount = 0;
		   String screenshotName = "Scenarios_dryStateSaveFor_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method dryStateShippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);  
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(1L);
			}
/*
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "AddNewAddressLink"));
			UIFoundation.waitFor(3L);*/
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(10L);
/*			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(5L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(20L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnContinuetostate));
			UIFoundation.waitFor(20L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(20L);
			}
			productCount = Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			UIFoundation.waitFor(1L);
			System.out.println("productCount"+productCount);
			boolean status = UIFoundation.isDisplayed(CartPage.btnContinueShopping);
			UIFoundation.waitFor(36L);
			int saveForLaterCount = Integer.parseInt(UIFoundation.getText(CartPage.spnSaveForLaterCount));
			for (int i = 1; i <= saveForLaterCount; i++) {
				prodInSaveForLater = driver
						.findElement(By.xpath("//section[@class='saveForLater']/ul/li[" + i + "]/div[2]/div[3]/a/span"))
						.getText();
				productsInSaveForLater.add(prodInSaveForLater);
				UIFoundation.waitFor(1L);
			}
//			if ((productCount == 0 ) && productsInCart.containsAll(productsInSaveForLater)) {
			if ((productCount == 0 )) {
				
				  objStatus+=true;
			      String objDetail="Verified the functionality of �Save items for later� in state change dialog ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified the functionality of �Save items for later� in state change dialog ");
			} else {
				 objStatus+=false;
			       String objDetail="Verify the functionality of �Save items for later� in state change dialog test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Verify the functionality of �Save items for later� in state change dialog test case is failed");
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnContinueShopping));
			UIFoundation.waitFor(1L);
/*			String expectedPAgeTitle=objExpectedRes.getProperty("homePageTitle");
			String actualHomePageTitle=driver.getTitle();*/
			if (UIFoundation.isDisplayed(ListPage.lnkhomePageBanner)) {
				
				  objStatus+=true;
			      String objDetail="Verified user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty");
			} else {
				 objStatus+=false;
			       String objDetail="Verify user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Verify  user is navigated to home page, on clicking 'Continue Shopping' button when the cart is empty is failed");
			}
			
			log.info("The execution of the method dryStateShippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method dryStateShippingDetails " + e);
			return "Fail";
		}

	}

}
