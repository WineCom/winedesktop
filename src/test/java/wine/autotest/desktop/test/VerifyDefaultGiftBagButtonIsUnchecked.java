package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyDefaultGiftBagButtonIsUnchecked extends Desktop {

	/***************************************************************************
	 * Method Name : VerifyAddToMyWineFunctionality(NFP-4253) Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose : TM-4128
	 ****************************************************************************
	 */

	public static String verifyNoGiftBagButtonIsUnchecked() {

		String ScreenshotName = "VerifyNoGiftBagButtonIsUnchecked.jpeg";
		String addToCart1, addToCart2;
		String objStatus = null;

		try {
			log.info("Verify No Gift Bag Button Is Unchecked method started here.....");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(3L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			} else {
				addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
				if (addToCart2.contains("Add to Cart")) {
					UIFoundation.waitFor(1L);
					UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
				}
			}
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkChangeAddress)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			}
			WebElement gift = driver.findElement(By.xpath(
					"//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if (!gift.isSelected()) {
				UIFoundation.clickObject(FinalReviewPage.chkRecipientGift);
			}
			WebElement noGiftBag = driver.findElement(By.xpath(
					"//main/section[@class='checkoutMainContent']//section[@class='giftWrapSection']/ul/li/div/div//ul/li[4]//span/input"));
			if (!noGiftBag.isSelected()) {
				objStatus += true;
				String objDetail = "No Gift Bag radio button is not selected by default";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "No Gift Bag radio is selected by default";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("Verify No Gift Bag Button Is Unchecked method ended here.....");
			if (objStatus.contains("false")) {

				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case is failed");

				return "Fail";
			} else {
				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}

	public static String verifyGiftbagName() {

		String objStatus = null;
		String ScreenshotName = "VerifyGiftbagName.jpeg";
		
		/***************************************************************************
		 * Method Name : VerifyGiftbagName Created By : Vishwanath Chavan Reviewed By :
		 * Ramesh,KB Purpose : TM-4128
		 ****************************************************************************
		 */
		try {
			log.info("VerifyGiftbagName method started here......");

			String giftBagDesc = UIFoundation.getText(FinalReviewPage.spngiftBagNames1);
			String actGiftBagDesc =  verifyexpectedresult.actGiftBagName1;
			if (giftBagDesc.equals(actGiftBagDesc)) {
				System.out.println("Gift Bag Name for $3.99 " + giftBagDesc);
				objStatus += true;
				String objDetail = "Gift Bag Name for $3.99 is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Gift Bag Name for $3.99 is wrong";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}

			giftBagDesc = UIFoundation.getText(FinalReviewPage.spngiftBagNames2);
			actGiftBagDesc =  verifyexpectedresult.actGiftBagName2;
			if (giftBagDesc.equals(actGiftBagDesc)) {
				System.out.println("Gift Bag Name for $6.99 " + giftBagDesc);
				objStatus += true;
				String objDetail = "Gift Bag Name for $6.99 is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Gift Bag Name for $6.99 is wrong";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}

			giftBagDesc = UIFoundation.getText(FinalReviewPage.spngiftBagNames3);
			actGiftBagDesc =  verifyexpectedresult.actGiftBagName3;
			if (giftBagDesc.equals(actGiftBagDesc)) {
				System.out.println("Gift Bag Name for $9.99 " + giftBagDesc);
				objStatus += true;
				String objDetail = "Gift Bag Name for $9.99 is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Gift Bag Name for $9.99 is wrong";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("Verify No Gift Bag Button Is Unchecked method ended here.....");
			if (objStatus.contains("false")) {

				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case is failed");

				return "Fail";
			} else {
				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}
}