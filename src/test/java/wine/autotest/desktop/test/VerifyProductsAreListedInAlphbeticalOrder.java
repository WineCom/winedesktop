package wine.autotest.desktop.test;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyProductsAreListedInAlphbeticalOrder extends Desktop {
	
	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyProductsAreListedAlbhabeticalOrder()
	{
		
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		boolean isSorted=true;
		String objStatus=null;
		   String screenshotName = "Scenarios__AlbhbeticalScreenshot.jpeg";
					
		try {
		
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			
			
			 for(int i = 0; i < expectedItemsName.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((expectedItemsName.get(i).compareToIgnoreCase(expectedItemsName.get(i + 1)) > 0) && (expectedItemsName.get(i).compareToIgnoreCase(expectedItemsName.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	objStatus+=true;
					ReportUtil.addTestStepsDetails("Products are listed in Alphabatical order", "Pass", "");
			    	System.out.println("Verify products are listed in Alphabatical order test case executed successfully");
					
			    }
			    else
			    {
			    	objStatus +=false;
					String objDetail="Prooducts are not listed in Alphabatical order";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);

			    	System.err.println("Prooducts are not listed in Alphabatical order");
					
			    }
			    if(objStatus.contains("false"))
				{
					
					return "Fail";
					
				}
				else
				{
					
					return "Pass";
				}
			
		
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}
	

}
