package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class HandelsXBottelsPerCustomer extends Desktop {
	
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addLimitProdTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		   String screenshotName = "Scenarios_ProductLimit_Screenshot.jpeg";
			
		try {
			
			UIFoundation.waitFor(3L);
			driver.get("https://qwww.wine.com/product/louis-roederer-cristal-brut-with-gift-box-2008/427652");
			UIFoundation.waitFor(4L);
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "State");
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(LoginPage.lnktoolTipComponet))
			{
				UIFoundation.clckObject(LoginPage.lnktoolTipComponet);
				UIFoundation.waitFor(1L);
			}
			int prodLimit=Integer.parseInt(UIFoundation.getText(ListPage.spnProductItemLimitCount));
			UIFoundation.waitFor(1L);
			int prodLimitInDropDown=UIFoundation.getLastOptionFromDropDown(ListPage.spnProductItemLimitCount);
			if(prodLimit==prodLimitInDropDown)
			{
				objStatus+=true;
				System.out.println("Product limit test case is executed successfully");
				 String objDetail="Product limit test case is executed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
			       String objDetail="Product limit test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.err.println("Product limit test case is failed");
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}
