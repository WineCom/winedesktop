package wine.autotest.desktop.test;

import java.util.ArrayList;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class SortOrderInNewArrivalAlerts extends Desktop{
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String sortedArrivalLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
		
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "newArrivalAlertEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				//System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				//System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: validationForSortZtoA()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyTheSortingInNewArrival()
	{
			ArrayList<String> arrayList=new ArrayList<String>();
			boolean isSorted = true;
			String screenshotName = "Scenarios_verifyTheSortingInNewArrival.jpeg";
			
		try
		{
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkEmailPreferences));
			UIFoundation.waitFor(5L);
			String products1=UIFoundation.getText(UserProfilePage.lnknewArrivalAlertFirstProduct);
			String products2=UIFoundation.getText(UserProfilePage.lnknewArrivalAlertSecondProduct);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	  objStatus+=true;
				      String objDetail="New arrival alerts are sorted in descending order by default.";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	  }
			    else
			    {
			    		objStatus+=false;
				       String objDetail="New arrival alerts are not sorted in descending order by default.";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				 }
			    log.info("The execution of the method removeCreditCard ended here ...");
				if (objStatus.contains("false")) {
				
					return "Fail";
				} else {
					
					return "Pass";
				}
		}catch(Exception e)
		{
			return "Fail";
			
		}
	
	}

}
