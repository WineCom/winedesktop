package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class FilterIsHighlightedInBlueInListPage extends Desktop{
	
	/***************************************************************************
	 * Method Name			: FilterIsHighlightedInBlueInListPage()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String filterIsHighlightedInBlueInListPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios__FilterHigh.jpeg";
					
		try {
			log.info("The execution of the method filterIsHighlightedInBlueInListPage started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(10L);
		//	String backgroundcolor=UIFoundation.getColor(ListPage.lnkfilterMenuType);
			if(UIFoundation.isDisplayed(ListPage.lnkvarietalFilterIcon))
			{
				  objStatus+=true;
			      String objDetail="'Filter icon' is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Filter icon' is not displayed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			if(UIFoundation.isDisplayed(ListPage.spnvarietalFilterName))
			{
				  objStatus+=true;
			      String objDetail="'Filter Name' is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Filter Name' is not displayed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			if(UIFoundation.isDisplayed(ListPage.lnkvarietalFilterClose))
			{
				  objStatus+=true;
			      String objDetail="'Close icon' is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Close icon' is not displayed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method FilterIsHighlightedInBlueInListPage ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method filterIsHighlightedInBlueInListPage "+ e);
			return "Fail";
		}

	}

}
