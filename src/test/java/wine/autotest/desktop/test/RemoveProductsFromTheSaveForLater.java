package wine.autotest.desktop.test;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;


public class RemoveProductsFromTheSaveForLater extends Desktop {
	static ArrayList<String> expectedItemsName=new ArrayList<String>();
	static ArrayList<String> actualItemsName=new ArrayList<String>();
	static String products1=null;
	static String products2=null;
	static String products3=null;
	static String products4=null;
	static String products5=null;
	/***************************************************************************
	 * Method Name			: saveForLaterWithoutSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart() {
		
		
		String objStatus=null;
		try {
			log.info("The execution of the method saveForLater started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnMainNav));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSort);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			log.info("The execution of the method saveForLater ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: moveProductsToSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String moveProductsToSaveForLater() {

		String objStatus=null;
		try {
			log.info("The execution of the method saveForLater started here ...");
			objStatus+=String.valueOf(UIBusinessFlows.validationForRemoveProductsSaveForLater());
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveThirdProductFromSave));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
			UIFoundation.waitFor(5L);
			driver.navigate().refresh();
			
			System.out.println("=========================Products available in save for later section Before  logout================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.lnkThirdProductInSaveFor).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInSaveFor);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSignout));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method saveForLater ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: saveForLaterWithoutSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String productsAvailableInSaveForLater() {
		String objStatus=null;
		   String screenshotName = "Scenarios__ProductsInSaveFor.jpeg";
			
		try {
			log.info("The execution of the method saveForLater started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Deskusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			System.out.println("=========================Products available in save for later section after login================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInSaveFor);
				System.out.println("1) "+products1);
				actualItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProdcutInSaveFor);
				System.out.println("2) "+products2);
				actualItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.lnkThirdProductInSaveFor).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInSaveFor);
				System.out.println("3) "+products3);
				actualItemsName.add(products3);
			}
			
			if(expectedItemsName.containsAll(actualItemsName))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("all products are available in save for later section.", "Pass", "");
				System.out.println("all products are available in save for later section");
				
			}else{
				objStatus +=false;
				String objDetail="all products are not available in save for later section";
				UIFoundation.captureScreenShot(screenshotpath+"_Shipping", objDetail);
				System.out.println("mismatched products name in save for later section");
			}
			
			log.info("The execution of the method saveForLater ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}

}
