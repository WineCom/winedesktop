package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class NewArrivalAlertsInUserProfile extends Desktop{
	
	/***************************************************************************
	 * Method Name : newArrivalAlertsInUserProfile() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : 
	 * TM-2224,2159
	 ****************************************************************************
	 */

	public static String newArrivalAlertsInUserProfile() {
		String objStatus = null;
		   String screenshotName = "Scenarios_newArrival_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method newArrivalAlertsInUserProfile started here ...");
/*		objStatus += String.valueOf(UIFoundation.clickObject(driver, "addProduct"));
			UIFoundation.waitFor(5L);*/
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSangviovese));
		    objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddProductToPIP));
		    UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ListPage.lnksetAlert))
			{
				//UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnksetAlert);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnksetAlert));
				UIFoundation.waitFor(7L);
			}else{
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkvintageAlert));
				//UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnksetAlert);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnksetAlert));
				UIFoundation.waitFor(8L);
			}
			

			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.Iconshare));
			objStatus += String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkEmailPreferences));
			UIFoundation.waitFor(5L);
			/*if(UIFoundation.isSelected(UserProfilePage.rdoemailPreference))
			{
				  objStatus+=true;
			      String objDetail="Include me on all Wine.com emails Radio button is selected by deafault";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      System.out.println("Include me on all Wine.com emails Radio button is selected by deafaul");
				
			}else{
				  objStatus+=false;
			       String objDetail="Include me on all Wine.com emails Radio button is not selected by deafaul";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"radioButton", objDetail);
			}*/
			if(UIFoundation.isDisplayed(UserProfilePage.spnnewArrivalAlertHeader) && UIFoundation.isDisplayed(UserProfilePage.spnnewArrivalAlertListItem))
			{
				  objStatus+=true;
			      String objDetail="Verified the 'New arrival alerts' in User profile are updated on setting the alert for the product in PIP";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      System.out.println("Verified  the 'New arrival alerts' in User profile are updated on setting the alert for the product in PIP");
			}
			else
			{
				   objStatus+=false;
			       String objDetail="Verify the 'New arrival alerts' in User profile are updated on setting the alert for the product in PIP is failed";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(UserProfilePage.lnkdeleteAlerts))
			{
				String statusDeleteALert=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkdeleteAlerts));
				boolean status=Boolean.valueOf(statusDeleteALert);
				 objStatus+=status;
				 if(status)
				 {
					 objStatus+=true;
				      String objDetail="Alert is deleted";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
				      System.out.println("Alert is deleted successfully");
				 }else{
					 objStatus+=false;
				       String objDetail="Failed to delete alert";
				       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				       UIFoundation.captureScreenShot(screenshotpath+"alert", objDetail);
				 }
			}
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method newArrivalAlertsInUserProfile ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}

}
