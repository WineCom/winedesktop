package wine.autotest.desktop.test;

import java.util.ArrayList;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;


public class AlphabeticalOrderMovingFromSaveForLaterToCart extends Desktop {
	
	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String alphabeticalOrderMovingFromSaveForLaterToCart()
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		ArrayList<String> missingProductName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		   String screenshotName = "Scenarios_SaveForLater_Screenshot.jpeg";
		
		try {
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			String totalItemsBeforeSaveForLater=UIFoundation.getText(CartPage.btnCartCount);
			System.out.println("Number of Products Added to the CART :"+totalItemsBeforeSaveForLater);
			System.out.println("Number of Products in Saved For Later Section :"+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			
			if(!UIFoundation.getText(CartPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			
			if(!UIFoundation.getText(CartPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(CartPage.lnkFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(CartPage.lnkFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			/*String saveForLaterCnt=UIFoundation.getText(CartPage.spnSaveForLaterCount);
			int saveForLaterCount=Integer.parseInt(saveForLaterCnt.replaceAll("\\(|\\)", ""));*/
			boolean saveForLaterTitle=UIFoundation.isDisplayed(CartPage.spnSaveForLaterHeadline);
			boolean noItemsInSaveForLater=UIFoundation.isDisplayed(CartPage.spnNoItemsInSaveForLater);
			boolean saveForLaterAfterCheckout=UIFoundation.isDisplayed(CartPage.spnSaveForLaterAfterCheckout);
			if(saveForLaterTitle==true && noItemsInSaveForLater==true && saveForLaterAfterCheckout==true)
			{
				System.out.println("==========Verify save for later section, if the list empty===============");
				System.out.println("Title 'Saved for Later (0)' is displayed");
				System.out.println("'You have no items saved' text is available");
				System.out.println("Save for later section is available below the Checkout button");
				
				strStatus+=true;
			      String objDetail="Verified  save for later section, if the list empty ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified save for later section, if the list empty");
				logger.pass(objDetail);
				
			}else{
					strStatus+=false;
			       String objDetail="Verify save for later section, if the list empty is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.out.println("Verify save for later section, if the list empty is failed");
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			System.out.println("=======================================================================");
			System.out.println("=======================CART Order Summary Before Moving to Saved for Later =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items before moving :"+totalPriceBeforeSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Name moving from CART to �Saved For Later�:"+expectedItemsName);
			if(!UIFoundation.getText(CartPage.lnkThirdProductSaveForLater).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkThirdProductSaveForLater));
				
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProductSaveForLater).contains("Fail"))
			{
				String saveForLaterLink=String.valueOf(UIFoundation.clickObject(CartPage.lnkSecondProductSaveForLater));
				strStatus+=saveForLaterLink;
				boolean saveForLaterLnk =String.valueOf(saveForLaterLink) != null;
				if(saveForLaterLnk){
						strStatus+=true;
				      String objDetail="Verified the functionality of 'Save for later' link in cart section";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
					System.out.println("Verified the functionality of 'Save for later' link in cart section");
				}else{
						strStatus+=false;
				       String objDetail="Verify the functionality of 'Save for later' link in cart section is failed";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					   System.out.println("Verify the functionality of 'Save for later' link in cart section is failed");
					   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				
			}
			UIFoundation.waitFor(2L);
			String totalItemsAfterSaveForLater=UIFoundation.getText(CartPage.btnCartCount);
			String totalPriceAfterSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductSaveForLater).contains("Fail"))
			{
				String actualProducts1=UIFoundation.getText(CartPage.lnkFirstProductSaveForLater);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProductSaveForLater).contains("Fail"))
			{
				String actualProducts2=UIFoundation.getText(CartPage.lnkSecondProductSaveForLater);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Names in �Saved for Later�: "+actualItemsName);
			System.out.println("=======================Order Summary After Moving =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items after moving :"+totalPriceAfterSaveForLater);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			System.out.println("=======================================================================");
			if(UIFoundation.isDisplayed(CartPage.spnSaveForLaterCount))
			{
					strStatus+=true;
			      String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount));
			}else{
					strStatus+=false;
			       String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.spnSaveForLaterCount);;
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.out.println("Verify save for later section, if the list non empty is failed");
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkmoveToCartSecondLink));
			UIFoundation.waitFor(2L);
			strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkmoveToCartFirstLink));
			UIFoundation.waitFor(2L);
			if(strStatus.contains("false"))
			{
				System.out.println("Product Name "+missingProductName+"is not matched or missing.");
				return "Fail";
				
			}
			else
			{
				System.out.println("Product Names are matched before moving and after moving to the �Saved For Later�");
				return "Pass";
			}
			
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}

}
