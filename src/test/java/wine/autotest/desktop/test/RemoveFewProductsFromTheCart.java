package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;

public class RemoveFewProductsFromTheCart extends Desktop {
	
	

	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotalBefore=null;
		String subTotalAfter=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String secondProductPrice=null;
		String thirdProductPrice=null;
	
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before removing products from the cart===============");
			subTotalBefore=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalBefore);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			secondProductPrice=UIFoundation.getText(CartPage.lnkSecondProductPrice);
			thirdProductPrice=UIFoundation.getText(CartPage.lnkFirstProductPrice);
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.lnkRemoveFirstProduct).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveThirdProduct));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
				UIFoundation.waitFor(3L);
			}
			if(!UIFoundation.getText(CartPage.lnkRemoveSecondProduct).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveSecondProduct));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(3L);
			subTotalAfter=UIFoundation.getText(CartPage.spnSubtotal);
			UIBusinessFlows.removerProductPrice(driver, subTotalBefore, subTotalAfter, secondProductPrice, thirdProductPrice);
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary after removing products from the cart===============");
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalAfter);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			log.info("The execution of the method captureOrdersummary started here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Remove products from the cart test case is failed");
				logger.fail("Remove products from the cart test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Remove products from the cart test case is executed successfully");
				logger.pass("Remove products from the cart test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}

}
