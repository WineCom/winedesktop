package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyUserIsNavigatedToAppropriateTextUrl extends Desktop {
	
	/***************************************************************************
	 * Method Name			: itemsNotEligibleForPromoCodeRedemption()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 * TM-1924
	 ****************************************************************************
	 */
	public static String verifyUserIsNavigatedToAppropriateTextUrl() {
		String objStatus = null;

		   String screenshotName = "Scenarios__productNameInURL.jpeg";
				
		try {
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.mouseHover(driver, "varietal"));
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "CaberNet"));*/
			driver.navigate().to("https://qwww.wine.com/product/avalon-napa-cabernet-sauvignon-2014/159588");
			UIFoundation.waitFor(4L);
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "productInfo2"));
			//UIFoundation.waitFor(5L);
			String productNameInPIP=UIFoundation.getText(ListPage.spnPIPName);
			productNameInPIP=productNameInPIP.replace(" ", "");
			productNameInPIP=productNameInPIP.replaceAll("&", "");
			String productNameIn=driver.getCurrentUrl();
			 int index=productNameIn.lastIndexOf('/');
			String productNameInURL=productNameIn.substring(30,index);
			String actualProductNameInURL=productNameInURL.replaceAll("-", "");
			actualProductNameInURL=actualProductNameInURL.replaceAll("and", "");
			if(actualProductNameInURL.equalsIgnoreCase(productNameInPIP))
			{
				objStatus+=true;
				logger.pass("Verified the user is navigated to appropriate text url on clicking the product in list page");
				ReportUtil.addTestStepsDetails("Verified the user is navigated to appropriate text url on clicking the product in list page", "Pass", "");
			}else
			{
				objStatus+=false;
				String objDetail="Verify the user is navigated to appropriate text url on clicking the product in list page test case is failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
