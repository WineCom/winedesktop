package wine.autotest.desktop.test;



import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class CreditCardInfoIsUpdatedInPaymentMethods extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addPaymentMethod()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 ****************************************************************************
	 */

	public static String addPaymentMethod()
	{
		String objStatus=null;
		 			
		try
		{
			log.info("The execution of the method addAddress started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPaymentMethods));
			UIFoundation.waitFor(7L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSecondAddrEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(UserProfilePage.lnkeditNameField);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txteditCVVField, "CardCvid"));
			String expectedEditedName=UIFoundation.setObjectEditShippingAddress(UserProfilePage.txtenterNameField, "firstName");
			expectedEditedName=expectedEditedName.replaceAll(" ", "");
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.lnkcontactUs);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnSaveCreditCard));
			UIFoundation.waitFor(1L);
			String actualEditedName=UIFoundation.getText(UserProfilePage.spnuserPayementName);
			actualEditedName=actualEditedName.replaceAll(" ", "");
			if(actualEditedName.equalsIgnoreCase(expectedEditedName))
			{
				  objStatus+=true;
			      String objDetail="Verified that user is able to update Credit Card info in Payment methods";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify that user is able to update Credit Card info in Payment methods  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"addressBook", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				  
			}
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") )
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addPaymentMethod "+ e);
			return "Fail";
		}
	}

}
