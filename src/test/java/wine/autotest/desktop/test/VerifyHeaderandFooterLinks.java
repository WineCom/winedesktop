package wine.autotest.desktop.test;
import java.io.IOException;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyHeaderandFooterLinks extends Desktop {

	/***************************************************************************
	 * Method Name			: VerifyCustomerCareHeader()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-2818
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String VerifyCustomerCareHeader() {
		String objStatus = null;
		String screenshotName = "Scenarios_CustomerCareHeader.jpeg";
		
		try {
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkcustomrCareHeader));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.titleContactUs))
			{
				objStatus+=true;
				String objDetail = "Customer Care Page is Displayed";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Customer Care Page is NOT displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
	
			if (objStatus.contains("false")) {
				String objDetail = "Verify Customer care header test case is failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			} else {
				String objDetail = "Verify Customer care header test case is executed successfully";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyLinksinFooter()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-2818
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String VerifyLinksinFooter() {
		String objStatus = null;
		String screenshotName = "Scenarios_CustomerCareHeader.jpeg";
		
		try {
			if(UIFoundation.isDisplayed(ListPage.lnkcustomrCare))
			{
				objStatus+=true;
				String objDetail = "Customer Care Label is Displayed";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				String objDetail = "Customer Care Label is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(UIFoundation.isDisplayed(ListPage.lnkaboutWineCom))
			{
				objStatus+=true;
				String objDetail = "About Wine.Com Label is Displayed";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				String objDetail = "About Wine.Com Label is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(UIFoundation.isDisplayed(ListPage.lnkhowToWork))
			{
				objStatus+=true;
				String objDetail = "How to Work With Us Label is Displayed";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				String objDetail = "How to Work With Us Label is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(UIFoundation.isDisplayed(ListPage.linkcontactUs))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linkcontactUs));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titleContactUs, "Contact Us", "label"));
			String objDetail = "Contact Us Page Label is Displayed";
			System.out.println(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Contact Us Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
					
			if(UIFoundation.isDisplayed(ListPage.linkEmailPref))
			{
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.linkEmailPref);
				UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linkEmailPref));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titleTrackonOrder, "Your Account", "label"));
			String objDetail = "Email Preferences Page Label is Displayed";
			System.out.println(objDetail);
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Email Preferences Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			if(UIFoundation.isDisplayed(ListPage.lnkaboutUs))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaboutUs));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titleAboutUs, "Why Shop with Wine.com?", "label"));
			String objDetail = "About Us Page Label is Displayed";
			System.out.println(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "About Us Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			if(UIFoundation.isDisplayed(ListPage.lnkcareers))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkcareers));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titleCareers, "Attention Wine Lovers", "label"));
			String objDetail = "Careers Page Label is Displayed";
			System.out.println(objDetail);
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Careers Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			if(UIFoundation.isDisplayed(ListPage.lnkpress))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkpress));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titlePress, "In the News", "label"));
			String objDetail = "Press Page Label is Displayed";
			System.out.println(objDetail);
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Press Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			if(UIFoundation.isDisplayed(ListPage.lnksellUs))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnksellUs));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titleSellUsYourWine, "Sell Us Your Wine", "label"));
			String objDetail = "Sell Us Your Wine Page Label is Displayed";
			System.out.println(objDetail);
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Sell Us Your Wine Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			if(UIFoundation.isDisplayed(ListPage.lnkAffilaiteProgram))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAffilaiteProgram));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titleAffiliatedProgram, "Affiliate Program", "label"));
			String objDetail = "Affiliate Program Page Label is Displayed";
			System.out.println(objDetail);
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Affiliate Program Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			if(UIFoundation.isDisplayed(ListPage.lnkpartnership))
			{
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkpartnership));
			UIFoundation.waitFor(3L);
//			objStatus += String.valueOf(UIFoundation.VerifyText(ListPage.titlePartnerships", "Partnerships", "label"));
			String objDetail = "Partnerships Page Label is Displayed";
			System.out.println(objDetail);
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Partnerships Page is NOT Displayed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			System.out.println("objStatus :"+objStatus);
			if (objStatus.contains("false")) {
				String objDetail = "Verify Links in Footer test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			} else {
				String objDetail = "Verify Links in Footer test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyCopyrightsinFooter()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-4203
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String VerifyCopyrightsinFooter() {
		String objStatus = null;
		String screenshotName = "Scenarios_VerifyCopyrightsinFooter.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		String ExpCopyRighttxt = null;
		String actualCopyRighttxt = verifyexpectedresult.CopyRightText;
		try {
			
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.labCopyRights))
			{
				objStatus+=true;
				ExpCopyRighttxt = UIFoundation.getText(ListPage.labCopyRights);
				System.out.println("CopyRighttxt :"+ExpCopyRighttxt);
				UIFoundation.waitFor(3L);	
				String objDetail = "Copy Right text found in Footer :"+ExpCopyRighttxt;
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails( "Copy Right text found in Footer", "Pass", "");
			}else {
				objStatus+=false;
				String objDetail = "Copy Right text not found in Footer";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(ExpCopyRighttxt.contains(actualCopyRighttxt)) {
				objStatus+=true;
				String objDetail = "Copy Right Text Matches the Expected Text";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else {
				objStatus += false;
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.labCopyRights);
				String objDetail = "Copy Right Text Not Matches the Expected Text";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
	
			if (objStatus.contains("false")) {
				System.out.println("Verify VerifyCopyrightsinFooter test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify VerifyCopyrightsinFooter test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
}
