package wine.autotest.desktop.test;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyGiftCardErrorMessageForInvalidCodes extends Desktop {

	/***************************************************************************
	 * Method Name			: giftCodeErrorMessage()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * 						  TM-4277
	 ****************************************************************************
	 */

	public static String giftCodeErrorMessage() {

		String objStatus = null;
		String screenshotName = "giftCodeErrorMessage.jpeg";
		
		try {
			log.info("Gift Code Error Message method started here");
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "invalidGiftNumber"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "invalidGiftNumber"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}
			if(UIFoundation.isDisplayed(CartPage.spnGiftErrorCode))
			{
				objStatus+=true;
				String objDetail="Invalid message for gift  code  is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Invalid message for gift code  is displayed");
				logger.pass(objDetail);
			}else
			{
				objStatus+=false;
				String objDetail="Invalid message for gift  code  is displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtGiftNumber));
			Actions action = new Actions(driver);
			action.sendKeys(Keys.BACK_SPACE).build().perform();
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(CartPage.spnGiftErrorCode)) {
				objStatus+=true;
				String objDetail="Error Mesaage for gift code  is disappeared after edting the code";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Error Mesaage for gift code  is disappeared after edting the code ");
				logger.pass(objDetail);
			}else
			{
				objStatus+=false;
				String objDetail="Error Mesaage for gift code  is not disappeared after edting the code";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("Gift Code Error Message method ended here");
			if(objStatus.contains("false")) {
				System.out.println("Verify Gift Card Error Message For Invalid Codes test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify Gift Card Error Message For Invalid Codes test case Passed");
				return "pass";
			}
		}
		catch(Exception e) {
			System.out.println("Gift Code Error Message method failed due to some exception");
			return "Fail";
		}
	}
}