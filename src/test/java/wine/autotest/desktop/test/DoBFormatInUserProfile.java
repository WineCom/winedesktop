package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class DoBFormatInUserProfile extends Desktop {
	
	/***************************************************************************
	 * Method Name			: DoBFormatInUserProfile()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String doBFormatInUserProfile()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_dob.jpeg";
		try
		{
			log.info("The execution of the method doBFormatInUserProfile started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkAccountInfo));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnDOBFormat)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("DOB format is in'MM / DD / YYYY'.", "Pass", "");
				logger.pass("DOB format is in'MM / DD / YYYY'.");
			}else{
				objStatus+=false;
					String objDetail="DOB is not in format 'MM / DD / YYYY'.";
					UIFoundation.captureScreenShot(screenshotpath+"dobFormat", objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtBrthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtBrthDay,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtBrthYear, "birthYear"));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnSaveAccountInfo));
			if(UIFoundation.isDisplayed(UserProfilePage.spnaccountInfoUpdated))
			{
				objStatus+=true;
				String objDetail="DOB format is dispalyed in 'MM / DD / YYYY' and valid date is saved";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				logger.pass(objDetail);
				System.out.println("Verify DoB drop down field is changed to numeric fields-Desktop test case executed succesfully");
			}else{
				objStatus+=false;
				String objDetail="DOB format is dispalyed in 'MM / DD / YYYY' and valid date is saved test cases failed.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Verify DoB drop down field is changed to numeric fields-Desktop test case is failed");
			}
			log.info("The execution of the method doBFormatInUserProfile ended here ...");
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				return "Pass";
			}
			
		}catch(Exception e)
		{			
			log.error("there is an exception arised during the execution of the method errorMessageDisplayedForTheUserBelow21Years "+ e);
			return "Fail";
			
		}
	}	

}
