package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyOrderOfProductsIconInPIP extends Desktop {
	
	/***************************************************************************
	 * Method Name : verifyOrderOfProductsIconInPIP() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static String verifyOrderOfProductsIconInPIP() {
		String objStatus = null;
		String screenshotName = "Scenarios__orderOfProduct.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method verifyOrderOfProductsIconInPIP started here ...");
			driver.get("https://qwww.wine.com/product/silver-oak-napa-valley-cabernet-sauvignon-2014/391727");
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(ListPage.imgorderOfProductIcons)) {
				objStatus+=true;
				String objDetail="Product icons are displayed in the order";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				logger.pass(objDetail);
			} else {
				objStatus+=false;
				String objDetail="Product icons are not displayed in the order";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method verifyShipToDateUpdated ended here ...");
			if (objStatus.contains("false")) {
				System.out.println(
						"Verify the order of product icons displayed in PIP test case is failed");
				return "Fail";
			} else {
				System.out.println(
						"Verify the order of product icons displayed in PIP test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error(
					"there is an exception arised during the execution of the method verifyOrderOfProductsIconInPIP "
							+ e);
			return "Fail";
		}
	}

}
