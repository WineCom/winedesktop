package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.library.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class CreditInAccountSectionRemainingCredit extends Desktop {
	

	/***************************************************************************
	 * Method Name : searchProductWithProdName() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to search for
	 * product with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductToCart() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSangviovese));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortLtoH));
			UIFoundation.waitFor(6L);
			addToCart1 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "SecondProductToCart");
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			UIFoundation.waitFor(4L);
			// System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name			: VerifyPinkBarDisplayed()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String enterGiftCertificate()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_verifyPinkBar_Screenshot.jpeg";
			
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			String giftCert=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Gift card is applied to the cart total and the appropriate message is displayed above the gift certificate field.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Gift certificate has been expired.Please enter new gift certificate";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			if(!UIFoundation.getText(CartPage.lnkRemoveThirdProduct).contains("Fail"))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveThirdProduct));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
			}
			if(!UIFoundation.getText(CartPage.lnkRemoveFourthProduct).contains("Fail"))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveFourthProduct));
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
				UIFoundation.waitFor(2L);
			}
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String checkoutProcess() {
	String objStatus=null;

	   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			
			
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				UIFoundation.waitFor(20L);
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
				{
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
					UIFoundation.waitFor(3L);	
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkRecipientShipToaddress);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientShipToaddress));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
				UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
				
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
       			UIFoundation.waitFor(2L);
       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
       			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
       			UIFoundation.waitFor(3L);
				WebElement ele1=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele1.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spneditCardModal)) {
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtpaywithThisCardCvv, "CardCvid"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnpaywithThisCardSave);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkOrderStatus));
			UIFoundation.waitFor(3L);
			String expectedBarColor=verifyexpectedresult.verifyPinkBar;
			String actualBarColor=UIFoundation.getColor(UserProfilePage.spnverifyPinkBar);
			if((UIFoundation.isDisplayed(UserProfilePage.spnverifyCreditTotal)) && (expectedBarColor.equalsIgnoreCase(actualBarColor)))
			{ objStatus+=true;
		      String objDetail="Pink bar is displayed, indicating FULL amount of credit";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      logger.pass(objDetail);
			}else
			{
				objStatus+=false;
				String objDetail="pink bar and FULL amount of credit is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+"pink", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the customerís account credit in Account section: remaining credit test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the customerís account credit in Account section: remaining credit test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	

}
