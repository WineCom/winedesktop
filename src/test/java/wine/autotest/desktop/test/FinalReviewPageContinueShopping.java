package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.pages.CartPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.OrderDetailsPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.UIFoundation;




public class FinalReviewPageContinueShopping extends Desktop {
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String verifyContinueShopping() {
	
	String expectedTitle=null;
	String actualTitle=null;
	String objStatus=null;

		try {
			log.info("The execution of the method checkoutProcess started here ...");
			expectedTitle=verifyexpectedresult.homePageTitle;
			UIFoundation.waitFor(4L);
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientShipToaddress));
			objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(3L);

			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(3L);
			}

/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkPaymentCheckoutEdit)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentCheckoutEdit));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			WebElement ele=driver.findElement(By.xpath("//input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnContinueShopping));
			UIFoundation.waitFor(1L);
			actualTitle=driver.getTitle();
			//finalReviewPageContinueShopping
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expectedTitle.equalsIgnoreCase(actualTitle) && objStatus.contains("false")) {
				System.out.println("Final review page Edit CVV test case is failed");
				String objDetail = "Final review page Edit CVV test case is failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			} else {
				System.out.println("Final review page Edit CVV test case is executed successfully");
				logger.pass("Final review page Edit CVV test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyContinueShopping "
					+ e);
			return "Fail";
		}
	}

}
