package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifyTheRecipientSectionUIUnderSubscriptionFlow extends Desktop {


	/*************************************************************************************************
	 * Method Name : addPickedByWineComSubscription() 
	 * Created By  : Rakesh
	 * Purpose     : The purpose of this method is to navigate and 'Proceed to Enrollment' 
	 * 
	 **************************************************************************************************
	 */

	public static String verifyRecipientSectionAfterSubscriptionFlow() {
		String objStatus = null;
		String screenshotName = "Scenarios_addPickedByWineComSubscription_Screenshot.jpeg";
		try {
			log.info("The execution of the method addPickedByWineComSubscription started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isEnabled(FinalReviewPage.rdoShipToFedEx)) {
				objStatus+=true;
				String objDetail="Ship to a Local Pickup Location checkbox is checked by default";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Ship to a Local Pickup Location checkbox is not checked by default";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAdultSignature)) {
				objStatus+=true;
				String objDetail="Adult signature required(Age 21+) for delivery! text is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Adult signature required(Age 21+) for delivery! text is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtOrderArrival)) {
				objStatus+=true;
				String objDetail="'Ship to home or work'  radio button is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Ship to home or work'  radio button is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.rdoShipToHome)) {
				objStatus+=true;
				String objDetail="'Ship to home or work'  radio button is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Ship to home or work'  radio button is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}		
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexZipCode)) {
				objStatus+=true;
				String objDetail="'Ship to local pickup'  radio button is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Ship to local pickup'  radio button is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}		
			if(UIFoundation.isDisplayed(FinalReviewPage.btnFedexSearch)) {
				objStatus+=true;
				String objDetail="'Search'button is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Search'button is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAdultSignature)) {
				String objDetail="Adult signature required(Age 21+) for delivery! text is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Adult signature required(Age 21+) for delivery! text is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}		
			if(UIFoundation.isDisplayed(PickedPage.btnHomeAddrCont)) {
				objStatus+=true;
				String objDetail="'Continue Button' Field is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="'Continue Button' Field is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(5L);
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify The Recipient Section UI Under Subscription Flow test case failed");
				String objDetail="Verified the UI for 'Ship to home or work' after Picked Quiz is not displayed";
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				System.out.println("Verify The Recipient Section UI Under Subscription Flow test case executed successfully");
				String objDetail="Verified the UI for 'Ship to home or work' after Picked Quiz is displayed successfully";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method  "+ e);
			return "Fail";
		}
	}}
