package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyProductsAreRemovedInMyWine extends Desktop {

	/***************************************************************************
	 * Method Name : login() Created By : Vishwanath Chavan Reviewed By : Ramesh,KB
	 * Purpose : The purpose of this method is Login into the Wine.com Application
	 ****************************************************************************
	 */

	public static String login() {
		String objStatus = null;

		try {
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "updateEmailInAccountInfo"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
						
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Login test case is failed");
				return "Fail";
			} else {
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method login " + e);
			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name : VerifyProductsAreRemovedInMyWine() Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB Purpose : TM-4110
	 ****************************************************************************
	 */
	public static String verifyProductsAreRemovedInMyWine() {

		String objStatus = null;
		String screenshotName = "VerifyProductsAreRemovedInMyWine.jpeg";
		
		try {
			log.info("The execution of the method VerifyProductsAreRemovedInMyWine started here ...");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(3L);
			int prodCount = UIFoundation.listSize(ListPage.lnkmywineproducts);
			if (UIFoundation.isElementDisplayed(ListPage.btnRemove)) 
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnRemove));
			    UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
				UIFoundation.waitFor(1l);
				int prodCountAfterRemove = UIFoundation.listSize(ListPage.lnkmywineproducts);
				if(prodCount-1==prodCountAfterRemove) {
				objStatus += true;
				String objDetail = "Product is removed from MyWine page ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Product is not  removed from MyWine page ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}

			log.info("The execution of the method VerifyProductsAreRemovedInMyWine ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify Products Are Removed In MyWine test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Products Are Removed In MyWine test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);

			return "Fail";

		}
	}
}
