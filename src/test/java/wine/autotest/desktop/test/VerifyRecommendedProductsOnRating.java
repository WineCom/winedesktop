package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;


public class VerifyRecommendedProductsOnRating extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyRecommendedProductsOnRating()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyRecommendedProductsOnRating() {
	
	String objStatus=null;

	   String screenshotName = "Scenarios_verifyRecommendedProductsOnRating_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method verifyRecommendedProductsOnRating started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkmerlotFirstProd));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
//			UIFoundation.waitFor(1L);
//			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkrecommendedProdRightArrow);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkrecommendedProdRightArrow));
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(ListPage.lnknextPageRecommendedProducts)){
				  objStatus+=true;
			      String objDetail="Next page of recommendations is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Next page of recommendations is displayed";
				UIFoundation.captureScreenShot(screenshotpath+"allCreditCard", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkrate2Product));
			log.info("The execution of the method verifyRecommendedProductsOnRating ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the functionality of Navigation arrows in the recommendation panel test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the functionality of Navigation arrows in the recommendation panel test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			return "Fail";
		}
	}

}
