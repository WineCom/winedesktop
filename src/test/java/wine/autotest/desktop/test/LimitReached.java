package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class LimitReached extends Desktop{
	
	/***************************************************************************
	 * Method Name			: limitReached()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String limitReached()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__limitReached.jpeg";
		try
		{
			
			log.info("The execution of the method limitReached started here ...");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);
			String addToCart1 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FirstProductToCart");
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.selectLastValueFromDropdown(ListPage.dwnThirdProdQuantutySelct));
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkThirdProductToCart));
			}
			if (UIFoundation.isDisplayed(ListPage.dwnlimitReachedInList)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Limit reached button is displayed in list page.", "Pass", "");
				
				
			} else {
				objStatus+=false;
				String objDetail="Limit reached button is not displayed in  list page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkthirdListProd));
			
			if (UIFoundation.isDisplayed(ListPage.spnlimitReachedInPIP)) {
				//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "AddToMyWineFunc"));
			    UIFoundation.waitFor(1L);
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Limit reached button is displayed in PIP page.", "Pass", "");
			} else {
				objStatus+=false;
				String objDetail="Limit reached button is not displayed in PIP page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(3L);
		
			if (UIFoundation.isDisplayed(ListPage.dwnlimitReachedInList)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Limit reached button is displayed in MyWine page.", "Pass", "");
							
			} else {
				objStatus+=false;
				String objDetail="Limit reached button is not displayed in MyWine page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				
			}
			log.info("The execution of the method limitReached ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Style 'Limit Reached' Button test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Style 'Limit Reached' Button test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method limitReached "+ e);
			return "Fail";
			
		}
	}

}
