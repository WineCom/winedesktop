package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyInvalidPromoCodeAndGiftCard extends Desktop {
	
	/***************************************************************************
	 * Method Name			: invalidGiftCardValidation()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String invalidGiftCardValidation()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedErrorMsg=null;
		String actualErrorMsg=null;
		
		String screenshotName = "Scenarios_ErrorMessage_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method invalidGiftCardValidation started here ...");
			System.out.println("============Order summary ===============");
			expectedErrorMsg=verifyexpectedresult.inValidGiftCardmsg;
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.spnTotalBeforeTax);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "InvalidGiftCertificate"));
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
			UIFoundation.webDriverWaitForElement(CartPage.spnGiftErrorCode, "element", "", 20);
			actualErrorMsg=UIFoundation.getText(CartPage.spnGiftErrorCode);
			UIFoundation.waitFor(2L);
			if(actualErrorMsg.equalsIgnoreCase(expectedErrorMsg))
			{
				System.out.println(expectedErrorMsg);
			    objStatus+=true;
			    String objDetail="Error message is displayed for invalid gift card";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				System.err.println(actualErrorMsg);
			    objStatus+=false;
		        String objDetail="Error message is not displayed for invalid gift card";
		        logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method invalidGiftCardValidation ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method invalidGiftCardValidation "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: invalidPromoCode()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String invalidPromoCode()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedPromocodeMsg=null;
		String actualPromocodeMsg=null;
		
		String screenshotName = "Scenarios_PromoCode_Screenshot.jpeg";
		
		
		try
		{
			log.info("The execution of the method invalidPromoCode started here ...");
			System.out.println("============Order summary ===============");
			expectedPromocodeMsg=verifyexpectedresult.inValidPromoCodemsg;
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{
				
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "inValidPromoCode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.webDriverWaitForElement(CartPage.lnkRemovePromoCode, "Clickable", "", 20);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));;
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "inValidPromoCode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.webDriverWaitForElement(CartPage.lnkRemovePromoCode, "Clickable", "", 20);
			}

			actualPromocodeMsg=UIFoundation.getText(CartPage.spnInvalidPromoCode);
			UIFoundation.waitFor(2L);
			if(actualPromocodeMsg.equalsIgnoreCase(expectedPromocodeMsg))
			{
				System.out.println(expectedPromocodeMsg);
		        objStatus+=true;
		        String objDetail="Error message is displayed for invalid promo code successfully";
		        ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		        logger.pass(objDetail);
			}
			else
			{
				System.err.println(actualPromocodeMsg);
		        objStatus+=false;
		        String objDetail="Error message is not displayed for invalid promo code successfully";
		        logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method invalidPromoCode ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method invalidPromoCode "+ e);
			return "Fail";
			
		}
	}
}
