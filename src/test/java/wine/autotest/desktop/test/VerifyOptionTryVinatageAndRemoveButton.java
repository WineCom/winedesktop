package wine.autotest.desktop.test;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyOptionTryVinatageAndRemoveButton extends Desktop{
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		   String screenshotName = "Scenarios__tryVintage.jpeg";
					
					//WebDriverWait wait=null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			int countOfProductsInCartBefore=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
		/*	objStatus += String.valueOf(UIFoundation.mouseHover(driver, "varietal"));
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "BordexBlends"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(driver, "TryVintageProduct"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "TryVintageProduct"));
			UIFoundation.waitFor(12L);*/
			driver.get("https://qwww.wine.com/product/robert-mondavi-oakville-district-cabernet-sauvignon-2016/535376");
			UIFoundation.waitFor(3L);
//			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkVintage));
			UIFoundation.waitFor(2L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(4L);
			int countOfProductsInCartAfter=Integer.parseInt(UIFoundation.getText(ListPage.spnCartCount));
			if(countOfProductsInCartAfter==countOfProductsInCartBefore+1)
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Try vintage product is  added to the cart", "Pass", "");
				System.out.println("Try vintage product is  added to the cart");
			}else
			{
				objStatus+=false;
				String objDetail="Not able to add Try vintage product is  added to the cart";
				UIFoundation.captureScreenShot(screenshotpath+"tryVintage", objDetail);
				
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveFirstProduct));
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(CartPage.btnRemove))
			{	objStatus+=true;
				ReportUtil.addTestStepsDetails("Remove button is displayed", "Pass", "");
				logger.pass("Remove button is displayed");
				System.out.println("Remove button is displayed");
			}else{
				objStatus+=false;
				String objDetail="Remove button is not displayed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnShoppingCartHeader));
			UIFoundation.waitFor(1L);
			boolean isHidden=UIFoundation.isDisplayed(CartPage.btnRemove);
			if(isHidden==false)
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("'Remove' icon is hided, Once the user clicks anywhere in the application", "Pass", "");
				System.out.println("'Remove' icon is hided, Once the user clicks anywhere in the application");
				logger.pass("'Remove' icon is hided, Once the user clicks anywhere in the application");
			}else{
				objStatus+=false;
				String objDetail="'Remove' icon is hided, Once the user clicks anywhere in the application is failed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
}
