package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class ValidateTheSubscriptionFlowForLocalPickupAddress extends Desktop {
	
/******************************************************************************************
 * Method Name : verifyLocalPickUpFlowForPickedUpWine
 * Created By  : Rakesh C S	
 * Purpose     : This method validates local pickup address with picked up wine
 * 
 ******************************************************************************************/
	
	public static String verifyLocalPickUpFlowForPickedUpWine() {		
	String objStatus = null;
	String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
	try {
		log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
		UIFoundation.waitFor(2L);
		if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientHeader))
		{
			objStatus+=true;
			String objDetail="Subscription flow 'Recipient' page is displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription flow 'Recipient' page should is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
		UIFoundation.waitFor(10L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
		if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
		{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			UIFoundation.clearField(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
		UIFoundation.waitFor(3L);
		if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
		}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkAcceptTermsandConditions));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
			UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
			if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
				objStatus+=true;
				String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
				logger.fail("Validate The Subscription Flow For Local Pickup Address test case failed", MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
				logger.pass("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}