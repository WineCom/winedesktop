package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class ShipSoonCheckbox extends Desktop {

	/***************************************************************************
	 * Method Name : ShipSoonCheckbox() Created By : Vishwanath Chavan Reviewed By :
	 * Ram Purpose :
	 ****************************************************************************
	 */

	public static String shipSoonCheckbox() {
		String objStatus = null;
		String screenshotName = "Scenarios__shipSoonChk.jpeg";
		

		try {
			
			log.info("The execution of the method shipSoonCheckbox started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkRhoneBlends));
			UIFoundation.waitFor(2L);
			if (!UIFoundation.isCheckBoxSelected(ListPage.chkshipSoon)) {
				objStatus += true;
				String objDetail = "By default the check box is unchecked.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "By default the check box is not unchecked.";
				// ReportUtil.addTestStepsDetails("Order number is null.Order not placed
				// successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());

			}
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.chkshipSoon));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkshipTodayFirstProd));
			UIFoundation.waitFor(5L);
			
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkshipTodayFirstProd));
			UIFoundation.waitFor(5L);
			if (!UIFoundation.isElementDisplayed(ListPage.chkshipSoon)) {
				objStatus += true;}else {
					objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.spnShipTodayThirdProductName));
					objStatus += true;
				}*/
			
			log.info("The execution of the method shipSoonCheckbox ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify 'Ships Soon' check box is added on list page test case is failed");
				return "Fail";
			} else {
				System.out.println(
						"Verify 'Ships Soon' check box is added on list page test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error(
					"there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "
							+ e);
			return "Fail";

		}
	}

}
