package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class EnteredGiftCardGiftCertificateSection extends Desktop{
	
	/***************************************************************************
	 * Method Name			: enterGiftCertificate()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String enterGiftCertificate()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_giftCert_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method enterGiftCertificate started here ...");
			String giftCert=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(16L);
			}
			if(UIFoundation.isDisplayed(CartPage.btnexistingCust))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnexistingCust));
				UIFoundation.waitFor(2L);
			}
//			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "giftCertificateUn"));
//			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
//			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(10L); 
			if(!UIFoundation.isDisplayed(CartPage.txtGiftNumber)){
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
			}
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Gift card is applied to the cart total and the appropriate message is displayed above the gift certificate field.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Gift certificate has been expired.Please enter new gift certificate";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			System.out.println("enterGiftCertificate :"+objStatus);
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the entered gift card is applied to the cart total when the user logs in to the application in 'Gift certificate' section test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the entered gift card is applied to the cart total when the user logs in to the application in 'Gift certificate' section test case executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}

}
