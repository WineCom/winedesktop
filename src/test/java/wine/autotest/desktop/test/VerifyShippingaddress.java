
package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyShippingaddress extends Desktop {

	/*****************************************************************************************
	 * Method Name : VerifyFedexDisplaysProperGoogleAdresses Created By : Reviewed
	 * By : Purpose : TM-3966
	 *******************************************************************************************
	 */

	public static String verifyHomePickUpLocationAddress() {

		String objStatus = null;
		String ScreenshotName = "verifyHomePickUpLocationAddress.jpeg";
		
		try {
			log.info("verifyHomePickUpLocationAddress Method Started Here");
			objStatus += String
					.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			System.out.println("Adding address for First time");
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexFirstName)) {
				String objDetail="Verified FedEx Address Form displayed for the First time while adding address";
				System.out.println(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else {
				String objDetail="Verified FedEx Address Form displayed for the First time while adding address";
				System.out.println(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			    UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(8L);
			System.out.println("Adding address for Second time");
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtFedexZipCode);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexFirstName)) {
				String objDetail="Verified FedEx Address Form displayed for the second time while adding address";
				System.out.println(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    logger.pass(objDetail);
			}else {
				String objDetail="Verified FedEx Address Form displayed for the second time while adding address";
				System.out.println(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			    UIFoundation.captureScreenShot(screenshotpath+"FedexFirstName", objDetail);
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+ScreenshotName)).build());
			}
			
		System.out.println(objStatus);
			log.info("verifyHomePickUpLocationAddress Method Ended Here");
			if (objStatus.contains("false")) {
				String objDetail="Verification of FedEx Address test case failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "fail";
			} else {
				String objDetail="Verification of FedEx Address test case Passed";
				 ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";

			}
		} catch (Exception e) {
			log.error("There is an exception arised during the execution of the method execution " + e);
			return "Fail";

		}

	}

}
