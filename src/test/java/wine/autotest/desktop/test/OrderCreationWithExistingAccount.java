package wine.autotest.desktop.test;

import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;

public class OrderCreationWithExistingAccount extends Desktop {

	static boolean isObjectPresent = false;

	/***************************************************************************
	 * Method Name : searchProductWithProdName() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to search for
	 * product with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus = null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "Wind"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(12L);
			// System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
			// UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			// System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : checkoutProcess() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to capture the order number
	 * and purchased id
	 ****************************************************************************
	 */

	public static String checkoutProcess() {

		String expected = null;
		String actual = null;
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			// driver.navigate().refresh();
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
//			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShiptoaddressOne));
			/*if (UIFoundation.isDisplayed(FinalReviewPage.rdoShiptoaddressOne)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShiptoaddressOne));
				UIFoundation.waitFor(10L);
			}*/
			UIFoundation.waitFor(10L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnContinuetostate)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnContinuetostate));
				UIFoundation.waitFor(10L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
		
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(10L);
//			}
//			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
//				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);	
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				
//				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
//				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
//				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal = UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling = UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total = UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax = UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(25L);
			  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);	                  
	            }               
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order number is null.Order not placed successfully";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			System.out.println("checkoutProcess objStatus :"+objStatus);
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with existing account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with existing account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order number is null.Order not placed successfully";
			UIFoundation.captureScreenShot( screenshotpath+ screenshotName, objDetail);
			return "Fail";
		}
	}

}
