package wine.autotest.desktop.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.OrderDetailsPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.UIFoundation;

public class QuantityPickerFromPIP extends Desktop {
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyQuantityPickerInCartPage() {
		String objStatus=null;
		//String actualQuantity=null;
		//int expectedQuantity=null;
		String screenshotName = "Scenarios_QuantityPick_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "shafer"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(4L);
			UIFoundation.getLastOptionFromDropDown(ListPage.dwnFirstProdQuantitySelect);
			UIFoundation.waitFor(3L);
			int expectedQuantity=Integer.parseInt(UIFoundation.getFirstSelectedValue(ListPage.dwnFirstProdQuantitySelect));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);
			WebElement drop_down =driver.findElement(By.xpath("//select[@class='productQuantity_select']"));
			Select se = new Select(drop_down);
			int actualQuantity=Integer.parseInt(se.getOptions().get(expectedQuantity).getAttribute("value"));
			UIFoundation.waitFor(1L);
			if(expectedQuantity==actualQuantity)
			{
				objStatus+=true;
				String objDetail="Selected quantity in PIP page is matched with the cart page quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("Selected quantity in PIP page is matched with the cart page quantity");
			}
			else
			{
				objStatus+=false;
		    	String objDetail="Selected quantity in PIP page does not match with the cart page quantity";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	logger.fail(objDetail);
				System.out.println("Selected quantity in PIP page does not match with the cart page quantity");
			}
			System.out.println(objStatus);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}

}
