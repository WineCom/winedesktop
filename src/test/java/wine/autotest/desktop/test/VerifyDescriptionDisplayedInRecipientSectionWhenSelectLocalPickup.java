package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.LoginPage;

public class VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup extends Desktop {
	static String expectedState=null;
	static String actualState=null;
	/***************************************************************************
	 * Method Name : offerPageDisplayed() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh,KB
	 * Purpose     : The purpose of this method is to Create.
	 ****************************************************************************
	 */
	public static String offerPageDisplayed() {
		String objStatus = null;
		try {
			log.info("The execution of method create Account started here");
			UIFoundation.waitFor(1L);			
			driver.get("https://qwww.wine.com/picked");				
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(3L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyFirsrPickedPage));			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifySeconePickedPage));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyThirdPickedPage));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.cmdPickedWrapNextThirdPage);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdPickedWrapNextThirdPage));	
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyForthPickedPage));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNextThirdPage));
			UIFoundation.waitFor(1L);			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblNotVery));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNextThirdPage));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedNext));
			UIFoundation.waitFor(1L);			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail2,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword2, "accPassword"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));			
			UIFoundation.waitFor(1L);					
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false") )
			{	
				logger.fail("User profile creation  test case is failed");
				
				return "Fail";				
			} else
			{	
				logger.pass("User profile creation  test case is Passed");
				return "Pass";
			}
		} 
		catch (Exception e)
		{
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnMainNav));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainAccSignIn));
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}			
			if(UIFoundation.isDisplayed(LoginPage.lnkSigninLinkAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSigninLinkAcc));
			}			
			objStatus+=String.valueOf(UIFoundation.newUsersetObject(LoginPage.txtLoginEmail,"userEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnSignIn));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			log.info("The execution of the method login ended here ...");			
			if(expectedState.equalsIgnoreCase(actualState))
			{
				objStatus+=true;
			    String objDetail="Expected state name and actual state name matched";
			    logger.pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    System.out.println("Expected satte name and actual state name matched");
			}
			else
			{				
				 objStatus+=false;
				 String objDetail="Expected state name and actual state name doest not match";
				 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}		
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}		
		}
		catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";		
		}
	}
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Vishwanath Chavan 
	 * Reviewed By : Ramesh,
	 * Purpose     : The purpose of this method is to fill the shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	public static String shippingDetails() {
		String objStatus = null;
		String screenshotName = "addNewCreditCard.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);			
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnFedexSearch);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(13L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnLocalPickupAddressselect)){
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnLocalPickupAddressselect));
			}					
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));			
			UIFoundation.waitFor(13L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtAdultSignature);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtAdultSignature));
			objStatus += String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtarivalMessage));		
			log.info("The execution of the method shippingDetails ended here ...");
			if (objStatus.contains("false")) {
				objStatus+=false;
				String objDetail = "Local pick up description is not displayed in the Compass Recipient section";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			} 
			else 
			{
				objStatus+=true;
				String objDetail = "Local pick up description is  displayed in the Compass Recipient section";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				return "Pass";		
			}
		}
		catch (Exception e)
		{
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard(WebDriver driver) {
		String objStatus = null;
		String screenshotName = "addNewCreditCard.jpeg";
		
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkTermsConditio);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkTermsConditio));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.cmdEnrollInPicked, "Invisible", "", 10);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.cmdEnrollInPicked));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtEnrollmentError));			
			UIFoundation.waitFor(3L);           
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
				String objDetail = "User is Not be able to enroll for  Existing/New dry state address in the recipient section";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "false";
			} 
			else
			{
				objStatus+=true;
				String objDetail = "User is Not be able to enroll for  Existing/New dry state address in the recipient section";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				return "Pass";
			}
		} 
		catch (Exception e) 
		{
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}

}