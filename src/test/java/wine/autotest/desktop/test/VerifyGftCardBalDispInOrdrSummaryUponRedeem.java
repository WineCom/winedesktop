package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;


public class VerifyGftCardBalDispInOrdrSummaryUponRedeem extends Desktop{


	/***************************************************************************
	 * Method Name : addProductToCartLessThnGftCrdPrice() 
	 * Created By  : Rakesh C S
	 * Reviewed By : Chandrashekar 
	 * Purpose     : Add product to cart that is less the gift card amount
	 ****************************************************************************
	 */

	public static String addProductToCartLessThnGftCrdPrice() {
		String objStatus=null;
		String addToCart1 = null;
	
		try {
			log.info("The execution of the addProductToCartLessThnGftCrdPrice method strated here");
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortLtoH));
			UIFoundation.waitFor(12L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);	
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(10L);

			}
			System.out.println("addprodTocrt : "+objStatus);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : VerifyGftCardBalDispInOrdrSummaryUponRedeem() 
	 * Created By  : Rakesh
	 * Reviewd By  : Chandrasekar 
	 * Purpose     : verifies the gift card and remaining balance 
	 ****************************************************************************
	 */
	
	public static String verifyGftCardBalDispInOrdrSummaryUponRedeem() {
		String objStatus = null;
		String total=null;
		String giftCardAmt = null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		try {

			log.info("The execution of the method verifyGftCardBalDispInOrdrSummaryUponRedeem started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				UIFoundation.waitFor(3L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("DOB format is in'MM / DD / YYYY'.", "Pass", "");
				logger.pass("DOB format is in'MM / DD / YYYY'.");
			}else{
				objStatus+=false;
				String objDetail="DOB is not in format 'MM / DD / YYYY'.";
				UIFoundation.captureScreenShot(screenshotpath+"dobFormat", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(6L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			String salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
//				UIFoundation.webDriverWaitForElement(CartPage.spngiftCertificateSuccessMsg, "element", "", 50);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
//				UIFoundation.webDriverWaitForElement(CartPage.spngiftCertificateSuccessMsg, "element", "", 50);

			}
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Verified gift card added sucessfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Gift card did not add sucessfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(2L);
			giftCardAmt = UIFoundation.getText(FinalReviewPage.txtGiftCrdAmt);
			String txtGiftCartAmt = giftCardAmt.replaceAll("[$,]", "");
			double totalGftAmount = Double.parseDouble(txtGiftCartAmt);
			String txtSubtotal = total.replaceAll("[$,]", "");
			double SubtotalAmount = Double.parseDouble(txtSubtotal);
			double expRemGftCardBal = totalGftAmount-SubtotalAmount;
			expRemGftCardBal=Math.round(expRemGftCardBal*100.0)/100.0;
			String expGiftCardRem = UIFoundation.getText(FinalReviewPage.txtGiftCrdRem);
			String txtGiftCardRem = expGiftCardRem.replaceAll("[$,]", "");
			double actRemCardBal = Double.parseDouble(txtGiftCardRem);
			if(expRemGftCardBal==actRemCardBal) {
				objStatus+=true;
				String objDetail="Verified Gift card Original amount and Remaining Balance is displayed properly";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else{
				objStatus+=false;
				String objDetail="Gift card Original amount and Remaining Balance is not displayed properly";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method verifyGftCardBalDispInOrdrSummaryUponRedeem ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="VerifyGftCardBalDispInOrdrSummaryUponRedeem' Test case failed ";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				System.out.println("Gift card Original amount and Remaining Balance is not displayed properly");
				return "Fail";
			}
			else
			{
				String objDetail="'VerifyGftCardBalDispInOrdrSummaryUponRedeem' Test case executed succesfully ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("Verified Gift card Original amount and Remaining Balance is displayed properly"); 
				return "Pass";
			}
		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}