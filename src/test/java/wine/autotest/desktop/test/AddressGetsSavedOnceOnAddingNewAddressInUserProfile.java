package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.UIFoundation;

public class AddressGetsSavedOnceOnAddingNewAddressInUserProfile extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
				
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "addsaveolyonce"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : UpdateShippingAddressInAddressBook() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : 
	 * TM-2224,2159
	 ****************************************************************************
	 */

	public static String addAddressInAddressBook() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		

		try {
			log.info("The execution of the method updateShippingAddressInAddressBook started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(5L);
			int addressCountBeforUAddingNewAddress=UIFoundation.listSize(UserProfilePage.lnkshippingAddressCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewAddress));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtAddressFullName, "firstName"));
		//	objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnState, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnAddressSave));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(UserProfilePage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnVerifyContinue));
//				UIFoundation.webDriverWaitForElement(UserProfilePage.btnVerifyContinue, "Invisible", "", 50);
			}
			int addressCountAfterUAddingNewAddress=UIFoundation.listSize(UserProfilePage.lnkshippingAddressCount);
			if(((addressCountBeforUAddingNewAddress+1)==addressCountAfterUAddingNewAddress)){
				objStatus+=true;
			      String objDetail="Verified that address gets saved only once on adding  the new address in Shipping addresses page under user profile";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified that address gets saved only once on adding the new address in Shipping addresses page under user profile");
			}else{
				 objStatus+=false;
				   String objDetail="Verify  that address gets saved only once on adding  the new address in Shipping addresses page under user profile  is failed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   System.err.println("Verify that address gets saved only once on adding  the new address in Shipping addresses page under user profile is failed ");
			}
			log.info("The execution of the method updateShippingAddressInAddressBook ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method updateShippingAddressInAddressBook "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : removeAddressInAddressBook() 
	 * Created By : Chandra shekhar
	 * Chavan Reviewed By : Ramesh, 
	 * Purpose : 
	 * TM-1859
	 ****************************************************************************
	 */

	public static String removeAddressInAddressBook() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		

		try {
			log.info("The execution of the method removeAddressInAddressBook started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavTabSignOut"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(UserProfilePage.lnkAddressBook));
//			UIFoundation.webDriverWaitForElement(UserProfilePage.lnkAddNewAddress, "Clickable", "", 50);
			int addressCountBeforUAddingNewAddress=UIFoundation.listSize(UserProfilePage.lnkshippingAddressCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkuserDataCardEdit));
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnremoveAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(UserProfilePage.btnYesRemove));
			UIFoundation.waitFor(3L);			
					
			/*if(UIFoundation.isDisplayed(driver, "DeliveryContinue"))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "DeliveryContinue"));
				UIFoundation.waitFor(10L);
			}*/
			int addressCountAfterUAddingNewAddress=UIFoundation.listSize(UserProfilePage.lnkshippingAddressCount);
			if(((addressCountBeforUAddingNewAddress-1)==addressCountAfterUAddingNewAddress)){
				objStatus+=true;
			      String objDetail="Address is deleted permanently from the user profile Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Address is deleted permanently from the user profile Recipient page");
				  
			}else{
				objStatus+=false;
				   String objDetail="Address is not deleted permanently from the user profile Recipient page";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   System.err.println("Address is not deleted permanently from the user profile Recipient page");
			}
			log.info("The execution of the method removeAddressInAddressBook ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method removeAddressInAddressBook "
					+ e);
			return "Fail";
		}
	}

	
}
