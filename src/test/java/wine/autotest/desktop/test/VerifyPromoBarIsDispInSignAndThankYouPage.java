package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyPromoBarIsDispInSignAndThankYouPage extends Desktop{
	/***************************************************************************
	 * Method Name			: VerifyPromoBarIsDispInSignAndThankYouPage()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4188
	 ****************************************************************************
	 */

	public static String promoBarInSignPageandCreateAct() {
		
		String objStatus = null;
		 String screenshotName = "promoBarInSignPage.jpeg";
					
			try {
				log.info("Promo Bar In Sign Page method started here......");
				driver.get("https://qwww.wine.com/auth/signin");
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(LoginPage.btnSignIn) && (UIFoundation.isDisplayed(LoginPage.lnkpromoBarDisp))){
				String promoBarDisptext =UIFoundation.getText(LoginPage.lnkpromoBarDisp);
				System.out.println("promo bar is displayed in sign in  page "+ promoBarDisptext);
				objStatus += true;
				String objDetail = "promo bar is displayed in sign in  page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "promo bar is not displayed in sign in  page";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			 objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
			 if(UIFoundation.isDisplayed(LoginPage.btnCreateAccount) && (UIFoundation.isDisplayed(LoginPage.lnkpromoBarDisp))){
					String promoBarDisptext =UIFoundation.getText(LoginPage.lnkpromoBarDisp);
					System.out.println("promo bar is displayed in Create account  page "+ promoBarDisptext);
					objStatus += true;
					String objDetail = "promo bar is displayed in sign in  page";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
				} else {
					objStatus += false;
					String objDetail = "promo bar is not displayed in Create account  page";
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			 objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSignIn));
			 UIFoundation.waitFor(2L);
			 objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			 objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			 objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(6L);
				
			 log.info("Promo Bar In Sign Page method started here......");

				if (objStatus.contains("false")) {

					System.out.println("Promo Bar In Sign Page and create account  test case is failed");

					return "Fail";
				} else {
					System.out.println("Promo Bar In Sign Page and create account  test casee test case executed succesfully");

					return "Pass";
				}
			} catch (Exception e) {
				return "Fail";

			}
		}
	/***************************************************************************
	 * Method Name			: promobarDispInThankYouPage()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4188
	 ****************************************************************************
	 */
	public static String promobarDispInThankYouPage() {
	
	String objStatus = null;
	String subTotal = null;
	String shippingAndHandling = null;
	String total = null;
	String salesTax = null;
	
	String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
	
	try {
		log.info("The execution of the method promo bar Disp In ThankYouPage started here ...");
		
		objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));

		UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
		objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
		if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(1L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			UIFoundation.clickObject(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			}
		if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              "+subTotal);
		System.out.println("Shipping & Handling:   "+shippingAndHandling);
		System.out.println("Sales Tax:             "+salesTax);
		System.out.println("Total:                 "+total);
		UIFoundation.waitFor(3L);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
	    if(UIFoundation.isDisplayed(ThankYouPage.lnkOrderNumber) && (UIFoundation.isDisplayed(LoginPage.lnkpromoBarDisp))){
	    	String promoBarDisptext = UIFoundation.getText(LoginPage.lnkpromoBarDisp);
			System.out.println("promo bar is displayed in Thank you  page "+ promoBarDisptext);
			objStatus += true;
			String objDetail = "promo bar is displayed in sign in  page";
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			logger.pass(objDetail);
		} else {
			objStatus += false;
			String objDetail = "promo bar is not displayed in Thank you  page";
			ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
	    log.info("The execution of the method promo bar Disp In ThankYouPage started here ...");
		if (objStatus.contains("false")) {
			System.out.println("Promo Bar In Sign Page and Thank You test case is failed");
			return "Fail";
		} else {
			System.out.println("Promo Bar In Sign Page and Thank You test case is executed successfully");
			return "Pass";
		}
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method getOrderDetails " + e);
		objStatus += false;
		return "Fail";
	}
}
	}