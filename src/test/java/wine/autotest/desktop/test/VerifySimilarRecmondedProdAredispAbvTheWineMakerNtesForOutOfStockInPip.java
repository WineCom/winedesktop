package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip extends Desktop {

	
	
	/***************************************************************************
	 * Method Name : RecomendedProdDisplayedForOutOfStock() Created By : Vishwanath
	 * Chavan Reviewed By : Purpose :TM-4314
	 ****************************************************************************
	 */

	public static String recomendedProdDisplayedForOutOfStock() {
	String objStatus = null;
	String screenshotName = "recomendedProdDisplayedForOutOfStock.jpeg";
	
	try {
		log.info("Recomended Prod Displayed For Out Of Stock started here");
		UIFoundation.waitFor(3L);
		driver.navigate().to("https://qwww.wine.com/product/lapostolle-cuvee-alexandre-cabernet-sauvignon-2015/514662");
		UIFoundation.waitFor(12L);
		if(UIFoundation.isDisplayed(ListPage.simProddIsAbvWineMakrNt)) {
		objStatus+=true;
		String objDetail="Similar recomended products are displayed above winemaker notes for out of stock products";
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		logger.pass(objDetail);
		}
		else {
			objStatus+=false;
			String objDetail="Similar recomended products are not displayed above winemaker notes for out of stock products";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		UIFoundation.waitFor(3L);
		log.info("Recomended Prod Displayed For Out Of Stock ended here");
		if (objStatus.contains("false")) {
			System.out.println("verification of recomended products above winemaker notes  test case is failed");
			return "Fail";
			

		} else {
			System.out.println("verification of recomended products above winemaker notes test case is Passed");
			return "Pass";
			
		}

	} catch (Exception e) {
		System.out.println(" Search product with product name test case is failed");
		log.error("there is an exception arised during the execution of the method  " + e);
		return "Fail";
	}
	}
}
