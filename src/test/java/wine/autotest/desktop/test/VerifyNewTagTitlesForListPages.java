package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;

public class VerifyNewTagTitlesForListPages extends Desktop {

	/***************************************************************************
	 * Method Name			: verifyNewTitleForListpages()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * 						  TM-4361
	 ****************************************************************************
	 */

	public static String  verifyNewTitleForListpages() {
		
		String objStatus = null;
		String screenshotName = "verifyNewTitleForListpages.jpeg";
		
		try {
		log.info("verify New Title ForList pages started here");
		UIFoundation.waitFor(5L);
		objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
		String tabTitle = driver.getTitle();
		System.out.println(tabTitle);
		String newTabTitle = verifyexpectedresult.tabNewTitle;
		if(tabTitle.equals(newTabTitle)) {
			objStatus+=true;
			String objDetail="New Tag titles for list pages are verified";
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("New Tag titles for list pages are verified");
			logger.pass(objDetail);
		}else
		{
			objStatus+=false;
			String objDetail="New Tag titles for list pages are not verified";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		log.info("verify New Title ForList pages ended here");
		if(objStatus.contains("false")) {
		System.out.println("verify New Title For Listpages test case Failed");
		return "Fail";
		}
		else {
		System.out.println("verify New Title For Listpages test case Passed");
		return "Pass";
		}
		}
		catch(Exception e) {
		System.out.println("verify New Title For Listpages test case failed due to some interuption"+e);
		return "Fail";
		}
		}
	}