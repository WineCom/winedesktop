package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyErrorMsgInvalidZipCodeForFedexeAddress extends Desktop {
	/***************************************************************************
	 * Method Name			: shippingDetails()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 * TM-1852
	 ****************************************************************************
	 */

	public static String invalidZipCodeInFedexAddress() {
		String objStatus=null;
		   String screenshotName = "Scenarios_fedexPickUpLocation_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(3L);
/*			objStatus+=String.valueOf(UIFoundation.setObject(driver,"FedexZipCode", "fedExInvalidZipcode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "FedexSearchButton"));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(driver, "fedexLocalPickUpIsNotAvailable"))
			{
				 objStatus+=true;
			      String objDetail="Verified the error message is displayed on adding the new FedEx address with invalid zipcode";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the error message is displayed on adding the new FedEx address with invalid zipcode");
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the error message is displayed on adding the new FedEx address with invalid zipcode  is failed";
			       UIFoundation.captureScreenShot(driver, screenshotpath+"PaymentSection", objDetail);
				   System.err.println("Verify the error message is displayed on adding the new FedEx address with invalid zipcode is failed ");
			}
			UIFoundation.clearField(driver, "FedexZipCode");
			UIFoundation.waitFor(1L);*/
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "VerifyFedexAddress"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "InvalidZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(8L);

			if(UIFoundation.isDisplayed(FinalReviewPage.spnzipCodeErrorMsg))
			{
				 objStatus+=true;
			      String objDetail="Verified the error message is displayed on adding the new FedEx address with invalid zipcode";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified the error message is displayed on adding the new FedEx address with invalid zipcode");
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the error message is displayed on adding the new FedEx address with invalid zipcode  is failed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   UIFoundation.captureScreenShot(screenshotpath+"invalidZipCode", objDetail);
				   System.err.println("Verify the error message is displayed on adding the new FedEx address with invalid zipcode is failed ");
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails "
					+ e);
			return "Fail";
		}

	}

}
