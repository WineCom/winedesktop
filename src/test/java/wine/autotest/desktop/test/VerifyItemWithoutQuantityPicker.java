package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyItemWithoutQuantityPicker extends Desktop {
	
	/***************************************************************************
	 * Method Name : addStewardshipToCart() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyQuantityPicker() {
		String objStatus = null;
		try {
			log.info("The execution of the method verifyQuantityPicker started here ...");
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.lnkFifthProductSaveForLater).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkFifthProductSaveForLater));
				UIFoundation.waitFor(2L);
			}
			
			if(!UIFoundation.getText(CartPage.lnkFourthProductSaveForLater).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkFourthProductSaveForLater));
				UIFoundation.waitFor(3L);
			}
			
			if(!UIFoundation.getText(CartPage.lnkThirdProductSaveForLater).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkThirdProductSaveForLater));
				UIFoundation.waitFor(3L);
				
			}
//			UIFoundation.scrollDownOrUpToParticularElement(driver, "ShoppingCartHeader");
			UIFoundation.waitFor(2L);
			if(!UIFoundation.getText(CartPage.lnkSecondProductSaveForLater).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkSecondProductSaveForLater));
				UIFoundation.waitFor(3L);
				
			}
			
			if(!UIFoundation.getText(CartPage.lnkFirstProductSaveForLater).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkFirstProductSaveForLater));
				UIFoundation.waitFor(2L);
				
			}
			boolean isPresent=UIFoundation.isDisplayed(CartPage.spnProductQuantity);
			
			log.info("The execution of the method verifyQuantityPicker ended here ...");
			if (objStatus.contains("false") && isPresent==true ) {
				
				System.out.println("Verify item without quantity picker test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify item without quantity picker test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyQuantityPicker "+ e);
			return "Fail";
		}
	}


}
