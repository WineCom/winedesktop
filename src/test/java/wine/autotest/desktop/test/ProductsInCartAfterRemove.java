package wine.autotest.desktop.test;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;


public class ProductsInCartAfterRemove extends Desktop {
	static ArrayList<String> expectedItemsName=new ArrayList<String>();
	static ArrayList<String> actualItemsName=new ArrayList<String>();
	static String products1=null;
	static String products2=null;
	static String products3=null;
	static String cartCountBeforeLogout=null;
	static String cartCountAfterLogin=null;
	
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart() {
		
		
		String objStatus=null;
		try {
			log.info("The execution of the method addProductsToCart started here ...");
			driver.navigate().refresh();
		//	UIFoundation.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnMainNav));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSort);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			log.info("The execution of the method addProductsToCart ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotalBefore=null;
		String subTotalAfter=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String secondProductPrice=null;
		String thirdProductPrice=null;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before removing products from the cart===============");
			subTotalBefore=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalBefore);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			secondProductPrice=UIFoundation.getText(CartPage.lnkSecondProductPrice);
			thirdProductPrice=UIFoundation.getText(CartPage.lnkRemoveThirdProduct);
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.lnkRemoveThirdProduct).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveThirdProduct));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
				UIFoundation.waitFor(5L);
			}
			if(!UIFoundation.getText(CartPage.lnkRemoveSecondProduct).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemoveSecondProduct));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnRemove));
				UIFoundation.waitFor(5L);
			}

			UIFoundation.waitFor(3L);
			subTotalAfter=UIFoundation.getText(CartPage.spnSubtotal);
			UIBusinessFlows.removerProductPrice(driver, subTotalBefore, subTotalAfter, secondProductPrice, thirdProductPrice);
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary after removing products from the cart===============");
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalAfter);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			
			
			
			////////////////////////////////////////////////////////////////
			System.out.println("============Below Products are available in the CART before logout ===================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			/////////////////////////////////////////////////////////
			cartCountBeforeLogout=UIFoundation.getText(ListPage.btnCartCount);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSignout));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method captureOrdersummary started here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: productsAvailableInTheCart()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String productsAvailableInTheCart()
	{
		String objStatus=null;

		try
		{
			log.info("The execution of the method productsAvailableInTheCartBeforeLogout started here ...");
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Deskusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			///////////////////////////////////////////////////////////////
			System.out.println("============Below Products are available in the CART after login ===================================");
			if(!UIFoundation.getText(CartPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				actualItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				actualItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				actualItemsName.add(products3);
			}
			/////////////////////////////////////////////////////////////////////////////////////
			cartCountAfterLogin=UIFoundation.getText(ListPage.btnCartCount);
			log.info("The execution of the method productsAvailableInTheCartBeforeLogout started here ...");
			if (! (expectedItemsName.containsAll(actualItemsName))   && objStatus.contains("false") && !(cartCountBeforeLogout==cartCountAfterLogin))
			{
				System.out.println("Verify the products remain in the cart after removing few products from the cart test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the products remain in the cart after removing few products from the cart test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productsAvailableInTheCartBeforeLogout "+ e);
			return "Fail";
		}
	}
	

}
