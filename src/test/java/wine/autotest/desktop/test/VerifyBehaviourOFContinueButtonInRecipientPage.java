package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyBehaviourOFContinueButtonInRecipientPage extends Desktop {
	
	/***************************************************************************
	 * Method Name : VerifyBehaviourOFContinueButtonInRecipientPage() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : 
	 * TM-895,
	 ****************************************************************************
	 */

	public static String verifyBehaviourOFContinueButtonInRecipientPage() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.spnnameRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.spnstreetAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.spncityRequired) && UIFoundation.isDisplayed(FinalReviewPage.spnstateRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.spnzipRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.spnphoneRequired) )
			{
				  objStatus+=true;
			      String objDetail="Verified  the behavior of the Continue button, if any mandatory data is not entered in the Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified  the behavior of the Continue button, if any mandatory data is not entered in the Recipient page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the behavior of the Continue button, if any mandatory data is not entered in the Recipient page is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"RecipientAddress", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				   System.err.println("Verify the behavior of the Continue button, if any mandatory data is not entered in the Recipient page is failed ");
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
