package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyEditFunctionalityofShippingAddress extends Desktop {
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String recipientAddressEdit() {
	String objStatus=null;
	String recipientName1=null;
	String recipientName2=null;
	String recipientName3=null;
	String recipientName4=null;
	
		try {
			log.info("The execution of the method recipientAddressEdit started here ...");

			/*UIFoundation.waitFor(4L);
			*/
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkEditShippingAddress));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.spnShippingHeaderMsg));
			log.info("The execution of the method recipientAddressEdit ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify the edit functionality of shipping address test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the edit functionality of shipping address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method recipientAddressEdit "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: preferredAddressEdit()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-2780
	 ****************************************************************************
	 */
	
	public static String preferredAddressEdit() {
	String objStatus=null;
	
	
		try {
			log.info("The execution of the method preferredAddressEdit started here ...");

			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			String PreferredAddresstoedit = UIFoundation.getAttribute(FinalReviewPage.RdoPrefferedAddress2);
			if(PreferredAddresstoedit.contains("false")) {
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSecondAddrEdit));
			UIFoundation.waitFor(2L);
			}
			else {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkThirdAddrEdit));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.spnShippingHeaderMsg));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.ChkPreferredAddressBook));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPreferredSave));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnaddressContinue))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnaddressContinue));
				UIFoundation.waitFor(3L);	
			}
//			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "btnaddressContinue"));
//			UIFoundation.waitFor(5L);
			String ShiptoAddress = UIFoundation.getAttribute(FinalReviewPage.RdoShiptoAddress);
			String PreferredAddress = UIFoundation.getAttribute(FinalReviewPage.RdoPrefferedAddress);
			if(ShiptoAddress.contains("true") && PreferredAddress.contains("true") )
			{
				objStatus+=true;
				String objDetail = "Preffered Address is Selected as Default Ship to Address";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Preffered Address is NOT Selected as Default Ship to Address";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			
			log.info("The execution of the method recipientAddressEdit ended here ...");
			if (objStatus.contains("false")) {
				String objDetail = "Verify the edit functionality of preferred shipping address test case is failed";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
			} else {
				String objDetail = "Verify the edit functionality of preferred shipping address test case is executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method preferredAddressEdit "
					+ e);
			return "Fail";
		}
	}
	

}
