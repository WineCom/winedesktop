
package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class AddToCartForRecomondedProducts extends Desktop {
	
	
	/***************************************************************************
	 * Method Name			: verifyAddToCartButtonForRecommendedProducts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * TC ID				: TM-289
	 ****************************************************************************
	 */
	
	
	public static String verifyAddToCartButtonForRecommendedProducts()
	{
		String objStatus = null;
		 
		try {
			log.info("The execution of the method verifyAddToCartButtonForRecommendedProducts started here ...");
			
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkhomePageRecommendedProducts1);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkhomePageRecommendedProducts1));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addToCartAlert));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkhomePageRecommendedProducts2);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkhomePageRecommendedProducts2));
		//	UIFoundation.scrollDownOrUpToParticularElement(ListPage.addToCartAlert);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addToCartAlert));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkhomePageRecommendedProducts3);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkhomePageRecommendedProducts3));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addToCartAlert));
			UIFoundation.waitFor(1L);
			
			if(!UIFoundation.isDisplayed(LoginPage.btnMainNav))
			 {
			    driver.navigate().refresh();
			    UIFoundation.waitFor(2L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnMainNav));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkmerlotSecProd);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkmerlotSecProd));
			UIFoundation.waitFor(2L);
			
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
			UIFoundation.waitFor(2L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addRecommendedProducts));		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.addRecommendedProducts));
			UIFoundation.waitFor(1L);		
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.addToCartAlert);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addToCartAlert));
			UIFoundation.waitFor(2L);	
			UIFoundation.scrollUp(driver);
	    	UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnAddToCart);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnAddToCart));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkmerlotSecProd));
	    	UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.imguserRatings));
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.imguserRatings));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.clearStarRating));
			UIFoundation.waitFor(2L);	
		//	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			
			log.info("The execution of the method verifyAddToCartButtonForRecommendedProducts ended here ...");
			
			if (objStatus.contains("false")) {
				String objDetail="Add to cart Button displayed and user is not able to add the recommended Product to the Cart " ;
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");

				return "Fail";
				
			} else {
				String objDetail="Add to cart Button displayed and user is able to add the recommended  product to the Cart " ;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}			
		
		} catch (Exception e) {
			return "Fail";
		}
	}	
	
	
	/***************************************************************************
	 * Method Name			: rateTheStarsAndVerifyRecommendedProducts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * TC ID				: TM-292
	 ****************************************************************************
	 */
	

	public static String rateTheStarsAndVerifyRecommendedProducts()
	{
		String objStatus = null;
		 
		try {
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts started here ...");
			
			UIFoundation.waitFor(5L);
			driver.get("https://qwww.wine.com/product/substance-cabernet-sauvignon-2016/414487");
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));

	//		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
			UIFoundation.waitFor(2L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addRecommendedProducts));		
		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.addRecommendedProducts));
			UIFoundation.waitFor(1L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.clearStarRating));
			UIFoundation.waitFor(2L);
						
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts ended here ...");
			
			if (objStatus.contains("false")) {
				String objDetail="A Recommended product is not displayed beneath the rating stars " ;
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				      return "Fail";
				
			} else {
				
				String objDetail="A Recommended products displayed beneath the rating stars " ;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
							return "Pass";
			}
			
		
		} catch (Exception e) {
			return "Fail";
		}
	}	

	
	
	/***************************************************************************
	 * Method Name			: verifyTheCumuletiveRatingChanges()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * TC ID				: TM-3648
	 ****************************************************************************
	 */
	

	public static String verifyTheCumuletiveRatingChanges()
	{
		String objStatus = null;
		 
		try {
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts started here ...");
			
			if(!UIFoundation.isDisplayed(LoginPage.btnMainNav))
			 {
			    driver.navigate().refresh();
			    UIFoundation.waitFor(2L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnMainNav));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkmerlotSecProd);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkmerlotSecProd));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct4));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkratedProduct4));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct2));
			UIFoundation.waitFor(1L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkratedProduct2));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addRecommendedProducts));		
		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.addRecommendedProducts));
			UIFoundation.waitFor(1L);		
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.addToCartAlert);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.addToCartAlert));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.imguserRatings));
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.imguserRatings));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.clearStarRating));
		
			
			log.info("The execution of the method rateTheStarsAndVerifyRecommendedProducts ended here ...");
			
			if (objStatus.contains("false")) {
				System.out.println("Recommended products is not displayed beneath the rating stars");
				return "Fail";
				
			} else {
				System.out.println("Recommended products is  displayed beneath the rating stars");
				return "Pass";
			}
			
		
		} catch (Exception e) {
			return "Fail";
		}
	}	

	
}
