package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifySuggestedShippingAddressWidget extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String verifySuggestedAddress() {
		String objStatus = null;
		   String screenshotName = "Scenarios__Screenshot.jpeg";
				
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			boolean radioButtonShipToHome=UIFoundation.isDisplayed(FinalReviewPage.rdoShipToHome);
			if(radioButtonShipToHome)
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Ship to Home or Address' radio button is checked by default.", "Pass", "");
				logger.pass("Ship to Home or Address' radio button is checked by default");
				System.out.println("Ship to Home or Address' radio button is checked by default");
			}
			else
			{
				objStatus+=false;
				String objDetail="'Ship to Home or Address' radio button is not checked by default";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				//System.out.println("'Ship to Home or Address' radio button is not checked by default");
			}
			boolean radioButtonShipToFedex=UIFoundation.isDisplayed(FinalReviewPage.rdoShipToFedEx);
			if(radioButtonShipToFedex)
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Ship to FedEx Pickup Location radio button is displayed", "Pass", "");
				logger.pass("Ship to FedEx Pickup Location radio button is displayed");
				System.out.println("Ship to FedEx Pickup Location radio button is displayed");
			}
			else
			{
				objStatus+=false;
				String objDetail="Ship to FedEx Pickup Location radio button is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail);
				System.out.println("Ship to FedEx Pickup Location radio button is not displayed");
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
				String borderHighlighted = driver.findElement(By.xpath("(//li[@class='userDataCard_listItem shippingAddress_listItem js-is-selected'])[1]")).getCssValue("border-style");
			if(borderHighlighted.contains("solid"))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Suggested address dialog box is highlighted", "Pass", "");
				logger.pass("Suggested address dialog box is highlighted");
				System.out.println("Suggested address dialog box is highlighted");
			}else
			{
				objStatus+=false;
				String objDetail="Suggested address dialog box is not highlighted";
				UIFoundation.captureScreenShot(screenshotpath+"dailog", objDetail);
				logger.fail(objDetail);
				System.out.println("Suggested address dialog box is not highlighted");
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			}
			
			boolean radioButton=UIFoundation.isSelected(FinalReviewPage.RdoShiptoAddress);
			if(radioButton)
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Suggested address radio button is selected by default", "Pass", "");
				logger.pass("Suggested address radio button is selected by default");
				System.out.println("Suggested address radio button is selected by default");
			}
			else
			{
				objStatus+=false;
				String objDetail="Suggested address radio button is not selected by default";
				UIFoundation.captureScreenShot(screenshotpath+"radBtn", objDetail);
				logger.fail(objDetail);
				System.out.println("Suggested address radio button is not selected by default");
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
				System.out.println("Verify suggested address dialog test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify suggested address dialog test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}


}
