package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated extends Desktop {

	/*****************************************************************************************
	 * Method Name : VerifyFedexDisplaysProperGoogleAdresses Created By : Reviewed
	 * By : Purpose : TM-3966
	 *******************************************************************************************
	 */

	public static String verifyAddressSuggestioIsDisplayed() {

		String objStatus = null;
		String suggestedAddress = null;
		String ScreenshotName = "VerifyAddressSuggestioIsDisplayed.jpeg";
		
		try {
			log.info("Verify Address Suggestio Is Displayed Method Started Here");
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(10L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkChangeAddress))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressR"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipR"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.spnSuggestedAddressR)) {
				objStatus += true;
				ReportUtil.addTestStepsDetails("verified that suggested addres is displayed in verify Shopping address","Pass", "");
				suggestedAddress = UIFoundation.getText(FinalReviewPage.lnkSugestedAddressDisply);
				System.out.println("Address displayed in Verify Shipping Address: " + suggestedAddress);
				logger.pass("verified that suggested addres is displayed in verify Shopping address");
			} else {
				objStatus += false;
				String objDetail = "Suggested address is not displayed and address is autoupdated";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("Verify Address Suggestion Is Displayed Method Ended Here");
			if (objStatus.contains("false")) {

				System.out.println("Verification of  Address Suggestion test case failed");
				return "fail";
			} else {
				System.out.println("Verification of  Address Suggestion test case passed");
				return "Pass";

			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method execution " + e);
			return "Fail";

		}

	}

}
