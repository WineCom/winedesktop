package wine.autotest.desktop.test;

import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyMenuVarietalFilters extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyVarietalMenu()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String verifyVarietalMenu() {
		 String objStatus=null;
		
		String expectedVarietalFilter[]={"Red Wine","White Wine","Champagne & Sparkling","Ros� Wine","Dessert, Sherry & Port","Sak�"};
		 ArrayList<String> expectedVarietalFilters=new ArrayList<String>(Arrays.asList(expectedVarietalFilter));
		 ArrayList<String> actualVarietalFilters=new ArrayList<String>();
		 String varietalFilter1=null;
		 String varietalFilter2=null;
		 String varietalFilter3=null;
		 String varietalFilter4=null;
		 String varietalFilter5=null;
		 String varietalFilter6=null;
		   String screenshotName = "Scenarios__VarietalScreenshot.jpeg";
					
		try {
			log.info("The execution of method verifyVarietalMenu started here");
			
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFilterMenuIcon));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkVarietalR));
			UIFoundation.waitFor(6L);
			varietalFilter1=UIFoundation.getText(ListPage.spnFirstRefinementWidget);
			varietalFilter2=UIFoundation.getText(ListPage.spnSecondRefinementWidget);
			varietalFilter3=UIFoundation.getText(ListPage.spnThirdRefinementWidget);
			varietalFilter4=UIFoundation.getText(ListPage.spnFourthRefinementWidget);
			varietalFilter5=UIFoundation.getText(ListPage.spnFifthRefinementWidget);
			varietalFilter6=UIFoundation.getText(ListPage.spnSixthRefinementWidget);
			actualVarietalFilters.add(varietalFilter1);
			actualVarietalFilters.add(varietalFilter2);
			actualVarietalFilters.add(varietalFilter3);
			actualVarietalFilters.add(varietalFilter4);
			actualVarietalFilters.add(varietalFilter5);
			actualVarietalFilters.add(varietalFilter6);
			System.out.println("exp:"+expectedVarietalFilters);
			System.out.println("act:"+actualVarietalFilters);
			log.info("The execution of the method verifyVarietalMenu ended here ...");
			if(expectedVarietalFilters.containsAll(actualVarietalFilters))
			{
				ReportUtil.addTestStepsDetails("Shipping and Handling and stewardship discount matched.", "Pass", "");
				System.out.println("Verify the sub menu elements under varietal filters test case is executed successfully");
				logger.pass("Verify the sub menu elements under varietal filters test case is executed successfully");
				return "Pass";
				
			}else
			{
				String objDetail="Verify the sub menu elements under varietal filters test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail);
				System.out.println("Verify the sub menu elements under varietal filters test case is failed");
				return "Fail";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyVarietalMenu "
					+ e);
			return "Fail";
		}
	}

}
