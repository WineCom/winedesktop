package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class AddAddressUsingUserProfileServicesForExistingUser extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addAddress()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 ****************************************************************************
	 */

	public static String addAddress()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
			
		try
		{
			log.info("The execution of the method addAddress started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtAddressFullName, "fullName"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtStreetAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCity, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtRecipientZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnAddressSave);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnAddressSave));
			UIFoundation.waitFor(3L);
/*			UIFoundation.scrollDownOrUpToParticularElement(driver, "SuggestedAddress");
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));*/
			UIFoundation.waitFor(5L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnVerifyContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnVerifyContinue));
			UIFoundation.waitFor(10L);
			UIFoundation.clckObject(UserProfilePage.rdoSuggestedAddress);
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnAddressBookHeader)){
				 	objStatus+=true;
			      String objDetail="Address Book Header is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      System.out.println("Address Book Header is displayed");
			}else{
				objStatus+=false;
			       String objDetail="Address Book Header is not displayed";
			       UIFoundation.captureScreenShot(screenshotpath+"alert", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false"))
			{
				System.out.println("Adding address using user profile services for existing user test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Adding address using user profile services for existing user test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addAddress "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: addAddressInAddressBook()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-442
	 ****************************************************************************
	 */

	
	public static String addAddressInAddressBook()
	{
		String objStatus=null;
		   String screenshotName = "AddressINAddressBook_Screenshot.jpeg";
			
		try
		{
			log.info("The execution of the method addAddress started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtAddressFullName, "fullName"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtStreetAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCity, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtRecipientZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.chkPreferredAddBook);
			objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.chkPreferredAddBook));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnAddressSave);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnAddressSave));
			UIFoundation.waitFor(8L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnVerifyContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnVerifyContinue));
			UIFoundation.waitFor(10L);
				
			log.info("The execution of the method addAddressInAddressBook ended here ...");	
			if (objStatus.contains("false"))
			{
				String objDetail = "Adding address using user profile services for existing user test case is failed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail = "Adding address using user profile services for existing user test case is executed successfully";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addAddress "+ e);
			return "Fail";
		}
	}


}
