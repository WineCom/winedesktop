package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class FedexLocationForUPSStatesInRecipientSection extends Desktop {
	
	/***************************************************************************
	 * Method Name			: fedexLocationForUPSStatesInRecipientSection()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */

	public static String fedexLocationForUPSStatesInRecipientSection() {
		String objStatus=null;
		   String screenshotName = "Scenarios_fedexPickUpLocation_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of the method fedexLocationForUPSStatesInRecipientSection started here ...");
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)){
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "fedExUPSZipcode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.spnfedexLocalPickUpIsNotAvailable))
			{
				 objStatus+=true;
			      String objDetail="Verified the error message is displayed on adding the new FedEx address with UPS state zipcode";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified the error message is displayed on adding the new FedEx address with UPS stat zipcode");
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the error message is displayed on adding the new FedEx address with UPS stat zipcode  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"PaymentSection", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   System.err.println("Verify the error message is displayed on adding the new FedEx address with UPS stat zipcode is failed ");
			}
			log.info("The execution of the method fedexLocationForUPSStatesInRecipientSection ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method fedexLocationForUPSStatesInRecipientSection "
					+ e);
			return "Fail";
		}

	}

}
