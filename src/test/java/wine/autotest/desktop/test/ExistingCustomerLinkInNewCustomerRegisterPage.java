package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class ExistingCustomerLinkInNewCustomerRegisterPage extends Desktop {
	
	/***************************************************************************
	 * Method Name			: ExistingCustomerLinkInNewCustomerRegisterPage()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String existingCustomerLinkInNewCustomerRegisterPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios_SignInNav_Screenshot.jpeg";
			
		try {
			log.info("The execution of method existingCustomerLinkInNewCustomerRegisterPage started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.txtsignIn))
			{
				  objStatus+=true;
			      String objDetail="on clicking on the Sign In link user is navigated to the sign in page.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			     
			}else{
				objStatus+=false;
				 String objDetail="on clicking on the Sign In link user is not navigated to the sign in page.";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method existingCustomerLinkInNewCustomerRegisterPage ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Existing customer should be a link in new customer register page  test case is failed");
				return "Fail";
			} else {
				System.out.println("Existing customer should be a link in new customer register page  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Existing customer should be a link in new customer register page  test case is failed");
			log.error("there is an exception arised during the execution of the method existingCustomerLinkInNewCustomerRegisterPage "
					+ e);
			return "Fail";
		}
	}

}
