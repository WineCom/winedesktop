package wine.autotest.desktop.test;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class Verify30LocationsAreDispInFedexAddress extends Desktop {
	/***************************************************************************
	 * Method Name : login() Created By : Vishwanath Chavan Reviewed By : Ramesh,KB
	 * Purpose : The purpose of this method is Login into the Wine.com Application
	 ****************************************************************************
	 */

	public static String login() {
		String objStatus = null;

		try {
			log.info("The execution of the method login started here ...");
			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "fedexloactions"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Login test case is failed");
				return "Fail";
			} else {
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method login " + e);
			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name : VerifyAddToMyWineFunctionality(NFP-4253) Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */

	public static String verify30LocationsAreDispInFedexAddress() {
		String objStatus = null;
		String ScreenshotName = "Verify30LocationsAreDispInFedexAddress.jpeg";
		
		try {
			log.info("Verify 30Locations Are Disp In FedexAddress Method started here........");
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "CartBtnCount"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeWA"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(4L);
			List<WebElement> fedLoc = driver.findElements(
					By.xpath("//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded ']"));

			if (fedLoc.size() == 30) {
				objStatus += true;
				String objDetail = "30 locations are displayed in 'Find Local Pickup Locations' under 'Recipient' section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "30 locations are not displayed in 'Find Local Pickup Locations' under 'Recipient' section";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("Verify 30Locations Are Disp In FedexAddress Method started here..................");
			if (objStatus.contains("false")) {
				System.out.println("Verify30LocationsAreDispInFedexAddress test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify30LocationsAreDispInFedexAddress test case executed succesfully");

				return "Pass";

			}

		} catch (Exception e) {
			return "Fail";

		}
	}
}
