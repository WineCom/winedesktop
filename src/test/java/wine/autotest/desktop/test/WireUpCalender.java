package wine.autotest.desktop.test;


import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.*;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class WireUpCalender extends Desktop {
	
	/***************************************************************************
	 * Method Name			: wireUpCalender()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String wireUpCalender() {
	
	String objStatus=null;

	   String screenshotName = "Scenarios_wireUpCalender_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method wireUpCalender started here ...");
			UIFoundation.waitFor(4L);
			String expectedBgColorCurrentDate=verifyexpectedresult.currentDateBgColor;
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeDate))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
				UIFoundation.waitFor(2L);
			}
			
			String deliveryHeaderDate=UIFoundation.getText(FinalReviewPage.spndeliveryHeaderDate);
			deliveryHeaderDate=deliveryHeaderDate.replaceAll("[^0-9]", "");
			int deliveryHeadrDate=Integer.parseInt(deliveryHeaderDate);
			String calenderTodayDate=UIFoundation.getText(FinalReviewPage.spncurrentDate);
			int todayCalDate=Integer.parseInt(calenderTodayDate);
			
			WebElement ele=UIFoundation.webelemnt(FinalReviewPage.spncurrentDate);
			String ActualBgColorCurrentDate=UIFoundation.getColor(FinalReviewPage.spncurrentDate);
			if((deliveryHeadrDate==todayCalDate) && (ele.isEnabled()) && (expectedBgColorCurrentDate.equalsIgnoreCase(ActualBgColorCurrentDate))){
				objStatus+=true;
				String objDetail="Available days depend on the Shipping Method, are shown in a distinct color, and are selectable";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Available days depend on the Shipping Method, are shown in a distinct color, and are selectable");
			}else{
				objStatus+=false;
				String objDetail="Available days depend on the Shipping Method, are shown in a distinct color, and are not selectable";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Available days depend on the Shipping Method, are shown in a distinct color, and are not selectable");
			}
			
			WebElement ele1=UIFoundation.webelemnt(FinalReviewPage.spnunavailableDate);
			System.out.println("ele1 : "+ele1);
			String expectedBgColorPreviousDate=verifyexpectedresult.previousDateBgColor;
			String actualdBgColorPreviousDate=UIFoundation.getColor(FinalReviewPage.spnunavailableDate);
	//		boolean status=UIFoundation.isSelected(FinalReviewPage.spnunavailableDate);
			if((ele1.isEnabled())){
				objStatus+=true;
				String objDetail="Unavailable dates are cannot be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Unavailable dates are cannot be selected");
			}else{
				objStatus+=false;
				String objDetail="Unavailable dates are can be selected";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Unavailable dates are  can be selected");
			}
			if(expectedBgColorPreviousDate.equalsIgnoreCase(actualdBgColorPreviousDate)){
				objStatus+=true;
				String objDetail="Unavailable dates are  greyed out";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Unavailable dates are greyed out");
			}else{
				objStatus+=false;
				String objDetail="Unavailable dates are not greyed out";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Unavailable dates are not greyed out");
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnnextAvailableDate));
			UIFoundation.waitFor(2L);
			String deliveryHeaderDateNext=UIFoundation.getText(FinalReviewPage.spndeliveryHeaderDate);
			System.out.println("deliveryHeaderDateNext : "+deliveryHeaderDateNext);
			deliveryHeaderDateNext=deliveryHeaderDateNext.replaceAll("[^0-9]", "");
			int deliveryHeaderDateNxt=Integer.parseInt(deliveryHeaderDateNext);
			String calenderNextDate=UIFoundation.getText(FinalReviewPage.spncurrentDate);
			int calenderNxtDate=Integer.parseInt(calenderNextDate);
			if(deliveryHeaderDateNxt==calenderNxtDate){
				objStatus+=true;
				String objDetail="If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are changed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are changed");
			}else{
				objStatus+=false;
				String objDetail="If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are not changed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+"nextDate", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are not changed");
			}
			System.out.println("objStatus: "+objStatus);
			log.info("The execution of the method wireUpCalender ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Wire up calendar test case is failed");
				return "Fail";
			} else {
				System.out.println("Wire up calendar test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			objStatus+=false;
			String objDetail="Exception : Wire up calendar test case is failed";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
