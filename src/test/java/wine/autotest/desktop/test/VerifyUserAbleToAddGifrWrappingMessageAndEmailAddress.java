package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;


public class VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress extends Desktop {

	/***************************************************************************
	 * Method Name : verifyTheUserAbleToAddGiftAddress() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * Jira id     : TM-4187
	 ****************************************************************************
	 */

	public static String verifyTheUserAbleToAddGiftAddress() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyTheUserAbleToAddGiftAddress_Screenshot.jpeg";
		try {
			log.info("The execution of the method verifyTheUserAbleToAddGiftAddress started here ...");		
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			else 
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutAlter));
			}
            UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(1L);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnReceiveState, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
			UIFoundation.waitFor(2L);
			WebElement gift = driver.findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift.isSelected()){
				UIFoundation.clickObject(FinalReviewPage.chkRecipientGift);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtgiftMessageTextArea) && UIFoundation.isDisplayed(FinalReviewPage.txtReceipientEmail))
			{				
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtReceipientEmail);
				UIFoundation.waitFor(2L); 
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "giftBagUserName"));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessage"));
				UIFoundation.waitFor(1L);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.rdogiftbag3_99);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdogiftbag3_99));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));	
				objStatus+=true;
				String objDetail="User is able to add the gift email,Gift message and select Gift wrap";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("User is able to add the gift email,Gift message and select Gift wrap");
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not able to add the gift email,Gift message and select Gift wrap is failed";
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}	
			log.info("The execution of the method verifyTheUserAbleToAddGiftAddress ended here ...");
			if (objStatus.contains("false")) 
			{
				return "Fail";
			} 
			else
			{
				return "Pass";
			}
		} 
		catch (Exception e) 
		{
			log.error("there is an exception arised during the execution of the method verifyTheUserAbleToAddGiftAddress "+ e);
			return "Fail";
		}
	}
}
