package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.UIFoundation;

public class ValidateRedWhiteVarietalPreffinAnythingElseSec extends Desktop {

	/***********************************************************************************************************
	 * Method Name : VerifyLikeandDislikeinAnythingElseSec() 
	 * Created By  : Ramesh S
	 * * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify Likeed and Disliked Red/White Wine in Anything else section' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String VerifyLikeandDislikeinAnythingElseSec() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkRedandWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWhitewinePagenNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.spnWhiteWineAnythingelselikeZinfandel) && UIFoundation.isDisplayed(PickedPage.SpnWhiteWineAnythingelselikeChardonnay))
		      { 
				  objStatus+=true;
				  String objDetail="Liked Red/White wine is present in LIKED section ";
				  logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println(objDetail); 
		      }
			else 
			{
		    	  objStatus+=false;
			       String objDetail="Liked Red/White wine is not present in LIKED section ";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
		      }		
			if(UIFoundation.isDisplayed(PickedPage.SpnWhiteWineAnythingelseDislikeSauvignonBlanc) && UIFoundation.isDisplayed(PickedPage.SpnWhiteWineAnythingelseDislikeMalbec))
		      { 
				  objStatus+=true;
				  String objDetail="DisLiked Red/White wine is present in DISLIKED section ";
				  logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println(objDetail); 
		      }
			else
			{
		    	  objStatus+=false;
			       String objDetail="DisLiked Red/White wine is not present in DISLIKED section ";
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
		      }		
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
				objStatus+=true;
				String objDetail="User is navigated to adventurous quiz next page";
				  logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to adventurous quiz next page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));
			UIFoundation.waitFor(1L);		
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				  logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				  logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAddRedBottleCount));
			UIFoundation.waitFor(1L);
			String strRedBottleCount = UIFoundation.getText(PickedPage.spnRedCount);
			if(strRedBottleCount!=null) {
			ReportUtil.addTestStepsDetails("Increased Red Bottle Count"+strRedBottleCount, "Pass", "");
			}
			else 
			{
				ReportUtil.addTestStepsDetails("Red Bottle Count Not Increased"+strRedBottleCount, "Fail", "");
			}
			System.out.println("strRedBottleCount :"+strRedBottleCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnaddRedWinePrice));
			UIFoundation.waitFor(1L);
			String strRedWinePrice = UIFoundation.getText(PickedPage.spnRedWinePrice);
			ReportUtil.addTestStepsDetails("Red Bottle Price"+strRedWinePrice, "Pass", "");
			System.out.println("strRedBottlePrice :"+strRedWinePrice.substring(1));
			String strRedWinetotalPrice = UIFoundation.getText(PickedPage.spnRedWineTotalPrice);
			System.out.println("strRedBottleTotalPrice :"+strRedWinetotalPrice);
			ReportUtil.addTestStepsDetails("Total Red Bottle Price"+strRedWinePrice, "Pass", "");
			int RedBottelCount = Integer.parseInt(strRedBottleCount);
			int RedWinePrice = Integer.parseInt(strRedWinePrice.substring(1));
			int RedWinetotalPrice = Integer.parseInt(strRedWinetotalPrice);
			int MultiplytotalRed = RedBottelCount*RedWinePrice;
			System.out.println("RedBottelCount :"+RedBottelCount+"  "+"RedWinePrice :"+RedWinePrice+"  "+"Multiplytotal :"+MultiplytotalRed);
			if(MultiplytotalRed==RedWinetotalPrice) {
				objStatus+=true;
				String objDetail="The total Red Wine price is multiple of Quantity and Bottle Price";
				  logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println(objDetail);
			}
			else 
			{
				objStatus+=false;
				String objDetail="The total Red Wine price is not multiple of Quantity and Bottle Price";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			//White Wine calculation
			UIFoundation.waitFor(1L);
			String strWhiteBottleCount = UIFoundation.getText(PickedPage.spnWhiteCount);
			if(strWhiteBottleCount!=null) {
			ReportUtil.addTestStepsDetails("Decreased  White Bottle Count "+strWhiteBottleCount, "Pass", "");
			}
			else
{
				ReportUtil.addTestStepsDetails("White Bottle Count Not Decreased "+strWhiteBottleCount, "Fail", "");
			}
			System.out.println("strWhiteBottleCount :"+strWhiteBottleCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSubtractWhiteWinePrice));
			UIFoundation.waitFor(1L);
			String strWhiteWinePrice = UIFoundation.getText(PickedPage.spnWhiteWinePrice);
			ReportUtil.addTestStepsDetails("White Bottle Price"+strWhiteWinePrice, "Pass", "");
			System.out.println("strWhiteWinePrice :"+strWhiteWinePrice.substring(1));
			String strWhiteWinetotalPrice = UIFoundation.getText(PickedPage.spnWhiteWineTotalPrice);
			System.out.println("strWhiteWinetotalPrice :"+strWhiteWinetotalPrice);
			ReportUtil.addTestStepsDetails("Total White Bottle Price"+strWhiteWinetotalPrice, "Pass", "");
			int WhiteBottelCount = Integer.parseInt(strWhiteBottleCount);
			int WhiteWinePrice = Integer.parseInt(strWhiteWinePrice.substring(1));
			int WhiteWinetotalPrice = Integer.parseInt(strWhiteWinetotalPrice);
			int MultiplytotalWhite = WhiteBottelCount*WhiteWinePrice;
			System.out.println("WhiteBottelCount :"+WhiteBottelCount+"  "+"WhiteWinePrice :"+WhiteWinePrice+"  "+"MultiplytotalWhite :"+MultiplytotalWhite);
			if(MultiplytotalWhite==WhiteWinetotalPrice) {
				objStatus+=true;
				String objDetail="The total White Wine price is multiple of Quantity and Bottle Price";
				  logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="The total White Wine price is not multiple of Quantity and Bottle Price";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}	
			String strTargetPrice = UIFoundation.getText(PickedPage.spnTargetPrice);
			if(strTargetPrice!=null) {
				ReportUtil.addTestStepsDetails("Target Price Exist in Quantity Selection page "+strTargetPrice, "Pass", "");
				}
			else
			{
					ReportUtil.addTestStepsDetails("Target Price Not-Exist in Quantity Selection page "+strTargetPrice, "Fail", "");
				}
			int intTargetPrice = Integer.parseInt(strTargetPrice);
			int totalTargetPrice = MultiplytotalRed+MultiplytotalWhite;
			ReportUtil.addTestStepsDetails("Total Target Price Calculated is "+intTargetPrice, "Pass", "");
			if(intTargetPrice==totalTargetPrice) {
				objStatus+=true;
				String objDetail="The total target price matches with calculated price";
				  logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="The total target price not matches with calculated price";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Validate Liked and Disliked Red/White wine in Anything Else Section test case failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Validate Liked and Disliked Red/White wine in Anything Else Section test case executed successfully";
				  logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}