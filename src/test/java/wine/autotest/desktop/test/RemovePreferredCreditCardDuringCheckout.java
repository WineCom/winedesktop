package wine.autotest.desktop.test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;


public class RemovePreferredCreditCardDuringCheckout extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-2197,
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 70);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 70);
				
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		   String screenshotName = "Scenarios_removeCreditCard_Screenshot.jpeg";
			
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			if(UIFoundation.isSelected(FinalReviewPage.rdopreferredAddress)){
				objStatus+=true;
			      String objDetail="Preferred card is selected";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				  objStatus+=false;
				   String objDetail="Credit card is not a preferred card";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"preferredAdr", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkeditCreditCardInPayment));
			UIFoundation.waitFor(1L);
			UIFoundation.clearField(FinalReviewPage.lnkremvePhoneNumInPayment);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkremoveCreditCardInPayment));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnYesRemove));
			if(UIFoundation.isDisplayed(FinalReviewPage.spncreditCardWarningMsgInPayment))
			{
				  objStatus+=true;
			      String objDetail="Preferred card is successfully removed from stored Payment options";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				 
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Preferred card is not removed from stored Payment options";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}

}
