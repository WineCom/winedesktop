package wine.autotest.desktop.test;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyProductsAreListedInAlphabeticalOrder extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyProductsAreListedInAlphabeticalOrder()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * TM-549
	 ****************************************************************************
	 */
	public static boolean verifyProductsAreListedInAlphabeticalOrder()
	{
		WebElement ele=null;
		String objStatus=null;
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
			String screenshotName = "Scenarios_AlphabeticalOrder_Screenshot.jpeg";
			
		try
		{
			
			if(!UIFoundation.getText(ListPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductInCart);
				arrayList.add(products1.replaceAll("[0-9]", ""));
				arrayListYr.add(products1.replaceAll("[^0-9]", ""));
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductInCart);
				arrayList.add(products2.replaceAll("[0-9]", ""));
				arrayListYr.add(products2.replaceAll("[^0-9]", ""));
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductInCart);
				arrayList.add(products3.replaceAll("[0-9]", ""));
				arrayListYr.add(products3.replaceAll("[^0-9]", ""));
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductInCart);
				arrayListYr.add(products4.replaceAll("[^0-9]", ""));
				arrayList.add(products4.replaceAll("[0-9]", ""));
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductInCart);
				arrayList.add(products5.replaceAll("[0-9]", ""));
				arrayListYr.add(products5.replaceAll("[^0-9]", ""));
				System.out.println("5) "+products5);

			}
		
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	 objStatus+=true;
				      String objDetail="Products are listed in alphabetical order";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
			    	  System.out.println("Products are listed in alphabetical order");
					return true;
			    }
			    else
			    {
			    	objStatus+=false;
				       String objDetail="Products names are not sorted in alphabetical order";
			    	System.err.println("Products names are not sorted in alphabetical order");
			    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}

}
