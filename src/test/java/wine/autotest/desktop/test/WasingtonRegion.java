package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class WasingtonRegion extends Desktop {
	
	/***************************************************************************
	 * Method Name			: VerifyPromoBarInWashington()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * TM-3229
	 ****************************************************************************
	 */
	
	public static String VerifyPromoBarInWashington()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__PromoBar.jpeg";
					
		try
		{
			log.info("The execution of method productFilteration started here");
			/*objStatus += String.valueOf(UIFoundation.mouseHover(driver, "RegionTab"));
			objStatus += String.valueOf(UIFoundation.clckObject(driver, "washingtonRegion"));*/
			driver.navigate().to("https://qwww.wine.com/list/list-washington-wines/wine/1122-7155#");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkheroImagePlusSymbol));
			
			UIFoundation.waitFor(5L);
		  String	expPromoBar = verifyexpectedresult.actPromoBarText;
		String actPromoBar = UIFoundation.getText(ListPage.promoBarWashingtonReg);
			if(expPromoBar.contains(actPromoBar))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verifiied the promo bar in washington Region", "Pass", "");
				logger.pass("Try vintage product is  added to the cart");
				System.out.println("Try vintage product is  added to the cart");
				
			}
			else
			{
				objStatus+=false;
				String objDetail="Verify the promo bar in washington Region is failed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+"washington", objDetail);
		
			}
			objStatus+= String.valueOf(UIBusinessFlows.isObjectExistForList());
			if (objStatus.contains("false"))
			{
				
				System.out.println(" Verify the 'Promo Bar' in list page when Washington is selected as Region test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println(" Verify the 'Promo Bar' in list page when Washington is selected as Region test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method VerifyPromoBarInWashington "+e);
			return "Fail";
		}
	}

}
