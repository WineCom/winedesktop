package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class ViewAllWineForDifferentStates extends Desktop{
	/***************************************************************************
	 * Method Name			: viewAllWineForDifferentStates()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String viewAllWineForDifferentStates()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_viewAllWineForDifferentStates.jpeg";
			
		try
		{
			
			UIFoundation.waitFor(5L);
			log.info("The execution of the method viewAllWineForDifferentStates started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddSecondProductToPIP));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkviewAllWine));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkviewAllWine));
			UIFoundation.waitFor(2L);
			String expectedListPageURL=verifyexpectedresult.viewAllWineUrl;
			String actualListPageUrlBefore=driver.getCurrentUrl();
			//System.out.println("befor:"+actualUrl);
			UIFoundation.waitFor(2L);
			if(actualListPageUrlBefore.contains(expectedListPageURL))
			{
				objStatus+=true;
				String objDetail="User is navigated to the winery list page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="User is unable navigated to the winery list page";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddSecondProductToPIP));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.SelectObject(LoginPage.dwnSelectState, "AllWineState"));
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkviewAllWine));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkviewAllWine));
			UIFoundation.waitFor(2L);
			String actualListPageUrlAfter=driver.getCurrentUrl();
			UIFoundation.waitFor(2L);
			if(actualListPageUrlBefore.equalsIgnoreCase(actualListPageUrlAfter)){
				objStatus+=true;
				String objDetail="User is navigated to the smae list page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="User is unable navigated to the same list page";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"sameListPage", objDetail);
			}
			log.info("The execution of the method viewAllWineForDifferentStates ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method viewAllWineForDifferentStates "+ e);
			return "Fail";
			
		}
	}	

}
