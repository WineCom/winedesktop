package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.PickedPage;


public class ValidateEnrollmentFlowforRedWine extends Desktop {


	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWine() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String enrollForPickedUpWine() {
		String objStatus = null;
		String screenshotName = "Scenarios_enrollForPickedUpWine_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				System.out.println("Verify SignUp Functionalities Flow test case failed");
				String objDetail="Verify SignUp Functionalities Flow test case failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				System.out.println("Verify SignUp Functionalities Flow test case executed successfully");
				String objDetail="Verify SignUp Functionalities Flow test case executed successfully";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	
	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyLocalPickUpForPickedUpWine() {		
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.txtRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickedSetZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnLocalPickupSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocation));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkAcceptTermsandConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
//				UIFoundation.webDriverWaitForElement(OMSPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}
				else
				{
					objStatus+=false;
					String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					UIFoundation.captureScreenShot(screenshotpath, objDetail);
				}
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
}