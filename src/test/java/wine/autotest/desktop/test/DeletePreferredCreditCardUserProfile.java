package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.UIFoundation;

public class DeletePreferredCreditCardUserProfile extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addPaymentMethod()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 ****************************************************************************
	 */

	public static String addPaymentMethod()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__PaymentMethod.jpeg";
				
		try
		{
			log.info("The execution of the method addAddress started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPaymentMethods));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewCard));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCVV, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingCty, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnBillingSelState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.chkmakeThisPreferredCard));	
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.lnkcontactUs);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnBillingSave));
//			UIFoundation.webDriverWaitForElement(UserProfilePage.btnBillingSave, "Invisible", "", 50);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkcreditCardEdit));
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkremoveThisCardPaymnt));
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.lnkcontactUs);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnYesRemove));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnyouDoNotHaveCreditCard)){
				  objStatus+=true;
			      String objDetail="The Preferred Card is removed, user is redirected to the Payment methods page";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				 objStatus+=false;
				   String objDetail="The Preferred Card is not removed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") && (!UIFoundation.isDisplayed(UserProfilePage.spnPaymentMethodsHeader)))
			{
				
				return "Fail";
			}
			else
			{
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addPaymentMethod "+ e);
			return "Fail";
		}
	}

}
