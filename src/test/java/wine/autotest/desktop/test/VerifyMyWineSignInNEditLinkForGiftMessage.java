package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyMyWineSignInNEditLinkForGiftMessage extends Desktop {

	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose : TM-4132
	 ****************************************************************************
	 */

	public static String signInModalForMyWinePage() {

		String objStatus = null;
		String ScreenshotName = "signInModalForMyWinePage.jpeg";
		
		try {
			log.info("sign In Modal For My Wine Page method started here........");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			if (UIFoundation.isElementDisplayed(LoginPage.spnsignInTextR)
					&& UIFoundation.isElementDisplayed(LoginPage.btnSignIn)) {
				objStatus += true;
				String objDetail = "Sign modal is displayed when clicks on Mywie";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywie";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("sign In Modal For My Wine Page method ended here........");

			if (objStatus.contains("false")) {

				System.out.println("signInModalForMyWinePage test case is failed");

				return "Fail";
			} else {
				System.out.println("signInModalForMyWinePage test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose : TM-4139
	 ****************************************************************************
	 */

	public static String editOptionForGiftMessage() {

		String objStatus = null;
		String ScreenshotName = "editOptionForGiftMessage.jpeg";
		String addToCart1, addToCart2;

		try {
			log.info("edit Option For Gift Message method started here.......");
			driver.navigate().refresh();
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "userGiftMessage"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "password"));
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSauvignonBlanc));
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			UIFoundation.scrollUp(driver);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
			UIFoundation.clearField(FinalReviewPage.txtReceipientEmail);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipientEmail, "addsaveolyonce"));	
			UIFoundation.clearField(FinalReviewPage.txtRecipientGiftMessage);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientGiftMessage, "giftMessage"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkrecipienteditR)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkrecipienteditR));
				objStatus += true;
				String objDetail = "Edit button is displayed next to gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Edit button is  not displayed next to gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			UIFoundation.clickObject(FinalReviewPage.rdonoGiftBag);
			log.info("edit Option For Gift Message method ended here.......");
			if (objStatus.contains("false")) {
				System.out.println("editOptionForGiftMessage test case is failed");
				return "Fail";
			} else {
				System.out.println("editOptionForGiftMessage test case  executed succesfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
}