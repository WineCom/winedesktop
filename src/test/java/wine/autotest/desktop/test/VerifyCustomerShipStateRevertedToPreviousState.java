package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyCustomerShipStateRevertedToPreviousState extends Desktop {

	static boolean isObjectPresent=false;

	/***************************************************************************
	 * Method Name			: customerShipstateRevertedToPreviousStateInSideShoppingCart()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * jira ID	            : TM-410
	 ****************************************************************************
	 */
	public static String customerShipstateRevertedToPreviousStateInSideShoppingCart() {

		String screenshotName = "Scenarios_customerShipstateRevertedToPreviousStateInSideShoppingCart_Screenshot.jpeg";
		String objStatus=null;

		try {
			log.info("The execution of the method customerShipstateRevertedToPreviousStateInSideShoppingCart started here ...");

			UIFoundation.waitFor(3L);				
			//driver.navigate().refresh();			
			String cartCountBefortStateChange=UIFoundation.getText(ListPage.btnCartCount);
			UIFoundation.waitFor(1L);
			String getBeforeChangeState=UIFoundation.getText(CartPage.spngetState);								
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.dwnChangeState, "dryState"));
			//objStatus+=String.valueOf(UIFoundation.SelectObject(driver, "SelectState", "dryState"));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(CartPage.btncontinueShipToKY));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCancleStayIn));
			UIFoundation.waitFor(8L);
			String cartCountAfterStateChange=UIFoundation.getText(ListPage.btnCartCount);
			String getAfterChangeState=UIFoundation.getText(CartPage.spngetState);	

			if(cartCountBefortStateChange.contains(cartCountAfterStateChange) && getBeforeChangeState.contains(getAfterChangeState))
			{
				String objDetail="Customer Ship to State is reverted to CA";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				objStatus+= true ;
					}
			else
			{
				String objDetail="Customer Ship to State is NOT reverted to CA";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				objStatus+= false ;
			}

			log.info("The execution of the method customerShipstateRevertedToPreviousStateInSideShoppingCart ended here ...");
			if(objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{	
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method customerShipstateRevertedToPreviousStateInSideShoppingCart "+ e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name			: customerShipstateRevertedToPreviousStateOutSideShoppingCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * jira ID	            : TM-412
	 ****************************************************************************
	 */
	public static String customerShipstateRevertedToPreviousStateOutSideShoppingCart() {
		String screenshotName = "Scenarios_customerShipstateRevertedToPreviousStateOutSideShoppingCart_Screenshot.jpeg";
		String objStatus=null;
		try {
			log.info("The execution of the method customerShipstateRevertedToPreviousStateOutSideShoppingCart started here ...");

			UIFoundation.waitFor(3L);				
			//	driver.navigate().refresh();			
			String cartCountBefortStateChange=UIFoundation.getText(ListPage.btnCartCount);
			UIFoundation.waitFor(1L);
			String getBeforeChangeState=UIFoundation.getText(CartPage.spngetState);								
			UIFoundation.waitFor(5L);

			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			UIFoundation.waitFor(5L);
//			objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(2L);  
			}                
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnrecipientState, "dryState"));
			//    UIFoundation.clickObject(driver, "shipState");
			UIFoundation.clearField(FinalReviewPage.txtZipCode);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZipCode, "dryZipCode"));
			UIFoundation.waitFor(5L);		
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
			UIFoundation.waitFor(5L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnaddressContinue));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnaddressContinue)){
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnaddressContinue));
				UIFoundation.waitFor(10L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.btnShipContinue)){
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
				UIFoundation.waitFor(10L);
			}else {
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinue));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}

			objStatus+=String.valueOf(UIFoundation.isDisplayed(CartPage.btncontinueShipToKY));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCancleStayIn));
			UIFoundation.waitFor(8L);			
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.imgWineLogo));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.imgWineLogo));
			UIFoundation.waitFor(1L);

			String cartCountAfterStateChange=UIFoundation.getText(ListPage.btnCartCount);
//			String getAfterChangeState=UIFoundation.getText(driver, "getStateInHomePage");	
			String getAfterChangeState=UIFoundation.getText(CartPage.spngetState);
			if(cartCountBefortStateChange.contains(cartCountAfterStateChange) && getBeforeChangeState.contains(getAfterChangeState))
			{
				String objDetail="Customer Ship to State is reverted to CA";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				objStatus+= true ;
			}
			else
			{
				String objDetail="Customer Ship to State is NOT reverted to CA";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				objStatus+= false ;
			}

			log.info("The execution of the method customerShipstateRevertedToPreviousStateOutSideShoppingCart ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method customerShipstateRevertedToPreviousStateOutSideShoppingCart "+ e);
			return "Fail";
		}
	}	
}
