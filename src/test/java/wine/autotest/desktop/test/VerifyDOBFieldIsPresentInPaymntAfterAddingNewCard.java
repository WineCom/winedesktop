package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard extends Desktop{


	/***************************************************************************
	 * Method Name			: VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4268
	 ****************************************************************************
	 */
public static String addingNewCardInPaymentSection() {
	
		String objStatus = null;
		try {
		log.info("Adding New Card In Payment Section method started here");
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkPaymentMethods));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewCard));
		UIFoundation.waitFor(3L);
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNameOnCard, "NameOnCard"));
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCardNumber, "Cardnum"));
		UIFoundation.waitFor(4L);
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtExpiryMonth, "Month"));
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtExpiryYear, "Yr"));
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCVV, "CardCvid"));
		UIFoundation.waitFor(2L);
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingAddress, "NameOnCard"));
		//objStatus += String.valueOf(UIFoundation.setObject(driver, "NewBillingSuite", "Address1"));
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingCity, "BillingCity"));
		objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnNewBillinState, "militaryStateAA"));
		objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingZip, "militaryStateAACode"));
		objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtNewBillingPhone, "PhoneNumber"));
		objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnBillingSave));
		UIFoundation.waitFor(3L);
		UIFoundation.clickObject(UserProfilePage.imgWineLogo);
		if (objStatus.contains("false")) {
			System.out.println("New credit card is not added in the payment section user profile");
			return "Fail";
			
		} else {
			System.out.println("New credit card is  added in the payment section user profile");
			return "Pass";
		}
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method addingNewCreditCard "
				+ e);
		return "Fail";
	}
	}

/***************************************************************************
 * Method Name			: searchProductWithProdName()
 * Created By			: Vishwanath Chavan 
 * Reviewed By			: Ramesh,KB
 * Purpose				: TM-4268
 ****************************************************************************
 */

public static String searchProductWithProdName() {
	String objStatus = null;
	try {
		log.info("The execution of the method searchProductWithProdName started here ...");
		UIFoundation.waitFor(3L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
		objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "vinye"));
		UIFoundation.waitFor(3L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
		UIFoundation.waitFor(12L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
		UIFoundation.waitFor(4L);
		objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
		UIFoundation.waitFor(3L);
		log.info("The execution of the method searchProductWithProdName ended here ...");
		if (objStatus.contains("false")) {

			return "Fail";

		} else {

			return "Pass";
		}

	} catch (Exception e) {
		System.out.println(" Search product with product name test case is failed");
		log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
		return "Fail";
	}

}	

/***************************************************************************
 * Method Name			: verifyDOBFiledPresent()
 * Created By			: Vishwanath Chavan 
 * Reviewed By			: Ramesh,KB
 * Purpose				: TM-4268
 ****************************************************************************
 */

public static String verifyDOBFiledPresent() {
	
	String screenshotName = "verifyDOBFiledIsPresentInPaymentSection.jpeg";
	
	String objStatus = null;
	try {
	log.info("verify DOB Filed Is Present In Payment Section method started here");
	objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
	UIFoundation.waitFor(6L);
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
	UIFoundation.waitFor(2L);
	objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
	objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
	objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
	objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
	objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
	UIFoundation.clickObject(FinalReviewPage.dwnState);
	objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
	objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
	UIFoundation.waitFor(5L);
	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
	UIFoundation.waitFor(3L);
	if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
		UIFoundation.waitFor(10L);
	}
	if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
	{
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
		UIFoundation.waitFor(6L);
	}
	if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
		objStatus+=true;
		ReportUtil.addTestStepsDetails("DOB field is present after adding card in userprofile", "Pass", "");
		logger.pass("DOB field is present after adding card in userprofile");
	}else{
		objStatus+=false;
			String objDetail="DOB field is not present after adding card in userprofile";
			UIFoundation.captureScreenShot(screenshotpath+"dobFormat", objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	
	}
	if (objStatus.contains("false")) {
		System.out.println("Verify DOB Field Is Present In Paymnt After Adding New Card test case failed");
		return "Fail";
		
	} else {
		System.out.println("Verify DOB Field Is Present In Paymnt After Adding New Card test case executed succesfully ");
		return "Pass";
	}
	
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method"
				+ e);
		return "Fail";
	}
	

	}
}
