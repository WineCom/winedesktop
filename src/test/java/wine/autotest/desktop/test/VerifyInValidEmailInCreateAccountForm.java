package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyInValidEmailInCreateAccountForm extends Desktop {
	
	/***************************************************************************
	 * Method Name			: createButtonInActive()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String inValidEmail() {
		String objStatus=null;
		   String screenshotName = "Scenarios__inValidEmailScreenshot.jpeg";
					
		try {
			log.info("The execution of method inValidEmail started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);;
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
				
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"inValidEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "accPassword"));

			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
			if(UIFoundation.isDisplayed(LoginPage.spnInValidEmail)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("'Invalid email format' text is displayed", "Pass", "");
				System.out.println("Invalid email format' text is displayed");
				
			}else
			{
				objStatus +=false;
				String objDetail="Invalid email format' is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Invalid email format' text is not displayed");
			}
		//	objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CreateAccountButton"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method inValidEmail ended here ...");
			if(objStatus.contains("false"))
			{
				System.out.println("Verify invalid email address in account creation form test case is failed");
				return "Fail";
			}else
			{
				System.out.println("Verify invalid email address in account creation form test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println("Invalid email format' text test case is failed");
			objStatus +=false;
			String objDetail="Invalid email format' is not displayed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			System.out.println("Invalid email format' text is not displayed");
			log.error("there is an exception arised during the execution of the method inValidEmail "
					+ e);
			return "Fail";
		}
	}

}
