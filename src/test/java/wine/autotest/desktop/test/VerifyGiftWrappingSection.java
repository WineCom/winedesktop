package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyGiftWrappingSection extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addGiftsTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String screenshotName = "Scenarios_GiftWrappingSection_Screenshot.jpeg";
		
		try {
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "floridaState");
			UIFoundation.waitFor(18L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkwineSet));
			UIFoundation.waitFor(4L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSecondProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFourthProductToCart);
			addToCart3 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.lnkThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));
			}

			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "floridaState"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "floridaStateZip"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(20L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
			UIFoundation.waitFor(2L);
		//	objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientEidt"));
		//	WebElement ele=driver.findElement(By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@name='giftCheckbox']"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkRecipientGift);
			if(!UIFoundation.isSelected(FinalReviewPage.chkRecipientGift))
			{
				
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkRecipientGift));
				UIFoundation.waitFor(2L);
			}
			//ApplicationDependent.isGiftCheckboxSelected(driver, "RecipientGiftCheckbox");
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.spnGiftBag))
			{
				  objStatus+=true;
			      String objDetail="Gift bag is not displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				objStatus+=false;
			       String objDetail="Gift bag is displayed";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			//objStatus+=String.valueOf(!UIFoundation.isDisplayed(driver, "GiftBag"));
			if (objStatus.contains("false")) {
				System.out.println("Verify gift wrapping section test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify gift wrapping section test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
/*	*//***************************************************************************
	 * Method Name : verifyGiftWrapping() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 *//*

	public static String verifyGiftWrapping(WebDriver driver) {
		String objStatus = null;
		try {
			log.info("The execution of the method verifyGiftWrapping started here ...");
			UIFoundation.waitFor(2L);

			log.info("The execution of the method verifyGiftWrapping ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftWrapping "
					+ e);
			return "Fail";
		}
	}*/

}
