package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;;

public class ItemsNotEligibleForPromoCodeRedemption extends Desktop {
	
	/***************************************************************************
	 * Method Name			: itemsNotEligibleForPromoCodeRedemption()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 * TM-1924
	 ****************************************************************************
	 */
	public static String itemsNotEligibleForPromoCodeRedemption() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		   String screenshotName = "Scenarios__promocodeRedemption.jpeg";
			
		try {
			
			UIFoundation.waitFor(5L);
            WebElement ele=driver.findElement(By.xpath("(//form[@class='formWrap searchBarForm']/div/div/select[@class='state_select state_select-promptsCartTransfer'])[1]"));
			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
			sel.selectByVisibleText("DC");
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "notEligibleForPromocode"));
			UIFoundation.waitFor(10L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnknotEligiblepromocodeRedemptionProduct));
			UIFoundation.waitFor(12L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
//				UIFoundation.scrollDownOrUpToParticularElement(driver, "FirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart2.contains("Add to Cart")) {
//				UIFoundation.scrollDownOrUpToParticularElement(driver, "FifthProductToCart");
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			/*addToCart3 = UIFoundation.getText(driver, "TenthProductToCart");
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(driver, "TenthProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(driver, "TenthProductToCart");
			}*/
			/*addToCart4 = UIFoundation.getText(driver, "FourthProductToCart");
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(driver, "FourthProductToCart");
			}

			addToCart5 = UIFoundation.getText(driver, "FifthProductToCart");
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(driver, "FifthProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(driver, "FifthProductToCart");
			}*/
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			//UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(CartPage.spnpromocodeRedemptionWarningMSg))
			{
				objStatus+=true;
				logger.pass("Verified appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption");
				ReportUtil.addTestStepsDetails("Verified appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption", "Pass", "");
			}else
			{
				objStatus+=false;
				String objDetail="Verify appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption test case is failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
