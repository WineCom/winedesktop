package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class ForgotPasswordValidation extends Desktop {
	
	/***************************************************************************
	 * Method Name : forgotPassword() 
	 * Created By : Vishwanath Chavan 
	 * Reviewed By: Ramesh,KB,srini, Purpose :
	 ****************************************************************************
	 */
	public static String forgotPassword()
	{
		String objStatus=null;
		String actual=null;
		String expected=null;
		String actualEnterEMailAddMsg=null;
		String expectedEnterEMailAddMsg=null;
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method Forgot Password started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkForgotPassword));
			UIFoundation.waitFor(3L);
			actualEnterEMailAddMsg=verifyexpectedresult.plzEnterEmailMsg;
			expectedEnterEMailAddMsg=UIFoundation.getText(LoginPage.txtEnterEmailAddMsg);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtForgotEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotContinue));
			UIFoundation.waitFor(3L);
			expected=verifyexpectedresult.plzCheckUrMail;
			actual=UIFoundation.getText(LoginPage.spnCheckUrEmail);
			if((expected.equalsIgnoreCase(actual)))
			{
				  objStatus+=true;
			      String objDetail="'Please Check Your Email' text is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		
				
			}else{
					objStatus+=false;
			       String objDetail="'Please Check Your Email' text is displayed";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if((actualEnterEMailAddMsg.equalsIgnoreCase(expectedEnterEMailAddMsg)))
			{
				  objStatus+=true;
			      String objDetail="'Please enter the email address' text is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				   objStatus+=false;
			       String objDetail="'Please enter the email address' text is displayed";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method Forgot Password ended here ...");
			if (!objStatus.contains("false"))
			{
				System.out.println("Forgot Password validation test case is executed successfully");
				return "Pass";
				
			}
			else
			{
				System.out.println("Forgot Password validation test case is failed");
				return "Fail";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method Forgot Password "+ e);
			return "Fail";
			
		}
	}

}
