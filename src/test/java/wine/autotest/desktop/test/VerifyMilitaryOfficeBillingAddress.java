package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyMilitaryOfficeBillingAddress extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "militaryOfficeUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: addNewCreditCard()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to add the new credit 
	 * 						  card details for billing process
	 ****************************************************************************
	 */
		public static String addNewCreditCard() {
			String objStatus=null;
			 try {
				log.info("The execution of the method addNewCreditCard started here ...");
				
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
				UIBusinessFlows.recipientEdit();
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
					WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
					if(ele.isSelected())
					{
				    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "militaryStateAA"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingZip, "militaryStateAACode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AA' state is displayed when adding new 'BILLING ADDRESS' of credit cards.", "Pass", "");
					logger.pass("'AA' state is displayed when adding new 'BILLING ADDRESS' of credit cards.");
				}else{
					objStatus+=false;
						String objDetail="'AA' state is not displayed when adding new 'BILLING ADDRESS' of credit cards.";
						UIFoundation.captureScreenShot(screenshotpath+"AAState", objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
					UIFoundation.waitFor(1L);
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
					ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
					if(ele.isSelected())
					{
				    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "militaryStateAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingZip, "militaryStateAECode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AE' state is displayed when adding new 'BILLING ADDRESS' of credit cards.", "Pass", "");
					logger.pass("'AE' state is displayed when adding new 'BILLING ADDRESS' of credit cards.");
				}else{
					objStatus+=false;
						String objDetail="'AE' state is not displayed when adding new 'BILLING ADDRESS' of credit cards.";
						UIFoundation.captureScreenShot(screenshotpath+"APState", objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
					UIFoundation.waitFor(1L);
					
				}
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(ele.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "militaryStateAP"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingZip, "militaryStateAPCode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
			
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AP' state is displayed when adding new 'BILLING ADDRESS' of credit cards.", "Pass", "");
					logger.pass("'AP' state is displayed when adding new 'BILLING ADDRESS' of credit cards.");
				}else{
					objStatus+=false;
						String objDetail="'AP' state is not displayed when adding new 'BILLING ADDRESS' of credit cards.";
						UIFoundation.captureScreenShot(screenshotpath+"APState", objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)){
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkpaywithThisCardEdit));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtpaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnbillingAdressState, "militaryStateAA"));
				UIFoundation.clearField(FinalReviewPage.txtbillingAdressZipCode);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbillingAdressZipCode, "militaryStateAACode"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnpaywithThisCardSave);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				UIFoundation.waitFor(4L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkpaywithThisCardEdit)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AA' state is displayed when editing 'BILLING ADDRESS' of credit cards.", "Pass", "");
					logger.pass("'AA' state is displayed when editing 'BILLING ADDRESS' of credit cards.");
				}else{
					objStatus+=false;
						String objDetail="'AA' state is not displayed when editing 'BILLING ADDRESS' of credit cards.";
						UIFoundation.captureScreenShot(screenshotpath+"AEStateEditCard", objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)){
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkpaywithThisCardEdit));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtpaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnbillingAdressState, "militaryStateAE"));
				UIFoundation.clearField(FinalReviewPage.txtbillingAdressZipCode);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbillingAdressZipCode, "militaryStateAECode"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnpaywithThisCardSave);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				UIFoundation.waitFor(4L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkpaywithThisCardEdit) || UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AE' state is displayed when editing 'BILLING ADDRESS' of credit cards.", "Pass", "");
					logger.pass("'AE' state is displayed when editing 'BILLING ADDRESS' of credit cards.");
				}else{
					objStatus+=false;
						String objDetail="'AE' state is not displayed when editing 'BILLING ADDRESS' of credit cards.";
						UIFoundation.captureScreenShot(screenshotpath+"APState", objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)){
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkpaywithThisCardEdit));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtpaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnbillingAdressState, "militaryStateAP"));
				UIFoundation.clearField(FinalReviewPage.txtbillingAdressZipCode);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbillingAdressZipCode, "militaryStateAPCode"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnpaywithThisCardSave);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				UIFoundation.waitFor(4L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkpaywithThisCardEdit) || UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AP' state is displayed when editing 'BILLING ADDRESS' of credit cards.", "Pass", "");
					logger.pass("'AP' state is displayed when editing 'BILLING ADDRESS' of credit cards.");
				}else{
					objStatus+=false;
						String objDetail="'AP' state is not displayed when editing 'BILLING ADDRESS' of credit cards.";
						UIFoundation.captureScreenShot(screenshotpath+"APState", objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
				log.info("The execution of the method addNewCreditCard ended here ...");	
				if (objStatus.contains("false")) {
					System.out.println("Verify 'AA,AE,AP'  state's are displayed when editing and adding new 'BILLING ADDRESS' of credit cards test case failed.");
					return "Fail";
					
				} else {
					System.out.println("Verify 'AA,AE,AP'  state's are displayed when editing and adding new 'BILLING ADDRESS' of credit cards test case executed successfully.");
					return "Pass";
				}
			} catch (Exception e) {
				log.error("there is an exception arised during the execution of the method addNewCreditCard "
						+ e);
				return "Fail";
			}
			
		}

}
