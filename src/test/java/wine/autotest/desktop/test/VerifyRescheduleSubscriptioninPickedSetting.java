package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;

public class VerifyRescheduleSubscriptioninPickedSetting extends Desktop{

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";		
		try
		{		
			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(2L);		
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "compassuser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnSignIn));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);		
			String actualtile=driver.getTitle();
			String expectedTile=verifyexpectedresult.shoppingCartPageTitle;
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
				logger.pass("Passed Login");
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWine() 
	 * Created By  : Ramesh S
	 * Reviewed By : Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify Likeed and Disliked Red/White Wine in Anything else section' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String enrollForPickedUpWine() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkRedandWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedwinePageNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWhitewinePagenNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
				objStatus+=true;
				String objDetail="User is navigated to adventurous quiz next page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to adventurous quiz next page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));			
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedWhiteQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				String objDetail="Enrollment flow for red/White wine is not completed successfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Enrollment flow for red/White wine is completed successfully";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

	
	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyLocalPickUpForPickedUpWine() {
			
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.txtRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkAcceptTermsandConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
				UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}
				else
				{
					objStatus+=false;
					String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					UIFoundation.captureScreenShot(screenshotpath, objDetail);
				}
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}
		catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
	
	/***********************************************************************************************************
	 * Method Name : ChangedDeliveryDateinPickedSettings() 
	 * Created By  : Ramesh S
	 * Reviewed By : Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the Delivery date changes in picked settings' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String ChangedDeliveryDateinPickedSettings() {
		String objStatus = null;
		String screenshotName = "Scenarios_ChangedDeliveryDateinPickedSettings_Screenshot.jpeg";
		try {
			log.info("Execution of the method ChangedDeliveryDateinPickedSettings started here .........");
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.imgPickedLogo));
			UIFoundation.waitFor(10L);
			String ExistingDate = UIFoundation.getText(PickedPage.spnPickedSetPickedDate);

			String strCurrentDate = UIFoundation.getDateTime("dd");
			System.out.println("ExistingDate : "+ExistingDate);
			System.out.println("strCurrentDate : "+strCurrentDate);
			if(ExistingDate.contains(strCurrentDate)) {
				objStatus+=true;
				String objDetail="The Scheduled Delivery date is current Date";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetChangeDate));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.radDelayDateOneWeek));
				String DelayDate = UIFoundation.getText(PickedPage.radDelayDateOneWeek);
				String[] picDate = DelayDate.split("/");
				System.out.println("picDate : "+picDate[1]);
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnDelaydateConfirm));
				UIFoundation.waitFor(1L);
				String ChangeDate = UIFoundation.getText(PickedPage.spnPickedSetPickedDate);
				String[] ChangedDate = ChangeDate.split("t");
				System.out.println("ChangedDate : "+ChangedDate[0]);
				if((ChangedDate[0].trim()).contains(picDate[1].trim())){
					objStatus+=true;
					String objDetail1="The Current delivery date is Rescheduled to delay delivery in Picked Setting page.";
					logger.pass(objDetail);
					System.out.println(objDetail1);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail2="The Current delivery date is Not Rescheduled to delay delivery in Picked Setting page.";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					UIFoundation.captureScreenShot(screenshotpath, objDetail);
					System.out.println(objDetail2);
				}	
			}
			else
			{
				objStatus+=false;
				String objDetail="The Scheduled Delivery is not Current date in Picked Setting page.";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verification of Reschedule Delivery date in Picked Settings page is not completed successfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verification of Change Delay date in Picked Settings page is completed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
