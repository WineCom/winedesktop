package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class PromoBannerBehaviourForDryState extends Desktop {
	/***************************************************************************
	 * Method Name			: PromoBannerBehaviourForDryState()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String promoBannerBehaviourForDryState()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_promoBannerBehaviourForDryState.jpeg";
			
			String stateName=null;
		try
		{
			log.info("The execution of the method promoBannerBehaviourForDryState started here ...");
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "dryState");
			UIFoundation.waitFor(2L);
			String expectedPromoBanner=verifyexpectedresult.dryStatePromoBar;
			String actualPromoBanner=UIFoundation.getText(LoginPage.spnpromoBannerDry);
			System.out.println("actualPromoBanner :"+actualPromoBanner);
			stateName=actualPromoBanner.substring(49);
			if(actualPromoBanner.contains(expectedPromoBanner))
			{
				objStatus+=true;
				String objDetail="Due to state regulations, we cannot ship wine to" +stateName+". message is displayed.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Due to state regulations, we cannot ship wine to" +stateName+". message is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			System.out.println("objStatus :"+objStatus);
			
			log.info("The execution of the method promoBannerBehaviourForDryState ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method superscriptInProductPrice "+ e);
			return "Fail";
			
		}
	}	

}
