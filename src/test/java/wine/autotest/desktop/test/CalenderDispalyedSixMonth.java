package wine.autotest.desktop.test;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class CalenderDispalyedSixMonth extends Desktop{
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "calenderMonthUN"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String calenderSixMnthInDeliverySection() {
	
	String objStatus=null;

	   String screenshotName = "Scenarios_CalenderSixMnth_Screenshot.jpeg";
		ArrayList<String> monthCount=new ArrayList<String>();
		String monthName=null;
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(8L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeDate))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
				UIFoundation.waitFor(2L);
			}
		//	UIFoundation.clckObject(driver, "SelectShippingMethod");
			monthName=UIFoundation.getText(FinalReviewPage.lnkcalenderFirstMnth);
			monthCount.add(monthName);
			UIFoundation.waitFor(1L);
			monthName=UIFoundation.getText(FinalReviewPage.lnkcalenderNxtMnth);
			monthCount.add(monthName);
			if(!UIFoundation.isDisplayed(FinalReviewPage.lnkpreviousMnthIcon)){
				objStatus+=true;
				String objDetail="Previous icon is not displayed for current month.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				 logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Previous icon is displayed for current month";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+"previousMnth", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			for(int i=0;i<5;i++){
				if(UIFoundation.isDisplayed(FinalReviewPage.lnknxtMnthIcon)){
					objStatus+=String.valueOf(UIFoundation.clckObject(FinalReviewPage.lnknxtMnthIcon));
					UIFoundation.waitFor(1L);
					monthName=UIFoundation.getText(FinalReviewPage.lnkcalenderNxtMnth);
					monthCount.add(monthName);
										
				}else{
					break;
				}
			}
			System.out.println("monthCount : "+monthCount.size());
			if(monthCount.size()>=6){
				objStatus+=true;
				String objDetail="calendar is displayed for 6 months in desktop view of delivery section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				 logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="calendar is not displayed for 6 months in desktop view of delivery section";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify  calendar is displayed for 6 months in desktop view of delivery section test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify  calendar is displayed for 6 months in desktop view of delivery section test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			objStatus+=false;
			String objDetail="Verify  calendar is displayed for 6 months in desktop view of delivery section is failed";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: AlaskaStateSaturdayCalenderSelect()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String AlaskaStateSaturdayCalenderSelect() {
	
	String objStatus=null;

	   String screenshotName = "AlaskaStateSaturdayCalenderSelect_Screenshot.jpeg";
	
		try {
			log.info("The execution of the method AlaskaStateSaturdayCalenderSelect started here ...");
			UIFoundation.waitFor(4L);
			objStatus += String
					.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			System.out.println("Adding address for First time");
			UIFoundation.waitFor(10L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkChangeAddress)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(1L);}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressAK"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "CityAK"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "StateAK"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipAK"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			
			if (UIFoundation.isElementDisplayed(FinalReviewPage.btnaddressContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnaddressContinue));
			UIFoundation.waitFor(1L);}
			List<WebElement> dayCount=	driver.findElements(By.xpath("//span[@tabindex='-1']"));
			System.out.println("Day Count : "+dayCount.size());
		//	UIFoundation.clckObject(driver, "SelectShippingMethod");
			if(dayCount.size()>=8) {
				objStatus+=true;
				String objDetail="Saturday cannot be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				 logger.pass(objDetail);
				System.out.println("Saturday cannot be selected");
			}else {
				objStatus+=false;
				String objDetail="Saturday can be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Saturday can be selected");
			}

			log.info("The execution of the method AlaskaStateSaturdayCalenderSelect ended here ...");
			if ( objStatus.contains("false")) {
				String objDetail="Verify  AlaskaStateSaturdayCalenderSelect in desktop view of delivery section test case is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			} else {
				String objDetail="Verify  AlaskaStateSaturdayCalenderSelect in desktop view of delivery section test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 logger.pass(objDetail);
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method AlaskaStateSaturdayCalenderSelect "
					+ e);
			objStatus+=false;
			String objDetail="Verify  AlaskaStateSaturdayCalenderSelect in desktop view of delivery section is failed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: HawaiiStateSaturdayCalenderSelect()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String HawaiiStateSaturdayCalenderSelect() {
	
	String objStatus=null;

	   String screenshotName = "HawaiiStateSaturdayCalenderSelect_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method HawaiiStateSaturdayCalenderSelect started here ...");
			UIFoundation.waitFor(4L);
			objStatus += String
					.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckout));
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			System.out.println("Adding address for First time");
			UIFoundation.waitFor(10L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkChangeAddress)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(1L);}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(2L);objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressHI"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "CityHI"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "StateHI"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipHI"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			
			if (UIFoundation.isElementDisplayed(FinalReviewPage.btnaddressContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnaddressContinue));
			UIFoundation.waitFor(1L);}
			
			List<WebElement> dayCount=	driver.findElements(By.xpath("//span[@tabindex='-1']"));
			System.out.println("Day Count : "+dayCount.size());
		//	UIFoundation.clckObject(driver, "SelectShippingMethod");
			if(dayCount.size()>=8) {
				objStatus+=true;
				String objDetail="Saturday cannot be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Saturday cannot be selected");
				 logger.pass(objDetail);
			}else {
				objStatus+=false;
				String objDetail="Saturday can be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				System.out.println("Saturday can be selected");
			}

			log.info("The execution of the method HawaiiStateSaturdayCalenderSelect ended here ...");
			if ( objStatus.contains("false")) {
				String objDetail="Verify  HawaiiStateSaturdayCalenderSelect in desktop view of delivery section test case is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			} else {
				String objDetail="Verify  HawaiiStateSaturdayCalenderSelect in desktop view of delivery section test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 logger.pass(objDetail);
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method HawaiiStateSaturdayCalenderSelect "
					+ e);
			objStatus+=false;
			String objDetail="Verify  HawaiiStateSaturdayCalenderSelect in desktop view of delivery section is failed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
