package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.ReportUtil;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyLikeDislikeSectionNotDisplayedinVaritalPreference extends Desktop {
	
	/***********************************************************************************************************
	 * Method Name : LikeDisLikeNotSelectedinVarietalPage() 
	 * Created By  : Ramesh S
	 * Reviewed By : Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the Like/Dislike section is not displayed in the varietal preference review page. 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String LikeDisLikeNotSelectedinVarietalPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_LikeDisLikeNotSelectedinVarietalPage_Screenshot.jpeg";
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkNoVoice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.chkRedOnly));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNextThirdPage));
			UIFoundation.waitFor(1L);
			if(!(UIFoundation.isDisplayed(PickedPage.spnlikedheadertext) && UIFoundation.isDisplayed(PickedPage.spnDislikedHeaderText))) {
				String objDetail="Liked or Disliked Varietals were displayed";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}
			else
			{
				String objDetail="Liked or Disliked Varietals were not displayed";
				System.out.println(objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				objStatus+=false;
			}
			if(UIFoundation.isDisplayed(PickedPage.spnAnythingelseQuest)) {
				String objDetail="Anything else question is present in the varietal review page";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}
			else
			{
				String objDetail="Anything else question is present in the varietal review page";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				objStatus+=false;
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verify non availability of selected varietal test case failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verify non availability of selected varietal test case executed successfully";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}
