package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class UpdateShippingAddressInAddressBook extends Desktop {
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clckObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "editHomeAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				logger.fail("Login test case is failed", MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				logger.pass("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name : UpdateShippingAddressInAddressBook() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : 
	 * TM-2224,2159
	 ****************************************************************************
	 */

	public static String updateShippingAddressInAddressBook() {
		String objStatus = null;
		   String screenshotName = "Scenarios_updateAddressInUser_Screenshot.jpeg";

		try {
			log.info("The execution of the method updateShippingAddressInAddressBook started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddressBook));
			UIFoundation.waitFor(5L);
			int addressCountBeforUpdating=UIFoundation.listSize(UserProfilePage.lnkshippingAddressCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkuserDataCardEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(UserProfilePage.txtuserDataFirstNameClear);
			UIFoundation.waitFor(2L);
			driver.findElement(By.xpath("(//fieldset[@class='formWrap_group'])[1]")).click();
			String expectedEditedName=UIFoundation.setObjectEditShippingAddress(UserProfilePage.txtuserDataFirstName, "firstName");
			expectedEditedName=expectedEditedName.replaceAll(" ", "");
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.lnkcontactUs);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnuserDataSave));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(UserProfilePage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}
			String actualEditedName=UIFoundation.getText(UserProfilePage.txtexpectedFirstName);
			actualEditedName=actualEditedName.replaceAll(" ", "");
			int addressCountAfterUpdating=UIFoundation.listSize(UserProfilePage.lnkshippingAddressCount);
			if(actualEditedName.equalsIgnoreCase(expectedEditedName))
			{
				  objStatus+=true;
			      String objDetail="Verified that user is able to update shipping address in Address Book";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified that user is able to update shipping address in Address Book");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify that user is able to update shipping address in Address Book  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"addressBook", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   System.err.println("Verify that user is able to update shipping address in Address Book is failed ");
			}
			if((addressCountBeforUpdating==addressCountAfterUpdating) && (addressCountBeforUpdating!=0 || addressCountAfterUpdating!=0 )){
				objStatus+=true;
			      String objDetail="Verified that address gets saved only once on editing the address in Shipping addresses page under user profile";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified that address gets saved only once on editing the address in Shipping addresses page under user profile");
			}else{
				 objStatus+=false;
				   String objDetail="Verify  that address gets saved only once on editing the address in Shipping addresses page under user profile  is failed";
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   System.err.println("Verify that address gets saved only once on editing the address in Shipping addresses page under user profile is failed ");
			}
			log.info("The execution of the method updateShippingAddressInAddressBook ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method updateShippingAddressInAddressBook "
					+ e);
			return "Fail";
		}
	}
	
	

}
