package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.UIFoundation;

public class RemoveAllProdcutsFromCartContinueShopingWithSignIn extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	static int countProducts=0;
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		boolean isElementPresent=false;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkRegionR));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkwashingtonWines));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				countProducts++;
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
				countProducts++;
			}

			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
				countProducts++;
			}

			UIFoundation.waitFor(2L);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			}
			isElementPresent=UIFoundation.isDisplayed(LoginPage.txtLoginEmail);
			if(isElementPresent)
			{
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: removeProductsFromTheCart()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String removeProductsFromTheCart()
	{
		String objStatus=null;
		String expected=null;
		String actual=null;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			UIFoundation.waitFor(1L);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(2L);
			for(int i=0;i<=countProducts;i++)
			{
				UIFoundation.javaScriptClick(CartPage.lnkRemoveFirstProduct);
				//UIFoundation.waitFor(2L);
				UIFoundation.javaScriptClick(CartPage.btnRemove);
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnContinueShopping));
			UIFoundation.waitFor(2L);
			expected=verifyexpectedresult.homePageTitle;
			actual=driver.getTitle();
			// Assertion
			Assert.assertEquals(actual, expected);
			log.info("The execution of the method removeProductsFromTheCart started here ...");
			if (objStatus.contains("false") && !(expected.equalsIgnoreCase(actual)))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}

}
