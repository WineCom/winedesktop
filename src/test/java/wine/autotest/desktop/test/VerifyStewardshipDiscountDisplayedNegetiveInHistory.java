package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.OrderDetailsPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyStewardshipDiscountDisplayedNegetiveInHistory extends Desktop {

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String stewardLogin()
	{
		String objStatus=null;
		
		try 
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainAccSignIn));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "passwordStewrd"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{	
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";		
		}
	}
	
	/***************************************************************************
	 * Method Name			: productIconCheck()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: To check product icon attribute in PIP
	 * 				
	 ****************************************************************************
	 */
	
	public static String productIconCheck()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_CreditCard_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method productIconCheck started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnMainNav));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);		
			if(!UIFoundation.isDisplayed(ListPage.spnproductAttributeName)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Product Attribute icons are not displayed in list", "Pass", "");
				logger.pass("Product Attribute icons are not displayed in list");
				System.out.println("Product Attribute icons are not displayed in list");
			}
			else
			{
				objStatus+=false;
				String objDetail="Product Attribute icons are displayed in list";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+"List", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(ListPage.spnproductAttributeName)){
				objStatus+=true;
				String objDetail="Product Attribute icons are displayed in PIP page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				System.out.println("Product Attribute icons are displayed in PIP page");
			}
			else
			{
				objStatus+=false;
				String objDetail="Product Attribute icons are not displayed in PIP";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+"List", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgWineLogo));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method productIconCheck ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("productIconCheck test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("productIconCheck test case is executed successfully");
				return "Pass";
			}	
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productIconCheck "+ e);
			return "Fail";	
		}
	}
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart( ) {			
		String objStatus=null;
		try {
			log.info("The execution of the method addProductsToCart started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnMainNav));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSort);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			log.info("The execution of the method addProductsToCart ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{	
				return "Pass";
			}			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture Ordersummary
	 ****************************************************************************
	 */
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String stewardshipSaving=null;
		String shippingHandling=null;
		String stewardshipSave=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying Stewardship code===============");
			objStatus+=String.valueOf(subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal));
			objStatus+=String.valueOf(shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling));
			objStatus+=String.valueOf(totalBeforeTax=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax));
			objStatus+=String.valueOf(stewardshipSaving=UIFoundation.getText(FinalReviewPage.spnStewardshipSaving));
			stewardshipSave=stewardshipSaving.replaceAll("[^0-9]", "");
			shippingHandling=shippingAndHandling.replaceAll("[^0-9]", "");
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(shippingHandling.equals(stewardshipSave))
			{
				System.out.println("Stewardship Savings:      "+stewardshipSaving);
				System.out.println("Stewardship amount is discounted successfully");
				objStatus+=true;
			    String objDetail="Stewardship amount is discounted successfully";
			    logger.pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				System.out.println("Stewardship amount is not discounted");
				objStatus+=false;
			    String objDetail="Stewardship amount is not discounted";
			    logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}			
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";		
		}
	}
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Vishwanath Chavan 
	 * Reviewed By : Ramesh,KB
	 *  Purpose    : The purpose of this method is to fill the
	 *               shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	public static String shippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutAlter));
			}
			
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnShipContinue)){
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShipContinue, "Clickable", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			log.info("The execution of the method shippingDetails ended here ...");
			if (objStatus.contains("false")) {
				return "Fail";
			} else
			{

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() 
	 * Created By  : Vishwanath Chavan 
	 * Reviewed By : Ramesh,KB
	 *  Purpose    : The purpose of this method is to add the new
	 *                credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String stewardShipDiscount=null;
		String stewardShipValue="-";
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");		
			stewardShipDiscount=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);			
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(3L);
            if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
            {
                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
                  UIFoundation.waitFor(5L);                  
            }                                        
            objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.lnkOrderNumber));
            UIFoundation.waitFor(3L);
            objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(OrderDetailsPage.spnStewardshipSaving));
			UIFoundation.waitFor(1L);				
			stewardShipDiscount=UIFoundation.getText(OrderDetailsPage.spnStewardshipSaving);
			if(stewardShipDiscount.contains(stewardShipValue))
			{
				objStatus+=true;
				String objDetail = "Stewardship discount displayed as negetive in order history";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		    			
			}
			else
			{
				objStatus+=false;
		    	String objDetail="Stewardship discount not displayed as negetive in order history";
		    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}		
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Order Creation With Edit Functionality In Final Review Page For New User test case is failed");
				return "Fail";
			} 
			else 
			{
				System.out.println("Order Creation With Edit Functionality In Final Review Page For New User test case is executed successfully");
				return "Pass";
			}
		} 
		catch (Exception e) 
		{
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
}
