package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class StewardshipUpsellWhenCartIsEmpty extends Desktop {
	
	/***************************************************************************
	 * Method Name			: stewardshipUpshellCartIsEmpty()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String stewardshipUpshellCartIsEmpty()
	{
		String objStatus=null;
		String screenshotName = "Scenarios__stewardship.jpeg";
		String actualStewardshipMsg=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			String expectedStewardshipMsg=verifyexpectedresult.stewardship;
			if(UIFoundation.isDisplayed(CartPage.spnwantFreeShipping)){
				actualStewardshipMsg=UIFoundation.getText(CartPage.spnwantFreeShipping);
			}
			if(UIFoundation.isDisplayed(CartPage.spnwantFreeShipingAllYear)){
				actualStewardshipMsg=UIFoundation.getText(CartPage.spnwantFreeShipingAllYear);
			}		if(UIFoundation.isDisplayed(CartPage.spnstewardShipUpsellOptionDefault)){
				actualStewardshipMsg=UIFoundation.getText(CartPage.spnstewardShipUpsellOptionDefault);
			}
			if(UIFoundation.isDisplayed(CartPage.dwnstewardShipUpsellOptionA)){
				actualStewardshipMsg=UIFoundation.getText(CartPage.dwnstewardShipUpsellOptionA);
			}
			
			if (expectedStewardshipMsg.contains(actualStewardshipMsg)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the Stewardship upsell is displayed in the Shopping cart page when the cart is empty", "Pass", "");
				
			} else {
				objStatus+=false;
				String objDetail="Verify the Stewardship upsell is displayed in the Shopping cart page when the cart is empty test case is failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the Stewardship upsell is displayed in the Shopping cart page when the cart is empty test case is failed");
				String objDetail="Verify the Stewardship upsell is displayed in the Shopping cart page when the cart is empty test case is failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			}
			else
			{
				System.out.println("Verify the Stewardship upsell is displayed in the Shopping cart page when the cart is empty test case is executed successfully");
				logger.pass("Verify the Stewardship upsell is displayed in the Shopping cart page when the cart is empty test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method stewardshipUpshellCartIsEmpty "+ e);
			return "Fail";
			
		}
	}

}
