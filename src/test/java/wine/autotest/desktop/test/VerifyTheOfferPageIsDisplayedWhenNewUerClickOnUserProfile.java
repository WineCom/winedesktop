package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifyTheOfferPageIsDisplayedWhenNewUerClickOnUserProfile extends Desktop {
	
	static String expectedState=null;
	static String actualState=null;
	/***************************************************************************
	 * Method Name  : userProfileCreation() 
	 * Created By 	: Vishwanath Chavan
	 * Reviewed By  : Ramesh,KB 
	 * Purpose      : 
	 ****************************************************************************
	 */

	public static String offerPageDisplayed() {
		String objStatus = null;
		String screenshotName = "Scenarios_LikeDisLikeNotSelectedinVarietalPage_Screenshot.jpeg";
		try {
			log.info("The execution of method offerPageDisplayedt started here");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);						
			if(UIFoundation.isDisplayed(PickedPage.lnkPickedSetting))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			}					
			if(UIFoundation.isDisplayed(PickedPage.imgMainPickedLogo))
			{							
				objStatus+=true;
				String objDetail = "Compass 'Offer Page' displayed succesfully";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");					
			}
			else
			{
				objStatus+=false;
				String objDetail = "Compass 'Offer Page' is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
			}			
			log.info("The execution of the method offerPageDisplayed ended here ...");
			if (objStatus.contains("false")) {				
				return "Fail";
			} 
			else 
			{				
				return "Pass";
			}
		}
		catch (Exception e)
		{
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create offerPageDisplayed " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";	
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkMainAccSignIn));
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}			
			if(UIFoundation.isDisplayed(LoginPage.lnkSigninLinkAcc))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSigninLinkAcc));
			}		
			objStatus+=String.valueOf(UIFoundation.newUsersetObject(LoginPage.txtLoginEmail,userEmail));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnSignIn));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			log.info("The execution of the method login ended here ...");
			if(expectedState.equalsIgnoreCase(actualState))
			{
				objStatus+=true;
			    String objDetail="Expected state name and actual state name matched";
			    logger.pass(objDetail);
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    System.out.println("Expected satte name and actual state name matched");
			}
			else
			{
				 objStatus+=false;
				 String objDetail="Expected state name and actual state name doest not match";
				 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				 UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}			
			if (objStatus.contains("false"))
			{			
				return "Fail";
			}
			else
			{	
				return "Pass";
			}		
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";		
		}
	}
}
