package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;


public class VerifyGftCrdSucessMsgDispAftrGftCrdApplied extends Desktop {

	
	/***************************************************************************
	 * Method Name : verifyGiftCardAppliedSuccessMsg() 
	 * Created By  : Rakesh C S
	 * Reviewed By : Chandrashekar 
	 * Purpose     : Verification of gift card applied success message
	 ****************************************************************************
	 */

	public static String verifyGiftCardAppliedSuccessMsg() {
		String objStatus=null;
		String actGftCrdSuccessMsg;
		String expGftCrdSuccessMsg;
		String screenshotName = "Scenarios_verifyGiftCardAppliedSuccessMsg_Screenshot.jpeg";
		try {
			log.info("The execution of the verifyGiftCardAppliedSuccessMsg method strated here");
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(6L);		
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.webDriverWaitForElement(CartPage.spngiftCertificateSuccessMsg, "element", "", 50);
			}
			else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));				
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.webDriverWaitForElement(CartPage.spngiftCertificateSuccessMsg, "element", "", 50);
			}
			expGftCrdSuccessMsg = verifyexpectedresult.txtGiftCrdSuccessMsg;
			actGftCrdSuccessMsg = UIFoundation.getText(CartPage.spngiftCertificateSuccessMsg);
			if(expGftCrdSuccessMsg.equals(actGftCrdSuccessMsg)) {
				objStatus+=true;
				String objDetail="Success! Your Gift card has been added to your Payment Methods message is displayed succesfully";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Success! Your Gift card has been added to your Payment Methods message is not displayed succesfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}	
			log.info("The execution of the method verifyGiftCardAppliedSuccessMsg ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="'VerifyGftCrdSucessMsgDispAftrGftCrdApplied' Test case Failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println("Success! Your Gift card has been added to your Payment Methods message is not displayed");
				return "Fail";
			}
			else
			{
				String objDetail="'VerifyGftCrdSucessMsgDispAftrGftCrdApplied' Test case excuted succesfully";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Success! Your Gift card has been added to your Payment Methods message is displayed succesfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}			
}