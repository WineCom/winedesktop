package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class ErrorMessageDisplayedForTheUserBelow21Years extends Desktop {
	
	/***************************************************************************
	 * Method Name			: ErrorMessageDisplayedForTheUserBelow21Years()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String errorMessageDisplayedForTheUserBelow21Years()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_errorrMsg.jpeg";
			
		try
		{
			log.info("The execution of the method errorMessageDisplayedForTheUserBelow21Years started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkAccountInfo));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(UserProfilePage.txtBrthMonth);
			UIFoundation.clearField(UserProfilePage.txtBrthDay);
			UIFoundation.clearField(UserProfilePage.txtBrthYear);
			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtBrthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtBrthDay,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(UserProfilePage.txtBrthYear, "brthYear"));
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnSaveAccountInfo));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnyearErrorMsg))
			{
				objStatus+=true;
				String objDetail="Appropriate error message is displayed for the user below 21 years in account information page.";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Appropriate error message is not displayed for the user below 21 years in account information page.";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method errorMessageDisplayedForTheUserBelow21Years ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method errorMessageDisplayedForTheUserBelow21Years "+ e);
			return "Fail";
			
		}
	}	

}
