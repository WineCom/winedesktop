package wine.autotest.desktop.test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class OrderCreationWithShippingMethodForNewUser extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	public static String shippingDetails() {
		String objStatus = null;
		 
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightPM));
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(8L);
			String stewardshipSave=UIFoundation.getText(FinalReviewPage.spnstewardShipSavePrice);
			String shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			if(!stewardshipSave.equalsIgnoreCase(shippingAndHandling))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Savings total in the Stewardship upsell does not match the price of 'Shipping & Handling' charges.", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Savings total in the Stewardship upsell is matched the price of 'Shipping & Handling' charges.";
				UIFoundation.captureScreenShot(screenshotpath+"warningMsgShip", objDetail);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}


	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			UIFoundation.clickObject(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.waitFor(5L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentSaveButton"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
			UIFoundation.waitFor(5L);*/
			int actualShippingMethodCharges= Integer.parseInt(shippingAndHandling.replaceAll("[^0-9]",""));
			if(actualShippingMethodCharges==expectedShippingMethodCharges)
			{
				objStatus+=true;
				String objDetail="Shipping method is applied for the order creation";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println(Shipping+" Shipping method is applied for the order creation");
				
			}else
			{
				objStatus+=false;
		    	String objDetail="Shipping method is not applied for the order creation";
		    	UIFoundation.captureScreenShot(screenshotpath+"shippingMethod", objDetail);
				System.out.println("Shipping method is not applied for the order creation");
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass("Order number is placed successfully :"+orderNum);
				System.out.println("Order number is placed successfully: "+orderNum);
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null and cannot placed successfully";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    	logger.fail(objDetail);
			}
			System.out.println("addNewCreditCard :"+objStatus);
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with selecting the shipping method in delivery section for new user test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with selecting the shipping method in delivery section for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}

}
