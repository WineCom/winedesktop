package wine.autotest.desktop.test;

import java.io.IOException;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class AddProdcutsToCartCaptureOrder extends Desktop{

	
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
	
		try {
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortLtoH));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
	
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
        	System.out.println("addToCart1 :"+addToCart1);
        	
			if (addToCart1.equals("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			
			if (addToCart2.equals("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			UIFoundation.waitFor(1L);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			
			if (addToCart3.equals("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
				UIFoundation.waitFor(1L);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			
			if (addToCart4.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
				UIFoundation.waitFor(1L);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			
			if (addToCart5.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
				UIFoundation.waitFor(1L);
			}
			
		/*	addToCart7 = UIFoundation.getText(ListPage.lnkSeventhProductToCart);
			
			if (addToCart7.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.clckObject(ListPage.lnkSeventhProductToCart);
				UIFoundation.waitFor(1L);
			}
			
			addToCart8 = UIFoundation.getText(ListPage.lnkEightProductToCart);
		
			if (addToCart8.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.clckObject(ListPage.lnkEightProductToCart);
				UIFoundation.waitFor(1L);
			}
			
			if (addToCart8.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.clckObject(ListPage.lnkEightProductToCart);
				UIFoundation.waitFor(1L);
			}
			
			if (addToCart8.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.clckObject(ListPage.lnkEightProductToCart);
				UIFoundation.waitFor(1L);
			}
			if (addToCart8.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.clckObject(ListPage.lnkEightProductToCart);
				UIFoundation.waitFor(1L);
			}
			if (addToCart8.contains("Add to Cart")) {
				//UIFoundation.waitFor(1L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.clckObject(ListPage.lnkEightProductToCart);
				UIFoundation.waitFor(6L);
			}
			*/
					
			
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			}
			else {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "listPageContainer");
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			}
			UIFoundation.waitFor(3L);
		//	UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
				UIFoundation.waitFor(10L);
				
			}
			System.out.println("addprodTocrt : "+objStatus);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductToCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductToCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductToCart);
				System.out.println("3) "+products3);

			}
			
		/*	if(!UIFoundation.getText(driver, "FourthProductInCart").contains("Fail"))
			{
				products4=UIFoundation.getText(driver, "FourthProductInCart");
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(driver, "FifthProductInCart").contains("Fail"))
			{
				products5=UIFoundation.getText(driver, "FifthProductInCart");
				System.out.println("5) "+products5);

			}*/
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForExistingUser() {
		String products1=null;
		
		try {
			
			System.out.println("=======================Products Added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductToCart);
				System.out.println("1) "+products1);

			}
			else
			{
				System.out.println("No products are added into the cart");
			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("=======================Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
		//String products1=null;
	public static String productDetailsPresentInCart() {
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			UIFoundation.waitFor(10L);
			System.out.println("=======================Products added in to the cart  =====================");
			/*if(!UIFoundation.getText(driver, "FirstProductInCart").contains("Fail"))
			{
				products1=UIFoundation.getText(driver, "FirstProductInCart");
				System.out.println("1) "+products1);
	
			}*/
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductToCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductToCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductToCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductToCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductToCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductToCart);
				System.out.println("5) "+products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	

}
