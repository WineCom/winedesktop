package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.*;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class InvalidGiftCertificate extends Desktop {
	
	/***************************************************************************
	 * Method Name			: invalidPromoCode()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String invalidGiftCardValidation()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedErrorMsg=null;
		String actualErrorMsg=null;
		 		
		try
		{
			log.info("The execution of the method invalidGiftCardValidation started here ...");
			System.out.println("============Order summary ===============");
			expectedErrorMsg=verifyexpectedresult.inValidGiftCardmsg;
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "InvalidGiftCertificate"));
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
			UIFoundation.waitFor(20L);
			actualErrorMsg=UIFoundation.getText(CartPage.spnGiftErrorCode);
			UIFoundation.waitFor(3L);
			if(actualErrorMsg.equalsIgnoreCase(expectedErrorMsg))
			{
				 	objStatus+=true;
			      String objDetail="Validated the error msg for invalid Gift Card";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Validated the error msg for invalid Gift Card");
				System.out.println(expectedErrorMsg);
			}
			else
			{
				 	objStatus+=false;
				   String objDetail="Verify the error msg for inValid Gift Card  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+"PaymentSection", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"PaymentSection")).build());
				   System.err.println("Verify the error msg for inValid Gift Card is failed ");
				   System.err.println(actualErrorMsg);
			}
			log.info("The execution of the method invalidGiftCardValidation ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method invalidGiftCardValidation "+ e);
			return "Fail";
			
		}
	}

}
