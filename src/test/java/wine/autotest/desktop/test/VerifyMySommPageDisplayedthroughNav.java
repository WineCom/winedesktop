package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.PickedPage;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;

public class VerifyMySommPageDisplayedthroughNav extends Desktop {
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{	
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "compassuser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyMySommPageDisplayedthroughNav()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: The purpose of this method is to verify My Somm page through Navigation
	 ****************************************************************************
	 */
	
	public static String VerifyMySommPageDisplayedthroughNavigation()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_VerifyMySommPageDisplayedthroughNav_Screenshot.jpeg";		
		try
		{
			log.info("The execution of the method VerifyMySommPageDisplayedthroughNav started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetting));
			UIFoundation.waitFor(3L);
			String strPickedSettingTitle = UIFoundation.getText(PickedPage.txtVerifyWelcomPicked);
			String strExistPickedSetting = verifyexpectedresult.PickedSettingTitle;
			System.out.println("strPickedSettingTitle :"+strPickedSettingTitle);
			System.out.println("strExistingPymtName :"+strExistPickedSetting);
			if((strPickedSettingTitle.contains(strExistPickedSetting)))
			{
				objStatus+=true;
				String objDetail="The My somm page is displayed through User-profile>>Picked Settings ";
				logger.pass(objDetail);
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="The My somm page is Not displayed through User-profile>>Picked Settings";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			log.info("The execution of the method VerifyMySommPageDisplayedthroughNav ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("VerifyMySommPageDisplayedthroughNav test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("VerifyMySommPageDisplayedthroughNav test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{		
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";		
		}
	}
}
