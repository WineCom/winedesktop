package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifySubscriptionSettingsSummIsDispInCheckoutNRecipientPaymnt extends Desktop {


	/*********************************************************************************************
	 * Method Name : verifySubscriptionSettingsInChckout() 
	 * Created By  : Rakesh C S
	 * Reviewed By : Chandrashekar 
	 * Purpose     : Verifies Subscription Settings is displayed in Recipient & Payment option
	 ********************************************************************************************
	 */

public static String verifySubscriptionSettingsInChckout() {
	String objStatus=null;
	String screenshotName = "Scenarios_verifySubscriptionSettingsInChckout_Screenshot.jpeg";
	try {
		log.info("The execution of the verifySubscriptionSettingsInChckout method strated here");
		if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsHeader)) {
			objStatus+=true;
			String objDetail="Subscription Settings' summary is  displayed checkout flow";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription Settings' summary is not displayed checkout flow";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.lnkSubscriptionEditOpt)) {
			objStatus+=true;
			String objDetail="Subscription settings edit is   displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Subscription settings edit is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtTargetPrice)) {
			objStatus+=true;
			String objDetail="Target price is displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Target price is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtWineTypes)) {
			objStatus+=true;
			String objDetail="Wine type selected is displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Wine type selected is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtFrequency)) {
			objStatus+=true;
			String objDetail="Frequency is  displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Frequency is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtDiscoveryLevel)) {
			objStatus+=true;
			String objDetail="Frequency is  displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Frequency is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtFreeShipping)) {
			objStatus+=true;
			String objDetail="Free Shipping is displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Free Shipping is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		if(UIFoundation.isDisplayed(PickedPage.txtSatisfactionGurante)) {
			objStatus+=true;
			String objDetail="Satisfaction Gurante is displayed";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}
		else
		{
			objStatus+=false;
			String objDetail="Satisfaction Gurante is not displayed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
		}
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCodeFedEx"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
		UIFoundation.waitFor(10L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
		UIFoundation.waitFor(15L);
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
		{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtFedexBillingAddrss)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
		}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsHeader)) {
				objStatus+=true;
				String objDetail="Subscription Settings' summary is  displayed checkout flow";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription Settings' summary is not displayed checkout flow";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.lnkSubscriptionEditOpt)) {
				objStatus+=true;
				String objDetail="Subscription settings edit is   displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Subscription settings edit is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtTargetPrice)) {
				objStatus+=true;
				String objDetail="Target price is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Target price is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtWineTypes)) {
				objStatus+=true;
				String objDetail="Wine type selected is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Wine type selected is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtFrequency)) {
				objStatus+=true;
				String objDetail="Frequency is  displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Frequency is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtDiscoveryLevel)) {
				objStatus+=true;
				String objDetail="Frequency is  displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Frequency is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtFreeShipping)) {
				objStatus+=true;
				String objDetail="Free Shipping is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Free Shipping is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(PickedPage.txtSatisfactionGurante)) {
				objStatus+=true;
				String objDetail="Satisfaction Gurante is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Satisfaction Gurante is not displayed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
	log.info("The execution of the method verifySubscriptionSettingsInChckout ended here ...");
	if (objStatus.contains("false"))
	{
		String objDetail="'VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt' Test case Failed ";
		logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		System.out.println("Verified the UI for 'Subscription Settings' in Recipient & Payment section is not displayed ");
		return "Fail";
	}
	else
	{
		String objDetail="'VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt' Test case executed succesfully";
		logger.pass(objDetail);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		System.out.println("Verified the UI for 'Subscription Settings' in Recipient & Payment sectionis  displayed");
		return "Pass";
	}
}catch(Exception e)
{
	log.error("there is an exception arised during the execution of the method"+ e);
	return "Fail";
}
}
}