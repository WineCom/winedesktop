package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.UIFoundation;

public class QuantityPicker extends Desktop {
	
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String quantityPick()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		WebElement oEle=null;
		String productPriceBefore=null;
		double prodPriceBefore=0.0;
		boolean isdisplayed=false;
		   String screenshotName = "Scenarios_quantityPick_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method quantityPick started here ...");
			System.out.println("============Order summary before increasing quantity===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);

			if(UIFoundation.isDisplayed(CartPage.spnProductPriceHasStrike)){
				productPriceBefore=UIFoundation.getText(CartPage.spnProductPriceHasSale);
				productPriceBefore=productPriceBefore.replaceAll("[ ]","");
				prodPriceBefore=Double.parseDouble(productPriceBefore);
				prodPriceBefore=UIBusinessFlows.productPrice(prodPriceBefore);
				UIFoundation.waitFor(2L);
			}else{
				productPriceBefore=UIFoundation.getText(CartPage.spnProductPrice);
				productPriceBefore=productPriceBefore.replaceAll("[ ]","");
				prodPriceBefore=Double.parseDouble(productPriceBefore);
				prodPriceBefore=UIBusinessFlows.productPrice(prodPriceBefore);
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.SelectObject(CartPage.spnProductQuantity, "quantity"));
			UIFoundation.waitFor(2L);
			System.out.println("============Order summary after increasing quantity===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);;
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			String productPriceAfter=UIFoundation.getText(CartPage.spnProductPriceTotal);
			productPriceAfter=productPriceAfter.replaceAll("[ ]","");
			double prodPriceAfter=Double.parseDouble(productPriceAfter);
			prodPriceAfter=UIBusinessFlows.productPrice(prodPriceAfter);
			int Quantity = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName,"quantity", 1));
			if(prodPriceBefore*Quantity==prodPriceAfter)
			{
				System.out.println("Product price is verified after increasing the quantity ");
				 objStatus+=true;
			      String objDetail="Product price is verified after increasing the quantity";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
					System.out.println("Product price is not matching after increasing the quantity ");
					objStatus+=false;
				   String objDetail="Product price is not matching after increasing the quantity ";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail);
				System.out.println("The Applied promo code amount is not removed from total amount");
			}
			log.info("The execution of the method quantityPick ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method quantityPick "+ e);
			return "Fail";
			
		}
	}

}
