package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyTheContentsInTheOrderPage extends Desktop {
	
	/***************************************************************************
	 * Method Name : verifyTheContentOntheOrderPage()
	 * Created By : Chandrashekhar
	 *  Reviewed By : Ramesh
	 *  Purpose : The purpose of this method is to verify the detail information displayed on the orderpage after the place an order.
	 * TM-2197
	 ****************************************************************************
	 */

	public static String verifyTheContentOntheOrderPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyTheContentOntheOrderPage.jpeg";
		
		try {
			log.info("The execution of the method verifyTheContentOntheOrderPage started here ...");
			UIFoundation.waitFor(3L);

			if(UIFoundation.isDisplayed(ThankYouPage.spnTickSymbol) && UIFoundation.isDisplayed(ThankYouPage.spnOrderConfirmation) && UIFoundation.isDisplayed(ThankYouPage.spnthanksOnOrderPage) && UIFoundation.isDisplayed(ThankYouPage.spnverifyTextOrderNumberIs) && UIFoundation.isDisplayed(ThankYouPage.lnkOrderAgain) && UIFoundation.isDisplayed(ThankYouPage.spnverifyTextSendThisWine))
			{
				objStatus+=true;
				String objDetail="All the values are verified and properly displayed in the thank you page successfully.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="All the values are not properly displayed in the thank you page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				
			}

			log.info("The execution of the method verifyTheContentOntheOrderPage ended here ...");

			if (objStatus.contains("false")) {
				System.out.println("Verify the contents of Thank You Page test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the contents of Thank You Page test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyTheContentOntheOrderPage " + e);
			return "Fail";
		}

	}

}

