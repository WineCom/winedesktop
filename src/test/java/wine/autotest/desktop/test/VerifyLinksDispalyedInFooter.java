package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyLinksDispalyedInFooter extends Desktop{
	
	/***************************************************************************
	 * Method Name : VerifyLinksDispalyedInFooter() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : 
	 ****************************************************************************
	 */
	
	public static String verifyLinksDispalyedInFooter()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_linksInFooter_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method stewardshipAutoRenewalOption started here ...");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkcustomrCare));
			UIFoundation.waitFor(1L);
			
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnktrackanOrder));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkemlPreferences));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkaboutWineCom));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkaboutUs));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkcareers));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkpress));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkhowToWork));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnksellUs));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkAffilaiteProgram));
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkpartnership));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isElementDisplayed(ListPage.lnkgooglePaly));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkhomePageBanner));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkcustomrCareHeader));
			UIFoundation.waitFor(1L);
			String expectedPageTitle=verifyexpectedresult.customerCarePage;
			String actualPageTitle=driver.getTitle();
			if(expectedPageTitle.equalsIgnoreCase(actualPageTitle)){
				objStatus+=true;
			      String objDetail="Customer care page is displayed.";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="Customer care page is not displayed.";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"customerCare", objDetail);
			}
			log.info("The execution of the method stewardshipAutoRenewalOption ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method stewardshipAutoRenewalOption "+ e);
			return "Fail";
			
		}
	}

}
