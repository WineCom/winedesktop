package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class PasswordExceptsMoreThan15Char extends Desktop {

	static String NewUseremailR = null;

	/*******************************************************************
	 * Method name: VerifyPasswordExceptsMoreThan15Char TM-4071 :
	 ******************************************************************
	 */

	public static String verifyPasswordExceptsMoreThan15Char() {

		String objStatus = null;
		// String NewUseremailR=null;
		String screenshotName = "PasswordExceptsMoreThan15Char";
		
		try {
			log.info("account creation method started here:");
			UIFoundation.waitFor(3L);
			objStatus = String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			if (UIFoundation.isDisplayed(LoginPage.btnCreateAcc)) {
				objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail, "email"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "grt15char"));
			UIFoundation.waitFor(2L);
			NewUseremailR = UIFoundation.getAttribute(LoginPage.txtEmail);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
			UIFoundation.waitFor(7L);
			log.info("account creation method ended here:");
			if (objStatus.contains("false")) {
				String objDetail = "User profile creation  test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			} else {
				String objDetail = "User profile creation  test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account " + e);
			return "Fail";
		}
	}

	/*******************************************************************
	 * Method name: LoginusingSamepassword TM-4071 :
	 ******************************************************************
	 */
	public static String loginusingSamepassword() {

		String objStatus = null;
		String screenshotName = "LoginusingSamepassword";
		
		try {
			log.info("login method started here:");
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.lnkSignout));
			driver.findElement(By.xpath("//input[@name='email']")).sendKeys(NewUseremailR);
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "grt15char"));
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus += String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false")) {
				String objDetail = "User able to login  successfully";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			} else {
				String objDetail = "user is able to login";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method login " + e);
			return "Fail";

		}
	}

}
