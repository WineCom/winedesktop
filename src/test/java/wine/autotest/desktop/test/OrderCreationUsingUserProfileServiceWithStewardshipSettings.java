package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;


public class OrderCreationUsingUserProfileServiceWithStewardshipSettings extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		   String screenshotName = "Scenarios__joinUs.jpeg";
			
		try {
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkRhoneBlends));
			UIFoundation.waitFor(2L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkSecondProductToCart));
			}
			//UIFoundation.scrollDownOrUpToParticularElement(driver, "ThirdProductToCart");
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}

			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkStewardshipSet));
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnstewardshipHeader)){
				  objStatus+=true;
			      String objDetail="'Stewardship Settings header' is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Stewardship Settings header' is not displayed";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(UserProfilePage.spnjoinUsToday)){
				  objStatus+=true;
			      String objDetail="'Join stewardship today' is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Join stewardship today' is not displayed";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(UserProfilePage.spncompleteTermsAndCondition)){
				  objStatus+=true;
			      String objDetail="'Read our complete terms and conditions' link is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				
			}else{
				 objStatus+=false;
			       String objDetail="'Read our complete terms and conditions' link is not displayed";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.btnStewardshipAdd));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(UserProfilePage.btnStewardshipConfirm)){
				  objStatus+=true;
				  objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.btnStewardshipConfirm));
				  UIFoundation.waitFor(8L);
			}
			if(UIFoundation.isDisplayed(UserProfilePage.btnStewardShipContinue)){
				  objStatus+=true;
				  objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.btnStewardShipContinue));
				  UIFoundation.waitFor(5L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productDetailsPresentInCart() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		String expectedStewardship=null;
		
		try {
			expectedStewardship=verifyexpectedresult.stewardshipSettings;
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductInCart);
				System.out.println("1) "+products1);
				if(products1.equalsIgnoreCase(expectedStewardship))
				{
					System.out.println("Stewardship Annual member added successfully");
				}
	
			}
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductInCart);
				System.out.println("2) "+products2);
				if(products2.equalsIgnoreCase(expectedStewardship))
				{
					System.out.println("Stewardship Annual member added successfully");
				}

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductInCart);
				System.out.println("3) "+products3);
				if(products3.equalsIgnoreCase(expectedStewardship))
				{
					System.out.println("Stewardship Annual member added successfully");
				}

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductInCart);
				System.out.println("4) "+products4);
				if(products4.equalsIgnoreCase(expectedStewardship))
				{
					System.out.println("Stewardship Annual member added successfully");
				}

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductInCart);
				System.out.println("5) "+products5);
				if(products5.equalsIgnoreCase(expectedStewardship))
				{
					System.out.println("Stewardship Annual member added successfully");
				}
			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	public static String shippingDetails() {
		String objStatus = null;
		   String screenshotName = "Scenarios__StandardshippingMethod.jpeg";
				
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "SuggestedAddress"));
			UIFoundation.waitFor(6L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeDate)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				String standardShippingPrice=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
				String expectedShippingPrice=verifyexpectedresult.standardShippingPrice;
				if(standardShippingPrice.equalsIgnoreCase(expectedShippingPrice)){
					
					objStatus+=true;
					logger.pass("Standard shipping is displayed as $0 for StewardShip members in delivery section");
					ReportUtil.addTestStepsDetails("Standard shipping is displayed as $0 for StewardShip members in delivery section", "Pass", "");
				}else{
					objStatus+=false;
					String objDetail="Standard shipping is not displayed as $0 for StewardShip members in delivery section' charges.";
					logger.fail(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+"warningMsgShip", objDetail);
				}
				UIFoundation.waitFor(1L);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}else{
				
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			expected = verifyexpectedresult.placeOrderConfirmation;
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			UIFoundation.clickObject(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
/*			UIFoundation.javaScriptClick(driver, "BillingAndShippingCheckbox");
			UIFoundation.waitFor(1L);*/
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			UIFoundation.waitFor(15L);
			  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);
	                  
	            }               
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				logger.pass("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
		    	objStatus+=true;			
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null.Order not placed successfully";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			
			}
			UIFoundation.waitFor(1L);
			  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);
	                  
	            }               
			if(UIFoundation.isDisplayed(ThankYouPage.lnkOrderNumber))
			{
				objStatus+=true;
				logger.pass("Order number is displayed as a link in the Order success Thank you page");
				ReportUtil.addTestStepsDetails("Order number is displayed as a link in the Order success Thank you page", "Pass", "");
			}else{
				objStatus+=false;
		    	String objDetail="Order number is not displayed as a link in the Order success Thank you page";
		    	logger.fail(objDetail);
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation using user profile services with stewardship settings for new user  test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation using user profile services with stewardship settings for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
}