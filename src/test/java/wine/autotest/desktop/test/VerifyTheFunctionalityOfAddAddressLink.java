package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyTheFunctionalityOfAddAddressLink extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyTheFunctionalityOfAddAddressLink()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String verifyTheFunctionalityOfAddAddressLink() {
	String objStatus=null;

	   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(4L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(20L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(4L);
			}

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.spnRecipientaddressFormHeaderr))
			{
				  objStatus+=true;
			      String objDetail="Verified the functionality of 'Add Address' link";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      System.out.println("Verify the functionality of 'Add Address' link test case is executed successfully");   
			}else{
				objStatus+=false;
				String objDetail="Verify the functionality of 'Add Address' link test case is failed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the functionality of 'Add Address' link test case is failed");
				logger.fail("Verify the functionality of 'Add Address' link test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the functionality of 'Add Address' link test case is executed successfully");
				logger.pass("Verify the functionality of 'Add Address' link test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyTheFunctionalityOfAddAddressLink "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
