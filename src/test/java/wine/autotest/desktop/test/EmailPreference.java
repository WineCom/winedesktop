package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class EmailPreference extends Desktop {
	
	/***************************************************************************
	 * Method Name			: saveButtonFunctionality()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 * TM-3532
	 ****************************************************************************
	 */
	
	public static String saveButtonFunctionality()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
			
		try
		{
			log.info("The execution of the method login started here ...");

			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			//objStatus+=String.valueOf(UIFoundation.clckObject(driver, "signInLink"));
			UIFoundation.waitFor(3L);
			/*if(UIFoundation.isDisplayed(driver, "signInLinkAccount")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "signInLinkAccount"));
			}*/
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(16L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.EmailPreferences));
			if(UIFoundation.isDisplayed(LoginPage.btnverifySave))
			{
				 	objStatus+=true;
			      String objDetail=" 'Save' button is displayed in Email Preferences section under user profile";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      System.out.println("'Save' button is displayed in Email Preferences section under user profile");   
				
			}else{
				   objStatus+=false;
				   String objDetail=" 'Save' button is not displayed in Email Preferences section under user profile";
				   logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the 'Save' button is displayed in Email Preference section under user profile test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'Save' button is displayed in Email Preferences section under user profile test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method saveButtonFunctionality "+ e);
			return "Fail";
			
		}
	}	

}
