package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.UIFoundation;


public class CreateNewAccountWhileRatingTheProduct extends Desktop {
	
	/***************************************************************************
	 * Method Name			: createNewAccountWhileRatingTheProduct()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String createNewAccountWhileRatingTheProduct()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method productAttributeDescriptionInListPage started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkrateProduct));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(LoginPage.lnkPIPCreateAccount))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.lnkPIPCreateAccount));
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "accPassword"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnCreateAccount, "Invisible", "", 50);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+= String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			log.info("The execution of the method productAttributeDescriptionInListPage ended here ...");
			if (objStatus.contains("false"))
			{
			System.out.println("Verify user is able to create New Account while Rating the Product test case failed");
			String objDetail = "Verify user is able to create New Account while Rating the Product test case failed";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			}
			else
			{
				System.out.println("Verify user is able to create New Account while Rating the Product test case executed successfully");
				logger.pass("Verify user is able to create New Account while Rating the Product test case executed successfully");
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method productAttributeDescriptionInListPage "+ e);
			return "Fail";
			
		}
	}	

}
