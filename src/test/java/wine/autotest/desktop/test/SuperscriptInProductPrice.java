package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class SuperscriptInProductPrice extends Desktop {
	
	/***************************************************************************
	 * Method Name			: superscriptInProductPrice()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String superscriptInProductPrice()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_superscriptInProductPrice.jpeg";
			
		try
		{
			log.info("The execution of the method superscriptInProductPrice started here ...");
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnproductPriceRegWhole);
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spnproductPriceRegWhole) && UIFoundation.isDisplayed(ListPage.spnproductPriceRegFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in home page.";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in home page.";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(6L);
			if(UIFoundation.isDisplayed(ListPage.spnproductPriceSaleWhole) && UIFoundation.isDisplayed(ListPage.spnproductPriceSaleFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in List page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in List page.";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddProductToPIP));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(ListPage.spnproductPriceSaleWhole) && UIFoundation.isDisplayed(ListPage.spnproductPriceSaleFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in PIP page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in PIP page.";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
		//	System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.spnproductPriceSaleWhole) && UIFoundation.isDisplayed(ListPage.spnproductPriceSaleFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in Cart page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in Cart page.";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method superscriptInProductPrice ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method superscriptInProductPrice "+ e);
			return "Fail";
			
		}
	}	

}
