package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyTheAddressInRecipientPage extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String recipientLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
				
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "recipientEditUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyTheAddressInRecipientPage() {
		String objStatus=null;
		   
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		//	driver.navigate().refresh();
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(2L);
			}
			
			String shippingAddresCountBfr=UIFoundation.getText(FinalReviewPage.lnkshippingAddressCountInRcp);
			if(shippingAddresCountBfr.contains("Fail")){
					objStatus+=false;
				   String objDetail="Unable to read shipping address count is failed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"count", objDetail);
			}
			int shippingAddressCountBfr=Integer.parseInt(shippingAddresCountBfr.replaceAll("[^0-9]", ""));
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdt));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtshippingAddressFullName);
			UIFoundation.clearField(FinalReviewPage.txtshippingAddressStreetAdrsClear);
			UIFoundation.clearField(FinalReviewPage.txtshippingAddressCityClear);
			UIFoundation.waitFor(3L);
			UIFoundation.clckObject(FinalReviewPage.lnkshippingOptionalClk);
			String expectedEditedName=UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtshippingAddressFullNameEnter, "firstName");
			expectedEditedName=expectedEditedName.replaceAll(" ", "");
			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtshippingAddressStreetAdrsEnter, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtshippingAddressCityEnter, "firstName"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSaveBtn));
			UIFoundation.waitFor(3L);
			UIFoundation.escapeBtn(driver);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				 objStatus+=true;
			      String objDetail="Modal/Light Box does not closes on clicking 'ESC' key for the Modal/Light box without  ' X' icon";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}else{
				 objStatus+=false;
				   String objDetail="Modal/Light Box  closed on clicking 'ESC' key for the Modal/Light box without  ' X' icon";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+"RecPaddrs", objDetail);
			}
			String actualEditedName=UIFoundation.getText(FinalReviewPage.spnshippingAddressFullNameAfterEdit);
			actualEditedName=actualEditedName.replaceAll(" ", "");
			String shippingAddresCountAftr=UIFoundation.getText(FinalReviewPage.lnkshippingAddressCountInRcp);
			int shippingAddressCountAftr=Integer.parseInt(shippingAddresCountAftr.replaceAll("[^0-9]", ""));
			if(actualEditedName.equalsIgnoreCase(expectedEditedName) && (shippingAddressCountBfr==shippingAddressCountAftr))
			{
				  objStatus+=true;
			      String objDetail="Verified Address does not get duplicated on editing the original address in Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Address should not get duplicated on editing the original address in Recipient page is failed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   UIFoundation.captureScreenShot(screenshotpath+"RecPaddrs", objDetail);
			}
			
/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "shippingAddressEdtRemove"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "removeThisAddress"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "YesremoveThisAddress"));
			UIFoundation.waitFor(2L);*/
			String shippingAddresCountBfrRemove=UIFoundation.getText(FinalReviewPage.lnkshippingAddressCountInRcp);
			int shippingAddressCountBfrRemove=Integer.parseInt(shippingAddresCountBfrRemove.replaceAll("[^0-9]", ""));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdtRemove));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkremoveThisAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkYesremoveThisAddress));
			UIFoundation.waitFor(3L);
			String shippingAddresCountAftrRemove=UIFoundation.getText(FinalReviewPage.lnkshippingAddressCountInRcp);
			int shippingAddressCountAftrRemove=Integer.parseInt(shippingAddresCountAftrRemove.replaceAll("[^0-9]", ""));
			if(shippingAddressCountAftrRemove==(shippingAddressCountBfrRemove-1)){
				  objStatus+=true;
			      String objDetail="Verified the address gets deleted permanently from the Recipient page on clicking 'Remove this address'";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the address gets deleted permanently from the Recipient page on clicking 'Remove this address' is failed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   UIFoundation.captureScreenShot(screenshotpath+"RecpAddrss", objDetail);
			}
			UIFoundation.waitFor(1L);
			String shippingAddresCountBeforeAddingNewAddress=UIFoundation.getText(FinalReviewPage.lnkshippingAddressCountInRcp);
			int shippingAddresCountBeforeAddingNewAddres=Integer.parseInt(shippingAddresCountBeforeAddingNewAddress.replaceAll("[^0-9]", ""));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(13L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(2L);
			String shippingAddresCountafterAddingNewAddress=UIFoundation.getText(FinalReviewPage.lnkshippingAddressCountInRcp);
			int shippingAddresCountafterAddingNewAddres=Integer.parseInt(shippingAddresCountafterAddingNewAddress.replaceAll("[^0-9]", ""));
			
			if(shippingAddresCountafterAddingNewAddres==(shippingAddresCountBeforeAddingNewAddres+1)){
				  objStatus+=true;
			      String objDetail="Verified the address gets saved only once on adding the address in Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the address gets saved only once on adding the address in Recipient page is failed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				   UIFoundation.captureScreenShot(screenshotpath+"addressBook", objDetail);
			}
			
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
		
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}

}
