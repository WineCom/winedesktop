package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class StewardshipUpsellInFinalReviewPAgeForStandardShipping extends Desktop {
	
	/***************************************************************************
	 * Method Name : shippingDetails() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	public static String shippingDetails() {
		String objStatus = null;
		String screenshotName = "Scenarios__shippingMethod.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(26L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
//			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkStandardShip));
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(8L);
			
			String shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			shippingAndHandling = shippingAndHandling.replaceAll("[$,]","");
			double shipingAndHandling = Double.parseDouble(shippingAndHandling);
			if(shipingAndHandling<49){
				String stewardShipDifferenceAmount=UIFoundation.getText(FinalReviewPage.spnstewardShipDifferenceAmount);
				stewardShipDifferenceAmount = stewardShipDifferenceAmount.replaceAll("[$,]", "");
				double stewardShipDiferenceAmount = Double.parseDouble(stewardShipDifferenceAmount);
				if(49-shipingAndHandling==stewardShipDiferenceAmount)
					{
						objStatus+=true;
						logger.pass("Savings total in the Stewardship upsell is matched the price of 'Shipping & Handling' charges.");
						ReportUtil.addTestStepsDetails("Savings total in the Stewardship upsell is matched the price of 'Shipping & Handling' charges.", "Pass", "");
					}else{
						objStatus+=false;
						String objDetail="Savings total in the Stewardship upsell doesnot match the price of 'Shipping & Handling' charges.";
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
						UIFoundation.captureScreenShot(screenshotpath+"warningMsgShip", objDetail);
					}
				
			}else{
				String jionStewardshipExpMsg=verifyexpectedresult.joinStewardshipJust;
				String jionStewardshipActMsg=UIFoundation.getText(FinalReviewPage.joinStewardshipJust);
				if(jionStewardshipActMsg.equalsIgnoreCase(jionStewardshipExpMsg))
				{
					objStatus+=true;
					logger.pass("Savings total in the Stewardship upsell is matched the price of 'Shipping & Handling' charges.");
					ReportUtil.addTestStepsDetails("Savings total in the Stewardship upsell is matched the price of 'Shipping & Handling' charges.", "Pass", "");
				}else{
					objStatus+=false;
					String objDetail="Savings total in the Stewardship upsell doesnot match the price of 'Shipping & Handling' charges.";
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					UIFoundation.captureScreenShot(screenshotpath+"warningMsgShip", objDetail);
				}
			}
			
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
