package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class GiftMessageRemainingCharacterCountdown extends Desktop{
	
	/***************************************************************************
	 * Method Name : editRecipientPlaceORder() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String editRecipientEnterGiftMessage() {
		String objStatus = null;
		   String screenshotName = "Scenarios_editRecGgt_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(26L);  
			
			//UIBusinessFlows.recipientEdit(driver);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientShipToaddress));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkRecipientGift);
			UIFoundation.waitFor(2L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtgiftMessageTextArea))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkRecipientGift));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.dwnmaximumCharacter)){
				objStatus+=true;
			      String objDetail="'240 characters remaining' is displayed.";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="character countdown starting in a state of (240 characters remaining) is not displayed.";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			String giftMessageInptTextCount= XMLData.getTestData(testScriptXMLTestDataFileName, "giftMessageCharacter", 1);
			int giftMessageInputTextCount=giftMessageInptTextCount.length();
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessageCharacter"));
			UIFoundation.waitFor(2L);
			String giftMessage=UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
			int giftMessageOutputTextCount=giftMessage.length();
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));
			if(UIFoundation.isDisplayed(FinalReviewPage.dwnlimitCharacter) &&(giftMessageOutputTextCount<giftMessageInputTextCount) ){
				objStatus+=true;
			      String objDetail="'0 characters remaining' is displayed.";
			      System.out.println(objDetail);
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="0 character is not displayed.";
			       System.out.println(objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}

}
