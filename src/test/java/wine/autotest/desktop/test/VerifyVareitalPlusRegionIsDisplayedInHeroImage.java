package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyVareitalPlusRegionIsDisplayedInHeroImage extends Desktop {

	/***************************************************************************
	 * Method Name			: VerifyVareitalPlusRegionIsDisplayedInHeroImage()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4221
	 ****************************************************************************
	 */


	public static String varietalPlusResgionInHeroImage() {
	
		String objStatus = null;
		String screenshotName = "varietalPlusResgionInHeroImage.jpeg";
		
	try {
		log.info("varietal Plus Resgion In HeroImage method started here" );
		objStatus+=String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkRegion));
		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFiltercalifornia));
		UIFoundation.waitFor(5L);
		if(UIFoundation.isDisplayed(ListPage.spnheroImageRegionText) && UIFoundation.isDisplayed(ListPage.spnheroImageVarietalText)) {
	    objStatus+=true;
	    String objDetail = "Verified Region + varietal is displayed in hero Image";
	    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	    logger.pass(objDetail);
		}
		else {
		objStatus+=false;
		 String objDetail = " Region + varietal is not displayed in hero Image";
		 UIFoundation.captureScreenShot(screenshotpath, objDetail);
		 logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
		if(objStatus.contains("false")) {
			System.out.println("Verify Vareital Plus Region Is Displayed In HeroImage test case is failed");
			return "Fail";
		} else {
			System.out.println("Verify Vareital Plus Region Is Displayed In HeroImage test case executed succesfully");
			return "Pass";
		}
	} catch (Exception e) {

		log.error("there is an exception arised during the execution of the method getOrderDetails " + e);

		return "Fail";
	}
	}
	}