package wine.autotest.desktop.test;



import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyErrorDisplayedForInvalidProductId extends Desktop {
	

	/***************************************************************************
	 * Method Name			: verifyErrorDisplayedForInvalidProductId()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyErrorDisplayedForInvalidProductId() {
		String objStatus=null;
		   String screenshotName = "Scenarios__ProductInvalidId.jpeg";
			
		try {
			log.info("The execution of the method verifyErrorDisplayedForInvalidProductId started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkaddProductToPIP));
			UIFoundation.waitFor(2L);
			String productURl=driver.getCurrentUrl();
			driver.get(productURl+"james");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnEror404);
			if(UIFoundation.isDisplayed(ListPage.spnEror404))
			{
				  objStatus+=true;
			      String objDetail="'Error 404' is displayed";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Error 404' is not displayed";
				   logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method verifyErrorDisplayedForInvalidProductId ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyErrorDisplayedForInvalidProductId "+ e);
			return "Fail";
		}

	}

}
