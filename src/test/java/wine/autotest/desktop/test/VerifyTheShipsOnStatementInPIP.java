package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyTheShipsOnStatementInPIP extends Desktop{
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyTheShipsOnStatementInPIP() {
		String objStatus = null;
		String product1 = null;
		String product2 = null;
		
			
		try {
		    /*UIFoundation.SelectObject(driver, "SelectState", "floridaState");
	        UIFoundation.waitFor(10L);*/
            objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkShipsSoonestR));
			UIFoundation.waitFor(5L);
			product1 = UIFoundation.getText(ListPage.spnfirstProdName);
			product2 = UIFoundation.getText(ListPage.spnsecondProdName);
			if (product1.contains("Ships today if ordered ")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstProdNameLink));
			}else if (product2.contains("Ships today")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnksecondProdNameLink));
			}else{
				objStatus +=false;
				String objDetail="No ship today products available";
				UIFoundation.captureScreenShot(screenshotpath+"_ChoosePhoto", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(ListPage.spnShipsOnStatementInPIP))
			{
				  objStatus+=true;
			      String objDetail="Verified the Ships on statement in PIP for the products that will be shipped today";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
	
			}
			else
			{
				objStatus +=false;
				String objDetail="Verify the Ships on statement in PIP for the products that will be shipped today is failed";
				UIFoundation.captureScreenShot( screenshotpath+"_ChoosePhoto", objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			if (objStatus.contains("false")) {
				System.out.println("Verify the Ships on statement in PIP for the products that will be shipped today test case failed");
				return "Fail";
			} else {
				System.out.println("Verify the Ships on statement in PIP for the products that will be shipped today test case executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}