package wine.autotest.desktop.test;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;

import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary extends Desktop {
	/***************************************************************************
	 * Method Name			: applyGiftCode()
	 * Created By			: Chandrashekhar KB  
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */

	public static String applyGiftCode()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_applyGiftCode_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;
		try
		{
			log.info("The execution of the method applyGiftCode started here ...");
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));				
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(20L);
			}
			else
			{
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGift));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkGiftApply));
				UIFoundation.waitFor(20L);				
			}
			if(UIFoundation.isDisplayed(CartPage.spngiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Gift cards are applied succesfully";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}
			else
			{
				objStatus+=false;
				String objDetail="Gift cards are not applied";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			log.info("The execution of the method applyGiftCode ended here ...");
			if (objStatus.contains("false"))
			{				
				return "Fail";
			}
			else
			{				
				return "Pass";
			}
		}
		catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method applyGiftCode "+ e);
			return "Fail";
		}
	}
	
		
	/***************************************************************************
	 * Method Name			: verifyGiftCardAmountInOrderSummary()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyGiftCardAmountInOrderSummary() {	
	String expected=null;	
	String objStatus=null;	
	String giftCardRmaining=null;
	String giftCardUsed=null;
	String giftCardInOrderHistory=null;
	String screenshotName = "Scenarios_checkoutProcess_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;
		try {
			log.info("The execution of the method verifyGiftCardAmountInOrderSummary started here ...");
		
			expected=verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(4L);						
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
			else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutAlter));
			}
			UIFoundation.waitFor(8L);		
			UIFoundation.waitFor(8L);			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}
			UIFoundation.waitFor(3L);         	
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);				
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);				
			}
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
			   {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
			   }			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtGiftCardUsed);
		    giftCardUsed = UIFoundation.getText(FinalReviewPage.txtGiftCardUsed);
			giftCardRmaining = UIFoundation.getText(FinalReviewPage.txtGiftCardRemaining);
			UIFoundation.waitFor(2L);						
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(12L);			
			 if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);                  
	            }                                        
	            objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.lnkOrderNumber));
	            UIFoundation.waitFor(1L);
	           UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtGiftCardUsed);
				UIFoundation.waitFor(1L);				
				giftCardInOrderHistory=UIFoundation.getText(FinalReviewPage.txtGiftCardUsed);
				if(giftCardUsed.contains(giftCardInOrderHistory)&&(!UIFoundation.isDisplayed(FinalReviewPage.txtGiftCardRemaining)))
				{
					objStatus+=true;
					String objDetail = "Gift card Remaining is displayed in the order summary section ";
					logger.pass(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		    			
				}
				else
				{
					objStatus+=false;
			    	String objDetail="Gift card remaining is not displayed in the order summary section ";
			    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
				}					
			log.info("The execution of the method verifyGiftCardAmountInOrderSummary ended here ...");
			if (objStatus.contains("false")) 
			{
				return "Fail";
			}
			else 
			{				
				return "Pass";
			}
		} 
		catch (Exception e) 
		{
			
			log.error("there is an exception arised during the execution of the method verifyGiftCardAmountInOrderSummary "
					+ e);
			return "Fail";
		}
	}

}
