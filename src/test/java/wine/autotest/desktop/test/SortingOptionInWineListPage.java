package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;

public class SortingOptionInWineListPage extends Desktop {

	/***************************************************************************
	 * Method Name : sortingOptionsAtoZ() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB 
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingOptionsAtoZ() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsAtoZ started here ...");
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortA_Z));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIBusinessFlows.validationForSortingAtoZ());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsAtoZ ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsAtoZ "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsZtoA() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsZtoA() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsZtoA started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortA_Z));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIBusinessFlows.validationForSorting());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsZtoA ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsZtoA "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsLtoH() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsLtoH() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsLtoH started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortLtoH));
			UIFoundation.waitFor(4L);

			objStatus += String.valueOf(UIBusinessFlows.validationForSortingPriceLtoH());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsLtoH ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsLtoH "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsHtoL() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsHtoL() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsHtoL started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortHtoL));
			UIFoundation.waitFor(4L);
			UIBusinessFlows.validationForSortingPriceHtoL();
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsHtoL ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsHtoL "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : sortingOptionsZtoA() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	/*public static String sortingOptions() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsZtoA started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SortNtoO"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SortOtoN"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SortSavings"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SortTopRated"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SortTopRated"));
			log.info("The execution of the method sortingOptionsZtoA ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsZtoA "
					+ e);
			return "Fail";
		}
	}*/

}
