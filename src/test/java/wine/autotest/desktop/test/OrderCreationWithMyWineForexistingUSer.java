package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import wine.autotest.fw.utilities.ReportUtil;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithMyWineForexistingUSer extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addMyWineProdTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addMyWineProdTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(8L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
			public static String checkoutProcess() {
				
				String expected=null;
				String actual=null;
				String objStatus=null;
				String subTotal=null;
				String shippingAndHandling=null;
				String total=null;
				String salesTax=null;
				String orderNum=null;
				   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
					
					try {
						log.info("The execution of the method checkoutProcess started here ...");
						expected =verifyexpectedresult.placeOrderConfirmation;
						objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
					//	driver.navigate().refresh();
						objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
						UIFoundation.waitFor(0);
						driver.navigate().refresh();
						if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
						{
							UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
							objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
							UIFoundation.waitFor(10L);
						}
						if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
							objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
							UIFoundation.waitFor(10L);
						}
						if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
							UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
							objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
							objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
							objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
							//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
						}
						if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
						UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
						objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
						UIFoundation.waitFor(1L);
						}
							if(UIFoundation.isDisplayed(FinalReviewPage.spnFinalReviewPage))
							{
								  objStatus+=true;
							      String objDetail="User is able to navigate directly to Final review page on entering 'CVV' in 'Edit credit card' popup";
							      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
							      System.out.println("User is able to navigate directly to Final review page on entering 'CVV' in 'Edit credit card' popup ");   
							}else{
								objStatus+=false;
								String objDetail="User is Not able to navigate directly to Final review page on entering 'CVV' in 'Edit credit card' popup";
								//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
								UIFoundation.captureScreenShot(screenshotpath+"final", objDetail);
							}
						

							System.out.println("============Order summary in the Final Review Page  ===============");
							subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
							shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
							total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
							salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
							System.out.println("Subtotal:              "+subTotal);
							System.out.println("Shipping & Handling:   "+shippingAndHandling);
							System.out.println("Sales Tax:             "+salesTax);
							System.out.println("Total:                 "+total);
							UIFoundation.waitFor(4L);
							UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
							UIFoundation.waitFor(1L);
							objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//							UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
							UIFoundation.waitFor(6L);
							  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
					            {
					                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
					                  UIFoundation.waitFor(5L);
					                  
					            }
							orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
							if(orderNum!="Fail")
							{
								System.out.println("Order Number :"+orderNum);
								logger.pass("Order Number :"+orderNum);
								UIFoundation.getOrderNumber(orderNum);
						    	objStatus+=true;			
							}else
							{
								objStatus+=false;
						    	String objDetail="Order number is null.Order not placed successfully";
						    	logger.fail("Order number is null.Order not placed successfully");
						    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
							
							}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				System.out.println("Order creation with My Wine products test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with My Wine products test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			return "Fail";
		}
	}

}
