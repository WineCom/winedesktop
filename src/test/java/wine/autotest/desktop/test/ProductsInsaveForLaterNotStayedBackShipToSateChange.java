package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class ProductsInsaveForLaterNotStayedBackShipToSateChange extends Desktop {
	
	/***************************************************************************
	 * Method Name			: ProductsInsaveForLate()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productsInsaveForLate() {
		String objStatus = null;
		String screenshotName = "Scenarios_ OutBound_Screenshot.jpeg";
		
		String state=null;
		try {
			
//			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.imgWineLogo));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//form[@class='formWrap searchBarForm']/div/div/select[@class='state_select state_select-promptsCartTransfer'])[1]"));
			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
			sel.selectByVisibleText("DE");
			UIFoundation.waitFor(20L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btncontinueShipToDE));
			UIFoundation.waitFor(9);
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.spnyourCartIsEmpty)){
				objStatus+=true;
			      String objDetail="Your cart is empty text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Your cart is empty text is displayed");
				  if(UIFoundation.isDisplayed(CartPage.spncurrentlyUnavailable)){
						objStatus+=true;
					      String objDetail1="Cart page is be displayed and the products are  moved to Save for Later and marked as currently unavailable";
					      ReportUtil.addTestStepsDetails(objDetail1, "Pass", "");
					      logger.pass(objDetail1);
					      System.out.println("Cart page is be displayed and the products are  moved to Save for Later and marked as currently unavailable");
						
					}else
					{
						objStatus+=false;
						String objDetail1="Cart page is be not displayed and the products are not marked as currently unavailable";
						//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
						UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail1);
					}
				
			}else
			{
				objStatus+=false;
				String objDetail="Your cart is empty text is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
//			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.imgWineLogo));
			UIFoundation.waitFor(3L);
			WebElement ele1=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[1]"));
			org.openqa.selenium.support.ui.Select sel1=new org.openqa.selenium.support.ui.Select(ele1);
			sel1.selectByVisibleText("CA");
			UIFoundation.waitFor(20L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.spnyourCartIsEmpty)){
				objStatus+=true;
			      String objDetail="Your cart is empty text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Your cart is empty text is displayed");
				  if(UIFoundation.isDisplayed(CartPage.lnkmoveToCartFirstLink)){
						objStatus+=true;
					      String objDetail1="Products are displayed under save for later with 'Move to cart' button";
					      ReportUtil.addTestStepsDetails(objDetail1, "Pass", "");
						  System.out.println("Products are displayed under save for later with 'Move to cart' button");
						
					}else
					{
						objStatus+=false;
						String objDetail1="Products are not displayed under save for later with with 'Move to cart' button";
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
						//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
						UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail1);
					}
				
			}else
			{
				objStatus+=false;
				String objDetail="Your cart is empty text is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
	
			
			
		} catch (Exception e) {
			String objDetail="Your cart is empty text is not displayed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, "");
			return "Fail";
		}
	}

}
