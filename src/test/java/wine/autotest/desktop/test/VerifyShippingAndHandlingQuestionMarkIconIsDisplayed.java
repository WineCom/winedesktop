package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyShippingAndHandlingQuestionMarkIconIsDisplayed extends Desktop {

	
	/***************************************************************************
	 * Method Name			: VerifyPinkBarDisplayed()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4215
	 ****************************************************************************
	 */
public static String ShippingAndHandlingQuestionMarkIconIsDisplayedInCart() {
	
	String objStatus=null;
	String screenshotName = "ShippingAndHandlingQuestionMarkIconIsDisplayedInCart.jpeg";
    
	try {
		log.info("Shipping And Handling Question Mark Icon Is Displayed");
		//UIFoundation.scrollDownOrUpToParticularElement(driver, "stewardQuestioMarkIcon");
		if(UIFoundation.isElementDisplayed(CartPage.stewardQuestioMarkIcon)) {
		objStatus+=true;
		objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.stewardQuestioMarkIcon));
		String objDetail="Stewardhip Questionmark icon is displayed in Order Summary";
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
		logger.pass(objDetail);
	}else{
		objStatus+=false;
		String objDetail="Stewardhip Questionmark icon is not displayed in Order Summary";
		//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
	  String actwhyShippingMess=verifyexpectedresult.whyShpping;
	  String expactwhyShippingMess=UIFoundation.getText(CartPage.spnwhyshippText);
	  if(actwhyShippingMess.equals(expactwhyShippingMess)) {
		  objStatus+=true;
			String objDetail="Verified Why is shipping wine so expensive' text is the popup";
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			logger.pass(objDetail);
		}else{
			objStatus+=false;
			String objDetail="Verification Why is shipping wine so expensive' text doesnt not match";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}	
	  
	  if(UIFoundation.isElementDisplayed(CartPage.btnlearnMore)) {
			objStatus+=true;
			String objDetail="Stewardhip learnMore button is displayed";
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			logger.pass(objDetail);
		}else{
			objStatus+=false;
			String objDetail="Stewardhip learnMore button is not displayed";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
	  
	  if (objStatus.contains("false"))
		{
			System.out.println("Verify ShippingAnd Handling QuestionMark Icon Is Displayed test case is failed");
			return "Fail";
		}
		else
		{
			System.out.println("Verify ShippingAnd Handling QuestionMark Icon Is Displayed test case passed succesfully");
			return "Pass";
		}
	}catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
		return "Fail";
	}
}
	}