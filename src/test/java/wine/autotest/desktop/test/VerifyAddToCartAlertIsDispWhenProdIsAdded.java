package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyAddToCartAlertIsDispWhenProdIsAdded  extends Desktop{

	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInHomePage
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInHomePage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInHomePage.jpeg";
		
		try {
			log.info("verify Add To Cart Alert In HomePage method started here");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkprodInHomePage));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.addToCartAlert)) {
				objStatus+=true;
				System.out.println("Add to cart alert is displayed while adding product in home page");
				String objDetail="Add to cart alert is displayed while adding product in home page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding product in home page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("verify Add To Cart Alert In HomePage method ended here");
			if(objStatus.contains("false")) {
				System.err.println("verify Add To Cart Alert In HomePage failed ");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In HomePage executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInListPage
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInListPage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInListPage.jpeg";
		
		try {
			log.info("verify Add To Cart Alert In List Page started here");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSangviovese));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.addToCartAlert)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed while adding product in List page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding increased product in List page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			UIFoundation.waitFor(2L);
			System.out.println("Adding product from quantity box");
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.dwnFirstProdQuantitySelect, "quantity"));
			String alertQtybox = UIFoundation.getText(ListPage.txtselQty);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(4L);
			String alertQty = UIFoundation.getText(ListPage.selQtyInALert);
			char altertext = alertQty.charAt(0);
			String alteredtext = Character.toString(altertext);
			if(alertQtybox.equals(alteredtext)) {
				objStatus+=true;
				//System.out.println("Add to cart alert is displayed with correct quantity");
				String objDetail="Add to cart alert is displayed with correct quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is displayed with Incorrect quantity";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("verify Add To Cart Alert In list method ended here");

			if(objStatus.contains("false")) {
				System.out.println("verify Add To Cart Alert In List page failed");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In List page executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInPiptPage
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInPiptPage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInPiptPage.jpeg";
		
		try {

			log.info("verify Add To Cart Alert In Pip Page started here");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkgiftProdutcAddToPIP));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.addToCartAlert)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed while adding product in pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding  product in pip page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			UIFoundation.waitFor(2L);
			System.out.println("Increasing count from quantity box");
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.dwnFirstProdQuantitySelect, "quantity"));
			String alertQtybox = UIFoundation.getText(ListPage.txtselQty);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(4L);
			String alertQty = UIFoundation.getText(ListPage.selQtyInALert);
			char altertext = alertQty.charAt(0);
			String alteredtext = Character.toString(altertext);
			if(alertQtybox.equals(alteredtext)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed with correct quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is displayed with Incorrect quantity";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("verify Add To Cart Alert In Pip method ended here");
			if(objStatus.contains("false")) {
				System.out.println("verify Add To Cart Alert In Pip page failed");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In Pip page executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInPiptPage
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInMyWinePage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInPiptPage.jpeg";
		
		try {

			log.info("verify Add To Cart Alert In MyWine Page started here");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.addToCartAlert)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed while adding product in MyWine page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding  product in MyWine page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			UIFoundation.waitFor(2L);
			System.out.println("Increasing count from quantity box");
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.dwnFirstProdQuantitySelect, "quantity"));
			String alertQtybox = UIFoundation.getText(ListPage.txtselQty);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			UIFoundation.waitFor(4L);
			String alertQty = UIFoundation.getText(ListPage.selQtyInALert);
			char altertext = alertQty.charAt(0);
			String alteredtext = Character.toString(altertext);
			if(alertQtybox.equals(alteredtext)) {
				objStatus+=true;
				//System.out.println("Add to cart alert is displayed with correct quantity");
				String objDetail="Add to cart alert is displayed with correct quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is displayed with Incorrect quantity";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("verify Add To Cart Alert In MyWine method ended here");
			if(objStatus.contains("false")) {
				System.out.println("verify Add To Cart Alert In MyWine page failed");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In MyWine page executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
}
