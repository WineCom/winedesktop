package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class ModalLightBoxWithXIcon extends Desktop{
	
	/***************************************************************************
	 * Method Name			: modalLightBoxWithXIcon()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String modalLightBoxWithXIcon() {
		String objStatus=null;
	
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "lightBoxProduct"));
			UIFoundation.waitFor(8L);
			UIFoundation.hitEnter(ListPage.txtsearchProduct);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkmerlotFirstProd));
			UIFoundation.waitFor(8L);
			
			if(UIFoundation.isDisplayed(ListPage.lnkAllVintages)){
				 objStatus+=true;
			      String objDetail="'All vintages' Link is displayed in PIP";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				 objStatus+=false;
				 String objDetail="'All vintages' Link is not displayed in PIP";
			       UIFoundation.captureScreenShot(screenshotpath+"RecPaddrs", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"RecPaddrs")).build());
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgcarouselContentItem);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgcarouselContentItem));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.lnklightBoxCloseIcon)){
				UIFoundation.waitFor(3L);
				UIFoundation.escapeBtn(driver);
				UIFoundation.waitFor(2L);
				if(!UIFoundation.isDisplayed(ListPage.lnklightBoxCloseIcon)){
					 objStatus+=true;
				      String objDetail="Modal/Light Box closes on clicking 'ESC' key when the Modal/Light box has ' X' icon";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					
				}else{
					 objStatus+=false;
					   String objDetail="Modal/Light Box is not closed on clicking 'ESC' key when the Modal/Light box has ' X' icon";
				       UIFoundation.captureScreenShot(screenshotpath+"RecPaddrs", objDetail);
				       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"RecPaddrs")).build());
				}
			}else{
				objStatus+=false;
				   String objDetail="Light Box close icon is not displayed";
			       UIFoundation.captureScreenShot(screenshotpath+"RecPaddrs", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"RecPaddrs")).build());
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnpipName);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
		//	System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name			: lightBoxInRecipientPage()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String lightBoxInRecipientPage() {
		String objStatus=null;
		 			
		try {
			log.info("The execution of the method lightBoxInRecipientPage started here ...");
			UIFoundation.waitFor(3L);
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		//	driver.navigate().refresh();
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdt));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtshippingAddressFullName);
			UIFoundation.waitFor(2L);
			UIFoundation.clckObject(FinalReviewPage.lnkshippingOptionalClk);

			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtshippingAddressFullNameEnter, "firstName"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnmodalWindowCancel)){
				UIFoundation.waitFor(3L);
				UIFoundation.escapeBtn(driver);
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkshippingAddressEdt)){
					 objStatus+=true;
				      String objDetail="Modal/Light Box closes on clicking 'ESC' key for the Modal/Light box with  ' X' icon";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else{
					 objStatus+=false;
					   String objDetail="Modal/Light Box does not closed on clicking 'ESC' key for the Modal/Light box with  ' X' icon";
				       UIFoundation.captureScreenShot(screenshotpath+"modalIcon", objDetail);
				       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"RecPaddrs")).build());
				}
			}else{
				   objStatus+=false;
				   String objDetail="Light Box close icon is not displayed";
			       UIFoundation.captureScreenShot(screenshotpath+"RecPaddrs", objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"RecPaddrs")).build());
			}

			log.info("The execution of the method lightBoxInRecipientPage ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
		
			log.error("there is an exception arised during the execution of the method lightBoxInRecipientPage "+ e);
			return "Fail";
		}

	}

}
