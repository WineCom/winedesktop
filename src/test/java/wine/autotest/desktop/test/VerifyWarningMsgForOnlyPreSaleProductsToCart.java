package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class VerifyWarningMsgForOnlyPreSaleProductsToCart extends Desktop {

	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleProdTocrt() {
		String objStatus = null;

		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "preSale"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPreSaleProduct));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			UIBusinessFlows.validationForOnlyPreSaleProducts();
		//	ApplicationDependent.verifyClockIconPresent(driver);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
