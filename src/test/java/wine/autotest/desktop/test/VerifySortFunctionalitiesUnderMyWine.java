package wine.autotest.desktop.test;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifySortFunctionalitiesUnderMyWine extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addMyWineProdTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addMyWineProdTocrt() {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnActivity));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkmyWineSelectSort));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkratingDateOldToNew));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIBusinessFlows.validationForMyWineSortingRatingDateOldToNew());
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkratingDateNewToOld));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIBusinessFlows.validationForMyWineSortingRatingDateNewToOld());
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
