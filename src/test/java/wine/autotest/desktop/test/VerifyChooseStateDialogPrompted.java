package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class VerifyChooseStateDialogPrompted extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		boolean isElementPresent=false;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clckObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFirstProductToCart);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkSecondProductToCart);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSecondProductToCart));
			}

			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkThirdProductToCart);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFourthProductToCart);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFifthProductToCart);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));
			}
			UIFoundation.waitFor(3L);
			boolean elementPresent=UIFoundation.isDisplayed(ListPage.spnChooseYourState);
			if (objStatus.contains("false") && (elementPresent==true) ) {
				System.out.println("Verify 'Choose state' dailog is prompted test case is failed");
				logger.fail("Verify 'Choose state' dailog is prompted test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify 'Choose state' dailog is prompted test case is executed successfully");
				logger.pass("Verify 'Choose state' dailog is prompted test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	

}
