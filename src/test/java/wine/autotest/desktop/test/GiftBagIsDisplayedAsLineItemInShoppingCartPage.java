package wine.autotest.desktop.test;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

import wine.autotest.desktop.library.UIBusinessFlows;

public class GiftBagIsDisplayedAsLineItemInShoppingCartPage extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "giftBagUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : addGiftBag() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String addGiftBag() {
		String objStatus = null;
		   String screenshotName = "Scenarios_giftBagIcon_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method addGiftBag started here ...");
			
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);  
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			UIFoundation.waitFor(3L);
			UIBusinessFlows.isGiftCheckboxSelected(FinalReviewPage.chkRecipientGift);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
//			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
	//		objStatus += String.valueOf(UIFoundation.javaSriptClick(driver, "RecipientContinue"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)){
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnViewCart);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnCartEdit));
			}
			int prodcutIcon=UIFoundation.listSize(FinalReviewPage.lnkprodItemgiftIcon);
			String strgiftBagInCart[]={"firstProdItemgiftIcon","secondProdItemgiftIcon","thirdProdItemgiftIcon","fourthProdItemgiftIcon","fifthProdItemgiftIcon","sixthProdItemgiftIcon","seventhProdItemgiftIcon","eightProdItemgiftIcon"};
			for(int i=0;i<prodcutIcon;i++){
				
				if(UIFoundation.isStrDisplayed(strgiftBagInCart[i])){
					
					  objStatus+=true;
				      String objDetail="Gift Bag is displayed for "+strgiftBagInCart[i];
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      logger.pass(objDetail);
				}else{
					objStatus+=false;
					String objDetail="Gift Bag is not displayed "+strgiftBagInCart[i];
					//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
			}
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkgiftWrapping);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnContinueShopping));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			for (int i = 0; i < prodcutIcon; i++) {

				if (UIFoundation.isStrDisplayed(strgiftBagInCart[i])) {

					objStatus += true;
					String objDetail = "Gift Bag is displayed for " + strgiftBagInCart[i];
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					logger.pass(objDetail);
				} else {
					objStatus += false;
					String objDetail = "Gift Bag is not displayed " + strgiftBagInCart[i];
					// ReportUtil.addTestStepsDetails("Order number is
					// null.Order not placed successfully", "", "");
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				}
			}
			log.info("The execution of the method addGiftBag ended here ...");
			String cartCount = UIFoundation.getText(CartPage.txtcartBtn);
			int count =Integer.parseInt(cartCount);	
			if(count>=9) {
			for(int i=0;i<=count;i++) {
			UIFoundation.clckObject(CartPage.lnkRemoveFirstProduct);
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(CartPage.btnRemove);
			UIFoundation.waitFor(1L);
			}
			}	
			if (objStatus.contains("false")) {
				System.out.println("Verify the gift bag is displayed as line item in shopping  cart page test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the gift bag is displayed as line item in shopping  cart page test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method addGiftBag "
					+ e);
			return "Fail";
		}
	}
	

}
