package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
/*import com.gargoylesoftware.htmlunit.html.applets.AppletClassLoader;*/
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class CollapsedDeliverySectionWhenThePreSaleItemIsAdded extends Desktop {
	
	/***************************************************************************
	 * Method Name			: addPreSaleprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addPreSaleprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSangviovese));
		//	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnBordexBlends"));		
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnFirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
//			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
//			if (addToCart2.contains("Add to Cart")) {
//				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnSecondProductToCart");
//				UIFoundation.waitFor(1L);
//			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
//			}
//			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
//			if (addToCart3.contains("Add to Cart")) {
//				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnThirdProductToCart");
//				UIFoundation.waitFor(1L);
//				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
//			}
//			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
//			if (addToCart4.contains("Add to Cart")) {
//				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnFourthProductToCart");
//				UIFoundation.waitFor(1L);
//				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
//			}
//
//			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
//			if (addToCart5.contains("Add to Cart")) {
//				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnFifthProductToCart");
//				UIFoundation.waitFor(1L);
//				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
//			}
//			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
//			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkFeatured));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexFeatures));
			UIFoundation.waitFor(4L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnFirstProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnSecondProductToCart");
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnThirdProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.spnFourthProductToCart");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: addPreSaleprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String verifyDeliverySection() {
		   String screenshotName = "Scenarios_DeliverySection_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
			String objStatus = null;
		try {
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnSuggestedAddress"));*/
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spnestimatedArrival))
			{
				  objStatus+=true;
			      String objDetail="estimated arrival dates is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			     
			}else{
				objStatus+=false;
				String objDetail="estimated arrival dates is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spndeliveryHeaderDate))
			{
				  objStatus+=true;
			      String objDetail="Delivery dates is  displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="Delivery dates is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"final", objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spnallPreSaleItemsDetails))
			{
				  objStatus+=true;
			      String objDetail="Your order contains a pre-sale item, including shipping & handling and   applicable taxes, will be billed when the order is placed is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			     
			}else{
				objStatus+=false;
				String objDetail="Your order contains a pre-sale item, including shipping & handling and   applicable taxes, will be billed when the order is placed is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"final", objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spncancellingThePreSale))
			{
				  objStatus+=true;
			      String objDetail="canceling this Pre-sale order is subject to a 20% cancellation fee, with the remaining balance applied as a credit in your wine.com  account is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			        
			}else{
				objStatus+=false;
				String objDetail="canceling this Pre-sale order is subject to a 20% cancellation fee, with the remaining balance applied as a credit in your wine.com  account is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"final", objDetail);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
