package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class PreferredAdress extends Desktop {
	
	
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String preferredAdressFunctionality() {
	

	String objStatus=null;
	   String screenshotName = "Scenarios_preferredAdress_Screenshot.jpeg";
	
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtPymtCVV))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPymtCVV, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPymtSave));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));

			if(UIFoundation.isDisplayed(FinalReviewPage.spnPreferredAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified the address that is preferred has text";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified the address that is preferred has text");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the address that is preferred has text test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify the address that is preferred has text");
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnMakePreferred));
			WebElement ele=driver.findElement(By.xpath("(//span[@class='shippingSelectedAddress_group'])[2]/input"));
			String shippingAddressExpectedName=UIFoundation.getText(FinalReviewPage.spnshippingAddressExpectedName);
 			if(!ele.isSelected()){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Clickable", "", 50);

			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnshippingSelectedAddress));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			}
	/*		if(UIFoundation.isDisplayed(driver, "mustSelectShippingAddress"))
			{
				  objStatus+=true;
			      String objDetail="Must select address before proceeding' error message IS displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Must select address before proceeding' error message IS NOT  displayed test case is failed";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "shippingSelectedAddress"));
			String shippingAddressExpectedName=UIFoundation.getText(driver, "shippingAddressExpectedName");
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));*/
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele1=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele1.isSelected())
				{
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
/*
			if(UIFoundation.isDisplayed(driver, "PaymentCVV"))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "PaymentCVV", "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "paywithThisCardSave"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PlaceOrderButton"));
				UIFoundation.waitFor(30L);
			}*/
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			String shippingAddressActualName=UIFoundation.getText(FinalReviewPage.spnShippingAddressActualName);
			if(shippingAddressExpectedName.equalsIgnoreCase(shippingAddressActualName))
			{
				  objStatus+=true;
			      String objDetail="Verified the functionality of 'Make preferred' text button available in the Address cards";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
				  System.out.println("Verified the functionality of 'Make preferred' text button available in the Address cards");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the functionality of 'Make preferred' text button available in the Address cards test case is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail);
				   System.err.println("Verify the functionality of 'Make preferred' text button available in the Address cards");
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				//System.out.println("Verify the address that is preferred has text test case is failed");
				return "Fail";
			} else {
				//System.out.println("Verify the address that is preferred has text test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method preferredAdressFunctionality "
					+ e);
			return "Fail";
		}
	}
	

}
