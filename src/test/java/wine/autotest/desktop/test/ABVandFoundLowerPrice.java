package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class ABVandFoundLowerPrice extends Desktop {
	
	/***************************************************************************
	 * Method Name			: aBVandFoundLowerPrice()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String aBVandFoundLowerPrice()
	{
		String objStatus=null;
			
		try
		{
			log.info("The execution of the method aBVandFoundLowerPrice started here ...");
			UIFoundation.waitFor(2L);
			driver.get("https://qwww.wine.com/product/columbia-crest-grand-estates-red-blend-2012/152746");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spnabvText))
			{
				objStatus+=true;
				String objDetail="'ABV' text is displayed beside the product icons in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="'ABV' text is not displayed beside the product icons in PIP";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"abv", objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.spnfoundLowerPrice))
			{
				objStatus+=true;
				String objDetail="'Found a lower price' text link is displayed beneath the price section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			
				logger.pass(objDetail);
			}else{
				objStatus+=false;
				String objDetail="'Found a lower price' text link is not displayed beneath the price section";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+"lowerPrice", objDetail);
			}
			UIFoundation.waitFor(2L);
			log.info("The execution of the method aBVandFoundLowerPrice ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method aBVandFoundLowerPrice "+ e);
			return "Fail";
			
		}
	}	

}
