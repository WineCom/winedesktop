package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.fw.utilities.UIFoundation;


public class UserCreationWithCheckoutProcess extends Desktop{
	
	
	static String expectedState=null;
	static String actualState=null;
	
	/***************************************************************************
	 * Method Name : userProfileCreation() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to Create
	 * new user account
	 ****************************************************************************
	 */

	public static String userProfileCreation() {
		String objStatus = null;
		try {
			log.info("The execution of method create Account started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "accPassword"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnCreateAccount, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement( LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			objStatus+= String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}

			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Deskusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[1]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false") && !(expectedState.equalsIgnoreCase(actualState)))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	




}
