package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.UIFoundation;

public class RatingStarsDisplayedProperlyInMyWine extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "ratingnew"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: RatingStarsDisplayedProperlyInMyWine()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String ratingStarsDisplayedProperlyInMyWine()
	{
		String objStatus=null;				
		try
		{			
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine started here ...");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imguserRatings));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.txtAreaCount));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btncancelInMyWine));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnsaveInMyWine));
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is failed");
				String objDetail = "Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is failed";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is executed successfully");
				logger.pass("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is executed successfully");
				return "Pass";
			}			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "+ e);
			return "Fail";
			
		}
	}

}
