package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifySaveCreditCardInfoCheckboxUnchecedFunctionality extends Desktop {
	
	
	/***************************************************************************
	 * Method Name			: verifySaveCrdeitCardInfo()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifySaveCrdeitCardInfo() {

	String objStatus=null;
	   String screenshotName = "Scenarios__CreditcountScreenshot.jpeg";
		
		try {
			log.info("The execution of the method verifySaveCrdeitCardInfo started here ...");
			UIFoundation.waitFor(4L);
		
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		//	driver.navigate().refresh();
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
			String creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentCount);
			int creditCardCntBefore=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(2L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				UIFoundation.waitFor(3L);
			}

			 
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.chkBillingAndShipping)){
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkCreditCard));
			if(!UIFoundation.isDisplayed(FinalReviewPage.rdomakeThisPreferredCard))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("verified the Make this card preffered check box is hidden when user unchecked Save card information", "Pass", "");
				System.out.println("verified the Make this card preffered check box is hidden when user unchecked Save card information");
			}else{
				objStatus+=false;
				String objDetail="Make this card preffered check box is not hidden when user unchecked Save card information";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+"MakePreferredCheckBox", objDetail);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
            {
                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
                  UIFoundation.waitFor(5L);
                  
            }
			AddProdcutsToCartCaptureOrder.addprodTocrt();
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		//	driver.navigate().refresh();
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			UIFoundation.waitFor(3L);
/*			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);*/
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
			creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentCount);
			int creditCardCountAfter=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if((creditCardCntBefore==creditCardCountAfter))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verify the sub menu elements under varietal filters test case is executed successfully.", "Pass", "");
				System.out.println("Verify the sub menu elements under varietal filters test case is executed successfully");
		
			}else
			{
				objStatus+=false;
				String objDetail="Verify the 'Save credit card info for next time' check box functionality, if checked test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail);
				
			}
			log.info("The execution of the method verifySaveCrdeitCardInfo ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if unchecked test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if unchecked  test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifySaveCrdeitCardInfo "
					+ e);
			return "Fail";
		}
	}

}
