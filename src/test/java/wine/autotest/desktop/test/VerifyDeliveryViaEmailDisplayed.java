package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.MediaEntityBuilder;



import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;

import wine.autotest.desktop.pages.ThankYouPage;


public class VerifyDeliveryViaEmailDisplayed extends Desktop {
	/***************************************************************************
	 * Method Name			: addGiftsToTheCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String addGiftsToTheCart( )
	{
		String objStatus=null;
		String addToCart4 = null;
		String addToCart5 = null;
		try
		{
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftCards));
			UIFoundation.waitFor(3L);
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFourthProductToCart);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFifthProductToCart);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFifthProductToCart));
			}
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to fill the shipping address of the customer
	 * 
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String screenshotName = "deliveryViaEmail.jpeg";
			try {
			UIFoundation.waitFor(2L);
			log.info("The execution of the method shippingDetails started here ...");
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutAlter));
			}
				
			UIFoundation.waitFor(12L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
			if(UIFoundation.isDisplayed(FinalReviewPage.spnDeliveryViaEmail))
			{				
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
				objStatus+=true;
				String objDetail="'Delivered via Email' is displayed in delivery section";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="'Delivered via Email' is not displayed in delivery section";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name : addNewCreditCard()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			expected = verifyexpectedresult.placeOrderConfirmation;
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);	
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
				
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(15L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(20L);			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
			{			
			UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);			
			}
			UIFoundation.waitFor(1L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(1L);						
			if(UIFoundation.isDisplayed(ThankYouPage.txtDeliveryTimeLine))
			{	
				objStatus+=true;
				String objDetail = "Delivered via email' is displayed in order history";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		    			
			}else
			{
				objStatus+=false;
		    	String objDetail="Delivered via email' is not displayed in order history";
		    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    	UIFoundation.captureScreenShot(screenshotpath, objDetail);
			
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				System.out.println("Order creation with Gifts for new user  test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with Gifts for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
	

}
