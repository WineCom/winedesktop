package wine.autotest.desktop.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifySaveCreditCardInfoCheckboxCheckedFunctionality extends Desktop {
	
		
	/***************************************************************************
	 * Method Name			: verifySaveCrdeitCardInfo()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifySaveCrdeitCardInfo() {

	String objStatus=null;
	   String screenshotName = "Scenarios__CreditcountScreenshot.jpeg";
				

		try {
			log.info("The execution of the method verifySaveCrdeitCardInfo started here ...");
			UIFoundation.waitFor(4L);
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
					if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(20L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
			String creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentCount);
			int creditCardCntBefore=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
			UIFoundation.waitFor(3L);
			 
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
			UIFoundation.waitFor(2L);
			WebElement ele=driver.findElement(By.xpath("//input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			UIFoundation.waitFor(10L);
			 if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);
	                  
	            }
	
			UIFoundation.waitFor(10L);
			AddProdcutsToCartCaptureOrder.addprodTocrt();
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
		    UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			UIFoundation.waitFor(6L); 
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());

			UIFoundation.waitFor(0);
			driver.navigate().refresh();
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
			creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentCount);
			int creditCardCountAfter=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if((creditCardCntBefore+1==creditCardCountAfter))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verify the sub menu elements under varietal filters test case is executed successfully.", "Pass", "");
				logger.pass("Verify the sub menu elements under varietal filters test case is executed successfully");
				System.out.println("Verify the sub menu elements under varietal filters test case is executed successfully");
		
			}else
			{
				objStatus+=false;
				String objDetail="Verify the 'Save credit card info for next time' check box functionality, if checked test case is failed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method verifySaveCrdeitCardInfo ended here ...");
			if (objStatus.contains("false") ) {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if checked test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if checked  test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifySaveCrdeitCardInfo "
					+ e);
			return "Fail";
		}
	}

}
