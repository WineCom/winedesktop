package wine.autotest.desktop.test;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;

public class VerifyOutOFStockCheckBoxChecked extends Desktop {
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		   String screenshotName = "Scenarios__CheckBoxScreenshot.jpeg";
				
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
/*			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "SearchBarFormInput"));
			UIFoundation.waitFor(1L);*/
			driver.navigate().to("https://qwww.wine.com/search/Jackaroo/0");
			UIFoundation.waitFor(3L);
			/*objStatus+=String.valueOf(UIFoundation.clickObject(driver, "TxtsearchProduct"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "TxtsearchProduct", "OOSProduct"));
			UIFoundation.waitFor(3L);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);*/
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "Jackarook"));
			
			boolean status=UIFoundation.isCheckBoxSelected(ListPage.chkOutOfStock);
			if(status)
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verify out of stock chekbox is Checked by default", "Pass", "");
				System.out.println("Verify out of stock chekbox is Checked test case executed successfully");
			}else
			{
				objStatus +=false;
				String objDetail="Out of Checkbox not selected by defaultt";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
				System.err.println("Out of Checkbox not selected by default");
			}
		//	System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
		//	UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(UIFoundation.clickObject(driver, "obj_AddToCart"));
		//	System.out.println(UIFoundation.getText(driver,"AddAgain"));
		//	objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CartBtnCount"));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}

}
