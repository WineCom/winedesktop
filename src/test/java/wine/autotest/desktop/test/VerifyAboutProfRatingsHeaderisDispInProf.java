package wine.autotest.desktop.test;

import java.io.IOException;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyAboutProfRatingsHeaderisDispInProf extends Desktop {

	/***************************************************************************
	 * Method Name : VerifyAboutProfRatingsHeaderisDispInProf Reviewed By :
	 * 
	 * @throws IOException
	 *             TM-4055
	 ****************************************************************************
	 */

	public static String verifyprofessionalRating() {

		String objStatus = null;
		String screenshotName = "VerifyprofessionalRating.jpeg";
		
		try {
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));
			UIFoundation.waitFor(4L);
			if (UIFoundation.isElementDisplayed(ListPage.lnkprofessionalRatingR)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkprofessionalRatingR));
			}
			UIFoundation.waitFor(2L);
			if (UIFoundation.isElementDisplayed(ListPage.spnprofessionalRatingHeader)) {
				String professionalRatingText = UIFoundation.getText(ListPage.spnprofessionalRatingHeader);
				System.out.println("Text displayed in ProfessionalRating header is: " + professionalRatingText);
				objStatus += true;
				String objDetail = "Proffesional header is displayed in list page:";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Proffesional header is not displayed in list page:";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());

			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.winmodalWindowClose));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			if (UIFoundation.isElementDisplayed(ListPage.lnkprofessionalRatingR)) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkprofessionalRatingR));
			}
			UIFoundation.waitFor(2L);
			if (UIFoundation.isElementDisplayed(ListPage.spnprofessionalRatingHeader)) {
				String professionalRatingText2 = UIFoundation.getText(ListPage.spnprofessionalRatingHeader);
				System.out.println("Text displayed in ProfessionalRating header: " + professionalRatingText2);
				objStatus += true;
				String objDetail = "Proffesional header is displayed in Pip page:";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Proffesional header is not displayed in Pip page:";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");

			}

			if (objStatus.contains("false")) {
				System.out.println(
						"Verify the 'About Professional ratings' header is displayed in the professional ratings modal test case is failed");
				return "Fail";
			} else {
				System.out.println(
						"Verify the 'About Professional ratings' header is displayed in the professional ratings modal test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
