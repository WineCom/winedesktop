package wine.autotest.desktop.test;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.pages.LoginPage;


public class BaseTest extends Desktop{

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Logi n into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	 
	public static String login()
	{
		String objStatus=null;
	try
		{
			log.info("The execution of the method login started here ...");

			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(3L);
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 20);
//			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
			}
			objStatus+=String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				}
			
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}	
	/***************************************************************************
	 * Method Name			: logout()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Logout from the Wine.com 
	 * 						  application
	 ****************************************************************************
	 */

	public static String logout()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method logout started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(5l);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSignout));
			log.info("The execution of the method logout ended here ...");	
			if (objStatus.contains("false"))
			{
				System.out.println("Logout test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Logout test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method logout "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: userProfileCreation()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String userProfileCreation() {
		String objStatus=null;
		try {
			log.info("The execution of method create Account started here");
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtPassword, "accPassword"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAccount));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnCreateAccount, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement( LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			objStatus+= String.valueOf(UIBusinessFlows.isObjectExistForSignIn());
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
//				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account "
					+ e);
			return "Fail";
		}
	}
	
}
