 package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;


import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.UserProfilePage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;


public class AddPaymentMethodUsingUserProfileServicesForNewUser extends Desktop{
	
	/***************************************************************************
	 * Method Name			: addPaymentMethod()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 ****************************************************************************
	 */

	public static String addPaymentMethod()
	{
		String objStatus=null;
		 String screenshotName = "Scenarios__PaymentMethod.jpeg";
		try
		{
			log.info("The execution of the method addAddress started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clckObject(UserProfilePage.lnkPaymentMethods));
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(UserProfilePage.spnPaymentMethodsHeader)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("User is navigated to Credit card info ", "Pass", "");
				logger.pass("Verify user is navigated to Credit card info test case is executed successfully");
				System.out.println("Verify user is navigated to Credit card info test case is executed successfully");

			}else{
				objStatus+=false;
				String objDetail="Verify user is navigated to Credit card info  test case is failed";
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(UserProfilePage.lnkAddNewCard));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.eventFiringWebDriver(UserProfilePage.txtExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.eventFiringWebDriver(UserProfilePage.txtExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtCVV, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingCty, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(UserProfilePage.dwnBillingSelState, "dryState"));
			UIFoundation.clickObject(UserProfilePage.dwnBillingSelState);
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(UserProfilePage.txtBillingPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(UserProfilePage.btnBillingSave);
			objStatus += String.valueOf(UIFoundation.clickObject(UserProfilePage.btnBillingSave));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") && (!UIFoundation.isDisplayed(UserProfilePage.spnPaymentMethodsHeader)))
			{
				System.out.println("Adding payment method using user profile services for new user test case is failed");
				logger.fail("Adding payment method using user profile services for new user test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Adding payment method using user profile services for new user test case is executed successfully");
				logger.pass("Adding payment method using user profile services for new user test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addPaymentMethod "+ e);
			return "Fail";
		}
	}

}
