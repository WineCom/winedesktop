package wine.autotest.desktop.test;


import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class UserSelectsToPayWithDifferentCard extends Desktop{
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "preferredCardUN"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
//			UIFoundation.webDriverWaitForElement(LoginPage.btnSignIn, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: userSelectsToPayWithDifferentCard()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String userSelectsToPayWithDifferentCard() {
	String objStatus=null;

	   String screenshotName = "Scenarios_diffPayCard_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method userSelectsToPayWithDifferentCard started here ...");
			UIFoundation.waitFor(4L);
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if(!UIFoundation.isSelected(FinalReviewPage.chkpreferredCardSelect)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkpreferredCardSelect));
				UIFoundation.waitFor(1L);
				
			}
			String nameBeforeSelPayWithCard=UIFoundation.getText(FinalReviewPage.creditCardHolderNameBeofreSelPay);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.selectPayWithThisCard));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.spneditCardModal)) {
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtpaywithThisCardCvv, "CardCvid"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnpaywithThisCardSave);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(1L);
			String nameAfterSelPayWithCard=UIFoundation.getText(FinalReviewPage.spnnameAfterSelPayWithCard);
			if(!UIFoundation.isSelected(FinalReviewPage.chkpreferredCardSelect) && (UIFoundation.isSelected(FinalReviewPage.chkpayWithThisCardSel) &&(nameBeforeSelPayWithCard.equalsIgnoreCase(nameAfterSelPayWithCard))) ){
				objStatus+=true;
				String objDetail="verify the preferred selection is not changed when user selects to pay with different card test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);				
			}else{
				objStatus+=false;
				String objDetail="Verify the preferred selection is not changed when user selects to pay with different card test case is failed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method userSelectsToPayWithDifferentCard ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the preferred selection is not changed when user selects to pay with different card test case is failed");
				return "Fail";
			} else {
				System.out.println("verify the preferred selection is not changed when user selects to pay with different card test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method userSelectsToPayWithDifferentCard "
					+ e);
			objStatus+=false;
			String objDetail="Verify the preferred selection is not changed when user selects to pay with different card test case is failed";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
