package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;

public class VerifyStewardshipMember extends Desktop {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-622
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	/***************************************************************************
	 * Method Name			: verifyStewardshipMessageNotDisplayed()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-622
	 ****************************************************************************
	 */
	
	
	public static String verifyStewardshipMessageNotDisplayed() {
		String screenshotName = "Scenarios_verifyStewardshipMessageNotDisplayed_Screenshot.jpeg";
		String objStatus=null;
		try {
			log.info("The execution of the method verifyStewardshipMessageNotDisplayed started here ...");
			
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.labstewardShipSection)) {
				objStatus+=true;
				String objDetail = "Stewardship message is not dispalyed";	
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			}else {
				objStatus+=false;
				String objDetail = "Stewardship message is dispalyed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
								
			}
			log.info("The execution of the method verifyStewardshipMessageNotDisplayed ended here ...");
			if(objStatus.contains("false"))
			{
				String objDetail = "Verify Stewardship message dispalyed test case is failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{
				String objDetail = "Verify Stewardship message not dispalyed test case is executed successfully";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
			
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyStewardshipMonthlyWineClub "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: VerifyDefaultstewardshipStdShipping()
	 * Created By			: Ramesh S
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-3223
	 ****************************************************************************
	 */
	
	public static String VerifyDefaultstewardshipStdShipping() {
	String objStatus=null;
	String Shipping=null;
	String screenshotName = "Scenarios_VerifyDefaultstewardshipStdShipping_Screenshot.jpeg";
	try {
			log.info("The execution of the method VerifyDefaultstewardshipStdShipping started here ...");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeDate))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
			}
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			if(Shipping.contains("0.00")) {
				objStatus+=true;
				String objDetail = "The Standard Shipping is $0";
				System.out.println(objDetail);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else {
				objStatus+=false;
				String objDetail = "The Standard Shipping is NOT $0";
				System.out.println(objDetail);
				logger.pass(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				String objDetail = "VerifyDefaultstewardshipStdShippingd in delivery section test case is failed";
				System.out.println(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			} else {
				String objDetail = "VerifyDefaultstewardshipStdShipping in delivery section test case is executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method VerifyDefaultstewardshipStdShipping "
					+ e);
			return "Fail";
		}
	}
}
