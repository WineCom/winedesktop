package wine.autotest.desktop.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.desktop.pages.FinalReviewPage;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;

public class GiftReceipientEmailValidation extends Desktop {
	
	static String recEmail=null;
	static int orderNumber=0;
	/***************************************************************************
	 * Method Name : editRecipientPlaceORder() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String editRecipientPlaceORder() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
			

		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
//			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientShipToaddress));
			UIBusinessFlows.isGiftCheckboxSelected(FinalReviewPage.chkRecipientGift);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag3_99));
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));
			objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(0);
			driver.navigate().refresh();
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
//				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);;
			}
				
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))	
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
			}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					UIFoundation.clearField(FinalReviewPage.txtCVV);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
					if(UIFoundation.isDisplayed(FinalReviewPage.chkBillingAndShipping))
					{
						WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
						if(!ele.isSelected())
						{
						UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
					    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
					    UIFoundation.waitFor(3L);
					    UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
						UIFoundation.waitFor(1L);
						objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
//						UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
						}
					}
					
				}
				
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
//			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			  if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);
	                  
	            }               
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{ objStatus+=true;
		      String objDetail="Order number is placed successfully";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      logger.pass(objDetail);
		      System.out.println("Order number is placed successfully: "+orderNum);   
			}else
			{
				objStatus+=false;
				String objDetail="Order number is null.Order not placed successfully";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail);
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
            {
                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
                  UIFoundation.waitFor(5L);
                  
            }     
			driver.navigate().refresh();
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnContinueShopping));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name : verifyGiftRecipientEmail() 
	 * Created By : Vishwanath
	 * Chavan Reviewed By : Ramesh,KB 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String verifyGiftRecipientEmail() {
		String objStatus = null;
		String screenshotName = "Scenarios_EmailIdInPrev_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method verifyGiftRecipientEmail started here ...");
			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(10L);  
			/*objStatus+=String.valueOf(UIFoundation.clickObject(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
				UIFoundation.waitFor(3L);	
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientShipToaddress));
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtReceipientEmail))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkRecipientGift));
				UIFoundation.waitFor(3L);
			}
			
			recEmail=UIFoundation.getAttribute(FinalReviewPage.txtReceipientEmail);
			System.out.println(recEmail);
			if(!recEmail.isEmpty())
			{
				  objStatus+=true;
			      String objDetail="email id used in previous order is not displayed by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			      System.out.println("email id used in previous order is not displayed by default");
			}
			else
			{
				   objStatus+=false;
			       String objDetail="email id used in previous order is displayed by default";
			       logger.fail(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyGiftRecipientEmail ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftRecipientEmail "
					+ e);
			return "Fail";
		}
	}
	
	
}
