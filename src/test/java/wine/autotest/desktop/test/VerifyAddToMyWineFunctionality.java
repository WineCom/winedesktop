package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyAddToMyWineFunctionality extends Desktop {
	
	
	/***************************************************************************
	 * Method Name : VerifyAddToMyWineFunctionality(NFP-4253) Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose :
	 * 
	 ****************************************************************************
	 */

	public static String verifyAddToMyWineFunctionality()

	{
		String objStatus = null;
		String screenshotName = "VerifyAddToMyWineFunctionality.jpeg";
		try {
			log.info("The execution of the method VerifyAddToMyWineFunctionality started here ...");
			String actualMyWine = verifyexpectedresult.expectedWineText;
			String actualWinecolor = verifyexpectedresult.expWineColor;
			//String actProdName = verifyexpectedresult.prodName");
			driver.get("https://qwww.wine.com/product/chateau-terrasson-cuvee-prevenche-2016/615274");
			UIFoundation.waitFor(1L);
			if (UIFoundation.isElementDisplayed(ListPage.lnkAddToMyWineFunc)) {
				objStatus += true;
				String objDetail = "AddToMyWine is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "AddToMyWine is not present in product pip page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			String mywinetextinpip = UIFoundation.getText(ListPage.lnkAddToMyWineFunc);
			String actualcolorofmywine = UIFoundation.getColor(ListPage.imgmywinecolor);
//			if (mywinetextinpip.equals(actualMyWine) && (actualcolorofmywine.equalsIgnoreCase(actualWinecolor))) {
			if ((actualcolorofmywine.equalsIgnoreCase(actualWinecolor))) {
				objStatus += true;
				String objDetail = "Product is added to My Wine and highligted to purple color";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Product is not added";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			String prodadded = UIFoundation.getText(ListPage.spnpipName);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
	        String	myWineAddedProd =	UIFoundation.getText(ListPage.lnkfirstListProd);
			if (prodadded.contains(myWineAddedProd)) {
				objStatus += true;
				String objDetail = "Product added in pip is displayed in My wine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Product added in pip is not displayed in My wine ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			log.info("The execution of the method VerifyAddToMyWineFunctionality ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify Add To My Wine Functionality test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Add To My Wine Functionality test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);

			return "Fail";

		}
	}
	
	/***************************************************************************
	 * Method Name : verifyAddToMyWineFunctionalityinPIP(NFP-4253) Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose :
	 * 
	 ****************************************************************************
	 */

	public static String verifyAddToMyWineFunctionalityinPIP()

	{
		String objStatus = null;
		String screenshotName = "VerifyAddToMyWineFunctionality.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method verifyAddToMyWineFunctionalityinPIP started here ...");
			String actualMyWine = verifyexpectedresult.expectedWineText;
			String actualWinecolor = verifyexpectedresult.expWineColor;
			driver.get("https://qwww.wine.com/product/hess-allomi-cabernet-sauvignon-2017/525938");
			UIFoundation.waitFor(5L);
			if (UIFoundation.isElementDisplayed(ListPage.lnkAddToMyWineFunc)) {
				objStatus += true;
//				objStatus += String.valueOf(UIFoundation.clickObject(driver, "modalWindowClose"));
				String objDetail = "AddToMyWine is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "AddToMyWine is not present in product pip page.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			String mywinetextinpip = UIFoundation.getText(ListPage.lnkAddToMyWineFunc);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			UIFoundation.waitFor(5L);
			String actualcolorofmywine = UIFoundation.getColor(ListPage.imgmywinecolor);
			System.out.println("mywinetextinpip :"+mywinetextinpip);
			System.out.println("actualcolorofmywine :"+actualcolorofmywine);
			if (mywinetextinpip.equals(actualMyWine) && (actualcolorofmywine.equalsIgnoreCase(actualWinecolor))) {
				objStatus += true;
				String objDetail = "Product is  highligted to purple color and added to My Wine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Product is not added";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			String prodadded = UIFoundation.getText(ListPage.spnpipName);
//			System.out.println("prodadded :"+prodadded);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
	        String	myWineAddedProd =	UIFoundation.getText(ListPage.lnkfirstListProd);
//	        System.out.println("myWineAddedProd :"+myWineAddedProd);
			if (prodadded.contains(myWineAddedProd)) {
				objStatus += true;
				String objDetail = "Product added in pip is displayed in My wine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Product added in pip is not displayed in My wine ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			
			//Reverse the changes done
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method VerifyAddToMyWineFunctionality ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify Add To My Wine Functionality test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Add To My Wine Functionality test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);

			return "Fail";

		}
	}
	
	/***************************************************************************
	 * Method Name : verifyAddToMyWineFunctionalityinPIPforRating(NFP-4253) Created By :
	 * Vishwanath Chavan Reviewed By : Ramesh,KB Purpose :
	 * 
	 ****************************************************************************
	 */

	public static String verifyAddToMyWineFunctionalityinPIPforRating()

	{
		String objStatus = null;
		String screenshotName = "VerifyAddToMyWineFunctionality.jpeg";
		
		try {
			log.info("The execution of the method verifyAddToMyWineFunctionalityinPIPforRating started here ...");
			String actualMyWine = verifyexpectedresult.expectedWineText;
			String actualWinecolor = verifyexpectedresult.expWineColor;
			driver.get("https://qwww.wine.com/product/hess-allomi-cabernet-sauvignon-2017/525938");
			UIFoundation.waitFor(1L);
			if (UIFoundation.isElementDisplayed(ListPage.imgstarsRating)) {
				objStatus += true;
				String objDetail = "Rating star is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Rating star is not present in product pip page.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			if (UIFoundation.isElementDisplayed(ListPage.lnkAddToMyWineFunc)) {
				objStatus += true;
//				objStatus += String.valueOf(UIFoundation.clickObject(driver, "modalWindowClose"));
				String objDetail = "AddToMyWine is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "AddToMyWine is not present in product pip page.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			String mywinetextinpip = UIFoundation.getText(ListPage.lnkAddToMyWineFunc);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgstarsRating));
			String prodadded = UIFoundation.getText(ListPage.spnpipName);
			System.out.println("prodadded :"+prodadded);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
	        String	myWineAddedProd =	UIFoundation.getText(ListPage.lnkfirstListProd);
	        System.out.println("myWineAddedProd :"+myWineAddedProd);
			if (prodadded.contains(myWineAddedProd)) {
				objStatus += true;
				String objDetail = "Product added in pip is displayed in My wine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail);
			} else {
				objStatus += false;
				String objDetail = "Product added in pip is not displayed in My wine ";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
			}
			
			//Reverse the changes done
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstListProd));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddToMyWineFunc));
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method verifyAddToMyWineFunctionalityinPIPforRating ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify Add To My Wine Functionality test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Add To My Wine Functionality test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);

			return "Fail";

		}
	}
}
