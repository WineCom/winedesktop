package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.LoginPage;
import wine.autotest.desktop.test.Desktop;


public class CreateAccountButtonInactive extends Desktop {
	
	/***************************************************************************
	 * Method Name			: createButtonInActive()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String createButtonInActive() {
		
		String objStatus=null;
		try {
			log.info("The execution of method createButtonInActive started here");
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.clckObject(LoginPage.lnkSignIn));
			UIFoundation.waitFor(3L);;
			if(UIFoundation.isDisplayed(LoginPage.btnCreateAcc)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnCreateAcc));
				UIFoundation.waitFor(2L);
				
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.txtEmail,"email"));
			boolean status=UIFoundation.isEnabled(LoginPage.btnCreateAccount);
			log.info("The execution of the method createButtonInActive ended here ...");
			if(status==true && objStatus.contains("false"))
			{
				System.out.println(" Verify the create account button is inactive until mandatory fields are filled test case is failed");
				logger.fail(" Verify the create account button is inactive until mandatory fields are filled test case is failed");
				return "Fail";
			}else
			{
				System.out.println("Verify the create account button is inactive until mandatory fields are filled test case is executed successfully");
				logger.pass("Verify the create account button is inactive until mandatory fields are filled test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method createButtonInActive "
					+ e);
			return "Fail";
		}
	}

}
