package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;
import wine.autotest.desktop.library.UIBusinessFlows;

public class VerifyTheSocialMediaLinks extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyTheSocialMediaLinks()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyTheSocialMediaLinks()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_SocialMedisLink.jpeg";
			
		try
		{
			log.info("The execution of the method verifyTheSocialMediaLinks started here ...");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkblogLink);
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.lnkblogLink))
			{
				objStatus+=true;
				String objDetail="Blog link is displayed ";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Blog link is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkfaceBookLink))
			{
				objStatus+=true;
				String objDetail="Facebook link is displayed ";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Facebook link is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnktwitterLink))
			{
				objStatus+=true;
				String objDetail="Twitter link is displayed ";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Twitter link is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkpinterestLink))
			{
				objStatus+=true;
				String objDetail="PinterestLink link is displayed ";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="PinterestLink link is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkinstagramLink))
			{
				objStatus+=true;
				String objDetail="Instagram link is displayed ";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Instagram link is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyTheSocialMediaLinks ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the social media links is displayed in the foote test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the social media links is displayed in the foote test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifyTheSocialMediaLinks "+ e);
			return "Fail";
			
		}
	}	

}
