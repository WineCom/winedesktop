package wine.autotest.desktop.test;

import java.io.IOException;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyPickUpFormIsNtDispForDiffBillingAddr extends Desktop {
	
	

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "pickFormUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.chkCaptcha)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.chkCaptcha));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));				
			}
		
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	/***************************************************************************
	 * Method Name : searchProductWithProdName() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to search for
	 * product with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus = null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "vinye"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
			return "Fail";
		}

	}

	
	/***************************************************************************
	 * Method Name			: VerifyPickUpFormIsNtDispForDiffBillingAddr
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4175
	 * @throws IOException 
	 ****************************************************************************
	 */
	
public static String verificationOfPickupInformationForm() {

	 String screenshotName = "verificationOfPickupInformationForm.jpeg";
		
		String objStatus =null;
		
try {
	log.info("verification of pickup in formation form method started here");
	
	driver.navigate().to("https://qwww.wine.com/checkout/payment/new");
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	UIFoundation.waitFor(2L);
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	UIFoundation.waitFor(3L);
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
	objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "WAbillingSt"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingZip, "WAZip"));
	objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
	UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
	UIFoundation.waitFor(1L);
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
	UIFoundation.waitFor(15L);
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.RdoShiptoAddress));
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
	UIFoundation.waitFor(3L);
	if(!UIFoundation.isDisplayed(FinalReviewPage.rdopickupFormHeader)) {
		 objStatus+=true;
	      String objDetail="Pick Up Location Dialog is not displayed";
	      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	      logger.pass(objDetail);
	}else{
		objStatus+=false;
		String objDetail="Pick Up Location Dialog is displayed";
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	}
	if(UIFoundation.isDisplayed(FinalReviewPage.imgpickupLocIcon))
	{
	objStatus+=true;
	ReportUtil.addTestStepsDetails("Pickup location with different state from billing state is selected", "Pass", "");
	logger.pass("Pickup location with different state from billing state is selected");
	}
	else {
		String objDetail="Pickup loacation and CA  not selected";
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	}
	if (objStatus.contains("false")) {
		System.out.println("verification Of Pickup Information Form test case is failed");
		return "Fail";
	} else {
		System.out.println("verification Of Pickup Information Form test case passed sucessfully");
		return "Pass";
	}

} catch (Exception e) {

	log.error("there is an exception arised during the execution of the method execution "
			+ e);
	return "Fail";
}
}
	
}
