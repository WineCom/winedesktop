package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyPromoBarOfWashingtonRegion extends Desktop {
	
	/***************************************************************************
	 * Method Name			: verifyPromoBarOfWashingtonRegion()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String verifyPromoBarOfWashingtonRegion() {
		String objStatus=null;
		   String screenshotName = "Scenarios_promoBarWA_Screenshot.jpeg";
			
		try {
			log.info("The execution of method verifyPromoBarOfWashingtonRegion started here");
			UIFoundation.SelectObject(LoginPage.dwnSelectState, "washingtonReg");
			UIFoundation.waitFor(13L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnAccount));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(5L);
			driver.get("https://qwww.wine.com/list/list-washington-wines/wine/7155-1122");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkWahintonWinesPromoPlusSymbol));
			if(UIFoundation.isDisplayed(LoginPage.spnpromoBarWashingtonReg))
			{
				  objStatus+=true;
			      String objDetail="10% off 6 Bottles from this list with code WAWINE is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      logger.pass(objDetail);
			     
			}else{
				objStatus+=false;
				 String objDetail="10% off 6 Bottles from this list with code WAWINE is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				
			}
			log.info("The execution of the method verifyPromoBarOfWashingtonRegion ended here ...");
			if (objStatus.contains("false")) {
				 System.out.println("Verify the Promo Bar is displayed in Learn about section when Washington is selected as Region test case is failed");
				return "Fail";
			} else {
				 System.out.println("Verify the Promo Bar is displayed in Learn about section when Washington is selected as Region test case executed successfuly");
				return "Pass";
			}

		} catch (Exception e) {
			 System.out.println("Verify the Promo Bar is displayed in Learn about section when Washington is selected as Region test case is failed");
			log.error("there is an exception arised during the execution of the method verifyPromoBarOfWashingtonRegion "
					+ e);
			return "Fail";
		}
	}

}
