package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.CartPage;
import wine.autotest.fw.utilities.UIFoundation;

public class ApplyAndRemovePromoCodeForNewUser extends Desktop {
	
	/***************************************************************************
	 * Method Name : captureOrdersummary() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 ****************************************************************************
	 */


	public static String captureOrdersummary() {
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String totalBeforeTax = null;
		String promeCode = null;
		boolean isdisplayed=false;
		   String screenshotName = "Scenarios_PromoCodeRemove_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Total Before Tax:      " + totalBeforeTax);

			if(UIFoundation.isDisplayed(CartPage.txtPromoCode))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
//				UIFoundation.webDriverWaitForElement(CartPage.lnkPromoCodeApply, "Invisible", "", 50);
				UIFoundation.waitFor(12L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeExpand));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkPromoCodeApply));
				UIFoundation.waitFor(12L);
			}

			System.out.println("============Order summary after applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);;
			promeCode=UIFoundation.getText(CartPage.spnPromoCodePrice);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Prome Code:            " + promeCode);
			System.out.println("Total Before Tax:      " + totalBeforeTax);
			UIFoundation.waitFor(3L);
			UIBusinessFlows.discountCalculator(subTotal, promeCode);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkRemovePromoCode));
			UIFoundation.waitFor(12L);
			isdisplayed=UIFoundation.isDisplayed(CartPage.spnPromoCodePrice);
			if(!isdisplayed)
			{
				System.out.println("Applied Prome code is Removed successfully");
				
				 objStatus+=true;
			      String objDetail=" Applied Prome code is Removed successfully";
			      logger.pass(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			   
			}
			else
			{
				objStatus+=false;
				   String objDetail="The Applied promo code amount is not removed from total amount";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail);
				System.out.println("The Applied promo code amount is not removed from total amount");
			}
			System.out.println("============Order summary after removing promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method captureOrdersummary " + e);
			return "Fail";

		}
	}

}
