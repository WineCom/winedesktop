package wine.autotest.desktop.test;

import com.aventstack.extentreports.MediaEntityBuilder;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.pages.*;
import wine.autotest.fw.utilities.*;
import wine.autotest.fw.utilities.UIFoundation;

public class VerifyShippingCostAppliedToOrderSummary extends Desktop {

	/***************************************************************************
	 * Method Name : VerifyShippingCostAppliedToOrderSummary
	 * Created By : Vishwanath Chavan 
	 * Reviewed By : Ramesh,KB 
	 * Purpose : TM-4189
	 ****************************************************************************
	 */
	
public static String shippingCostAppliedToOrderSummary() {

String objStatus = null;
String screenshotName = "shippingCostAppliedToOrderSummary.jpeg";

try {
	log.info("shipping Cost Applied To Order Summary metho started here...");
		
			  objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			  UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			  objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			 
	System.out.println("============Order summary with shipping And Handling ===============");
	String shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
	System.out.println("Shipping & Handling:   "+shippingAndHandling);
	if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
		//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
	}
	//objStatus += String.valueOf(UIFoundation.clickObject(driver, "ChangeDeliveryDate"));
	UIFoundation.waitFor(4L);
	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightPM));
	UIFoundation.waitFor(9L);
	System.out.println("shippingCostAppliedToOrderSummary : "+objStatus);
	String overnightpm = UIFoundation.getText(FinalReviewPage.spnshippingmethodPmtext);
	String overNgtShipPrice = (overnightpm.substring(16));
	System.out.println("Over Nigt Pm Shipping method total is "+overNgtShipPrice);
	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
	UIFoundation.waitFor(4L);
	shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
	if(overNgtShipPrice.equals(shippingAndHandling)) {
		objStatus += true;
		String objDetail = "Over Nigt PM Shipping total is added to order summary";
		System.out.println("Shipping & Handling:   "+shippingAndHandling);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	} else {
		objStatus += false;
		String objDetail = "Over Nigt PM Shipping total is not  added to order summary";
		ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	}
   UIFoundation.waitFor(2L);
   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDate));
   UIFoundation.waitFor(1L);
   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
   objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightAM));
   UIFoundation.waitFor(3L);
   String overnightam = UIFoundation.getText(FinalReviewPage.spnshippingmethodAmtext);
   String overNgamtShipPrice = (overnightam.substring(16));
   System.out.println("Over Nigt AM Shipping method total is "+overNgamtShipPrice);
   objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
   UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
	shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
	if(overNgamtShipPrice.equals(shippingAndHandling)) {
		objStatus += true;
		String objDetail = "Over Nigt AM Shipping total is added to order summary";
		System.out.println("Shipping & Handling:   "+shippingAndHandling);
		ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	} else {
		objStatus += false;
		String objDetail = "Over Nigt AM Shipping total is not  added to order summary";
		ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
	}
	System.out.println("shippingCostAppliedToOrderSummary : "+objStatus);
	log.info("The execution of the method verifyGiftWrapping ended here ...");
	if (objStatus.contains("false")) {

		return "Fail";
	} else {

		return "Pass";
	}

} catch (Exception e) {

	log.error("there is an exception arised during the execution of the method verifyGiftWrapping " + e);
	return "Fail";
}

}
}

