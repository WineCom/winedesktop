package wine.autotest.desktop.test;

import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.pages.*;
import wine.autotest.desktop.test.Desktop;


public class AboutProffessionalandProductAttributeDescriptionInPIP extends Desktop {
	
	
	/***************************************************************************
	 * Method Name			: AboutProffessionalandProductAttributeDescriptionInPIP()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String aboutProffessionalandProductAttributeDescriptionInPIP()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_SocialMedisLink.jpeg";
			
		try
		{
			log.info("The execution of the method verifyTheSocialMediaLinks started here ...");
/*			objStatus += String.valueOf(UIFoundation.mouseHover(driver, "varietal"));
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "BordexBlends"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "addProductToPIP"));
			UIFoundation.waitFor(2L);*/
			driver.get("https://qwww.wine.com/product/chateau-ducru-beaucaillou-2011/121443");
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(LoginPage.lnktoolTipComponet))
			{
				UIFoundation.clckObject(LoginPage.lnktoolTipComponet);
				UIFoundation.waitFor(1L);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgWineRating));
			if(UIFoundation.isDisplayed(ListPage.winModalWindow))
			{
				objStatus+=true;
				String objDetail="'About Professional Ratings' popup is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="'About Professional Ratings' popup is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.winmodalWindowClose));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.imgthumbImage1) && UIFoundation.isDisplayed(ListPage.imgthumbImage2))
			{
				objStatus+=true;
				String objDetail="thumbnails are displayed under main image";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="thumbnails are not  displayed under main image";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+"thumbnails")).build());
				UIFoundation.captureScreenShot(screenshotpath+"thumbnails", objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.spnproductAttributeName));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.spnproductAttributeTitle) && UIFoundation.isDisplayed(ListPage.spnproductAttributeContent))
			{
				objStatus+=true;
				String objDetail="A popup with explanations of the icons that a given product is associated with, is displayed";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="A popup with explanations of the icons that a given product is associated with, is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyTheSocialMediaLinks ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method aboutProffessionalandProductAttributeDescriptionInPIP "+ e);
			return "Fail";
			
		}
	}	

}
