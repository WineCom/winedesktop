package wine.autotest.desktop.pages;

import org.openqa.selenium.By;

public class OrderDetailsPage {
	
	public static By spnOrderDetailOrdNum1 = By.xpath("(//span[@class='orderItem_id'])[1]");
	public static By spnOrderDetailOrdNum2 = By.xpath("(//span[@class='orderItem_id'])[2]");
	public static By lnkSplitOrderNum = By.xpath("(//a[@class='openOrderDetails_text openOrderDetails_numberText'])[2]");
	public static By spnorderHistoryGftMssg = By.xpath("//span[@class='shippingDetailsRecipient_message']");
	
	public static By spnCreditCardLastFour = By.xpath("//span[@class='reviewPaymentInfo_lastFour']");
	public static By GiftBagInOrderHistory = By.xpath("//span[contains(text(),' Gift Bag')]");
	public static By spnPromo = By.xpath("//div[@class='orderSummary_name orderSummary_discountItemName']");
	public static By spnUsedGiftCard = By.xpath("(//tr[@class='orderSummary_row orderSummary_giftCardUsed']/th[text()='Gift Card Used'])");
	
	public static By spnProgressBarOrderHis = By.xpath("(//div[contains(@class,'openOrderStatus')])[1]");
	public static By lnkorderHistoryGftbag = By.xpath("(//a[@class='orderItemInfo_link'])[2]");
	public static By spnorderHisSilverSheer = By.xpath("//a/span[@class='orderItemInfo_name' and text()=' SILVER/SHEER Bottle Gift Bag']");
	public static By spnorderHisNatureBrown = By.xpath("//a/span[@class='orderItemInfo_name' and text()=' BROWN/BURLAP Bottle Gift Bag']");
	public static By spnorderHisRedVelvet = By.xpath("//a/span[@class='orderItemInfo_name' and text()=' RED/VELVET Bottle Gift Bag']");
	public static By spnCrdCardName = By.xpath("//span[@class='billingDetailsPayment_carrier']");
	public static By spnStewardshipSaving= By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");

}
