package wine.autotest.desktop.pages;

import org.openqa.selenium.By;

public class CartPage {

	public static By btnCheckout = By.xpath("//a[contains(text(),'Check')]");
	public static By btnCheckoutAlter = By.xpath("//a[text()='Check Out']");
	public static By spnSubtotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_subTotalPrice']");
	public static By spnShippingHandling = By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");
	public static By spnTotalBeforeTax = By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']");
	public static By spnOrderSummarySalesTax = By.xpath("//tr[@class='orderSummary_row orderSummary_tax']");
	public static By btnCartCount = By.xpath("//span[@class='cartBtn_count']");
	public static By spnShoppingCartHeader = By.xpath("//div[@class='cart_headline']");
	public static By spnyourCartIsEmpty = By.xpath("//p[text()='Your cart is empty.']");
	public static By spncurrentlyUnavailable = By.xpath("(//div[@class='productUnavailable'])[1]");
	
	//Promo Code
	public static By txtPromoCode = By.xpath("//input[@name='promoCode']");
	public static By lnkPromoCodeApply = By.xpath("//button[@class='btn btn-link orderCodesForm_apply orderCodesForm_applyPromo']");
	public static By spnPromoCodeApplied = By.xpath("//p[text()='Promo code applied']");
	public static By lnkPromoCodeExpand = By.xpath("//fieldset[@class='formWrap_toggleCheckboxGroup formWrap_toggleCheckboxGroupPromo']");
	public static By spnPromoCodePrice = By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
	public static By lnkRemovePromoCode = By.xpath("//div[@class='orderCodesForm_remove orderCodesForm_removePromo']");
	public static By spnInvalidPromoCode = By.xpath("//p[text()='We do not recognize this promotional code. Please check the spelling of the code and/or verify its expiration date.']");
	public static By spnpromocodeRedemptionWarningMSg = By.xpath("//div[text()='Only shipping discounts can be applied to this product.']");
	public static By spnminimumOfferAmount = By.xpath("//p[text()='Order subtotal does not meet minimum amount for this offer.']");
	public static By dwnincreaseQuantity = By.xpath("//select[@class='productQuantity_select']");
	
	//ExistingCustomer login in CartPage Popup
	public static By btnexistingCust = By.xpath("(//a[text()='Sign in'])[2]");
	public static By btncontinueShipToKY = By.xpath("//a[text()='Continue (ship to KY)']");
	public static By spnShipTodayWarningMsg = By.xpath("//div[@class='cartNote_message' and text()=' consider replacing the item(s) noted below or creating separate orders.']");
	public static By spnShipTodayWarningMsg1 = By.xpath("//div[@class='cartNote_message']");
	
	//
	public static By spnProductPriceHasStrike = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice']/div[@class='productPrice_price-reg has-strike']");
	public static By spnProductPriceHasSale = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By spnProductQuantity = By.xpath("(//select[@class='productQuantity_select'])[1]");
	public static By spnProductItemLimitQuantity = By.xpath("//select[@class='prodItemStock_quantitySelect']");
	public static By spnProductPriceTotal = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice_total']");
	public static By spnProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[1]");
	public static By btnCancleStayIn = By.xpath("//div[text()='Cancel (stay in ']");
	public static By spngetState = By.xpath("(//div[@class='pageHeader_dropDown']//select[@class='state_select state_select-promptsCartTransfer']//option[@selected='true'])");
	
	//Gift Card
	public static By chkGift = By.xpath("(//main//section[@class='orderCodes']//span[@class='formWrap_toggleCheckboxSpan'])[2]");
	public static By txtGiftNumber = By.xpath("//fieldset[@class='orderCodesForm_group orderCodesForm_groupGift formWrap_group']//input[@class='formWrap_input orderCodesForm_input orderCodesForm_inputGift']");
	public static By lnkGiftApply = By.xpath("//button[@class='btn btn-link orderCodesForm_apply orderCodesForm_applyGift']");
	public static By spnGiftCertificatePrice = By.xpath("//td[@class='orderSummary_price orderSummary_giftCertPrice']");
	public static By spnGiftErrorCode = By.xpath("//p[@class='orderCodes_error orderCodes_GiftError orderCodes_GiftErrorCode']");
	
	public static By spnaccountCreditLabelAmount = By.xpath("//tr[@class='orderSummary_row orderSummary_giftCardUsed']//th[@class='orderSummary_name']");
	public static By spntotalAmountZero = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_giftCardRemaining']");
	public static By spncreditRemainingHeader = By.xpath("//table[@class='orderSummary_table']/tbody/tr[@class='orderSummary_row orderSummary_accountCredit']/th[text()='Credit Remaining']");
	public static By spngiftCertificateSuccessMsg = By.xpath("//p[@class='orderCodes_success orderCodes_giftCodeSuccess']");
	public static By spnmaxGiftCodeWarningMsg = By.xpath("//p[@class='orderCodes_success orderCodes_maxGiftCodesEntered']");
	public static By spnaccountCreditLabelColor = By.xpath("//table[@class='orderSummary_table']/tbody/tr[@class='orderSummary_row orderSummary_giftCert']");
		
	//In Cart
	public static By lnkFirstProductInCart = By.xpath("//ul[@class='prodList']/li[1]/div[2]/div[3]/a/span");
	public static By lnkSecondProductInCart = By.xpath("//ul[@class='prodList']/li[2]/div[2]/div[3]/a/span");
	public static By lnkThirdProductInCart = By.xpath("//ul[@class='prodList']/li[3]/div[2]/div[3]/a/span");
	public static By lnkFourthProductInCart = By.xpath("//ul[@class='prodList']/li[4]/div[2]/div[3]/a/span");
	public static By lnkFifthProductInCart = By.xpath("//ul[@class='prodList']/li[5]/div[2]/div[3]/a/span");
	
	public static By lnkFirstProductPrice = By.xpath("(//div[@class='productPrice_total'])[1]");
	public static By lnkSecondProductPrice = By.xpath("(//span[@class='productPrice_unitPrice'])[2]");
	public static By lnkRemoveFirstProduct = By.xpath("(//span[@class='iconRoundedOutline'])[1]");
	public static By lnkRemoveSecondProduct = By.xpath("(//span[@class='iconRoundedOutline'])[2]");
	public static By lnkRemoveThirdProduct = By.xpath("(//span[@class='iconRoundedOutline'])[3]");
	public static By lnkRemoveFourthProduct = By.xpath("(//span[@class='iconRoundedOutline'])[4]");
	public static By btnRemove = By.xpath("//div[@class='leftSlide leftSlideActive']/span[text()='Remove']");
	public static By btnContinueShopping = By.xpath("//a[text()='Continue Shopping']");
	
	public static By imgFirstImageInCart = By.xpath("//section[@class='cartContents']/ul/li[1]/div[2]/div[2]/a/div");
	public static By imgSecondImageInCart = By.xpath("//section[@class='cartContents']/ul/li[2]/div[2]/div[2]/a/div");
	public static By imgThirdImageInCart = By.xpath("//section[@class='cartContents']/ul/li[3]/div[2]/div[2]/a/div");
	public static By imgFourthImageInCart = By.xpath("//section[@class='cartContents']/ul/li[4]/div[2]/div[2]/a/div");
	public static By imgFifthImageInCart = By.xpath("//section[@class='cartContents']/ul/li[5]/div[2]/div[2]/a/div");
	public static By txtcartBtn = By.xpath("//a[@class='cartBtn']/span[2]");
	
	//Save for later
	public static By spnSaveForLaterCount = By.xpath("//span[@class='saveForLater_headline-count']");
	public static By lnkFirstProductSaveForLater = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[1]");
	public static By lnkSecondProductSaveForLater = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[2]");
	public static By lnkThirdProductSaveForLater = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[3]");
	public static By lnkFourthProductSaveForLater = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[4]");
	public static By lnkFifthProductSaveForLater = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[5]");
	public static By lnkmoveToCartFirstLink = By.xpath("(//div[text()='Move to Cart'])[1]");
	public static By lnkmoveToCartSecondLink = By.xpath("(//div[text()='Move to Cart'])[2]");
	public static By lnkmoveToCartText = By.xpath("(//div[@class='prodItem_wrap prodItem_saveForLater']//span[@class='prodItemInfo_name'])[1]");
	public static By lnkmoveToCartSecText = By.xpath("(//div[@class='prodItem_wrap prodItem_saveForLater']//span[@class='prodItemInfo_name'])[2]");
	public static By lnkpreSelInCheckOut= By.xpath("(//div[@class='prodItemPreSale'])[1]");
	public static By spnSaveForLaterHeadline= By.xpath("//div[@class='saveForLater_headline']");
	public static By spnNoItemsInSaveForLater= By.xpath("//div[text()='You have no items saved for later.']");
	public static By spnSaveForLaterAfterCheckout= By.xpath("//section[@class='cartOrderDetails']/following-sibling::section");
	public static By lnkFirstProductInSaveFor= By.xpath("//section[@class='saveForLater']/ul/li[1]/div[2]/div[3]/a/span");
	public static By lnkSecondProdcutInSaveFor= By.xpath("//section[@class='saveForLater']/ul/li[2]/div[2]/div[3]/a/span");
	public static By lnkThirdProductInSaveFor= By.xpath("//section[@class='saveForLater']/ul/li[3]/div[2]/div[3]/a/span");
	public static By lnkFourthProdcutInSaveFor= By.xpath("//section[@class='saveForLater']/ul/li[2]/div[2]/div[3]/a/span");
	public static By lnkFifthProductInSaveFor= By.xpath("//section[@class='saveForLater']/ul/li[3]/div[2]/div[3]/a/span");
	public static By lnkRemoveThirdProductFromSave = By.xpath("(//span[@class='iconRoundedOutline'])[4]");
	
	//StewardShip
	public static By spngetFreeShippingAll = By.xpath("//div[@class='stewardShipSection_headline']/span[text()='Want Free Shipping?']");
	public static By spnStewardtextR = By.xpath("//span[@class='stewardShipContent_textPost']");
	public static By lnkgetFreeShipping = By.xpath("//a[@class='stewardShipUpsell_link stewardShipUpsell_addToCart']");
	public static By lnkstewardAdd2R = By.xpath("(//a[@class='stewardShipUpsell_link stewardShipUpsell_addToCart'])[1]");
	public static By spnstewardShipUpsellOptionDefault= By.xpath("//div[@class='stewardShipUpsellOption default']");
	public static By lnkstewardShipUpsellOption= By.xpath("//div[@class='stewardShipUpsellOption default']//following-sibling::a[2]");
	public static By spnwantFreeShiping = By.xpath("//span[text()='Get Free Shipping All Year!']");
	public static By dwnstewardShipUpsellOptionA = By.xpath("//div[@class='stewardShipUpsellOption varA']/div/span[text()='Want Free Shipping?']");
	public static By spnwantFreeShipping = By.xpath("(//div[@class='stewardShipSection_headline']/span[text()='Want Free Shipping?'])[1]");
	public static By spnwantFreeShipingAllYear = By.xpath("//span[text()='Get Free Shipping All Year!']");
	public static By btnStewardshipConfirm = By.xpath("//button[text()='Confirm']");
	public static By spnStewardshipDiscountPrice = By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
	public static By lnkAddStewardshipMember = By.xpath("//a[text()='Add to Cart']");
	
	public static By stewardQuestioMarkIcon = By.xpath("//span[@class='orderSummary_stewardshipHint']");
	public static By spnwhyshippText = By.xpath("//h2[text()='Why is shipping wine so expensive?']");
	public static By btnlearnMore = By.xpath("//button[@class='aboutShipping_learnStewardship btn btn-small btn-red btn-rounded']");
}
