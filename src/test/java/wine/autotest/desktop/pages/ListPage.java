package wine.autotest.desktop.pages;

import org.openqa.selenium.By;

public class ListPage {
	
	public static By lnkhomePageBanner = By.xpath("//section[@class='homePageBanner_textWrap']");
	public static By lnkvarietal = By.xpath("//a[@class='mainNav_sectionTab mainNav_section-varietal']");
	public static By imgWineLogo = By.xpath("//a[@class='wineLogo']");
	public static By lnkVarietalR = By.xpath("(//input[@class='filterMenu_showRadio'])[4]");
	public static By lnkRegion = By.xpath("(//input[@class='filterMenu_showRadio'])[5]");
	public static By lnkRegionTab = By.xpath("//div[@class='filterMenu filterMenu-list nestedRegion filterMenu-hasChildren']");
	public static By lnkRatingAndPrice = By.xpath("//div[@class='filterMenu filterMenu-priceFilter filterMenu-priceRatings filterMenu-hasChildren']");
	public static By lnkmoreFilters = By.xpath("//input[@class='filterWidgetExpander_input']");
	public static By spnreviewedBy = By.xpath("//span[text()='Reviewed By']");
	public static By spnsizeAndType = By.xpath("//span[text()='Size & Type']");
	public static By spnfineWine = By.xpath("//span[text()='Fine Wine']");
	public static By lnkVintage = By.xpath("//span[text()='Vintage']");
	public static By btnHide = By.xpath("//input[@class='filterWidgetExpander_input']");
	public static By lnkMyWine = By.xpath("//a[@class='activityBtn']");
	public static By spnCartCount = By.xpath("//span[@class='cartBtn_count']");
	
	//Varietals
	public static By lnkSangviovese = By.xpath("//a[text()='Sangiovese']");
	public static By lnksyrah = By.xpath("//a[text()='Syrah / Shiraz']");
	public static By lnkRiesling = By.xpath("//a[text()='Riesling']");
	public static By lnkZinfandel = By.xpath("//a[text()='Zinfandel']");
	public static By lnkNapaValley = By.xpath("(//a[text()='Napa Valley'])[1]");
	public static By lnkwashingtonWines = By.xpath("//a[text()='Washington']");
	public static By lnkSpainWines = By.xpath("//a[text()='Spain']");
	public static By lnkRegionR = By.xpath("(//a[text()='Region'])[1]");
	public static By lnkCaberNet = By.xpath("//a[text()='Cabernet Sauvignon']");
	public static By lnkBordexFeatures = By.xpath("//a[text()='Bordeaux Futures']");
	public static By lnkBordexBlends = By.xpath("//a[text()='Bordeaux Blends']");
	public static By lnkFeatured = By.xpath("(//a[text()='Discover'])[1]");
	public static By lnkCentralCoast = By.xpath("//a[text()='Central Coast']");
	public static By lnk94PlusRatedProducrts = By.xpath("//a[text()='94+ Rated Under $80']");
	public static By lnkPinotNoir = By.xpath("//a[text()='Pinot Noir']");
	public static By lnkotherUS = By.xpath("//a[text()='Other U.S.']");
	public static By lnkGreenWine = By.xpath("//a[text()='Green / Sustainable']");
	public static By lnkRhoneBlends = By.xpath("//a[text()='Rh�ne Blends']");
	public static By lnkSauvignonBlanc = By.xpath("//a[text()='Sauvignon Blanc']");
	public static By lnkcalifornia = By.xpath("//a[@class='filterMenu_itemLink']/span[text()='California']");
	public static By lnkcaliforniaAll = By.xpath("//span[@class='filterMenu_itemLink']/span[text()='California']");
	public static By spncurrentlyUnavailableProd = By.xpath("(//span[@class='prodItemStock_soldOut'])[1]");
	public static By lnkFiltercalifornia = By.xpath("(//span[text()='California'])[2]");
	public static By spnheroImageRegionText = By.xpath("(//span[text()='California'])[2]");
	public static By spnheroImageVarietalText = By.xpath("(//span[text()='Cabernet Sauvignon'])[1]");
	
	public static By dwnFirstProdQuantitySelect = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[1]");
	public static By dwnSecondProdQuantitySelect = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[2]");
	public static By dwnThirdProdQuantutySelct = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[3]");
	public static By dwnFourthProdQuantitySelect = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[4]");
	public static By dwnlimitReachedInList = By.xpath("//div[@class='prodItemStock_limitReached']");
	public static By lnkSort = By.xpath("(//span[text()='Sort: '])[1]");
	public static By btnAddToCart = By.xpath("(//button[text()='Add to Cart'])[1]");
	public static By lnklistPageContainer = By.xpath("(//div[@class='sortOptions_content'])[1]");
	public static By btnCartCount = By.xpath("//a[@class='cartBtn']");
	public static By spnProductItemLimitCount = By.xpath("//span[@class='prodItemLimit_count']");
	public static By simProddIsAbvWineMakrNt = By.xpath("(//h2[text()='Winemaker Notes']/ancestor::section[@class='pipSecContent']//h2[@class='prodRec_headline'])[2]");
	
	public static By dwnChangeState = By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[1]");
	public static By btnAddToCartFirst = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/div[3]/span");
	public static By btnAddToCartSecond = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/div[3]/span");
	public static By btnAddToCartThird = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/div[3]/span");
	public static By btnAddToCartFourth = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/div[3]/span");
	public static By btnAddToCartFifth = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/div[3]/span");
	public static By lnkQAUserPopUp = By.xpath("(//button[text()='Add to Cart'])[1]");
	public static By lnkFirstProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[1]");
	public static By lnkSecondProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[2]");
	public static By lnkThirdProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[3]");
	public static By lnkFourthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[4]");
	public static By lnkFifthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[5]");
	public static By lnkSixthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[6]");
	public static By lnkSeventhProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[7]");
	public static By lnkEightProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[8]");
	public static By lnknotEligiblepromocodeRedemptionProduct = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[1]");
	
	public static By txtsearchProduct = By.xpath("(//input[@class='formWrap_input searchBarForm_input'])[1]");
	public static By lnkSearchTypeList = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[6]");
	public static By lnkgifts = By.xpath("//a[@class='mainNav_sectionTab mainNav_section-gifts']");
	public static By txtWhiteWine = By.xpath("(//a[text()='White Wine'])[2]");
	public static By spnHeaderText = By.xpath("(//span[@class='sortOptions_label'])[1]");
	public static By chkshipSoon = By.xpath("//div[@class='listOptions listOptions-tablet']//input[@class='shipsSoonest_checkbox']");
	public static By lnkSortOptions = By.xpath("//div[@class='listOptions listOptions-tablet']//select[@class='sortOptions_sort']");
	public static By lnkmostIntersting = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Most Interesting']");
	public static By lnkSortA_Z = By.xpath("(//select[@class='sortOptions_sort'])[1]/option[text()='Winery: A to Z']");	
	public static By lnkShipsSoonestR = By.xpath("//div[@class='listOptions listOptions-tablet']//input[@class='shipsSoonest_checkbox']");
	public static By lnkshipTodayFirstProd = By.xpath("(//div/div[contains(text(),'Ships today')])[1]");
	public static By lnkshipTodaySecoProd = By.xpath("(//div/div[contains(text(),'Ships today')])[2]");
	public static By lnkshipTodayThirdProd = By.xpath("(//div/div[contains(text(),'Ships today')])[3]");
	public static By spnShipTodayThirdProductName = By.xpath("//ul[@class='prodList']/li[3]//*[@class='prodItemStock_quantitySelect']/option[text()='Ships Today']");
	
	public static By lnkFrstProductName = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/a/span");
	public static By lnkSecProductName = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/a/span");
	public static By lnkThrdProductName = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/a/span");
	public static By lnkFrthProductName = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/a/span");
	public static By lnkFfthProductName = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/a/span");
	public static By lnkFrstProductPrice = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice_price-reg']");
	public static By lnkSecndProductPrice = By.xpath("(//div[@class='prodItemInfo'])[2]/div[@class='productPrice']/div[@class='productPrice_price-reg']");
	public static By lnkThrdProductPrice = By.xpath("(//div[@class='prodItemInfo'])[3]/div[@class='productPrice']/div[@class='productPrice_price-reg']");
	public static By lnkFrthProductPrice = By.xpath("(//div[@class='prodItemInfo'])[4]/div[@class='productPrice']/div[@class='productPrice_price-reg']");
	public static By lnkFifhtProductPrice = By.xpath("(//div[@class='prodItemInfo'])[5]/div[@class='productPrice']/div[@class='productPrice_price-reg']");

	public static By lnkFrstProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By lnkSecondProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[2]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By lnkThirdProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[3]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By lnkFourthProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[4]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By lnkFifthProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[5]/div[@class='productPrice']/div[@class='productPrice_price-sale']");

	public static By lnkSortLtoH = By.xpath("(//select[@class='sortOptions_sort'])[1]/option[text()='Price: Low to High']");
	public static By lnkSortHtoL = By.xpath("(//select[@class='sortOptions_sort'])[1]/option[text()='Price: High to Low']");
	public static By chkOutOfStock = By.xpath("(//input[@class='checkStock_checkbox'])[2]");
	public static By lnkFirstProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[6]/a");
	public static By lnkSecondProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[7]/a");
	public static By lnkThirdProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[8]/a");
	public static By lnkFourthProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[9]/a");
	public static By lnkFifthProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[10]/a");
	public static By imguserRatings = By.xpath("//div[@class='userRatingsComp_minOverlay'][1]");
	public static By txtAreaCount = By.xpath("(//fieldset[@class='formWrap_group userRatingNote_fieldset']/span[@class='textarea_counter'])[1]");
	public static By btncancelInMyWine = By.xpath("(//button[text()='Cancel'])[1]");
	public static By btnsaveInMyWine = By.xpath("(//button[text()='Save'])[1]");
	public static By clearStarRating = By.xpath("//div[@class='starRating js-hasRating']/button[@class='starRating_clear']");
	
	public static By lnkFirstProductInCart = By.xpath("//ul[@class='prodList']/li[1]/div[2]/div[3]/a/span");
	public static By lnkSecondProductInCart = By.xpath("//ul[@class='prodList']/li[2]/div[2]/div[3]/a/span");
	public static By lnkThirdProductInCart = By.xpath("//ul[@class='prodList']/li[3]/div[2]/div[3]/a/span");
	public static By lnkFourthProductInCart = By.xpath("//ul[@class='prodList']/li[4]/div[2]/div[3]/a/span");
	public static By lnkFifthProductInCart = By.xpath("//ul[@class='prodList']/li[5]/div[2]/div[3]/a/span");
	
	public static By spnFirstRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[1]");
	public static By spnSecondRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[2]");
	public static By spnThirdRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[3]");
	public static By spnFourthRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[4]");
	public static By spnFifthRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[5]");
	public static By spnSixthRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[6]");

	public static By spnFirstProductPrice = By.xpath("(//div[@class='productPrice_total'])[1]");
	public static By spnSecondProductPrice = By.xpath("(//div[@class='productPrice_total'])[2]");
	public static By spnThirdProductPrice = By.xpath("(//div[@class='productPrice_total'])[3]");
	public static By btncontinueShipToDE = By.xpath("//a[text()='Continue (ship to DE)']");
	public static By spnfirstProdName = By.xpath("(//div[@class='prodItemShipping'])[1]");
	public static By lnkfirstProdNameLink = By.xpath("(//div[@class='prodItemInfo'])[1]/a");
	public static By spnsecondProdName = By.xpath("(//div[@class='prodItemShipping'])[2]");
	public static By lnksecondProdNameLink = By.xpath("(//div[@class='prodItemInfo'])[2]/a");
	public static By spnShipsOnStatementInPIP = By.xpath("//div[contains(text(),'Ships today if ordered in next ')]");
	
	public static By spnprofessionalRatingHeader = By.xpath("//div[@class='modalWindow_header']/h2[@class='modalWindowHeader_title']");
	public static By lnkprofessionalRatingR = By.xpath("(//span[@class='wineRatings_initials'])[1]");
	public static By cmbCARegion = By.xpath("//span[@class='filterMenu_itemName' and text()='California']");
	public static By cmbCARegSonomaCounty = By.xpath("//span[@class='filterMenu_itemName' and text()='Sonoma County']");
	public static By titheroImageTitleSonoma = By.xpath("(//span[text()='Sonoma County Wine'])[1]");
	public static By heroImagePlusSymbol = By.xpath("//section[@id='learnAboutContent']/div[@class='listPageContentContainerWrap']//button[@class='listPageContent_expand']/a[@href='#learnAboutContent']");
	public static By ContheroImageTextSonoma = By.xpath("//p[contains(text(),'Sonoma County has something for every wine lover')]");
			
	public static By spnproductPriceRegWhole = By.xpath("(//div[@class='productPrice_price-reg']/span[@class='productPrice_price-regWhole'])[1]");
	public static By spnproductPriceRegFraction = By.xpath("(//div[@class='productPrice_price-reg']/span[@class='productPrice_price-regFractional'])[1]");
	public static By spnproductPriceSaleWhole = By.xpath("(//div[@class='productPrice_price-sale']//span[@class='productPrice_price-saleWhole'])[1]");
	public static By spnproductPriceSaleFraction = By.xpath("(//div[@class='productPrice_price-sale']//span[@class='productPrice_price-saleFractional'])[1]");
	public static By spnabvText = By.xpath("(//span[@class='prodAlcoholPercent_inner'])[1]");
	public static By spnfoundLowerPrice = By.xpath("//a[text()='Found a lower price?']");	
	
	public static By lnkcabernetMore = By.xpath("(//li[text()='More'])[1]");	
	public static By lnkmerlot = By.xpath("//a[text()='Merlot']");	
	public static By lnkselectSortOption = By.xpath("(//select[@class='sortOptions_sort'])[1]");	
	public static By lnkvinatageNtoO = By.xpath("(//option[text()='Vintage: New to Old'])[1]");	
	public static By lnkmerlotFirstProd = By.xpath("(//span[@class='prodItemInfo_name'])[1]");	
	public static By lnkmerlotSecProd = By.xpath("(//span[@class='prodItemInfo_name'])[2]");	
	public static By lnkmerlotThirdProd = By.xpath("(//span[@class='prodItemInfo_name'])[3]");	
	public static By lnkmerlotFourthProd = By.xpath("(//span[@class='prodItemInfo_name'])[4]");	
	public static By lnkmerlotFifthProd = By.xpath("(//span[@class='prodItemInfo_name'])[5]");	
	public static By lnkrate2Product = By.xpath("//span[@class='starRating_star starRating_star-right starRating_star-2']");
	public static By lnkrateProduct = By.xpath("//span[@class='starRating_star starRating_star-left starRating_star-7']");
	public static By lnkrateProduct2 = By.xpath("//span[@class='starRating_star starRating_star-right starRating_star-4']");
	public static By lnkratedProduct2 = By.xpath("//span[@class='starRating_star starRating_star-right starRating_star-4 js-hasRating']");
	public static By lnkrateProduct4 = By.xpath("//span[@class='starRating_star starRating_star-right starRating_star-8']");
	public static By lnkratedProduct4 = By.xpath("//span[@class='starRating_star starRating_star-right starRating_star-8 js-hasRating']]");
	public static By lnkprodInHomePage = By.xpath("//span[@class='prodRec_addToCart'][1]");	
	public static By addToCartAlert = By.xpath("//div[@class='addToCartAlert']");
	public static By txtselQty= By.xpath("(//span[@class='prodItemStock_quantitySelectLength'])[1]");
	public static By selQtyInALert= By.xpath("(//span[@class='addToCartAlert_message'])");
	
	//PIP Page
	public static By imgstarsRating = By.xpath("//div[contains(@class,'starRating')]");
	public static By imgWineRating = By.xpath("(//span[@class='wineRatings_rating'])[1]");
	public static By imgshare = By.xpath("//i[@class='prodActionsIcons_share icon icon-share']");
	public static By lnkgiftProdutcAddToPIP = By.xpath("(//span[@class='prodItemInfo_name'])[2]");
	public static By lnkGiftsBottles = By.xpath("//a[text()='1 \u2013 4 Bottles']");
	public static By lnkaddProductToPIP = By.xpath("(//a[@class='prodItemInfo_link'])[1]");
	public static By lnksetAlert = By.xpath("//button[@class='prodAlertForm_btn btn btn-link']");
	public static By lnkvintageAlert = By.xpath("//span[@class='prodActionsIcons_alertIcon icon icon-bell']");
	public static By Iconshare = By.xpath("//span[text()='Share']");	
	public static By spnPIPName = By.xpath("//h1[@class='pipName']");
	public static By winModalWindow = By.xpath("//div[@class='modalWindow_content']");
	public static By imgAttributeIcon = By.xpath("(//ul[@class='prodAttr'])[1]");
	public static By winmodalWindowClose = By.xpath("//div[@class='js-closeModal modalWindow_cancel icon icon-menu-close']");
	public static By imgthumbImage1 = By.xpath("//img[@class='pipThumb_image pipThumb_img-1']");
	public static By imgthumbImage2 = By.xpath("//img[@class='pipThumb_image pipThumb_img-2']");
	public static By spnproductAttributeName = By.xpath("(//li[@class='icon icon-glass-red prodAttr_icon prodAttr_icon-redWine'])[1]");
	public static By spnproductAttributeTitle = By.xpath("//div[@class='prodAttrLegend_title']");
	public static By spnproductAttributeContent = By.xpath("//p[contains(text(),'Learn about red wine')]");
	public static By spnprodAlert= By.xpath("//div[@class='prodAlert']");
	public static By lnkaddSecondProductToPIP= By.xpath("(//a[@class='prodItemInfo_link'])[3]");
	public static By lnkviewAllWine= By.xpath("(//span[text()='View all products'])[1]");
	public static By spnEror404 = By.xpath("//p[text()='404 Error']");
	public static By lnkfilterMenuType= By.xpath("(//div[@class='filterMenu_type'])[3]");
	public static By lnkvarietalFilterIcon = By.xpath("//i[@class='filterMenu_filterIcon icon icon-grapes']");
	public static By spnvarietalFilterName = By.xpath("(//span[text()='Cabernet Sauvignon'])[1]");
	public static By lnkvarietalFilterClose = By.xpath("//div[@class='filterMenu_clearRefinements filterMenu_clearRefinements-showing']");
	public static By spnsliderBarRating = By.xpath("//div[@id='ratingRangeSliderInput']");
	public static By spnsliderBarPricing = By.xpath("//div[@id='priceRangeSliderInput']");
	public static By spnlimitReachedInPIP = By.xpath("//div[text()='Limit Reached']");
	public static By imgorderOfProductIcons = By.xpath("(//div[@class='prodInfo']//li[@title='Red Wine'])[1]/following-sibling::li[@title='Collectible']/following-sibling::li[@title='Great Gift']");
	public static By lnkvarietalOrigin = By.xpath("(//a[@class='pipOrigin_link'])[1]");
	public static By lnkregionOrgigin = By.xpath("(//a[@class='pipOrigin_link'])[2]");
	public static By addRecommendedProducts = By.xpath("(//span[@class='prodRec_addToCart'])[1]");
	public static By lnkhomePageRecommendedProducts1 = By.xpath("(//div[@data-rr-placement='home_page.rr1']//span[@class='prodRec_addToCart'])[1]");
	public static By lnkhomePageRecommendedProducts2 = By.xpath("(//div[@data-rr-placement='home_page.rr2']//span[@class='prodRec_addToCart'])[1]");
	public static By lnkhomePageRecommendedProducts3 = By.xpath("(//div[@data-rr-placement='home_page.rr3']//span[@class='prodRec_addToCart'])[1]");
	public static By lnkrecommendedProduct = By.xpath("(//li[@class='scrollerList_item'][1]//span[@class='scrollerList_title'])[1]");
	public static By rareProductAttribute = By.xpath("(//li[@class='icon icon-rare prodAttr_icon prodAttr_icon-rare'])[1]");
	
	//My Wine Page
	public static By lnkmyWineSelectSort = By.xpath("//select[@class='sortOptions_sort']");
	public static By lnkpurchaseDateNewToOld = By.xpath("//option[text()='Purchase Date: New to Old']");
	public static By lnkpurchaseDateOldToNew = By.xpath("//option[text()='Purchase Date: Old to New']");
	public static By lnkratingDateNewToOld = By.xpath("//option[text()='Rating Date: New to Old']");
	public static By lnkratingDateOldToNew = By.xpath("//option[text()='Rating Date: Old to New']");
	public static By lnkmyWineFirstProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[1]");
	public static By lnkmyWineSecProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[2]");
	public static By lnkmyWineThirdProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[3]");
	public static By lnkmyWineFourtProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[4]");
	public static By lnkmyWineFifthProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[5]");
	public static By lnkmyWineSixthProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[6]");
	public static By lnkmyWineSevenProdPrice = By.xpath("(//span[@class='userRatingsComp_date'])[7]");
	public static By imgmywinecolor = By.xpath("//span[@class='prodActionsIcons_myWine js-is-myWine']");
	public static By lnkmywineproducts = By.xpath("//span[@class='prodItemInfo_name']");
	public static By btnRemove = By.xpath("(//span[@class='iconRoundedOutline'])[1]");
			
	//Gift Products
	public static By lnkFirstGiftProduct = By.xpath("(//div[@class='prodItemStock']//button[text()='Add to Cart'])[1]");
	public static By lnksecondGiftProduct = By.xpath("(//div[@class='prodItemStock']//button[text()='Add to Cart'])[2]");
	
	public static By lnkBisinessGiftTab = By.xpath("(//a[text()='Wine Tasting Sets'])[3]");
	public static By lnkBirthdayGifts = By.xpath("(//a[text()='Birthday Gifts'])");
	public static By lnkThankyouGift = By.xpath("(//a[text()='Thank You Gifts'])");
	public static By lnkGiftBasket1 = By.xpath("(//a[text()='Wine & Food Gifts'])");
	public static By lnkGiftBasket2 = By.xpath("(//a[text()='Food & Chocolate Gifts'])");
	public static By lnkwineSet = By.xpath("(//a[text()='6 \u2013 12 Bottles'])");
	public static By lnkGiftCards = By.xpath("//a[text()='Gift Cards']");
	public static By lnkFilterMenuIcon = By.xpath("//div[@class='filterMenu_clearRefinements filterMenu_clearRefinements-showing']");
	public static By spnGiftSPriceFirstProduct = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/div[1]/span[1]/span[2]");
	public static By spnGiftSPriceSecondProduct = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/div[1]/span[1]/span[2]");
	public static By spnGiftSPriceThirdProduct = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/div[1]/span[1]/span[2]");
	public static By spnGiftSPriceFourthProduct = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/div[1]/span[1]/span[2]");
	public static By spnGiftSPriceFifthProduct = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/div[1]/span[1]/span[2]");
	
	public static By listPageFirstProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[1]");
	public static By listPageSecProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[2]");
	public static By listPageThirdProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[3]");
	public static By listPageFourthProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[4]");
	public static By listPageFifthProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[5]");
	public static By spnChooseYourState = By.xpath("//span[text()='Choose Your State ']");
					
	//Spirits
	public static By lnkSpirits = By.xpath("(//a[text()='Spirits'])[1]");
	public static By lnkWhiskey = By.xpath("//a[text()='Whiskey']");
	public static By lnkVodka = By.xpath("//a[text()='Vodka']");
	public static By lnkTequila = By.xpath("//a[text()='Tequila']");
	public static By lnkBrandy = By.xpath("//a[text()='Brandy & Cognac']");
	public static By lnkSpiritFeatured = By.xpath("//a[text()='Featured']");
	public static By lnkRegionScotland = By.xpath("//span[@class='filterMenu_itemName' and text()='Scotland']");
	public static By lnkRegionIreland = By.xpath("//span[@class='filterMenu_itemName' and text()='Ireland']");
	public static By lnkRegionOtherUS = By.xpath("//span[@class='filterMenu_itemName' and text()='Other U.S.']");
	public static By lnkRegionJalisco = By.xpath("//span[@class='filterMenu_itemName' and text()='Jalisco']");
	public static By lnkGiftBasketsFoodandChoco = By.xpath("//a[text()='Food & Chocolate']");
	public static By lnkGiftGlasswareandAccess = By.xpath("//a[text()='Glassware & Accessories']");
	public static By lnkGiftWineAccessoryGlassware = By.xpath("//a[text()='Wine Glassware']");
	public static By lnkGiftWineAccessoryDecanters = By.xpath("//a[text()='Decanters']");
	public static By lnkSpiritAccessories = By.xpath("//a[text()='Spirits Accessories']");
	public static By lnkCocktailRecipes = By.xpath("//a[text()='Cocktail Recipes']");
	public static By lnkrareSpirits = By.xpath("//a[text()='Rare Spirits']");
	
	public static By txtPreSaleProduct = By.xpath("//span[text()='Pre-sale:']");
	public static By lnkAllVintages = By.xpath("//a[@class='prodPedigree_allLink']");
	public static By imgcarouselContentItem = By.xpath("(//img[@itemprop='thumbnail'])[2]");
	public static By lnklightBoxCloseIcon = By.xpath("//i[@class='icon icon-plus lightBox_closeIcon']");
	public static By spnpipName = By.xpath("//h1[@class='pipName']");
	public static By selBottleSize = By.xpath("//input[@id='filter-Special-Designation']");
	public static By selStdBottleSize = By.xpath("//span[@class='filterMenu_itemName' and text()='Standard (750ml)']");
	public static By selHalfBottleSize = By.xpath("//span[@class='filterMenu_itemName' and text()='Half Bottles']");
	public static By lnkMoreOptions = By.xpath("//input[@class='filterWidgetExpander_input']");
	public static By selFeaturedProd = By.xpath("//input[@id='filter-Fine-Wine']");
	public static By SelFeaturedProdRare = By.xpath("//span[@class='filterMenu_itemName' and text()='Rare']");
	public static By SelFeaturedProdCollectible = By.xpath("//span[@class='filterMenu_itemName' and text()='Collectible']");
	public static By lnkSpiritCollectible = By.xpath("(//a[@class='mainNavList_itemLink ' and text()='Collectible'])[2]");
	public static By lnkSpiritRareSpirits = By.xpath("(//a[@class='mainNavList_itemLink ' and text()='Rare Spirits'])");

	public static By lnkPreSaleProduct = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[2]");
	public static By lnkSearchTypeListWill = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[3]");
	public static By lnkpreSelFirstProdct= By.xpath("(//div[@class='prodItemInfo_stock'][div[div[span[text()='Pre-sale:']]]]/div[2]/button[@class='prodItemStock_addCart'])[1]");
	public static By lnkpreSelSecondProdct= By.xpath("(//div[@class='prodItemInfo_stock'][div[div[span[text()='Pre-sale:']]]]/div[2]/button[@class='prodItemStock_addCart'])[2]");
	public static By lnkpreSelThirdProdct= By.xpath("(//div[@class='prodItemInfo_stock'][div[div[span[text()='Pre-sale:']]]]/div[2]/button[@class='prodItemStock_addCart'])[3]");
	public static By lnkpreSelFourthProdct= By.xpath("(//div[@class='prodItemInfo_stock'][div[div[span[text()='Pre-sale:']]]]/div[2]/button[@class='prodItemStock_addCart'])[4]");
	public static By lnkpreSelFifthProdct= By.xpath("(//div[@class='prodItemInfo_stock'][div[div[span[text()='Pre-sale:']]]]/div[2]/button[@class='prodItemStock_addCart'])[5]");
	
	public static By lnkfirstListProd= By.xpath("(//span[@class='prodItemInfo_name'])[1]");
	public static By lnkthirdListProd= By.xpath("(//span[@class='prodItemInfo_name'])[3]");
	public static By lnkAddToMyWineFunc= By.xpath("//span[@class='prodActionsIcons_myWineIconText']");
	public static By btnCocktailFirstprodtoCart= By.xpath("(//a/img)[1]");
	public static By btnCocktailSecondprodtoCart= By.xpath("(//a/img)[2]");
	public static By btnCocktailThirdprodtoCart= By.xpath("(//a/img)[3]");
	public static By btnCocktailFourthprodtoCart= By.xpath("(//a/img)[4]");
	public static By spncurrentlyUnavailableProduct= By.xpath("(//div[@class='prodItemInfo']//div[@class='productUnavailable'])[2]");
	public static By spncurrentlyUnavailableProductName = By.xpath("(//div[@class='prodItemInfo']/a)[2]");
	public static By spncurrentlyUnavailableProductInPIP = By.xpath("//span[@class='prodItemStock_soldOut']");
	public static By lnkrecommendedProductsList = By.xpath("(//div[@class='prodRec_scroller'])[2]");
	public static By lnkrecommendedProdRightArrow = By.xpath("(//div[@class='prodRecControls_next']//i[@class='prodRecControls_arrows icon icon-arrow-right'])[1]");
	public static By lnkrecommendedProdLeftArrow = By.xpath("(//i[@class='prodRecControls_arrows icon icon-arrow-left'])[1]");
	public static By lnkrecommendedProductsFirstAddToCart = By.xpath("((//div[@class='prodRec_scroller'])[2]//div//div[@class='scrollerList_content']/span[text()='Add to Cart'])[1]");
	public static By lnkrecommendedProductsSecondAddToCart = By.xpath("((//div[@class='prodRec_scroller'])[2]//div//div[@class='scrollerList_content']/span[text()='Add to Cart'])[2]");
	public static By lnkrecommendedProductsThirdAddToCart = By.xpath("((//div[@class='prodRec_scroller'])[2]//div//div[@class='scrollerList_content']/span[text()='Add to Cart'])[3]");
	public static By lnknextPageRecommendedProducts = By.xpath("(//h2[@class='prodRec_headline'])[2]//following-sibling::div/div/ul/li[5]");
	
	//Hero Image
	public static By lnkheroImagePlusSymbol= By.xpath("//section[@id='learnAboutContent']/div[@class='listPageContentContainerWrap']//button[@class='listPageContent_expand']/a[@href='#learnAboutContent']");
	public static By promoBarWashingtonReg= By.xpath("//section[@class='listPageContent_bodyText']/p");
	public static By ChkShowOutOfStock = By.xpath("(//span[@class='checkStock_checkboxSpan'])[1]");

	//Footer Links
	public static By lnkblogLink = By.xpath("//a[@href='http://blog.wine.com/?iid=Footer:Link:Blog']");
	public static By lnkfaceBookLink = By.xpath("//a[@href='https://www.facebook.com/winecom']");
	public static By lnktwitterLink = By.xpath("//a[@href='https://twitter.com/wine_com/']");
	public static By lnkpinterestLink = By.xpath("//a[@href='https://www.pinterest.com/winecom/']");
	public static By lnkinstagramLink = By.xpath("//a[@href='https://www.instagram.com/wine_com/']");
	public static By lnkCorperateGifts = By.xpath("//div[@class='pageFooter_contentLinks'][2]/ul/li[4]");
	
	public static By titleContactUs = By.xpath("//div[@class='title' and contains(text(),'Contact Us')]");
	public static By lnkcustomerCareLink = By.xpath("//div[@class='pageFooter_contentLinks']/h3");
	public static By lnkcustomrCareHeader = By.xpath("(//a[text()='Customer Care'])[1]");
	public static By lnkcustomrCare = By.xpath("//h3[text()='Customer Care']");
	public static By lnkcontactUs = By.xpath("(//a[text()='Contact Us'])[2]");
	public static By linkcontactUs = By.xpath("//a[text()='Contact Us']");
	public static By lnktrackanOrder = By.xpath("//a[text()='Track an Order']");
	public static By lnkemlPreferences = By.xpath("(//a[text()='Email Preferences'])[3]");
	public static By lnkaboutWineCom = By.xpath("//h3[text()='About Wine.com']");
	public static By lnkaboutUs = By.xpath("//a[text()='About Us']");
	public static By lnkcareers = By.xpath("//a[text()='Careers']");
	public static By lnkpress = By.xpath("//a[text()='Press']");
	public static By lnkhowToWork = By.xpath("//h3[text()='How to Work With Us']");
	public static By lnksellUs = By.xpath("//a[text()='Sell Us Your Wine']");
	public static By lnkAffilaiteProgram = By.xpath("//a[text()='Affiliate Program']");
	public static By lnkpartnership = By.xpath("//a[text()='Partnerships']");
	public static By lnkgooglePaly = By.xpath("(//img[@src='/static/images/Google_play140x45.png'])");
	public static By lnkgetWineComApp = By.xpath("//h3[text()='Get the Wine.com App']");
	public static By lnkenterMobileNumberLink = By.xpath("//form[@name='sendSMSgetAppForm']/div/input");
	public static By linkEmailPref = By.xpath("(//a[text()='Email Preferences'])[4]");
	public static By titleTrackonOrder = By.xpath("//h1[text()='Your Account']");
	public static By titleAboutUs = By.xpath("//h1[text()='Why Shop with Wine.com?']");
	public static By titleCareers = By.xpath("//h1[text()='Attention Wine Lovers']");
	public static By titlePress = By.xpath("//h1[text()='In the News']");
	public static By titleSellUsYourWine = By.xpath("//h1[text()='Sell Us Your Wine']");
	public static By titleAffiliatedProgram = By.xpath("//h1[text()='Affiliate Program']");
	public static By labCopyRights = By.xpath("//footer//*[text()=' Wine.com, LLC. All rights reserved.']");
	
	
	
}
