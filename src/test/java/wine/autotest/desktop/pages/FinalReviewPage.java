package wine.autotest.desktop.pages;

import org.openqa.selenium.By;

public class FinalReviewPage {

	public static By imgWineLogo = By.xpath("//main[@class='coreMain wineMain']//a[@class='wineLogo']");
	public static By spnFinalReviewPage = By.xpath("//div[text()='Final Review']");
	//Address
	public static By rdoShipToHome = By.xpath("(//fieldset[@class='formWrap_group shippingForm_shippingType']/label)[1]");
	public static By rdoShipToFedEx = By.xpath("(//main//fieldset[@class='formWrap_group shippingForm_shippingType']//span[@class='formWrap_radioSpan'])[2]");
	public static By txtFedexZipCode = By.xpath("//input[@class='formWrap_input checkoutForm_input shipToFedEx_zipInput']");
	public static By btnFedexSearch = By.xpath("//button[text()='Search']");
	public static By btnShipToThisLocation = By.xpath("(//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded '])[1]");
	public static By rdopickupFormHeader = By.xpath("//h2[text()='Pickup Information']");
	public static By txtFirstName = By.xpath("//input[@name='firstName']");
	public static By txtLastName = By.xpath("//input[@name='lastName']");
	public static By txtStreetAddress = By.xpath("//input[@name='address1' and @placeholder='Street Address']");
	public static By txtCity = By.xpath("//fieldset[@class='formWrap_group formWrap_group-half']//input[@name='city' and @placeholder='City']");
	public static By dwnState = By.xpath("//select[@class='state_select' and @name='shipToState']");
	public static By dwnReceiveState = By.xpath("(//select[@class='state_select'])[1]");
	public static By txtZipCode = By.xpath("(//input[@placeholder='Zip'])[3]");
	public static By txtRecipientZipCode = By.xpath("//fieldset[@class='formWrap_group formWrap_group-quarter float-right']//input[@placeholder='Zip']");
	public static By txtPhoneNum = By.xpath("//input[@placeholder='Phone # (mobile preferred)']");
	public static By btnShipContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn recipientAction_continue']");
	public static By btnVerifyContinue = By.xpath("(//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn'])");
	public static By btnDeliveryContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn cartButton delivery_actionText']");
	public static By spnlocalPickUp = By.xpath("//span[text()='Find nearby Local Pickup locations']");
	public static By lnkEditShippingAddress = By.xpath("(//div[@class='shippingAddress_container']//a[@class='userDataCard_editBtn'])[1]");
	public static By spnShippingHeaderMsg = By.xpath("//h2[text()='Edit Shipping Address']");
	public static By RdoPrefferedAddress2 = By.xpath("(//input[@class='formWrap_radio shippingPreferredAddress_input checkoutForm_radio'])[2]");
	public static By lnkSecondAddrEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[2]");
	public static By lnkThirdAddrEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[3]");
	public static By ChkPreferredAddressBook = By.xpath("(//span[@class='formWrap_checkboxSpan checkoutForm_checkboxSpan'])[2]");
	public static By btnPreferredSave = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn'])[2]	");	
	public static By btnaddressContinue = By.xpath("//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn']");
	public static By RdoShiptoAddress = By.xpath("(//input[@class='formWrap_radio checkoutForm_radio shippingSelectedAddress_input'])[1]");
	public static By RdoPrefferedAddress = By.xpath("(//input[@class='formWrap_radio shippingPreferredAddress_input checkoutForm_radio'])[1]	");	
	public static By lnkShipToThisAddress = By.xpath("/fieldset/ul/li/div[4]/label/span/span");
	public static By lnkPaymentCount = By.xpath("//span[@class='paymentLinks_count']");
	public static By spnPreferredAddress = By.xpath("(//div[@class='userDataCardPreferences'])[1]/label/span[@class='shippingPreferredAddress_group']");
	public static By spnMakePreferred = By.xpath("//span[text()='Make Preferred']");
	public static By spnShippingAddressActualName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[1]");
	public static By spnshippingAddressExpectedName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[2]");
	public static By spnshippingSelectedAddress = By.xpath("(//span[@class='shippingSelectedAddress_group'])[2]");
	
	public static By spnmustSelectShippingAddress = By.xpath("//p[text()='Must select address before proceeding']");
	public static By lnkSearchTypeList = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[6]");
	public static By btnmodalWindowCancel = By.xpath("//div[@class='modalWindow_cancel icon icon-menu-close']");
	public static By spnnameRequiredInRecipientPage = By.xpath("//p[text()='Name is required.']");
	public static By spnstateRequiredInRecipientPage = By.xpath("//p[text()='State is required.']");
	public static By spnzipRequiredInRecipientPage = By.xpath("//p[text()='Zip is required.']");
	public static By lnkshippingAddressCountInRcp = By.xpath("//span[@class='shippingAddressLinks_count']");
	public static By lnkshippingAddressEdt = By.xpath("(//div[@class='userDataCard_actionBtns'])[1]");
	public static By txtshippingAddressFullName = By.xpath("//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");
	public static By txtshippingAddressFullNameEnter = By.xpath("(//input[@class='formWrap_input checkoutForm_input' and @placeholder='Full Name'])[2]");
	public static By txtshippingAddressStreetAdrsClear = By.xpath("(//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group']/label/input[@name='address1'])[2]");
	public static By txtshippingAddressStreetAdrsEnter = By.xpath("(//input[@class='formWrap_input checkoutForm_input' and @placeholder='Street Address'])[2]");
	public static By txtshippingAddressCityClear = By.xpath("(//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group formWrap_group-half']/label/input[@name='city'])[2]");
	public static By txtshippingAddressCityEnter = By.xpath("(//input[@class='formWrap_input checkoutForm_input' and @placeholder='City'])[3]");
	public static By txtshippingAddressSuiteEnter = By.xpath("(//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group']/label/input)[1]");
	public static By btnshippingAddressSaveBtn = By.xpath("//div[@class='modalWindow']/div[2]//form/div[2]/div/button");
	public static By spnshippingAddressFullNameBeforeEdit = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[2]");
	public static By spnshippingAddressFullNameAfterEdit = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[1]");
	public static By lnkshippingOptionalClk = By.xpath("(//fieldset[@class='formWrap_group'])[1]");			
	public static By lnkshippingAddressEdtRemove = By.xpath("(//a[text()='Edit'])[4]");	
	public static By lnkremoveThisAddress = By.xpath("//div[@class='shippingRecipient_btnBar shippingRecipient_saveSection']//a");	
	public static By lnkYesremoveThisAddress = By.xpath("//button[text()='Yes, remove']");	
	public static By chkshipAdrPreferredCard = By.xpath("(//form[@class='shippingRecipientSection_form']//fieldset[@class='formWrap_group stateSelect_checkboxGroup shippingRecipient_makePreferred']/label[@class='formWrap_label formWrap_checkboxLabel'])[2]");
	public static By btnshipAdrSave = By.xpath("(//form[@action='/account/shipping'])[2]/div[2]/div/button");	
	public static By spnshipAdrExpFullName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[1]");	
	public static By btnshipTothisAdrRadion = By.xpath("(//input[@class='formWrap_radio checkoutForm_radio shippingSelectedAddress_input'])[1]");	
	public static By chkpreferredCardSelect = By.xpath("(//input[@class='formWrap_radio billingPreferredAddress_input checkoutForm_radio'])[1]");	
	public static By creditCardHolderNameBeofreSelPay = By.xpath("(//div[@class='userDataCard_headline storedPayment_name'])[2]");	
	public static By selectPayWithThisCard = By.xpath("(//span[@class='billingSelectedAddress_group'])[2]");
	public static By spneditCardModal = By.xpath("//div[@class='modalWindow_header']");
	public static By spnSuggestedAddressR = By.xpath("(//label[@class='userDataCardPreferences_group verifyAddressPreferences_group userDataCardPreferences_group-left'])[1]");
	public static By lnkaddressCountInRecipient = By.xpath("//li[contains(@class,'userDataCard_listItem shippingAddress_listItem')]");
	public static By lnkcreditCardCountInRecipient = By.xpath("//li[contains(@class,'userDataCard_listItem storedPayment_listItem')]");
	public static By lnkviewAllAllAddress = By.xpath("//a[@class='shippingAddressLinks_link shippingAddressLinks_link-right shippingAddress_viewAll']");
	public static By lnkviewAllPaymentMethods = By.xpath("//a[@class='paymentLinks_link paymentLinks_link-right paymentLinks_viewAll']");
	public static By lnkmodalWindowIcon = By.xpath("//div[@class='js-closeModal modalWindow_cancel icon icon-menu-close']");
	public static By spnEditShipAddName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name' and text()='QA User Automation'])[1]");
	public static By dwnrecipientState = By.xpath("(//form[@class='shippingRecipientSection_form']//select[@class='state_select'])[2]");
	
	//FedEx Address Addition
	public static By txtFedexFirstName = By.xpath("//div[@class='modalWindow_content']//input[@name='firstName']");
	public static By txtFedexLastName = By.xpath("//div[@class='modalWindow_content']//input[@name='lastName']");
	public static By txtFedexStreetAddress = By.xpath("//div[@class='modalWindow_content']//input[@placeholder='Street Address']");
	public static By txtFedexCity = By.xpath("//div[@class='modalWindow_content']//input[@name='city']");
	public static By txtFedexZip = By.xpath("//div[@class='modalWindow_content']//input[@name='zip']");
	public static By txtFedexPhoneNum = By.xpath("//div[@class='modalWindow_content']//input[@name='phone']");
	public static By btnFedexSave = By.xpath("//div[@class='modalWindow_content']//button[text()='Save']");
	public static By btnFedexContinue = By.xpath("(//button[text()='Continue'])[1]");
	public static By spnfedexLocalPickUpIsNotAvailable = By.xpath("//*[text()='No locations found for this zip code']");
	public static By btngoBack = By.xpath("//span[text()='Go back']");
	public static By VerifyFindPickupLoactionear = By.xpath("//div[@class='localPickupContainer_headerContent']/h2");
	public static By txtFedexBillingAddrss = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']/fieldset/label/input)[1]");
	public static By txtFedexBillingCity = By.xpath("//fieldset[@class='formWrap_group paymentForm_cityWrap formWrap_group-half']/label/input");
	public static By dwnFedexBillingState = By.xpath("(//fieldset[@class='stateSelect formWrap_group formWrap_group-quarter']/label/select)[2]");
	public static By txtFedexBillingZip = By.xpath("//fieldset[@class='paymentForm_zipCodeWrap formWrap_group formWrap_group-quarter float-right']/label/input");
	public static By txtFedexBillingPhoneNum = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']/fieldset/label/input)[5]");
	public static By btnStateChangeCancel = By.xpath("//div[@class='cartTransfer_link cartTransfer_stayInState' and contains(text(),'Cancel')]");
	public static By lnkaddGiftMessage = By.xpath("//a[@class='checkoutHeader_giftNotSetLink']");
	public static By txtgiftRecipientEmail = By.xpath("//fieldset[@class='formWrap_group checkoutForm_textareaGroup']/input");
	public static By txtgiftMessageTextArea = By.xpath("//fieldset[@class='formWrap_group checkoutForm']/textarea");
	public static By btnRecipientContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn recipientAction_continue']");
	public static By spnCartHeadLineCount = By.xpath("//span[@class='cart_headline-count']");
	public static By txtFedExAddEditName = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input[@name='name']");
	public static By txtexpectedEditedName = By.xpath("//div[@class='userDataCard_headline shippingAddress_name js-is-fedEx']");
	public static By spnzipCodeErrorMsg = By.xpath("//p[text()='Zip is not in selected state.']");
	public static By spnFedEXShippingDeliveryMessage = By.xpath("//span[@class='deliveryHeader_disclaimer-instructions']");
	public static By imgloactionIcon = By.xpath("//div[@class='localPickupContainer_locations']/ol/li//span[@class='listItem_marker']");
	public static By spnactAddrOnList = By.xpath("//div[@class='description']/div[1]");
	public static By spnactAddrOnMap = By.xpath("//div[@class='popupDetails_description']/div[1]");
	public static By lnkshipToThisLocation = By.xpath("(//button[text()='Ship to this location'])[1]");
	
	//Shipping method
	public static By lnkChangeDate = By.xpath("//span[text()='Change Date']");
	public static By txtShippingMethod = By.xpath("//div[@class='shippingMethod_overlay']/div[1]");
	public static By lnkoverNightAM = By.xpath("//span[@class='orderItem_shippingMethodValue']");
	public static By lnkoverNightPM = By.xpath("//span[@class='orderItem_shippingMethodValue']");
	public static By lnkStandardShip = By.xpath("//span[@class='shippingDetailsMethod_name']");
	public static By dwnSelectShippingMethod = By.xpath("//select[@name='shippingMethod']");
	public static By dwnSelShipMethodOverNightPM = By.xpath("//select[@name='shippingMethod']/option[3]");
	public static By dwnSelShipMethodOverNightAM = By.xpath("//select[@name='shippingMethod']/option[4]");
	public static By dwnSelShipMethodOption2Day = By.xpath("//select[@name='shippingMethod']/option[2]");
	public static By SelectDateSaturday = By.xpath("(//div[@class='calendarWidgetMonth']/span[@class='calendarWidget_day date-premium date-currentMonth'][1])[1]");
	public static By SelectSecondSaturday = By.xpath("(//div[@class='calendarWidgetMonth']/span[@class='calendarWidget_day date-premium date-currentMonth'][2])[1]");
	public static By SelectThirdSaturday = By.xpath("(//div[@class='calendarWidgetMonth']/span[@class='calendarWidget_day date-selected date-premium date-currentMonth'][21])[1]");
	public static By SelectForthSaturday = By.xpath("(//div[@class='calendarWidgetMonth']/span[@class='calendarWidget_day date-selected date-premium date-currentMonth'][28])[1]");
	public static By joinStewardshipJust = By.xpath("//div[@class='stewardShipContent stewardShipContent-over']");
	public static By spnstewardShipDifferenceAmount = By.xpath("//span[@class='stewardShipSection_difference']");
	public static By btnstateContinueButton = By.xpath("(//a[contains(text(),'Continue')])[3]");
	public static By btnsaveCreditCard = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup paymentForm_saveCard']/label");
	public static By spnshippingmethodPmtext = By.xpath("//select[@class='shippingMethod_select']/option[3]");
	public static By spnshippingmethodAmtext = By.xpath("//select[@class='shippingMethod_select']/option[4]");
	public static By chkAcceptTermsandConditions= By.xpath("//label[@class='formWrap_label formWrap_checkboxLabel']/input[@class='formWrap_checkbox checkoutForm_checkbox pickedTerms_checkbox']");
	public static By spnDeliveryViaEmail= By.xpath("//p[text()='Delivered via email']");
	
	//RecipientEdit
	public static By lnkRecipientShipToaddress = By.xpath("(//span[@class='shippingSelectedAddress_group'])[1]");
	public static By lnkAddNewAddress = By.xpath("//a[text()='Add a new address']");
	public static By lnkChangeAddress = By.xpath("//a[@class='checkoutHeader_editBtn']/span[text()='Change Address']");
	public static By lnkRecipientEdit = By.xpath("//section[@class='refinementWidgetSection']/div[1]/div[3]/div[5]");
//	public static By lnkshippingAddressEdit = By.xpath("(//a[text()='Edit'])[2]");
	public static By lnkshippingAddressEdit = By.xpath("//li[@class='userDataCard_listItem shippingAddress_listItem js-is-selected']//a");
	public static By dwnshipState = By.xpath("(//select[@name='shipToState'])[5]");
	public static By txtRecipientEditZipCode = By.xpath("//span[text()='Street Address']/following::input[1]");
	public static By btnshippingAddressSave = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn' and text()='Save'])[2]");
	public static By btnContinuetostate = By.xpath("//a[@class='btn btn-large btn-rounded btn-red btn-processing cartTransfer_saveBtn']");
	public static By btnReceipEditCvv = By.xpath("//fieldset[contains(@class,'paymentForm_cvvFieldset isInvalid')]//label//input[@id='expirationCvv']");
	public static By rdoShiptoaddressOne = By.xpath("(//span[@class='formWrap_radioSpan'])[1]");
	public static By rdoUseShipaddresstwo = By.xpath("(//input[@name='shippingAddress' ])[5]");
	public static By txtReceipientEmail = By.xpath("//fieldset[@class='formWrap_group checkoutForm_textareaGroup']//input[@class='formWrap_input checkoutForm_input giftEmailInput']");
	public static By lnkStayInPreviousState = By.xpath("//div[@class='cartTransfer_link cartTransfer_stayInState']");
	public static By lnkRecipientHeader = By.xpath("(//div[@class='checkoutHeader'])[1]");
	public static By spnRecipientaddressFormHeaderr = By.xpath("//header[@class='checkoutForm_headline checkoutForm_adultRequired']");
	public static By rdoOriginalAddress = By.xpath("(//span[@class='shippingSelectedAddress_group'])[2]/input");
	public static By txthomeAddEditStreetAddress = By.xpath("(//fieldset[@class='formWrap_section'])[5]/fieldset/label/input[@id='js-google-autocomplete']");
	public static By txtPhoneNumAddress = By.xpath("(//fieldset[@class='formWrap_group'])[13]/label/input[@name='phone']");
	public static By btnhomeAddressSave = By.xpath("//div[@class='shippingRecipient_btnContainer']/button");
	public static By spnexpectedEditedAddress = By.xpath("(//div[@class='shippingAddress_addressOne'])[1]");
	public static By spngiftMessageText = By.xpath("//p[@class='checkoutHeader_giftMessageSummary']/span");
	public static By spngiftMessageSummary = By.xpath("//p[@class='checkoutHeader_giftMessageSummary']");
	public static By lnkSugestedAddressDisply = By.xpath("//li[@class='userDataCard_listItem shippingAddress_listItem js-is-selected']");	
	public static By btnLocalPickupAddressselect= By.xpath("(//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded '])[4]");
	public static By spnStewardshipSaving= By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
	
	//Gift Section
	public static By lnkNogiftselected = By.xpath("//a[@class='checkoutHeader_giftNotSetLink']");
	public static By rdogftBagTot = By.xpath("(//div[@class='giftWrapping']/span[@class='giftWrapping_price'])");
	public static By btnViewCart = By.xpath("//button[@class='finalReview_toggleProductDisplay' and text()='View Cart']");	
	public static By rdogiftbag3_99 = By.xpath("(//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])[1]");
	public static By rdogiftbag6_99 = By.xpath("(//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])[2]");
	public static By rdogiftbag9_99 = By.xpath("(//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])[3]");
	public static By radNogiftBag = By.xpath("(//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])[4]");
	public static By chkRecipientGift = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");
	public static By giftWrappingSection = By.xpath("//span[@class='giftWrapping_total']");
	public static By spnGiftBag = By.xpath("(//div[@class='productPrice_price-reg'])[1]");
	public static By GiftWrapOptionCount = By.xpath("//span[@class='giftWrapOption_addCountText']");
	public static By txtInvalidEmailAddInGiftRecipPage = By.xpath("//div[@class='checkout_errorMessage']/p[text()='Invalid email address.']");
	public static By chkRecipientGiftSel = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input");
	public static By RecipientGiftCheckboxLabel  = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");
	public static By spnRecipientGiftAdd  = By.xpath("(//span[@class='giftWrapOption_group'])[1]");
	public static By dwnmaximumCharacter = By.xpath("//fieldset[@class='formWrap_group checkoutForm']/span/span[text()='240']");
	public static By dwnlimitCharacter = By.xpath("//fieldset[@class='formWrap_group checkoutForm']/span/span[text()='0']");
	public static By lnkprodItemgiftIcon = By.xpath("//div[@class='prodItem_wrap js-has-giftWrap']/i[@class='icon icon-gift prodItem_giftIcon']");
	public static By lnkgiftWrapping  = By.xpath("//div[@class='giftWrapping']");
	public static By rdogiftbag2  = By.xpath("(//div[@class='productPrice_price-reg'])[2]");
	public static By rdogiftbag3  = By.xpath("(//div[@class='productPrice_price-reg'])[3]");
	public static By spngiftBagNames1  = By.xpath("//span[@class='giftWrapOption_header'][1]");
	public static By spngiftBagNames2  = By.xpath("(//span[@class='giftWrapOption_header'])[2]");
	public static By spngiftBagNames3  = By.xpath("(//span[@class='giftWrapOption_header'])[3]");
	public static By rdonoGiftBag  = By.xpath("(//span[@class='giftWrapOption_radioBagText-eachOrNone'])[4]");
	public static By txtRecipientGiftMessage  = By.xpath("//fieldset[@class='formWrap_group checkoutForm']//textarea[@class='formWrap_textarea giftMessageBox']");
	public static By lnkrecipienteditR  = By.xpath("//a[text()='Edit'][1]");
	
	public static By labstewardShipSection  = By.xpath("(//span[@class='stewardShipSection_headline-highlight'])[1]");
	
	//Delivery
	public static By spnestimatedArrival  = By.xpath("//p[text()='Estimated Arrival']");
	public static By spndeliveryHeaderDate  = By.xpath("//p[@class='deliveryHeader_date']");
	public static By spnallPreSaleItemsDetails  = By.xpath("//p[text()='All Pre-Sale items, including shipping & handling and applicable taxes, will be billed when the order is placed.']");
	public static By spncancellingThePreSale  = By.xpath("//p[text()='Canceling this Pre-sale order is subject to a 20% cancellation fee, with the remaining balance applied as a credit in your Wine.com account.']");

	public static By lnkcalenderFirstMnth  = By.xpath("(//div[@class='calendarHeader'])[1]");
	public static By lnkcalenderNxtMnth  = By.xpath("(//div[@class='calendarHeader'])[2]");
	public static By lnkpreviousMnthIcon  = By.xpath("(//i[@class='calendarHeader_iconRight icon icon-arrow-right'])[1]");
	public static By lnkcurrentMnthIcon  = By.xpath("(//i[@class='calendarHeader_iconRight icon icon-arrow-right'])[1]");
	public static By lnknxtMnthIcon  = By.xpath("(//button[@class='calendarHeader_iconRight icon icon-arrow-right'])[2]");
	public static By lnkDisableddates  = By.xpath("//span[@tabindex='-1']");
	public static By spncurrentDate  = By.xpath("//span[@class='calendarWidget_day date-selected date-currentMonth']");
	public static By spnunavailableDate = By.xpath("//span[@class='calendarWidget_day date-selected date-currentMonth']/preceding-sibling::span[1]");
	public static By spnnextAvailableDate = By.xpath("//span[@class='calendarWidget_day date-selected date-currentMonth']/following-sibling::span[1]");
	public static By imgpickupLocIcon = By.xpath("//section[@class='checkoutMainContent']/div[1]/section/div/div/div/div/i");
	
	//Payments
	public static By lnkchangePayment = By.xpath("//span[text()='Change Payment']");
	public static By lnkAddPymtMethod = By.xpath("//a[text()='Add payment method']");
	public static By lnkAddNewCard = By.xpath("//a[text()='Add a new card']");
	public static By txtNameOnCard = By.xpath("//input[@placeholder='Name On Card']");
	public static By txtCardNumber = By.xpath("//input[@id='cardNumber']");
	public static By txtExpiryMonth = By.xpath("//input[@id='expirationMonth']");
	public static By txtExpiryYear = By.xpath("//input[@id='expirationYear']");
	public static By txtCVV = By.xpath("//label//input[@id='expirationCvv']");
	public static By chkBillingAndShipping = By.xpath("//label[@class='formWrap_label formWrap_checkboxLabel formWrap_label_sameAsShipping']/span");
	public static By txtbirthMonth = By.xpath("//input[@name='monthOfBirth']");
	public static By txtbirthDate = By.xpath("//input[@name='dayOfBirth']");
	public static By txtbirthYear = By.xpath("//input[@name='yearOfBirth']");
	public static By btnPaymentContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn cartButton payment_actionText']");
	public static By lnkPaymentEdit = By.xpath("//li[@class='userDataCard_listItem storedPayment_listItem js-is-selected']//a");
	public static By txtCVVR = By.xpath("(//input[@id='expirationCvv'])[2]");
	public static By txtDOBFormat = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-month']/following-sibling::fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-day']/following-sibling::fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-year']");
	public static By btnpaywithThisCardSave = By.xpath("//div[@class='paymentForm_btnContainer']/button[text()='Save']");
	public static By chkCreditCard = By.xpath("(//label[@class='formWrap_label formWrap_checkboxLabel'])[2]");
	public static By lnkPaymentCheckoutEdit = By.xpath("(//a[@href='/checkout/payment/list'])[1]");
	public static By txtPaymentCVV = By.xpath("(//input[@name='cvv'])[2]");
	public static By btnPaymentSave = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])[2]");
	public static By txtPymtCVV = By.xpath("(//input[@name='cvv'])[1]");	
	public static By btnPymtSave = By.xpath("(//button[@class='btn btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])[1]");
	public static By lnkpaywithThisCardEdit = By.xpath("//li[@class='userDataCard_listItem storedPayment_listItem js-is-selected']/div[1]/div[2]/a");
	public static By dwnbillingAdressState = By.xpath("//div[@class='modalWindowWrap']//select[@name='stateOrProvince']");
	public static By txtbillingAdressZipCode = By.xpath("//div[@class='modalWindowWrap']//input[@name='zipOrPostalCode' and @placeholder='Zip']");
	
	public static By spnnameRequiredErrorMsg = By.xpath("//p[text()='Name On Card is required.']");
	public static By spncreditCardRequiredErrorMsg = By.xpath("//p[text()='Credit Card # is required.']");
	public static By spnexpirtDateRequiredErrorMsg = By.xpath("//p[text()='Expiration date is required.']");
	public static By spncvvRequiredErrorMsg = By.xpath("//p[text()='CVV is required.']");
	public static By spnpleaseSpeciftCardProvider = By.xpath("//div[@class='checkout_errorMessage']/p");
	public static By spnstreetAddressRequired = By.xpath("//p[text()='Street Address is required.']");
	public static By spncityRequired = By.xpath("//p[text()='City is required.']");
	public static By spnstateAddressRequired = By.xpath("//p[text()='State/Province is required.']");
	public static By spnZipRequired = By.xpath("//p[text()='Zip/Postal Code is required.']");
	public static By spnphoneRequired = By.xpath("//p[text()='Phone # is required.']");
	public static By spnPhoneNumberReq = By.xpath("//p[text()='Phone number is required.']");
	public static By rdopreferredAddress = By.xpath("//input[@class='formWrap_radio billingPreferredAddress_input checkoutForm_radio']");
	public static By lnkeditCreditCardInPayment = By.xpath("(//a[@class='userDataCard_editBtn'])[2]");
	public static By lnkremvePhoneNumInPayment = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='formWrap_section paymentForm_paymentAddress']//fieldset[@class='formWrap_group']/label/input[@name='phone']");
	public static By lnkremoveCreditCardInPayment = By.xpath("//div[@class='modalWindow_content']//a[@class='paymentForm_removeBtn']");
	public static By btnYesRemove = By.xpath("//button[text()='Yes, remove']");
	public static By spncreditCardWarningMsgInPayment = By.xpath("//span[text()='Please provide a valid credit card']");
			
	public static By txtpaywithThisCardCvv = By.xpath("//div[@class='modalWindowWrap']//input[@id='expirationCvv']");
	public static By spnnameAfterSelPayWithCard = By.xpath("(//div[@class='userDataCard_headline storedPayment_name'])[1]");
	public static By chkpayWithThisCardSel = By.xpath("(//input[@class='formWrap_radio checkoutForm_radio billingSelectedAddress_input'])[1]");
	public static By txtGiftCrdAmt = By.xpath("//span[@class='storedPayment_original']");
	public static By txtGiftCrdRem = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_giftCardRemaining']");
	
	//Order Summary
	public static By spnSubtotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_subTotalPrice']");
	public static By spnShippingHnadling = By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");
	public static By spnTotalBeforeTax = By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']");
	public static By spnOrderSummaryTaxTotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_taxTotal']");
	public static By spnstewardShipSavePrice = By.xpath("//span[@class='stewardShipSection_savings']");
	public static By btnPlaceOrder = By.xpath("//button[text()='Place Order']");
	public static By btnclosePopUp = By.xpath("//div[@class='extole-js-widget-wrapper extole-widget-wrapper']//a[@class='extole-js-widget-close extole-controls extole-controls--close']");
	
	//Cart Edit
	public static By btnCartEdit = By.xpath("//a[@class='cart_editBtn']");
	public static By lnkeditCreditCard = By.xpath("(//a[text()='Edit'])[8]");
	public static By btnremoveThisCard = By.xpath("//div[@class='modalWindow_content']/section[@class='paymentFormSection']/form[@action='/account/payments']//a");
	public static By btnyesRemove = By.xpath("//button[text()='Yes, remove']");
	
	//Billing address
	public static By txtNewBillingAddress = By.xpath("(//form[@class='paymentForm_form']//fieldset[@class='formWrap_section paymentForm_paymentAddress']/fieldset/label/input)[1]");
	public static By txtNewBillingSuite = By.xpath("(//input[@class='formWrap_input checkoutForm_input'])[13]");
	public static By txtNewBillingCity = By.xpath("//form[@class='paymentForm_form']/fieldset[@class='formWrap_section paymentForm_paymentAddress']//fieldset/label/input[@name='city']");
	public static By dwnBillinState = By.xpath("//form[@class='paymentForm_form']//select[@class='state_select']");
	public static By txtNewBillingZip = By.xpath("(//input[@placeholder='Zip'])[2]");
	public static By txtNewBillingPhone = By.xpath("(//form[@class='paymentForm_form']//fieldset[@class='formWrap_section paymentForm_paymentAddress']/fieldset/label/input)[5]");
	public static By rdomakeThisPreferredCard = By.xpath("//fieldset[@class='formWrap_group stateSelect_checkboxGroup paymentForm_makePreferred']/label");			
	public static By btnShipCont= By.xpath("(//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn recipientAction_continue'])[2]");
	public static By txtZip= By.xpath("(//input[@class='formWrap_input checkoutForm_input' and @name='zip'])[1]");
	public static By txtAdultSignature= By.xpath("//strong[text()=\"Adult signature required (Age 21+) for delivery!\"]");
	public static By txtRemainingGiftCard1= By.xpath("(//label[@class='userDataCardPreferences_group userDataCardPreferences_group-left storedPayment_applyGiftCard'])[1]");
	public static By txtRemainingGiftCard2= By.xpath("(//label[@class='userDataCardPreferences_group userDataCardPreferences_group-left storedPayment_applyGiftCard'])[2]");

	public static By txtarivalMessage= By.xpath("//span[text()=\"Once your order arrives, you will have 5 days to pick it up.\"]");
	public static By txtOrderArrival= By.xpath("//p[contains(text(),'when your order arrives and have 5 days to pick it up')]");
	public static By chkTermsConditio= By.xpath("(//span[@class='formWrap_checkboxSpan'])[4]");
	public static By lnkTermsandCond= By.xpath("//a[text()='Please read our Terms & Conditions']");
	public static By lnkCompleteTermsandCond= By.xpath("//a[text()='Read Our Complete Terms & Conditions']");
	public static By txtHeaderCompleteTerms= By.xpath("//h1[text()='Terms - Benefits, Eligibility and Conditions Last updated January 15, 2020']");
	public static By cmdEnrollInPicked= By.xpath("//button[@class='btn btn-large btn-processing btn-rounded btn-red formWrap_btn cartButton finalReview_btn pickedSubscribeButton']");
	public static By txtEnrollmentError= By.xpath("(//div/p[text()='Due to county regulations, we cannot ship alcohol to this zip code.'])");
	public static By txtGiftCardUsed= By.xpath("//th[text()='Gift Card Used']");
	public static By objGiftCardBal= By.xpath("(//div[@class='storedPayment_remaining'])[1]");
	public static By txtGiftCardRemaining= By.xpath("//th[text()='Gift Card Remaining']");
	public static By txtGiftCrdRemAmtZero= By.xpath("//th[text()='Gift Card Remaining']/following::td[text()='$0.00']");
	public static By txtGiftCardRemZero= By.xpath("//ul[@class='giftCardsOption_collection userDataCard']/li[@class='userDataCard_listItem storedPayment_listItem']");
	public static By txtGiftcardusedtotal= By.xpath("//td[@class='orderSummary_price orderSummary_giftCardUsedTotal']");
	
}

