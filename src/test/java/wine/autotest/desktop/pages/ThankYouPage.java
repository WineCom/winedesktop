package wine.autotest.desktop.pages;

import org.openqa.selenium.By;

public class ThankYouPage {
	
	public static By lnkOrderNumber = By.xpath("//p[@class='thankYou_orderLink']/a");
	public static By spnTickSymbol = By.xpath("//span[@class='icon icon-check']");
	public static By spnOrderConfirmation = By.xpath("//p[text()='You will receive an order confirmation email at']");
	public static By spnthanksOnOrderPage = By.xpath("//p[(text()='Thanks!')]");
	public static By spnverifyTextOrderNumberIs = By.xpath("//p[(text()='Your order number is ')]");
	public static By lnkOrderAgain = By.xpath("//span[(text()='Order Again')]");
	public static By spnverifyTextSendThisWine = By.xpath("//p[(text()='Send this wine to another recipient.')]");
	public static By lnkAdvertisementClose = By.xpath("//a[@class='extole-js-widget-close extole-controls extole-controls--close']");
	public static By txtDeliveryTimeLine = By.xpath("//div[text()='Delivered via email']");
}
