package wine.autotest.desktop.pages;

import org.openqa.selenium.By;

public class PickedPage {
	
//---------------------------Picked Page--------------------------------------------
	public static By imgMainPickedLogo=By.xpath("//span[@class='pickedLogo']");
	public static By imgPickedLogo= By.xpath("//a[@class='pickedLogo']");
	public static By lnkPickedSetting= By.xpath("(//a[text()='Picked Settings'])[1]");
	public static By lnkPickedCompassTiles= By.xpath("//a[@href='https://qwww.wine.com/picked?iid=Homepage:Tile-2-1:Default:tile2-20-07-07-pickedbeta']/section[@class='homePageTile_textWrap']//span[@class='homePageTile_title']");
	public static By txtVerifyWelcomPicked= By.xpath("//h2[text()='Welcome to Picked!']");
	public static By cmdPickedThankyou= By.xpath("//div[text()='Glad to meet you!']");
	public static By imgAppStore= By.xpath("//img[@alt='Download on the App Store']");
	public static By imgAppStore2= By.xpath("//img[@alt='Get it on Google Play']");
	public static By cmdVerifyRedWine= By.xpath("//button[@class='btn btn-rounded pickedAccountUpdateVarietal_redBtn atomic-ctaHover']");
	public static By cmdVerifyWhiteWine= By.xpath("//button[@class='btn btn-rounded pickedAccountUpdateVarietal_whiteBtn atomic-ctaHover']");
	//public static By = By.xpath("");
	public static By txtSubscriptionSettingsHeader= By.xpath("//div[@class='subscriptionReviewWrap subscriptionReview_settingsSummary']");
	public static By lnkSubscriptionEditOpt= By.xpath("//h2[@class='subscriptionReview_titleText']/following::a[text()='Edit']");
	public static By spnPickedSetYourSubscription= By.xpath("//h2[@class='accountPage_headline pickedAccount_headline pickedAccount_headline-subscriptionSettings']");
	public static By spnpickedsetTargetPrice= By.xpath("//span[@class='subscriptionReviewPrice_total-price']");
	public static By lnkpickedsetMinPrice= By.xpath("//span[@class='subscriptionReviewPrice_range-low']");
	public static By lnkpickedsetMaxPrice= By.xpath("//span[@class='subscriptionReviewPrice_range-high']");
	public static By spnpickedsetRedQty= By.xpath("//span[@class='subscriptionReviewContents_text-redAmount']");
	public static By spnpickedsetRedPrice= By.xpath("//span[@class='subscriptionReviewContents_text-redPrice']");
	public static By spnpickedsetWhiteQty= By.xpath("//span[@class='subscriptionReviewContents_text-whiteAmount']");
	public static By spnpickedsetWhitePrice= By.xpath("//span[@class='subscriptionReviewContents_text-whitePrice']");
	public static By spnpickedsetFreqDesc= By.xpath("//dd[@class='subscriptionReviewSettings_data subscriptionReviewSettings_frequency']");
	public static By spnpickedsetDiscDesc= By.xpath("//dd[@class='subscriptionReviewSettings_data subscriptionReviewSettings_adventurousness']");
	public static By spnpickedsetFreeShip= By.xpath("(//li[@class='subscriptionReviewProgram_item'])[1]");
	public static By spnpickedsetSatisfactGuarant= By.xpath("(//li[@class='subscriptionReviewProgram_item'])[3]");
	
	
	
	public static By spnExistingPymtName= By.xpath("//div[@class='userDataCard_headline storedPayment_name']");
	public static By lnkPickedSettingChangePymt= By.xpath("//button[@class='pickedAccountSettings_headerLink changePayment atomic-ctaHover']");
	public static By spnPickedSettingHeader= By.xpath("//h2[@class='pickedAccountFeedback_heading']");
	public static By lnkSubSettingsEdit= By.xpath("//a[@class='subscriptionReview_editBtn atomic-ctaHover']");
	public static By btnSubSettSaveChange= By.xpath("//button[@class='pickedProgramSettings_btn pickedProgramSettings_btn-save btn btn-rounded btn-red']");
	public static By txtPickedSetNameonCard= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='name']");
	public static By txtPickedSetBillAddressCardNumber= By.xpath("//input[@id='cardNumber']");
	public static By txtPickedSetExpMonth= By.xpath("//input[@id='expirationMonth']");
	public static By txtPickedSetExpYear= By.xpath("//input[@id='expirationYear']");
	public static By txtPickedSetExpCvv= By.xpath("//input[@id='expirationCvv']");
	
	public static By txtPickedSetBillAddressName= By.xpath("//input[@id='js-google-autocomplete-payment']");
	public static By txtPickedSetBillAddressCity= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='city']");
	public static By txtPickedSetBillAddressState= By.xpath("//select[@class='state_select']");
	public static By txtPickedSetBillAddressZipCode= By.xpath("//input[@class='formWrap_input checkoutForm_input paymentForm_zipCodeInput' and @name='zipOrPostalCode']");
	public static By txtPickedSetBillAddressPhone= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='phone']");
	public static By chkPickedSetPymtPrefferedAdd= By.xpath("//input[@class='formWrap_checkbox checkoutForm_checkbox stateSelect_checkbox']");
	public static By btnPickedSetPymtSave= By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-new']");
	
	public static By spnRecipientNamePickedSettings= By.xpath("//div[@class='userDataCard_headline shippingAddress_name js-is-fedEx']");
	public static By lnkChangedRecipientAddress= By.xpath("//button[@class='pickedAccountSettings_headerLink shippingAddress_change atomic-ctaHover']");
	public static By txtpickedSetZipCode= By.xpath("//input[@class='formWrap_input checkoutForm_input shipToFedEx_zipInput']");
	public static By btnLocalPickupSearch= By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn shipToFedEx_btn']");
	public static By btnShipToThisLocation= By.xpath("(//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded '])[1]");
	public static By btnLocalPickupAddressselect= By.xpath("(//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded '])[4]");
	public static By txtpickupInfoFirstName= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='firstName']");
	public static By txtpickupinfoLastName= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='lastName']");
	public static By txtpickupinfoAddress1= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='address1']");
	public static By txtpickupinfoCity= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='city']");
	public static By dwnpickupinfoState= By.xpath("//select[@class='state_select' and @name='shipToState']");
	public static By txtpickupinfoZip= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='zip']");
	public static By txtpickupinfoPhone= By.xpath("//input[@class='formWrap_input checkoutForm_input' and @name='phone']");
	public static By btnpickupinfoSave= By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn']");
	
	//public static By = By.xpath("");
	

	public static By txtTargetPrice= By.xpath("//p[text()='Target Price: ']");
	public static By txtGetTaggetPrice= By.xpath("//span[@class='subscriptionReviewPrice_total-price']");
	//public static By = By.xpath("");
	public static By spnTargetPrice= By.xpath("//span[@class='pickedSettingsGrid_totalPriceValue']");
	public static By txtWineTypes= By.xpath("//p[@class='subscriptionReviewContents_text subscriptionReviewContents_text-red']");
	public static By txtFrequency= By.xpath("//dt[text()='Frequency']");
	public static By txtDiscoveryLevel= By.xpath("//dt[text()='Discovery Level']");
	public static By txtFreeShipping= By.xpath("//li[text()='Free shipping']");
	public static By txtSatisfactionGurante= By.xpath("//li[text()='Satisfaction guaranteed']");
	
	public static By txtVerifyMyRangeForm= By.xpath("//p[text()='May range from ']");
	public static By txtVerifyLowRange= By.xpath("//span[@class='subscriptionReviewPrice_range-low']");
	public static By txtVerifyHighRange= By.xpath("//span[@class='subscriptionReviewPrice_range-high']");
	public static By txtRedAmount= By.xpath("//span[@class='subscriptionReviewContents_text-redAmount']");
	public static By txtRedPrice= By.xpath("//span[@class='subscriptionReviewContents_text-redPrice']");
	public static By txtWhiteAmount= By.xpath("//span[@class='subscriptionReviewContents_text-whiteAmount']");
	public static By txtWhitePrice= By.xpath("//span[@class='subscriptionReviewContents_text-whitePrice']");
	
	
	public static By spnAdventurTypeVery= By.xpath("//span[@class='pickedQuizQuestion_text' and text()='Very']");
	public static By rdoFrequency3Months= By.xpath("//label[@class='pickedQuizQuestion_label']//span[@class='pickedQuizQuestion_text' and text()='Every 3 months']");
	
	public static By txtOrderStatus= By.xpath("//h2[text()='Order Status']");
	public static By txtSubcreption= By.xpath("//h2[text()='Subscription Settings']");
	public static By txtRecipient= By.xpath("//h2[text()='Recipient']");
	public static By txtPayment= By.xpath("//h2[text()='Payment']");
	public static By txtChangeAdd= By.xpath("//button[text()='Change Address']");
	public static By txtChangePay= By.xpath("//button[text()='Change Payment']");
	public static By txtCancleSub= By.xpath("//a[text()='Cancel your subscription']");
	public static By txtTermAndCon= By.xpath("//a[text()='Term & Conditions']");
	public static By lblPickedCancel= By.xpath("(//input[@name='pickedCancelWhy'])[1]");
	public static By cmdCancleSubscription= By.xpath("//button[text()='Cancel Subscription']");
	public static By cmdCloseSubscription= By.xpath("//button[text()='Close']");
	public static By txtRestartSubscription= By.xpath("//button[text()='Restart Subscription']");
	public static By txtVerifyPicked= By.xpath("//h2[text()='Ready to give Picked another try?']");
//------------------------------------------------Sign up--------------------------------------
	
	public static By cmdSignUpCompus= By.xpath("//a[@class='pickedApp_button']");
	public static By btnSignUpPickedWine= By.xpath("//a[@href='/picked/onboarding?iid=PickedOfferPageHero']");
	
	public static By txtVerifyFirsrPickedPage= By.xpath("//span[text()='Where are you in your wine journey?']");
	public static By txtVerifySeconePickedPage= By.xpath("//span[text()='What types of wine would you like to receive?']");
	public static By txtVerifyThirdPickedPage= By.xpath("//strong[text()='How do you feel about these red wines?']");
	public static By txtVerifyForthPickedPage= By.xpath("//span[text()='Is there anything else your personal somm should know?']");
	public static By txtPersonalSommPge= By.xpath("//strong[contains(text(),'ll find your perfect picks from over 15,000 win')]");
	
	public static By txtMonthTypPge= By.xpath("//span[@class='pickedQuizQuestion_questionText']");
	
	
	public static By cmdPickedWrapNext= By.xpath("(//button[@class='pickedQuizWrap_button pickedQuizWrap_next'])[2]");
	public static By cmdPickedNext= By.xpath("(//button[@class='pickedQuizWrap_button pickedQuizWrap_next'])[1]");
	public static By cmdPickedWrapNextThirdPage= By.xpath("(//button[@class='pickedQuizWrap_button pickedQuizWrap_next'])[3]");
	//public static By = By.xpath("");
	public static By btnWineJourneyNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-1')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By btnWineTypeNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-2')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By btnRedwinePageNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-3')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By btnWhitewinePagenNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-4')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By btnAdventuTypNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-6')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By btnMnthTypNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-7')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By btnRedWhiteQtyNxt= By.xpath("(//div[@class='pickedQuizWrap_buttonGroup']//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')])[1]");
	public static By btnSelQtyPrev= By.xpath("//div[@class='pickedSettingsWrap js-show-red-wine']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	//public static By = By.xpath("");
	public static By btnWineJourneyPrev= By.xpath("//section[@class='pickedQuizSection pickedQuizSection-1 pickedQuizSection-plain pickedQuizSection-wineBoxImg']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	public static By btnWineTypePrev= By.xpath("//section[@class='pickedQuizSection pickedQuizSection-2 pickedQuizSection-plain pickedQuizSection-wineBoxSecondImg']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	public static By btnRedwinePagePrev= By.xpath("//section[@class='pickedQuizSection pickedQuizSection-3 pickedQuizSection-redWine pickedQuizSection-redWineImg']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	public static By btnPersonalSommPrev= By.xpath("//section[@class='pickedQuizSection pickedQuizSection-5 pickedQuizSection-plain pickedQuizSection-preferenceSummary picked-core-forward']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	public static By btnAdventuTypPrev= By.xpath("//section[@class='pickedQuizSection pickedQuizSection-6 pickedQuizSection-plain pickedQuizSection-discovery']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	public static By btnMonthTypPrev= By.xpath("//section[@class='pickedQuizSection pickedQuizSection-7 pickedQuizSection-plain pickedQuizSection-calendar picked-core-forward']//button[@class='pickedQuizWrap_button pickedQuizWrap_prev']");
	
	
	public static By iconLikdThumsUpFrZinfandel= By.xpath("//i[@class='icon icon-thumb-up pickedPreferences_icon pickedPreferences_icon-red']/following::span[text()='Zinfandel']");
	public static By iconLikdThumsUpFrMalbec= By.xpath("//i[@class='icon icon-thumb-down pickedPreferences_icon pickedPreferences_icon-red']/following::span[text()='Malbec']");
	
	public static By lblNotVery= By.xpath("(//fieldset[@class='formWrap_group pickedQuizQuestion_fieldset pickedQuizQuestion_fieldset-adventurousness']//span[@class='pickedQuizQuestion_radioGroup'])[1]");
	public static By chkRedOnly= By.xpath("(//fieldset[@class='formWrap_group pickedQuizQuestion_fieldset pickedQuizQuestion_fieldset-caseStyle']//span[@class='pickedQuizQuestion_radioGroup'])[1]");
	public static By rdoPickedQuizWineTypRed= By.xpath("//span[@class='pickedQuizQuestion_radioGroup']/span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan']/following::span[contains(text(),'Red only')]");
	public static By rdoPickedQuizWineTypWhite= By.xpath("//span[@class='pickedQuizQuestion_radioGroup']/span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan']/following::span[contains(text(),'White only')]");
	public static By chkRedandWhite= By.xpath("//span[@class='pickedQuizQuestion_radioGroup']/span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan']/following::span[contains(text(),'Both Red and White')]");
	public static By btnSelQtyNxt= By.xpath("//div[@class='pickedSettingsWrap js-show-red-wine']/div[@class='pickedQuizWrap_buttonGroup']/button[@class='pickedQuizWrap_button pickedQuizWrap_next']");
	public static By chkNoVoice= By.xpath("(//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan'])[1]");
	public static By rdoPickedQuizNovice= By.xpath("//span[@class='pickedQuizQuestion_radioGroup']/span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan']/following::span[contains(text(),'Novice')]");
	public static By spnlikedheadertext= By.xpath("//span[@class='pickedPreferenceSummary_headerText' and text()='Your somm will base your picks on these']]");
	public static By spnDislikedHeaderText= By.xpath("//span[@class='pickedPreferenceSummary_headerText' and contains(text(),'never receive these')]");
	public static By spnAnythingelseQuest= By.xpath("//span[@class='pickedQuizQuestion_questionText' and text()='Is there anything else your personal somm should know?']");
	
	public static By rdoZinfandelLike= By.xpath("//div[@class='pickedQuizQuestion pickedQuizQuestion-3 pickedQuizQuestion-winePreference']//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-like']");
	public static By rdoMalbecDisLike= By.xpath("//div[@class='pickedQuizQuestion pickedQuizQuestion-5 pickedQuizQuestion-winePreference']//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-dislike']");
	public static By rdoChardonnaylLike= By.xpath("//div[@class='pickedQuizQuestion pickedQuizQuestion-18 pickedQuizQuestion-winePreference']//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-like']");
	public static By rdoSauvignonBlancDisLike= By.xpath("//div[@class='pickedQuizQuestion pickedQuizQuestion-23 pickedQuizQuestion-winePreference']//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-dislike']");
	public static By rdoWhiteOakyChardonnayLike= By.xpath("//div[@class='pickedQuizQuestion pickedQuizQuestion-19 pickedQuizQuestion-winePreference']//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-like']");
	public static By rdoWhiteViognierDislike= By.xpath("//div[@class='pickedQuizQuestion pickedQuizQuestion-20 pickedQuizQuestion-winePreference']//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-dislike']");
	 
	public static By spnWhiteWineAnythingelselikeZinfandel= By.xpath("//section[@class='pickedPreferenceSummary_section pickedPreferenceSummary_section-likes']//span[@class='pickedPreferences_item' and text()='Zinfandel']");
	public static By SpnWhiteWineAnythingelselikeChardonnay= By.xpath("//section[@class='pickedPreferenceSummary_section pickedPreferenceSummary_section-likes']//span[@class='pickedPreferences_item' and text()='Chardonnay']");
	public static By SpnWhiteWineAnythingelseDislikeSauvignonBlanc= By.xpath("//section[@class='pickedPreferenceSummary_section pickedPreferenceSummary_section-dislikes']//span[@class='pickedPreferences_item' and text()='Sauvignon Blanc']");
	public static By SpnWhiteWineAnythingelseDislikeMalbec= By.xpath("//section[@class='pickedPreferenceSummary_section pickedPreferenceSummary_section-dislikes']//span[@class='pickedPreferences_item' and text()='Malbec']");
	
	public static By btnaddRedWinePrice= By.xpath("//button[@class='pickedSettingsGroup_button pickedSettingsGroup_button-plus pickedSettingsGroup_valueText-redAddPrice']");
	public static By spnRedWinePrice= By.xpath("//span[@class='pickedSettingsGroup_valueText pickedSettingsGroup_valueText-redPrice']");
	public static By spnWhiteWinePrice= By.xpath("//span[@class='pickedSettingsGroup_valueText pickedSettingsGroup_valueText-whitePrice']");
	public static By spnRedWineTotalPrice= By.xpath("//span[@class='pickedSettings_totalPrice-red']");
	public static By btnSubtractWhiteWinePrice= By.xpath("//button[@class='pickedSettingsGroup_button pickedSettingsGroup_button-minus pickedSettingsGroup_valueText-whiteSubtractPrice']");
	public static By spnWhiteWineTotalPrice= By.xpath("//span[@class='pickedSettings_totalPrice-white']");
	//public static By = By.xpath("");
	
	public static By btnpersonalcommntNxt= By.xpath("//section[contains(@class,'pickedQuizSection pickedQuizSection-5')]//button[@class='pickedQuizWrap_button pickedQuizWrap_next'][contains(text(),'Next')]");
	public static By rdoPickedQuizNtVry= By.xpath("//span[@class='pickedQuizQuestion_radioGroup']/span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan']/following::span[contains(text(),'Not very')]");
	public static By rdoPickedQuizVery= By.xpath("//span[@class='pickedQuizQuestion_radioGroup']/span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan']/following::span[contains(text(),'Very')]");
	public static By txtSubscriptionSettingsPge= By.xpath("//span[@class='pickedSettings_legend pickedQuizQuestion_legend pickedQuizQuestion_selectMix']");
	public static By btnSelQtyWhiteWineNxt= By.xpath("//div[@class='pickedSettingsWrap js-show-white-wine']/div[@class='pickedQuizWrap_buttonGroup']/button[@class='pickedQuizWrap_button pickedQuizWrap_next']");
	public static By spnLiketoMixQuest= By.xpath("//p[@class='pickedSettingsAddClass_caption']");
	public static By btnMixRedWhite= By.xpath("//button[@class='btn btn-rounded pickedSettingsAddClass_btn']");
	public static By rdoMixQuestChardonnay= By.xpath("//div[contains(@class,'pickedQuizQuestion pickedQuizQuestion-18')]//span[@class='formWrap_radioSpan pickedQuizQuestion_radioSpan pickedQuizQuestion_radioSpan-like']");
	public static By spnHeaderRedWhitePopup= By.xpath("//div[@class='pickedQuizSection_header']/strong");
	
	public static By btnConfirmMixPopup= By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing pickedQuizWrap_confirmUpdate']");
	public static By btnCancelMixPopup= By.xpath("//button[@class='btn btn-link pickedQuizWrap_cancelUpdate']");
	
	
	public static By btnAddWhiteBottleCount= By.xpath("//button[@class='pickedSettingsGroup_button pickedSettingsGroup_button-plus pickedSettings_addWhiteCount']");
	public static By btnAddRedBottleCount= By.xpath("//button[@class='pickedSettingsGroup_button pickedSettingsGroup_button-plus pickedSettings_addRedCount']");
	public static By spnWhiteCount= By.xpath("//span[@class='pickedSettingsGroup_valueText pickedSettingsGroup_valueText-white']");
	public static By spnRedCount= By.xpath("//span[@class='pickedSettingsGroup_valueText pickedSettingsGroup_valueText-red']");

	public static By txtRecipientHeader= By.xpath("(//div[@class='checkoutHeader'])[1]");
	public static By btnEnrollInPicked= By.xpath("//button[@href='/picked/subscribe/thankyou']");
	public static By txtHeadlinePicked= By.xpath("//section[@class='pickedThankYouWrap']//div[@class='pickedThankYou_headline']");
	
//-------------------------
	public static By lnkPickedSetChangeDate= By.xpath("//a[@class='pickedAccount_link pickedShippingStatus_link atomic-ctaHover']");
	public static By radDelayDateOneWeek= By.xpath("//span[@class='pickedNextOrderDelay_text' and text()='Delay by 1 week ']/span[@class='pickedNextOrderDelay_text-date']");
	public static By btnDelaydateConfirm= By.xpath("//button[@class='btn btn-small btn-rounded btn-red pickedNextOrderDelay_confirm']");
	public static By spnPickedSetPickedDate= By.xpath("//span[@class='pickedShippingNextOrder_day']");
	//public static By = By.xpath("");
	public static By btnHomeAddrCont= By.xpath("//section[@class='shipToHomeBusiness']//button[.='Continue']");
	
}


