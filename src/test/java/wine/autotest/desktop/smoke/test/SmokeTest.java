package wine.autotest.desktop.smoke.test;

import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import org.testng.annotations.Parameters; 
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.GlobalVariables;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.desktop.library.Initialize;

public class SmokeTest extends GlobalVariables {
	static int numberOfTestCasePassed=0;
	static int numberOfTestCaseFailed=0;
	public static String TestScriptStatus="";
	public static String startTime=null;
	public static String endTime=null;
	public static String objStatus = null;
	String teststarttime=null;
	/***************************************************************************
	 * Method Name			: loadFiles()
	 * Created By			: Ramesh S
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method  is to load .properties 
	 * 						   files.
	 ****************************************************************************
	 */
	
	@BeforeClass
	@Parameters({"browser", "testEnvironment", "teststate"})
	public static void loadFiles(String browser, String testEnvironment, String teststate)
	{ 
		String teststarttime=null;
		try
		{
				log=Logger.getLogger("wine Automation ...");
				teststarttime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				environmentUrl=testEnvironment;
				state=teststate;
				ReportUtil.createReport(SmokeReportFileName, teststarttime,environmentUrl);
				UIBusinessFlows.deleteFile(driver);
				xmldata=new XMLData();
				url=UIFoundation.property(configurl);
				UIFoundation.screenshotFolderName("screenshots");
				ReportUtil.startScript("Scenarios");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: ExtendReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method  is to generate extend report
	 ****************************************************************************
	 */
	@BeforeTest
	public void ExtendReport()
	{
		try
		{
			//Extend Report
			ExtentHtmlReporter reporter =new ExtentHtmlReporter(ExtendSmokeReportFileName);
	        report =new ExtentReports();
	        report.attachReporter(reporter);
	        reporter.config().setDocumentTitle("Platform Automation");
	        reporter.config().setReportName("Order Creation Execution Report");
	      
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	}

	/***************************************************************************
	 * Method Name			: LaunchBrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: This method is used to launch the browser.
	 ****************************************************************************
	 */
	@BeforeMethod
	@Parameters({"browser"})
	public static void Launchbrowser(String browser)
	{ 
			try
		{
				log=Logger.getLogger("Launch Browser ...");
				ReportUtil.deleteDescription();
				driver=Initialize.launchBrowser(browser);
				objStatus = null;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
		
	/***************************************************************************
	 * Method Name : TC01_OrderCreationWithNewAccount() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ****************************************************************************/
	@Test
	public static void TC01_OrderCreationWithNewAccount()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method); 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC02_OrderCreationWithDifferentVarietalProducts() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ****************************************************************************
	 */
	@Test
	public static void TC02_OrderCreationWithDifferentVarietalProducts()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method); 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithDifferentVarietalProducts.addprodTocrt());
		            objStatus += String.valueOf(OrderCreationWithDifferentVarietalProducts.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC03_OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC03_OrderCreationWithGftMsgNGftEmailWithSilverSheerGftBag()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method); 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag.OrderCreationWithSilverSheerGftBag());	
					 System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name : TC04_OrderCreationWithStandardShippingMethodAsStewardshipUser() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC04_OrderCreationWithStandardShippingMethodAsStewardshipUser()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.stewardLogin());	
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.searchProductWithProdName());	
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.checkoutUsingStandardShipping());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC05_VerifyOrderWithVisaCard() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	
	@Test
	public static void TC05_VerifyOrderWithVisaCard()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1];
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyOrderWithVisaCard.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: Closebrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser.
	 ****************************************************************************
	 */
	@AfterMethod
	public static void Closebrowser()
	{ 
			try
		{
				if (objStatus.contains("Fail"))
				{
				 	numberOfTestCaseFailed++;
				 	ReportUtil.writeTestResults(testCaseID, methodName, "Fail", startTime, endTime);
				}
				else
				{
					numberOfTestCasePassed++;
					ReportUtil.writeTestResults(testCaseID, methodName, "Pass", startTime, endTime);
				}
				log=Logger.getLogger("Close Browser ...");
//				Initialize.closeApplication();
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: CloseBrowserAndEndReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser and end the report.
	 * ****************************************************************************
	 */
	 @AfterTest
     public void CloseBrowserAndEndReport()
     {
		 try {
		
			 	report.flush(); 
		 }catch(Exception e)
			{
				e.printStackTrace();
			}
           }

		/***************************************************************************
		 * Method Name			: endScenariosExecution()
		 * Created By			: Vishwanath Chavan
		 * Reviewed By			: Ramesh,KB
		 * Purpose				: This method responsible for updating the end time to 
		 * 						  customized html report
		 ****************************************************************************
		 */
	@AfterSuite
	public static void endScenariosExecution()
	{
		String endTime=null;
		try
		{

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			ReportUtil.endScript(numberOfTestCasePassed,numberOfTestCaseFailed);
			ReportUtil.updateEndTime(endTime);
			UIFoundation.waitFor(3L);
			System.out.println("=======Order creation test case details==============");
			System.out.println("Total no test case Passed:"+numberOfTestCasePassed);
			System.out.println("Total no test case Failed:"+numberOfTestCaseFailed);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
