package wine.autotest.desktop.order.pages;

import org.openqa.selenium.By;

public class UserProfilePage {
	
	public static By lnkOrderStatus = By.xpath("(//a[text()='Order Status'])[1]");
	public static By lnkAddressBook = By.xpath("(//a[text()='Address Book'])[1]");
	public static By lnkPaymentMethods = By.xpath("(//a[text()='Payment Methods'])[1]");
	public static By lnkStewardshipSet = By.xpath("(//a[text()='StewardShip Settings'])[1]");
	public static By lnkEmailPreferences = By.xpath("(//a[text()='Email Preferences'])[1]");
	public static By lnkAccountInfo = By.xpath("(//a[text()='Account Info'])[1]");
	
	//Add Address Book
	public static By lnkAddNewAddress = By.xpath("//a[text()='Add a new address']");
	public static By txtAddressFullName = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");
	public static By txtStreetAddress = By.xpath("//span[text()='Street Address']/following::input[1]");
	public static By txtCity = By.xpath("(//input[@class='formWrap_input checkoutForm_input'])[7]");
	public static By dwnState = By.xpath("(//select[@name='shipToState'])[4]");
	public static By txtRecipientZipCode = By.xpath("(//input[@placeholder='Zip'])[1]");
	public static By txtPhoneNum = By.xpath("//input[@placeholder='Phone # (mobile preferred)']");
	public static By btnAddressSave = By.xpath("//button[text()='Save']");
	public static By btnVerifyContinue = By.xpath("//div[@class='shippingRecipient_btnBar']/div/button");
	public static By rdoSuggestedAddress = By.xpath("html/body/div[1]/div[2]/div/div[2]/div/ul/li[1]/div[3]/label");
	public static By spnAddressBookHeader = By.xpath("//h2[text()='Address Book']");
	public static By chkPreferredAddBook = By.xpath("//span[@class='formWrap_checkboxSpan checkoutForm_checkboxSpan']");
	public static By lnkshippingAddressCount = By.xpath("//li[@class='userDataCard_listItem shippingAddress_listItem']"); 
	public static By lnkuserDataCardEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[1]");
	public static By txtuserDataFirstNameClear = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");
	public static By txtuserDataFirstName = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName isInvalid']/label/input");
	public static By lnkcontactUs = By.xpath("(//a[text()='Contact Us'])[2]");
	public static By btnuserDataSave = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn']");
	public static By txtexpectedFirstName = By.xpath("//div[@class='userDataCard_headline shippingAddress_name']");
	public static By txtclearUpdateAccountLastName = By.xpath("//form[@class='formWrap updateAccount_form']/fieldset[@class='formWrap_group updateAccount_lastName']/label/input");
	public static By txtupdateAccountemail = By.xpath("//form[@class='formWrap updateAccount_form']/fieldset[@class='formWrap_group updateAccount_email']/label/input");
	public static By txtenterUpdateAccountLastName = By.xpath("//form[@class='formWrap updateAccount_form']/fieldset[@class='formWrap_group updateAccount_lastName isInvalid']/label/input");
	public static By lnkremoveThisAddress = By.xpath("//div[@class='shippingRecipient_btnBar shippingRecipient_saveSection']//a");
	public static By btnYesRemove = By.xpath("//button[text()='Yes, remove']");
	public static By spnyouDoNotHaveAnyAddress = By.xpath("//p[text()='You do not have any addresses saved.']");
	public static By btnremoveAddress = By.xpath("(//a[@class='shippingRecipient_removeBtn'])[1]");
	public static By chkmakeThisPreferredCard = By.xpath("//fieldset[@class='formWrap_group stateSelect_checkboxGroup paymentForm_makePreferred']/label");
	public static By lnkcreditCardEdit = By.xpath("//a[text()='Edit']");
	public static By lnkremoveThisCardPaymnt = By.xpath("//section[@class='accountPage']/section/section[@class='paymentFormSection']/form[@action='/account/payments']//a");
	public static By spnyouDoNotHaveCreditCard = By.xpath("//p[text()='You do not have any credit cards saved']");
	
	//StewardShip Settings
	public static By spnstewardshipHeader = By.xpath("//h1[text()='StewardShip Settings']");
	public static By spnstewardshiplandingHeader = By.xpath("//h1[text()='Unlimited FREE SHIPPING']");
	
	public static By spnjoinUsToday = By.xpath("//h2[text()='Join StewardShip today!']");
	public static By spncompleteTermsAndCondition = By.xpath("//a[text()='Read Our Complete Terms & Conditions']");
	public static By btnStewardshipAdd = By.xpath("//button[@class='stewardshipNonMember_btn btn btn-red btn-small btn-rounded']");
	public static By btnStewardshipConfirm = By.xpath("//button[text()='Confirm']");
	public static By btnStewardShipContinue = By.xpath("//span[contains(text(),'Continue')]");
	
	public static By txtstewardslifesavings = By.xpath("//section/div[@class='stewardshipSavingsOverview_savings']");
	public static By btnstewardsCancelMembership = By.xpath("//button[text()='Cancel Membership']");
	public static By btnStwmodalCancelMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-outline-blue btn-processing stewardshipOptOut_cancelMembershipBtn']");
	public static By btnStwmodalKeepMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-blue btn-processing stewardshipOptOut_keepMembershipBtn js-closeModal']");
	public static By btnStwmodalConfrimCancelMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-blue btn-processing stewardshipOptOut_confirmCancelation']");
	public static By btnStwmodaloptoutKeepMembership = By.xpath("//button[@class='btn btn-fullWidth btn-rounded btn-outline-blue btn-processing js-closeModal']");
	public static By spnStwmodalCancelerror = By.xpath("//div/p[@class='stewardshipOptOutCancel_errorMessage']");
	public static By spnstewardoptout = By.xpath("//h1[text()='StewardShip Settings']");
	
	public static By btnstewardsCancel = By.xpath("//span[text()='Cancel']");
	public static By spnareYouSure = By.xpath("//span[text()='Are you sure?']");
	public static By spnremainMember = By.xpath("//span[text()='No, remain a member']");
	public static By btnstewardsYesCancel = By.xpath("//button[text()='Yes, cancel']");
	public static By spnstewardsExpiryOn = By.xpath("//span[contains(text(),'Your StewardShip membership will expire on')]");
	public static By btnstewardshipRenew = By.xpath("//span[text()='Renew']");
	public static By btnstewardshipYesRenew = By.xpath("//button[text()='Yes, renew']");
	public static By spnstewardsRenewOn = By.xpath("//span[contains(text(),'Your StewardShip membership will renew on')]");
	public static By spnunlimitedFreeShipping = By.xpath("//span[text()='Unlimited FREE Shipping']");
	public static By spndedicatedCustomerService = By.xpath("//span[text()='Dedicated Customer Service']");
	
	
	//Payment Methods
	public static By spnPaymentMethodsHeader = By.xpath("//h2[text()='Payment Methods']");
	public static By lnkAddNewCard = By.xpath("//a[text()='Add payment method']");
	public static By txtNameOnCard = By.xpath("//input[@placeholder='Name On Card']");
	public static By txtCardNumber = By.xpath("//input[@id='cardNumber']");
	public static By txtExpiryMonth = By.xpath("(//form[@class='paymentForm_form']//fieldset/div/fieldset/label/input)[1]");
	public static By txtExpiryYear = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='paymentForm_expYear formWrap_group formWrap_group-third']/label/input");
	public static By txtCVV = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='formWrap_group formWrap_group-third formWrap_group-cvv paymentForm_cvvFieldset']/label/input[1]");
	public static By txtBillingAddress = By.xpath("(//fieldset[@class='formWrap_group']/label/input)[2]");
	public static By txtBillingCty = By.xpath("//fieldset[@class='formWrap_group paymentForm_cityWrap formWrap_group-half']/label/input");
	public static By dwnBillingSelState = By.xpath("(//select[@class='state_select'])[1]");
	public static By txtBillingZipCode = By.xpath("//fieldset[@class='paymentForm_zipCodeWrap formWrap_group formWrap_group-quarter float-right']/label/input");
	public static By txtBillingPhoneNum = By.xpath("(//fieldset[@class='formWrap_group']/label/input)[4]");
	public static By btnBillingSave = By.xpath("(//button[text()='Save'])[1]");
	
	public static By txtenterNameField = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='formWrap_section']//fieldset[@class='formWrap_group isInvalid']/label/input");
	public static By lnkeditNameField = By.xpath("(//input[@class='formWrap_input checkoutForm_input'])[1]");
	public static By txteditCVVField = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='formWrap_section']//fieldset[@class='formWrap_group formWrap_group-third formWrap_group-cvv paymentForm_cvvFieldset']/label/input");
	public static By btnSaveCreditCard = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])[1]");
	public static By spnuserPayementName = By.xpath("(//div[@class='userDataCard_headline storedPayment_name'])[2]");
	
	//Account Information
	public static By spnAccountInfoPromoBar = By.xpath("//a[@class='promoBar_header']");
	public static By txtBrthMonth = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-month']/label/input");
	public static By txtBrthDay = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-day']/label/input");
	public static By txtBrthYear = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-year']/label/input");
	public static By btnSaveAccountInfo = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing updateAccount_save']");
	public static By spnyearErrorMsg = By.xpath("//form[@class='formWrap updateAccount_form']/div/p[text()='You must be at least 21 to use Wine.com.']");
	
	//Order Status
	public static By spnOrderHistoryPage = By.xpath("//h3[text()='Order History']");
	public static By spnverifyPinkBar = By.xpath("//div[@class='creditRemaining']");
	public static By spnverifyCreditTotal = By.xpath("//span[@class='creditTotal']");
	
	//Email Preferences
	public static By spnEmailPreferenceHeader = By.xpath("//h2[text()='Email Preferences']");
	public static By rdoemailPreference = By.xpath("/(//input[@class='formWrap_radio frequency_radio frequency_full'])[1]");
	public static By spnnewArrivalAlertHeader = By.xpath("//h2[text()='New Arrival Alerts']");
	public static By spnnewArrivalAlertListItem = By.xpath("(//li[@class='newArrivalAlert_listItem'])[1]");
	public static By lnkdeleteAlerts = By.xpath("(//a[text()='Delete'])[1]");
	
	public static By lnknewArrivalAlertFirstProduct = By.xpath("(//span[@class='newArrivalAlert_listItemName'])[1]");
	public static By lnknewArrivalAlertSecondProduct = By.xpath("(//span[@class='newArrivalAlert_listItemName'])[2]");
	
}
