package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import wine.autotest.desktop.order.pages.*;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithOrderAgainToTheSameLocation extends OrderCreation{


	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Ramesh S 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "orderagain"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : checkoutProcess() Created By : Vishwanath Chavan Reviewed By :
	 * Ramesh,KB Purpose : The purpose of this method is to capture the order number
	 * and purchased id
	 ****************************************************************************
	 */

	public static String checkoutProcess() {

		String expected = null;
		String actual = null;
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation28-1_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			// driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			/*if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}*/
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
			UIFoundation.waitFor(3L);
			//objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentCheckoutEdit));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaymentCVV, "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentSave));
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(10L);
			orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
			}else
			{
				objStatus+=false;
				String objDetail="Order  is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with existing account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with existing account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";

			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: orderAgainToTheSameLocation()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Viswanath,
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String orderAgainToTheSameLocation() {
		String objStatus=null;
		String screenshotName = "Scenarios_orderAgain_Screenshot28-2.jpeg";
			try {
				log.info("The execution of the method orderAgainToTheSameLocation started here ...");
				UIFoundation.waitFor(15L);			
				objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderAgain));
				UIFoundation.waitFor(15L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
					UIFoundation.waitFor(1L);
					objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnshipState, "State"));
					UIFoundation.clickObject(FinalReviewPage.dwnshipState);
					UIFoundation.clearField(FinalReviewPage.txtZipCode);
					objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCode"));
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
					UIFoundation.waitFor(6L);
					if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
						objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
						UIFoundation.waitFor(5L);
					}
					//objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShiptoaddressOne));
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
					UIFoundation.waitFor(5L);
					if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
						objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));	
						}			
					driver.navigate().refresh();
					UIFoundation.waitFor(6L);
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
					UIFoundation.waitFor(30L);
					String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
					if(orderNum!="Fail")
					{
						System.out.println("Order Creation With Order Again To The SameLocation test cases executed succesfully");
						UIFoundation.getOrderNumber(orderNum);
					}else
					{
						objStatus+=false;
						String objDetail="Order Creation With Order Again To The SameLocation test cases failed";
						UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
						logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
					}
					log.info("The execution of the method checkoutProcess ended here ...");
					if (objStatus.contains("false"))
					{
						return "Fail";
					}
					else
					{
						return "Pass";
					}
					
				}catch(Exception e)
				{
					
					log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
							+ e);
					return "Fail";
					
				}
		

	}
}
