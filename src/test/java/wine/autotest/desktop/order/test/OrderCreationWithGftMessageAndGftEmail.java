package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.ReportUtil;

public class OrderCreationWithGftMessageAndGftEmail extends OrderCreation {

	/***************************************************************************
	 * Method Name			: OrderCreationWithGftMessageAndGftEmail()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to  Create an order 
	 * With Gift Message and Gift email (Without Gift Bag) 
	 * 
	 ****************************************************************************
	 */
	

	public static String orderWithGftMessageNGftEmail () {
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "orderWithGftMessageNGftEmail.jpeg";
      
		try {
			log.info("order With Gft Message N Gft Email method started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 70);
             UIFoundation.waitFor(8L);  
             UIBusinessFlows.recipientEdit();
             if(UIFoundation.isDisplayed(FinalReviewPage.lnkaddGiftMessage))
   			{
                  objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
   				UIFoundation.waitFor(3L);	
   			}else{
 				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
 				UIFoundation.waitFor(3L);	
 			}  
             UIFoundation.clearField(FinalReviewPage.txtgiftRecipientEmail);
             UIFoundation.waitFor(1L);
             objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftRecipientEmail));
             objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(FinalReviewPage.txtgiftRecipientEmail,"email"));
             objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftMessageTextArea));
             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessage"));
             UIFoundation.waitFor(1L);
             objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftRecipientEmail));
             String enteredGftMessg = UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
             UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientContinue);
             UIFoundation.waitFor(1L);
             objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
             if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
  				//DeliveryContinue
  			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
  			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
  			UIFoundation.waitFor(1L);
  			}
  			else
  			{
  			
  	 			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
  	 			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
  	 			UIFoundation.waitFor(1L);
  				
  			}
           
 			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
 				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
 				UIFoundation.waitFor(1L);
 			}
 			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
 				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);
 				UIFoundation.waitFor(1L);
 				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
 				UIFoundation.waitFor(5L);
 				UIFoundation.clickObject(FinalReviewPage.txtCVVR);
 				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
 				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
 			}
 			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
 				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
 				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
 				UIFoundation.waitFor(2L);
 				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
 				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
 				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
 				UIFoundation.waitFor(1L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
 				if(!ele.isSelected())
 				{
 			    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
 			    UIFoundation.waitFor(3L);
 				}
 				}
 			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
 				//DeliveryContinue
 			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
 			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
 			UIFoundation.waitFor(1L);
 			}
 			else
 			{
 			
 	 			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
 	 			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
 	 			UIFoundation.waitFor(1L);
 				
 			}
 			subTotal = UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling = UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total = UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax = UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(25L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully :";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				logger.pass(objDetail+orderNum);
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			} 
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			String giftMsg=UIFoundation.getText(OrderDetailsPage.spnorderHistoryGftMssg);
			System.out.println("giftMsg :"+giftMsg);
			System.out.println("enteredGftMessg :"+enteredGftMessg);
			if(enteredGftMessg.equals(enteredGftMessg)) {
			objStatus+=true;
			String objDetail = "Order is succesfully placed with gift message and gift email";
			logger.pass(objDetail);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else {
			objStatus+=false;
			String objDetail = "Order is not succesfully placed with gift message and gift email";
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("order With Gft Message N Gft Email method ended here");
		if (objStatus.contains("false"))
		{
			System.out.println("Verify the Order Creation With Gft Message And GftEmail test case failed ");
			return "Fail";
		}
		else
		{
			System.out.println("Verify the Order Creation With Gft Message And GftEmail test case executed succesfully");
			return "Pass";
		}
	}catch(Exception e)
	{
		log.error("there is an exception arised during the execution of the method "+ e);
		return "Fail";
	}

}
	}
