package wine.autotest.desktop.order.test;

import java.io.IOException;

import org.openqa.selenium.By;
import wine.autotest.desktop.order.pages.*;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithPresaleOrder extends OrderCreation{

	/***************************************************************************
	 * Method Name			: addProductPresale()
	 * Created By			: 
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	
	public static String addProductPresale() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		
		try {
			
			UIFoundation.waitFor(5L);		
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkFeatured));
		UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexFeatures));			
	    	UIFoundation.waitFor(4L);
			addToCart1 = UIFoundation.getText(ListPage.lnkpreSelFirstProdct);
			if (addToCart1.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "SecondProdct");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkpreSelFirstProdct);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkpreSelSecondProdct);
			if (addToCart2.contains("Add to Cart")) {
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "SecondProdct");
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkpreSelSecondProdct);
			}
			addToCart3 = UIFoundation.getText(ListPage.lnkpreSelThirdProdct);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkpreSelThirdProdct);
			}
									
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			//UIFoundation.scrollDownOrUpToParticularElement(driver, "listPageContainer");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
		//	UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(3L);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name : checkoutProcess() Created By : Chandrashekhar Reviewed By :
	 *  Purpose : The purpose of this method is to capture the order number
	 * and purchased id
	 ****************************************************************************
	 */

	public static String checkoutProcess() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation11_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.isDisplayed(CartPage.lnkpreSelInCheckOut));
				UIFoundation.waitFor(1L);
			
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
				// driver.navigate().refresh();
				UIFoundation.waitFor(8L);
				objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
				

				if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.waitFor(1L);
				}
				if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
					objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
					//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
					WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
					if(!ele.isSelected())
					{
						 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					}
				if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
				}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(15L);
				orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				if (orderNum != "Fail") {
					objStatus += true;
					String objDetail = "Order is placed successfully";
					UIFoundation.getOrderNumber(orderNum);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Order is placed successfully: " + orderNum);
				} else {
					objStatus += false;
					String objDetail = "Order is null.Order not placed successfully";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}

			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with pre-sale products test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with pre-sale products test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method  " + e);
			objStatus += false;
			String objDetail = "Order number is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}	

}
