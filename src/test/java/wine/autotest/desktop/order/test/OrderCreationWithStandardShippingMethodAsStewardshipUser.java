package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithStandardShippingMethodAsStewardshipUser extends OrderCreation {

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String stewardLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "StewrdshipRenewalUsername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	/***************************************************************************
	 * Method Name : searchProductWithProdName() Created By : Chandrashekhar
	 * Reviewed By :  Purpose : The purpose of this method is to search for
	 * product with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus = null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProduct));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "bonarda"));
			UIFoundation.waitFor(6L);
			WebElement var=driver.findElement(ListPage.txtsearchProduct);
			var.sendKeys(Keys.ENTER);
			//objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
			return "Fail";
		}

	}
	/*********************************************************************************
	 * Method Name			: checkoutUsingStandardShipping()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				:  The purpose of this method is to Create an order with 
	 * Standard Shipping Method as a stewardship user
	 **********************************************************************************
	 */

	public static String checkoutUsingStandardShipping() {
	
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "checkoutUsingStandardShipping.jpeg";
	    
	
	        try {
	    	log.info("checkout Using Standard Shipping method started here");
	    	objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout)); 
	        UIFoundation.waitFor(8L);  
	        UIBusinessFlows.recipientEdit();
            UIFoundation.waitFor(1L);  
            if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeDeliveryDate)) {
            objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeDeliveryDate));
            }
            UIFoundation.waitFor(2L);  
            String shppingMethod =UIFoundation.getText(FinalReviewPage.txtShippingMethod);
            System.out.println("shppingMethod :"+shppingMethod);
            String expShippingMethod = verifyexpectedresult.standShippingText;
           if(shppingMethod.equals(expShippingMethod)) {
            objStatus+=true;
            String objDetail = "Standard shpping method is selected";
            ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
            }
            else {
            objStatus+=false;
            String objDetail = "Standard shpping method is not selected";
            UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
            logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath)).build());
            }
           if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
            if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
            if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
            if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
            if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
    			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
    			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
    			UIFoundation.waitFor(1L);
    			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(15L);
			String StdShipingtext = UIFoundation.getText(FinalReviewPage.lnkStandardShip);
			String expShippingtxt = verifyexpectedresult.stdShipping;
			System.out.println("StdShipingtext :"+StdShipingtext);
			if(StdShipingtext.contains(expShippingtxt))
			{			
				objStatus+=true;				
				String objDetail = "Order is succesfully placed and \"FedEx Express Ground displayed in the order History.";	
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else {
				objStatus+=false;
				String objDetail = "FedEx Express Ground is not displayed in the order History.";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
		
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With Shipping Method For Existing User test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With Shipping Method For Existing User test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method  " + e);
			objStatus += false;
			String objDetail = "Order number is null.Order not placed successfully";
			// ReportUtil.addTestStepsDetails("Order number is null.Order not placed
			// successfully", "", "");
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	    }

	}