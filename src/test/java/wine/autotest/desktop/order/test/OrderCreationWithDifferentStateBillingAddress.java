package wine.autotest.desktop.order.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;


public class OrderCreationWithDifferentStateBillingAddress extends OrderCreation {

	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced4399_Screenshot.jpeg";
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			
           if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
           UIFoundation.waitFor(10L);
           if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
 			{
 				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
 				UIFoundation.waitFor(3L);	
 			}
            UIFoundation.waitFor(2L);
            if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEdit))
             {
            	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEdit));
              	UIFoundation.waitFor(3L);	
              }
              objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
              UIFoundation.waitFor(3L);
              objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
              UIFoundation.waitFor(10L);
              objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
              UIFoundation.waitFor(3L);
              if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
            	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
              	UIFoundation.waitFor(5L);
              	}
              if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
            	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
             	UIFoundation.waitFor(5L);
             	}
              		/*objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
              		UIFoundation.hitTab(FinalReviewPage.btnRecipientContinue);
              		UIFoundation.waitFor(15L);*/
              	objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	             UIFoundation.waitFor(2L);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
	             UIFoundation.clearField(FinalReviewPage.txtCVV);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	             UIFoundation.waitFor(3L);
	             WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
	             if(ele.isSelected())
				{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnBillinState, "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingZip, "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdomakeThisPreferredCard));
				UIFoundation.waitFor(1L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
				}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{	
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 60);
				String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				if(orderNum!="Fail")
				{
			      objStatus+=true;
			      String objDetail="Order is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order is placed successfully: "+orderNum);   
				}else
				{
			       objStatus+=false;
			       String objDetail="Order is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath+ screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
			       String objDetail="New Billing address is not added";
			       UIFoundation.captureScreenShot(screenshotpath+ screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				return "Fail";
			}
			else
			{ 
				String objDetail="New Billing address is added successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: newBillingAddress()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	public static String newBillingAddress(WebDriver driver) {
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		
		

			try {
				log.info("The execution of the method newBillingAddress started here ...");
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(driver, "BillingAndShippingCheckbox");
				   UIFoundation.javaScriptClick(driver, "BillingAndShippingCheckbox");
				   UIFoundation.waitFor(3L);
				}
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(driver,"NewBillingAddress", "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(driver,"NewBillingSuite", "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(driver,"NewBillingCity", "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"BillinState", "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(driver,"obj_BillingZip", "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(driver,"NewBillingPhone", "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(driver, "PaymentContinue")){
				UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
				UIFoundation.webDriverWaitForElement(driver, "PaymentContinue", "Invisible", "", 60);
				}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(driver, "Subtotal");
				shippingAndHandling=UIFoundation.getText(driver, "obj_Shipping&Hnadling");
				total=UIFoundation.getText(driver, "TotalBeforeTax");
				salesTax=UIFoundation.getText(driver, "OrderSummaryTaxTotal");
				System.out.println("Subtotal:              "+subTotal);
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				System.out.println("Sales Tax:             "+salesTax);
				System.out.println("Total:                 "+total);
				UIFoundation.waitFor(2L);
				UIFoundation.scrollDownOrUpToParticularElement(driver, "PlaceOrderButton");
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PlaceOrderButton"));
				UIFoundation.webDriverWaitForElement(driver, "PlaceOrderButton", "Invisible", "", 50);
				String orderNum=UIFoundation.getText(driver,"OrderNum");
				if(orderNum!="Fail")
				 {
				      objStatus+=true;
				      String objDetail="Order number is placed successfully";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      System.out.println("Order number is placed successfully: "+orderNum);   
				 }else
				 {
				       objStatus+=false;
				       String objDetail="Order number is null and cannot placed successfully";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				 }
				log.info("The execution of the method newBillingAddress ended here ...");
				if ( objStatus.contains("false")) {
					System.out.println("Order creation with existing account billing address test case is failed");
					return "Fail";
				} else {
					System.out.println("Order creation with existing account billing address test case is executed successfully");
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method newBillingAddress "
						+ e);
				return "Fail";
			}
		}*/

	

}
