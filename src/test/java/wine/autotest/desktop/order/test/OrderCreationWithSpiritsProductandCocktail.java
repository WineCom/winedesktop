package wine.autotest.desktop.order.test;

import java.io.IOException;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.order.pages.*;


public class OrderCreationWithSpiritsProductandCocktail extends OrderCreation{

	/***************************************************************************
	 * Method Name			: addSpiritandCocktailprodTocrt()
	 * Created By			: 
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addSpiritandCocktailprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;

		try {
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkWhiskey));	//previously lnkVodka
			UIFoundation.waitFor(3L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkSpirits));
			UIFoundation.waitFor(1L);
	    	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCocktailRecipes));
			UIFoundation.waitFor(1L);
					
			
			if (UIFoundation.isDisplayed(ListPage.btnCocktailFirstprodtoCart)) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.btnCocktailFirstprodtoCart);
			}
			if (UIFoundation.isDisplayed(ListPage.btnCocktailSecondprodtoCart)) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.btnCocktailSecondprodtoCart);
			}						
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(3L);
			}


			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	

}
