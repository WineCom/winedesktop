package wine.autotest.desktop.order.test;

import java.io.IOException;

import org.openqa.selenium.By;

import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.UIFoundation;

public class OrderCreationWithGftMsgNGftEmailAlongWithNaturalBurlapGftBag extends OrderCreation{
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;

		try {			
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
	       	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
			UIFoundation.waitFor(1L);
			
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
            UIFoundation.waitFor(5L);
			
            UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(3L);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***********************************************************************************
	 * Method Name			: OrderCreationWithGftMsgNGftEmailAlongWithGftBag()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to  Create an order with
	 * Gift message ,Email and Natural Burlap Gift Bag
	 * 
	 **********************************************************************************
	 */

	public static String  OrderCreatioWithNaturalBurlapGftBag() {
	
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "OrderCreationSilverSheerWithNaturalBurlapGftBag.jpeg";
	   
	    try {
	    	log.info("Order Creation  With Natural Burlap Gft Bag started here");
	    	objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout));
	        UIFoundation.waitFor(8L);  
	    //    ApplicationDependent.recipientEdit(driver);
	        if(UIFoundation.isDisplayed(FinalReviewPage.lnkaddGiftMessage))
			{
              objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
				UIFoundation.waitFor(3L);	
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
			}  
	        UIFoundation.clearField(FinalReviewPage.txtgiftRecipientEmail);
	        UIFoundation.waitFor(1L);
	        objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(FinalReviewPage.txtgiftRecipientEmail,"email"));
	        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftMessageTextArea));
	        objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessage"));
	        UIFoundation.waitFor(1L);
	        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtgiftRecipientEmail));
	        String enteredGftMessg = UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
	        UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.rdogiftbag6_99);
	        UIFoundation.waitFor(1L);
	        objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdogiftbag6_99));
	        UIFoundation.waitFor(1L);
	        objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnRecipientContinue));
	        if (UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
				UIFoundation.waitFor(3L);
			}
	        UIFoundation.waitFor(1L);
	        UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.rdogftBagTot);
	        String silvrSheer = UIFoundation.getText(FinalReviewPage.rdogftBagTot);
	        String expSilvrsheeramt = verifyexpectedresult.naturalBurlap;
	        if(silvrSheer.equals(expSilvrsheeramt)) {
				objStatus += true;
				String objDetail = "NaturalBurlapGftBag gift bag is added succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "NaturalBurlapGftBag gift bag is not added succesfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
	        if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
	        if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			String giftbag=UIFoundation.getText(OrderDetailsPage.spnorderHisNatureBrown);
			String giftMsg=UIFoundation.getText(OrderDetailsPage.spnorderHistoryGftMssg);
			System.out.println("giftbag :"+giftbag);
			String expGftbagTxt = verifyexpectedresult.naturalGftBagText;
			if(giftbag.equals(expGftbagTxt)) {
				objStatus+=true;
				String objDetail = "NaturalBurlapGftBag gift bag is added successfully and displayed in order history";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}
				else {
				objStatus+=false;
				String objDetail = "NaturalBurlapGftBag gift bag is not added, and not displayed in order history";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			
			log.info("Order Creation NaturalBurlap With Gft Bag ended here");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the Order Creation With GftMsg N GftEmail Along With NaturalBurlap GftBag test case failed ");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the the Order Creation With GftMsg N GftEmail Along With NaturalBurlap GftBag test case executed succesfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method "+ e);
			return "Fail";
		}

	}
	}