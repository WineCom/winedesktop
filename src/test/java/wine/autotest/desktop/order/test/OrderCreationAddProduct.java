package wine.autotest.desktop.order.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import wine.autotest.desktop.order.pages.*;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.fw.utilities.ReportUtil;


public class OrderCreationAddProduct extends OrderCreation {

	
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderCreationAddprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			
			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 50);
			}
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));//previously lnkSangviovese
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 20);
			}
	
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
        	
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			
			if (addToCart2.contains("Add to Cart")) {
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			UIFoundation.waitFor(1L);
			}
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBordexBlends));//previously lnkSangviovese
			UIFoundation.waitFor(5L);
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
				UIFoundation.waitFor(1L);
			}
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
				UIFoundation.waitFor(1L);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
				UIFoundation.waitFor(1L);
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			 System.out.println("orderCreationAddprodTocrt Status:"+objStatus);
			if (objStatus.contains("false")) {
             
				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: shippingDetails()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: 
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 ****************************************************************************
	 */
	public static String shippingDetails() {
		String objStatus = null;
		   String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
		
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToHome));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			/*objStatus += String.valueOf(ApplicationIndependent.clickObject(driver, "SuggestedAddress"));
			ApplicationIndependent.waitFor(3L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.spnlocalPickUp)){
				objStatus+=true;
				String objDetail="Local pickup icon is  displayed next to pickup location address.";
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				
			}else{
				objStatus+=false;
					String objDetail="Local pickup icon is not displayed next to pickup location address.";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}

			log.info("The execution of the method shippingDetails ended here ...");
			 System.out.println("shippingDetails Status:"+objStatus);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String checkoutProcessSplitOrder()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		
		String totalBeforeTax=null;
		String finalReviewtotal=null;
		String finalReviewsalesTax=null;
		String finalReviewtotalBeforeTax=null;
		String finalReviewshippingAndHandling=null;
		
		String screenshotName = "Scenarios_OrderNotPlaced63_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			

           if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutAlter));
			}
                          UIFoundation.waitFor(2L);
		
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
	        {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
	        }
	          if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
	          {
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	             UIFoundation.waitFor(2L);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	             UIFoundation.waitFor(3L);   
	            WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);	
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
	                 
	          }
	          System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			finalReviewshippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			finalReviewtotal=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);	                 
          	
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			UIFoundation.waitFor(25L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      UIFoundation.getOrderNumber(orderNum);
			      UIFoundation.waitFor(1L);
			      objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));	
			      String objDetail="Order is placed successfully: "+orderNum;
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			 }
			
//			objStatus+=String.valueOf(ApplicationIndependent.clickObject(driver, "OrderNum"));
			UIFoundation.waitFor(10L);
			 if(UIFoundation.isDisplayed(OrderDetailsPage.spnOrderDetailOrdNum1) && UIFoundation.isDisplayed(OrderDetailsPage.spnOrderDetailOrdNum2))
		      { 
				  objStatus+=true;
				  String SplitorderNum=UIFoundation.getText(OrderDetailsPage.lnkSplitOrderNum);
				  String objDetail="Order Split successfully: "+SplitorderNum;
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order Split successfully: "+SplitorderNum); 
		      }else {
		    	  objStatus+=false;
			       String objDetail="Order is not splited ";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		      }
		      				
			log.info("The execution of the method checkoutProcess ended here ...");
			 System.out.println("checkoutProcessSplitOrder Status:"+objStatus);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String orderNum = null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced48495253585960_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			// driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIBusinessFlows.recipientEdit());
			

			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVVR));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(5L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
			
				}

			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			UIFoundation.webDriverWaitForElement(ThankYouPage.lnkOrderNumber, "Invisible", "", 20);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			System.out.println("orderNum :"+orderNum);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				logger.pass(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}		      				
			log.info("The execution of the method checkoutProcess ended here ...");
			 System.out.println("checkoutProcess Status:"+objStatus);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}
	

}
