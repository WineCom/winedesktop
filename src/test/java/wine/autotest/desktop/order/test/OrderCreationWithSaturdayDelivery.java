package wine.autotest.desktop.order.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;

public class OrderCreationWithSaturdayDelivery extends OrderCreation{


	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderCreationAddprodTocrt() {
		String objStatus = null;
			try {

			UIFoundation.waitFor(3L);
       
			  driver.get("https://qwww.wine.com/product/bogle-cabernet-sauvignon-2018/676592");
			           
            UIFoundation.waitFor(2L);
            if(UIFoundation.isDisplayed(ListPage.btnAddToCart))
            {
            	 objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
            }
            else 
            {
            	driver.get("https://qwww.wine.com/product/rodney-strong-reserve-cabernet-sauvignon-2016/577774");
            	UIFoundation.waitFor(2L); 
            	if(UIFoundation.isDisplayed(ListPage.btnAddToCart))
            	{
            		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
            	}
            	else
            	{
            		driver.get("https://qwww.wine.com/product/substance-vineyard-collection-powerline-cabernet-sauvignon-2017/671474");
            		UIFoundation.waitFor(2L);            	
            		objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
            	}
            }  
			
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}


	/*********************************************************************************
	 * Method Name			: checkoutUsingOverNightAM()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				:  The purpose of this method is to Create an order with saturday delivery
	 * 
	 **********************************************************************************
	 */

	public static String checkoutUsingOverNightAM() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "checkoutUsingOverNightAM.jpeg";
		
		try {
			log.info("checkout Using OverNight AM Shipping method started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout)); 
			UIFoundation.waitFor(7L);  
			//   ApplicationDependent.recipientEdit(driver);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkChangeDeliveryDate)); 
			UIFoundation.waitFor(1L);  
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelShipMethodOverNightAM));
			UIFoundation.waitFor(2L);
			// 	 UIFoundation.isDisplayed(driver, strObjectName)

			if (UIFoundation.isDisplayed(FinalReviewPage.SelectDateSaturday)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.SelectDateSaturday));
				UIFoundation.waitFor(1L);
			}
			else
			if  (UIFoundation.isDisplayed(FinalReviewPage.SelectSecondSaturday)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.SelectSecondSaturday));
				UIFoundation.waitFor(1L);  				
			}
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(4L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVVR));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(5L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				
				}

			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			log.info("checkout Using OverNight AM Shipping method started here ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With saturday delivery test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With saturday delivery test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method  " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			// ReportUtil.addTestStepsDetails("Order number is null.Order not placed
			// successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+ screenshotName, objDetail);
			return "Fail";
		}
	}
}
