package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.*;

public class VerifyOrderWithAmexCard extends OrderCreation {


		/***************************************************************************
		 * Method Name : checkoutProcess() Created By : Chandrashekhar Reviewed By :
		 *  Purpose : The purpose of this method is to capture the order number
		 * and purchased id
		 ****************************************************************************
		 */

	public static String checkoutProcess() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation35_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(2L);
			
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			 driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);

			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
			  objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
				
		}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCardAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "CardnumAE"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"MonthAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"YearAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvidAE"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				
				}
			/*UIFoundation.clearField(FinalReviewPage.txtCVV);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVV));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvidAE"));*/
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			System.out.println("orderNum :"+orderNum);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}

			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with existing account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with existing account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("There is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : SpiritcheckoutProcess() Created By : Ramesh S
	 * Reviewed By : Purpose :
	 ****************************************************************************
	 */

	public static String SpiritcheckoutProcess() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation95_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			 driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);
				}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
			  objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));				
			}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCardAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "CardnumAE"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"MonthAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"YearAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvidAE"));
				UIFoundation.waitFor(5L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
				}
				}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order is placed successfully";
				UIFoundation.getOrderNumber(orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(20L);
			String CreditName=UIFoundation.getText(OrderDetailsPage.spnCrdCardName);
			String expCardName = verifyexpectedresult.AmexCard;
			System.out.println("CreditLastFour : "+CreditName);
			if(CreditName.equals(expCardName)) {
				objStatus+=true;
				String objDetail = expCardName+" Expected Credit card is dispalyed in Order History";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}
				else {
				objStatus+=false;
				String objDetail = "Expected Credit card is not dispalyed in Order History";
				UIFoundation.captureScreenShot(screenshotpath, objDetail);
				logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with existing account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with existing account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("There is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order number is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath, objDetail);
			return "Fail";
		}
	}
}





