package wine.autotest.desktop.order.test;

import java.io.IOException;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;

public class VerifyStewardshipProgramAddedToCart extends OrderCreation {
	
	/***************************************************************************
	 * Method Name : addStewardshipToCart() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addStewardshipToCart() {
		String objStatus = null;
		String shippingAndHandling=null;
		String stewardshipPrice=null;
		String stewardshipDiscount=null;
		String stewardshipText=null;
		String expectedStewardshipSave=null;
		String expectedStewardshipText=null;
		String screenshotName = "Scenarios__Screenshot.jpeg";
			
		try {
			log.info("The execution of the method addStewardshipToCart started here ...");
			UIFoundation.waitFor(3L);
			expectedStewardshipSave=verifyexpectedresult.stewardshipSave;
			expectedStewardshipText=verifyexpectedresult.stewardshipText;
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			System.out.println("Shipping and handling : "+shippingAndHandling);
			UIFoundation.waitFor(2L);
			stewardshipText=UIBusinessFlows.stewardshipSection();
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnStewardshipConfirm));
			UIFoundation.waitFor(20L);
			if(!UIFoundation.isDisplayed(CartPage.spnStewardshipDiscountPrice)){
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkAddStewardshipMember));
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnStewardshipConfirm));
				UIFoundation.waitFor(2L);
			}
			stewardshipPrice=UIFoundation.getText(CartPage.spnStewardshipDiscountPrice);
			stewardshipDiscount=stewardshipPrice.replaceAll("\\(|\\)", "");
			System.out.println("Stewardship Savings : "+stewardshipDiscount);
			if(stewardshipDiscount.contains(shippingAndHandling))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Shipping and Handling and stewardship discount matched.", "Pass", "");
				System.out.println("Shipping and Handling and stewardship discount matched");
				
			}else
			{
				objStatus +=false;
				String objDetail="Shipping and Handling and stewardship discount did not match";
				UIFoundation.captureScreenShot(screenshotpath+"_Shipping", objDetail);
				System.out.println("Shipping and Handling and stewardship discount does not match");
				
			}
			if(stewardshipText.contains(expectedStewardshipText))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Actual stewarship text and expected stewardship text matched", "Pass", "");
				System.out.println("Actual stewarship text and expected stewardship text mathced.");
				
			}else
			{
				objStatus +=false;
				String objDetail="Actual stewarship text and expected stewardship text did not match";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Actual stewarship text and expected stewardship text doesnot match.");
			}
			log.info("The execution of the method addStewardshipToCart ended here ...");
			if (objStatus.contains("false")) {
				
				System.out.println("Verify 'Stewardship program' is added to cart test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify 'Stewardship program' is added to cart test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addStewardshipToCart "+ e);
			return "Fail";
		}
	}


}
