package wine.autotest.desktop.order.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import org.testng.annotations.Parameters; 
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.GlobalVariables;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.desktop.library.Initialize;

public class OrderCreation extends GlobalVariables {
	static int numberOfTestCasePassed=0;
	static int numberOfTestCaseFailed=0;
	public static String TestScriptStatus="";
	public static String startTime=null;
	public static String endTime=null;
	public static String objStatus = null;
	String teststarttime=null;
	/***************************************************************************
	 * Method Name			: loadFiles()
	 * Created By			: Ramesh S
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method  is to load .properties 
	 * 						   files.
	 ****************************************************************************
	 */
	
	@BeforeClass
	@Parameters({"browser", "testEnvironment", "teststate"})
	public static void loadFiles(String browser, String testEnvironment, String teststate)
	{ 
		String teststarttime=null;
		try
		{
				log=Logger.getLogger("wine Automation ...");
				teststarttime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				environmentUrl=testEnvironment;
				state=teststate;
				ReportUtil.createReport(OrderReportFileName, teststarttime,environmentUrl);
				UIBusinessFlows.deleteFile(driver);
				xmldata=new XMLData();
				url=UIFoundation.property(configurl);
				UIFoundation.screenshotFolderName("screenshots");
				ReportUtil.startScript("Scenarios");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: ExtendReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method  is to generate extend report
	 ****************************************************************************
	 */
	@BeforeTest
	public void ExtendReport()
	{
		try
		{
			//Extend Report
			ExtentHtmlReporter reporter =new ExtentHtmlReporter(ExtendOrderReportFileName);
	        report =new ExtentReports();
	        report.attachReporter(reporter);
	        reporter.config().setDocumentTitle("Platform Automation");
	        reporter.config().setReportName("Order Creation Execution Report");
	      
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	}

	/***************************************************************************
	 * Method Name			: LaunchBrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: This method is used to launch the browser.
	 ****************************************************************************
	 */
	@BeforeMethod
	@Parameters({"browser"})
	public static void Launchbrowser(String browser)
	{ 
			try
		{
				log=Logger.getLogger("Launch Browser ...");
				ReportUtil.deleteDescription();
				driver=Initialize.launchBrowser(browser);
				objStatus = null;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: TC01_orderCreationForValidateOrderSummary
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: this method is responsible for execution of all the 
	 * 						  scenarios.
	 ****************************************************************************
	 */
	
	
	@Test
	public static void TC01_orderCreationForValidateOrderSummary()
	{
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
              		objStatus += String.valueOf(Initialize.navigate());		
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationAddProduct.orderCreationAddprodTocrt());			
					objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**********************************************************************************				
	 * Method Name :OrderCreationWithGftMessageAndGftEmail() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */	
	@Test
	public static void TC02_OrderCreationWithGftMessageAndGftEmail()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());	
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithGftMessageAndGftEmail.orderWithGftMessageNGftEmail());	
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC03_orderCreationWithGiftsForExistingUser()
	{
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());	
					objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.addGiftsToTheCart());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC04_OrderCreationWithNewAccount()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : OrderCreationWithPromoCodeForExistingUser() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC05_orderCreationWithPromoCodeForExistingUser()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC06_OrderCreationWithDifferentVarietalProducts()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC06_OrderCreationWithDifferentVarietalProducts()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithDifferentVarietalProducts.addprodTocrt());
		            objStatus += String.valueOf(OrderCreationWithDifferentVarietalProducts.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC07_OrderCreationWithDifferentRegionProduct() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC07_OrderCreationWithDifferentRegionProduct()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.addRegionTocrt());
		            objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC08_OrderCreationWithVarietalRegionFeaturedProducts()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC08_OrderCreationWithVarietalRegionFeaturedProducts()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithVarietalRegionFeaturedProducts.addRegionNVaritalNFeaturedProductsTocrt());
		            objStatus += String.valueOf(OrderCreationWithVarietalRegionFeaturedProducts.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC09_OrderCreationWithNneWineProdToDryState()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC09_OrderCreationWithNneWineProdToDryState()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithNneWineProdToDryState.login());
		            objStatus += String.valueOf(OrderCreationWithNneWineProdToDryState.createNonWineOrderDryState());
		            objStatus += String.valueOf(OrderCreationWithNneWineProdToDryState.checkoutProcess());
					
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC10_OrderCreationByMovingItemsFromSaveForLaterToCart() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC10_OrderCreationByMovingItemsFromSaveForLaterToCart()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationByMovingItemsFromSaveForLaterToCart.moveItemFromSaveForLaterToCart());
		            objStatus += String.valueOf(OrderCreationByMovingItemsFromSaveForLaterToCart.saveForLater());        
		            objStatus += String.valueOf(OrderCreationByMovingItemsFromSaveForLaterToCart.checkoutProcess());;
		            System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC11_OrderCreationWithPresaleOrder()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC11_OrderCreationWithPresaleOrder()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithPresaleOrder.addProductPresale());                    
		            objStatus += String.valueOf(OrderCreationWithPresaleOrder.checkoutProcess());
		            System.out.println("objStatus :"+objStatus);
		        endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC12_OrderCreationWithPresaleAndRegulerProducts() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC12_OrderCreationWithPresaleAndRegulerProducts()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithPresaleAndShiptoday.addRegulerProductAndPresale());                             
		            objStatus += String.valueOf(OrderCreationWithPresaleAndShiptoday.checkoutProcess());
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC13_orderCreationWithGiftCertificatesForExistinguser() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC13_orderCreationWithGiftCertificatesForExistinguser()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.checkoutProcess());
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name : TC14_OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC14_OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag.OrderCreationWithSilverSheerGftBag());	
					 System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC15_OrderCreationWithGftMsgNGftEmailAlongWithNaturalBurlapGftBag()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC15_OrderCreationWithGftMsgNGftEmailAlongWithNaturalBurlapGftBag()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithNaturalBurlapGftBag.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithNaturalBurlapGftBag.OrderCreatioWithNaturalBurlapGftBag());	
					 System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC16_OrderCreationWithGftMsgNGftEmailAlongWithRedVelvetGftBag()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC16_OrderCreationWithGftMsgNGftEmailAlongWithRedVelvetGftBag()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method);
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithRedVelvetGftBag.OrderCreatioWithRedVelvetGftBag());	
					 System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC17_OrderCreationWithGftProductsAlongWithGftEmailNGftMssg()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC17_OrderCreationWithGftProductsAlongWithGftEmailNGftMssg()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithGftProductsAlongWithGftEmailNGftMssg.addGiftsToTheCart());	
					objStatus += String.valueOf(OrderCreationWithGftProductsAlongWithGftEmailNGftMssg.orderCreationWithGftProdNGftEmailNGftMsg());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC18_OrderCreationWithExistingFacebookUser() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC18_OrderCreationWithExistingFacebookUser()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithExistingFacebookUser.loginWithFacebook());	
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithExistingFacebookUser.checkoutProcess());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC19_OrderCreationWithStandardShippingMethodAsStewardshipUser()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC19_OrderCreationWithStandardShippingMethodAsStewardshipUser()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.stewardLogin());	
					//objStatus += String.valueOf(UIBusinessFlows.addprodTocrt());
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.searchProductWithProdName());	
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.checkoutUsingStandardShipping());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC20_OrderCreationWithOvernightAmShippingMethodAsStewardshipUser()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC20_OrderCreationWithOvernightAmShippingMethodAsStewardshipUser()
	{
		
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsStewardshipUser.stewardLogin());	
					objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsStewardshipUser.searchProductWithProdName());	
					objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsStewardshipUser.checkoutUsingOverNightAM());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC21_OrderCreationWithOverNightPMAsStewardshipUser()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC21_OrderCreationWithOverNightPMAsStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsStewardshipUser.stewardLogin());	
					objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsStewardshipUser.searchProductWithProdName());	
					objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsStewardshipUser.checkoutUsingOverNightPM());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC22_OrderCreationWith2DayShippingMethodAsAStewardshipUser() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC22_OrderCreationWith2DayShippingMethodAsAStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsAStewardshipUser.stewardLogin());	
					objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsAStewardshipUser.searchProductWithProdName());	
					objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsAStewardshipUser.checkoutUsing2dayShippingMethod());	
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC23_OrderCreationWithStandardShippingMethodAsNnStewardshipUser() 
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC23_OrderCreationWithStandardShippingMethodAsNnStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsNnStewardshipUser.searchProductWithProdName());
					objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsNnStewardshipUser.checkoutUsingStandardShipping());
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC24_OrderCreationWithOvernightAmShippingMethodAsNnStewardshipUser()
	  * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC24_OrderCreationWithOvernightAmShippingMethodAsNnStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsNnStewardshipUser.searchProductWithProdName());
					objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsNnStewardshipUser.checkoutUsingOverNightAM());
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC25_OrderCreationWithOvernightAmShippingMethodAsNnStewardshipUser()
	 * Created By  : Ramesh S 
	 * Reviewed By : 
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC25_OrderCreationWithOvernightPmShippingMethodAsNnStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsNnStewardshipUser.searchProductWithProdName());
					objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsNnStewardshipUser.checkoutUsingOverNightPM());
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC26_OrderCreationWith2DayShippingMethodAsNnStewardshipUser() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC26_OrderCreationWith2DayShippingMethodAsNnStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
		
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsNnStewardshipUser.login());
					objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsNnStewardshipUser.searchProductWithProdName());
					objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsNnStewardshipUser.checkoutUsing2dayShippingMethod());
					System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC27_OrderCreationWithStewardshipAccount() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC27_OrderCreationWithStewardshipAccount()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithStewardshipAccount.stewardLogin());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());	
					objStatus += String.valueOf(OrderCreationWithStewardshipAccount.checkoutProcess());
					objStatus += String.valueOf(Initialize.closeApplication());
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC28_OrderCreationWithOrderAgainToTheSameLocation() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC28_OrderCreationWithOrderAgainToTheSameLocation()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithOrderAgainToTheSameLocation.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	                objStatus += String.valueOf(OrderCreationWithOrderAgainToTheSameLocation.checkoutProcess());
	                objStatus += String.valueOf(OrderCreationWithOrderAgainToTheSameLocation.orderAgainToTheSameLocation()); 
//	                objStatus += String.valueOf(Initialize.closeApplication());
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC29_OrderCreationWithOrderAgainToTheDiffLocation() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC29_OrderCreationWithOrderAgainToTheDiffLocation()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithOrderAgainToTheDiffLocation.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(OrderCreationWithOrderAgainToTheDiffLocation.checkoutProcess());
	                objStatus += String.valueOf(OrderCreationWithOrderAgainToTheDiffLocation.orderAgainToTheDifferentLocation());   
	                objStatus += String.valueOf(Initialize.closeApplication());
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC30_orderCreationWihNewAccountBillingAddress() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC30_orderCreationWihNewAccountBillingAddress()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
		 			objStatus += String.valueOf(OrderCreationWithNewAccountBillingAddress.shippingDetails());
		 			objStatus += String.valueOf(OrderCreationWithNewAccountBillingAddress.addNewCreditCard());
		 			objStatus += String.valueOf(Initialize.closeApplication());
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC31_OrderCreationWithNewHomeShippingAddress() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC31_OrderCreationWithNewHomeShippingAddress()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewHomeShippingAddress.shippingDetails());
	 	 			System.out.println("objStatus :"+objStatus);
		            
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC32_OrderCreationWithNewLocalPickUpLocation() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC32_OrderCreationWithNewLocalPickUpLocation()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithNewLocalPickUpLocation.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewLocalPickUpLocation.shippingDetails());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC33_VerifyMaximumQuantityOrderPlaced() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC33_VerifyMaximumQuantityOrderPlaced()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(VerifyMaximumQuantityOrderPlaced.login());
	 	 			objStatus += String.valueOf(VerifyMaximumQuantityOrderPlaced.limitReached());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyMaximumQuantityOrderPlaced.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC34_OrderCreationWithGiftCards() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC34_OrderCreationWithGiftCards()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              System.out.println("Executing "+Method);
              logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithGiftCards.addGiftCards());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(OrderCreationWithGiftCards.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC35_VerifyOrderWithAmexCard() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC35_VerifyOrderWithAmexCard()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyOrderWithAmexCard.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC36_VerifyOrderWithVisaCard() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC36_VerifyOrderWithVisaCard()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyOrderWithVisaCard.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC37_VerifyOrderWithMasterCard() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC37_VerifyOrderWithMasterCard()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyOrderWithMasterCard.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/***************************************************************************
	 * Method Name : TC38_VerifyOrderWithDiscoverCard() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC38_VerifyOrderWithDiscoverCard()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyOrderWithDiscoverCard.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC39_VerifyCombinedProductsOrder() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC39_VerifyCombinedProductsOrder()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(VerifyCombinedProductsOrder.addMyWineProdTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(VerifyCombinedProductsOrder.checkoutProcess());
	 	 			System.out.println("objStatus :"+objStatus);
		            endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC40_OrderCreationExistingNnStewardshipUser() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC40_OrderCreationExistingNnStewardshipUser()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 				objStatus += String.valueOf(OrderCreationWithExistingNnStewardshipUser.checkoutOrder());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC41_OrderCreationWithExistingLocalPickUpLocation() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC41_OrderCreationWithExistingLocalPickUpLocation()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithExistingLocalPickUpLocation.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(OrderCreationWithExistingLocalPickUpLocation.shippingDetails());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC42_orderCreationWithExistingHomeShippingAddress() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC42_orderCreationWithExistingHomeShippingAddress()
	{
		
		try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithExistingLocalPickUpLocation.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(OrderCreationWithExistinghomeShippingAddress.checkoutProcess());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC43_orderCreationWithDifferentBillingAddress() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC43_orderCreationWithDifferentBillingAddress()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithExistingLocalPickUpLocation.login());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(OrderCreationWithDifferentStateBillingAddress.checkoutProcess());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC44_OrderCreationWithGiftProducts() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC44_OrderCreationWithGiftProducts()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
	 	 			objStatus += String.valueOf(OrderCreationWithGiftProducts.addGiftProducts());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(OrderCreationWithGiftProducts.checkoutProcess());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC45_OrderCreationWithRegulerProductsGiftsAndGiftCards() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC45_OrderCreationWithRegulerProductsGiftsAndGiftCards()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
	 	 			objStatus += String.valueOf(OrderCreationWithRegulerProductsAndGiftCards.addGiftCards());
	 	 			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
	 	 			objStatus += String.valueOf(OrderCreationWithRegulerProductsAndGiftCards.checkoutProcess());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC46_OrderCreationWithDelayedDeliveryAndRegulerProducts() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC46_OrderCreationWithDelayedDeliveryAndRegulerProducts()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
              objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithDelayedDeliveryAndShipToday.addRegulerProductAndDelayedDelivery());                             
	 	            objStatus += String.valueOf(OrderCreationWithDelayedDeliveryAndShipToday.checkoutProcess());
	 	 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC47_orderCreationWithSaturdayDelivery() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC47_orderCreationWithSaturdayDelivery()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithSaturdayDelivery.orderCreationAddprodTocrt());
		 			objStatus += String.valueOf(OrderCreationWithSaturdayDelivery.checkoutUsingOverNightAM());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC48_SpiritOrderCreationForExistingNnStewardMember() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC48_SpiritOrderCreationForExistingNnStewardMember()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());			
					objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC49_SpiritOrderCreationForExistingStewardMember() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC49_SpiritOrderCreationForExistingStewardMember()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.Stewardshiplogin());
					objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());			
					objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC50_SpiritorderCreationForNewUser() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC50_SpiritorderCreationForNewUser()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
					objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());	
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC51_SpiritorderCreationForNewStewardshipUser() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC51_SpiritorderCreationForNewStewardshipUser()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
              methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
					objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());	
					objStatus += String.valueOf(VerifyStewardshipProgramAddedToCart.addStewardshipToCart());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC52_SpiritorderCreationForExistingFacebookUser() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC52_SpiritorderCreationForExistingFacebookUser()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(OrderCreationWithExistingFacebookUser.loginWithFacebook());
					objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());			
					objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC53_SpiritorderCreationDiffSpiritProd() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC53_SpiritorderCreationDiffSpiritProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
              logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(UIBusinessFlows.addTocrtWithDiffSpiritsProd());			
					objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC54_SpiritorderCreationDiffRegionSpiritProd() Created By :
	 * Ramesh S Reviewed By : , Purpose :
	 ***************************************************************************
	 */
	@Test
	public static void TC54_SpiritorderCreationDiffRegionSpiritProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.addDiffRegionProdTocrt());			
					objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.checkoutProcess());
		 			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC55_SpiritorderCreationDiffRegionandBottelSizeSpiritProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC55_SpiritorderCreationDiffRegionandBottelSizeSpiritProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
           methodName = m1[1]; 
             logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(UIBusinessFlows.login());
					objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.addDiffRegionProdwithDiffeBottelsizeTocrt());			
					objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC56_SpiritorderCreationDiffRegionandFeaturedSpiritProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC56_SpiritorderCreationDiffRegionandFeaturedSpiritProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.addDiffRegionProdwithDiffeFeaturedprodTocrt());			
				objStatus += String.valueOf(OrderCreationWithDifferentRegionProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC57_SpiritorderCreationAddtoCartfromSaveforLater()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC57_SpiritorderCreationAddtoCartfromSaveforLater()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationByMovingItemsFromSaveForLaterToCart.moveItemFromSaveForLaterToCart());			
			    objStatus += String.valueOf(OrderCreationByMovingItemsFromSaveForLaterToCart.saveForLater());        
			    objStatus += String.valueOf(OrderCreationByMovingItemsFromSaveForLaterToCart.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC58_SpiritorderCreationAddtoCartfromPIPpage()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC58_SpiritorderCreationAddtoCartfromPIPpage()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
            
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addTocrtFromPIPpage());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC59_SpiritorderCreationAddtoCartfromMyWinepage()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC59_SpiritorderCreationAddtoCartfromMyWinepage()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
           
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addTocrtFromMyWinePage());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC60_SpiritorderCreationwithDiffGiftCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC60_SpiritorderCreationwithDiffGiftCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
          
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addGiftCertTocrt());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC61_SpiritorderCreationwithDiffGiftBaskets()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC61_SpiritorderCreationwithDiffGiftBaskets()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addGiftBasketTocrt());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC62_SpiritorderwithWineAccessories()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC62_SpiritorderwithWineAccessories()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftProducts.addGiftAccessories());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC63_SpiritorderwithGiftCardandWineProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC63_SpiritorderwithGiftCardandWineProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; 
            System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftCards.addGiftCardswithWineProd());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcessSplitOrder());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC64_SpiritorderwithGiftBasketsandWineProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC64_SpiritorderwithGiftBasketsandWineProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftCards.addGiftBasketwithWineProd());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC65_SpiritorderwithWineAccessoriesandWineProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC65_SpiritorderwithWineAccessoriesandWineProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftCards.addWineAccessorieswithWineProd());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC66_SpiritorderwithSpiritAccessoriesandWineProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC66_SpiritorderwithSpiritAccessoriesandWineProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftCards.addSpiritAccessorieswithWineProd());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC67_SpiritorderwithGiftProdandGiftCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC67_SpiritorderwithGiftProdandGiftCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftCards.addGiftProdGiftCard());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC68_SpiritorderwithGiftProdGiftCardandWineProd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC68_SpiritorderwithGiftProdGiftCardandWineProd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithGiftCards.addGiftProdGiftCardWineProd());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC69_SpiritorderwithCocktailRecipiesd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC69_SpiritorderwithCocktailRecipiesd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithSpiritsProductandCocktail.addSpiritandCocktailprodTocrt());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC70_SpiritorderwithDiffBottleSize()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC70_SpiritorderwithDiffBottleSize()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(VerifyCombinationProductsOrder.addDiffProdwithDiffeBottelsizeTocrt());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC71_SpiritorderwithRareProdNGftEmailGftMsg()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC71_SpiritorderwithRareProdNGftEmailGftMsg()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(OrderCreationWithRareProductsWithGftEmailGftMssg.addRareProdtoCart());
				objStatus += String.valueOf(OrderCreationWithRareProductsWithGftEmailGftMssg.orderCreationWithGftEmailGftMsg());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC72_SpiritorderwithGftEmailGftMsgNGifBag()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC72_SpiritorderwithGftEmailGftMsgNGifBag()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addTocrtWithDiffSpiritsProd());
				objStatus += String.valueOf(OrderCreationWithRareProductsWithGftEmailGftMssg.orderCreationWithGftEmailGftMsgandNoBag());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC73_SpiritorderwithNGifBag()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC73_SpiritorderwithNGifBag()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addTocrtWithDiffSpiritsProd());
				objStatus += String.valueOf(OrderCreationWithRareProductsWithGftEmailGftMssg.orderCreationWithNoBagOption());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC74_SpiritorderwithMaximumQty()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC74_SpiritorderwithMaximumQty()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(VerifyMaximumQuantityOrderPlaced.MaximumQuantity());
				objStatus += String.valueOf(OrderCreationAddProduct.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC75_SpiritorderCreationWithPromoCode()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC75_SpiritorderCreationWithPromoCode()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
				objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.captureOrdersummary());
				objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC76_SpiritOrderCreationWithGiftCert()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC76_SpiritOrderCreationWithGiftCert()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
				objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.OrderWithGiftCert());
				objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcessGiftCert());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC77_SpiritOrderCreationWithPromoCodeandGiftCert()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC77_SpiritOrderCreationWithPromoCodeandGiftCert()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
				objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.PromoCodeWithGiftCert());
				objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcessGiftCertandPromo());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC78_SpiritorderCreationWithExistingHomeAdd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC78_SpiritorderCreationWithExistingHomeAdd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithExistinghomeShippingAddress.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC79_SpiritorderCreationWithNewHomeAdd()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC79_SpiritorderCreationWithNewHomeAdd()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithNewHomeShippingAddress.shippingDetails());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC80_SpiritorderCreationWithExistingFedExLocalPickUp()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC80_SpiritorderCreationWithExistingFedExLocalPickUp()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithExistingLocalPickUpLocation.shippingDetails());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC81_SpiritorderCreationWithNewFedExLocalPickUp()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC81_SpiritorderCreationWithNewFedExLocalPickUp()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithNewLocalPickUpLocation.shippingDetails());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC82_SpiritorderCreationWithGifMsgGifEmailSilverSheer()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC82_SpiritorderCreationWithGifMsgGifEmailSilverSheer()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithSilverSheerGftBag.OrderCreationWithSilverSheerGftBag());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC83_SpiritorderCreationWithGifMsgGifEmailNaturalBurlap()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC83_SpiritorderCreationWithGifMsgGifEmailNaturalBurlap()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithNaturalBurlapGftBag.OrderCreatioWithNaturalBurlapGftBag());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC84_SpiritorderCreationWithGifMsgGifEmailRedVelvet()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC84_SpiritorderCreationWithGifMsgGifEmailRedVelvet()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithGftMsgNGftEmailAlongWithRedVelvetGftBag.OrderCreatioWithRedVelvetGftBag());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC85_SpiritorderCreationWithStdShippingNnSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC85_SpiritorderCreationWithStdShippingNnSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsNnStewardshipUser.checkoutUsingStandardShipping());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC86_SpiritorderCreationWithOverNightPMNnSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC86_SpiritorderCreationWithOverNightPMNnSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsNnStewardshipUser.checkoutUsingOverNightPM());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC87_SpiritorderCreationWithOverNightAMNnSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC87_SpiritorderCreationWithOverNightAMNnSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsNnStewardshipUser.checkoutUsingOverNightAM());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC88_SpiritorderCreationWith2DaysShippingNnSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC88_SpiritorderCreationWith2DaysShippingNnSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsNnStewardshipUser.checkoutUsing2dayShippingMethod());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC89_SpiritorderCreationWithStdShippingAsSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC89_SpiritorderCreationWithStdShippingAsSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.Stewardshiplogin());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithStandardShippingMethodAsStewardshipUser.checkoutUsingStandardShipping());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC90_SpiritorderCreationWithOverNightPMAsSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC90_SpiritorderCreationWithOverNightPMAsSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.Stewardshiplogin());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithOvernightPmShippingMethodAsStewardshipUser.checkoutUsingOverNightPM());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC91_SpiritorderCreationWithOverNightAMAsSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC91_SpiritorderCreationWithOverNightAMAsSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.Stewardshiplogin());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithOvernightAmShippingMethodAsStewardshipUser.checkoutUsingOverNightAM());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC92_SpiritorderCreationWith2DaysShippingAsSteward()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC92_SpiritorderCreationWith2DaysShippingAsSteward()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.Stewardshiplogin());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWith2DayShippingMethodAsAStewardshipUser.checkoutUsing2dayShippingMethod());	
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC93_SpiritorderCreationWithSaturdayDelivery()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC93_SpiritorderCreationWithSaturdayDelivery()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithSaturdayDelivery.checkoutUsingOverNightAM());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC94_SpiritorderCreationWithNewCreditCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC94_SpiritorderCreationWithNewCreditCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.userProfileCreation());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
				objStatus += String.valueOf(OrderCreationWithNewAccount.SpiritaddNewCreditCard());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC95_SpiritorderCreationWithNewAmexCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC95_SpiritorderCreationWithNewAmexCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(VerifyOrderWithAmexCard.SpiritcheckoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC96_SpiritorderCreationWithNewVisaCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC96_SpiritorderCreationWithNewVisaCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(VerifyOrderWithVisaCard.SpiritcheckoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC97_SpiritorderCreationWithNewMasterCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC97_SpiritorderCreationWithNewMasterCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(VerifyOrderWithMasterCard.SpiritcheckoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC98_SpiritorderCreationWithNewDiscoverCard()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC98_SpiritorderCreationWithNewDiscoverCard()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(VerifyOrderWithDiscoverCard.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC99_SpiritorderCreationWithDifferentBillingAddress()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC99_SpiritorderCreationWithDifferentBillingAddress()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithDifferentStateBillingAddress.checkoutProcess());
				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC100_SpiritOrderCreationWithOrderAgainToTheSameLocation()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC100_SpiritOrderCreationWithOrderAgainToTheSameLocation()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithOrderAgainToTheSameLocation.checkoutProcess());
                objStatus += String.valueOf(OrderCreationWithOrderAgainToTheSameLocation.orderAgainToTheSameLocation()); 
                endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC101_SpiritOrderCreationWithOrderAgainToTheDiffLocation()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC101_SpiritOrderCreationWithOrderAgainToTheDiffLocation()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.login());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithOrderAgainToTheDiffLocation.checkoutProcess());
                objStatus += String.valueOf(OrderCreationWithOrderAgainToTheDiffLocation.orderAgainToTheDifferentLocation());   
                endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC102_SpiritorderCreationWithExistingWalgreensLocalPickUp()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC102_SpiritorderCreationWithExistingWalgreensLocalPickUp()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.Walgreenslogin());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithExistingLocalPickUpLocation.shippingDetails());
 				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name : TC103_SpiritorderCreationWithNewWalgreensLocalPickUp()
	 * Created By  : Ramesh S 
	 * Reviewed By :
	 * Purpose     :
	 ***************************************************************************
	 */
	@Test
	public static void TC103_SpiritorderCreationWithNewWalgreensLocalPickUp()
	{
	try		
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
            String[] m1=  Method.split("_");
            testCaseID = m1[0];
            methodName = m1[1]; System.out.println("Executing "+Method);
            logger=report.createTest(Method);
				objStatus += String.valueOf(Initialize.navigate());	 
				objStatus += String.valueOf(UIBusinessFlows.Walgreenslogin());
				objStatus += String.valueOf(UIBusinessFlows.addSpiritsprodTocrt());
				objStatus += String.valueOf(OrderCreationWithNewLocalPickUpLocation.shippingDetails());
 				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: Closebrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser.
	 ****************************************************************************
	 */
	@AfterMethod
	public static void Closebrowser()
	{ 
			try
		{
				if (objStatus.contains("Fail"))
				{
				 	numberOfTestCaseFailed++;
				 	ReportUtil.writeTestResults(testCaseID, methodName, "Fail", startTime, endTime);
				}
				else
				{
					numberOfTestCasePassed++;
					ReportUtil.writeTestResults(testCaseID, methodName, "Pass", startTime, endTime);
				}
				log=Logger.getLogger("Close Browser ...");
      			Initialize.closeApplication();
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: CloseBrowserAndEndReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser and end the report.
	 * ****************************************************************************
	 */
	 @AfterTest
     public void CloseBrowserAndEndReport()
     {
		 try {
		
			 	report.flush(); 
		 }catch(Exception e)
			{
				e.printStackTrace();
			}
           }

		/***************************************************************************
		 * Method Name			: endScenariosExecution()
		 * Created By			: Vishwanath Chavan
		 * Reviewed By			: Ramesh,KB
		 * Purpose				: This method responsible for updating the end time to 
		 * 						  customized html report
		 ****************************************************************************
		 */
	@AfterSuite
	public static void endScenariosExecution()
	{
		String endTime=null;
		try
		{

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			ReportUtil.endScript(numberOfTestCasePassed,numberOfTestCaseFailed);
			ReportUtil.updateEndTime(endTime);
			UIFoundation.waitFor(3L);
			System.out.println("=======Order creation test case details==============");
			System.out.println("Total no test case Passed:"+numberOfTestCasePassed);
			System.out.println("Total no test case Failed:"+numberOfTestCaseFailed);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
