package wine.autotest.desktop.order.test;



import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.pages.ListPage;
import wine.autotest.desktop.order.pages.*;



public class AddProdcutsToCartCaptureOrder extends OrderCreation {

	
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ***********************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {			

			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 20);
			}
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPinotNoir));//lnkSangviovese
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ListPage.lnkQAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkQAUserPopUp));
				UIFoundation.webDriverWaitForElement(ListPage.lnkQAUserPopUp, "Invisible", "", 20);
			}
	
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
        	
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			
			if (addToCart2.contains("Add to Cart")) {
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			UIFoundation.waitFor(1L);
			}
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnksyrah));
			UIFoundation.waitFor(5L);
			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkThirdProductToCart);
				UIFoundation.waitFor(1L);
			}
			
			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.clckObject(ListPage.lnkFourthProductToCart);
				UIFoundation.waitFor(1L);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(ListPage.btnCartCount)) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			}
			else {
				UIFoundation.scrollUp(driver);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			}
			UIFoundation.waitFor(5L);


			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(!UIFoundation.isDisplayed(ListPage.btnCartCount)){
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
				UIFoundation.waitFor(3L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
				UIFoundation.waitFor(3L);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductToCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductToCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductToCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductToCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductToCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductToCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductToCart);
				System.out.println("5) "+products5);

			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForExistingUser() {
		String products1=null;
		
		try {
			
			System.out.println("=======================Products Added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.lnkFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.lnkFirstProductToCart);
				System.out.println("1) "+products1);

			}
			else
			{
				System.out.println("No products are added into the cart");
			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("=======================Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHandling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productDetailsPresentInCart() {
		//String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			/*if(!ApplicationIndependent.getText(driver, "FirstProductInCart").contains("Fail"))
			{
				products1=ApplicationIndependent.getText(driver, "FirstProductInCart");
				System.out.println("1) "+products1);
	
			}*/
			
			if(!UIFoundation.getText(ListPage.lnkSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.lnkSecondProductToCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.lnkThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.lnkThirdProductToCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFourthProductToCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.lnkFourthProductToCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.lnkFifthProductToCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.lnkFifthProductToCart);
				System.out.println("5) "+products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	

}
