package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import wine.autotest.desktop.order.pages.*;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.*;

public class OrderCreationWithNewLocalPickUpLocation extends OrderCreation {

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.btnAccount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkAccSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginEmail, "fedexloactions"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtLoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignIn));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: shippingDetails()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus=null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation3281_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method shippingDetails started here ...");
			//
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkChangeAddress);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(1L);
			}			
		
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			if(UIFoundation.isDisplayed(FinalReviewPage.rdopickupFormHeader)){
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(8L);
			}
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexContinue));
			UIFoundation.waitFor(8L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
			}
			}
			
		if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              " + subTotal);
		System.out.println("Shipping & Handling:   " + shippingAndHandling);
		System.out.println("Sales Tax:             " + salesTax);
		System.out.println("Total:                 " + total);
		UIFoundation.waitFor(2L);
		
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.waitFor(10L);
		orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
		if (orderNum != "Fail") {
			objStatus += true;
			String objDetail = "Order is placed successfully";
			UIFoundation.getOrderNumber(orderNum);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Order is placed successfully: " + orderNum);
		} else {
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With New Local Pickup Address test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With New Local Pickup Address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
		
	/***************************************************************************
	 * Method Name			: shippingDetailsWalgreens()
	 * Created By			: Ramesh S
	 * Reviewed By			:
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 ****************************************************************************
	 */

	public static String shippingDetailsWalgreens() {
		String objStatus=null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method shippingDetails started here ...");
			//
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkChangeAddress);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(1L);
			}			
		
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.rdoShipToFedEx));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearch));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			if(UIFoundation.isDisplayed(FinalReviewPage.rdopickupFormHeader)){
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSave));
			UIFoundation.waitFor(8L);
			}
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexContinue));
			UIFoundation.waitFor(8L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(driver, "paywithcardR"));
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
			}
			}
			
		if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
		shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
		total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
		salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
		System.out.println("Subtotal:              " + subTotal);
		System.out.println("Shipping & Handling:   " + shippingAndHandling);
		System.out.println("Sales Tax:             " + salesTax);
		System.out.println("Total:                 " + total);
		UIFoundation.waitFor(2L);
		
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
		UIFoundation.waitFor(10L);
		orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
		if (orderNum != "Fail") {
			objStatus += true;
			String objDetail = "Order is placed successfully";
			UIFoundation.getOrderNumber(orderNum);
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			System.out.println("Order is placed successfully: " + orderNum);
		} else {
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		}			

			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With New Local Pickup Address test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With New Local Pickup Address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
		

	}
