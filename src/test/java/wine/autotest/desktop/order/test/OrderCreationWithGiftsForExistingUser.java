package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import wine.autotest.desktop.order.pages.*;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.ReportUtil;

public class OrderCreationWithGiftsForExistingUser extends OrderCreation {
	
	/***************************************************************************
	 * Method Name			: addGiftsToTheCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String addGiftsToTheCart()
	{
		String objStatus=null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			objStatus+=String.valueOf(UIFoundation.clckObject(ListPage.txtWhiteWine));
			UIFoundation.waitFor(5L);
			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(2L);
				UIFoundation.javaScriptClick(ListPage.lnkFirstProductToCart);
			}

			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.javaScriptClick(ListPage.lnkSecondProductToCart);				
			}

			addToCart3 = UIFoundation.getText(ListPage.lnkThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.javaScriptClick(ListPage.lnkThirdProductToCart);
			}

			addToCart4 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.javaScriptClick(ListPage.lnkFourthProductToCart);
			}

			addToCart5 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFifthProductToCart);
			}
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnHeaderText);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			UIFoundation.waitFor(0);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				UIFoundation.clickObject(FinalReviewPage.txtCVVR);
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
			UIFoundation.clickObject(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			UIFoundation.clearField(FinalReviewPage.txtCVV);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(15L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order is placed successfully :";
				UIFoundation.getOrderNumber(orderNum);
				logger.pass(objDetail+orderNum);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: "+orderNum);			
			}else
			{
				objStatus+=false;
		    	String objDetail="Order is null and cannot placed successfully";
		    	logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Order creation with Gifts for existing user  test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Order creation with Gifts for existing user test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}
}
