package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.*;


public class OrderCreationWithExistinghomeShippingAddress extends OrderCreation {

	
	
	
/*	
	*//***************************************************************************
	 * Method Name			: shippingDetails()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: 
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 ****************************************************************************
	 *//*
	public static String shippingDetails(WebDriver driver) {
		String objStatus = null;
		   String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
			
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
		
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CheckoutButton"));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "ShippingTyp"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(driver, "FirstName", "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "LastName", "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "StreetAddress", "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(driver, "obj_State", "State"));
			UIFoundation.clickObject(driver, "obj_State");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "ZipCode", "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(driver, "PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "ShipContinueButton"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "SuggestedAddress"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(driver, "VerifyContinueButton")){
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButton"));
				UIFoundation.waitFor(10L);
			}

			if(UIFoundation.isDisplayed(driver, "localPickUp")){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Local pickup icon is displayed next to pickup location address.", "Pass", "");
			}else{
				objStatus+=false;
				//	System.out.println("Verify the header  in the 'About Stewardship' popup in Final Review section when the user has not enrolled stewardship.");
					String objDetail="Local pickup icon is not displayed next to pickup location address.";
					UIFoundation.captureScreenShot(driver, screenshotpath+"localPickUp", objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}*/

	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced4278_Screenshot.jpeg";
		
			
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHandling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			

           if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			}
                          UIFoundation.waitFor(2L);
			
		
//			UIFoundation.webDriverWaitForElement(driver, "SignInButton", "Invisible", "", 50);
			objStatus+=String.valueOf(UIBusinessFlows.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod))
	        {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
	        }
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
			    UIFoundation.waitFor(3L);
			}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
	                 
	          }
	        
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkPaymentCheckoutEdit));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaymentCVV, "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPaymentSave));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order is placed successfully";
			      UIFoundation.getOrderNumber(orderNum);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath, objDetail);
			       logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
			 }
						
			log.info("The execution of the method checkoutProcess ended here ...");	
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				return "Pass";
			}
			
	}
		catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}
	

}
