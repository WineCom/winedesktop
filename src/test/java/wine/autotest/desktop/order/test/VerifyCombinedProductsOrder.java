package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.fw.utilities.ReportUtil;

public class VerifyCombinedProductsOrder extends OrderCreation {
	
	/***************************************************************************
	 * Method Name			: addMyWineProdTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			:
	 * Purpose				: The purpose of this method is to Create an order with
	 *  Gift cards
	 ****************************************************************************
	 */
	
	
	public static String addMyWineProdTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMyWine));
			UIFoundation.waitFor(8L);

			addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstProductToCart));
			}		
			
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBisinessGiftTab));
			
		
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstGiftProduct);
			if (addToCart1.contains("Add to Cart")) {
				
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstGiftProduct);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnksecondGiftProduct);
			if (addToCart2.contains("Add to Cart")) {
				
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnksecondGiftProduct);
			}
			UIFoundation.waitFor(2L);
             /*objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.txtgifts));
			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkBirthdayGifts));*/
			
			driver.get("https://qwww.wine.com/list/gifts/birthday/7151-9009");
			
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstGiftProduct);
			if (addToCart1.contains("Add to Cart")) {
				
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstGiftProduct);
			}
			
			UIFoundation.waitFor(2L);
           /* objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.txtgifts));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkThankyouGift));*/
			
			driver.get("https://qwww.wine.com/list/gifts/thank-you/7151-9026");
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstGiftProduct);
			if (addToCart1.contains("Add to Cart")) {
				
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstGiftProduct);
			}
						
			UIFoundation.waitFor(2L);
           /* objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.txtgifts));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftBasket1));*/
			
			driver.get("https://qwww.wine.com/list/gifts/wine-and-food-gifts/7151-7152");
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstGiftProduct);
			if (addToCart1.contains("Add to Cart")) {
				
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstGiftProduct);
			}
			
			UIFoundation.waitFor(2L);
			 /*objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.txtgifts));
				UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftBasket2));*/
			
			driver.get("https://qwww.wine.com/list/gifts/food-and-chocolate-gifts/7151-8992");
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstGiftProduct);
			if (addToCart1.contains("Add to Cart")) {
				
				UIFoundation.waitFor(2L);
				UIFoundation.clckObject(ListPage.lnkFirstGiftProduct);
			}
			UIFoundation.waitFor(2L);
			 objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
				
				UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkwineSet));
			
		
			UIFoundation.waitFor(5L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstGiftProduct);
			if (addToCart1.contains("Add to Cart")) {
				
				UIFoundation.waitFor(2L);
				UIFoundation.clckObject(ListPage.lnkFirstGiftProduct);
			}
     		UIFoundation.waitFor(4L);
			
            objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkgifts));
			
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftCards));
	
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFourthProductToCart);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFourthProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFourthProductToCart));				
			}
			
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFifthProductToCart);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFifthProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFifthProductToCart));				
			}

			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.mouseHover(ListPage.lnkvarietal));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkCaberNet));
	
			UIFoundation.waitFor(3L);
        	addToCart1 = UIFoundation.getText(ListPage.lnkFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkFirstProductToCart);
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.lnkFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.lnkSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkSecondProductToCart);
				UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSecondProductToCart);
			}
			
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar
	 * Reviewed By			:
	 * Purpose				: The purpose of this method is to Create an order with
	 *  Gift cards
	 ****************************************************************************
	 */
	public static String checkoutProcess() {

		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation39_Screenshot.jpeg";
		
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(2L);
			
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			 driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);

			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				  objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
					
			}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
					WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
					if(!ele.isSelected())
					{
						 UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					
					}
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVV));
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(15L);
				orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				System.out.println("orderNum :"+orderNum);
				if (orderNum != "Fail") {
					objStatus += true;
					String objDetail = "Order is placed successfully";
					UIFoundation.getOrderNumber(orderNum);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Order is placed successfully: " + orderNum);
				} else {
					objStatus += false;
					String objDetail = "Order is null.Order not placed successfully";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}

			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with existing account test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with existing account test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("There is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
