package wine.autotest.desktop.order.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.order.test.OrderCreation;
import wine.autotest.desktop.order.pages.*;
import wine.autotest.desktop.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;


public class OrderCreationWithExistingNnStewardshipUser extends OrderCreation {

	
	/*********************************************************************************
	 * Method Name			: checkoutUsingStandardShipping()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				:  The purpose of this method is to Create an order with Existing non stewardship user

	 **********************************************************************************
	 */

	public static String checkoutOrder() {
	
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "checkoutUsingExistingnonStewardshipUser.jpeg";
	   
	        try {
	    	log.info("checkout Using checkoutUsingExistingNonStewardshipUser method started here");
	    	UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			 driver.navigate().refresh();
			UIFoundation.waitFor(8L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddPymtMethod)) {
				  objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddPymtMethod));
					
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtExpiryYear,"Year"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
					UIFoundation.waitFor(3L);
					WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
					if(!ele.isSelected())
					{
					UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
				    UIFoundation.waitFor(3L);
					}
					}
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtCVV));
				UIFoundation.clearField(FinalReviewPage.txtCVV);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			}
				System.out.println("============Order summary in the Final Review Page  ===============");
				subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
				shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
				total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
				salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
				System.out.println("Subtotal:              " + subTotal);
				System.out.println("Shipping & Handling:   " + shippingAndHandling);
				System.out.println("Sales Tax:             " + salesTax);
				System.out.println("Total:                 " + total);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(15L);
				orderNum = UIFoundation.getText(ThankYouPage.lnkOrderNumber);
				System.out.println("orderNum :"+orderNum);
				if (orderNum != "Fail") {
					objStatus += true;
					String objDetail = "Order is placed successfully";
					UIFoundation.getOrderNumber(orderNum);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Order is placed successfully: " + orderNum);
				} else {
					objStatus += false;
					String objDetail = "Order is null.Order not placed successfully";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					logger.fail(objDetail, MediaEntityBuilder.createScreenCaptureFromPath(UIFoundation.ExtendcaptureScreenShot(screenshotpath+screenshotName)).build());
				}		
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With ExistingnonStewardshipUser test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With ExistingnonStewardshipUser test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method  " + e);
			objStatus += false;
			String objDetail = "Order is null.Order not placed successfully";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	    }

}
