package wine.autotest.mobile.pages;

import org.openqa.selenium.By;

public class CartPage {

	public static By btnCheckout = By.xpath("//a[contains(text(),'Check')]");
	public static By btnObjCheckout = By.xpath("//a[text()='Check Out']");
	public static By btnCheckoutButton = By.xpath("//a[text()='Proceed to Checkout']");	
	public static By btnCheckoutAlter = By.xpath("//a[text()='Check Out']");
	public static By spnSubtotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_subTotalPrice']");
	public static By spnShippingHnadling = By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");
	public static By spnTotalBeforeTax = By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']");	
	public static By txtOrderSummarySalesTax = By.xpath("//tr[@class='orderSummary_row orderSummary_tax']");
	public static By txtOrderSummaryTaxTotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_taxTotal']");
	public static By spnPromeCodePrice = By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
	public static By spnThirdProductPrice = By.xpath("(//span[@class='productPrice_unitPrice'])[3]");
	public static By spnSecondProductPrice = By.xpath("(//span[@class='productPrice_unitPrice'])[2]");
	public static By spnRemoveThirdProductFromSave = By.xpath("(//span[@class='iconRoundedOutline'])[4]");
	
	public static By spnRemoveFirstProduct = By.xpath("(//span[@class='iconRoundedOutline'])[1]");
	public static By spnRemoveSecondProduct = By.xpath("(//span[@class='iconRoundedOutline'])[2]");
	public static By spnRemoveThirdProduct = By.xpath("(//span[@class='iconRoundedOutline'])[3]");
	public static By spnObjRemove = By.xpath("//div[@class='leftSlide leftSlideActive']");
	public static By spnRemove = By.xpath("(//span[@class='iconRoundedOutline'])[1]");	
	public static By btnContinueShopping = By.xpath("//a[text()='Continue Shopping']");
	public static By txtPromoCodeText = By.xpath("//input[@name='promoCode']");
	public static By btnPromoCodeApplyLink = By.xpath("//button[@class='btn btn-link orderCodesForm_apply orderCodesForm_applyPromo']");
	public static By txtInvalidPromocode = By.xpath("//p[@class='orderCodes_error orderCodes_PromoMin']");
	public static By spnRemovePromoCode = By.xpath("//section[@class='cart checkoutMainContent']/section[@class='cartOrderDetails']/section[3]//section[@class='orderCodesFormPromo']//i");
	public static By txtObjPromoCode = By.xpath("//fieldset[@class='formWrap_toggleCheckboxGroup formWrap_toggleCheckboxGroupPromo']");
	public static By txtSearchBarFormInput = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	public static By txtsearchProduct = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	public static By lnkSearchTypeList = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[7]");
	public static By btnAddToCart = By.xpath("//button[@class='prodItemStock_addCart']");
	public static By btnCartCount = By.xpath("(//a[@class='cartBtn' ]/span[@class='cartBtn_count'])");
	public static By txtCartBtn = By.xpath("(//a[@class='cartBtn' ]/span[@class='cartBtn_count'])");
	public static By txtSearchProduct = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	public static By lnkSearchTypeListMobile = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[9]");
	
	public static By lnkmainEntityOfPage = By.xpath("(//a[@itemprop='mainEntityOfPage'])[6]");
	public static By txtProductAttributeName = By.xpath("(//li[@class='icon icon-glass-red prodAttr_icon prodAttr_icon-redWine'])[2]");
	public static By txtProductAttributeTitle = By.xpath("//span[text()='Red Wine']");
	public static By txtProductAttributeContent = By.xpath("//p[text()='Learn about red wine � the range of styles, how it�s made and more ...']");
	
	/*****************************************Limit X bottoles per customer******************************************************/
	
	public static By btnAllLinkPlusIcon = By.xpath("//a[@class='prodPedigree_allLink-mobile']");
	public static By lnkVintageDetail = By.xpath("//div[@class='pedigreeList_container']//div[@class='prodPedigree_flexContainer ']");
	public static By btnAllLinkCloseIcon = By.xpath("//div[@class='js-closeModal modalWindow_cancel icon icon-menu-close']");
	public static By lnkAllVintagesLink = By.xpath("/h2[text()='Critical Acclaim']/following-sibling::a[text()='All Vintages']");
	public static By cboProductItemLimitQuantity = By.xpath("//select[@class='prodItemStock_quantitySelect']");
	public static By cboProductItemLimitCount = By.xpath("//span[@class='prodItemLimit_count']");
	
	/************************************************PreSaleProduct************************************************/
	public static By txtPreSaleProduct = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[3]");
	public static By txtPreSaleProductText = By.xpath("//span[text()='Pre-sale:']");	
	public static By spnPIPAddCartQuantity = By.xpath("//select[@class='prodItemStock_quantitySelect']");
	/*****************************************************************************************************************/
	/***********************************Stewardship details*********************************************/
	public static By btnObj_StewardshipSaving = By.xpath("//span[@class='stewardShipSection_savings']");
	public static By txtStewardshipSaving = By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
	public static By btnStewardshipSave = By.xpath("//section[@class='stewardShipSection stewardShipUpsell']/div");
	public static By txtStewardshipText = By.xpath("(//section[@class='stewardShipSection stewardShipUpsell ss-orig-var']//span[@class='stewardShipContent_textPost'])");
	public static By btnAddStewardshipMember = By.xpath("//a[@class='stewardShipUpsell_link stewardShipUpsell_addToCart']");
	public static By txtStewardshipConfirm = By.xpath("//button[text()='Confirm']");
	public static By txtStewardshipDiscountPrice = By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
	
				
  /*******************************************************************************************************/			
	public static By btnSaveForLaterCount = By.xpath("//span[@class='saveForLater_headline-count']");
	public static By spnFirstProductInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[1]/div[2]/div[3]/a/span");
	public static By spnSecondProdcutInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[2]/div[2]/div[3]/a/span");
	public static By spnThirdProductInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[3]/div[2]/div[3]/a/span");
	public static By spnFourthProdcutInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[4]/div[2]/div[3]/a/span");
	public static By spnFifthProductInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[5]/div[2]/div[3]/a/span");
	public static By txtSaveForLaterHeadline = By.xpath("//div[@class='saveForLater_headline']");
	public static By btnSaveForLaterAfterCheckout = By.xpath("//section[@class='cartOrderDetails']/following-sibling::section");
	
	public static By spnFirstProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[1]");
	public static By spnSecondProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[2]");
	public static By spnThirdProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[3]");
	public static By spnFourthProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[4]");
	public static By spnFifthProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[5]");
	
	public static By txtProductPriceHasStrike = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice']/div[@class='productPrice_price-reg has-strike']");
	public static By txtProductPriceTotal = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice_total']");
	public static By txtProductPriceHasSale = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	
	public static By txtProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[1]");
	public static By txtProductQuantity = By.xpath("(//select[@class='productQuantity_select'])[1]");
	public static By cboIncreaseQuantity = By.xpath("//select[@class='productQuantity_select']");
	
	public static By txtMoveToCartFirstLink = By.xpath("(//div[text()='Move to Cart'])[1]");
	public static By txtMoveToCartSecondLink = By.xpath("(//div[text()='Move to Cart'])[2]");	

	//================================Gift code=======================================
	
	public static By chkGiftCheckbox = By.xpath("(//main//section[@class='orderCodes']//div[@class='orderNoteGift']//span[@class='formWrap_toggleCheckboxSpan'])");
	public static By txtGiftNumber = By.xpath("//input[@class='formWrap_input orderCodesForm_input orderCodesForm_inputGift']");
	public static By btnGiftApply = By.xpath("//button[@class='btn btn-link orderCodesForm_apply orderCodesForm_applyGift']");
	public static By txtGiftCertificatePrice = By.xpath("//td[@class='orderSummary_price orderSummary_giftCertPrice']");
	public static By txtGiftErrorCode = By.xpath("//p[@class='orderCodes_error orderCodes_GiftError orderCodes_GiftErrorCode']");
	public static By btnGiftCardApplyng = By.xpath("//section[@class='checkoutMainContent']/section[2]//section[@class='orderSummaryList']/section[3]//section[@class='orderCodesFormGift']//button[.='Applying...']");
	public static By txtGiftCertificateSuccessMsg = By.xpath("//p[@class='orderCodes_success orderCodes_giftCodeSuccess']");
	public static By txtGiftCrdAmt = By.xpath("//span[@class='storedPayment_original']");
	public static By txtGiftCrdRem = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_giftCardRemaining']");
	public static By txtGiftCrdRemAmtZero = By.xpath("//th[text()='Gift Card Remaining']/following::td[text()='$0.00']");
	
	//==================================Verify item image is available ===================================
	
	public static By spnFirstImageInCart = By.xpath("//section[@class='cartContents']/ul/li[1]/div[2]/div[2]/a/div");
	public static By spnSecondImageInCart = By.xpath("//section[@class='cartContents']/ul/li[2]/div[2]/div[2]/a/div");
	public static By spnThirdImageInCart = By.xpath("//section[@class='cartContents']/ul/li[3]/div[2]/div[2]/a/div");
	public static By spnFourthImageInCart = By.xpath("//section[@class='cartContents']/ul/li[4]/div[2]/div[2]/a/div");
	public static By spnFifthImageInCart = By.xpath("//section[@class='cartContents']/ul/li[5]/div[2]/div[2]/a/div");
				
	
	/*************************************Try Vintage*******************************/
		
	public static By txtShoppingCartHeader = By.xpath("//section[@class='cartContents']/div[1]");
	public static By cboProductQuantitySel = By.xpath("(//select[@class='productQuantity_select'])[1]");	
	public static By txtCartHeadLineCount = By.xpath("//span[@class='cart_headline-count']");
	
	public static By btnRemoveFirstProduct = By.xpath("(//span[@class='iconRoundedOutline'])[1]");
	public static By btnRemoveSecondProduct = By.xpath("(//span[@class='iconRoundedOutline'])[2]");
	public static By btnRemoveThirdProduct = By.xpath("(//span[@class='iconRoundedOutline'])[3]");	
	
	public static By txtProductPriceRegWhole = By.xpath("(//div[@class='productPrice_price-reg']/span[@class='productPrice_price-regWhole'])[1]");
	public static By txtProductPriceRegFraction = By.xpath("(//div[@class='productPrice_price-reg']/span[@class='productPrice_price-regFractional'])[1]");	
	public static By txtProductPriceSaleWhole = By.xpath("(//span[@class='productPrice_price-regWhole'])[1]");
	public static By txtProductPriceSaleFraction = By.xpath("(//span[@class='productPrice_price-regFractional'])[1]");	
			
	public static By cboSelectRatingAndPrice = By.xpath("(//div[@class='filterMenu filterMenu-priceFilter filterMenu-hasChildren filterMenu-priceRatings']//input[@class='filterMenu_showRadio'])");
	public static By cboSliderBarRating = By.xpath("//div[@id='ratingRangeSliderInput']");	
	public static By cboSliderBarPricing = By.xpath("//div[@id='priceRangeSliderInput']");	
	public static By txtProdItemShippingDate = By.xpath("//div[@class='prodItemShipping']");	
	public static By cboPIPAddCartQuantity = By.xpath("//select[@class='prodItemStock_quantitySelect']");
	
	public static By spnAddRecommendedProducts = By.xpath("(//span[@class='prodRec_addToCart'])[1]");
	public static By spnRecommendedProduct = By.xpath("(//li[@class='scrollerList_item'][1]//span[@class='scrollerList_title'])[1]");
	
	public static By spnUserRating = By.xpath("//div[@class='userRatingsComp_minOverlay']");
	public static By spnMouseOveruserRating = By.xpath("//div[@class='starRating js-hasRating']");
	public static By imgClearStarRating = By.xpath("//div[@class='starRating js-hasRating']/button[@class='starRating_clear']");	
	public static By spnlabstewardShipSection = By.xpath("(//span[@class='stewardShipSection_headline-highlight'])[1]");
	
	public static By spnAddToMyWineFunc = By.xpath("//span[@class='prodActionsIcons_myWineIconText']");
	public static By spnMywinecolor = By.xpath("//span[@class='prodActionsIcons_myWine js-is-myWine']");
	public static By txtProductName = By.xpath("//h1[@class='pipName']");	
	
	/***************************************VerifyFooterLinks**************************************************/
	
	public static By labCustomerCare = By.xpath("(//footer[@class='pageFooter ']//a[text()='Customer Care'])");
	public static By labCorperateGifts = By.xpath("(//i[@class='icon icon-gift-light'])");	
	public static By labHowtoWorkWithUs = By.xpath("//a[text()='How to Work With Us']");
	public static By labCopyRights = By.xpath("//footer//*[text()=' Wine.com, LLC. All rights reserved.']");	
	public static By txtMobContactUs = By.xpath("//h1[text()='Contact Us']");
	public static By txtAboutUs = By.xpath("//h1[text()='Why Shop with Wine.com?']");
	public static By txtCareers = By.xpath("//h1[text()='Attention Wine Lovers']");	
	public static By lnkEmailPref = By.xpath("(//footer[@class='pageFooter ']//a[text()='Email Preferences'])");
	public static By lnkAboutWine = By.xpath("//a[text()='About Wine.com']");
	public static By lnkCareers = By.xpath("//a[text()='Careers']");
	public static By lnkCustCareHeader = By.xpath("(//footer[@class='pageFooter ']//a[text()='Customer Care'])");			
	public static By lnkSellUsYourWine = By.xpath("//a[text()='Sell Us Your Wine']");
	public static By lnkAffiliatedProgram = By.xpath("//a[text()='Affiliate Program']	");
	public static By lnkPartnerships = By.xpath("//a[text()='Partnerships']");	
	public static By titleAboutUs = By.xpath("//h1[text()='Why Shop with Wine.com?']");
	public static By titleCareers = By.xpath("//h1[text()='Attention Wine Lovers']");
	public static By titleSellUsYourWine = By.xpath("//h1[text()='Sell Us Your Wine']");
	public static By titleContactUs = By.xpath("//div[@class='title' and contains(text(),'Contact Us')]");	
	
	/******************VerifyMyWineSignInNEditLinkForGiftMessage*****************************************/
		
	public static By txtSignInTextR = By.xpath("//p[text()='Existing Customer?']");
	public static By txtSignInBtnR = By.xpath("//button[text()='Sign In']");
	
	public static By spnVarietalOriginLink = By.xpath("(//a[@class='pipOrigin_link'])[1]");
	public static By spnRegionOrgiginLink = By.xpath("(//a[@class='pipOrigin_link'])[2]");
	
	/********************************** VerifyAddToCartAlertIsDispWhenProdIsAdded*************************/
	public static By txtItemAdded = By.xpath("//span[text()='1 Item added']");
	public static By dwnShipToState = By.xpath("(//div[@class='pageHeader_dropDown']//select[@name='shipToState'])");	
	public static By imgWinelogoR = By.xpath("(//div[@class='pageHeader_headWrap']/div/a[@class='wineLogo'])");
	

/**************************Modal light box view**********************************************/
	
	public static By txtSearchTypeListWill = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[2]");
	public static By txtAllVintages = By.xpath("(//a[text()='All Vintages'])[2]");
	public static By spnCarouselContentItem = By.xpath("(//img[@class='carouselContent_item'])[1]");
	public static By lnklightBoxCloseIcon = By.xpath("//i[@class='icon icon-plus lightBox_closeIcon']");
	public static By btmCodalWindowCancel = By.xpath("//div[@class='js-closeModal modalWindow_cancel icon icon-menu-close']");
	public static By txtStewardShipDifferenceAmount = By.xpath("//span[@class='stewardShipSection_difference']");
	
	
	}
