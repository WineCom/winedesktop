package wine.autotest.mobile.pages;

import org.openqa.selenium.By;

public class OrderDetailsPage {
	
    public static By spnOrderDetailOrdNum1 = By.xpath("(//span[@class='orderItem_id'])[1]");
	public static By spnOrderDetailOrdNum2 = By.xpath("(//span[@class='orderItem_id'])[2]");
	public static By lnkSplitOrderNum = By.xpath("(//a[@class='openOrderDetails_text openOrderDetails_numberText'])[2]");
	public static By spnorderHistoryGftMssg = By.xpath("(//section[@class='reviewDatePaymentInfo']//span[@class='reviewRecipientGift_message'])");
	

}
