package wine.autotest.mobile.pages;

import org.openqa.selenium.By;

public class ListPage {
	
	public static By lnkvarietal = By.xpath("//a[@class='mainNav_sectionTab mainNav_section-varietal' and (text()='Varietal')]");
	public static By lnkRegion = By.xpath("//span[text()='Region']");
	public static By lnkRatingAndPrice = By.xpath("//span[text()='Rating & Price']");
	public static By lnkMoreFilters = By.xpath("//div[@class='listPageOptions']//input[@id='filter-widget-expander']");
	public static By lnkReviewedBy = By.xpath("//span[text()='Reviewed By']");
	public static By spnSizeAndType = By.xpath("//span[text()='Size & Type']");
	public static By lnkFineWine = By.xpath("//span[text()='Fine Wine']");
	public static By lnkVintage = By.xpath("//span[text()='Vintage']");
	public static By btnHide = By.xpath("//input[@class='filterWidgetExpander_input']");
	public static By lnkRating = By.xpath("//button[text()='90+']");
	public static By lnkPrice = By.xpath("//button[text()='$20 to $40']");
	public static By lnkFilterBy = By.xpath("(//a[@class='filterWidgetFilterBy_text'])");
	public static By lnkSortFilter = By.xpath("//div[@class='listPageOptions']//div[@class='sortOptions_content']//select[@id='product-list-sort-1']");
	public static By lnkFilterByinput = By.xpath("//input[@class='filterWidgetFilterBy_input']");			
	public static By lnkRhoneBlends = By.xpath("//a[text()='Rh�ne Blends']");			
	public static By lnkShowOutOfStock = By.xpath("//div[@class='listOptions listOptions-phoneOnly']//span[@class='checkStock_checkboxSpan']");
	public static By dwnTotalNoOfItems = By.xpath("");
	public static By lnkBordexBlends = By.xpath("//a[text()='Bordeaux Blends']");
	public static By lnkDone = By.xpath("//span[text()='90+ Rated and $20 to $40']");
	public static By lnkPinotNoir = By.xpath("//span[text()='90+ Rated and $20 to $40']"); 		
	public static By lnkSrt = By.xpath("//div[@class='listOptions listOptions-phoneOnly']//span[@class='count']"); 	
	public static By dwnselectFilterBy = By.xpath("(//input[@class='filterWidgetFilterBy_input'])");	
	public static By chkOutOfStockCheckbox = By.xpath("//div[@class='listOptions listOptions-phoneOnly']//span[@class='checkStock_checkboxSpan");
	public static By dwnselectVarital = By.xpath("//div[@class='filterWidget']//div[@class='filterMenu filterMenu-list varietal filterMenu-hasChildren']//input[@name='refinementRadio']");	
	public static By btnMainNavAccTab = By.xpath("//input[@class='accountBtn_checkbox']");
	public static By btnMainNavButton = By.xpath("//input[@class='mainNavBtn']");
	public static By txtCaberNet = By.xpath("//a[text()='Cabernet Sauvignon']");
	public static By txtBordexBlends = By.xpath("//a[text()='Bordeaux Blends']");
	public static By txtPinotNoir = By.xpath("//a[text()='Pinot Noir']");
	public static By txtSelRegion = By.xpath("(//li[@class='mainNavList_item mainNavList_item-level1']/label[text()='Region'])");
	public static By txtSelCentralCoast = By.xpath("//a[text()='Central Coast']");
	public static By txtSangviovese = By.xpath("//a[text()='Sangiovese']");
	public static By txtCabernetSauvignon = By.xpath("(//span[text()='Cabernet Sauvignon'])");	
	public static By linFeatured = By.xpath("(//a[text()='Featured'])");
	public static By btnFeaturedMainTab = By.xpath("//a[@class='mainNav_tabLink ' and (text()='Discover')]");
	public static By txtBordexFeatures = By.xpath("//a[text()='Bordeaux Futures']");	
	public static By dwnShipsSoonestR = By.xpath("(//div[@class='listOptions listOptions-phoneOnly']//span[@class='shipsSoonest_labelText'])");
	public static By dwnShipSoonCheckBox = By.xpath("(//div[@class='listOptions listOptions-phoneOnly']//span[@class='shipsSoonest_checkboxSpan'])");	
	public static By dwnFilterMenuIcon = By.xpath("(//span[@class='filterMenu_icon icon icon-circled-times'])[4]FilterMenuIcon=xpath>(//span[@class='filterMenu_icon icon icon-circled-times'])[4]");
	public static By dwnVarietalLink = By.xpath("//a[@class='mainNav_sectionTab mainNav_section-varietal' and (text()='Varietal')]");
	public static By dwnRegionR = By.xpath("//div[@class='filterWidget']//div[@class='filterMenu filterMenu-list nestedRegion filterMenu-hasChildren']//input[@name='refinementRadio']");
	public static By lnkRegionTab = By.xpath("//a[@class='mainNav_tabLink ' and (text()='Region')]");	
	public static By dwnOregon = By.xpath("//span[text()='Oregon']");
	public static By dwnWillametteValley = By.xpath("//span[text()='Willamette Valley']");
	public static By dwnYamhillCarlton = By.xpath("//a[@class='filterMenu_itemLink']/span[text()='Yamhill-Carlton District']");	
	public static By linSpecialsProducts = By.xpath("//a[@class='mainNav_tabLink ' and (text()='Discover')]");
	public static By linsmallprod = By.xpath("//a[text()='Boutique & Small Production']");
	public static By linBordeauxFuture = By.xpath("//a[text()='Bordeaux Futures']");
	public static By linWineAndFoodGifts = By.xpath("//a[text()='Wine & Food Gifts']");
	public static By lnkChardonnayWine = By.xpath("//a[text()='Chardonnay']");
	public static By lnkheroImagePlusSymbol = By.xpath("//div[@class='listPageContentExpander_icon icon icon-circled-plus']");
	public static By txtNapaValley = By.xpath("(//a[text()='Napa Valley'])");
	public static By txtTopRated = By.xpath("//a[text()='94+ Rated']");
	public static By txtGiftsBottttles = By.xpath("//a[text()='1 \\u2013 4 Bottles']");
	public static By lnklistPageContainer = By.xpath("//div[@class='listPageContentContainer']");			
	public static By imgObj_MyWine = By.xpath("//a[@class='activityBtn']");	
	public static By txtMyWine = By.xpath("//h1[text()='My Wine']");	
	public static By txtChardonnay = By.xpath("(//span[text()='Chardonnay'])");	
	public static By lnkStewardshipSettings = By.xpath("(//a[@class='mainNavList_itemLink ' and text()='StewardShip Settings'])");	
	public static By btnStewardshipAdd = By.xpath("//a[@class='stewardship-add-to-cart btn btn-large btn-rounded btn-red btn-processing checkoutPageButton cartButton']");
	public static By lnkPromoBarWashingtonReg = By.xpath("//section[@class='listPageContent_bodyText']/p");
	public static By lnkproductInfo = By.xpath("(//span[@class='prodItemInfo_name'])[2]");
	public static By txtVintageAlert = By.xpath("//div[@class='winePage']/main[@class='wineMain']/section[@class='pipMainContent']//span[@class='prodActionsIcons_alertIcon icon icon-bell']");
	public static By txtsetAlert = By.xpath("//form[@class='formWrap prodAlertForm']//button[@class='prodAlertForm_btn btn btn-link']");
	public static By txtProdAlert = By.xpath("//div[@class='prodAlert']");
	
	public static By btnAlertSaving = By.xpath("//div[@class='winePage']/main[@class='wineMain']/section[@class='pipMainContent']//form[@class='formWrap prodAlertForm']//span[@class='prodAlertForm_btnText']");
	public static By txtNewArrivalAlertHeader = By.xpath("//h2[text()='New Arrival Alerts']");
	public static By txtNewArrivalAlertListItem = By.xpath("(//li[@class='newArrivalAlert_listItem'])[1]");
	public static By txtDeleteAlerts = By.xpath("(//a[text()='Delete'])[1]");
	public static By btnAddProduct = By.xpath("(//div[@class='scrollerList_image'])[2]");	
	public static By lnkAddProductToPIP = By.xpath("(//span[@class='prodItemInfo_name'])[1]");
	public static By txtEror404 = By.xpath("//p[text()='404 Error']");	
	public static By lnkViewAllProdusts = By.xpath("//a[@class='pipWinery_viewAllLink']");
		
	public static By txtVarietalFilterName = By.xpath("(//span[text()='Cabernet Sauvignon'])");
	public static By btnVarietalFilterClose = By.xpath("//div[@class='filterMenu_clearRefinements filterMenu_clearRefinements-showing']");
		
	public static By lnkProductInfo = By.xpath("(//span[@class='prodItemInfo_name'])[2]");
	public static By txtPIPName = By.xpath("//h1[@class='pipName']");
	public static By cboGetState = By.xpath("(//select[@name='shipToState']//option[@selected='true'])[4]");
	public static By lnkPipProduct = By.xpath("(//span[@class='prodItemInfo_name'])[2]");
	public static By txtDistillerNotes = By.xpath("//h2[text()='Distiller Notes']");
	
      /***********************	ship soon checkbox ***************************************/
			
	public static By btnSipTodayFirstProd = By.xpath("(//div/div[contains(text(),'Ships today')])[1]");
	public static By btnShipTodaySecoProd =  By.xpath("(//div/div[contains(text(),'Ships today')])[2]");
	public static By btnShipTodayThirdProd =  By.xpath("(//div/div[contains(text(),'Ships today')])[3]");
					
	/*************Sort options*************************************************************************/
	public static By linSortOptions = By.xpath("(//select[@class='sortOptions_sort'])[2]");
	public static By linSortTopRated = By.xpath("(//option[@value='topRated'])[2]");
	public static By linSortMostInteresting = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Most Interesting']");
	public static By linSortCustomerRating = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Customer Rating']");
	public static By lnkSortprofissionalRating = By.xpath("((//select[@class='sortOptions_sort'])[2]/option[text()='Professional Rating']");
		
	public static By lnkSortLtoH = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Price: Low to High']");
	public static By lnkSortAtoZ = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Winery: A to Z']");
	public static By lnkSortHtoL = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Price: High to Low']");
	
	public static By lnkGiftSortHtoL = By.xpath("(//select[@class='sortOptions_sort'])[2]//option[@value='priceHighToLow']");
	public static By lnkGiftSortLtoH = By.xpath("(//select[@class='sortOptions_sort'])[2]//option[@value='priceLowToHigh']");
	public static By lnkGiftSortZtoA = By.xpath("(//select[@class='sortOptions_sort'])[2]//option[@value='zToA']");
	public static By lnkGiftSortAtoZ = By.xpath("(//select[@class='sortOptions_sort'])[2]//option[@value='aToZ']");
	
			
	public static By SortNtoO = By.xpath("//option[@value='newToOld']");
	public static By SortOtoN = By.xpath("//option[@value='oldToNew']");
	
	public static By lnkSavings = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Savings']");
	public static By lnkjustIn = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Just In']");
	public static By lnkSortOldToNew = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Vintage: Old to New']");
	public static By lnkSortNewToOld = By.xpath("(//select[@class='sortOptions_sort'])[2]/option[text()='Vintage: New to Old']");
					
	public static By lnksmallprod = By.xpath("//a[text()='Boutique & Small Production']"); 	
	public static By imgWineLogo = By.xpath("//a[@class='wineLogo']");
	public static By cboChooseYourState = By.xpath("//span[text()='Choose Your State ']");	
	public static By cboSelectState = By.xpath("(//div[@class='corePage winePage']//following::select[@class='state_select state_select-promptsCartTransfer'])");
	public static By cboStateChangeDailog = By.xpath("//p[@class='cartTransfer_headline']");	
	public static By linobj_Gifts = By.xpath("//a[@class='mainNav_tabLink ' and (text()='Gifts')]"); 	
	public static By linWineAndFood = By.xpath("(//span[text()='Wine & Food Gifts'])");
	public static By linChampange = By.xpath("(//a[text()='Red Wine'])");
	public static By lnkmyWneSortWd = By.xpath("//select[@id='product-list-sort-2']");
	public static By TxtPromoBannerDry = By.xpath("//div[@class='bannerMessage bannerMessage_dryState']");
	public static By cbo_SelectState = By.xpath("(//div[@class='pageHeader_dropDown']//select[@class='state_select state_select-promptsCartTransfer'])");
	public static By cboObj_ChangeState = By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[4]");	
	public static By txtViewAllWine = By.xpath("//a[text()='View all wine']");
	public static By lnkAllGiftCard = By.xpath("//a[text()='Gift Cards']");
	public static By spnHeadaerMessage = By.xpath("//p[@class='cartTransfer_headline']");
	public static By btnCancelStayInState = By.xpath("//div[@class='cartTransfer_link cartTransfer_stayInState']");
		
				
	public static By btnFirstProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[1]");
	public static By btnSecondProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[2]");
	public static By btnThirdProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[3]");
	public static By btnFourthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[4]");
	public static By btnFifthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[5]");
	public static By btnSixthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[6]");
	public static By btnSeventhProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[7]");
	
	public static By btnMyWineFirstProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[1]");
	public static By btnMyWineSecondProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[2]");
	public static By btnMyWineThirdProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[3]");
	public static By btnMyWineFourthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[4]");
	public static By btnMyWineFifthProductToCart = By.xpath("(//button[@class='prodItemStock_addCart'])[5]");
	
	public static By btnFirstProdct = By.xpath("(//div[@class='prodItemStock']/button)[1]");
	public static By btnSecondProdct =  By.xpath("(//div[@class='prodItemStock']/button)[2]");
	public static By btnThirdProdct =  By.xpath("(//div[@class='prodItemStock']/button)[3]");
	public static By btnFourthProdct = By.xpath("(//div[@class='prodItemStock']/button)[4]");
	public static By btnFifthProdct =  By.xpath("(//div[@class='prodItemStock']/button)[5]");
		
	public static By btnUnavailableFirstProduct = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/div[2]/span");
	public static By btnUnavailableSecondProduct = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/div[2]/span");
	public static By btnUnavailableThirdProduct = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/div[2]/span");
	public static By btnUnavailableFourthProduct = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/div[2]/span");
	public static By btnUnavailableFifthProduct = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/div[2]/span");
	
	public static By btnFirstProductInCart = By.xpath("//ul[@class='prodList']/li[1]/div[2]/div[3]/a/span");
	public static By btnSecondProductInCart = By.xpath("//ul[@class='prodList']/li[2]/div[2]/div[3]/a/span");
	public static By btnThirdProductInCart = By.xpath("//ul[@class='prodList']/li[3]/div[2]/div[3]/a/span");
	public static By btnFourthProductInCart = By.xpath("//ul[@class='prodList']/li[4]/div[2]/div[3]/a/span");
	public static By btnFifthProductInCart = By.xpath("//ul[@class='prodList']/li[5]/div[2]/div[3]/a/span");
	
	public static By btnFirstProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[6]/a");
	public static By btnSecondProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[7]/a");
	public static By btnThirdProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[8]/a");
	public static By btnFourthProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[9]/a");
	public static By btnFifthProductInList = By.xpath("//div[@class='searchTypeAheadWrap js-typing']/ul/li[10]/a");
	
	/**********************************Add To Cart button functionality when dry state is selected ******************/
	public static By btnAddToCartFirstButton = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/div[3]/span");
    public static By btnAddToCartSecondButton = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/div[3]/span");
	public static By btnAddToCartThirdButton = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/div[3]/span");
	public static By btnAddToCartFourthButton = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/div[3]/span");
	public static By btnAddToCartFifthButton = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/div[3]/span");
			
		
	public static By txtSearchBarFormInput = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	public static By txtsearchProduct = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	public static By lnkSearchTypeList = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[7]");
	public static By btnAddToCart = By.xpath("//button[@class='prodItemStock_addCart']");
	public static By btnCartCount = By.xpath("(//a[@class='cartBtn']/span[@class='cartBtn_icon icon icon-cart'])");
	public static By lblShowOutOfStock = By.xpath("//div[@class='listOptions listOptions-phoneOnly']//span[@class='checkStock_checkboxSpan']");
	public static By txtupdatingState = By.xpath("//span[.='Updating your delivery state...']");
	
	/*********************Ship today Products ******************************************************/
	public static By btnShipToday = By.xpath("(//div/div[text()='Ships Today'])[1]");
	
	public static By btnShipTodayFirstProductNam = By.xpath("(//div/div[text()='Ships Today'])[1]/following::span[1]");
	public static By btnShipTodaySecondProductName = By.xpath("(//div/div[text()='Ships Today'])[5]/following::span[1]");
	public static By btnShipTodayThirdProductName = By.xpath("(//div/div[text()='Ships Today'])[6]/following::span[1]");
	public static By txtShipTodayWarningMsg = By.xpath("//div[@class='cartNote_message' and text()=' consider replacing the item(s) noted below or creating separate orders.']"); 

	/*********************************Product Attribute*********************************************/
	public static By btnProductAttributeIcon = By.xpath("//div[@class='starRating']/ancestor::section/following-sibling::section[@class='pipSecContent']//ul[@class='prodAttr']/li");
	public static By txtProductOne = By.xpath("(//section[@class='products']//ul/li//a[@class='prodItemInfo_link'])[1]");
      	
	
	/*#=================================Varietal filters=============================*/
	public static By btnFirstRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[1]");
	public static By btnSecondRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[2]");
	public static By btnThirdRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[3]");
	public static By btnFourthRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[4]");
	public static By btnFifthRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[5]");
	public static By btnSixthRefinementWidget = By.xpath("(//section[@class='refinementWidgetSection']//div[@class='filterMenu_dropdown']/ul/li/a/span[1])[6]");
		
	/********************Verify Total of the order in order summary*******************************/
	
	public static By btnFirstProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[1]");
	public static By btnSecondProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[2]");
	public static By btnThirdProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[3]");
	public static By btnFourthProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[4]");
	public static By btnFifthProductPrice = By.xpath("(//div[@class='productPrice_price-reg'])[5]");
		
	public static By btnFirstFilteredProd = By.xpath("(//ul[@class='prodList']//div[@class='prodItemStock']/button)[1]");
	public static By btnSecondFilteredProd = By.xpath("(//ul[@class='prodList']//div[@class='prodItemStock']/button)[2]");
	public static By btnThirdFilteredProd = By.xpath("(//ul[@class='prodList']//div[@class='prodItemStock']/button)[3]");
	public static By btnFourthFilteredProd = By.xpath("(//ul[@class='prodList']//div[@class='prodItemStock']/button)[4]");
	public static By btnFifthFilteredProd = By.xpath("(//ul[@class='prodList']//div[@class='prodItemStock']/button)[5]");
			
	public static By txtTotalBeforeTax = By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']");
	public static By txtSaveForLaterCount = By.xpath("//span[@class='saveForLater_headline-count']");	
	public static By txtSaveForLaterHeadline = By.xpath("//div[@class='saveForLater_headline']");
	public static By txtNoItemsInSaveForLater = By.xpath("//div[text()='You have no items saved for later.']");
	public static By txtSaveForLaterAfterCheckout = By.xpath("//section[@class='cartOrderDetails']/following-sibling::section");
	public static By txtSubtotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_subTotalPrice']");
	public static By txtobjShippingHnadling= By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");	
	public static By btnSecondProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[2]");
	public static By btnThirdProductSaveFor = By.xpath("(//span[@class='prodItemStock_saveForLater noAdd'])[3]");	
	public static By btnFirstProductInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[1]/div[2]/div[3]/a/span");
	public static By btnSecondProdcutInSaveFor = By.xpath("//section[@class='saveForLater']/ul/li[2]/div[2]/div[3]/a/span");
	
	
	/**************************Verify gift wrapping section **************************************************/
	public static By btnGfts = By.xpath("//a[@class='mainNav_tabLink ' and (text()='Gifts')]");
	public static By lnkBottelName = By.xpath("//a[@href='/list/gifts/curated-wine-sets/wine-set-1-4-bottles/7151-8984-513']");
	public static By lnkGiftProdutcAddToPIP = By.xpath("(//span[@class='prodItemInfo_name'])[1]");
	
	
	public static By cboStarsRating = By.xpath("//div[@class='starRating']");
	public static By lnkshare = By.xpath("//i[@class='prodActionsIcons_share icon icon-share']");	
	public static By txtSearchProduct = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	
	public static By txtsearchProductMobile = By.xpath("(//form[@class='formWrap searchBarForm']//input[@class='formWrap_input searchBarForm_input'])[2]");
	public static By txtSearchTypeListMobile = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])[5]");
	public static By txtGiftCardsTab = By.xpath("//a[@class='searchTypeAheadList_itemLink']");
	
	/*================ItemsNotEligibleForPromoCodeRedemption======*/
	
	public static By btnNotEligiblepromocodeRedemptionProduct = By.xpath("(//a[@class='searchTypeAheadList_itemLink'])");
	public static By txtPromoCode = By.xpath("//input[@name='promoCode']");
	public static By lnkPromoCodeApply = By.xpath("//button[@class='btn btn-link orderCodesForm_apply orderCodesForm_applyPromo']");
	public static By txtObj_PromoCode = By.xpath("//fieldset[@class='formWrap_toggleCheckboxGroup formWrap_toggleCheckboxGroupPromo']");
	
	public static By txtMinimumOfferAmount = By.xpath("//p[text()='Order subtotal does not meet minimum amount for this offer.']");
		
	public static By btnContinueShipToKY = By.xpath("//a[text()='Continue (ship to KY)']");
	public static By btnContinueShipToDE = By.xpath("//a[text()='Continue (ship to DE)']");
	public static By btnContinueShipToDC = By.xpath("//a[text()='Continue (ship to DC)']");	
	public static By btnYourCartIsEmpty = By.xpath("//p[text()='Your cart is empty.']");
	public static By btnCurrentlyUnavailable = By.xpath("(//div[@class='productUnavailable'])[1]");
	public static By btnCancleStayIn = By.xpath("//div[text()='Cancel (stay in ']");
	public static By btnMoveToCart = By.xpath("(//div[text()='Move to Cart'])[1]");
	public static By txtProductUnavailable = By.xpath("(//div[@class='productUnavailable']//span)[1]");
	
	
 /************ StewardShip Auto*********************************************************************************************/
	
	public static By txtStewardshipSet = By.xpath("(//a[text()='StewardShip Settings'])[1]");
	public static By btnStewardsCancel = By.xpath("//button[text()='Cancel Membership']");
	public static By btnStewardsYesCancel = By.xpath("//button[text()='Cancel membership']");
	public static By cboStewardsConfirmCancle = By.xpath("//span[text()='Wine selection or price']");
	public static By btnStewardsConfirmCanclation = By.xpath("//button[text()='Confirm cancelation']");
	public static By btnStewardshipSubmitClose = By.xpath("//button[text()='Submit & Close']");
	public static By txtStewardsExpiryOn = By.xpath("//p[starts-with(text(),'Your membership ends on')]");
	public static By btnStewardshipRenew = By.xpath("//preceding-sibling::p/preceding-sibling::h2/following-sibling::button[text()='Renew']");
	public static By btnStewardshipYesRenew = By.xpath("//button[text()='Yes, renew']");
	public static By txtStewardsRenewOn = By.xpath("//span[contains(text(),'Your StewardShip membership will renew on')]");
			
	/**************************Verify the social media links********************************************************************/
	
	public static By lnkBlog = By.xpath("//span[@aria-label='wine.com blog icon']");
	public static By lnkFaceBook = By.xpath("//span[@aria-label='facebook icon']");
	public static By lnkTwitter = By.xpath("//span[@aria-label='twitter icon']");
	public static By lnkPinterest = By.xpath("//span[@aria-label='pinterest icon']");
	public static By lnkInstagram = By.xpath("//span[@aria-label='instagram icon']");
	
	
	/********************************************out of stcok and recommended products=*************************************/	
	public static By btnRecommendedProductsFirstAddToCart = By.xpath("((//div[@class='prodRec_scroller'])[2]//div//div[@class='scrollerList_content']/span[text()='Add to Cart'])[1]");
	public static By btnRecommendedProductsSecondAddToCart = By.xpath("((//div[@class='prodRec_scroller'])[2]//div//div[@class='scrollerList_content']/span[text()='Add to Cart'])[2]");
	public static By btnRecommendedProductsThirdAddToCart = By.xpath("((//div[@class='prodRec_scroller'])[2]//div//div[@class='scrollerList_content']/span[text()='Add to Cart'])[3]");
	public static By btnRecommendedProductsList = By.xpath("(//div[@class='prodRec_scroller'])[2]");
	public static By lblCurrentlyUnavailableProduct = By.xpath("(//span[@class='prodItemStock_soldOut'])[1]");
	public static By lblCurrentlyUnavailableProductName = By.xpath("(//div[@class='prodItemInfo']/a)[5]");
	public static By lblCurrentlyUnavailableProductInPIP = By.xpath("//span[@class='prodItemStock_soldOut']");
	
	public static By btnNewArrivalAlertFirstProduct = By.xpath("(//span[@class='newArrivalAlert_listItemName'])[1]");
	public static By btnNewArrivalAlertSecondProduct = By.xpath("(//span[@class='newArrivalAlert_listItemName'])[2]");
	
	/**************************************Response header menu***********************************************************/
	
	public static By btnlistPageFirstProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[1]");
	public static By btnlistPageSecProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[2]");
	public static By btnlistPageThirdProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[3]");
	public static By btnlistPageFourthProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[4]");
	public static By btnlistPageFifthProdName = By.xpath("(//div[@class='prodItemInfo_origin'])[5]");
	public static By lnkListProdName = By.xpath("(//div[@class='prodItemInfo'])[1]");
	
	/**************************************rating the product********************************************************/
	
	public static By lnkFirstListProd = By.xpath("(//span[@class='prodItemInfo_name'])[1]");
	public static By imgRateProduct = By.xpath("//span[@class='starRating_star starRating_star-left starRating_star-7']");
	public static By imgRateProduct4 = By.xpath("//span[@class='starRating_star starRating_star-left starRating_star-4']");	
	public static By imgRatedProduct4 = By.xpath("//div[@class='starRating js-hasRating']//span[@class='starRating_star starRating_star-right starRating_star-4 js-hasRating']");
	
	public static By imgRateProduct2 = By.xpath("//span[@class='starRating_star starRating_star-left starRating_star-2']");
	public static By imgRatedProduct2 = By.xpath("//div[@class='starRating js-hasRating']//span[@class='starRating_star starRating_star-right starRating_star-2 js-hasRating']");
	
	
	/***********************VerifyTheShipsOnStatementInPIP******************************/
	public static By btnFirstListProd = By.xpath("(//div[@class='prodItemInfo'])[1]");
	public static By lnkfirstProdNameLink = By.xpath("(//span[@class='prodItemInfo_name'])[1]");	
	public static By btnSecondProdName = By.xpath("(//div[@class='prodItemInfo'])[2]");
	public static By lnkSecondProdNameLink = By.xpath("(//div[@class='prodItemInfo'])[4]/a");	
	public static By txtShipsOnStatementInPIP = By.xpath("//div[contains(text(),'Ships today if ordered in next ')]");
	
	
	public static By txtVerifySearchBar = By.xpath("(//input[@class='formWrap_input searchBarForm_input' and @placeholder='Search Wine.com'])[2]");
	public static By imgSelectWineLogo = By.xpath("//a[@class='wineLogo']");
	
	/******************RatingStarsDisplayedProperlyInMyWine***********************************************/
	
	public static By imgUserRatings = By.xpath("(//span[contains(@class,'starRating_star starRating_star-right starRating_star-6')])");
	public static By imgRatingStarTextArea = By.xpath("(//textarea[@class='formWrap_textarea userRatingNote_textArea'])[1]");	
	public static By txtTextAreaCount = By.xpath("(//fieldset[@class='formWrap_group userRatingNote_fieldset']/span[@class='textarea_counter'])[1]");
	public static By btnCancelInMyWine = By.xpath("(//div[@class='userRatingNote_actions']//button[text()='Cancel'])[1]");
	public static By btnSaveInMyWine = By.xpath("(//div[@class='userRatingNote_actions']//button[text()='Save'])[1]");		
	public static By lnkProdInHomePage = By.xpath("//span[@class='prodRec_addToCart'][1]");	
	public static By btnAddToCartAlert = By.xpath("//div[@class='addToCartAlert']");	
	public static By btnSelQtyInALert = By.xpath("(//span[@class='addToCartAlert_message'])");	
	public static By cboGetStateInHomePage = By.xpath("(//div[@class='corePage winePage']//select[@name='shipToState']//option[@selected='true'])");	
	public static By imgUserRatingstext = By.xpath("(//div[@class='userRatingWrap'])[1]");	
	
	public static By imgCustomerRating = By.xpath("//div[@class='prodItemInfo_rating']//span[@class='averageRating_average']");
	public static By profissionalRatingOne = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[1]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingTwo = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[2]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingThree = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[3]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingFour = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[4]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingFive = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[5]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingSix = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[6]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingSeven = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[7]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingEight = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[8]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingNine = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[9]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingTen = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[10]/li/span[@class='wineRatings_rating']");
	public static By profissionalRatingEleven = By.xpath("(//div[@class='prodItemInfo']//ul[@class='wineRatings_list'])[11]/li/span[@class='wineRatings_rating']");
	
		
	/************************************************Rare Products***********************************************************/
	
	public static By spnRareProducts = By.xpath("//a[text()='Rare Spirits']");	
	public static By spnRareProductAttribute = By.xpath("(//li[@class=\"icon icon-rare prodAttr_icon prodAttr_icon-rare\"])[2]");
	
	public static By btnSpiritFilterBy = By.xpath("//input[@id='filter-widget-filterBy']");
	public static By btnSpiritVarity = By.xpath("//input[@id='filter-Spirit-Type']");
	public static By btnRegionVarity = By.xpath("//input[@id='filter-Nested-Region']");
	public static By btnSpiritRating = By.xpath("//input[@id='filter-Rating-Price']");
	public static By btnSpiritReviedBy = By.xpath("//input[@id='filter-Publication']");
	public static By btnSpiritSize = By.xpath("//input[@id='filter-Special-Designation']");
	public static By txtSpiritVarity = By.xpath("(//span[@class='filterMenu_itemName'])[1]");
	public static By txtRegionVarity = By.xpath("(//span[text()='Scotland'])[1]");
	public static By txtSpiritReviedBy = By.xpath("(//span[text()='Whisky Advocate'])[1]");
	public static By txtSpiritSize = By.xpath("(//span[text()='Standard (750ml)'])[1]");	
	
	/************************************************Hero Image**************************************************/
	
	public static By txtHeroImageTitle = By.xpath("(//span[text()='Cabernet Sauvignon'])[1]");	
	public static By lnklistPageContent = By.xpath("//section[@class='listPageContent_bodyText']");	
	public static By lnklistPageLink = By.xpath("//section[@class='listPageContent_bodyText']/p/a[@href='/list/wine/bordeaux/7155-107078']");	
	public static By imgHeroImagePlusSymbol = By.xpath("//div[@class='listPageContentExpander_icon icon icon-circled-plus']");	
		
	public static By imgWinelogoR = By.xpath("(//a[@class='wineLogo'])[1]");	
	public static By btnFrstProductName = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/a/span");
	public static By btnSecProductName = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/a/span");
	public static By btnThrdProductName = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/a/span");
	public static By btnFrthProductName = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/a/span");
	public static By btnFfthProductName = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/a/span");	
	
	public static By GiftSPriceFirstProduct = By.xpath("//section[@class='products']/ul/li[1]/div/div[2]/div[1]/span[1]/span[2]");
	public static By GiftSPriceSecondProduct = By.xpath("//section[@class='products']/ul/li[2]/div/div[2]/div[1]/span[1]/span[2]");
	public static By GiftSPriceThirdProduct = By.xpath("//section[@class='products']/ul/li[3]/div/div[2]/div[1]/span[1]/span[2]");
	public static By GiftSPriceFourthProduct = By.xpath("//section[@class='products']/ul/li[4]/div/div[2]/div[1]/span[1]/span[2]");
	public static By GiftSPriceFifthProduct = By.xpath("//section[@class='products']/ul/li[5]/div/div[2]/div[1]/span[1]/span[2]");
	
	public static By imgWineRatings = By.xpath("(//span[@class='wineRatings_rating'])[1]");
	public static By spnModalWindow = By.xpath("//div[@class='modalWindow_content']");
	public static By btnModalWindowClose = By.xpath("//div[@class='js-closeModal modalWindow_cancel icon icon-menu-close']");
	public static By spnProductAttributeName = By.xpath("(//li[@class='icon icon-glass-red prodAttr_icon prodAttr_icon-redWine'])[2]");
//	public static By spnProductAttributeTitle = By.xpath("(//div[@class='prodAttrLegend_title'])[1]");
	public static By spnProductAttributeContent = By.xpath("//p[text()='Learn about red wine � the range of styles, how it�s made and more ...']");
	
	/********************VerifyprofessionalRating************************************/
	public static By spnProfessionalRatingR = By.xpath("(//span[@class='wineRatings_initials'])[1]");
	public static By spnProfessionalRatingHeader = By.xpath("//div[@class='modalWindow_header']/h2[@class='modalWindowHeader_title']");
	
	public static By spnOrderOfProductIcons = By.xpath("(//li[@title='Red Wine'])[2]/following-sibling::li[@title='Collectible']/following-sibling::li[@title='Green Wine']/following-sibling::li[@title='Great Gift']");
	
	/**************************Limit Reached*******************************************************/
	public static By btnFirstProdQuantitySelect = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[1]");
	public static By btnSecondProdQuantitySelect = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[2]");
	public static By btnThirdProdQuantitySelect = By.xpath("(//select[@class='prodItemStock_quantitySelect'])[3]");
	public static By txtLimitReachedInPIP = By.xpath("//div[text()='Limit Reached']");
	public static By spnRatingProd = By.xpath("//span[@class='starRating_star starRating_star-left starRating_star-7']");
	public static By spnlimitReachedInMyWine = By.xpath("//div[@class='prodItemStock_limitReached']");
	
	public static By btnFrstProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[1]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By btnSecondProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[2]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By btnThirdProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[3]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By btnFourthProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[4]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	public static By btnFifthProductPriceSale = By.xpath("(//div[@class='prodItemInfo'])[5]/div[@class='productPrice']/div[@class='productPrice_price-sale']");
	
					
}
