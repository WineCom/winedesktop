package wine.autotest.mobile.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	

	
	public static By MainNavAccountTab = By.xpath("(//div[@class='pageHeader_headWrap']//span[@class='accountBtn']/i)");
	public static By MainNavSignIn = By.xpath("(//li[@class='headerAccountMenu_listItem mainNavList_itemLink-accntSignIn'])");
	public static By MainNavButton = By.xpath("//input[@class='mainNavBtn']");
	public static By MainNavAccSignIn = By.xpath("(//li[@class='mainNavList_item mainNavList_item-level1'])[5]");
	public static By MainNavTabSignOut = By.xpath("(//div[@class='headerAccountMenu']//a[text()='Sign Out'])");
	public static By signInLinkAccount = By.xpath("(//a[text()='Sign in'])[2]");
	public static By lnkSign = By.xpath("(//div[@class='mainNav_scrollContainer']//a[text()='Sign in'])");
	public static By MainNavAccTabAfterSignIn = By.xpath("(//section[a[contains(text(),'Account')]]//div[@class='mainNav_scrollContainer']/ul/li/a)[1]");
	public static By accountBtn = By.xpath("//span[@class='accountBtn']");
	public static By checkboxLabel = By.xpath("//label[@class='formWrap_label formWrap_checkboxLabel signinForm_checkboxLabel']");
	public static By checkboxUncheck = By.xpath("//input[@class='formWrap_checkbox signInForm_checkbox']");
	public static By LoginEmail = By.xpath("//input[@name='email']");
	public static By LoginPassword = By.xpath("//input[@name='password']");
	public static By SignInButton = By.xpath("//button[text()='Sign In']");
	public static By QAUserPopUp = By.xpath("//button[text()='Close this Modal']");	
	public static By btnObjMyWine = By.xpath("//a[@class='activityBtn']");		
	public static By createAccountLnk= By.xpath("//a[text()='Create Account']");
	public static By txtSignIn = By.xpath("//button[text()='Sign In']");			
	public static By SignoutLink = By.xpath("(//div[@class='headerAccountMenu']//a[text()='Sign Out'])");
	public static By JoinNowButton= By.xpath("//a[text()='Create Account']");
	public static By FirstName= By.xpath("//input[@name='firstName']");
	public static By LastName= By.xpath("//input[@name='lastName']");
	public static By Email= By.xpath("//input[@name='email']");
	public static By Password= By.xpath("//input[@name='password']");
	public static By CreateAccountButton= By.xpath("//form[@class='formWrap registerForm']/button");
	public static By txtPwdErrorMessage= By.xpath("//form[@class='formWrap registerForm']/div[1]");	
	public static By txtEmailPreferences= By.xpath("(//nav[@class='accountMenu']//a[text()='Email Preferences'])");
	public static By btnDeleteAlerts = By.xpath("(//a[text()='Delete'])");
	public static By btnEmailPreferenceRadio = By.xpath("(//input[@class='formWrap_radio frequency_radio frequency_full'])[1]");		
	public static By btnApplyNow =By.xpath("//span[@class='promoBarModal_applyBtnText' and text()='Apply Now']");
	public static By imgApplyNowClose = By.xpath("//i[@class='promoBarModal_cancelIcon icon icon-menu-close']");
	public static By SelectState =By.xpath("(//form[@class='formWrap searchBarForm']/div/div/select[@class='state_select state_select-promptsCartTransfer'])[2]");
	public static By toolTipComponet = By.xpath("(//a[@class='tooltipComponent_text tooltipComponent_link'])[1]");
	public static By AWUSiteCantReached = By.xpath("//h1[text()='This site can\\u2019t be reached']");
	
	public static By OrdersHistory = By.xpath("(//nav[@class='accountMenu']//a[text()='Order Status'])");
	public static By AddressBook = By.xpath("(//nav[@class='accountMenu']//a[text()='Address Book'])");
	public static By PaymentMethods = By.xpath("(//nav[@class='accountMenu']//a[text()='Payment Methods'])");
	public static By EmailPreferences = By.xpath("(//div[@class='headerAccountMenu']//a[text()='Email Preferences'])");	
	public static By obj_ForgotPasowrd = By.xpath("//a[text()='Forgot your password?']");
	public static By obj_NewToWine = By.xpath("//p[@class='modalWindow_title signInWrap_secondaryTitle']");
	public static By obj_SignInFacebook = By.xpath("(//a[@class='btn btn-large btn-rounded btn-facebook formWrap_btn signInWrap_facebook']");
	public static By btnsignInLinkAccount = By.xpath("(//a[text()='Sign in'])[2]");
	public static By btnverifySave = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing emailPreferences_saveBtn']");
	public static By txtInvalidLoginErrorMsg = By.xpath("//p[text()='Invalid email address or password.']");
	public static By btnSignInLnkR = By.xpath("(//a[text()='Sign in'])[2]");	
	public static By dwnSelectState = By.xpath("(//form[@class='formWrap searchBarForm']/following::select[@id='stateSelect-main'])");
			
/*===================================Forgot Password validation======================================*/
	public static By txtEnterEmailAddMsg = By.xpath("//p[@class='resetPasswordForm_disclaimer']");
	public static By txtinvalidEmailMsg = By.xpath("//p[text()='Email not found. Please enter a valid email address.']");		
	public static By btnForgotPasswordLink = By.xpath("//a[@class='formWrap_link formWrap_link-blue signinForm_forgotPassword']");
	public static By txtForgotEmail = By.xpath("//input[@class='resetPasswordForm_email resetPasswordForm_input formWrap_input']");	
	public static By btnForgotContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn resetPasswordForm_btn']");
	public static By txtCheckUrEmail = By.xpath("//p[@class='resetPasswordSuccess_paragraph']");
	
	/****************************************Account info**************************************************/
	public static By lnkAccountInfo = By.xpath("(//div[@class='headerAccountMenu']//a[text()='Account Info'])");
	public static By btnSaveAccountInfo = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing updateAccount_save']");		
	public static By txtBrthMonth = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-month']/label/input");
	public static By txtBrthDay = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-day']/label/input");	
	public static By txtBrthYear = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-year']/label/input");
	public static By BtnSave = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing updateAccount_save']");
	public static By txtYearErrorMsg = By.xpath("//form[@class='formWrap updateAccount_form']/div/p[text()='You must be at least 21 to use Wine.com.']");
	public static By btnSaveButtonAccountInfo = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing updateAccount_save']");
	
	public static By txtSignInTextR = By.xpath("//p[text()='Existing Customer?']");
	public static By btnSignInBtnR = By.xpath("//button[text()='Sign In']");
	public static By txtClearUpdateAccountLastName = By.xpath("//form[@class='formWrap updateAccount_form']/fieldset[@class='formWrap_group updateAccount_lastName']/label/input");
	public static By txtUpdateAccountemail = By.xpath("//form[@class='formWrap updateAccount_form']/fieldset[@class='formWrap_group updateAccount_email']/label/input");
	public static By txtEnterUpdateAccountLastName = By.xpath("//form[@class='formWrap updateAccount_form']/fieldset[@class='formWrap_group updateAccount_lastName isInvalid']/label/input");

	/*******************Continue with facebook********************************************************************/
	public static By btnContinueWithFacebook = By.xpath("//a[@class='btn btn-large btn-rounded btn-outline formWrap_btn signInWrap_facebook']");
	public static By txtFacebookEmail = By.xpath("//input[@id='email']");
	public static By txtFacebookPass = By.xpath("//input[@id='pass']");
	public static By txtFacebookLogin = By.xpath("//button[@name='login']");	
	public static By txtToolTipComponet = By.xpath("(//a[@class='tooltipComponent_text tooltipComponent_link'])");
	
			
}
