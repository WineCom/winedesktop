package wine.autotest.mobile.pages;

import org.openqa.selenium.By;

public class ThankYouPage {
	
	
	public static By lnkOrderNumber = By.xpath("//p[@class='thankYou_orderLink']/a");
	public static By lnkOrderDate = By.xpath("//span[@class='openOrdersDelivery_statusLarge']");
	public static By spnTickSymbol = By.xpath("//span[@class='icon icon-check']");
	public static By spnOrderConfirmation = By.xpath("//p[text()='You will receive an order confirmation email at']");
	public static By spnthanksOnOrderPage = By.xpath("//p[(text()='Thanks!')]");
	public static By spnverifyTextOrderNumberIs = By.xpath("//p[(text()='Your order number is ')]");
	public static By spnverifyOrderAgain = By.xpath("//span[(text()='Order Again')]");	
	public static By spnVerifyTextSendThisWine = By.xpath("//p[(text()='Send this wine to another recipient.')]");	
	public static By txtYourOrder = By.xpath("//div[@class='headerAccountMenu']//a[text()='Order Status']");
	public static By txtAddressBook = By.xpath("//div[@class='headerAccountMenu']//a[text()='Address Book']");
	
	public static By btnUserDataCardEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[1]");
	
	public static By btnUserDataFirstNameClear = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");
	public static By txtPaymentMethods = By.xpath("//div[@class='headerAccountMenu']//a[text()='Payment Methods']");
	public static By btnUserDataSave = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn']");
	public static By btnVerifyContinueButtonShipRecpt = By.xpath("//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn']");
	public static By txtExpectedFirstName = By.xpath("//div[@class='userDataCard_headline shippingAddress_name']");	
	public static By txtAddNewAddressLink = By.xpath("//a[text()='Add a new address']");
	public static By spnShippingAddressCount = By.xpath("//li[@class='userDataCard_listItem shippingAddress_listItem']");
	
	public static By spnEditShippingAddress = By.xpath("(//a[@class='userDataCard_editBtn'])[1]");
	public static By btnRemoveAddress = By.xpath("(//a[@class='shippingRecipient_removeBtn'])[1]");
	
	public static By btnYesremoveThisAddress = By.xpath("//button[text()='Yes, remove']");
	public static By btnRemoveThisAddressUserProfile = By.xpath("(//a[text()='Remove this address'])");	
	public static By obj_ShippingAndHnadling = By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");	
	public static By spnGiftCardRemZero = By.xpath("//ul[@class='giftCardsOption_collection userDataCard']/li[@class='userDataCard_listItem storedPayment_listItem']");
	public static By txtAddressFullName = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");	
	public static By txtStreetAddress = By.xpath("//input[@placeholder='Street Address']");
	public static By txtCity = By.xpath("(//input[@placeholder='City'])");
	public static By dwnObj_State = By.xpath("//section[@class='shippingRecipientSection']//select[@name='shipToState']");
	public static By txtObj_Zip = By.xpath("//input[@placeholder='Zip']");
	public static By txtPhoneNum = By.xpath("//input[@placeholder='Phone # (mobile preferred)']");	
	public static By btnAddressSave = By.xpath("//button[text()='Save']");
	public static By btnDeliveryContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn cartButton delivery_actionText']");
	
		
	/************************************Credit card edit in payment methods***************************************/
	
	public static By txtCreditCardName = By.xpath("(//li[@class='userDataCard_listItem storedPayment_listItem'])[2]/div[@class='userDataCard_headline storedPayment_name']");
	public static By txtEnterNameField = By.xpath("//fieldset[@class='formWrap_group isInvalid']/label/input[@name='name']");
	
	public static By lnkCreditCardEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[2]");
	
	public static By txtEditNameField = By.xpath("//form[@class='paymentForm_form']/fieldset/fieldset[@class='formWrap_group']/label/input[@name='name']");
	public static By txtEditCVVField = By.xpath("//fieldset[@class='formWrap_group formWrap_group-third formWrap_group-cvv paymentForm_cvvFieldset']/label/input");
	
	public static By btnSaveCreditCard = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])[1]");
	public static By txtUserPayementName = By.xpath("(//div[@class='userDataCard_headline storedPayment_name'])[2]");
		
	
	public static By txtAddNewCard = By.xpath("//a[text()='Add a new card']");
	public static By txtNameOnCard = By.xpath("//input[@placeholder='Name On Card']");
	public static By txtCardNumber = By.xpath("//input[@id='cardNumber']");
	
	public static By txtExpiryMonth = By.xpath("(//form[@class='paymentForm_form']//fieldset/div/fieldset/label/input[@id='expirationMonth'])");
	public static By txtExpiryYear = By.xpath("(//form[@class='paymentForm_form']//fieldset/div/fieldset/label/input[@id='expirationYear'])");
	public static By txtCVV = By.xpath("(//input[@id='expirationCvv'])");
	public static By txtBillingAddress = By.xpath("(//input[@name='address1'])");	
	public static By txtBillingCty = By.xpath("//fieldset[@class='formWrap_group paymentForm_cityWrap formWrap_group-half']/label/input");	
	public static By txtBillingSelState = By.xpath("(//select[@class='state_select'])");	
	public static By txtBillingZipCode = By.xpath("//fieldset[@class='paymentForm_zipCodeWrap formWrap_group formWrap_group-quarter float-right']/label/input");
	public static By txtBillingPhoneNum = By.xpath("(//fieldset[@class='formWrap_group']/label/input)[4]");
	
	public static By txtMakeThisPreferredCard = By.xpath("//fieldset[@class='formWrap_group stateSelect_checkboxGroup paymentForm_makePreferred']/label");
	
	public static By txtBillingSaveButton = By.xpath("//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-new']");
	public static By lnkcreditCardEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[2]");
	
	public static By btnemoveThisCard = By.xpath("//a[text()='Remove this card']");
	public static By btnRemoveThisCard = By.xpath("(//a[text()='Remove this card'])");	
	public static By btnYesRemoveThisCard = By.xpath("//button[text()='Yes, remove']");
	public static By txtYouDoNotHaveCreditCard = By.xpath("//p[text()='You do not have any credit cards saved']");	
	public static By txtPromoBarDisp = By.xpath("(//div[contains(@class,'promoBarWrap')])");
	
	public static By txtProgressBarOrderHistory = By.xpath("(//div[contains(@class,'openOrderStatus')])[1]");
	
	public static By txtNewBillingAddress = By.xpath("(//input[@class='formWrap_input checkoutForm_input'])[11]");	
	public static By NewBillingSuite = By.xpath("(//form[@class='paymentForm_form']//fieldset//input[@class='formWrap_input checkoutForm_input'])[2]");
	public static By txtNewBillingCity = By.xpath("(//form[@class='paymentForm_form']//fieldset//input[@class='formWrap_input checkoutForm_input'])[4]");
	
	public static By txtBillinState = By.xpath("//form[@class='paymentForm_form']//select[@class='state_select']");
	public static By txtObj_BillingZip = By.xpath("(//input[@name='zipOrPostalCode'])[1]");
	public static By txtNewBillingPhone = By.xpath("(//form[@class='paymentForm_form']//fieldset//input[@class='formWrap_input checkoutForm_input'])[5]");
	public static By btnBillingSave = By.xpath("(//button[text()='Save'])[1]");
	public static By obj_WineLog = By.xpath("(//a[@class='wineLogo'])[1]");
	public static By txtPhoneNumberError = By.xpath("//div[@class='formWrap_errorMessage formWrap_errorMessage-showing']/p");
	
	/************************************VerifyPromoBarIsDisplayedInAccountPages***************************************/
	
	public static By txtPaymentMethodsHeader = By.xpath("//h2[text()='Payment Methods']");
	public static By txtAccountInfoPromoBar = By.xpath("(//div[@class='promoBarWrap js-has-fadeInOut']//div[@class='promoBar_headerText-short cmsRichText']/p/span)");
	public static By txtOrdersHistoryNav = By.xpath("(//nav[@class='accountMenu']//a[text()='Order Status'])");
	public static By txtOrderHistoryPage = By.xpath("//h3[text()='Open Orders']");
	public static By txtAddressBookNav = By.xpath("(//nav[@class='accountMenu']//a[text()='Address Book'])");
	public static By txtAddressBookHeader = By.xpath("//h2[text()='Address Book']");
	public static By txtPaymentMethodsNav = By.xpath("(//nav[@class='accountMenu']//a[text()='Payment Methods'])");
	public static By txtEmailPreferencesNav = By.xpath("(//nav[@class='accountMenu']//a[text()='Email Preferences'])");
	public static By txtEmailPreferenceHeader = By.xpath("//h2[text()='Email Preferences']");
	
	
	/********************verify address gets deleted permanently from user profile***********************************/
	public static By imgWineLogoRecPage = By.xpath("(//div[@class='corePage winePage']//a[@class='wineLogo'])");
	public static By lnkUserDataEdit = By.xpath("//a[@class='userDataCard_editBtn']");
	public static By txtYouDoNotHaveAnyAddress = By.xpath("//p[text()='You do not have any addresses saved.']");
	
									
}
