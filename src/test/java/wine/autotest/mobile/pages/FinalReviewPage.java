package wine.autotest.mobile.pages;

import org.openqa.selenium.By;

public class FinalReviewPage {
	
	//Address
	public static By dwnShippingTyp = By.xpath("//section[@class='refinementWidgetSection']/div[1]/div[3]/div[5]");
	public static By radShippingTyp = By.xpath("(//fieldset[@class='formWrap_group shippingForm_shippingType']/label/input[@value='shipHome'])");
	public static By radShippingType=By.xpath("(//fieldset[@class='formWrap_group shippingForm_shippingType']/label/input[@name='shippingTypeRadio' and @value='shipHome'])");
	public static By txtFirstName = By.xpath("//input[@name='firstName']");
	public static By txtLastName = By.xpath("//input[@name='lastName']");
	public static By txtStreetAddress = By.xpath("(//section[@class='shippingRecipientSection']//input[@placeholder='Street Address'])");
	public static By txtCity = By.xpath("(//section[@class='shippingRecipientSection']//input[@placeholder='City'])");
	public static By dwnState = By.xpath("(//section[@class='shippingRecipientSection']//select[@name='shipToState'])");
	public static By obj_StateMobile = By.xpath("(//section[@class='shippingRecipientSection']//input[@name='phone'])");	
	public static By txtZipCode = By.xpath("(//section[@class='shippingRecipientSection']//input[@name='zip'])");
	public static By txtobj_Zip = By.xpath("(//input[@class='formWrap_input checkoutForm_input' and @name='zip'])");	
	public static By txtReceipentZipCode = By.xpath("(//fieldset[@class='formWrap_group formWrap_group-quarter float-right']//input[@placeholder='Zip'])");
	public static By txtPhoneNum = By.xpath("//input[@placeholder='Phone # (mobile preferred)']");
	public static By btnShipContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn recipientAction_continue']");
	public static By btnVerifyContinue = By.xpath("(//div[@class='shippingRecipient_btnBar']//button[contains(text(),'Continue')])");
	public static By btnDeliveryContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn cartButton delivery_actionText']");
	public static By btnAddressContinue = By.xpath("//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn']");
	public static By spnlocalPickUp = By.xpath("//span[text()='Find nearby Local Pickup locations']");
	public static By btnshippingSaving = By.xpath("//div[@class='winePage']//section[@class='checkoutMainContent']/div[@class='checkoutMainContent_mainSteps']/section[1]//div[@class='recipientAction_btnContainer']/button[.='Saving...']");
	public static By lnkaddGiftMessage = By.xpath("//a[@class='checkoutHeader_giftNotSetLink']");
	public static By txtgiftRecipientEmail = By.xpath("//fieldset[@class='formWrap_group checkoutForm_textareaGroup']/input");
	public static By txtgiftMessageTextArea = By.xpath("//fieldset[@class='formWrap_group checkoutForm']/textarea");
	public static By btnRecipientContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn recipientAction_continue']");
	public static By txtDeliveryViaEmail = By.xpath("//p[text()='Delivered via email']");	
	public static By btnVerifyContinueShipRecpt = By.xpath("//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn']");
	public static By lnkEditShippingAddress = By.xpath("(//a[@class='userDataCard_editBtn'])[1]");	
	public static By lnkRecipientEid = By.xpath("(//a[@class='checkoutHeader_editBtn'])[1]");
	public static By lnkAddNewAddressLink = By.xpath("//a[text()='Add a new address']");
	public static By spnRecipientChangeAddress = By.xpath("//span[text()='Change Address']");	
	public static By lnkShippingTypeMobile = By.xpath("(//fieldset[@class='formWrap_group shippingForm_shippingType']/label/input[@name='shippingTypeRadio' and @value='shipHome'])");
	
	public static By lnkRecipientaddressFormHeader = By.xpath("//header[@class='checkoutForm_headline checkoutForm_adultRequired']");	
	public static By lnkShipAdrEdit = By.xpath("(//a[@class='userDataCard_editBtn'])[1]");
	public static By txtFhipAdrFullName = By.xpath("//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");	
	public static By cboChipAdrPreferredCardCheckBox = By.xpath("(//form[@class='shippingRecipientSection_form']//fieldset[@class='formWrap_group stateSelect_checkboxGroup shippingRecipient_makePreferred']/label[@class='formWrap_label formWrap_checkboxLabel'])[2]");
	public static By bynShipAdrSave = By.xpath("(//div[@class='shippingRecipient_btnBar shippingRecipient_saveSection js-is-hidden']//button[text()='Save'])");
	public static By btnShipAdrSave = By.xpath("(//div[@class='modalWindow']//button[text()='Save'])");	
	public static By txtShipAdrExpFullName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[1]");
	public static By btnShipTothisAdrRadion = By.xpath("(//input[@class='formWrap_radio checkoutForm_radio shippingSelectedAddress_input'])[1]");
		
	public static By btnStayInPreviousStateLink = By.xpath("//div[@class='cartTransfer_link cartTransfer_stayInState']");
	public static By btnRecipientHeader = By.xpath("(//div[@class='checkoutHeader'])[1]");
	public static By txtShippingHeaderMsg = By.xpath("//h2[text()='Edit Shipping Address']");	
	public static By txtShippingHeaderTitle = By.xpath("//h3[@class='verifyForm_title headline-h3']");
	
	public static By lnkAddressBook = By.xpath("(//div[@class='mainNav_scrollContainer']//a[text()='Address Book'])");	
	public static By lnkAddNewAddress = By.xpath("//a[text()='Add a new address']");	
	public static By txtAddressFullName = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");	
	public static By btnAddressSave = By.xpath("//button[text()='Save']");		
	public static By txtAddressBookHeader = By.xpath("//h2[text()='Address Book']");	
	public static By txtPreferredAddress = By.xpath("//ul[@class='shippingAddress_collection userDataCard']/li/div[4]/label[2]/span/span[text()='Preferred Address']");
	public static By txtMakePreferred = By.xpath("(//span[text()='Make Preferred'])[1]");	
	public static By btnshippingAddressExpectedName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[2]");
	public static By cboShippingSelectedAddress = By.xpath("(//span[@class='shippingSelectedAddress_group'])[2]");	
	public static By btnShippingAddressActualName = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[1]");
	public static By txtMustSelectShippingAddress = By.xpath("//p[text()='Please select an address option']");
	public static By lnkShippingAddressCountInRcp = By.xpath("//span[@class='shippingAddressLinks_count']");
	
	public static By btnHomeAddressEdit= By.xpath("//li[div[label[span[span[text()='Preferred Address']]]]]/div/a[text()='Edit']");
	public static By btnHomeAddressEditStreetAddressClear= By.xpath("(//fieldset[@class='formWrap_group'])[11]/label/input");
	public static By txtHomePhoneNumber= By.xpath("(//form[@class='shippingRecipientSection_form']//fieldset[@class='formWrap_group']//input[@name='phone'])[2]");	
	public static By txtHomeAddressEditStreetAddress= By.xpath("(//fieldset[@class='formWrap_group isInvalid'])[1]/label/input");
	public static By btnHomeAddressSaveButton= By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn'])[2]");
	public static By lnkExpectedEditedAddress= By.xpath("(//div[@class='shippingAddress_addressOne'])[1]");
	public static By btnShipCont= By.xpath("//div[@class='winePage']//section[@class='checkoutMainContent']/div[@class='checkoutMainContent_mainSteps']/section[1]//button[.='Saving...']");
	
	public static By SuggestedAddress= By.xpath("html/body/div[1]/div[2]/div/div[2]/div/ul/li[1]/div[3]/label");	
	public static By txtMustSelectAddress= By.xpath("//p[text()='Must select address before proceeding']");	
	public static By txtAddNewAddressLink= By.xpath("//a[text()='Add a new address']");
	
	/***************************Shipping Methods*******************************************************/
	public static By lnkShippingAddressEdit = By.xpath("(//a[text()='Edit'])[2]");
	public static By dwnShipState = By.xpath("(//div[@class='modalWindow']//select[@name='shipToState'])");
	public static By txtZip = By.xpath("(//div[@class='modalWindow_content']//input[@class='formWrap_input checkoutForm_input' and @name='zip'])");
	public static By txtPhoneNumbere = By.xpath("//input[@placeholder='Phone # (mobile preferred)']");	
	public static By btnShippingAddressSave = By.xpath("//div[@class='shippingRecipient_btnContainer']/button");
	public static By btnVerifyContinueButton = By.xpath("(//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn'])");
	public static By cboShippingAddrRadioButton = By.xpath("(//input[@name='shippingAddress'])");
	public static By cboShipToHomeRadioButton = By.xpath("(//input[@name='shippingTypeRadio' and @value='shipHome'])");	
	public static By cboOriginalAddrsRadioButton = By.xpath("(//input[@class='formWrap_radio checkoutForm_radio verifyAddressPreferences_useShippingAddress'])[2]");
	public static By btnShippingSaving = By.xpath("//div[@class='winePage']//section[@class='checkoutMainContent']/div[@class='checkoutMainContent_mainSteps']/section[1]//div[@class='recipientAction_btnContainer']/button[.='Saving...']");
	public static By txtEstimatedArival = By.xpath("//p[text()='Estimated Arrival']");
	public static By dwnSelectShippingMethod = By.xpath("//select[@name='shippingMethod']");
	public static By lnkSelectStandardShippingMethodOption = By.xpath("//select[@name='shippingMethod']/option[@value='1']");
	public static By lnkSelectShippingMethodOption = By.xpath("//select[@name='shippingMethod']/option[contains(text(),'Overnight p.m')]");
	public static By cboSelectShippingMethodOption  = By.xpath("//select[@name='shippingMethod']/option[contains(text(),'Overnight p.m')]");
	public static By cboShippingmethodPmtext  = By.xpath("//select[@class='shippingMethod_select']/option[contains(text(),'Overnight p.m')]");
	public static By cboSelectShippingMethodOption2  = By.xpath("//select[@name='shippingMethod']/option[contains(text(),'Overnight a.m')]");	
	public static By cboShippingmethodAmtext  = By.xpath("//select[@name='shippingMethod']/option[contains(text(),'Overnight a.m')]");
			
	public static By txtCancellingThePreSale = By.xpath("//p[text()='Canceling this Pre-sale order is subject to a 20% cancellation fee, with the remaining balance applied as a credit in your Wine.com account.']");
	public static By txtAllPreSaleItemsDetails = By.xpath("//p[text()='All Pre-Sale items, including shipping & handling and applicable taxes, will be billed when the order is placed.']");
	
	public static By txtAbv = By.xpath("(//span[@class='prodAlcoholPercent_inner'])[2]");
	public static By txFoundLowerPrice = By.xpath("//a[text()='Found a lower price?']");
					
	/*========================Verify suggested shipping address===============================================*/
			
	public static By cboShippingAddrRadioButtonShipToHome = By.xpath("//fieldset[@class='formWrap_group shippingForm_shippingType']/label[input[@value='shipHome']]");
	public static By cboShippingAddrRadioButtonShipToFedex = By.xpath("//fieldset[@class='formWrap_group shippingForm_shippingType']/label[input[@value='shipFedEx']]");
	
	/**************************************RecipientEdit****************************************/	
	
	public static By lnkRecipientChangeAddress = By.xpath("//span[text()='Change Address']");
	public static By lnkReciptEidt = By.xpath("//a[@class='checkoutHeader_editBtn']/span");
	public static By lnkChangeAddress = By.xpath("//a[@class='checkoutHeader_editBtn']/span");
	public static By lnkRecipientEidt = By.xpath("//section[@class='refinementWidgetSection']/div[1]/div[3]/div[5]");
//	public static By lnkshippingAddressEdit = By.xpath("//input[@name='firstName']");
	public static By lnkshippingAddressEdit = By.xpath("(//ul[@class='shippingAddress_collection userDataCard']//a[text()='Edit'])[1]");
	public static By dwnshipState = By.xpath("(//div[@class='modalWindow_content']//select[@name='shipToState'])");
	public static By txtRecipientEditZipCode = By.xpath("//span[text()='Street Address']/following::input[1]");
	public static By btnshippingAddressSave = By.xpath("(//div[@class='modalWindow_content']//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn'])");
	public static By btnContinuetostate = By.xpath("//a[@class='btn btn-large btn-rounded btn-red btn-processing cartTransfer_saveBtn']");
	public static By btnReceipEditCvv = By.xpath("//fieldset[contains(@class,'paymentForm_cvvFieldset isInvalid')]//label//input[@id='expirationCvv']");
	public static By radShiptoaddressOne = By.xpath("(//span[@class='formWrap_radioSpan'])[1]");
	public static By imgGiftBagR = By.xpath("//span[@class='formWrap_checkboxSpan giftOptions_checkbox']");
	public static By txtRecipientEmail = By.xpath("//fieldset[@class='formWrap_group checkoutForm_textareaGroup']//input[@class='formWrap_input checkoutForm_input giftEmailInput']");
	public static By txtRecipientGiftCheckbox = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");
	public static By txtRecipientGiftCheckboxInValidEmail = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");
	public static By cboRecipientGiftCheckboxClick = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");	
	public static By txtRecipientEmailField = By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']");
	public static By btnReciptContinue = By.xpath("(//div[@class='recipientAction_btnContainer']//button[text()='Continue'])");
	public static By btnRecipientShipToaddress = By.xpath("(//span[@class='shippingSelectedAddress_group'])[1]");	
	public static By lnkChangePayment = By.xpath("(//a[@class='checkoutHeader_editBtn']/span[text()='Change Payment'])");
	public static By txtInvalidEmailAddressInGiftRecipientPage = By.xpath("//div[@class='checkout_errorMessage']/p[text()='Invalid email address.']");
	public static By lnkAddressCountInRecipient = By.xpath("//li[contains(@class,'userDataCard_listItem shippingAddress_listItem')]");
	public static By lnkOriginalAddressSelectRadioButton = By.xpath("(//span[@class='shippingSelectedAddress_group'])[2]/input");
	
	/****************************************Recipient modify address****************************************/
	public static By txtShippingAddressFullNameClear = By.xpath("//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input");
	public static By txtShippingAddressStreetAdrsClear = By.xpath("(//div[@class='modalWindow_content']//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group']/label/input[@name='address1'])");
	public static By txtShippingAddressCityClear = By.xpath("(//div[@class='modalWindow_content']//form[@class='shippingRecipientSection_form']//fieldset//fieldset[@class='formWrap_group formWrap_group-half']/label/input[@name='city'])");
	public static By lnkShippingOptionalClk = By.xpath("(//div[@class='modalWindow_content']//fieldset[@class='formWrap_group']//input[@placeholder='Suite/Apt # (optional)'])");
	public static By lnkShippingAddressEdtRemove = By.xpath("(//a[text()='Edit'])[4]");
	public static By txtRemoveThisAddress = By.xpath("(//div[@class='modalWindow_content']//a[text()='Remove this address'])");
	public static By txtYesremoveThisAddress = By.xpath("//button[text()='Yes, remove']");				
	public static By txtShippingAddressFullNameEnter = By.xpath("(//div[@class='modalWindow_content']//input[@class='formWrap_input checkoutForm_input' and @placeholder='Full Name'])");
	public static By txtShippingAddressStreetAdrsEnter = By.xpath("(//div[@class='modalWindow_content']//input[@class='formWrap_input checkoutForm_input' and @placeholder='Street Address'])");
	public static By txtShippingAddressCityEnter = By.xpath("(//div[@class='modalWindow_content']//input[@class='formWrap_input checkoutForm_input' and @placeholder='City'])");
	public static By txtShippingAddressSaveBtn = By.xpath("//div[@class='shippingRecipient_btnContainer']/button");
	public static By txtShippingAddressFullNameAfterEdit = By.xpath("(//div[@class='userDataCard_headline shippingAddress_name'])[1]");	
	public static By lnkShippingAddressCount = By.xpath("//li[@class='userDataCard_listItem shippingAddress_listItem']");
	
	
	/*******************************************Verify gift wrapping section**********************************************/
	public static By radRecipientGiftCheckbox = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");
	public static By cboGiftWrapOption = By.xpath("//label[span[text()='Silver Sheer']]//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan']");
	public static By cboGiftBag = By.xpath("(//label[span[text()='Silver Sheer']]//div[@class='productPrice_price-reg'])");
	public static By txtGiftWrapOptionCount = By.xpath("//span[@class='giftWrapOption_addCountText']");
	public static By txtGiftBagInOrderHistory = By.xpath("//span[contains(text(),' Gift Bag')]");
	public static By txtAddGiftMessage = By.xpath("//a[@class='checkoutHeader_giftNotSetLink']");
	public static By txtGiftMessageTextArea = By.xpath("//div[@class='checkoutSection_content']//fieldset[@class='formWrap_group checkoutForm']/textarea");
	public static By txtObj_RecipientEmail = By.xpath("//fieldset[@class='formWrap_group checkoutForm_textareaGroup']//input[@class='formWrap_input checkoutForm_input giftEmailInput']");
	public static By txtGiftMessage = By.xpath("//p[@class='checkoutHeader_giftMessageSummary']/span");
	public static By txtGiftMessageSummary = By.xpath("//p[@class='checkoutHeader_giftMessageSummary']");		
	public static By btnViewCart = By.xpath("//p[text()='View Cart']");
	public static By imgProdItemgiftIcon = By.xpath("(//li[@class='prodItem']//div[@class='prodItem_wrap js-has-giftWrap']/i[@class='icon icon-gift prodItem_giftIcon'])[1]");
	public static By cboRecipientGiftAdd = By.xpath("(//label[span[text()='Silver Sheer']]//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])");	
	public static By txtRecipientGiftMessage = By.xpath("//fieldset[@class='formWrap_group checkoutForm']//textarea[@class='formWrap_textarea giftMessageBox']");
	public static By imgGiftBagNames1 = By.xpath("//span[text()='Silver Sheer']");
	public static By imgGiftBagNames2 = By.xpath("//span[text()='Natural Burlap']");
	public static By imgGiftBagNames3 = By.xpath("//span[text()='Red Velvets']");	
	public static By btnRecipientedit = By.xpath("//a[text()='Edit'][1]");
	public static By imgNoGiftBag = By.xpath("//span[div[span[text()='No Gift Bag']]]//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan']");	
	
	/**************************GiftCardsCheckBoxInPayment*******************************************/
	
	public static By txtGiftCardUsed = By.xpath("//th[text()='Gift Card Used']");
	public static By txtGiftCardRemaining = By.xpath("//th[text()='Gift Card Remaining']");
	public static By txtgiftCardUsed = By.xpath("//td[@class='orderSummary_price orderSummary_giftCardUsedTotal']");
	
	/************************VerifyGftCrdAccIsDispInPymntOpt*******************************/
	public static By objGiftCardAct = By.xpath("(//label[@class='userDataCardPreferences_group userDataCardPreferences_group-left storedPayment_applyGiftCard'])[1]");
	public static By objGiftCardBal = By.xpath("(//div[@class='storedPayment_remaining'])[1]");
	public static By txtRemainingGiftCard1 = By.xpath("(//label[@class='userDataCardPreferences_group userDataCardPreferences_group-left storedPayment_applyGiftCard'])[1]");
	public static By txtRemainingGiftCard2 = By.xpath("(//label[@class='userDataCardPreferences_group userDataCardPreferences_group-left storedPayment_applyGiftCard'])[2]");

	
	/*******************************GiftCardsCheckBoxInPayment***********************************/
	public static By txtChckBoxGiftCard1 = By.xpath("(//input[@class='formWrap_checkbox checkoutForm_checkbox billingSelectedGiftCard_input'])[1]");
	public static By txtChckBoxGiftCard2 = By.xpath("(//input[@class='formWrap_checkbox checkoutForm_checkbox billingSelectedGiftCard_input'])[2]");
	public static By txtChckBoxGiftCard3 = By.xpath("(//input[@class='formWrap_checkbox checkoutForm_checkbox billingSelectedGiftCard_input'])[3]");
	
	/******************************VerifyDiffGiftBagTot******************************************/
	public static By spnGiftoptionsR = By.xpath("//a[@class='checkoutHeader_giftNotSetLink']");
	public static By txtGiftbag399 = By.xpath("(//span[@class='giftWrapOption_group'][div[span[text()='3']]]//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])");
	public static By txtGiftbag699 = By.xpath("(//span[@class='giftWrapOption_group'][div[span[text()='6']]]//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])");	
	public static By txtGiftbag999 = By.xpath("(//span[@class='giftWrapOption_group'][div[span[text()='9']]]//span[@class='formWrap_radioSpan giftWrapOption_radioBagSpan'])");	
	public static By spnGiftWrappingSection = By.xpath("//span[@class='giftWrapping_total']");	
	public static By spnGiftBag = By.xpath("(//div[@class='productPrice_price-reg'])[1]");
	public static By spnProdItemgiftIcon = By.xpath("(//li[@class='prodItem']//div[@class='prodItem_wrap js-has-giftWrap']/i[@class='icon icon-gift prodItem_giftIcon'])[1]");
	public static By spnGiftbag2 = By.xpath("(//div[@class='productPrice_price-reg'])[2]");
	public static By spnGiftbag3 = By.xpath("(//div[@class='productPrice_price-reg'])[3]");
	
	
				
	/****************************************Payments************************************************/
	public static By lnkchangePayment = By.xpath("//span[text()='Change Payment']");
	public static By lnkAddNewCard = By.xpath("//a[text()='Add payment method']");
	public static By txtNameOnCard = By.xpath("//input[@placeholder='Name On Card']");
	public static By txtCardNumber = By.xpath("//input[@id='cardNumber']");
	public static By dwnExpiryMonth = By.xpath("(//input[@class='formWrap_input checkoutForm_input paymentForm_expMonthSelect'])");
	public static By dwnExpiryYear = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='paymentForm_expYear formWrap_group formWrap_group-third']/label/input");
	public static By txtCVV = By.xpath("//input[@class='formWrap_input checkoutForm_input paymentForm_cvv']");
	public static By chkBillingAndShippingCheckbox = By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']");
	public static By chkBillingAndShipping = By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label");
	public static By txtbirthMonth = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-month']/label/input[@name='monthOfBirth']");
	public static By txtbirthDate = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-day']/label/input[@name='dayOfBirth']");
	public static By txtbirthYear = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-year']/label/input[@name='yearOfBirth']");
	public static By btnPaymentContinue = By.xpath("//button[@class='btn btn-large btn-rounded btn-red btn-processing formWrap_btn cartButton payment_actionText']");
	public static By lnkPaymentEdit = By.xpath("(//a[text()='Edit'])[6]");
	public static By txtCVVR = By.xpath("//div[@class='modalWindowWrap']//section[@class='paymentFormSection']/form[@action='/account/payments']//input[@id='expirationCvv']");
	public static By txtDOBFormat = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-month']/following-sibling::fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-day']/following-sibling::fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-year']");
	public static By btnpaywithThisCardSave = By.xpath("//div[@class='modalWindow_content']/section[@class='paymentFormSection']/form[@action='/account/payments']//div[@class='paymentForm_btnContainer']/button[.='Save']");
	public static By dwnChangeDeliveryDate = By.xpath("//span[text()='Change Delivery Date']");
	public static By btnFnlRvwContinueShoppingBtn = By.xpath("//a[text()='Continue shopping']");
	public static By btnPaymentCheckoutEdit = By.xpath("(//a[@href='/checkout/payment/list'])[1]");	
	public static By lnkPaymentMethods = By.xpath("(//a[text()='Payment Methods'])[2]");
	public static By txtAddNewCard = By.xpath("//a[text()='Add a new card']");	
	public static By txtBillingAddress = By.xpath("(//fieldset[@class='formWrap_group']/label/input)[2]");
	public static By txtBillingCty = By.xpath("//fieldset[@class='formWrap_group paymentForm_cityWrap formWrap_group-half']/label/input");
	public static By txtBillingSelState = By.xpath("(//select[@class='state_select'])[1]");
	public static By txtBillingZipCode = By.xpath("//fieldset[@class='paymentForm_zipCodeWrap formWrap_group formWrap_group-quarter float-right']/label/input");
	public static By txtobj_BillingPhoneNum = By.xpath("(//fieldset[@class='formWrap_group']/label/input)[4]");
	public static By btnBillingSave = By.xpath("(//button[text()='Save'])[1]");	
	
	public static By txtPaymentMethodsHeader = By.xpath("//h2[text()='Payment Methods']");		
	public static By btnPaymentEdit = By.xpath("(//div[@class='userDataCard_actionBtns storedPayment_actionBtns'])[1]");
	public static By txtPaymentCVV = By.xpath("(//input[@name='cvv'])[2]");
	public static By btnPaymentSaveButton = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])[2]");	
	public static By txtCvvR = By.xpath("(//input[@class='formWrap_input checkoutForm_input paymentForm_cvv'])[2]");
	public static By btnPaywithThisCardSave = By.xpath("(//div[@class='paymentFormSection paymentFormSection-creditCard']//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])");
	public static By txtCvvDisp = By.xpath("(//form[@class='paymentForm_form'])[1]/fieldset/div/fieldset[@class='formWrap_group formWrap_group-third formWrap_group-cvv isInvalid']/label/input");	
	public static By cboSelectPayWithThisCard = By.xpath("(//input[@name='payment'])[1]");
	public static By cboSelectCreditCard = By.xpath("(//div[@class='userDataCardPreferences']//span[@class='billingSelectedAddress_group'])[1]");		
	public static By lnkBillPaymentEdit = By.xpath("//span[text()='Change Payment']");
	public static By btnPaymentLinksCount = By.xpath("//a[@class='paymentLinks_link paymentLinks_link-right paymentLinks_viewAll']/span[2]");
	public static By btnUncheckCreditCard = By.xpath("(//label[@class='formWrap_label formWrap_checkboxLabel'])[2]");
	public static By txtPleaseSpeciftCardProvider = By.xpath("//div[@class='checkout_errorMessage']//p");
	public static By btnViewAllPaymentMethods = By.xpath("//a[@class='paymentLinks_link paymentLinks_link-right paymentLinks_viewAll']");
	public static By txtCreditCardCountInRecipient = By.xpath("//li[contains(@class,'userDataCard_listItem storedPayment_listItem')]");
	public static By btnModalWindowIcon = By.xpath("//div[@class='js-closeModal modalWindow_cancel icon icon-menu-close']");
	public static By lnkViewAllAllAddress = By.xpath("//a[@class='shippingAddressLinks_link shippingAddressLinks_link-right shippingAddress_viewAll']");

	/***********************************************************Credit card remove*******************************************************/
	public static By lnkEditCreditCard = By.xpath("(//a[text()='Edit'])[7]");
	public static By btnRemoveThisCard = By.xpath("(//div[@class='paymentForm_btnBar']//a[text()='Remove this card'])");
	public static By btnYesRemove = By.xpath("//button[text()='Yes, remove']");	
	public static By lnkPaymentLinksCount = By.xpath("//a[@class='paymentLinks_link paymentLinks_link-right paymentLinks_viewAll']/span[2]");	
	public static By btnPreferredAddressRadio = By.xpath("//input[@class='formWrap_radio billingPreferredAddress_input checkoutForm_radio']");
	public static By btnEditCreditCardInPayment = By.xpath("//div[@class='userDataCard_headline storedPayment_headline'][//div[label[span[span[text()='Preferred Card']]]]]//div/a[text()='Edit']");	
	public static By btnRemovePhoneNumInPayment = By.xpath("//form[@class='paymentForm_form']//fieldset[@class='formWrap_section paymentForm_paymentAddress']//fieldset[@class='formWrap_group']/label/input[@name='phone']");
	public static By txtRemoveCreditCardInPayment = By.xpath("(//div[@class='paymentForm_btnBar']//a[text()='Remove this card'])");
	public static By btnYesRemoveCreditCardInPayment = By.xpath("//button[text()='Yes, remove']");
	public static By txtCreditCardWarningMsgInPayment = By.xpath("//p[text()='You do not have any credit cards saved']");
	
				
/***************************VerifyErrorMsgForAddingNewCreditCard****************************************/
	public static By nameRequiredErrorMsg = By.xpath("//p[text()='Name On Card is required.']");
	public static By creditCardRequiredErrorMsg = By.xpath("//p[text()='Credit Card # is required.']");
	public static By expirtDateRequiredErrorMsg = By.xpath("//p[text()='Expiration date is required.']");
	public static By cvvRequiredErrorMsg = By.xpath("//p[text()='CVV is required.']");
	
	public static By StreetAddressRequired = By.xpath("//p[text()='Street Address is required.']");
	public static By CityRequired = By.xpath("//p[text()='City is required.']");
	public static By StateAddressRequired = By.xpath("//p[text()='State/Province is required.']");
	public static By ZipRequired = By.xpath("//p[text()='Zip/Postal Code is required.']");	
	public static By PhoneRequired = By.xpath("//p[text()='Phone # is required.']");
	
	//Order Summary
	public static By spnSubtotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_subTotalPrice']");
	public static By spnShippingHnadling = By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");
	public static By spnTotalBeforeTax = By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']");
	public static By spnOrderSummaryTaxTotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_taxTotal']");
	public static By spnstewardShipSavePrice = By.xpath("//span[@class='stewardShipSection_savings']");
	public static By spnOrderSummarySalesTax = By.xpath("//tr[@class='orderSummary_row orderSummary_tax']");	
	
	public static By btnPlaceOrder = By.xpath("//button[text()='Place Order']");
	public static By txtOrderNum = By.xpath("//p[@class='thankYou_orderLink']/a");	
	public static By btnclosePopUp = By.xpath("//div[@class='extole-js-widget-wrapper extole-widget-wrapper']//a[@class='extole-js-widget-close extole-controls extole-controls--close']");
	public static By btnCartEditButton = By.xpath("//div[@class='cart_headline']//a[@class='cart_editBtn']");
	public static By txtPaywithThisCardCvv = By.xpath("(//fieldset//input[@placeholder='CVV'])[2]");
	public static By txtDeliveryTimeLine = By.xpath("//div[text()='Delivered via email']");	
	
	/****************************Order creation with Fedex Address*************************************/
	public static By chkShipToFedex = By.xpath("//label/input[@value='shipFedEx']");
	public static By txtFedexZipCode = By.xpath("//input[@class='formWrap_input checkoutForm_input shipToFedEx_zipInput']");
	public static By btnFedexSearchButton = By.xpath("//button[text()='Search']");
	public static By txtfedexLocalPickUpIsNotAvailable = By.xpath("//p[text()='Zip is not valid.']");
	public static By txtFinalReviewPage = By.xpath("//div[text()='Final Review']");
	public static By chkSelectRecipientAdr = By.xpath("(//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded '])[1]");
	
	public static By txtFedexFirstName = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@name='firstName'])");
	public static By txtFedexLastName = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@name='lastName'])");
	public static By txtFedexStreetAddress = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@placeholder='Street Address'])");
	public static By txtFedexCity = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@placeholder='City'])");
	public static By txtFedexZip = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@placeholder='Zip'])");
	public static By txtFedexPhoneNum = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@name='phone'])");
	public static By txtFedexSaveButton = By.xpath("//section[@class='shippingRecipientSection fedExShipping']//button[text()='Save']");
	public static By btnFedexContinueButton = By.xpath("(//div[@class='recipientAction']//button[text()='Continue'])");
	public static By btnZipCodeErrorMsg = By.xpath("//div[@class='formWrap_errorMessage formWrap_errorMessage-showing']/p");		
	public static By txtFedexBillingAddrss = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']/fieldset/label/input)");
	public static By txtFedexBillingCity = By.xpath("//fieldset[@class='formWrap_group paymentForm_cityWrap formWrap_group-half']/label/input");
	public static By dwnFedexBillingState = By.xpath("(//fieldset[@class='stateSelect formWrap_group formWrap_group-quarter']/label/select");
	
	public static By txtFedexBillingZip = By.xpath("//fieldset[@class='paymentForm_zipCodeWrap formWrap_group formWrap_group-quarter float-right']/label/input");
	public static By txtFedexBillingPhoneNum = By.xpath("(//div[@class='paymentFormSection paymentFormSection-creditCard']//input[@name='phone'])");
	
	public static By txtBirthMonth = By.xpath("(//input[@id='dateOfBirth-month' and @class='formWrap_input updateAccount_dobInput-month'])");
	public static By txtBirthDate = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-day']/label/input[@name='dayOfBirth']");
	public static By txtBirthYear = By.xpath("//form[@class='formWrap updateAccount_form']//fieldset[@class='formWrap_group formWrap_group-third updateAccount_dob-year']/label/input[@name='yearOfBirth']");

	public static By btnFedexAddressEditNameClear= By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName']/label/input[@name='name']");
	public static By txtFedexPhoneNumber = By.xpath("(//section[@class='shippingRecipientSection fedExShipping']//input[@name='phone'])");
	public static By lnkFedexAddressEditName = By.xpath("//fieldset[@class='formWrap_group shippingRecipientFullName isInvalid']/label/input[@name='name']");
	public static By lnkExpectedEditedName = By.xpath("//div[@class='userDataCard_headline shippingAddress_name js-is-fedEx']");	
	public static By txtShipToThisLocation = By.xpath("(//button[text()='Ship to this location'])[1]");
	public static By btnShipToThisLocation = By.xpath("(//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded '])[1]");
	public static By btnGoBackButton = By.xpath("(//span[text()='Go back'])");	
	
	public static By dwnlocationIcon = By.xpath("//div[@class='localPickupContainer_locations']/ol/li//span[@class='listItem_marker']");	
	public static By lnkActAddrOnList = By.xpath("//div[@class='description']/div[1]");	
	public static By lnkActAddrOnMap = By.xpath("//div[@class='popupDetails_description']/div[1]");
	
	public static By spnFedEXShippingDeliveryMessage = By.xpath("//span[@class='deliveryHeader_disclaimer-instructions']");	
			
/*	=======================credit card unchecked functionality==========================================================*/
	public static By cboMakePreferedCardCheckBoxHidden = By.xpath("//fieldset[@class='formWrap_group stateSelect_checkboxGroup paymentForm_makePreferred']");	
	
	public static By dwnReceiveState = By.xpath("(//select[@class='state_select'])");	
	public static By btnVerifyContinueButtonShipRecpt = By.xpath("//button[@class='btn btn-large cartButton btn-rounded btn-red btn-processing shippingRecipient_btn useSuggestedAddress_btn']");
	public static By txtNewBillingAddress = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']//input[@placeholder='Address'])");
	public static By txtNewBillingSuite = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']//input[@placeholder='Suite/Apt #'])");
	public static By txtNewBillingCity = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']//input[@placeholder='City'])");
	public static By txtBillinState = By.xpath("//form[@class='paymentForm_form']//select[@class='state_select']");
	public static By txtobj_BillingZip = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']//input[@placeholder='Zip'])");
	public static By txtNewBillingPhone = By.xpath("(//fieldset[@class='formWrap_section paymentForm_paymentAddress']//input[@placeholder='Phone #'])");
	
	public static By txtSubtotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_subTotalPrice']");
	public static By txtShippingHandaling = By.xpath("//td[@class='orderSummary_price orderSummary_priceShipping']");
	public static By txtTotalBeforeTax = By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']");
	public static By txtOrderSummaryTaxTotal = By.xpath("//td[@class='orderSummary_td-extraSpace orderSummary_price orderSummary_taxTotal']");
	public static By btnPlaceOrderButton = By.xpath("//button[@class='btn btn-large btn-rounded btn-red formWrap_btn cartButton finalReview_btn']");
	public static By txtStewardShipDifferenceAmount = By.xpath("//span[@class='stewardShipSection_difference']");

	public static By btnStewardShipAddToCart = By.xpath("(//a[@class='stewardShipUpsell_link stewardShipUpsell_addToCart'])");
	public static By btnStewardshipConfirm = By.xpath("//button[text()='Confirm']");
	public static By txtStewardshipDiscountPrice = By.xpath("//div[@class='orderSummary_price orderSummary_discountItemPrice']");
		
	public static By btnRecipientEidt = By.xpath("(//a[@class='checkoutHeader_editBtn'])[1]");
	public static By cboRecipientGiftCheckbox = By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label");
	public static By txtNameRequiredInRecipientPage = By.xpath("//p[text()='Name is required.']");
	public static By txtStreetAddressRequired = By.xpath("//p[text()='Street Address is required.']");
	public static By txtCityRequired = By.xpath("//p[text()='City is required.']");
	public static By txtStateRequiredInRecipientPage = By.xpath("//p[text()='State is required.']");
	public static By txtZipRequiredInRecipientPage = By.xpath("//p[text()='Zip is required.']");	
	public static By txtPhoneRequired = By.xpath("//p[text()='Phone # is required.']");	
	public static By cboPreferredCardSelect = By.xpath("(//input[@class='formWrap_radio billingPreferredAddress_input checkoutForm_radio'])");
	public static By lnkCreditCardHolderNameBeofreSelPay = By.xpath("(//div[@class='userDataCard_headline storedPayment_name'])");
	public static By txtNameAfterSelPayWithCard = By.xpath("(//div[@class='userDataCard_headline storedPayment_name'])");
	public static By btnPayWithThisCardSel = By.xpath("(//input[@class='formWrap_radio checkoutForm_radio billingSelectedAddress_input'])");
	public static By btnSelectPayWithThisCard = By.xpath("(//input[@name='payment'])");	
	public static By btnFnlRvwContinueShopping= By.xpath("//a[text()='Continue shopping']");	
	public static By cboRecipientState= By.xpath("(//form[@class='shippingRecipientSection_form']//select[@class='state_select'])");
	
	/*************************************calendar is displayed for 6 months********************************************/
	
	public static By cboCalenderFirstMnth= By.xpath("(//div[@class='calendarHeader'])[1]");
	public static By cboCalenderNxtMnth= By.xpath("(//div[@class='calendarHeader'])[2]");
	public static By cboPreviousMnthIcon= By.xpath("(//i[@class='calendarHeader_iconRight icon icon-arrow-right'])[1]");
	public static By cboNxtMnthIcon= By.xpath("(//button[@class='calendarHeader_iconRight icon icon-arrow-right'])[1]");
	
	/*****************************************Billing Address*******************************************/
	
	public static By txtBillingAdressState= By.xpath("(//div[@class='paymentFormSection paymentFormSection-creditCard']//fieldset[@class='stateSelect formWrap_group formWrap_group-quarter']/label/select[@class='state_select'])");
	public static By txtBillingAdressZipCode= By.xpath("(//div[@class='paymentFormSection paymentFormSection-creditCard']//fieldset[@class='paymentForm_zipCodeWrap formWrap_group formWrap_group-quarter float-right']/label/input[@name='zipOrPostalCode'])");
	public static By btnPaywithThisCardEdit = By.xpath("//li[@class='userDataCard_listItem storedPayment_listItem js-is-selected']//a");
	public static By btnPaywithThisCardCvv = By.xpath("(//fieldset[@class='formWrap_group formWrap_group-third formWrap_group-cvv paymentForm_cvvFieldset isInvalid']//input[@placeholder='CVV'])");
	public static By btnPaywithThisCard = By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing paymentForm_btn paymentForm_btn-edit'])");
	public static By btnSaveCreditCard= By.xpath("//fieldset[@class='formWrap_group checkoutForm_checkboxGroup paymentForm_saveCard']/label");
	public static By txtPhoneNumberReq= By.xpath("//div[@class='checkout_errorMessage']/p");	
	public static By cboMyWneSortWd= By.xpath("//select[@id='product-list-sort-2']");
	
	public static By spnSuggestedAddressR= By.xpath("(//label[@class='userDataCardPreferences_group verifyAddressPreferences_group userDataCardPreferences_group-left'])[1]");
	public static By spnSuugestedAddressDisply= By.xpath("//li[@class='userDataCard_listItem shippingAddress_listItem js-is-selected']");
		
	public static By txtShoppingCart= By.xpath("//div[@class='cart_headline']");
	
	
	/*********************character count gift message*************************************/	
	public static By txtMaximumCharacter= By.xpath("//fieldset[@class='formWrap_group checkoutForm']/span/span[text()='240']");
	public static By txtLimitCharacter= By.xpath("//fieldset[@class='formWrap_group checkoutForm']/span/span[text()='0']");	
	public static By btnShippingOptionalClk= By.xpath("(//fieldset[@class='formWrap_group'])");
	
	public static By ChkPreferredAddressBook= By.xpath("(//span[@class='formWrap_checkboxSpan checkoutForm_checkboxSpan'])[2]");
	public static By obj_ThirdAddrEdit= By.xpath("(//a[@class='userDataCard_editBtn'])[3]");
	public static By obj_SecondAddrEdit= By.xpath("(//a[@class='userDataCard_editBtn'])[2]");
	public static By spnRadioPrefferedAddress2= By.xpath("(//input[@class='formWrap_radio shippingPreferredAddress_input checkoutForm_radio'])[2]");
	public static By btnPreferredSave= By.xpath("(//button[@class='btn btn-small btn-rounded btn-red btn-processing shippingRecipient_btn'])[2]");
	public static By cboRadioPrefferedAddress= By.xpath("(//input[@class='formWrap_radio shippingPreferredAddress_input checkoutForm_radio'])[1]");
	
	
	/*****************************wire up calender************************************/
	
	public static By txtChangeDeliveryDate= By.xpath("//span[text()='Change Delivery Date']");
	public static By txtDeliveryHeaderDate= By.xpath("//p[@class='deliveryHeader_date']");
	public static By btnCurrentDate= By.xpath("//span[@class='calendarWidget_day date-selected date-currentMonth']");
	public static By btnNextcurrentDate= By.xpath("//span[@class='calendarWidget_day date-currentMonth date-selected']");
	public static By btnUnavailableDate= By.xpath("//span[@class='calendarWidget_day date-selected date-currentMonth']/preceding-sibling::span[1]");
	public static By btnNextAvailableDate= By.xpath("//span[@class='calendarWidget_day date-selected date-currentMonth']/following-sibling::span[1]");
	
	
			
			
}
