package wine.autotest.mobile.library;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.fw.utilities.ReportUtil;

public class UIBusinessFlow extends Mobile {


	static int expectedItemCount[]={1519,253,217,1623,271,63};
	static int actualItemCount[]=new int[expectedItemCount.length];

	/***************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean isObjectExistForSignOut(WebDriver driver)
	{
		String strStatus=null;

		try
		{
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.obj_ForgotPasowrd));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.obj_NewToWine));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.obj_SignInFacebook));
			if(strStatus.contains("false"))
			{
				return false;
			} 
			else
			{

				return true;
			}

		}catch(Exception e)
		{
			return false;
		}
	}

	/***************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to handle alert popup
	 ****************************************************************************
	 */
	public static boolean isObjectExistForSignIn()
	{
		String strStatus=null;
		try
		{

			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.OrdersHistory));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.AddressBook));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.PaymentMethods));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.EmailPreferences));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(LoginPage.SignoutLink));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{

				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}

	/***************************************************************************
	 * Method Name			: javaScriptClick()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean ClickObjectItems(By strObjectName)
	{
		WebElement ele=null;
		WebElement totalItemsBefore,ItemsAfterSelShowOutIfStock=null;
		//int numberOfItems=0;
		try
		{
			ele=driver.findElement(strObjectName);
			if(ele!=null)
			{
				ele.click();
				UIFoundation.waitFor(2L);
				totalItemsBefore=driver.findElement(ListPage.dwnTotalNoOfItems);
				String totalItem=totalItemsBefore.getText();
				int totalItemBefore=Integer.parseInt(totalItem.replaceAll("[^0-9]", ""));
				//	int totalItemBefore=Integer.parseInt(totalItemsBefore.getText());
				UIFoundation.waitFor(2L);
				UIFoundation.javaScriptClick(ListPage.lnkShowOutOfStock);
				UIFoundation.waitFor(2L);
				ItemsAfterSelShowOutIfStock=driver.findElement(ListPage.dwnTotalNoOfItems);
				String totalItemAftr=ItemsAfterSelShowOutIfStock.getText();
				//	double totalItemAfter=Integer.parseInt(ItemsAfterSelShowOutIfStock.getText());
				int totalItemAfter=Integer.parseInt(totalItemAftr.replaceAll("[^0-9]", ""));
				if(totalItemAfter>=totalItemBefore)
				{
					System.out.println("total no of items are:"+totalItemBefore);
				}
				else
				{
					System.out.println("products are not filtering properly");
				}
			}
			UIFoundation.javaScriptClick(ListPage.lnkShowOutOfStock);
			return true;

		}
		catch(Exception e)
		{
			return false;
		}
	}


	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */

	public static String login()
	{
		String objStatus=null;

		try
		{

			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.accountBtn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}
	
	/***************************************************************************
	 * Method Name			: loginR()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	
	public static String loginR()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
		    UIFoundation.waitFor(6L); 
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usrID"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "passR"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.shoppingCartPageTitle;	
			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	

	/***************************************************************************
	 * Method Name			: logout()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Logout from the Wine.com 
	 * 						  application
	 ****************************************************************************
	 */

	public static String logout()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method logout started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(5l);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.accountBtn));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignoutLink));
			//			UIBusinessFlows.isObjectExistForSignOut(driver);

			log.info("The execution of the method logout ended here ...");	
			if (objStatus.contains("false"))
			{
				System.out.println("Logout test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Logout test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method logout "+ e);
			return "Fail";
		}
	}

	/*
	 *//***************************************************************************
	 * Method Name			: recipientEdit()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */

	public static boolean recipientEdit()
	{
		String objStatus=null;
		try
		{
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEidt))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEidt));
				UIFoundation.waitFor(3L);	
			}				
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnshipState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnshipState);
		
			UIFoundation.clearField(FinalReviewPage.txtZip);			
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZip, "ZipCode"));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnRecipientContinue, "Invisible", "", 50);
			UIFoundation.waitFor(10L);
			if(objStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}
	
	/***************************************************************************
     * Method Name                    : validationForSortingCustomerRating ()
     * Created By                     : Chandra shekhar
     * Reviewed By                    : Ramesh.
     * Purpose                        : 
      ****************************************************************************
     */
    public static boolean validationForSortingCustomerRating()
    {

    	List<String> arrayList = new ArrayList<String>();;
    	boolean isSorted=true;
    	String objStatus=null;
    	String screenshotName = "Scenarios_HtoL_Screenshot.jpeg";
    	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
    			+ screenshotName;
    	try
    	{
    		List<WebElement> ele=  UIFoundation.webelements(ListPage.imgCustomerRating);
    		
    		for(int i = 0; i < ele.size()-1; i++) {                   
    			String st=ele.get(i).getText();
    			st.replaceAll("[ ]","");
    			arrayList.add(st);
    		}
    		System.out.println(arrayList);                                        
    		for(int i = 1; i < arrayList.size()-1; i++)
    		{			
    		if(arrayList.get(i-1).compareTo(arrayList.get(i)) < 0){
                    isSorted= false;
                    break;
                }    		
    		}
    		
    		if(isSorted)
    		{
    			objStatus+=true;
    			String objDetail="Products are sorted based on customer rating";
    			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
    			System.out.println("Products are sorted on customer rating");
    			return true;
    		}
    		else
    		{
    			objStatus+=false;
    			String objDetail="Products are not sorted on customer rating";
    			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
    			System.err.println("Products are not sorted on customer rating");
    			return false;
    		}

    	}catch(Exception e)
    	{
    		return false;

    	}
    }
    
    /***************************************************************************
     * Method Name                    : validationForProfissionlRating ()
     * Created By                     : Chandra shekhar
     * Reviewed By                    : Ramesh.
     * Purpose                        : 
      ****************************************************************************
     */
    public static boolean validationForProfissionlRating()
    {

    	List<String> arrayList1 = new ArrayList<String>();
    	List<String> arrayList2 = new ArrayList<String>();
    	List<String> arrayList3 = new ArrayList<String>();
    	List<String> arrayList4 = new ArrayList<String>();
    	List<String> arrayList5 = new ArrayList<String>();
    	List<String> arrayList6 = new ArrayList<String>();
    	List<String> arrayList7 = new ArrayList<String>();
    	List<String> arrayList8 = new ArrayList<String>();
    	List<String> arrayList9 = new ArrayList<String>();
    	List<String> arrayList10 = new ArrayList<String>();
    	List<String> arrayList11= new ArrayList<String>();
    	
    	boolean isSorted=true;
    	
    	String objStatus=null;
    	String screenshotName = "Scenarios_HtoL_Screenshot.jpeg";
    	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
    			+ screenshotName;
    	try
    	{
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingOne))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingOne);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList1.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList1.size()-1; i++)
    			 {			
    				 if(arrayList1.get(i-1).compareTo(arrayList1.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingTwo))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingTwo);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList2.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList2.size()-1; i++)
    			 {		
    				 String str01=arrayList2.get(i-1);
    				 String str02=arrayList2.get(i);
    				 System.out.println(""+str01);
    				 System.out.println(""+str02);    				
    				 if(arrayList2.get(i-1).compareTo(arrayList2.get(i)) > 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingThree))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingThree);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList3.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList3.size()-1; i++)
    			 {			
    				 if(arrayList3.get(i-1).compareTo(arrayList3.get(i)) > 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingFour))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingFour);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList4.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList4.size(); i++)
    			 {			
    				 if(arrayList4.get(i-1).compareTo(arrayList4.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingFive))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingFive);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList5.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList5.size()-1; i++)
    			 {			
    				 if(arrayList5.get(i-1).compareTo(arrayList5.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingSix))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingSix);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList6.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList6.size()-1; i++)
    			 {			
    				 if(arrayList6.get(i-1).compareTo(arrayList6.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingSeven))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingSeven);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList7.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList7.size()-1; i++)
    			 {			
    				 if(arrayList7.get(i-1).compareTo(arrayList7.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingEight))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingEight);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList8.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList8.size()-1; i++)
    			 {			
    				 if(arrayList8.get(i-1).compareTo(arrayList8.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingNine))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingNine);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList9.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList9.size()-1; i++)
    			 {			
    				 if(arrayList9.get(i-1).compareTo(arrayList9.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingTen))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingTen);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList10.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList10.size()-1; i++)
    			 {			
    				 if(arrayList10.get(i-1).compareTo(arrayList10.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		 if(UIFoundation.isDisplayed(ListPage.profissionalRatingEleven))
    		 {
    			 List<WebElement> ele=  UIFoundation.webelements(ListPage.profissionalRatingEleven);
    			 for(int i = 0; i < ele.size(); i++) {                   
    				 String st=ele.get(i).getText();
    				 st.replaceAll("[ ]","");
    				 arrayList11.add(st);
    			 }
    			 //	System.out.println(arrayList);                                        
    			 for(int i = 1; i < arrayList11.size()-1; i++)
    			 {			
    				 if(arrayList11.get(i-1).compareTo(arrayList11.get(i)) < 0){
    					 isSorted= false;
    					 break;
    				 }    		
    			 } 
    		 }
    		
    		 
    		
    		if(!isSorted)
    		{
    			objStatus+=true;
    			String objDetail="Products are sorted based on customer rating";
    			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
    			System.out.println("Products are sorted on customer rating");
    			return true;
    		}
    		else
    		{
    			objStatus+=false;
    			String objDetail="Products are not sorted on customer rating";
    			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
    			System.err.println("Products are not sorted on customer rating");
    			return false;
    		}

    	}catch(Exception e)
    	{
    		return false;
    	}
    }

    /***************************************************************************
	 * Method Name			: validationForSortAtoZ()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForSortingAtoZ()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
		   String screenshotName = "Scenarios_validationForSortingAtoZ_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		  
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.btnFrstProductName);
			String products2=UIFoundation.getText(ListPage.btnSecProductName);
			String products3=UIFoundation.getText(ListPage.btnThrdProductName);
			String products4=UIFoundation.getText(ListPage.btnFrthProductName);
			String products5=UIFoundation.getText(ListPage.btnFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			arrayListYr.add(products1.replaceAll("[^0-9]", ""));
			arrayListYr.add(products2.replaceAll("[^0-9]", ""));
			arrayListYr.add(products3.replaceAll("[^0-9]", ""));
			arrayListYr.add(products4.replaceAll("[^0-9]", ""));
			arrayListYr.add(products5.replaceAll("[^0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Products names are sorted in the order of A-Z");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Products names are not sorted in the order of A-Z");
			    	objStatus+=false;
			    	String objDetail="Products names are not sorted in the order of A-Z";			    	
			    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);			   
					return objStatus;
			    }
		}catch(Exception e)
		{
			System.err.println("Products names are not sorted in the order of A-Z");
	    	objStatus+=false;
	    	String objDetail="Products names are not sorted in the order of A-Z";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);	    
			return objStatus;
			
		}
	
	}

	
	/***************************************************************************
	 * Method Name			: validationForSortZtoA()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForSortingZtoA()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		   boolean isSorted = true;
		   String screenshotName = "Scenarios_validationForSortingZtoA_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.btnFrstProductName);
			String products2=UIFoundation.getText(ListPage.btnSecProductName);
			String products3=UIFoundation.getText(ListPage.btnThrdProductName);
			String products4=UIFoundation.getText(ListPage.btnFrthProductName);
			String products5=UIFoundation.getText(ListPage.btnFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=true;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Products names are sorted in the order of Z-A");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Products names are not sorted in the order of Z-A");
			    	objStatus+=false;
			    	String objDetail="Products names are not sorted in the order of Z-A";
			    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);			    	
					return objStatus;
			    }
			   
		}catch(Exception e)
		{
			System.err.println("Products names are not sorted in the order of Z-A");
	    	objStatus+=false;
	    	String objDetail="Products names are not sorted in the order of Z-A";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);	    	
			return objStatus;
		}
	
	}
	
	 /***************************************************************************
	    * Method Name                    : validationForSortingPriceLtoH ()
	    * Created By                     : Chandrashekhar.
	    * Reviewed By                    : Ramesh.
	    * Purpose                        : 
	     ****************************************************************************
	    */
	    public static boolean validationForSortingPriceLtoH()
	    {
	           WebElement ele=null;
	           String objStatus=null;
	           List<Double> arrayList = new ArrayList<Double>();;
	                  Double product1=0.0;
	                  Double product2=0.0;
	                  Double product3=0.0;
	                  Double product4=0.0;
	                  Double product5=0.0;
	           boolean isSorted=true;
	           String screenshotName = "Scenarios_LtoH_Screenshot.jpeg";
	           String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
						+ screenshotName;
	           try
	           {
	                driver.navigate().refresh();
	                  if(UIFoundation.isDisplayed(ListPage.btnFirstProductPrice))
	                  {
	                        String products1=UIFoundation.getText(ListPage.btnFirstProductPrice);
	                        products1=products1.replaceAll("[ ]","");
	                        product1=Double.parseDouble(products1);
	                        product1=UIFoundation.productPrice(product1);
	                        arrayList.add(product1);
	                        
	                  }else{
	                        if(UIFoundation.isDisplayed(ListPage.btnFrstProductPriceSale))
	                        {
	                               String products1=UIFoundation.getText(ListPage.btnFrstProductPriceSale);
	                               products1=products1.replaceAll("[ ]","");
	                               product1=Double.parseDouble(products1);
	                               product1=UIFoundation.productPrice(product1);
	                               arrayList.add(product1);
	                        }
	                        
	                  }                  
	                  
	                  if(UIFoundation.isDisplayed(ListPage.btnSecondProductPrice))
	                  {
	                        String products2=UIFoundation.getText(ListPage.btnSecondProductPrice);
	                        products2=products2.replaceAll("[ ]","");
	                        product2=Double.parseDouble(products2);
	                        product2=UIFoundation.productPrice(product2);
	                        arrayList.add(product2);
	                        
	                  }else{
	                        if(UIFoundation.isDisplayed(ListPage.btnSecondProductPriceSale))
	                        {
	                               String products2=UIFoundation.getText(ListPage.btnSecondProductPriceSale);
	                               products2=products2.replaceAll("[ ]","");
	                               product2=Double.parseDouble(products2);
	                               product2=UIFoundation.productPrice(product2);
	                               arrayList.add(product2);
	                        }
	                        
	                  }
	                  if(UIFoundation.isDisplayed(ListPage.btnThirdProductPrice))
	                  {
	                        String products3=UIFoundation.getText(ListPage.btnThirdProductPrice);
	                        products3=products3.replaceAll("[ ]","");
	                        product3=Double.parseDouble(products3);
	                        product3=UIFoundation.productPrice(product3);
	                        arrayList.add(product3);
	                        
	                  }else{
	                        if(UIFoundation.isDisplayed(ListPage.btnThirdProductPriceSale))
	                        {
	                               String products3=UIFoundation.getText(ListPage.btnThirdProductPriceSale);
	                               products3=products3.replaceAll("[ ]","");
	                               product3=Double.parseDouble(products3);
	                               product3=UIFoundation.productPrice(product3);
	                               arrayList.add(product3);
	                        }
	                        
	                  }
	                  if(UIFoundation.isDisplayed(ListPage.btnFourthProductPrice))
	                  {
	                        String products4=UIFoundation.getText(ListPage.btnFourthProductPrice);
	                        products4=products4.replaceAll("[ ]","");
	                        product4=Double.parseDouble(products4);
	                        product4=UIFoundation.productPrice(product4);
	                        arrayList.add(product4);
	                        
	                  }else{
	                        if(UIFoundation.isDisplayed(ListPage.btnFourthProductPriceSale))
	                        {
	                               String products4=UIFoundation.getText(ListPage.btnFourthProductPriceSale);
	                               products4=products4.replaceAll("[ ]","");
	                               product4=Double.parseDouble(products4);
	                               product4=UIFoundation.productPrice(product4);
	                               arrayList.add(product4);
	                        }
	                        
	                  }
	                  if(UIFoundation.isDisplayed(ListPage.btnFifthProductPrice))
	                  {
	                        String products5=UIFoundation.getText(ListPage.btnFifthProductPrice);
	                        products5=products5.replaceAll("[ ]","");
	                        product5=Double.parseDouble(products5);
	                        product5=UIFoundation.productPrice(product5);
	                        arrayList.add(product5);
	                        
	                  }else{
	                        if(UIFoundation.isDisplayed(ListPage.btnFifthProductPriceSale))
	                        {
	                               String products5=UIFoundation.getText(ListPage.btnFifthProductPriceSale);
	                               products5=products5.replaceAll("[ ]","");
	                               product5=Double.parseDouble(products5);
	                               product5=UIFoundation.productPrice(product5);
	                               arrayList.add(product5);
	                        }
	                        
	                  }               
	                  
	                
	              
	                  System.out.println(arrayList);
	                     for(int i = 0; i < arrayList.size()-1; i++) {
	                         // current String is < than the next one (if there are equal list is still sorted)
	                         if(arrayList.get(i)>(arrayList.get(i + 1))) { 
	                            isSorted=false;
	                             break;
	                         }
	                        
	                      }
	                      if(isSorted)
	                      {
	                          objStatus+=true;
	                              String objDetail="Products are sorted from low to high price";
	                              ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	                         System.out.println("Products are sorted from low to high price");
	                               return true;
	                      }
	                      else
	                      {
	                         objStatus+=false;
	                               String objDetail="Products are not sorted from low to high price";
	                               UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
	           
	                         System.err.println("Products are not sorted from low to high price");
	                               return false;
	                      }
	                  
	           }catch(Exception e)
	           {
	                  return false;
	                  
	           }
	    
	    }
	    
	    
	    /***************************************************************************
	     * Method Name                    : validationForSortingPriceHtoL ()
	     * Created By                     : Chandrashekhar
	     * Reviewed By                    : Ramesh.
	     * Purpose                        : 
	      ****************************************************************************
	     */
	     public static boolean validationForSortingPriceHtoL()
	     {

	            List<Double> arrayList = new ArrayList<Double>();;
	            boolean isSorted=true;
	            Double product1=0.0;
	            Double product2=0.0;
	            Double product3=0.0;
	            Double product4=0.0;
	            Double product5=0.0;
	            String objStatus=null;
	            String screenshotName = "Scenarios_HtoL_Screenshot.jpeg";
	            String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
	 					+ screenshotName;
	            try
	            {
	                   
	                   
	            //     driver.navigate().refresh();
	                   if(UIFoundation.isDisplayed(ListPage.btnFirstProductPrice))
	                   {
	                         String products1=UIFoundation.getText(ListPage.btnFirstProductPrice);
	                         products1=products1.replaceAll("[,]","");
	                         product1=Double.parseDouble(products1);
	                         product1=UIFoundation.productPrice(product1);
	                         arrayList.add(product1);
	                         
	                   }else{
	                         if(UIFoundation.isDisplayed(ListPage.btnFrstProductPriceSale))
	                         {
	                                String products1=UIFoundation.getText(ListPage.btnFrstProductPriceSale);
	                                products1=products1.replaceAll("[,]","");
	                                product1=Double.parseDouble(products1);
	                                product1=UIFoundation.productPrice(product1);
	                                arrayList.add(product1);
	                         }
	                         
	                   }
	                   
	                   
	                   if(UIFoundation.isDisplayed(ListPage.btnSecondProductPrice))
	                   {
	                         String products2=UIFoundation.getText(ListPage.btnSecondProductPrice);
	                         products2=products2.replaceAll("[,]","");
	                         product2=Double.parseDouble(products2);
	                         product2=UIFoundation.productPrice(product2);
	                         arrayList.add(product2);
	                         
	                   }else{
	                         if(UIFoundation.isDisplayed(ListPage.btnSecondProductPriceSale))
	                         {
	                                String products2=UIFoundation.getText(ListPage.btnSecondProductPriceSale);
	                                products2=products2.replaceAll("[,]","");
	                                product2=Double.parseDouble(products2);
	                                product2=UIFoundation.productPrice(product2);
	                                arrayList.add(product2);
	                         }
	                         
	                   }
	                   if(UIFoundation.isDisplayed(ListPage.btnThirdProductPrice))
	                   {
	                         String products3=UIFoundation.getText(ListPage.btnThirdProductPrice);
	                         products3=products3.replaceAll("[,]","");
	                         product3=Double.parseDouble(products3);
	                         product3=UIFoundation.productPrice(product3);
	                         arrayList.add(product3);
	                         
	                   }else{
	                         if(UIFoundation.isDisplayed(ListPage.btnThirdProductPriceSale))
	                         {
	                                String products3=UIFoundation.getText(ListPage.btnThirdProductPriceSale);
	                                products3=products3.replaceAll("[,]","");
	                                product3=Double.parseDouble(products3);
	                                product3=UIFoundation.productPrice(product3);
	                                arrayList.add(product3);
	                         }
	                         
	                   }
	                   if(UIFoundation.isDisplayed(ListPage.btnFourthProductPrice))
	                   {
	                         String products4=UIFoundation.getText(ListPage.btnFourthProductPrice);
	                         products4=products4.replaceAll("[,]","");
	                         product4=Double.parseDouble(products4);
	                         product4=UIFoundation.productPrice(product4);
	                         arrayList.add(product4);
	                         
	                   }else{
	                         if(UIFoundation.isDisplayed(ListPage.btnFourthProductPriceSale))
	                         {
	                                String products4=UIFoundation.getText(ListPage.btnFourthProductPriceSale);
	                                products4=products4.replaceAll("[,]","");
	                                product4=Double.parseDouble(products4);
	                                product4=UIFoundation.productPrice(product4);
	                                arrayList.add(product4);
	                         }
	                         
	                   }
	                   if(UIFoundation.isDisplayed(ListPage.btnFifthProductPrice))
	                   {
	                         String products5=UIFoundation.getText(ListPage.btnFifthProductPrice);
	                         products5=products5.replaceAll("[,]","");
	                         product5=Double.parseDouble(products5);
	                         product5=UIFoundation.productPrice(product5);
	                         arrayList.add(product5);
	                         
	                   }else{
	                         if(UIFoundation.isDisplayed(ListPage.btnFifthProductPriceSale))
	                         {
	                                String products5=UIFoundation.getText(ListPage.btnFifthProductPriceSale);
	                                products5=products5.replaceAll("[,]","");
	                                product5=Double.parseDouble(products5);
	                                product5=UIFoundation.productPrice(product5);
	                                arrayList.add(product5);
	                         }
	                         
	                   }
	                  
	                   System.out.println(arrayList);
	                      for(int i = 0; i < arrayList.size()-1; i++) {
	                          // current String is < than the next one (if there are equal list is still sorted)
	                          if(arrayList.get(i)<(arrayList.get(i + 1))) { 
	                             isSorted=false;
	                              break;
	                          }
	                         
	                       }
	                       if(isSorted)
	                       {
	                           objStatus+=true;
	                               String objDetail="Products are sorted from High to Low price";
	                               ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	                          System.out.println("Products are sorted from High to Low price");
	                                return true;
	                       }
	                       else
	                       {
	                          objStatus+=false;
	                                String objDetail="Products are not sorted from High to Low price";
	                                UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
	                          System.err.println("Products are not sorted from High to Low price");
	                                return false;
	                       }
	                   
	            }catch(Exception e)
	            {
	                   return false;
	                   
	            }
	     
	     }
	
	
	/****************************************************************************
	 * Method Name			: removerProductPrice()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 *****************************************************************************//*


	public static boolean removerProductPrice(By  subTotalBefore,String subTotalAfter,String secondProductPrice, String thirdProductPrice)
	{

		String sub_Total_Before=null;
		String sub_Total_After=null;
		String second_Product_Price=null;
		String third_Product_Price=null;

		try {
			sub_Total_Before=subTotalBefore;
			sub_Total_After=subTotalAfter;
			second_Product_Price=secondProductPrice;
			third_Product_Price=thirdProductPrice;
			double subTotal_Before=Double.parseDouble(sub_Total_Before.substring(1));
			double subTotal_After=Double.parseDouble(sub_Total_After.substring(1));
			double secondProduct_Price=Double.parseDouble(second_Product_Price.substring(1));
			double thirdProduct_Price=Double.parseDouble(third_Product_Price.substring(1));
			double removedProductsPrice=secondProduct_Price+thirdProduct_Price;
			double finalSubtotal=subTotal_Before-removedProductsPrice;
			finalSubtotal=Math.round(finalSubtotal*100);
			finalSubtotal=finalSubtotal/100;
			if(finalSubtotal==subTotal_After)
			{
				System.out.println("Two products are removed from the cart");
				return true;

			}
			else
			{
				System.out.println("Unable to remove products from the cart");
				return false;
			}

       } catch (Exception e) {
       	return false;

       }
	}*/

	/*
	 *//***************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*
	public static boolean isObjectExistForList(WebDriver driver)
	{
		String strStatus=null;

		try
		{
			strStatus+=String.valueOf(UIFoundation.VerifyText(driver, "ShowOutOfStock","Show out of stock", "label"));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}

		}catch(Exception e)
		{
			return false;
		}
	}


	  *//***************************************************************************
	  * Method Name			: isElementExistForList()
	  * Created By			: Vishwanath Chavan
	  * Reviewed By			: Ramesh,KB
	  * Purpose				: 
	  ****************************************************************************
	  *//*

	public static boolean isElementExist(WebDriver driver)
	{
		String strStatus=null;

		try
		{
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address1'])[3]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address2'])[3]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='city'])[3]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("//select[@name='stateOrProvince']"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='zipOrPostalCode'])[1]"), "element", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='phone'])[3]"), "element", "",5));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}

	   *//***************************************************************************
	   * Method Name			: isElementNotExist()
	   * Created By			: Vishwanath Chavan
	   * Reviewed By			: Ramesh,KB
	   * Purpose				: 
	   ****************************************************************************
	   *//*

	public static boolean isElementNotExist(WebDriver driver)
	{
		String strStatus=null;

		try
		{
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address1'])[3]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='address2'])[3]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='city'])[3]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("//select[@name='stateOrProvince']"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='zipOrPostalCode'])[1]"), "Invisible", "",5));
			strStatus+=String.valueOf(UIFoundation.waitFor(driver, By.xpath("(//input[@name='phone'])[3]"), "Invisible", "",5));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}

	    *//***************************************************************************
	    * Method Name			: isElementExistForList()
	    * Created By			: Vishwanath Chavan
	    * Reviewed By			: Ramesh,KB
	    * Purpose				: 
	    ****************************************************************************
	    */

	public static void deleteFile(WebDriver driver)
	{


		try {
			String path=System.getProperty("user.dir")+"\\src\\test\\resources\\Results\\DetailedReport.html";
			File file = new File(path);
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */

	public static String validationForSaveForLater()
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		ArrayList<String> missingProductName=new ArrayList<String>();
		ArrayList<String> allProductName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		String screenshotName = "Scenarios_SaveForLater_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			String totalPriceBeforeSaveForLater=UIFoundation.getText(ListPage.txtTotalBeforeTax);
			String totalItemsBeforeSaveForLater=UIFoundation.getText(ListPage.btnCartCount);
			System.out.println("Number of Products Added to the CART :"+totalItemsBeforeSaveForLater);
			System.out.println("Number of Products in Saved For Later Section :"+UIFoundation.getText(ListPage.txtSaveForLaterCount));
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{

				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}

			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}

			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}

			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}

			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			/*String saveForLaterCnt=UIFoundation.getText(AddProductToCartPage.txtSaveForLaterCount);
			int saveForLaterCount=Integer.parseInt(saveForLaterCnt.replaceAll("\\(|\\)", ""));*/
			boolean saveForLaterTitle=UIFoundation.isDisplayed(ListPage.txtSaveForLaterHeadline);
			boolean noItemsInSaveForLater=UIFoundation.isDisplayed(ListPage.txtNoItemsInSaveForLater);
			boolean saveForLaterAfterCheckout=UIFoundation.isDisplayed(ListPage.txtSaveForLaterAfterCheckout);
			/*if(saveForLaterTitle==true && noItemsInSaveForLater==true && saveForLaterAfterCheckout==true)
			{
				System.out.println("==========Verify save for later section, if the list empty===============");
				System.out.println("Title 'Saved for Later (0)' is displayed");
				System.out.println("'You have no items saved' text is available");
				System.out.println("Save for later section is available below the Checkout button");

				strStatus+=true;
			      String objDetail="Verified  save for later section, if the list empty ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified save for later section, if the list empty");

			}else{
					strStatus+=false;
			       String objDetail="Verify save for later section, if the list empty is failed";
			  //     UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				   System.out.println("Verify save for later section, if the list empty is failed");
			}*/
			System.out.println("=======================================================================");
			System.out.println("=======================CART Order Summary Before Moving to Saved for Later =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(ListPage.txtSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(ListPage.txtobjShippingHnadling));
			System.out.println("Total price of all items before moving :"+totalPriceBeforeSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Name moving from CART to �Saved For Later�:"+expectedItemsName);
			if(!UIFoundation.getText(ListPage.btnThirdProductSaveFor).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductSaveFor));

			}
			if(!UIFoundation.getText(ListPage.btnSecondProductSaveFor).contains("Fail"))
			{
				String saveForLaterLink=String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductSaveFor));
				strStatus+=saveForLaterLink;
				boolean saveForLaterLnk =String.valueOf(saveForLaterLink) != null;
				if(saveForLaterLnk){
					strStatus+=true;
					String objDetail="Verified the functionality of 'Save for later' link in cart section";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Verified the functionality of 'Save for later' link in cart section");
				}else{
					strStatus+=false;
					String objDetail="Verify the functionality of 'Save for later' link in cart section is failed";
					//     UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
					System.out.println("Verify the functionality of 'Save for later' link in cart section is failed");
				}

			}
			UIFoundation.waitFor(2L);
			String totalItemsAfterSaveForLater=UIFoundation.getText(ListPage.btnCartCount);
			String totalPriceAfterSaveForLater=UIFoundation.getText(ListPage.txtTotalBeforeTax);
			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInSaveFor).contains("Fail"))
			{
				String actualProducts1=UIFoundation.getText(ListPage.btnFirstProductInSaveFor);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}
			if(!UIFoundation.getText(ListPage.btnSecondProdcutInSaveFor).contains("Fail"))
			{
				String actualProducts2=UIFoundation.getText(ListPage.btnSecondProdcutInSaveFor);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}
			/*String actualProducts1=UIFoundation.getText(AddProductToCartPage.btnFirstProductInSaveFor);
			String actualProducts2=UIFoundation.getText(AddProductToCartPage.btnSecondProdcutInSaveFor);
			actualItemsName.add(actualProducts1);
			actualItemsName.add(actualProducts2);*/
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			//	System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			//	System.out.println("1) "+actualProducts1);
			//	System.out.println("2) "+actualProducts2);
			System.out.println("=======================================================================");
			System.out.println("Product Names in �Saved for Later�: "+actualItemsName);
			System.out.println("=======================Order Summary After Moving =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(ListPage.txtSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(ListPage.txtobjShippingHnadling));
			System.out.println("Total price of all items after moving :"+totalPriceAfterSaveForLater);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(ListPage.txtSaveForLaterCount));
			System.out.println("=======================================================================");
			if(UIFoundation.isDisplayed(ListPage.txtSaveForLaterCount))
			{
				strStatus+=true;
				String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(ListPage.txtSaveForLaterCount);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(ListPage.txtSaveForLaterCount));
			}else{
				strStatus+=false;
				String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(ListPage.txtSaveForLaterCount);
				System.out.println("Verify save for later section, if the list non empty is failed");
			}
			
			for(int i=0;i<expectedItemsName.size();i++)
			{

				if(actualItemsName.contains(expectedItemsName.get(i)))
				{
					allProductName.add(expectedItemsName.get(i));
				}
				else
				{
					missingProductName.add(expectedItemsName.get(i));
				}
			}
			if(strStatus.contains("false"))
			{
				System.out.println("Product Name "+missingProductName+"is not matched or missing.");
				return "Fail";

			}
			else
			{
				System.out.println("Product Names are matched before moving and after moving to the �Saved For Later�");
				return "Pass";
			}

		} catch (Exception e) {

			e.printStackTrace();
			return "Fail";
		}
	}




	/****************************************************************************
	 * Method Name			: validationForRemoveProductsSaveForLater()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */

	public static String validationForRemoveProductsSaveForLater(WebDriver driver)
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		ArrayList<String> missingProductName=new ArrayList<String>();
		ArrayList<String> allProductName=new ArrayList<String>();
		String actualProducts1=null;
		String actualProducts2=null;
		String actualProducts3=null;
		String actualProducts4=null;
		String actualProducts5=null;
		boolean isSorted = true;
		try {
			if(!UIFoundation.getText(CartPage.spnFifthProductSaveFor).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFifthProductSaveFor));
				UIFoundation.waitFor(2L);
			}

			if(!UIFoundation.getText(CartPage.spnFourthProductSaveFor).contains("Fail"))
			{

				strStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFourthProductSaveFor));
				UIFoundation.waitFor(2L);
			}

			/*if(!UIFoundation.getText(CartPage.spnThirdProductSaveForr).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnThirdProductSaveForr));
				UIFoundation.waitFor(2L);
			}*/

			if(!UIFoundation.getText(CartPage.spnSecondProductSaveFor).contains("Fail"))
			{

				strStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnSecondProductSaveFor));
				UIFoundation.waitFor(2L);
			}

			if(!UIFoundation.getText(CartPage.spnFirstProductSaveFor).contains("Fail"))
			{
				UIFoundation.waitFor(3L);
				strStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFirstProductSaveFor));
			}

			//driver.navigate().refresh();

			UIFoundation.waitFor(2L);

			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(CartPage.spnFirstProductInSaveFor).contains("Fail"))
			{
				actualProducts1=UIFoundation.getText(CartPage.spnFirstProductInSaveFor);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}

			if(!UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor).contains("Fail"))
			{
				actualProducts2=UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}

			if(!UIFoundation.getText(CartPage.spnThirdProductInSaveFor).contains("Fail"))
			{
				actualProducts3=UIFoundation.getText(CartPage.spnThirdProductInSaveFor);
				System.out.println("3) "+actualProducts3);
				actualItemsName.add(actualProducts3);
			}

			if(!UIFoundation.getText(CartPage.spnFourthProdcutInSaveFor).contains("Fail"))
			{
				actualProducts4=UIFoundation.getText(CartPage.spnFourthProdcutInSaveFor);
				System.out.println("4) "+actualProducts4);
				actualItemsName.add(actualProducts4);
			}

			if(!UIFoundation.getText(CartPage.spnFifthProductInSaveFor).contains("Fail"))
			{
				actualProducts5=UIFoundation.getText(CartPage.spnFifthProductInSaveFor);
				System.out.println("5) "+actualProducts5);
				actualItemsName.add(actualProducts5);
			}
			for(int i = 0; i < actualItemsName.size()-1; i++) {
				// current String is < than the next one (if there are equal list is still sorted)
				if((actualItemsName.get(i).compareToIgnoreCase(actualItemsName.get(i + 1)) > 0) && (actualItemsName.get(i).compareToIgnoreCase(expectedItemsName.get(i + 1))<0)) { 
					isSorted=false;
					break;
				}

			}
			if(isSorted)
			{
				System.out.println("Verify products are listed in Alphabatical order in save for later section");

			}
			else
			{
				System.err.println("Prooducts are not listed in Alphabatical order in save for later section");

			}
			System.out.println("=======================================================================");
			if(strStatus.contains("false"))
			{

				return "Fail";

			}
			else
			{

				return "Pass";
			}

		} catch (Exception e) {

			e.printStackTrace();
			return "Fail";
		}
	}


	/***************************************************************************
	 * Method Name			: productPrice()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static double productPrice(double d)
	{
		String strStatus=null;
		int cartConut=0;
		double prodPrice=0;

		try
		{
			prodPrice=d;
			int x = 2; // Or 2, or whatever
			BigDecimal unscaled = new BigDecimal(prodPrice);
			BigDecimal scaled = unscaled.scaleByPowerOfTen(-x);
			prodPrice=scaled.doubleValue();
			return prodPrice;


		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cartConut;
	}


	/*	**************************************************************************
	 * Method Name			: userProfileCreation()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to Create new user account
	 *****************************************************************************/


	public static String userProfileCreation() {
		String objStatus=null;
		try {
			log.info("The execution of method create Account started here");
			
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
			if(UIFoundation.isDisplayed(LoginPage.MainNavSignIn))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.MainNavSignIn));
			}
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.JoinNowButton));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.Password, "accPassword"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account "
					+ e);
			return "Fail";
		}
	}


	/* ********************************************************************************
	 * Method Name			: validationForOnlyPreSaleProducts()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 * *****************************************************************************
	 */


	public static boolean validationForOnlyPreSaleProducts(WebDriver driver)
	{
		String objStatus=null;
		String preSaleProducts1 = null;
		String preSaleProducts2 = null;
		String preSaleProducts3 = null;

		try
		{
			log.info("The execution of the method validationForOnlyPreSaleProducts started here ...");


			if (UIFoundation.isDisplayed(CartPage.txtPreSaleProductText)) {
				System.out.println("Pre-sale text is Displayed for first product");
				return true;
			} else {
				System.out.println("Product is not pre-sale product");
				return false;
			}


		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method validationForOnlyPreSaleProducts "+ e);

			return false;
		}

	}


	/****************************************************************************
	 * Method Name			: isObjectExistForSignIn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 *****************************************************************************/

	public static int cartCount()
	{
		String strStatus=null;
		int cartConut=0;

		try
		{
			String num=UIFoundation.getText(CartPage.txtCartHeadLineCount);
			cartConut = Integer.parseInt(num.replaceAll("[^0-9?!\\.]",""));
			return cartConut;


		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cartConut;
	}


	/**************************************************************************
	 * Method Name			: recommendedProductsAddToCartBtn()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 *************************************************************************/

	public static boolean recommendedProductsAddToCartBtn()
	{
		String strStatus=null;

		try
		{
			strStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnRecommendedProductsFirstAddToCart));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnRecommendedProductsSecondAddToCart));
			strStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnRecommendedProductsThirdAddToCart));
			if(strStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}

	/* ====================================================================
	 * Method Name			: validationForGiftSortingAtoZ()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 *****************************************************************************/

	public static String validationForResponseHeader(String prodName)
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		String screenshotName = "Scenarios_listViewPr_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try
		{
			String products1=UIFoundation.getText(ListPage.btnlistPageFirstProdName);
			String products2=UIFoundation.getText(ListPage.btnlistPageSecProdName);
			String products3=UIFoundation.getText(ListPage.btnlistPageThirdProdName);
			String products4=UIFoundation.getText(ListPage.btnlistPageFourthProdName);
			String products5=UIFoundation.getText(ListPage.btnlistPageFifthProdName);
			arrayList.add(products1);
			arrayList.add(products2);
			arrayList.add(products3);
			arrayList.add(products4);
			arrayList.add(products5);
			for(int i = 0; i < arrayList.size()-1; i++) {
				// current String is < than the next one (if there are equal list is still sorted)
				if((arrayList.get(i).contains(prodName))) { 
					objStatus+=true;
				}else{
					objStatus+=false;
				}

			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verified the content of the list test case if failed";
				System.out.println("Verified the content of the list test case if failed");							
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"fet", objDetail);
				return objStatus;
			}
			else
			{
				System.out.println("Verified the content of the list and data is dispalyed");
				return objStatus;
			}

		}catch(Exception e)
		{
			return objStatus;

		}

	}


	/***************************************************************************
	 * Method Name                : recipientEditOrderHIS()
	 * Created By                 : Chandrashekhar
	 * Reviewed By                : Ramesh.
	 * Purpose                    : 
	 ****************************************************************************
	 */
	public static boolean recipientEditOrderHIS()
	{
		String objStatus=null;
		try
		{
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEidt))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEidt));
				UIFoundation.waitFor(3L);	
			}				
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkshippingAddressEdit));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnshipState, "State"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnshipState));

			UIFoundation.clickObject(FinalReviewPage.txtZipCode);
			UIFoundation.clearField(FinalReviewPage.txtZipCode);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCodeHis"));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnshippingAddressSave));
			UIFoundation.waitFor(15L);


			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueShipRecpt)){
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueShipRecpt));
				UIFoundation.waitFor(10L);
			}		

			if(UIFoundation.isDisplayed(FinalReviewPage.btnShipContinue)){
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
				UIFoundation.waitFor(10L);
			}else {
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinue));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.waitFor(10L);
			}			

			if(objStatus.contains("false"))
			{
				return false;
			}
			else
			{
				return true;
			}
		}catch(Exception e)
		{
			return false;
		}
	}

	
	  /***************************************************************************
  	 * Method Name			: login()
  	 * Created By			: Chandrashekhar
  	 * Reviewed By			: Ramesh.
  	 * Purpose				: The purpose of this method is Login into the Wine.com
  	 * 						  Application
  	 ****************************************************************************
  	 */
  	
  	public static String loginOtherState(String State)
  	{
  		String objStatus=null;
  		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
 		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
 					+ screenshotName;
  		try
  		{
  			
  			log.info("The execution of the method login started here ...");
  			UIFoundation.waitFor(8L);
  			UIFoundation.SelectObject(LoginPage.dwnSelectState, State);
  			UIFoundation.waitFor(5L);

  					objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
  				    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
  					UIFoundation.waitFor(1L); 
  					objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
  					objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
  					UIFoundation.waitFor(1L);
  					objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
  					UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
  					String actualtile=driver.getTitle();  					
  					String expectedTile = verifyexpectedresult.shoppingCartPageTitle;	
  					if(actualtile.equalsIgnoreCase(expectedTile))
  					{
  						objStatus+=true;
  					}else
  					{
  						objStatus+=false;
  				    	String objDetail="Failed to login";
  				    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
  					}
  					System.out.println("login :"+objStatus);
  			log.info("The execution of the method login ended here ...");
  			if (objStatus.contains("false"))
  			{
  				System.out.println("Login test case is failed");
  				return "Fail";
  			}
  			else
  			{
  				System.out.println("Login test case is executed successfully");
  				return "Pass";
  			}
  			
  		}catch(Exception e)
  		{
  			
  			log.error("there is an exception arised during the execution of the method login "+ e);
  			return "Fail";
  			
  		}
  	}
  	
  	/***************************************************************************
	 * Method Name			: loginPrefferedAddress()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String loginPrefferedAddress()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(3L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "preferenceAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
  
	
	/***************************************************************************
	 * Method Name			: discountCalculator()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static void discountCalculator(String subTotal,String promoCode)
	{
		
		String sub_Total=null;
		String prome_Code=null;
		try {
			sub_Total=subTotal;
			sub_Total = sub_Total.replaceAll("[$,]","");
			prome_Code=promoCode;
			double sub_total=Double.parseDouble(sub_Total);
			double promo_code=Double.parseDouble(prome_Code.substring(2,6));
			promo_code=(promo_code*10/100);
			promo_code=Math.round(promo_code*100);
			promo_code=promo_code/100;
			double discountPrice=(sub_total*10/100);
			discountPrice=Math.round(discountPrice*100);
			discountPrice=discountPrice/100;
			if(promo_code==discountPrice)
			{
				System.out.println("Thanx for using promocde,10% of amount is discounted from SubTotal");
			
				
			}
			else
			{
				System.out.println("Sorry unable to discount 10% of amount from SubTotal");
				
			}
		
        } catch (Exception e) {
        	e.printStackTrace();
            
        }
	}
	
	/****************************************************************************
	 * Method Name			: isCheckboxSelected()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 *****************************************************************************//*

	public static void isGiftCheckboxSelected(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		WebElement emailField=null;
		String emailValue=null;
		String objStatus=null;
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
			if(!UIFoundation.isDisplayed(driver, "Obj_RecipientEmail"))
			{
				ele.click();
				UIFoundation.waitFor(2L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(emailValue.isEmpty())
				{
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
				}
				else
				{
					emailField.clear();
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
					recEmail=UIFoundation.getAttribute(driver, "obj_RecipientEmail");
				}


			}
			else
			{
				UIFoundation.waitFor(1L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(emailValue.isEmpty())
				{
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
				}
				else
				{
					emailField.clear();
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
					recEmail=UIFoundation.getAttribute(driver, "obj_RecipientEmail");
				}
			}

		}catch(Exception e)
		{
			e.printStackTrace();

		}

	}*/
	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingZtoA()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean validationForGiftSortingZtoA()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		   boolean isSorted = true;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.btnFrstProductName);
			String products2=UIFoundation.getText(ListPage.btnSecProductName);
			String products3=UIFoundation.getText(ListPage.btnThrdProductName);
			String products4=UIFoundation.getText(ListPage.btnFrthProductName);
			String products5=UIFoundation.getText(ListPage.btnFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted in the order of Z-A");
					return true;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted in the order of Z-A");
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingAtoZ()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForGiftSortingAtoZ()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
		   String screenshotName = "Scenarios_validationForGiftSortingAtoZ_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;

		  // String objStatus=null;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(ListPage.btnFrstProductName);
			String products2=UIFoundation.getText(ListPage.btnSecProductName);
			String products3=UIFoundation.getText(ListPage.btnThrdProductName);
			String products4=UIFoundation.getText(ListPage.btnFrthProductName);
			String products5=UIFoundation.getText(ListPage.btnFfthProductName);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			arrayListYr.add(products1.replaceAll("[^0-9]", ""));
			arrayListYr.add(products2.replaceAll("[^0-9]", ""));
			arrayListYr.add(products3.replaceAll("[^0-9]", ""));
			arrayListYr.add(products4.replaceAll("[^0-9]", ""));
			arrayListYr.add(products5.replaceAll("[^0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted in the order of A-Z");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted in the order of A-Z");
			    	objStatus+=false;
			    	String objDetail="Products are not sorted from High to Low price";
			    	  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);			   
					return objStatus;
			    }
	
			
		}catch(Exception e)
		{
			System.err.println("Gift Certificates are not sorted in the order of A-Z");
	    	objStatus+=false;
	    	String objDetail="Products are not sorted from High to Low price";
	    	  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
	   		return objStatus;
			
		}
	
	}
	

	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingPriceLtoH()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForGiftSortingPriceLtoH()
	{
		WebElement ele=null;
		String objStatus=null;
		 List<Double> arrayList = new ArrayList<Double>();;
		 Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
		boolean isSorted=true;
		   String screenshotName = "Scenarios_validationForGiftSortingPriceLtoH_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			driver.navigate().refresh();
			if(!UIFoundation.getText(ListPage.GiftSPriceFirstProduct).contains("Fail"))
			{	String products1=UIFoundation.getText(ListPage.GiftSPriceFirstProduct);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlow.productPrice(product1);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceSecondProduct).contains("Fail"))
			{
				String products2=UIFoundation.getText(ListPage.GiftSPriceSecondProduct);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlow.productPrice(product2);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceThirdProduct).contains("Fail"))
			{
				String products3=UIFoundation.getText(ListPage.GiftSPriceThirdProduct);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlow.productPrice(product3);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceFourthProduct).contains("Fail"))
			{
				String products4=UIFoundation.getText(ListPage.GiftSPriceFourthProduct);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlow.productPrice(product4);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceFifthProduct).contains("Fail"))
			{
				String products5=UIFoundation.getText(ListPage.GiftSPriceFifthProduct);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlow.productPrice(product5);
			}
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)>(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted from low to high price");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted from low to high price");
			    	objStatus+=false;
			    	String objDetail="Products are not sorted from High to Low price";
			    	  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
					return objStatus;
			    }
			
			
		}catch(Exception e)
		{
			System.err.println("Gift Certificates are not sorted from low to high price");
	    	objStatus+=false;
	    	String objDetail="Products are not sorted from High to Low price";
	    	  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;
			
		}
	
	}
	
	/***************************************************************************
	 * Method Name			: validationForGiftSortingPriceHtoL()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String validationForGiftSortingPriceHtoL()
	{

		 List<Double> arrayList = new ArrayList<Double>();;
		boolean isSorted=true;
		 Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
			String objStatus=null;
			   String screenshotName = "Scenarios_validationForGiftSortingPriceHtoL_Screenshot.jpeg";
			   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
						+ screenshotName;
		try
		{
			driver.navigate().refresh();
			if(!UIFoundation.getText(ListPage.GiftSPriceFirstProduct).contains("Fail"))
			{	String products1=UIFoundation.getText(ListPage.GiftSPriceFirstProduct);
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=UIBusinessFlow.productPrice(product1);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceSecondProduct).contains("Fail"))
			{
				String products2=UIFoundation.getText(ListPage.GiftSPriceSecondProduct);
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=UIBusinessFlow.productPrice(product2);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceThirdProduct).contains("Fail"))
			{
				String products3=UIFoundation.getText(ListPage.GiftSPriceThirdProduct);
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=UIBusinessFlow.productPrice(product3);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceFourthProduct).contains("Fail"))
			{
				String products4=UIFoundation.getText(ListPage.GiftSPriceFourthProduct);
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=UIBusinessFlow.productPrice(product4);
			}
			if(!UIFoundation.getText(ListPage.GiftSPriceFifthProduct).contains("Fail"))
			{
				String products5=UIFoundation.getText(ListPage.GiftSPriceFifthProduct);
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=UIBusinessFlow.productPrice(product5);
			}
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)<(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted from High to Low price");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted from High to Low price");
			    	objStatus+=false;
			    	String objDetail="Products are not sorted from High to Low price";
			    	  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);			    
					return objStatus;
			    }

			
		}catch(Exception e)
		{
			System.err.println("Gift Certificates are not sorted from High to Low price");
	    	objStatus+=false;
	    	String objDetail="Products are not sorted from High to Low price";
	    	  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);	 
			return objStatus;			
		}	
	}
	
	/***************************************************************************
	 * Method Name : getText()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose :
	 ****************************************************************************
	 */
	public static List<WebElement> webelements(By strObjectName) {
		List<WebElement> oEle = null;
		String txt = null;
		try {
			oEle = driver.findElements(strObjectName);
			/*for(int i=0; i<=oEle.size();i++)
			{
				//txt=oEle.get(i).toString();
				txt=oEle.get(i).getText();
			}*/
			return  oEle;
		} catch (Exception e) {
			return  oEle;
		}
	}
	
	
	

	/***************************************************************************
	 * Method Name			: discountCalculator()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */


	/*	public static void discountCalculator(By spnSubtotal,String promoCode)
	{

		String sub_Total=null;
		String prome_Code=null;
		try {
			sub_Total=spnSubtotal;
			sub_Total = sub_Total.replaceAll("[$,]","");
			prome_Code=promoCode;
			double sub_total=Double.parseDouble(sub_Total);
			double promo_code=Double.parseDouble(prome_Code.substring(2,6));
			promo_code=(promo_code*10/100);
			promo_code=Math.round(promo_code*100);
			promo_code=promo_code/100;
			double discountPrice=(sub_total*10/100);
			discountPrice=Math.round(discountPrice*100);
			discountPrice=discountPrice/100;
			if(promo_code==discountPrice)
			{
				System.out.println("Thanx for using promocde,10% of amount is discounted from SubTotal");


			}
			else
			{
				System.out.println("Sorry unable to discount 10% of amount from SubTotal");

			}

       } catch (Exception e) {
       	e.printStackTrace();

       }
	}
	 */
	/*


	 *//***************************************************************************
	 * Method Name			: discountCalculator()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 *//*

	public static void discountCalculator(WebDriver driver,String subTotal,String promoCode)
	{

		String sub_Total=null;
		String prome_Code=null;
		try {
			sub_Total=subTotal;
			sub_Total = sub_Total.replaceAll("[$,]","");
			prome_Code=promoCode;
			double sub_total=Double.parseDouble(sub_Total);
			double promo_code=Double.parseDouble(prome_Code.substring(2,6));
			promo_code=(promo_code*10/100);
			promo_code=Math.round(promo_code*100);
			promo_code=promo_code/100;
			double discountPrice=(sub_total*10/100);
			discountPrice=Math.round(discountPrice*100);
			discountPrice=discountPrice/100;
			if(promo_code==discountPrice)
			{
				System.out.println("Thanx for using promocde,10% of amount is discounted from SubTotal");


			}
			else
			{
				System.out.println("Sorry unable to discount 10% of amount from SubTotal");

			}

        } catch (Exception e) {
        	e.printStackTrace();

        }
	}

	  *//***************************************************************************
	  * Method Name			: removerProductPrice()
	  * Created By			: Vishwanath Chavan
	  * Reviewed By			: Ramesh,KB
	  * Purpose				: 
	  ****************************************************************************
	  *//*

	public static boolean removerProductPrice(WebDriver driver,String subTotalBefore,String subTotalAfter,String secondProductPrice, String thirdProductPrice)
	{

		String sub_Total_Before=null;
		String sub_Total_After=null;
		String second_Product_Price=null;
		String third_Product_Price=null;

		try {
			sub_Total_Before=subTotalBefore;
			sub_Total_After=subTotalAfter;
			second_Product_Price=secondProductPrice;
			third_Product_Price=thirdProductPrice;
			double subTotal_Before=Double.parseDouble(sub_Total_Before.substring(1));
			double subTotal_After=Double.parseDouble(sub_Total_After.substring(1));
			double secondProduct_Price=Double.parseDouble(second_Product_Price.substring(1));
			double thirdProduct_Price=Double.parseDouble(third_Product_Price.substring(1));
			double removedProductsPrice=secondProduct_Price+thirdProduct_Price;
			double finalSubtotal=subTotal_Before-removedProductsPrice;
			finalSubtotal=Math.round(finalSubtotal*100);
			finalSubtotal=finalSubtotal/100;
			if(finalSubtotal==subTotal_After)
			{
				System.out.println("Two products are removed from the cart");
				return true;

			}
			else
			{
				System.out.println("Unable to remove products from the cart");
				return false;
			}

        } catch (Exception e) {
        	return false;

        }
	}
	   *//***************************************************************************
	   * Method Name			: isCheckboxSelected()
	   * Created By			: Vishwanath Chavan
	   * Reviewed By			: Ramesh,KB
	   * Purpose				: 
	   ****************************************************************************
	   *//*
	public static boolean isCheckboxSelected(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		String objStatus=null;
		//WebElement emailField=null;
		//String emailValue=null;
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
			if(!UIFoundation.isDisplayed(driver, "Obj_RecipientEmail"))
			{
				ele.click();
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
				recEmail=UIFoundation.getAttribute(driver, "obj_RecipientEmail");
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "RecipientGiftMessage", "RecipientMessage"));
				recGiftMsg=UIFoundation.getText(driver, "RecipientGiftMessage");
				return true;
			}
			else
			{
				UIFoundation.waitFor(1L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(!emailValue.isEmpty())
				{
					driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']")).clear();
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
				}
				UIFoundation.waitFor(1L);
				recEmail=UIFoundation.getAttribute(driver, "Obj_RecipientEmail");
				objStatus+=String.valueOf(UIFoundation.setObject(driver, "RecipientGiftMessage", "RecipientMessage"));
				recGiftMsg=UIFoundation.getAttribute(driver, "RecipientGiftMessage");
				return true;
			}

		}catch(Exception e)
		{
			return false;

		}

	}

	    *//***************************************************************************
	    * Method Name			: isCheckboxSelected()
	    * Created By			: Vishwanath Chavan
	    * Reviewed By			: Ramesh,KB
	    * Purpose				: 
	    ****************************************************************************
	    *//*
	public static void isCheckboxSelectedAWU(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		WebElement emailField=null;
		String emailValue=null;
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
		//	WebElement emailFld=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
			if(!UIFoundation.isDisplayed(driver, "Obj_RecipientEmail"))
			{
				ele.click();
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(!emailValue.isEmpty())
				{
					emailField.clear();
				}
				UIFoundation.waitFor(2L);

			}

		}catch(Exception e)
		{
			e.printStackTrace();

		}

	}



	     *//***************************************************************************
	     * Method Name			: isCheckboxSelected()
	     * Created By			: Vishwanath Chavan
	     * Reviewed By			: Ramesh,KB
	     * Purpose				: 
	     ****************************************************************************
	     *//*
	public static void isGiftCheckboxSelectd(WebDriver driver,String strObjectName)
	{
		WebElement ele=null;
		WebElement emailField=null;
		String emailValue=null;
		String objStatus=null;
		try
		{
			ele=driver.findElement(object.getLocator(strObjectName));
			if(!UIFoundation.isDisplayed(driver, "Obj_RecipientEmail"))
			{
				ele.click();
				UIFoundation.waitFor(2L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(emailValue.isEmpty())
				{
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
				}
				else
				{
					emailField.clear();
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
					recEmail=UIFoundation.getAttribute(driver, "obj_RecipientEmail");
				}


			}
			else
			{
				UIFoundation.waitFor(1L);
				emailField=driver.findElement(By.xpath("//input[@class='formWrap_input checkoutForm_input giftEmailInput']"));
				emailValue=emailField.getAttribute("value");
				if(emailValue.isEmpty())
				{
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
				}
				else
				{
					emailField.clear();
					objStatus+=String.valueOf(UIFoundation.setObject(driver, "Obj_RecipientEmail", "RecipientEmail"));
					recEmail=UIFoundation.getAttribute(driver, "obj_RecipientEmail");
				}
			}

		}catch(Exception e)
		{
			e.printStackTrace();

		}

	}
	      *//***************************************************************************
	      * Method Name			: validationForSortZtoA()
	      * Created By			: Vishwanath Chavan
	      * Reviewed By			: Ramesh,KB
	      * Purpose				: 
	      ****************************************************************************
	      *//*
	public static String validationForSortingZtoA(WebDriver driver)
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		   boolean isSorted = true;
		   String screenshotName = "Scenarios_validationForSortingZtoA_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(driver, "FrstProductName");
			String products2=UIFoundation.getText(driver, "SecProductName");
			String products3=UIFoundation.getText(driver, "ThrdProductName");
			String products4=UIFoundation.getText(driver, "FrthProductName");
			String products5=UIFoundation.getText(driver, "FfthProductName");
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }

			    }
			    if(isSorted)
			    {
			    	System.out.println("Products names are sorted in the order of Z-A");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Products names are not sorted in the order of Z-A");
			    	objStatus+=false;
			    	String objDetail="Products names are not sorted in the order of Z-A";
			    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
					return objStatus;
			    }

		}catch(Exception e)
		{
			System.err.println("Products names are not sorted in the order of Z-A");
	    	objStatus+=false;
	    	String objDetail="Products names are not sorted in the order of Z-A";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;
		}

	}

	       *//***************************************************************************
	       * Method Name			: validationForSortAtoZ()
	       * Created By			: Vishwanath Chavan
	       * Reviewed By			: Ramesh,KB
	       * Purpose				: 
	       ****************************************************************************
	       *//*
	public static String validationForSortingAtoZ(WebDriver driver)
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
		   String screenshotName = "Scenarios_validationForSortingAtoZ_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;

		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(driver, "FrstProductName");
			String products2=UIFoundation.getText(driver, "SecProductName");
			String products3=UIFoundation.getText(driver, "ThrdProductName");
			String products4=UIFoundation.getText(driver, "FrthProductName");
			String products5=UIFoundation.getText(driver, "FfthProductName");
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			arrayListYr.add(products1.replaceAll("[^0-9]", ""));
			arrayListYr.add(products2.replaceAll("[^0-9]", ""));
			arrayListYr.add(products3.replaceAll("[^0-9]", ""));
			arrayListYr.add(products4.replaceAll("[^0-9]", ""));
			arrayListYr.add(products5.replaceAll("[^0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }

			    }
			    if(isSorted)
			    {
			    	System.out.println("Products names are sorted in the order of A-Z");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Products names are not sorted in the order of A-Z");
			    	objStatus+=false;
			    	String objDetail="Products names are not sorted in the order of A-Z";
			    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
					return objStatus;
			    }
		}catch(Exception e)
		{
			System.err.println("Products names are not sorted in the order of A-Z");
	    	objStatus+=false;
	    	String objDetail="Products names are not sorted in the order of A-Z";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;

		}

	}



	        *//***************************************************************************
	        * Method Name                    : validationForSortingPriceLtoH ()
	        * Created By              : Vishwanath Chavan
	        * Reviewed By                    : Ramesh,KB
	        * Purpose                        : 
	        ****************************************************************************
	        *//*
    public static boolean validationForSortingPriceLtoH(WebDriver driver)
    {
           WebElement ele=null;
           String objStatus=null;
           List<Double> arrayList = new ArrayList<Double>();;
                  Double product1=0.0;
                  Double product2=0.0;
                  Double product3=0.0;
                  Double product4=0.0;
                  Double product5=0.0;
           boolean isSorted=true;
           String screenshotName = "Scenarios_LtoH_Screenshot.jpeg";
           String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
           try
           {
           //     driver.navigate().refresh();
                  if(UIFoundation.isDisplayed(driver, "FrstProductPrice"))
                  {
                        String products1=UIFoundation.getText(driver, "FrstProductPrice");
                        products1=products1.replaceAll("[ ]","");
                        product1=Double.parseDouble(products1);
                        product1=ApplicationDependent.productPrice(driver, product1);
                        arrayList.add(product1);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "FrstProductPriceSale"))
                        {
                               String products1=UIFoundation.getText(driver, "FrstProductPriceSale");
                               products1=products1.replaceAll("[ ]","");
                               product1=Double.parseDouble(products1);
                               product1=ApplicationDependent.productPrice(driver, product1);
                               arrayList.add(product1);
                        }

                  }


                  if(UIFoundation.isDisplayed(driver, "SecndProductPrice"))
                  {
                        String products2=UIFoundation.getText(driver, "SecndProductPrice");
                        products2=products2.replaceAll("[ ]","");
                        product2=Double.parseDouble(products2);
                        product2=ApplicationDependent.productPrice(driver, product2);
                        arrayList.add(product2);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "SecondProductPriceSale"))
                        {
                               String products2=UIFoundation.getText(driver, "SecondProductPriceSale");
                               products2=products2.replaceAll("[ ]","");
                               product2=Double.parseDouble(products2);
                               product2=ApplicationDependent.productPrice(driver, product2);
                               arrayList.add(product2);
                        }

                  }
                  if(UIFoundation.isDisplayed(driver, "ThrdProductPrice"))
                  {
                        String products3=UIFoundation.getText(driver, "ThrdProductPrice");
                        products3=products3.replaceAll("[ ]","");
                        product3=Double.parseDouble(products3);
                        product3=ApplicationDependent.productPrice(driver, product3);
                        arrayList.add(product3);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "ThirdProductPriceSale"))
                        {
                               String products3=UIFoundation.getText(driver, "ThirdProductPriceSale");
                               products3=products3.replaceAll("[ ]","");
                               product3=Double.parseDouble(products3);
                               product3=ApplicationDependent.productPrice(driver, product3);
                               arrayList.add(product3);
                        }

                  }
                  if(UIFoundation.isDisplayed(driver, "FrthProductPrice"))
                  {
                        String products4=UIFoundation.getText(driver, "FrthProductPrice");
                        products4=products4.replaceAll("[ ]","");
                        product4=Double.parseDouble(products4);
                        product4=ApplicationDependent.productPrice(driver, product4);
                        arrayList.add(product4);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "FourthProductPriceSale"))
                        {
                               String products4=UIFoundation.getText(driver, "FourthProductPriceSale");
                               products4=products4.replaceAll("[ ]","");
                               product4=Double.parseDouble(products4);
                               product4=ApplicationDependent.productPrice(driver, product4);
                               arrayList.add(product4);
                        }

                  }
                  if(UIFoundation.isDisplayed(driver, "FifhtProductPrice"))
                  {
                        String products5=UIFoundation.getText(driver, "FifhtProductPrice");
                        products5=products5.replaceAll("[ ]","");
                        product5=Double.parseDouble(products5);
                        product5=ApplicationDependent.productPrice(driver, product5);
                        arrayList.add(product5);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "FifthProductPriceSale"))
                        {
                               String products5=UIFoundation.getText(driver, "FifthProductPriceSale");
                               products5=products5.replaceAll("[ ]","");
                               product5=Double.parseDouble(products5);
                               product5=ApplicationDependent.productPrice(driver, product5);
                               arrayList.add(product5);
                        }

                  }








                  if(!UIFoundation.getText(driver, "FrstProductPrice").contains("Fail"))
                  {      String products1=UIFoundation.getText(driver, "FrstProductPrice");
                        products1=products1.replaceAll("[ ]","");
                        product1=Double.parseDouble(products1);
                        product1=ApplicationDependent.productPrice(driver, product1);
                  }
                  if(!UIFoundation.getText(driver, "SecndProductPrice").contains("Fail"))
                  {
                        String products2=UIFoundation.getText(driver, "SecndProductPrice");
                        products2=products2.replaceAll("[ ]","");
                        product2=Double.parseDouble(products2);
                        product2=ApplicationDependent.productPrice(driver, product2);
                  }
                  if(!UIFoundation.getText(driver, "ThrdProductPrice").contains("Fail"))
                  {
                        String products3=UIFoundation.getText(driver, "ThrdProductPrice");
                        products3=products3.replaceAll("[ ]","");
                        product3=Double.parseDouble(products3);
                        product3=ApplicationDependent.productPrice(driver, product3);
                  }
                  if(!UIFoundation.getText(driver, "FrthProductPrice").contains("Fail"))
                  {
                        String products4=UIFoundation.getText(driver, "FrthProductPrice");
                        products4=products4.replaceAll("[ ]","");
                        product4=Double.parseDouble(products4);
                        product4=ApplicationDependent.productPrice(driver, product4);
                  }
                  if(!UIFoundation.getText(driver, "FrthProductPrice").contains("Fail"))
                  {
                        String products5=UIFoundation.getText(driver, "FifhtProductPrice");
                        products5=products5.replaceAll("[ ]","");
                        product5=Double.parseDouble(products5);
                        product5=ApplicationDependent.productPrice(driver, product5);
                  }
                   Double products1=UIFoundation.getDouble(driver, "FrstProductPrice");
                  Double products2=UIFoundation.getDouble(driver, "SecndProductPrice");
                  Double products3=UIFoundation.getDouble(driver, "ThrdProductPrice");
                  Double products4=UIFoundation.getDouble(driver, "FrthProductPrice");
                  Double products5=UIFoundation.getDouble(driver, "FifhtProductPrice");
                   arrayList.add(product1);
                  arrayList.add(product2);
                  arrayList.add(product3);
                  arrayList.add(product4);
                  arrayList.add(product5);
                  System.out.println(arrayList);
                     for(int i = 0; i < arrayList.size()-1; i++) {
                         // current String is < than the next one (if there are equal list is still sorted)
                         if(arrayList.get(i)>(arrayList.get(i + 1))) { 
                            isSorted=false;
                             break;
                         }

                      }
                      if(isSorted)
                      {
                          objStatus+=true;
                              String objDetail="Products are sorted from low to high price";
                              ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
                         System.out.println("Products are sorted from low to high price");
                               return true;
                      }
                      else
                      {
                         objStatus+=false;
                               String objDetail="Products are not sorted from low to high price";
                               UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);

                         System.err.println("Products are not sorted from low to high price");
                               return false;
                      }

           }catch(Exception e)
           {
                  return false;

           }

    }


	         *//***************************************************************************
	         * Method Name                    : validationForSortingPriceHtoL ()
	         * Created By              : Vishwanath Chavan
	         * Reviewed By                    : Ramesh,KB
	         * Purpose                        : 
	         ****************************************************************************
	         *//*
    public static boolean validationForSortingPriceHtoL(WebDriver driver)
    {

           List<Double> arrayList = new ArrayList<Double>();;
           boolean isSorted=true;
           Double product1=0.0;
           Double product2=0.0;
           Double product3=0.0;
           Double product4=0.0;
           Double product5=0.0;
           String objStatus=null;
           String screenshotName = "Scenarios_HtoL_Screenshot.jpeg";
           String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
           try
           {


           //     driver.navigate().refresh();
                  if(UIFoundation.isDisplayed(driver, "FrstProductPrice"))
                  {
                        String products1=UIFoundation.getText(driver, "FrstProductPrice");
                        products1=products1.replaceAll("[,]","");
                        product1=Double.parseDouble(products1);
                        product1=ApplicationDependent.productPrice(driver, product1);
                        arrayList.add(product1);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "FrstProductPriceSale"))
                        {
                               String products1=UIFoundation.getText(driver, "FrstProductPriceSale");
                               products1=products1.replaceAll("[,]","");
                               product1=Double.parseDouble(products1);
                               product1=ApplicationDependent.productPrice(driver, product1);
                               arrayList.add(product1);
                        }

                  }


                  if(UIFoundation.isDisplayed(driver, "SecndProductPrice"))
                  {
                        String products2=UIFoundation.getText(driver, "SecndProductPrice");
                        products2=products2.replaceAll("[,]","");
                        product2=Double.parseDouble(products2);
                        product2=ApplicationDependent.productPrice(driver, product2);
                        arrayList.add(product2);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "SecondProductPriceSale"))
                        {
                               String products2=UIFoundation.getText(driver, "SecondProductPriceSale");
                               products2=products2.replaceAll("[,]","");
                               product2=Double.parseDouble(products2);
                               product2=ApplicationDependent.productPrice(driver, product2);
                               arrayList.add(product2);
                        }

                  }
                  if(UIFoundation.isDisplayed(driver, "ThrdProductPrice"))
                  {
                        String products3=UIFoundation.getText(driver, "ThrdProductPrice");
                        products3=products3.replaceAll("[,]","");
                        product3=Double.parseDouble(products3);
                        product3=ApplicationDependent.productPrice(driver, product3);
                        arrayList.add(product3);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "ThirdProductPriceSale"))
                        {
                               String products3=UIFoundation.getText(driver, "ThirdProductPriceSale");
                               products3=products3.replaceAll("[,]","");
                               product3=Double.parseDouble(products3);
                               product3=ApplicationDependent.productPrice(driver, product3);
                               arrayList.add(product3);
                        }

                  }
                  if(UIFoundation.isDisplayed(driver, "FrthProductPrice"))
                  {
                        String products4=UIFoundation.getText(driver, "FrthProductPrice");
                        products4=products4.replaceAll("[,]","");
                        product4=Double.parseDouble(products4);
                        product4=ApplicationDependent.productPrice(driver, product4);
                        arrayList.add(product4);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "FourthProductPriceSale"))
                        {
                               String products4=UIFoundation.getText(driver, "FourthProductPriceSale");
                               products4=products4.replaceAll("[,]","");
                               product4=Double.parseDouble(products4);
                               product4=ApplicationDependent.productPrice(driver, product4);
                               arrayList.add(product4);
                        }

                  }
                  if(UIFoundation.isDisplayed(driver, "FifhtProductPrice"))
                  {
                        String products5=UIFoundation.getText(driver, "FifhtProductPrice");
                        products5=products5.replaceAll("[,]","");
                        product5=Double.parseDouble(products5);
                        product5=ApplicationDependent.productPrice(driver, product5);
                        arrayList.add(product5);

                  }else{
                        if(UIFoundation.isDisplayed(driver, "FifthProductPriceSale"))
                        {
                               String products5=UIFoundation.getText(driver, "FifthProductPriceSale");
                               products5=products5.replaceAll("[,]","");
                               product5=Double.parseDouble(products5);
                               product5=ApplicationDependent.productPrice(driver, product5);
                               arrayList.add(product5);
                        }

                  }
                  if(!UIFoundation.getText(driver, "FrstProductPrice").contains("Fail"))
                  {      String products1=UIFoundation.getText(driver, "FrstProductPrice");
                        products1=products1.replaceAll("[ ]","");
                        product1=Double.parseDouble(products1);
                        product1=ApplicationDependent.productPrice(driver, product1);
                  }
                  if(!UIFoundation.getText(driver, "SecndProductPrice").contains("Fail"))
                  {
                        String products2=UIFoundation.getText(driver, "SecndProductPrice");
                        products2=products2.replaceAll("[ ]","");
                        product2=Double.parseDouble(products2);
                        product2=ApplicationDependent.productPrice(driver, product2);
                  }
                  if(!UIFoundation.getText(driver, "ThrdProductPrice").contains("Fail"))
                  {
                         String products3=UIFoundation.getText(driver, "ThrdProductPrice");
                        products3=products3.replaceAll("[ ]","");
                        product3=Double.parseDouble(products3);
                        product3=ApplicationDependent.productPrice(driver, product3);
                  }
                  if(!UIFoundation.getText(driver, "FrthProductPrice").contains("Fail"))
                  {
                        String products4=UIFoundation.getText(driver, "FrthProductPrice");
                        products4=products4.replaceAll("[ ]","");
                        product4=Double.parseDouble(products4);
                        product4=ApplicationDependent.productPrice(driver, product4);
                  }
                  if(!UIFoundation.getText(driver, "FrthProductPrice").contains("Fail"))
                  {
                        String products5=UIFoundation.getText(driver, "FifhtProductPrice");
                        products5=products5.replaceAll("[ ]","");
                        product5=Double.parseDouble(products5);
                        product5=ApplicationDependent.productPrice(driver, product5);
                  }
                  String products1=UIFoundation.getText(driver, "FrstProductPrice");
                  Double product1 = Double.parseDouble(products1.replaceAll("[,]",""));
                   String products2=UIFoundation.getText(driver, "SecndProductPrice");
                  Double product2 = Double.parseDouble(products2.replaceAll("[,]",""));
                  String products3=UIFoundation.getText(driver, "ThrdProductPrice");
                  Double product3 = Double.parseDouble(products3.replaceAll("[,]",""));
                String products4=UIFoundation.getText(driver, "FrthProductPrice");
                  Double product4 = Double.parseDouble(products4.replaceAll("[,]",""));
                  String products5=UIFoundation.getText(driver, "FifhtProductPrice");
                  Double product5 = Double.parseDouble(products5.replaceAll("[,]",""));
                   arrayList.add(product1);
                  arrayList.add(product2);
                  arrayList.add(product3);
                  arrayList.add(product4);
                  arrayList.add(product5);
                  System.out.println(arrayList);
                     for(int i = 0; i < arrayList.size()-1; i++) {
                         // current String is < than the next one (if there are equal list is still sorted)
                         if(arrayList.get(i)<(arrayList.get(i + 1))) { 
                            isSorted=false;
                             break;
                         }

                      }
                      if(isSorted)
                      {
                          objStatus+=true;
                              String objDetail="Products are sorted from High to Low price";
                              ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
                         System.out.println("Products are sorted from High to Low price");
                               return true;
                      }
                      else
                      {
                         objStatus+=false;
                               String objDetail="Products are not sorted from High to Low price";
                               UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
                         System.err.println("Products are not sorted from High to Low price");
                               return false;
                      }

           }catch(Exception e)
           {
                  return false;

           }

    }




	          */
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 *//*

	public static String login(WebDriver driver)
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{

			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavAccountTab"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavSignIn"));
			UIFoundation.waitFor(6L); 
		//	UIFoundation.waitFor(4L);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			WebElement element = wait.until(
			        ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));

			objStatus+=String.valueOf(UIFoundation.setObject(driver, "LoginEmail", "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "LoginPassword", "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "SignInButton"));
			UIFoundation.waitFor(8L);
			String actualtile=driver.getTitle();
			String expectedTile=objExpectedRes.getProperty("homePageTitle");
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}



	  *//***************************************************************************
	  * Method Name			: verifyClockIconPresent()
	  * Created By			: Vishwanath Chavan 
	  * Reviewed By			: Ramesh,KB
	  * Purpose				: The purpose of this method is Login into the Wine.com
	  * 						  Application
	  ****************************************************************************
	  *//*

	public static void verifyClockIconPresent(WebDriver driver)
	{


		try
		{
			log.info("The execution of the method validationForOnlyPreSaleProducts started here ...");
			if (UIFoundation.isDisplayed(driver, "FirstClockIcon")) {
				System.out.println("Clock icon is present for the first product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "SecondClockIcon")) {
				System.out.println("Clock icon is present for the Second product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "ThirdClockIcon")) {
				System.out.println("Clock icon is present for the Third product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "FourthClockIcon")) {
				System.out.println("Clock icon is present for the fourth product");
			}
			UIFoundation.waitFor(3L);
			if (UIFoundation.isDisplayed(driver, "FifthClockIcon")) {
				System.out.println("Clock icon is present for the fifth product");
			}
			log.info("The execution of the method validationForOnlyPreSaleProducts ended here ...");


		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method validationForOnlyPreSaleProducts "+ e);


		}
	}

	   *//***************************************************************************
	   * Method Name			: validationForGiftSortingZtoA()
	   * Created By			: Vishwanath Chavan
	   * Reviewed By			: Ramesh,KB
	   * Purpose				: 
	   ****************************************************************************
	   *//*
	public static boolean validationForGiftSortingZtoA(WebDriver driver)
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		   boolean isSorted = true;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(driver, "FrstProductName");
			String products2=UIFoundation.getText(driver, "SecProductName");
			String products3=UIFoundation.getText(driver, "ThrdProductName");
			String products4=UIFoundation.getText(driver, "FrthProductName");
			String products5=UIFoundation.getText(driver, "FfthProductName");
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }

			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted in the order of Z-A");
					return true;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted in the order of Z-A");
					return false;
			    }
		}catch(Exception e)
		{
			return false;

		}

	}

	    *//***************************************************************************
	    * Method Name			: validationForGiftSortingAtoZ()
	    * Created By			: Vishwanath Chavan
	    * Reviewed By			: Ramesh,KB
	    * Purpose				: 
	    ****************************************************************************
	    *//*
	public static String validationForGiftSortingAtoZ(WebDriver driver)
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
		   String screenshotName = "Scenarios_validationForGiftSortingAtoZ_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;

		  // String objStatus=null;
		try
		{
			driver.navigate().refresh();
			String products1=UIFoundation.getText(driver, "FrstProductName");
			String products2=UIFoundation.getText(driver, "SecProductName");
			String products3=UIFoundation.getText(driver, "ThrdProductName");
			String products4=UIFoundation.getText(driver, "FrthProductName");
			String products5=UIFoundation.getText(driver, "FfthProductName");
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			arrayList.add(products3.replaceAll("[0-9]", ""));
			arrayList.add(products4.replaceAll("[0-9]", ""));
			arrayList.add(products5.replaceAll("[0-9]", ""));
			arrayListYr.add(products1.replaceAll("[^0-9]", ""));
			arrayListYr.add(products2.replaceAll("[^0-9]", ""));
			arrayListYr.add(products3.replaceAll("[^0-9]", ""));
			arrayListYr.add(products4.replaceAll("[^0-9]", ""));
			arrayListYr.add(products5.replaceAll("[^0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }

			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted in the order of A-Z");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted in the order of A-Z");
			    	objStatus+=false;
			    	String objDetail="Products are not sorted from High to Low price";
			    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
					return objStatus;
			    }


		}catch(Exception e)
		{
			System.err.println("Gift Certificates are not sorted in the order of A-Z");
	    	objStatus+=false;
	    	String objDetail="Products are not sorted from High to Low price";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;

		}

	}



	     *//***************************************************************************
	     * Method Name			: validationForGiftSortingPriceLtoH()
	     * Created By			: Vishwanath Chavan
	     * Reviewed By			: Ramesh,KB
	     * Purpose				: 
	     ****************************************************************************
	     *//*
	public static String validationForGiftSortingPriceLtoH(WebDriver driver)
	{
		WebElement ele=null;
		String objStatus=null;
		 List<Double> arrayList = new ArrayList<Double>();;
		 Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
		boolean isSorted=true;
		   String screenshotName = "Scenarios_validationForGiftSortingPriceLtoH_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			driver.navigate().refresh();
			if(!UIFoundation.getText(driver, "GiftSPriceFirstProduct").contains("Fail"))
			{	String products1=UIFoundation.getText(driver, "GiftSPriceFirstProduct");
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=ApplicationDependent.productPrice(driver, product1);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceSecondProduct").contains("Fail"))
			{
				String products2=UIFoundation.getText(driver, "GiftSPriceSecondProduct");
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=ApplicationDependent.productPrice(driver, product2);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceThirdProduct").contains("Fail"))
			{
				String products3=UIFoundation.getText(driver, "GiftSPriceThirdProduct");
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=ApplicationDependent.productPrice(driver, product3);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceFourthProduct").contains("Fail"))
			{
				String products4=UIFoundation.getText(driver, "GiftSPriceFourthProduct");
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=ApplicationDependent.productPrice(driver, product4);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceFifthProduct").contains("Fail"))
			{
				String products5=UIFoundation.getText(driver, "GiftSPriceFifthProduct");
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=ApplicationDependent.productPrice(driver, product5);
			}
			driver.navigate().refresh();
			Double products1=UIFoundation.getDouble(driver, "GiftSPriceFirstProduct");
			Double products2=UIFoundation.getDouble(driver, "GiftSPriceSecondProduct");
			Double products3=UIFoundation.getDouble(driver, "GiftSPriceThirdProduct");
			Double products4=UIFoundation.getDouble(driver, "GiftSPriceFourthProduct");
			Double products5=UIFoundation.getDouble(driver, "GiftSPriceFifthProduct");
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)>(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }

			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted from low to high price");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted from low to high price");
			    	objStatus+=false;
			    	String objDetail="Products are not sorted from High to Low price";
			    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
					return objStatus;
			    }


		}catch(Exception e)
		{
			System.err.println("Gift Certificates are not sorted from low to high price");
	    	objStatus+=false;
	    	String objDetail="Products are not sorted from High to Low price";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;

		}

	}

	      *//***************************************************************************
	      * Method Name			: validationForGiftSortingPriceHtoL()
	      * Created By			: Vishwanath Chavan
	      * Reviewed By			: Ramesh,KB
	      * Purpose				: 
	      ****************************************************************************
	      *//*
	public static String validationForGiftSortingPriceHtoL(WebDriver driver)
	{

		 List<Double> arrayList = new ArrayList<Double>();;
		boolean isSorted=true;
		 Double product1=0.0;
			Double product2=0.0;
			Double product3=0.0;
			Double product4=0.0;
			Double product5=0.0;
			String objStatus=null;
			   String screenshotName = "Scenarios_validationForGiftSortingPriceHtoL_Screenshot.jpeg";
			   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
						+ screenshotName;
		try
		{
			driver.navigate().refresh();
			if(!UIFoundation.getText(driver, "GiftSPriceFirstProduct").contains("Fail"))
			{	String products1=UIFoundation.getText(driver, "GiftSPriceFirstProduct");
				products1=products1.replaceAll("[ ]","");
				product1=Double.parseDouble(products1);
				product1=ApplicationDependent.productPrice(driver, product1);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceSecondProduct").contains("Fail"))
			{
				String products2=UIFoundation.getText(driver, "GiftSPriceSecondProduct");
				products2=products2.replaceAll("[ ]","");
				product2=Double.parseDouble(products2);
				product2=ApplicationDependent.productPrice(driver, product2);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceThirdProduct").contains("Fail"))
			{
				String products3=UIFoundation.getText(driver, "GiftSPriceThirdProduct");
				products3=products3.replaceAll("[ ]","");
				product3=Double.parseDouble(products3);
				product3=ApplicationDependent.productPrice(driver, product3);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceFourthProduct").contains("Fail"))
			{
				String products4=UIFoundation.getText(driver, "GiftSPriceFourthProduct");
				products4=products4.replaceAll("[ ]","");
				product4=Double.parseDouble(products4);
				product4=ApplicationDependent.productPrice(driver, product4);
			}
			if(!UIFoundation.getText(driver, "GiftSPriceFifthProduct").contains("Fail"))
			{
				String products5=UIFoundation.getText(driver, "GiftSPriceFifthProduct");
				products5=products5.replaceAll("[ ]","");
				product5=Double.parseDouble(products5);
				product5=ApplicationDependent.productPrice(driver, product5);
			}
			arrayList.add(product1);
			arrayList.add(product2);
			arrayList.add(product3);
			arrayList.add(product4);
			arrayList.add(product5);
			System.out.println(arrayList);
			   for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i)<(arrayList.get(i + 1))) { 
			    	   isSorted=false;
			           break;
			       }

			    }
			    if(isSorted)
			    {
			    	System.out.println("Gift Certificates are sorted from High to Low price");
			    	objStatus+=true;
					return objStatus;
			    }
			    else
			    {
			    	System.err.println("Gift Certificates are not sorted from High to Low price");
			    	objStatus+=false;
			    	String objDetail="Products are not sorted from High to Low price";
			    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
					return objStatus;
			    }


		}catch(Exception e)
		{
			System.err.println("Gift Certificates are not sorted from High to Low price");
	    	objStatus+=false;
	    	String objDetail="Products are not sorted from High to Low price";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;

		}

	}


	       *//***************************************************************************
	       * Method Name			: productPrice()
	       * Created By			: Vishwanath Chavan
	       * Reviewed By			: Ramesh,KB
	       * Purpose				: 
	       ****************************************************************************
	       *//*
	public static double productPrice(WebDriver driver,double d)
	{
		String strStatus=null;
		int cartConut=0;
		double prodPrice=0;

		try
		{
			prodPrice=d;
			int x = 2; // Or 2, or whatever
			BigDecimal unscaled = new BigDecimal(prodPrice);
			BigDecimal scaled = unscaled.scaleByPowerOfTen(-x);
			prodPrice=scaled.doubleValue();
			return prodPrice;


		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return cartConut;
	}

	        *//***************************************************************************
	        * Method Name			: productPrice()
	        * Created By			: Vishwanath Chavan
	        * Reviewed By			: Ramesh,KB
	        * Purpose				: 
	        ****************************************************************************
	        *//*
	public static String verifyAdGiftWrappingPrice(Double giftBagValue,int giftWrapCount,Double cartSubTot,int productCountInCart)
	{
		String strStatus=null;
		int cartConut=0;
		double prodPrice=0;
		String objStatus = null;
		String finalsubTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		//int productCountInCart=0;
		int productCountInShoppingCart=0;
		String screenshotName = "Scenarios_validationForSortingPriceLtoH_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			finalsubTotal=UIFoundation.getText(driver, "Subtotal");
			finalsubTotal = finalsubTotal.replaceAll("[$,]","");
			double finalSubTot=Double.parseDouble(finalsubTotal);
			finalSubTot=(finalSubTot*10/100);
			finalSubTot=Math.round(finalSubTot*100);
			finalSubTot=finalSubTot/100;
			double finalSubtotalPrice=(giftBagValue*giftWrapCount)+cartSubTot;
			finalSubtotalPrice=(finalSubtotalPrice*10/100);
			finalSubtotalPrice=Math.round(finalSubtotalPrice*100);
			finalSubtotalPrice=finalSubtotalPrice/100;
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "ViewCart"));
			UIFoundation.waitFor(1L);
			String proCnt=UIFoundation.getText(driver, "CartHeadLineCount");
			productCountInShoppingCart=Integer.parseInt(proCnt);
			if((finalSubtotalPrice==finalSubTot) && (productCountInShoppingCart==productCountInCart ))
			{
				System.out.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are matched ");
		    	objStatus+=true;
				return objStatus;
			}
			else
			{

				System.err.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are not matched ");
		    	objStatus+=false;
		    	String objDetail="Final Review page Subtotal and cart page SubTotal and prices are not matched";
		    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
		    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
				return objStatus;
			}


		}catch(Exception e)
		{
			System.err.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are not matched ");
	    	objStatus+=false;
	    	String objDetail="Final Review page Subtotal and cart page SubTotal and prices are not matched";
	    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
	    //	ReportUtil.addTestStepsDetails("Products are not sorted from low to high price", "Fail", "");
			return objStatus;
		}

	}

	         *//***************************************************************************
	         * Method Name			: login()
	         * Created By			: Vishwanath Chavan 
	         * Reviewed By			: Ramesh,KB
	         * Purpose				: The purpose of this method is Login into the Wine.com
	         * 						  Application
	         ****************************************************************************
	         *//*

	public static String loginPrefferedAddress(WebDriver driver)
	{
		String objStatus=null;

		try
		{

			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "MainNavButton"));	
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "MainNavAccountTab"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "MainNavSignIn"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "LoginEmail", "preferenceAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "LoginPassword", "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "SignInButton"));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}


	          *//***************************************************************************
	          * Method Name			: footerValidation()
	          * Created By			: Haripriya
	          * Reviewed By			: Ramesh
	          * Purpose				: The purpose of this method is to verify footer
	          ****************************************************************************
	          *//*

	public static String footerValidation(WebDriver driver)
	{
		String objStatus=null;
		String screenshotName = "Scenarios_FooterValidation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try
		{

			log.info("The execution of the method footerValidation started here ...");
			UIFoundation.waitFor(8L);


			if(UIFoundation.isDisplayed(driver, "footerCustomerCare")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Customer Care is displayed in footer", "Pass", "");
				 System.out.println("Customer Care is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Customer Care is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"care", objDetail);
				 System.err.println("Customer Care is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerWineCom")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Wine.com is displayed in footer", "Pass", "");
				 System.out.println("Wine.com is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Wine.com is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"wine", objDetail);
				 System.err.println("Wine.com is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerWorkWithUs")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("How to work with us is displayed in footer", "Pass", "");
				 System.out.println("How to work with us is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="How to work with us is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"work", objDetail);
				 System.err.println("How to work with us is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerAppStore")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("App Store is displayed in footer", "Pass", "");
				 System.out.println("App Store is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="App Store is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"app", objDetail);
				 System.err.println("App Store is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerGooglePlay")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Google Play is displayed in footer", "Pass", "");
				 System.out.println("Google Play is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Google Play is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"play", objDetail);
				 System.err.println("Google Play is displayed in footer");
			 }
			UIFoundation.waitFor(4L);

			if(UIFoundation.isDisplayed(driver, "footerBuyUsingWineApp")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Buy using Wine.com app is displayed in footer", "Pass", "");
				 System.out.println("Buy using Wine.com app is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Buy using Wine.com app is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"buy", objDetail);
				 System.err.println("Buy using Wine.com app is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerFeedback")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Feedback is displayed in footer", "Pass", "");
				 System.out.println("Feedback is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Feedback is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"feedback", objDetail);
				 System.err.println("Feedback is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerPrivacypolicy")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Privacy policy is displayed in footer", "Pass", "");
				 System.out.println("Privacy policy is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Privacy policy is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"privacy", objDetail);
				 System.err.println("Privacy policy is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerTermsOfService")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Terms of service is displayed in footer", "Pass", "");
				 System.out.println("Terms of service is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Terms of service is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"terms", objDetail);
				 System.err.println("Terms of service is displayed in footer");
			 }

			if(UIFoundation.isDisplayed(driver, "footerFeedback")){
				 objStatus+=true;
			     ReportUtil.addTestStepsDetails("Copyright is displayed in footer", "Pass", "");
				 System.out.println("Copyright is displayed in footer");
			}else{
				 objStatus+=false;
				 String objDetail="Copyright is not displayed in footer";
			     UIFoundation.captureScreenShot(driver, screenshotpath+"copyright", objDetail);
				 System.err.println("Copyright is displayed in footer");
			 }

			log.info("The execution of the method footerValidation ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("footerValidation test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("footerValidation test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method footerValidation "+ e);
			return "Fail";

		}
	}

	           *//***************************************************************************
	           * Method Name			: recommendedProductsAddToCartBtn()
	           * Created By			: Vishwanath Chavan
	           * Reviewed By			: Ramesh,KB
	           * Purpose				: 
	           ****************************************************************************
	           *//*



	            *//***************************************************************************
	            * Method Name			: validationForMyWineSortingPurchaseDateNewToOld()
	            * Created By			: Vishwanath Chavan
	            * Reviewed By			: Ramesh,KB
	            * Purpose				: 
	            ****************************************************************************
	            *//*
	public static String validationForMyWineSortingPurchaseDateNewToOld(WebDriver driver)
	{
		String product1 = null;
		String product2 = null;
		String product3 = null;
		String product4 = null;
		String product5 = null;
		String product6 = null;
		String product7 = null;
		ArrayList<String> dates=new ArrayList<String>();
		String screenshotName = "Scenarios_PurchaseDateHtoL_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		String objStatus="";
		try
		{	if(UIFoundation.isDisplayed(driver, "myWineFirstProdPrice")){
			product1 = UIFoundation.getText(driver, "myWineFirstProdPrice");
			product1=product1.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product1);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSecProdPrice")){
			product2 = UIFoundation.getText(driver, "myWineSecProdPrice");
			product2=product2.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product2);

		}
		if(UIFoundation.isDisplayed(driver, "myWineThirdProdPrice")){
			product3 = UIFoundation.getText(driver, "myWineThirdProdPrice");
			product3=product3.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product3);

		}
		if(UIFoundation.isDisplayed(driver, "myWineFourtProdPrice")){
			product4 = UIFoundation.getText(driver, "myWineFourtProdPrice");
			product4=product4.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product4);

		}
		if(UIFoundation.isDisplayed(driver, "myWineFifthProdPrice")){
			product5 = UIFoundation.getText(driver, "myWineFifthProdPrice");
			product5=product5.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product5);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSixthProdPrice")){
			product6 = UIFoundation.getText(driver, "myWineSixthProdPrice");
			product6=product6.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product6);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSevenProdPrice")){
			product7 = UIFoundation.getText(driver, "myWineSevenProdPrice");
			product7=product7.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product7);

		}	
		System.out.println(dates);
		for(int i=0;i<dates.size();i++){
			Date d1=new Date(dates.get(i));
			for(int j=i+1;j<dates.size();j++)
			{
				Date d2=new Date(dates.get(j));
				if(d1.before(d2) || d1.equals(d2)){
					objStatus+=true;

				}else{
					objStatus+=false;
				}
			}
			if(objStatus.contains("false")){
				 objStatus+=true;
			      String objDetail="Product's purchased rate from new to old are displayed with the respective selected sort option.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		    	System.out.println("Product's purchased rate from new to old are displayed with the respective selected sort option.");
				//return true;
			}else{
				 objStatus+=false;
			       String objDetail="Product's purchased rate from new to old are not displayed with the respective selected sort option.";
			       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			       System.err.println("Product's purchased rate from new to old are not displayed with the respective selected sort option.");
			      // return false;
			}
		}
		return objStatus;
		}catch(Exception e)
		{
			return objStatus;

		}



	}



	             *//***************************************************************************
	             * Method Name			: validationForMyWineSortingRatingDateNewToOld()
	             * Created By			: Vishwanath Chavan
	             * Reviewed By			: Ramesh,KB
	             * Purpose				: 
	             ****************************************************************************
	             *//*
	public static String validationForMyWineSortingRatingDateNewToOld(WebDriver driver)
	{
		String product1 = null;
		String product2 = null;
		String product3 = null;
		String product4 = null;
		String product5 = null;
		String product6 = null;
		String product7 = null;
		ArrayList<String> dates=new ArrayList<String>();
		String screenshotName = "Scenarios_RatingDateNtoO_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		String objStatus="";
		try
		{	if(UIFoundation.isDisplayed(driver, "myWineFirstProdPrice")){
			product1 = UIFoundation.getText(driver, "myWineFirstProdPrice");
			product1=product1.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product1);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSecProdPrice")){
			product2 = UIFoundation.getText(driver, "myWineSecProdPrice");
			product2=product2.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product2);

		}
		if(UIFoundation.isDisplayed(driver, "myWineThirdProdPrice")){
			product3 = UIFoundation.getText(driver, "myWineThirdProdPrice");
			product3=product3.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product3);

		}
		if(UIFoundation.isDisplayed(driver, "myWineFourtProdPrice")){
			product4 = UIFoundation.getText(driver, "myWineFourtProdPrice");
			product4=product4.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product4);

		}
		if(UIFoundation.isDisplayed(driver, "myWineFifthProdPrice")){
			product5 = UIFoundation.getText(driver, "myWineFifthProdPrice");
			product5=product5.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product5);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSixthProdPrice")){
			product6 = UIFoundation.getText(driver, "myWineSixthProdPrice");
			product6=product6.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product6);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSevenProdPrice")){
			product7 = UIFoundation.getText(driver, "myWineSevenProdPrice");
			product7=product7.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product7);

		}	
		System.out.println(dates);
		for(int i=0;i<dates.size();i++){
			Date d1=new Date(dates.get(i));
			for(int j=i+1;j<dates.size();j++)
			{
				Date d2=new Date(dates.get(j));
				if(d1.after(d2) || d1.equals(d2)){
					objStatus+=true;

				}else{
					objStatus+=false;
				}
			}
		}
		if(objStatus.contains("false")){
			objStatus+=false;
			  String objDetail="Rating date from new to old are not displayed with the respective selected sort option.";
		       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
		       System.err.println("Rating date from new to old  are not displayed with the respective selected sort option.");

			//return true;
		}else{

			 objStatus+=true;
			  String objDetail="Rating date from new to old  are displayed with the respective selected sort option.";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	    	System.out.println("Rating date from new to old  are displayed with the respective selected sort option.");
		      // return false;
		}
		return objStatus;
		}catch(Exception e)
		{
			return objStatus;

		}

	}

	              *//***************************************************************************
	              * Method Name			: validationForMyWineSortingRatingDateNewToOld()
	              * Created By			: Vishwanath Chavan
	              * Reviewed By			: Ramesh,KB
	              * Purpose				: 
	              ****************************************************************************
	              *//*
	public static String validationForMyWineSortingRatingDateOldToNew(WebDriver driver)
	{
		String product1 = null;
		String product2 = null;
		String product3 = null;
		String product4 = null;
		String product5 = null;
		String product6 = null;
		String product7 = null;
		ArrayList<String> dates=new ArrayList<String>();
		String screenshotName = "Scenarios_RatingDateLtoN_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		String objStatus="";
		try
		{	if(UIFoundation.isDisplayed(driver, "myWineFirstProdPrice")){
			product1 = UIFoundation.getText(driver, "myWineFirstProdPrice");
			product1=product1.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product1);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSecProdPrice")){
			product2 = UIFoundation.getText(driver, "myWineSecProdPrice");
			product2=product2.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product2);

		}
		if(UIFoundation.isDisplayed(driver, "myWineThirdProdPrice")){
			product3 = UIFoundation.getText(driver, "myWineThirdProdPrice");
			product3=product3.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product3);

		}
		if(UIFoundation.isDisplayed(driver, "myWineFourtProdPrice")){
			product4 = UIFoundation.getText(driver, "myWineFourtProdPrice");
			product4=product4.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product4);

		}
		if(UIFoundation.isDisplayed(driver, "myWineFifthProdPrice")){
			product5 = UIFoundation.getText(driver, "myWineFifthProdPrice");
			product5=product5.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product5);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSixthProdPrice")){
			product6 = UIFoundation.getText(driver, "myWineSixthProdPrice");
			product6=product6.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product6);

		}
		if(UIFoundation.isDisplayed(driver, "myWineSevenProdPrice")){
			product7 = UIFoundation.getText(driver, "myWineSevenProdPrice");
			product7=product7.replaceAll("[a-z A-Z ( )]", "");
			dates.add(product7);

		}	
		System.out.println(dates);
		for(int i=0;i<dates.size();i++){
			Date d1=new Date(dates.get(i));
			for(int j=i+1;j<dates.size();j++)
			{
				Date d2=new Date(dates.get(j));
				if(d1.before(d2) || d1.equals(d2)){
					objStatus+=true;

				}else{
					objStatus+=false;
				}
			}
		}
		if(objStatus.contains("false")){
			objStatus+=false;
			  String objDetail="Rating date from old to new are not displayed with the respective selected sort option.";
		       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
		       System.err.println("Rating date from old to new  are not displayed with the respective selected sort option.");

			//return true;
		}else{

			 objStatus+=true;
			  String objDetail="Rating date from old to new  are displayed with the respective selected sort option.";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	    	System.out.println("Rating date from old to new  are displayed with the respective selected sort option.");
		      // return false;
		}
		return objStatus;
		}catch(Exception e)
		{
			return objStatus;

		}



	}

	               */


}


