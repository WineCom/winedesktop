package wine.autotest.mobile.library;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;
import wine.autotest.fw.utilities.XMLData;




public class Initialize extends Mobile {
	
	//***********************************************************************************************
	
	// Sauce usernameMobile
 //   public static String usernameMobile = "vishu1234";

    // Sauce access key
 //   public static String accesskey = "a8258f5b-93b2-443d-bb5c-9b01f5f14136";
	
//	static String sauceLabUN=objConfig.getProperty("userName");
//	static String sauceAccessKey=objConfig.getProperty("accessKey");
//	public static String usernameMobile = sauceLabUN;
//    public static String accesskey =sauceAccessKey;
    private static ThreadLocal<String> sessionId = new ThreadLocal<String>();

    public String getSessionId() {
    	return sessionId.get();
    }
    
    
    /***************************************************************************
	 * Method Name			: launchBrowser()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini
	 * Purpose				: The purpose of this method is to launch the browser
	 ****************************************************************************
	 */
	
	public static WebDriver launchRemoteDriver()
	{
		String browserType=null;
		try
		{
			log.info("The execution of the method launchBrowser started here ...");
			browserType=environmentUrl;
			
			if(browserType.equalsIgnoreCase("ff"))
			{
				
				DesiredCapabilities capsff = DesiredCapabilities.firefox();
                capsff.setBrowserName("firefox");
                capsff.setCapability("tunnel-identifier", "saucelabexecutor");
//                driver = new RemoteWebDriver(new URL("http://" + usernameMobile + ":" + accesskey + "@saucelabexecutor:4445/wd/hub"), capsff);

			}
			if(browserType.equalsIgnoreCase("ch"))
			{
                DesiredCapabilities capsch = DesiredCapabilities.chrome();
                capsch.setBrowserName("chrome");
                capsch.setCapability("tunnel-identifier", "saucelabexecutor");
                
//                driver = new RemoteWebDriver(new URL("http://" + usernameMobile + ":" + accesskey + "@192.168.19.124:4445/wd/hub"), capsch);
			}
			else
			{
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\Driver\\chromedriver.exe");
				driver=new InternetExplorerDriver();
			}
				
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method launchBrowser "+e);
		}
		
        String id = ((RemoteWebDriver) driver).getSessionId().toString();
        sessionId.set(id);

		return driver;
	}

	
	//***********************************************************************************************
//	public static WebDriver driver;

	/***************************************************************************
	 * Method Name			: launchBrowser()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini
	 * Purpose				: The purpose of this method is to launch the browser
	 ****************************************************************************
	 */
	
	public static WebDriver launchBrowser(String browser)
	{
		String browserType=null;
		try
		{
			log.info("The execution of the method launchBrowser started here ...");
			if(browser.equalsIgnoreCase("firefox"))
			{
				driver=new FirefoxDriver();
			}
			if(browser.equalsIgnoreCase("chrome"))
			{
				 final DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                 final ChromeOptions options = new ChromeOptions();
                 options.addArguments("chrome.switches","--disable-extensions");
                 options.addArguments("--start-maximized");
                 options.addArguments("chrome.switches","--disable-popup-blocking");
                 options.addArguments("disable-infobars");
                 Map<String, Object> prefs = new LinkedHashMap<String, Object>();
                 prefs.put("credentials_enable_service", Boolean.valueOf(false));
                 prefs.put("profile.password_manager_enabled", Boolean.valueOf(false));
                 
                 options.setExperimentalOption("prefs", prefs);
                 capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
                 driver=new ChromeDriver(options);
				driver.manage().deleteAllCookies();
			}
			else
			{
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\Driver\\IEDriverServer.exe");
				driver=new InternetExplorerDriver();
			}
				
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method launchBrowser "+e);
		}
		return driver;
	}
	
	
	
	/***************************************************************************
	 * Method Name			: nodeCnavigateount()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini
	 * Purpose				: The purpose of this method is to navigate to wine.com 
	 * 						  application
	 ****************************************************************************
	 */
	
	
	public static String navigate()
	{
		
		WebElement ele=null;
		try
		{
			log.info("The execution of the method navigate started here ...");
			if(state.equalsIgnoreCase("CA"))
			{
				XMLData.tagName("CA");
				
				
				
			}else if(state.equalsIgnoreCase("TX"))
			{
				XMLData.tagName("TX");
				
			}
			else
			{
				XMLData.tagName("MN");
			}
			
			if(environmentUrl.equalsIgnoreCase("q2m"))
			{
				
			//	driver.navigate().to("https://q2m.wine.com");
				driver.get(url.getProperty("q2m"));
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.manage().deleteAllCookies();
				UIFoundation.SelectObject(LoginPage.SelectState, "State");
				UIFoundation.waitFor(10L);
			}
			else if(environmentUrl.equalsIgnoreCase("prod"))
			{
				//driver.navigate().to("https://m.wine.com");
				driver.get(url.getProperty("prod"));
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				ele=driver.findElement(By.xpath("(//select[@class='formWrap_select searchBarForm_select'])[2]"));
				org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
				sel.selectByVisibleText(state); 
				UIFoundation.waitFor(15L);
			}
			else if(environmentUrl.equalsIgnoreCase("qwww"))
			{
				
				driver.get(url.getProperty("qwww"));
				driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(375, 667));
			//	Dimension d = new Dimension(375,667);
				//driver.manage().window().setSize(d);
				
				UIFoundation.SelectObject(LoginPage.SelectState, "State");
				UIFoundation.waitFor(5L);
			}
			else
			{
				driver.navigate().to(url.getProperty("dlm"));
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				
			}
			//ApplicationIndependent.waitFor(driver,By.xpath("(//input[@class='formWrap_input searchBarForm_input'])[2]"), "element","", 120);
//			UIFoundation.waitFor(3L);
//			WebElement element=driver.findElement(By.xpath("(//select[@name='state'])[2]"));
//			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(element);
//			String value = XMLData.getTestData(testScriptXMLTestDataFileName, "State", 1);
//			sel.selectByVisibleText(value);
		//	ApplicationIndependent.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(5L);
			
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			log.info("The execution of the method navigate ended here ...");
			return "Pass";
		
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method navigate "+e);
			return "Fail";

		}
	}
	
	/***************************************************************************
	 * Method Name			: navigateAWU()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini
	 * Purpose				: The purpose of this method is to navigate to AreWeUp 
	 * 						  application
	 ****************************************************************************
	 */
	public static String navigateAWU(WebDriver driver)
	{
		String environment=null;
		try
		{
			
			log.info("The execution of the method navigate started here ...");
			if(environmentUrl.equalsIgnoreCase("q2m"))
			{
				
				driver.navigate().to("https://qwww4.wine.com/v6/admin/login.aspx");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

			}
			else if(environmentUrl.equalsIgnoreCase("prod"))
			{
				
				driver.navigate().to("https://www.wine.com/v6/admin/login.aspx");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			}
			else
			{
				
				driver.navigate().to("https://d1m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				
			}
			driver.manage().window().maximize();
			log.info("The execution of the method navigate ended here ...");
			return "Pass";
		
		}catch(Exception e)
		{
			System.out.println("Unable to launch AreWeUP wine.com application");
			log.error("there is an exception arised during the execution of the method navigate "+e);
			return "Fail";

		}
	}
	/***************************************************************************
	 * Method Name			: nodeCnavigateount()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini
	 * Purpose				: The purpose of this method is to navigate to wine.com 
	 * 						  application
	 ****************************************************************************
	 */
	
	
	public static String navigateToApp(WebDriver driver)
	{
		
		WebElement ele=null;
		try
		{
			log.info("The execution of the method navigate started here ...");
				
			if(environmentUrl.equalsIgnoreCase("q2m"))
			{
				
			//	driver.navigate().to("https://q2m.wine.com");
				driver.get("https://q2m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.manage().deleteAllCookies();
				UIFoundation.waitFor(10L);
			}
			else if(environmentUrl.equalsIgnoreCase("prod"))
			{
				//driver.navigate().to("https://m.wine.com");
				driver.get("https://m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				ele=driver.findElement(By.xpath("(//select[@class='formWrap_select searchBarForm_select'])[2]"));
				org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
				sel.selectByVisibleText(state);
				UIFoundation.waitFor(15L);
			}
			else if(environmentUrl.equalsIgnoreCase("qwww"))
			{
				
				driver.get("http://qwww.wine.com");
				driver.manage().deleteAllCookies();
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

				/*ApplicationIndependent.SelectObject(driver, "SelectState", "State");
				ApplicationIndependent.waitFor(5L);*/
			}
			else
			{
				driver.navigate().to("https://d1m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				
			}
			

			Dimension d = new Dimension(375,667);
			driver.manage().window().setSize(d);
			//ApplicationIndependent.waitFor(driver,By.xpath("(//input[@class='formWrap_input searchBarForm_input'])[2]"), "element","", 120);
			UIFoundation.waitFor(3L);
			log.info("The execution of the method navigate ended here ...");
			return "Pass";
		
		}catch(Exception e)
		{
			System.out.println("Unable to launch wine.com application");
			log.error("there is an exception arised during the execution of the method navigate "+e);
			return "Fail";

		}
	}
	
	
	/***************************************************************************
	 * Method Name			: closeApplication()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini,
	 * Purpose				: The purpose of this method is to close the application
	 ****************************************************************************
	 */
	
	public static String closeApplication()
	{
		try
		{
			log.info("The execution of the method closeApplication started here ...");
			driver.close();
		//	driver.quit();
			log.info("The execution of the method closeApplication ended here ...");
			return "Pass";
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method closeApplication "+e);
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: nodeCnavigateount()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB,srini
	 * Purpose				: The purpose of this method is to navigate to wine.com 
	 * 						  application
	 ****************************************************************************
	 */
	
	
	public static String navigateWithoutSelectinState()
	{
		WebElement ele=null;
		try
		{
			log.info("The execution of the method navigate started here ...");
			if(state.equalsIgnoreCase("CA"))
			{
				XMLData.tagName("CARecord");
				
				
				
			}else if(state.equalsIgnoreCase("TX"))
			{
				XMLData.tagName("TXRecord");
				
			}
			else
			{
				XMLData.tagName("MNRecord");
			}
			
			if(environmentUrl.equalsIgnoreCase("q2m"))
			{
				
			//	driver.navigate().to("https://q2m.wine.com");
				driver.get("https://q2m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				driver.manage().deleteAllCookies();
				UIFoundation.SelectObject(LoginPage.SelectState, "State");
				UIFoundation.waitFor(10L);
			}
			else if(environmentUrl.equalsIgnoreCase("prod"))
			{
				//driver.navigate().to("https://m.wine.com");
				driver.get("https://m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				UIFoundation.waitFor(15L);
			}
			else if(environmentUrl.equalsIgnoreCase("qwww"))
			{
				
				driver.get("http://qwww.wine.com");
			//	driver.manage().deleteAllCookies();
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

				/*ApplicationIndependent.SelectObject(driver, "SelectState", "State");
				ApplicationIndependent.waitFor(5L);*/
			}
			else
			{
				driver.navigate().to("https://d1m.wine.com");
				driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
				
			}
			

			Dimension d = new Dimension(375,667);
			driver.manage().window().setSize(d);
			//ApplicationIndependent.waitFor(driver,By.xpath("(//input[@class='formWrap_input searchBarForm_input'])[2]"), "element","", 120);
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(LoginPage.toolTipComponet))
			{
				UIFoundation.clckObject(LoginPage.toolTipComponet);
				UIFoundation.waitFor(1L);
			}
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			log.info("The execution of the method navigate ended here ...");
			return "Pass";
		
		}catch(Exception e)
		{
			System.out.println("Unable to launch wine.com application");
			log.error("there is an exception arised during the execution of the method navigate "+e);
			return "Fail";

		}
	}
	
	/********************************************************...
	/***************************************************************************
		 * Method Name			: navigateToProduction()
		 * Created By			: Vishwanath Chavan
		 * Reviewed By			: Ramesh,KB,srini
		 * Purpose				: The purpose of this method is to navigate to wine.com 
		 * 						  application
		 ****************************************************************************
		 */
	
	
	public static String navigateToProduction(WebDriver driver)
	{

		
		WebElement ele=null;
		try
		{
			log.info("The execution of the method navigate started here ...");
			if(state.equalsIgnoreCase("CA"))
			{
				XMLData.tagName("CARecord");
				
				
				
			}else if(state.equalsIgnoreCase("TX"))
			{
				XMLData.tagName("TXRecord");
				
			}
			else
			{
				XMLData.tagName("MNRecord");
			}
			
			driver.get("https://www.wine.com/");
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			driver.manage().deleteAllCookies();
			
			Dimension d = new Dimension(375,667);
			driver.manage().window().setSize(d);
			//ApplicationIndependent.waitFor(driver,By.xpath("(//input[@class='formWrap_input searchBarForm_input'])[2]"), "element","", 120);
			UIFoundation.waitFor(3L);
			WebElement element=driver.findElement(By.xpath("(//select[@name='state'])[2]"));
			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(element);
			String value = XMLData.getTestData(testScriptXMLTestDataFileName, "State", 1);
			sel.selectByVisibleText(value);
		//	ApplicationIndependent.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(5L);
			driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
			log.info("The execution of the method navigate ended here ...");
			return "Pass";
		
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method navigate "+e);
			return "Fail";

		}
	}
}
