package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class UserSelectsToPayWithDifferentCard extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhara 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(2L);			
			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "preferredCardUN"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: userSelectsToPayWithDifferentCard()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String userSelectsToPayWithDifferentCard() {
	String objStatus=null;

	   String screenshotName = "Scenarios_diffPayCard_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method userSelectsToPayWithDifferentCard started here ...");
			UIFoundation.waitFor(4L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if(!UIFoundation.isSelected(FinalReviewPage.cboPreferredCardSelect)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboPreferredCardSelect));
				UIFoundation.waitFor(1L);
				
			}
			String nameBeforeSelPayWithCard=UIFoundation.getText(FinalReviewPage.lnkCreditCardHolderNameBeofreSelPay);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPayWithThisCardSel));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(4L);		
			if(UIFoundation.isDisplayed(FinalReviewPage.txtPaywithThisCardCvv)){
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaywithThisCardCvv, "CardCvid"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaywithThisCardSave);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardSave));
				UIFoundation.waitFor(2L);
			}

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
			UIFoundation.waitFor(1L);
			String nameAfterSelPayWithCard=UIFoundation.getText(FinalReviewPage.txtNameAfterSelPayWithCard);
			if(!UIFoundation.isSelected(FinalReviewPage.cboPreferredCardSelect) && (UIFoundation.isSelected(FinalReviewPage.btnPayWithThisCardSel) &&(nameBeforeSelPayWithCard.equalsIgnoreCase(nameAfterSelPayWithCard))) ){
				objStatus+=true;
				String objDetail="verify the preferred selection is not changed when user selects to pay with different card test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				
			}else{
				objStatus+=false;
				String objDetail="Verify the preferred selection is not changed when user selects to pay with different card test case is failed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method userSelectsToPayWithDifferentCard ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the preferred selection is not changed when user selects to pay with different card test case is failed");
				return "Fail";
			} else {
				System.out.println("verify the preferred selection is not changed when user selects to pay with different card test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method userSelectsToPayWithDifferentCard "
					+ e);
			objStatus+=false;
			String objDetail="Verify the preferred selection is not changed when user selects to pay with different card test case is failed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}	
}
