package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyBillingPhoneNumberIsRequiredField extends Mobile {


	/***************************************************************************************
	 * Method Name : AddnewcreditCardWithoutPhoneNumberInPaymentSection 
	 * Created By :  Chandrashekhar
	 * Reviewed By : Ramesh
	 * TM-4033
	 * @throws IOException
	 ***************************************************************************************
	 */


	public static String addnewcreditCardWithoutPhoneNumberInPaymentSection() {

		String ScreenshotName="AddnewcreditCardWithoutPhoneNumber.jpeg";
		String ScreenshotPath=System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		String objStatus= null;
		String ActualErrorMesage=null;
		try
		{
			log.info("Execution adding new credit card withput phone number started here");
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}	
			
			UIFoundation.waitFor(5L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.lnkRecipientChangeAddress))
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			if(UIFoundation.isElementDisplayed(FinalReviewPage.dwnChangeDeliveryDate))
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.lnkchangePayment)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Yr"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillinState, "militaryStateAA"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingZip, "militaryStateAACode"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtPhoneNumberReq);
			ActualErrorMesage=UIFoundation.getText(FinalReviewPage.txtPhoneNumberReq);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.txtPhoneNumberReq)) {
				System.out.println("Phone number required validation message is displayed :"+ActualErrorMesage);
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the error mesage validation for phonenumber in billing and info section", "Pass", "");
			}
			else {
				objStatus+=false;
				String objDetail="Phonenumber validation message is not displayed in billing and shipping info section.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
				 UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);

			}
			log.info("Execution method ended here");
			if(objStatus.contains("fail"))
			{
				System.out.println("Verification of error message failed in payment section");
				return "fail";

			}
			else
			{
				System.out.println("Verification of error message in payment section executed succesfully");
				return "pass";

			}
		}
		catch(Exception e) {
			log.error("Execution method aborted due to some error "+e);
			return "fail";

		}			

	}

	/***************************************************************************************
	 * Method Name : AddnewcreditCardWithoutPhoneNumberInPaymentSection
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * 
	 * @throws IOException
	 ***************************************************************************************
	 */



	public static String addnewcreditCardWithoutPhoneNumberInPaymentMethod() {

		String objStatus = null;
		String ScreenshotName="AddnewcreditCardWithoutPhoneNumberInPaymentMethod.jpeg";
		String ScreenshotPath=System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		String ActualErrorMesageInPayemntSection=null;
		try
		{
			log.info("Excution method started here");
     	
			objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgWinelogoR));
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgWinelogoR));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavAccTab));
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtPaymentMethods));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtAddNewCard));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtExpiryYear,"Yr"));
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.NewBillingSuite, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtNewBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(ThankYouPage.txtBillinState, "militaryStateAA"));
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtObj_BillingZip, "militaryStateAACode"));
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.btnBillingSave));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage.txtPhoneNumberError);
			ActualErrorMesageInPayemntSection=UIFoundation.getText(ThankYouPage.txtPhoneNumberError);
			if(UIFoundation.isElementDisplayed(ThankYouPage.txtPhoneNumberError)) {
				System.out.println("Phone number required validation message :"+ActualErrorMesageInPayemntSection);
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the error mesage validation for phonenumber in PayemntMethod", "Pass", "");
			}
			else {
				objStatus+=false;
				String objDetail="Phonenumber validation message is not displayed in PayemntMethod section.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);

			}
			log.info("Execution method ended here");
			if(objStatus.contains("false"))
			{
				System.out.println("Verification of error message failed in paymentmethod section failed");
				return "fail";

			}
			else
			{
				System.out.println("Verification of error message in paymentmethod section executed succesfully");
				return "pass";

			}
		}
		catch(Exception e) {
			log.error("Execution method aborted due to some error "+e);
			return "fail";

		}

	}
}
