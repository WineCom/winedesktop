package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class CalenderDispalyedSixMonth extends Mobile {

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(2L);			
			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "calenderMonthUN"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String calenderSixMnthInDeliverySection() {
	
	String objStatus=null;

	   String screenshotName = "Scenarios_CalenderSixMnth_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;
		ArrayList<String> monthCount=new ArrayList<String>();
		String monthName=null;
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.dwnChangeDeliveryDate))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
				UIFoundation.waitFor(2L);
			}
			monthName=UIFoundation.getText(FinalReviewPage.cboCalenderFirstMnth);
			monthCount.add(monthName);
			UIFoundation.waitFor(1L);
			monthName=UIFoundation.getText(FinalReviewPage.cboCalenderNxtMnth);
			monthCount.add(monthName);
			if(!UIFoundation.isDisplayed(FinalReviewPage.cboPreviousMnthIcon)){
				objStatus+=true;
				String objDetail="Previous icon is not displayed for current month.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Previous icon is displayed for current month";				
				UIFoundation.captureScreenShot( screenshotpath+screenshotName+"previousMnth", objDetail);
			}
			for(int i=0;i<6;i++){
				if(UIFoundation.isDisplayed(FinalReviewPage.cboNxtMnthIcon)){
					objStatus+=String.valueOf(UIFoundation.clckObject(FinalReviewPage.cboNxtMnthIcon));
					UIFoundation.waitFor(1L);
					monthName=UIFoundation.getText(FinalReviewPage.cboCalenderNxtMnth);
					monthCount.add(monthName);
					
				}else{
					break;
				}
			}
			if(monthCount.size()==6){
				objStatus+=true;
				String objDetail="calendar is displayed for 6 months in desktop view of delivery section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="calendar is not displayed for 6 months in desktop view of delivery section";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}

			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify  calendar is displayed for 6 months in desktop view of delivery section test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify  calendar is displayed for 6 months in desktop view of delivery section test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			objStatus+=false;
			String objDetail="Verify  calendar is displayed for 6 months in desktop view of delivery section is failed";			
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: AlaskaStateSaturdayCalenderSelect()
	 * Created By			: Chandrashekhara
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String AlaskaStateSaturdayCalenderSelect() {
	
	String objStatus=null;

	   String screenshotName = "AlaskaStateSaturdayCalenderSelect_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		
		String dayName=null;
		try {
			log.info("The execution of the method AlaskaStateSaturdayCalenderSelect started here ...");
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(10L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientChangeAddress)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			UIFoundation.waitFor(1L);}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			UIFoundation.waitFor(2L);						
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressAK"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "CityAK"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "StateAK"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipentZipCode, "ZipAK"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.btnAddressContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnAddressContinue));
			UIFoundation.waitFor(1L);}
			List<WebElement> dayCount=	driver.findElements(By.xpath("//span[@tabindex='-1']"));
			System.out.println("Day Count : "+dayCount.size());
		//	UIFoundation.clckObject(driver, "SelectShippingMethod");
			if(dayCount.size()>=8) {
				objStatus+=true;
				String objDetail="Saturday cannot be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Saturday cannot be selected");
			}else {
				objStatus+=false;
				String objDetail="Saturday can be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				System.out.println("Saturday can be selected");
			}

			log.info("The execution of the method AlaskaStateSaturdayCalenderSelect ended here ...");
			if ( objStatus.contains("false")) {
				String objDetail="Verify  AlaskaStateSaturdayCalenderSelect in desktop view of delivery section test case is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
			} else {
				String objDetail="Verify  AlaskaStateSaturdayCalenderSelect in desktop view of delivery section test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method AlaskaStateSaturdayCalenderSelect "
					+ e);
			objStatus+=false;
			String objDetail="Verify  AlaskaStateSaturdayCalenderSelect in desktop view of delivery section is failed";
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: HawaiiStateSaturdayCalenderSelect()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String HawaiiStateSaturdayCalenderSelect() {
	
	String objStatus=null;

	   String screenshotName = "HawaiiStateSaturdayCalenderSelect_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		
		String dayName=null;
		try {
			log.info("The execution of the method HawaiiStateSaturdayCalenderSelect started here ...");
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(10L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientChangeAddress)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			UIFoundation.waitFor(1L);}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressHI"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "CityHI"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "StateHI"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtReceipentZipCode, "ZipHI"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnAddressContinue));
			UIFoundation.waitFor(3L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.btnAddressContinue)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnAddressContinue));
			UIFoundation.waitFor(1L);}
			List<WebElement> dayCount=	driver.findElements(By.xpath("//span[@tabindex='-1']"));
			System.out.println("Day Count : "+dayCount.size());
		//	UIFoundation.clckObject(driver, "SelectShippingMethod");
			if(dayCount.size()>=8) {
				objStatus+=true;
				String objDetail="Saturday cannot be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Saturday cannot be selected");
			}else {
				objStatus+=false;
				String objDetail="Saturday can be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				System.out.println("Saturday can be selected");
			}

			log.info("The execution of the method HawaiiStateSaturdayCalenderSelect ended here ...");
			if ( objStatus.contains("false")) {
				String objDetail="Verify  HawaiiStateSaturdayCalenderSelect in desktop view of delivery section test case is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
			} else {
				String objDetail="Verify  HawaiiStateSaturdayCalenderSelect in desktop view of delivery section test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method HawaiiStateSaturdayCalenderSelect "
					+ e);
			objStatus+=false;
			String objDetail="Verify  HawaiiStateSaturdayCalenderSelect in desktop view of delivery section is failed";
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	
}
