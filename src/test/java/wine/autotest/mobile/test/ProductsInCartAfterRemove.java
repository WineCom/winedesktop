
package wine.autotest.mobile.test;

import java.util.ArrayList;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;


public class ProductsInCartAfterRemove extends Mobile {
	static ArrayList<String> expectedItemsName=new ArrayList<String>();
	static ArrayList<String> actualItemsName=new ArrayList<String>();
	static String products1=null;
	static String products2=null;
	static String products3=null;
	static String cartCountBeforeLogout=null;
	static int cartCountBefreLogout=0;
	static String cartCountAfterLogin=null;
	static int cartCountAftrLogin=0;
	
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart() {
		
		
		String objStatus=null;
		try {
			log.info("The execution of the method addProductsToCart started here ...");
			driver.navigate().refresh();
		//	UIFoundation.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSrt);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCartCount));
			log.info("The execution of the method addProductsToCart ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotalBefore=null;
		String subTotalAfter=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String secondProductPrice=null;
		String thirdProductPrice=null;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before removing products from the cart===============");
			subTotalBefore=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalBefore);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			secondProductPrice=UIFoundation.getText(CartPage.spnSecondProductPrice);
			thirdProductPrice=UIFoundation.getText(CartPage.spnThirdProductPrice);
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.spnRemoveThirdProduct).contains("Fail"))
			{
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "RemoveThirdProduct");
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveThirdProduct));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));
				UIFoundation.waitFor(5L);
			}
			if(!UIFoundation.getText(CartPage.spnRemoveSecondProduct).contains("Fail"))
			{
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "RemoveSecondProduct");
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveSecondProduct));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));
				UIFoundation.waitFor(5L);
			}
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveThirdProduct));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnObjRemove));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveSecondProduct));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnObjRemove));*/
			UIFoundation.waitFor(3L);
			subTotalAfter=UIFoundation.getText(CartPage.spnSubtotal);
		//	ApplicationDependent.removerProductPrice(driver, subTotalBefore, subTotalAfter, secondProductPrice, thirdProductPrice);
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary after removing products from the cart===============");
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalAfter);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
					
			////////////////////////////////////////////////////////////////
			System.out.println("============Below Products are available in the CART before logout ===================================");
			if(!UIFoundation.getText(ListPage.btnFirstProductToCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductToCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			if(!UIFoundation.getText(ListPage.btnSecondProductToCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductToCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			if(!UIFoundation.getText(ListPage.btnThirdProductToCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductToCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			/////////////////////////////////////////////////////////
			cartCountBeforeLogout=UIFoundation.getText(ListPage.btnCartCount);
			cartCountBefreLogout=Integer.parseInt(cartCountBeforeLogout);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.scrollDownOrUpToParticularElement(LoginPage.MainNavAccountTab);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignoutLink));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method captureOrdersummary started here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: productsAvailableInTheCart()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String productsAvailableInTheCart()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_ListNotMatched_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
			

		try
		{
			log.info("The execution of the method productsAvailableInTheCartBeforeLogout started here ...");
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			///////////////////////////////////////////////////////////////
			System.out.println("============Below Products are available in the CART after login ===================================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
				actualItemsName.add(products1);
			}
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);
				actualItemsName.add(products2);
			}
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);
				actualItemsName.add(products3);
			}
			if(expectedItemsName.containsAll(actualItemsName))
			{
				System.out.println("Expected products and actual products in cart are matched" );
				objStatus+=true;
			    String objDetail="Expected products and actual products in cart are matched";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else
			{
				objStatus+=false;
			    String objDetail="Expected products and actual products in cart does not match";
		//	    UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				//ReportUtil.addTestStepsDetails("Expected products and actual products in cart does not match", "", "");
			}
			cartCountAfterLogin=UIFoundation.getText(ListPage.btnCartCount);
			cartCountAftrLogin=Integer.parseInt(cartCountAfterLogin);
			if(cartCountBefreLogout==cartCountAftrLogin)
			{
				System.out.println("Cart count before logout and cart count after login are matched" );
				objStatus+=true;
			    String objDetail="Cart count before logout and cart count after login are matched";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else
			{
				objStatus+=false;
			    String objDetail="Cart count before logout and cart count after login does not match";
			//    UIFoundation.captureScreenShot(driver, screenshotpath+"cart", objDetail);
				//ReportUtil.addTestStepsDetails("Cart count before logout and cart count after login does not match", "", "");
			}
					
			/////////////////////////////////////////////////////////////////////////////////////
			
			log.info("The execution of the method productsAvailableInTheCartBeforeLogout started here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the products remain in the cart after removing few products from the cart test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the products remain in the cart after removing few products from the cart test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productsAvailableInTheCartBeforeLogout "+ e);
			return "Fail";
		}
	}
	

}
