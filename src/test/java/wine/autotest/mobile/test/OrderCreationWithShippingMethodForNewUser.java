package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithShippingMethodForNewUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : shippingDetails()
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	String objStatus = null;
			
	public static String shippingDetails() {
		String objStatus = null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShippingSaving, "Invisible", "", 50);
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
		//	UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage. "VerifyContinueButtonShipRecpt");
		//	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "VerifyContinueButtonShipRecpt"));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			/*objStatus+=String.valueOf(UIFoundation.selectValueByValue(FinalReviewPage. "SelectShippingMethod", "3"));
			UIFoundation.waitFor(2L);
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage. "SelectShippingMethod");
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "DeliveryContinue"));*/
			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSelectShippingMethodOption));
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			String stewardshipSave=UIFoundation.getText(FinalReviewPage.spnstewardShipSavePrice);
			String shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			/*if(!stewardshipSave.equalsIgnoreCase(shippingAndHandling))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Savings total in the Stewardship upsell does not match the price of 'Shipping & Handling' charges.", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Savings total in the Stewardship upsell is matched the price of 'Shipping & Handling' charges.";
				UIFoundation.captureScreenShot(driver, screenshotpath+"warningMsgShip", objDetail);
			}*/
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : addNewCreditCard()
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage. "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentSaveButton"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. "PaymentContinue"));
			UIFoundation.waitFor(5L);*/
			int actualShippingMethodCharges= Integer.parseInt(shippingAndHandling.replaceAll("[^0-9]",""));
			if(actualShippingMethodCharges==expectedShippingMethodCharges)
			{
				objStatus+=true;
				String objDetail="Shipping method is applied for the order creation";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println(Shipping+" Shipping method is applied for the order creation");
				
			}else
			{
				objStatus+=false;
		    	String objDetail="Shipping method is not applied for the order creation";
		    	UIFoundation.captureScreenShot( screenshotpath+"shippingMethod", objDetail);
		    	System.out.println("Shipping method is not applied for the order creation");
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			 }
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with selecting the shipping method in delivery section for new user test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with selecting the shipping method in delivery section for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
}
