package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyDefaultGiftBagButtonIsUnchecked extends Mobile {


	/***************************************************************************
	 * Method Name : VerifyAddToMyWineFunctionality(NFP-4253) 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     : TM-4128
	 ****************************************************************************
	 */

	public static String verifyNoGiftBagButtonIsUnchecked() {

		String ScreenshotName = "VerifyNoGiftBagButtonIsUnchecked.jpeg";
		String addToCart1, addToCart2,addToCart3;
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;
		String objStatus = null;

		try {
			log.info("Verify No Gift Bag Button Is Unchecked method started here.....");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.btnFirstProductToCart);
			} else {
				addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
				if (addToCart2.contains("Add to Cart")) {
					UIFoundation.waitFor(1L);
					UIFoundation.clckObject(ListPage.btnSecondProductToCart);
				}
			}			
			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.btnThirdProductToCart);
			}						
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);

			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(3L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkRecipientChangeAddress)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			}
			WebElement gift = driver.findElement(By.xpath(
					"//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if (!gift.isSelected()) {
				UIFoundation.clickObject(FinalReviewPage.txtRecipientGiftCheckbox);
			}
			WebElement noGiftBag = driver.findElement(By.xpath(
					"//main/section[@class='checkoutMainContent']//section[@class='giftWrapSection']/ul/li/div/div//ul/li[4]//span/input"));
			if (!noGiftBag.isSelected()) {
				objStatus += true;
				String objDetail = "No Gift Bag radio button is not selected by default";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "No Gift Bag radio is selected by default";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("Verify No Gift Bag Button Is Unchecked method ended here.....");
			if (objStatus.contains("false")) {

				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case is failed");

				return "Fail";
			} else {
				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}

	public static String verifyGiftbagName() {

		String objStatus = null;
		String ScreenshotName = "VerifyGiftbagName.jpeg";
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;

		/***************************************************************************
		 * Method Name : VerifyGiftbagName 
		 * Created By  : Chandrashekhar
		 * Reviewed By : Ramesh. 
		 * Purpose     : TM-4128
		 ****************************************************************************
		 */
		try {
			log.info("VerifyGiftbagName method started here......");

			String giftBagDesc = UIFoundation.getText(FinalReviewPage.imgGiftBagNames1);
			String actGiftBagDesc = verifyexpectedresult.actGiftBagName1;			
			if (giftBagDesc.equals(actGiftBagDesc)) {
				System.out.println("Gift Bag Name for $3.99 " + giftBagDesc);
				objStatus += true;
				String objDetail = "Gift Bag Name for $3.99 is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Gift Bag Name for $3.99 is wrong";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}

			giftBagDesc = UIFoundation.getText(FinalReviewPage.imgGiftBagNames2);
			actGiftBagDesc = verifyexpectedresult.actGiftBagName2;
			
			if (giftBagDesc.equals(actGiftBagDesc)) {
				System.out.println("Gift Bag Name for $6.99 " + giftBagDesc);
				objStatus += true;
				String objDetail = "Gift Bag Name for $6.99 is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Gift Bag Name for $6.99 is wrong";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}

			giftBagDesc = UIFoundation.getText(FinalReviewPage.imgGiftBagNames3);
			actGiftBagDesc = verifyexpectedresult.actGiftBagName3;			
			if (giftBagDesc.equals(actGiftBagDesc)) {
				System.out.println("Gift Bag Name for $9.99 " + giftBagDesc);
				objStatus += true;
				String objDetail = "Gift Bag Name for $9.99 is correct";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Gift Bag Name for $9.99 is wrong";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);				
			}
			log.info("Verify No Gift Bag Button Is Unchecked method ended here.....");
			if (objStatus.contains("false")) {

				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case is failed");

				return "Fail";
			} else {
				System.out.println("VerifyNoGiftBagButtonIsUnchecked test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}
}


