package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithSpecialProductsForExistingUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: specialProducts()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String specialProducts()
	{
		String objStatus=null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.linSpecialsProducts));
			UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."GreenWine"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.linsmallprod));			
			UIFoundation.waitFor(3L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage."FirstProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnSecondProductToCart);
			UIFoundation.waitFor(1L);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage."ThirdProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage."FourthProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage."FifthProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			}
			UIFoundation.waitFor(3L);
			
	
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String totalBeforeTax=null;
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			

           if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.signInLinkAccount));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			
/*		if(UIFoundation.isDisplayed(FinalReviewPage."shippingAddressEdit")){
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."shippingAddressEdit"));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(FinalReviewPage."StreetAddress");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."StreetAddress", "Address1"));
			UIFoundation.clearField(FinalReviewPage."City");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage."shipState", "State"));
			UIFoundation.javaScriptClick(FinalReviewPage."shipState");
			UIFoundation.clearField(FinalReviewPage."ZipCode");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."ZipCode", "ZipCode"));
			UIFoundation.clearField(FinalReviewPage."PhoneNum");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage."PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."shippingAddressSave"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage."VerifyContinueButton"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."RecipientContinue"));
			UIFoundation.waitFor(5L);
			}*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				UIFoundation.waitFor(1L);
			}
			
         //   objStatus+=String.valueOf(ApplicationDependent.existingUserOrderCreation(driver));
			
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentCheckoutEdit"));
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."AddPayment"));
			UIFoundation.waitFor(1L);*/
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)){
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			}
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage."PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."PaymentSaveButton"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage."PaymentContinue"));*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			/*if(!UIFoundation.isDisplayed(FinalReviewPage."MainNavButton")){
				driver.navigate().refresh();
				UIFoundation.waitFor(2L);
			}*/
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."MainNavButton"));
			//UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage."MainNavTabSignOut");
			//UIFoundation.waitFor(1L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."MainNavAccountTab"));
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage."YourOrder"));
			UIFoundation.waitFor(3L);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			System.out.println(orderNum);
		//	System.out.println(UIFoundation.getText(driver,"OrderDate"));
			UIFoundation.getOrderNumber(orderNum);
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Order creation with special products for existing user  test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Order creation with special products for existing user test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}

	
	
	
}
