
package wine.autotest.mobile.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ShipSoonCheckbox extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: ShipSoonCheckbox()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String shipSoonCheckbox()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__shipSoonChk.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;
				
		
		try
		{
			
			log.info("The execution of the method shipSoonCheckbox started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkMoreFilters));
			if(!UIFoundation.isCheckBoxSelected(ListPage.dwnShipSoonCheckBox)){
				objStatus+=true;
				String objDetail="By default the check box is unchecked.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else{
				objStatus+=false;
				String objDetail="By default the check box is not unchecked.";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);				
			}
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.dwnShipSoonCheckBox));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnSipTodayFirstProd));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnShipTodaySecoProd));
			log.info("The execution of the method shipSoonCheckbox ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify 'Ships Soon' check box is added on list page test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify 'Ships Soon' check box is added on list page test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "+ e);
			return "Fail";
			
		}
	}
	
}
