package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class PromoBannerBehaviourForDryState extends Mobile {
	

	/***************************************************************************
	 * Method Name			: PromoBannerBehaviourForDryState()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String promoBannerBehaviourForDryState()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_promoBannerBehaviourForDryState.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
			String stateName=null;
		try
		{
			log.info("The execution of the method promoBannerBehaviourForDryState started here ...");
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(10L);
			String expectedPromoBanner = verifyexpectedresult.dryStatePromoBar;
			String actualPromoBanner=UIFoundation.getText(ListPage.TxtPromoBannerDry);
			stateName=actualPromoBanner.substring(49);
			if(actualPromoBanner.contains(expectedPromoBanner))
			{
				objStatus+=true;
				String objDetail="Due to state regulations, we cannot ship wine to" +stateName+". message is displayed.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Due to state regulations, we cannot ship wine to" +stateName+". message is not displayed";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}


			log.info("The execution of the method promoBannerBehaviourForDryState ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method superscriptInProductPrice "+ e);
			return "Fail";
			
		}
	}	
	
}
