package wine.autotest.mobile.test;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class UpdateAccountInfoWithOutDate extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L); 
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: updateAccountInfoWithOutDate()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String updateAccountInfoWithOutDate()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_Stewardship.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			log.info("The execution of the method updateAccountInfoWithOutDate started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));	
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkAccountInfo));
			UIFoundation.webDriverWaitForElement(LoginPage.btnSaveAccountInfo, "Clickable", "", 50);
			UIFoundation.clearField(LoginPage.txtClearUpdateAccountLastName);
			UIFoundation.waitFor(1L);
		//	UIFoundation.clckObject(LoginPage.txtUpdateAccountemail);
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.txtEnterUpdateAccountLastName,"lastName"));
			UIFoundation.waitFor(2L);			
			UIFoundation.clearField(LoginPage.txtBrthMonth);
			UIFoundation.clearField(LoginPage.txtBrthDay);
			UIFoundation.clearField(LoginPage.txtBrthYear);
			if(UIFoundation.isEnabled(LoginPage.btnSaveButtonAccountInfo))
			{
				objStatus+=true;
				String objDetail="User is not able to update Account info with out Date ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Verify user is not able to update Account info with out Date test case is failed";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify user is not able to update Account info with out Date test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify user is not able to update Account info with out Date test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method updateAccountInfoWithOutDate "+ e);
			return "Fail";
			
		}
	}	

}
