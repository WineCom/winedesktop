package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class HandelsXBottelsPerCustomer extends Mobile {

	
	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addLimitProdTocrt() {
        String objStatus = null;
        String addToCart1 = null;
           String screenshotName = "Scenarios_ProductLimit_Screenshot.jpeg";
           String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
        try {
               UIFoundation.waitFor(8L);
              driver.get("https://qwww.wine.com/product/chateau-mouton-rothschild-2016/202479");           
               UIFoundation.waitFor(4L);       
                         
               objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnAllLinkPlusIcon));
               UIFoundation.waitFor(6L);
               
               if(UIFoundation.isDisplayed(CartPage.lnkVintageDetail))
               {
            	   objStatus+=true;
                   System.out.println("VintageDetail is displayed when clicking on + icon");
                   String objDetail="VintageDetail is displayed when clicking on + icon";
                   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
            	   
               }else{
            	   objStatus+=false;
                   String objDetail="VintageDetail is not displayed when clicking on + icon";
                   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                   System.err.println("VintageDetail is not displayed when clicking on + icon");
               }
               objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnAllLinkCloseIcon));
               UIFoundation.waitFor(6L);
               
               int prodLimit=Integer.parseInt(UIFoundation.getText(CartPage.cboProductItemLimitQuantity));
               UIFoundation.waitFor(4L);
               int prodLimitInDropDown=UIFoundation.getLastOptionFromDropDown(CartPage.cboProductItemLimitQuantity);
               if(prodLimit==prodLimitInDropDown)
               {
                     objStatus+=true;
                     System.out.println("Product limit test case is executed successfully");
                     String objDetail="Product limit test case is executed successfully";
                     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
               }
               else
               {
                     objStatus+=false;
                     String objDetail="Product limit test case is failed";
                     UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                     System.err.println("Product limit test case is failed");
               }
               UIFoundation.waitFor(6L);
               objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(CartPage.lnkAllVintagesLink));
               if(UIFoundation.isDisplayed(CartPage.lnkAllVintagesLink))
               {
            	   objStatus+=true;
                   System.out.println("All Vintages link is displayed");
                   String objDetail="All Vintages link is displayed";
                   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
            	   
               }else{
            	   objStatus+=false;
                   String objDetail="All Vintages link is not displayed";
                   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                   System.err.println("All Vintages link is not displayed");
               }
               
               objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkAllVintagesLink));
               UIFoundation.waitFor(6L);
               
               if(UIFoundation.isDisplayed(CartPage.lnkVintageDetail))
               {
            	   objStatus+=true;
                   System.out.println("VintageDetail is displayed when clicking on AllVintages link");
                   String objDetail="VintageDetail is displayed when clicking on AllVintages link";
                   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
            	   
               }else{
            	   objStatus+=false;
                   String objDetail="VintageDetail is not displayed when clicking on AllVintages link";
                   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                   System.err.println("VintageDetail is not displayed when clicking on AllVintages link");
               }
              
                           
               if (objStatus.contains("false")) {

                     return "Fail";
               } else {

                     return "Pass";
               }

        } catch (Exception e) {

               return "Fail";
        }

	}
}

