package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class UserProfileCreationWithMyWine extends Mobile {
	

	static String expectedState=null;
	static String actualState=null;
	/***************************************************************************
	 * Method Name : userProfileCreation() 
	 * Created By :  Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to Create
	 * new user account
	 ****************************************************************************
	 */

	public static String userProfileCreation() {
		String objStatus = null;
		String isElement=null;
		try {
			log.info("The execution of method create Account started here");
			WebElement ele = driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel = new Select(ele);
			expectedState = sel.getFirstSelectedOption().getText();
			System.out.println("expected state:" + expectedState);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnObjMyWine));
			UIFoundation.waitFor(3L);
				
			if(UIFoundation.isDisplayed(LoginPage.createAccountLnk))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.createAccountLnk));
			}
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email, "email"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.Password, "password"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(6L);
			isElement=String.valueOf(UIFoundation.isDisplayed(LoginPage.CreateAccountButton));
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false") || isElement.equalsIgnoreCase("true")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;

		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.btnsignInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnsignInLinkAccount));
			}
			
			objStatus+=String.valueOf(UIFoundation.newUsersetObject(LoginPage.LoginEmail,userEmail));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			log.info("The execution of the method login ended here ...");
			
			if(expectedState.equalsIgnoreCase(actualState))
			{
				objStatus+=true;
			    String objDetail="Expected state name and actual state name matched";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    System.out.println("Expected satte name and actual state name matched");
			}else{
				
				 objStatus+=false;
				 String objDetail="Expected state name and actual state name doest not match";		
				  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}



}
