package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class UpdateShippingAddressInAddressBook extends Mobile {
	

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 

			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name : UpdateShippingAddressInAddressBook() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose : 
	 * TM-2224,2159
	 ****************************************************************************
	 */

	public static String updateShippingAddressInAddressBook() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;

		try {
			log.info("The execution of the method updateShippingAddressInAddressBook started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtAddressBook));
			UIFoundation.webDriverWaitForElement(ThankYouPage.btnUserDataCardEdit, "Clickable", "", 50);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.btnUserDataCardEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(ThankYouPage.btnUserDataFirstNameClear);
			UIFoundation.waitFor(1L);
		//	UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage. "userPhoneNumber");
			UIFoundation.waitFor(1L);
		//	objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage. "userPhoneNumber"));
			UIFoundation.waitFor(1L);
		//	UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage. "userDataFirstName");
			UIFoundation.waitFor(1L);
			String expectedEditedName=UIFoundation.setObjectEditShippingAddress(ThankYouPage.btnUserDataFirstNameClear, "firstName");
			expectedEditedName=expectedEditedName.replaceAll(" ", "");
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.btnUserDataSave));
			UIFoundation.waitFor(1L);
		//	objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage. "VerifyContinueButton"));
		//	UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ThankYouPage.btnVerifyContinueButtonShipRecpt))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnVerifyContinueButtonShipRecpt));
				UIFoundation.webDriverWaitForElement(ThankYouPage.btnVerifyContinueButtonShipRecpt, "Invisible", "", 50);
			}
			String actualEditedName=UIFoundation.getText(ThankYouPage.txtExpectedFirstName);
			actualEditedName=actualEditedName.replaceAll(" ", "");
			if(actualEditedName.equalsIgnoreCase(expectedEditedName))
			{
				  objStatus+=true;
			      String objDetail="Verified that user is able to update shipping address in Address Book";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified that user is able to update shipping address in Address Book");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify that user is able to update shipping address in Address Book  is failed";
				   UIFoundation.captureScreenShot( screenshotpath+screenshotName+"addressBook", objDetail);
			       System.err.println("Verify that user is able to update shipping address in Address Book is failed ");
			}
			
			log.info("The execution of the method updateShippingAddressInAddressBook ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method updateShippingAddressInAddressBook "
					+ e);
			return "Fail";
		}
	}
		
}
