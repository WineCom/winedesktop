package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class WireUpCalender extends Mobile {	

	
	/***************************************************************************
	 * Method Name			: wireUpCalender()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String wireUpCalender() {
	
	String objStatus=null;

	   String screenshotName = "Scenarios_wireUpCalender_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method wireUpCalender started here ...");
			UIFoundation.waitFor(4L);			
			String expectedBgColorCurrentDate = verifyexpectedresult.currentDateBgColor;
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(8L);
	//		objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtChangeDeliveryDate))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtChangeDeliveryDate));
				UIFoundation.waitFor(2L);
			}
			String deliveryHeaderDate=UIFoundation.getText(FinalReviewPage.txtDeliveryHeaderDate);
			deliveryHeaderDate=deliveryHeaderDate.replaceAll("[^0-9]", "");
			int deliveryHeadrDate=Integer.parseInt(deliveryHeaderDate);
			String calenderTodayDate=UIFoundation.getText(FinalReviewPage.btnCurrentDate);
			int todayCalDate=Integer.parseInt(calenderTodayDate);
			WebElement ele=UIFoundation.webelemnt(FinalReviewPage.btnCurrentDate);
			String ActualBgColorCurrentDate=UIFoundation.getColor(FinalReviewPage.btnCurrentDate);
			if((deliveryHeadrDate==todayCalDate) && (ele.isEnabled()) && (expectedBgColorCurrentDate.equalsIgnoreCase(ActualBgColorCurrentDate))){
				objStatus+=true;
				String objDetail="Available days depend on the Shipping Method, are shown in a distinct color, and are selectable";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Available days depend on the Shipping Method, are shown in a distinct color, and are selectable");
			}else{
				objStatus+=false;
				String objDetail="Available days depend on the Shipping Method, are shown in a distinct color, and are not selectable";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Available days depend on the Shipping Method, are shown in a distinct color, and are not selectable");
			}
			
			WebElement ele1=UIFoundation.webelemnt(FinalReviewPage.btnUnavailableDate);			
			String expectedBgColorPreviousDate = verifyexpectedresult.previousDateBgColor;
			String actualdBgColorPreviousDate=UIFoundation.getColor(FinalReviewPage.btnUnavailableDate);
	//		boolean status=UIFoundation.isSelected(FinalReviewPage. "unavailableDate");
			if((ele1.isEnabled())){
				objStatus+=true;
				String objDetail="Unavailable dates are cannot be selected";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Unavailable dates are cannot be selected");
			}else{
				objStatus+=false;
				String objDetail="Unavailable dates are can be selected";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("Unavailable dates are  can be selected");
			}
			if(expectedBgColorPreviousDate.equalsIgnoreCase(actualdBgColorPreviousDate)){
				objStatus+=true;
				String objDetail="Unavailable dates are  greyed out";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Unavailable dates are greyed out");
			}else{
				objStatus+=false;
				String objDetail="Unavailable dates are not greyed out";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("Unavailable dates are not greyed out");
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnNextAvailableDate));
			UIFoundation.waitFor(2L);
			String deliveryHeaderDateNext=UIFoundation.getText(FinalReviewPage.txtDeliveryHeaderDate);
			deliveryHeaderDateNext=deliveryHeaderDateNext.replaceAll("[^0-9]", "");
			int deliveryHeaderDateNxt=Integer.parseInt(deliveryHeaderDateNext);
		//	String calenderNextDate=UIFoundation.getText(FinalReviewPage. "currentDate");
			String calenderNextDate=UIFoundation.getText(FinalReviewPage.btnNextcurrentDate);			
			int calenderNxtDate=Integer.parseInt(calenderNextDate);
			if(deliveryHeaderDateNxt==calenderNxtDate){
				objStatus+=true;
				String objDetail="If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are changed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are changed");
			}else{
				objStatus+=false;
				String objDetail="If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are not changed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("If the user selects a day from the Calendar, then changes is  Shipping Method, Estimated Delivery Day date are not changed");
			}

			log.info("The execution of the method wireUpCalender ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Wire up calendar test case is failed");
				return "Fail";
			} else {
				System.out.println("Wire up calendar test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			objStatus+=false;
			String objDetail="Verify  calendar is displayed for 6 months in desktop view of delivery section is failed";
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}	
}
