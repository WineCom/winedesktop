package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyProductsAreListedInAlphabeticalOrder extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyProductsAreListedInAlphabeticalOrder()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * TM-549
	 ****************************************************************************
	 */
	public static boolean verifyProductsAreListedInAlphabeticalOrder()
	{
		WebElement ele=null;
		String objStatus=null;
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> arrayListYr=new ArrayList<String>();
		   boolean isSorted = true;
			String screenshotName = "Scenarios_AlphabeticalOrder_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				arrayList.add(products1.replaceAll("[0-9]", ""));
				arrayListYr.add(products1.replaceAll("[^0-9]", ""));
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				arrayList.add(products2.replaceAll("[0-9]", ""));
				arrayListYr.add(products2.replaceAll("[^0-9]", ""));
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				arrayList.add(products3.replaceAll("[0-9]", ""));
				arrayListYr.add(products3.replaceAll("[^0-9]", ""));
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				arrayListYr.add(products4.replaceAll("[^0-9]", ""));
				arrayList.add(products4.replaceAll("[0-9]", ""));
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				arrayList.add(products5.replaceAll("[0-9]", ""));
				arrayListYr.add(products5.replaceAll("[^0-9]", ""));
				System.out.println("5) "+products5);

			}
		
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) > 0) && (arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	 objStatus+=true;
				      String objDetail="Products are listed in alphabetical order";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    	  System.out.println("Products are listed in alphabetical order");
					return true;
			    }
			    else
			    {
			    	objStatus+=false;
				       String objDetail="Products names are not sorted in alphabetical order";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				     	System.err.println("Products names are not sorted in alphabetical order");
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}	
	}
}
