package wine.autotest.mobile.test;


import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class ValidateTheSubscriptionFlowForLocalPickupAddress extends Mobile {	

	
/******************************************************************************************
 * Method Name : 
 * Created By  : Chandrashekhar
 * Reviewed By : Ramesh
 * Purpose     : This method validates local pickup address with picked up wine
 * 
 ******************************************************************************************/
	
	public static String verifyLocalPickUpFlowForPickedUpWine() {
		
	String objStatus = null;
	String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
	try {
		log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
		UIFoundation.waitFor(2L);
		if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
			objStatus+=true;
			String objDetail="Subscription flow 'Recipient' page is displayed";
			ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
		}else{
			objStatus+=false;
			String objDetail="Subscription flow 'Recipient' page should is not displayed";
			 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		}
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
		UIFoundation.waitFor(10L);
		//objStatus+=String.valueOf(UIFoundation.clickObject(OMSPage. "VerifyFedexAddress"));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocarion));
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
		objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtFedexSaveButton));
		UIFoundation.waitFor(15L);
		if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
		{
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
			//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
		}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkAcceptTermsAndConditions));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
			if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
				objStatus+=true;
				String objDetail="User is successfully enrolled to compass and Glad to meet you screen displayed.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}	
}
