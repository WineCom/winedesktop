package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ABVandFoundLowerPrice extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: aBVandFoundLowerPrice()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String aBVandFoundLowerPrice()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_Abv.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			log.info("The execution of the method aBVandFoundLowerPrice started here ...");
			UIFoundation.waitFor(2L);
			driver.get("https://qwww.wine.com/product/tenuta-san-guido-sassicaia-2016/519066");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAbv))
			{
				objStatus+=true;
				String objDetail="'ABV' text is displayed beside the product icons in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="'ABV' text is not displayed beside the product icons in PIP";					
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"abv", objDetail);
			}
			
			UIFoundation.waitFor(2L);
			log.info("The execution of the method aBVandFoundLowerPrice ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method aBVandFoundLowerPrice "+ e);
			return "Fail";
			
		}
	}
}
