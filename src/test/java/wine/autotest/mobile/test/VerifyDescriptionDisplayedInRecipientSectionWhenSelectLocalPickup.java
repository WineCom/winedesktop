package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup extends Mobile {
	

	static String expectedState=null;
	static String actualState=null;
	/***************************************************************************
	 * Method Name : offerPageDisplayed() 
	 * Created By : Chandrashekhar 
	 * Reviewed By : Ramesh,KB
	 * Purpose : The purpose of this method is to Create.
	 ****************************************************************************
	 */

	public static String offerPageDisplayed() {
		String objStatus = null;
		String isElement=null;
		try {
			log.info("The execution of method create Account started here");
			UIFoundation.waitFor(1L);
			
			driver.get("https://qwww.wine.com/picked");	
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.cmdSignUpCompus));
			UIFoundation.waitFor(3L);
			
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyFirsrPickedPage));			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkNoVoice));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifySeconePickedPage));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkRedOnly));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyThirdPickedPage));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtPickedWrapNextThirdPage);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.txtPickedWrapNextThirdPage));	
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtVerifyForthPickedPage));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextThirdPage));
			UIFoundation.waitFor(1L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(PickedPage.lblNotVery));
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedWrapNext));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.txtPickedWrapNextThirdPage));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.cmdPickedNext));
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFirstname, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtLastname, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(PickedPage.txtEmail,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPassword, "accPassword"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnCreateAccount));
			
			UIFoundation.waitFor(1L);		
			
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false") ) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
/*	public static String login(WebDriver driver)
	{
		String objStatus=null;

		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavAccountTab"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavSignIn"));
			if(UIFoundation.isDisplayed(driver, "JoinNowButton")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "JoinNowButton"));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(driver, "signInLinkAccount")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver,"signInLinkAccount"));
			}
			
			objStatus+=String.valueOf(UIFoundation.newUsersetObject(driver, "LoginEmail",userEmail));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "LoginPassword", "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "SignInButton"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			log.info("The execution of the method login ended here ...");
			
			if(expectedState.equalsIgnoreCase(actualState))
			{
				objStatus+=true;
			    String objDetail="Expected state name and actual state name matched";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    System.out.println("Expected satte name and actual state name matched");
			}else{
				objStatus+=false;
				ReportUtil.addTestStepsDetails("Expected state name and actual state name doest not match", "", "");
				 objStatus+=false;
				 String objDetail="Expected state name and actual state name doest not match";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}*/
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh,
	 * Purpose     : The purpose of this method is to fill the shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	public static String shippingDetails() {
		String objStatus = null;
		String screenshotName = "addNewCreditCard.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;
		try {

			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
						
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnFedexSearchButton);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
			UIFoundation.waitFor(13L);
			if(UIFoundation.isDisplayed(PickedPage.btnLocalPickupAddresSelect)){
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnLocalPickupAddresSelect));
			}					
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtFedexSaveButton));
			
			UIFoundation.waitFor(13L);
			UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtAdultSignature);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtAdultSignature));
			objStatus += String.valueOf(UIFoundation.isDisplayed(PickedPage.txtarivalMessage));
				
			
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
				objStatus+=false;
				String objDetail = "Local pick up description is not displayed in the Compass Recipient section";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);

				return "Fail";
			} else {
				objStatus+=true;
				String objDetail = "Local pick up description is  displayed in the Compass Recipient section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				return "Pass";
				
			}
		
			
	} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	/*public static String addNewCreditCard(WebDriver driver) {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		int cartConutBeforeRemovingProduct=0;
		int cartConutAfterRemovingProduct=0;
		String screenshotName = "addNewCreditCard.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;

		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(driver, "AddNewCard"))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "AddNewCard"));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "NameOnCard", "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "CardNumber", "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"ExpiryMonth","Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"ExpiryYear","Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "CVV", "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(driver, "BillingAndShippingCheckbox");
		    UIFoundation.javaScriptClick(driver, "BillingAndShippingCheckbox");
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(driver, "birthMonth"))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthMonth","birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthDate","birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(driver, "birthYear", "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(driver, "chkTerms&Condition");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "chkTerms&Condition"));
			UIFoundation.webDriverWaitForElement(driver, "cmdEnrollInPicked", "Invisible", "", 10);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "cmdEnrollInPicked"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(driver, "txtEnrollmentError"));
						
			UIFoundation.waitFor(3L);
           
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				
				objStatus+=false;
				String objDetail = "User is Not be able to enroll for  Existing/New dry state address in the recipient section";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "false";
				
			} else {
				objStatus+=true;
				String objDetail = "User is Not be able to enroll for  Existing/New dry state address in the recipient section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}*/


}
