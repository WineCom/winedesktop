package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ErrorMessageDisplayedForTheUserBelow21Years extends Mobile {

	
	/***************************************************************************
	 * Method Name			: ErrorMessageDisplayedForTheUserBelow21Years()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String errorMessageDisplayedForTheUserBelow21Years()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_errorrMsg.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			log.info("The execution of the method errorMessageDisplayedForTheUserBelow21Years started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));	
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkAccountInfo));
			UIFoundation.clearField(LoginPage.txtBrthMonth);
			UIFoundation.clearField(LoginPage.txtBrthDay);
			UIFoundation.clearField(LoginPage.txtBrthYear);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtBrthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtBrthDay,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtBrthYear, "brthYearBelow21"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.BtnSave));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.txtYearErrorMsg))
			{
				objStatus+=true;
				String objDetail="Appropriate error message is displayed for the user below 21 years in account information page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Appropriate error message is not displayed for the user below 21 years in account information page.";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method errorMessageDisplayedForTheUserBelow21Years ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method errorMessageDisplayedForTheUserBelow21Years "+ e);
			return "Fail";
			
		}
	}	
	
}
