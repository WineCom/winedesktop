package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyContenDisplayedUnderMySOMMSection extends Mobile {
	

	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L);		
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "compassUser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.shoppingCartPageTitle;	
			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: verifySubscriptions()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String verifySubscriptions()
	{
		String objStatus=null;			
		String screenshotName = "Scenarios_verifySubscriptions_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method verifySubscriptions started here ...");
					
				
			UIFoundation.waitFor(1L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.linPickedSetting));
		    UIFoundation.waitFor(1L);	
	
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.cmdVerifyRedWine));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.cmdVerifyWhiteWine));			
			 UIFoundation.waitFor(1L);   
		    
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtOrderStatus));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtSubcreption));
		    
		    UIFoundation.waitFor(1L);
		    UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtRecipient);
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtPayment));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtChangeAdd));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtChangePay));
		    UIFoundation.waitFor(1L);
		    
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtCancleSub));
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtTermAndCon));
		    UIFoundation.waitFor(1L);		  		    
		    
			log.info("The execution of the method verifySubscriptions ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
		    	String objDetail="Existing users settings are  not remains same";
		    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
				return "Fail";
			}
			else
			{
				  objStatus+=true;
					String objDetail = "Existing users settings are matching";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				return "Pass";
			}
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to verifySubscriptions";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method verifySubscriptions "+ e);
			return "Fail";
			
		}
	}
}
