package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyFilterByLinkDisplayed extends Mobile {
	

	
	static Pattern pattern;
	static String arrTestData[];
	static String arrObjectMap[];
	static String expectedText;
	static String expected,actual;
	static String value;
	static int expectedItemCount[]={1519,253,217,1623,271,64};
	static int actualItemCount[]=new int[expectedItemCount.length];
	static boolean isObjectPresent=false;

	
	/***************************************************************************
	 * Method Name			: verifyOnlyThreeFiltersAreVisible()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,
	 * Purpose				:					 
	 * Jira Id              : TM-4313
	 ****************************************************************************
	 */
	
	public static String verifyOnlyThreeFiltersAreVisible()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkvarietal));
			UIFoundation.waitFor(1L);			
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkvarietal));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkRegion));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkRatingAndPrice));
			if (objStatus.contains("false"))
			{
				
				System.out.println("Verify only 3 filters are visible by default on tablet/desktop list page test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify only 3 filters are visible by default on tablet/desktop list page. test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyOnlyThreeFiltersAreVisible "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyMoreFiltersElementsAreVisible()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 * TM-4313              :  
	 ****************************************************************************
	 */
	
	public static String verifyMoreFiltersElementsAreVisible()
	{
		String objStatus=null;
		try
		{
			
			log.info("The execution of method verifyMoreFiltersElementsAreVisible started here");
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkFilterBy));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkSortFilter));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkMoreFilters));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkReviewedBy));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.spnSizeAndType));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkFineWine));
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkVintage));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnHide));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(!UIFoundation.isDisplayed(ListPage.lnkVintage));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkFilterByinput));

			if((!UIFoundation.isDisplayed(ListPage.lnkvarietal)) && (!UIFoundation.isDisplayed(ListPage.lnkMoreFilters)))
					{
				objStatus+=true;
				String  objDetail="All Filter collapsed when click on Filtr By";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");

					}
			else
			{
				objStatus+=false;
				String  objDetail="All Filter are not collapsed when click on Filtr By";
				ReportUtil.addTestStepsDetails(objDetail, "false", "");

			}
			if (objStatus.contains("false"))
			{
				   objStatus+=false;
				   String  objDetail="Filter By link is not displayed, expanded and collapse section in list page is failed ";
				   ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
				
				return "Fail";
			}
			else
			{
				   objStatus+=true;
				String   objDetail="Filter By link is displayed, expanded and collapse section in list page is excecuted succesfully";
				   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verify all the available filters are visible on clicking More filters test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyMoreFiltersElementsAreVisible "+e);
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: productFilteration()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 ****************************************************************************
	 */
	
	/*public static String productFilteration(WebDriver driver)
	{
		String objStatus=null;
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage. "MainNavButton"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage. "VarietalLink"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(ApplicationDependent.ClickObjectItems(ListPage. "PinotNoir"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage. "Region"));
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage. "RatingAndPrice"));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage. "Price"));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage. "Rating"));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(ApplicationDependent.ClickObjectItems(ListPage. "Done"));
			UIFoundation.waitFor(8L);
		
			if (objStatus.contains("false"))
			{
				
				System.out.println("Product Filteration test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Product Filteration test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}*/
	
}
