package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySpiritsShowsOutOfCartWhenSelectedOutSideState extends Mobile {
	
	
	
	/***************************************************************************
	 * Method Name			: RatingStarsDisplayedProperlyInMyWine()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: TM-4393
	 ****************************************************************************
	 */
	
	public static String verifyOutOfStockSpirits()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__ratingStarMyWine.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;			
		
		try
		{			
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine started here ...");
					
			UIFoundation.waitFor(2L);
			driver.get("https://qwww.wine.com/product/the-macallan-18-year-sherry-oak-single-malt-scotch-whisky/544824");
			UIFoundation.waitFor(2L);		
		
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.btnAddToCart));			
						
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);	
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cbo_SelectState, "dryState"));
			UIFoundation.waitFor(1L);	
			if(UIFoundation.isDisplayed(ListPage.btnContinueShipToKY))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnContinueShipToKY));
			}
			UIFoundation.waitFor(2L);									
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.txtProductUnavailable));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.SelectObject(ListPage.cbo_SelectState, "State"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cboObj_ChangeState, "dryState"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnContinueShipToKY));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnContinueShipToKY));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnYourCartIsEmpty));	
			
			
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is executed successfully");
				return "Pass";
			}			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "+ e);
			return "Fail";
			
		}
	}

}
