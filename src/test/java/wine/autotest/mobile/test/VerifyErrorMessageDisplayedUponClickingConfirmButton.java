package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyErrorMessageDisplayedUponClickingConfirmButton extends Mobile {
	

	
	
	
	/***************************************************************************
	 * Method Name			: verifySubscriptions()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	
	public static String verifyErrorMessageDisplayed()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_verifyErrorMessageDisplayed_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{			
			log.info("The execution of the method verifyErrorMessageDisplayed started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(1L);	
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.linPickedSetting));
		    UIFoundation.waitFor(1L);
		    UIFoundation.scrollDownOrUpToParticularElement(PickedPage.txtPickedChangeDate);
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.txtPickedChangeDate));
		    UIFoundation.waitFor(1L);		    
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.btnConfirmPicked));
		    UIFoundation.waitFor(1L);	
		    objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtConfirmingMessage));		    
		  
			log.info("The execution of the method verifyErrorMessageDisplayed ended here ...");
			if (objStatus.contains("false"))
			{				
				objStatus+=false;
		    	String objDetail="Error message is not displayed above the Confirm button";
		    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);			
				return "Fail";
			}
			else
			{
				  objStatus+=true;
					String objDetail = "Error message is displayed above the Confirm button";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to verifyErrorMessageDisplayed";
	    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method verifyErrorMessageDisplayed "+ e);
			return "Fail";
			
		}
	}

}
