package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyDryStateProductsUnavailableFunctionality extends Mobile {
	

	

	/***************************************************************************
	 * Method Name : orderSummaryForNewUser() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyproductUnavailabileFunctionality() {
		String products1 = null;
		String products2 = null;
		String products3 = null;
		String products4 = null;
		String products5 = null;
		String objStatus = null;

		try {
			
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			System.out.println("=======================verify the message �Currently Unavailable� is displayed when dry state is selected  =====================");
			if (!UIFoundation.getText(ListPage.btnUnavailableFirstProduct).contains("Fail")) {
				products1 = UIFoundation.getText(ListPage.btnUnavailableFirstProduct);
				System.out.println("1) " + products1);
				

			}

			if (!UIFoundation.getText(ListPage.btnUnavailableSecondProduct).contains("Fail")) {
				products2 = UIFoundation.getText(ListPage.btnUnavailableSecondProduct);
				System.out.println("2) " + products2);				

			}
			
			if (!UIFoundation.getText(ListPage.btnUnavailableThirdProduct).contains("Fail")) {
				products4 = UIFoundation.getText(ListPage.btnUnavailableThirdProduct);
				System.out.println("4) " + products4);
				

			}

			if (!UIFoundation.getText(ListPage.btnUnavailableFourthProduct).contains("Fail")) {
				products5 = UIFoundation.getText(ListPage.btnUnavailableFourthProduct);
				System.out.println("5) " + products5);
				

			}
			if (!UIFoundation.getText(ListPage.btnUnavailableFifthProduct).contains("Fail")) {
				products3 = UIFoundation.getText(ListPage.btnUnavailableFifthProduct);
				System.out.println("3) " + products3);
				

			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}


	
	
}
