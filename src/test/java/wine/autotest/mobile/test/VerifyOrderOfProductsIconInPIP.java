package wine.autotest.mobile.test;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyOrderOfProductsIconInPIP extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : verifyOrderOfProductsIconInPIP() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     :
	 ****************************************************************************
	 */

	public static String verifyOrderOfProductsIconInPIP() {
		String objStatus = null;
		String screenshotName = "Scenarios__orderOfProduct.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;

		try {

			log.info("The execution of the method verifyOrderOfProductsIconInPIP started here ...");		
			driver.get("https://qwww.wine.com/product/silver-oak-napa-valley-cabernet-sauvignon-2014/391727");
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnOrderOfProductIcons);
			if (UIFoundation.isDisplayed(ListPage.spnOrderOfProductIcons)) {
				objStatus+=true;
				String objDetail="Product icons are displayed in the order";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				
			} else {
				objStatus+=false;
				String objDetail="Product icons are not displayed in the order";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method verifyShipToDateUpdated ended here ...");
			if (objStatus.contains("false")) {
				
				System.out.println(
						"Verify the order of product icons displayed in PIP test case is failed");
				return "Fail";
			

			} else {
				System.out.println(
						"Verify the order of product icons displayed in PIP test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error(
					"there is an exception arised during the execution of the method verifyOrderOfProductsIconInPIP "
							+ e);
			return "Fail";

		}
	}
	
}
