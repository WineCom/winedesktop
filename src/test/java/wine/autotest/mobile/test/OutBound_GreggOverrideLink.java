package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class OutBound_GreggOverrideLink extends Mobile {

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[3]"));
			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
			sel.selectByVisibleText("DC");
		//	UIFoundation.webDriverWaitForElement(ListPage. "updatingState", "Invisible", "", 50);
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			 {
			    driver.navigate().refresh();
			    UIFoundation.waitFor(4L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
				UIFoundation.webDriverWaitForElement(ListPage.dwnselectFilterBy, "Clickable", "", 50);

			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));				
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {				
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {				
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {

				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {				
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
			}
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtCabernetSauvignon);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		String objStatus = null;
		String screenshotName = "Scenarios_ OutBound_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		String state=null;
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgWineLogo));
			UIFoundation.webDriverWaitForElement(ListPage.txtSearchBarFormInput, "Clickable", "", 50);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[3]"));
			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
			sel.selectByVisibleText("FL");
			UIFoundation.webDriverWaitForElement(ListPage.txtupdatingState, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(ListPage.spnHeadaerMessage)){
				String s=UIFoundation.getText(ListPage.spnHeadaerMessage);
				String[] s2=s.split(" ");
				state=s2[s2.length-1];
				System.out.println("item in your cart cannot be shipped to "+state+". message is displayed");
				ReportUtil.addTestStepsDetails("item in your cart cannot be shipped to "+state+". message is displayed", "Pass", "");
				
			}else
			{
				objStatus+=false;
				String objDetail="item in your cart cannot be shipped to "+state+". message is not displayed";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			System.out.println(":"+UIFoundation.getText(ListPage.spnHeadaerMessage));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCancelStayInState));
			UIFoundation.waitFor(6L);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCartCount);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);
			String url = driver.getCurrentUrl();
			String newurl = url+"?swap=disabled";
			driver.get(newurl);
			UIFoundation.waitFor(3L);
			System.out.println("url:"+driver.getCurrentUrl());
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.imgWineLogo));
			UIFoundation.webDriverWaitForElement(ListPage.txtSearchBarFormInput, "Clickable", "", 50);
			WebElement ele1=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[1]"));
			//WebElement ele1=driver.findElement(By.xpath("(//form[@class='formWrap searchBarForm'])[2]/label/select[@class='state_select']"));
			org.openqa.selenium.support.ui.Select sel1=new org.openqa.selenium.support.ui.Select(ele1);
			sel1.selectByVisibleText("CA");
			UIFoundation.webDriverWaitForElement(ListPage.txtupdatingState, "Invisible", "", 50);
			if(!UIFoundation.isDisplayed(ListPage.spnHeadaerMessage)){
				
				System.out.println("No message should be displayed and the product should be displayed in the CART");
			//	ReportUtil.addTestStepsDetails("item in your cart cannot be shipped to "+state+". message is displayed", "Pass", "");
				
			}
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCartCount);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);
			UIFoundation.javaScriptClick(CartPage.txtProductQuantity);
			objStatus+=String.valueOf(UIFoundation.SelectObject(CartPage.txtProductQuantity, "quantity"));
			UIFoundation.waitFor(2L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
	
			
			
		} catch (Exception e) {
			String objDetail="OutBound Gregg override link failed: ";
			 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name : shippingDetails()
	 * Created By  : chandrashekhar
	 * Reviewed By : Ramesh,
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(1L);
			

             if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}
                          UIFoundation.waitFor(2L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			//VerifyContinueButton
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinueShipRecpt);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueShipRecpt))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueShipRecpt));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueShipRecpt, "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : addNewCreditCard() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(1L);
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);	
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(1L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);		
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			}			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println(" Order creation with outBoundGreggOverrideLink test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with outBoundGreggOverrideLink test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
			
}

