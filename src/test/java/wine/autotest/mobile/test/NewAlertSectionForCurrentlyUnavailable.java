package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class NewAlertSectionForCurrentlyUnavailable extends Mobile {

	
	/***************************************************************************
	 * Method Name			: NewAlertSectionForCurrentlyUnavailable()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String newAlertSectionForCurrentlyUnavailable()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_newArrival.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			UIFoundation.waitFor(6L);
			log.info("The execution of the method newAlertSectionForCurrentlyUnavailable started here ...");
			driver.get("https://qwww.wine.com/product/beaulieu-vineyard-georges-de-latour-private-reserve-2016/524720");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.txtProdAlert))
			{
				objStatus+=true;
				String objDetail="New Arrival Alert section is expanded by default when landing on a Currently Unavailable PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="New Arrival Alert section is not expanded by default when landing on a Currently Unavailable PIP";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method newAlertSectionForCurrentlyUnavailable ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method newAlertSectionForCurrentlyUnavailable "+ e);
			return "Fail";
			
		}
	}
	
}
