package wine.autotest.mobile.test;


import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages extends Mobile {


	/***************************************************************************
	 * Method Name : varietalNRegionLinkInPip
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * (TM-4394)
	 ****************************************************************************
	 */

	public static String varietalNRegionLinkInPip() {

		String objStatus=null;
		String screenshotName = "varietalNRegionLinkInPip.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("varietal N Region Link In Pip method started here");
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstListProd));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.spnVarietalOriginLink)) 
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnVarietalOriginLink));
			UIFoundation.waitFor(2L);
			String expVarietalUrl = driver.getCurrentUrl();
			String actVarietalUrl = verifyexpectedresult.orginvarietalUrl;			
			if(expVarietalUrl.equals(actVarietalUrl)) {
				objStatus+=true;
				String objDetail = "Varietal origin link is displayed and user is navigated succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail = "Varietal origin link is not displayed  displayed and user is not navigated succesfully";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			driver.navigate().back();
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.spnRegionOrgiginLink)) 
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnRegionOrgiginLink));
			UIFoundation.waitFor(2L);
			String expRegionUrl = driver.getCurrentUrl();
			String actRegionUrl = verifyexpectedresult.orginvarietalUrl;			
			if(expRegionUrl.equals(actRegionUrl)) {
				objStatus+=true;
				String objDetail = "Region origin link is displayed and user is navigated succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail = "Region origin link is not displayed  displayed and user is not navigated succesfully";			
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(objStatus.contains("false")) {
				System.out.println("Verify Tat Varietal N Region origin Text In PIP Are The Links To ListPages test case failed");
				return "false";
			}
			else {
				System.out.println("Verify Tat Varietal N Region origin Text In PIP Are The Links To ListPages test case passed");
				return "true";
			}
		}catch(Exception e){
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}	
}
