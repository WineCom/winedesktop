
package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class ApplyAndRemovePromoCodeForNewUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : captureOrdersummary() 
	 * Created By : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 ****************************************************************************
	 */

	
	
    public static String captureOrdersummary() {
        String objStatus = null;
        String subTotal = null;
        String shippingAndHandling = null;
        String totalBeforeTax = null;
        String promeCode = null;
        boolean isdisplayed=false;
           String screenshotName = "Scenarios_PromoCodeRemove_Screenshot.jpeg";
              String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
                          + screenshotName;
        try {
              log.info("The execution of the method captureOrdersummary started here ...");
              System.out.println("============Order summary before applying promo code===============");
              subTotal = UIFoundation.getText(CartPage.spnSubtotal);
              shippingAndHandling = UIFoundation.getText(CartPage.spnShippingHnadling);
              totalBeforeTax = UIFoundation.getText(CartPage.spnTotalBeforeTax);
              System.out.println("Subtotal:              " + subTotal);
              System.out.println("Shipping & Handling:   " + shippingAndHandling);
              System.out.println("Total Before Tax:      " + totalBeforeTax);
              if(UIFoundation.isDisplayed(CartPage.txtPromoCodeText))
              {
                    objStatus += String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
                    objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnPromoCodeApplyLink));
                    UIFoundation.waitFor(5L);
              }else
              {
                    objStatus += String.valueOf(UIFoundation.clickObject(CartPage.txtObjPromoCode));
                    objStatus += String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
                    objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnPromoCodeApplyLink));
                    UIFoundation.waitFor(5L);
              }

              System.out.println("============Order summary after applying promo code===============");
              subTotal = UIFoundation.getText(CartPage.spnSubtotal);
              shippingAndHandling = UIFoundation.getText(CartPage.spnShippingHnadling);
              totalBeforeTax = UIFoundation.getText(CartPage.spnTotalBeforeTax);
              promeCode = UIFoundation.getText(CartPage.spnPromeCodePrice);
              System.out.println("Subtotal:              " + subTotal);
              System.out.println("Shipping & Handling:   " + shippingAndHandling);
              System.out.println("Prome Code:            " + promeCode);
              System.out.println("Total Before Tax:      " + totalBeforeTax);
              UIFoundation.waitFor(3L);
         //     ApplicationDependent.discountCalculator(CartPage.spnSubtotal, promeCode);
              objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnRemovePromoCode));
              UIFoundation.waitFor(2L);
              isdisplayed=UIFoundation.isDisplayed(CartPage.spnPromeCodePrice);
              if(!isdisplayed)
              {
                    System.out.println("Applied Prome code is Removed successfully");
                    
                    objStatus+=true;
                    String objDetail=" Applied Prome code is Removed successfully";
                    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
                 
              }
              else
              {
                    objStatus+=false;
                       String objDetail="The Applied promo code amount is not removed from total amount";
                       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                    System.out.println("The Applied promo code amount is not removed from total amount");
              }
              System.out.println("============Order summary after removing promo code===============");
              subTotal=UIFoundation.getText(CartPage.spnSubtotal);
              shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
              totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
              System.out.println("Subtotal:              "+subTotal);
              System.out.println("Shipping & Handling:   "+shippingAndHandling);
              System.out.println("Total Before Tax:      "+totalBeforeTax);
              log.info("The execution of the method captureOrdersummary ended here ...");
              if (objStatus.contains("false")) {

                    return "Fail";
              } else {

                    return "Pass";
              }

        } catch (Exception e) {

              log.error("there is an exception arised during the execution of the method captureOrdersummary " + e);
              return "Fail";

        }
  }



	
	
	
}