package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class MultipleTrackingOrderNumber extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);		
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(1L);				
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(LoginPage.MainNavSignIn));         
		
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "multipleOrderLogin"));
			UIFoundation.waitFor(1L);	
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(3L);
		//	objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "VarietalLink"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnThirdProductToCart);
			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnThirdProductToCart));
			}
			//UIFoundation.scrollDownOrUpToParticularElement(ListPage. "FifthProductToCart");
			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnklistPageContainer);
			UIFoundation.scrollUp(driver);
		
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(5L);
		   objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnFeaturedMainTab));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtBordexFeatures));
			UIFoundation.waitFor(2L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnSixthProductToCart);
			addToCart3 = UIFoundation.getText(ListPage.btnSixthProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnSixthProductToCart));
			}
			//UIFoundation.scrollDownOrUpToParticularElement(ListPage. "headerText");
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: multipleTrackingOrderNumber()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh,
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String multipleTrackingOrderNumber() {

	String objStatus=null;
	String orderNum=null;
	   String screenshotName = "Scenarios_OmultipleOrder_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(4L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(2L);
			
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				//ShipContinueButton
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				UIFoundation.waitFor(3L);
			}				
					        
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
	        {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
	        }
	          if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
	          {
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	             UIFoundation.waitFor(2L);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	             UIFoundation.waitFor(3L);
	             
	            WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
			    UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		          UIFoundation.waitFor(1L);
		          // PaymentContinue
		          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		          UIFoundation.waitFor(10L);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtBirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
	                 
	          }
	         UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtCvvDisp)){
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCvvDisp, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardSave));
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			}

			orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			int splitFirstOrderNumebr=Integer.parseInt(orderNum);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtOrderNum));
			UIFoundation.waitFor(3L);
			int splitSecOrderNumebr=splitFirstOrderNumebr+1;

			WebElement splitSecondOrderNumebr=driver.findElement(By.xpath("//span[text()='"+splitSecOrderNumebr+"']"));
			if(splitSecondOrderNumebr.isDisplayed())
			{ objStatus+=true;
		      String objDetail="Multiple tracking numbers are displayed under order history";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		      
			}else
			{
				objStatus+=false;
				String objDetail="Multiple tracking numbers are not displayed under order history";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify multiple tracking numbers are displayed under Tracking # column in the 'order History' grid for split orders test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify multiple tracking numbers are displayed under Tracking # column in the 'order History' grid for split orders test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";			
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}	
}
