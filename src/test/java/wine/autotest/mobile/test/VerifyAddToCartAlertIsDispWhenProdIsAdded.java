package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyAddToCartAlertIsDispWhenProdIsAdded extends Mobile {
	


	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInHomePage
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInHomePage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInHomePage.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("verify Add To Cart Alert In HomePage method started here");
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkProdInHomePage));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ListPage.btnAddToCartAlert)) {
				objStatus+=true;
				System.out.println("Add to cart alert is displayed while adding product in home page");
				String objDetail="Add to cart alert is displayed while adding product in home page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding product in home page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("verify Add To Cart Alert In HomePage method ended here");

			if(objStatus.contains("false")) {
				System.err.println("verify Add To Cart Alert In HomePage failed ");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In HomePage executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInListPage
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInListPage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInListPage.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("verify Add To Cart Alert In List Page started here");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.btnAddToCartAlert)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed while adding product in List page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding  product in List page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("verify Add To Cart Alert In list method ended here");

			if(objStatus.contains("false")) {
				System.out.println("verify Add To Cart Alert In List page failed");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In List page executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInPiptPage
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInPiptPage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInPiptPage.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {

			log.info("verify Add To Cart Alert In Pip Page started here");
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFirstListProd));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ListPage.btnAddToCartAlert)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed while adding product in pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding  product in pip page";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(5L);
			System.out.println("Increasing count from quantity box");
			objStatus+=String.valueOf(UIFoundation.SelectObject(CartPage.cboProductItemLimitQuantity, "quantity"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(2L);
			String alertQty = UIFoundation.getText(ListPage.btnSelQtyInALert);
			if(alertQty.contains("2")) {
				objStatus+=true;
				//System.out.println("Add to cart alert is displayed with correct quantity");
				String objDetail="Add to cart alert is displayed with correct quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is displayed with Incorrect quantity";						
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("verify Add To Cart Alert In Pip method ended here");
			if(objStatus.contains("false")) {
				System.out.println("verify Add To Cart Alert In Pip page failed");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In Pip page executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: verifyAddToCartAlertInPiptPage
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String verifyAddToCartAlertInMyWinePage () {

		String objStatus=null;
		String screenshotName = "verifyAddToCartAlertInPiptPage.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {

			log.info("verify Add To Cart Alert In MyWine Page started here");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProdct));
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(ListPage.btnAddToCartAlert)) {
				objStatus+=true;
				String objDetail="Add to cart alert is displayed while adding product in MyWine page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Add to cart alert is not displayed while adding  product in MyWine page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}

			log.info("verify Add To Cart Alert In MyWine method ended here");
			if(objStatus.contains("false")) {
				System.out.println("verify Add To Cart Alert In MyWine page failed");
				return "fail";
			}
			else {
				System.out.println("verify Add To Cart Alert In MyWine page executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}

}
