package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;



import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyOutOFStockCheckBoxChecked extends Mobile {


	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		

	String screenshotName = "Scenarios_ProdWithProductName_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
		//	UIFoundation.waitFor(3L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "SearchBarFormInput"));
		//	UIFoundation.waitFor(1L);
		//	objStatus+=String.valueOf(UIFoundation.setObject(driver, "TxtsearchProduct", "OOSProduct"));

			driver.navigate().to("https://qwww.wine.com/search/Jackaroo/0");			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.dwnselectFilterBy));
			UIFoundation.waitFor(2L);
			boolean status=UIFoundation.isCheckBoxSelected(ListPage.chkOutOfStockCheckbox);
			if(status==true)
			{
				System.out.println("Verify out of stock chekbox is Checked test case executed successfully");
				objStatus+=true;
	            String objDetail="Out of Checkbox is selected by default";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else
			{
				System.err.println("Out of Checkbox not selected by default");
			    objStatus+=false;
			    String objDetail="Out of Checkbox not selected by default";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
			}
		//	System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
		//	UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "obj_AddToCart"));
		//	System.out.println(UIFoundation.getText(driver,"AddAgain"));
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "CartBtnCount"));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}



}
