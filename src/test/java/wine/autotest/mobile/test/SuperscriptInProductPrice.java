package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class SuperscriptInProductPrice extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: superscriptInProductPrice()
	 * Created By			: Chandrashekar. 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String superscriptInProductPrice()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_superscriptInProductPrice.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			UIFoundation.waitFor(8L);
			log.info("The execution of the method superscriptInProductPrice started here ...");
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtProductPriceRegWhole);
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.txtProductPriceRegWhole) && UIFoundation.isDisplayed(CartPage.txtProductPriceRegFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in home page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in home page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtSearchProduct);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtPinotNoir));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddProductToPIP));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.txtProductPriceSaleWhole) && UIFoundation.isDisplayed(CartPage.txtProductPriceSaleFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in PIP page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in PIP page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
		//	System.out.println(UIFoundation.getText(CartPage."AddAgain"));			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);
	    	if(UIFoundation.isDisplayed(CartPage.txtProductPriceSaleWhole) && UIFoundation.isDisplayed(CartPage.txtProductPriceSaleFraction))
			{
				objStatus+=true;
				String objDetail="Cents in Sale Price & Regular Price are displayed as 'Superscript' in Cart page.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Cents in Sale Price & Regular Price are not displayed as 'Superscript' in Cart page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method superscriptInProductPrice ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method superscriptInProductPrice "+ e);
			return "Fail";
			
		}
	}	
}
