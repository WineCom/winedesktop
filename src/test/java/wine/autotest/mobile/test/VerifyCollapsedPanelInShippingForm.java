package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyCollapsedPanelInShippingForm extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : verifyCollapsedPanel() 
	 * Created By : chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String verifyCollapsedPanel() {
		String objStatus = null;
		
		String screenshotName = "Scenarios_AddressLink_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			

            if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkReciptEidt);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkReciptEidt));
			//boolean AddNewAddressLink=UIFoundation.isDisplayed(driver, "AddNewAddressLink");
			boolean isDeliveryContinuePresent=UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue);
			boolean isPaymentContinuePresent=UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue);
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			
			if(isDeliveryContinuePresent && isPaymentContinuePresent){
				  objStatus+=false;
			      String objDetail="Verify the collapsed panel in shipping form test case is failed";
			      UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}else{
				 objStatus+=true;
			     String objDetail="Verify the collapsed panel in shipping form test case is executed successfully";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			if (objStatus.contains("false")) {
				System.out.println("Verify the collapsed panel in shipping form test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the collapsed panel in shipping form test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
}
