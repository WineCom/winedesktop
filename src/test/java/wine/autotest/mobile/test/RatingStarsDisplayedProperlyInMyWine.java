package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class RatingStarsDisplayedProperlyInMyWine extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "ratingUsername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: RatingStarsDisplayedProperlyInMyWine()
	 * Created By			: Chandrashekhara 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String ratingStarsDisplayedProperlyInMyWine()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__ratingStarMyWine.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;	
		
		try
		{
			
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine started here ...");
			UIFoundation.waitFor(1L);
		    objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			UIFoundation.waitFor(2L);		
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.imgUserRatings));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgUserRatings));		
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgRatingStarTextArea));			
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.txtTextAreaCount));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnCancelInMyWine));
			objStatus+=String.valueOf(UIFoundation.isElementDisplayed(ListPage.btnSaveInMyWine));
			log.info("The execution of the method ratingStarsDisplayedProperlyInMyWine ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'Rating stars' are displayed properly  on clicking the rating stars for already rated product in My Wine test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "+ e);
			return "Fail";
			
		}
	}	
}
