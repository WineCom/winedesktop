package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class RatingStarsHiddenInPIPForGifts extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: VerifyPromoBarInWashington()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * TM-3287
	 ****************************************************************************
	 */
	
	public static String VerifyRatingStarsHiddenInPIPForGifts()
	{
		String screenshotName = "Scenarios_RatingStar_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		String objStatus=null;
		try
		{

			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnGfts));
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkBottelName));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkGiftProdutcAddToPIP));
			UIFoundation.waitFor(2L);
			
			if(!UIFoundation.isDisplayed(ListPage.cboStarsRating) && !UIFoundation.isDisplayed(ListPage.lnkshare)){
				  objStatus+=true;
			      String objDetail="Rating Star and Share is not displayed in PIP page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Rating Star and Share is not displayed in PIP page");   
			}else{
				 objStatus+=false;
				 String objDetail="Rating Star and Share is displayed on PIP page";
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false"))
			{
				
				System.out.println("Verify the 'share' & 'rating stars' are hidden in PIP of Gift products test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'share' & 'rating stars' are hidden in PIP of Gift products test case is executed successfully");
				return "Pass";
			}
		
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method VerifyRatingStarsHiddenInPIPForGifts "+e);
			return "Fail";
		}
	}


}
