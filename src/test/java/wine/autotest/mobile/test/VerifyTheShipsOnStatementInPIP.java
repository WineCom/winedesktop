package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheShipsOnStatementInPIP extends Mobile {

	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyTheShipsOnStatementInPIP() {
		String objStatus = null;
		String product1 = null;
		String product2 = null;
		String product3 = null;
		   String screenshotName = "Scenarios__shipTodayProd.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;
		try {		
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
		//	objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "BordexBlends"));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtPinotNoir));			
			UIFoundation.waitFor(1L);
		//	objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkmoreFilters));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.dwnShipsSoonestR));
			product1 = UIFoundation.getText(ListPage.btnFirstListProd);
			product2 = UIFoundation.getText(ListPage.btnSecondProdName);
			UIFoundation.waitFor(2L);
			if (product1.contains("Ships today if ordered ")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstProdNameLink));
			}else if (product2.contains("Ships today if ordered in next  hours")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProdName));
			}else{
				objStatus +=false;
				String objDetail="No ship today products available";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"_ChoosePhoto", objDetail);
			}
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.txtShipsOnStatementInPIP))
			{
				  objStatus+=true;
			      String objDetail="Verified the Ships on statement in PIP for the products that will be shipped today";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	
			}
			else
			{
				objStatus +=false;
				String objDetail="Verify the Ships on statement in PIP for the products that will be shipped today is failed";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"_ChoosePhoto", objDetail);
			}
			if (objStatus.contains("false")) {
				System.out.println("Verify the Ships on statement in PIP for the products that will be shipped today test case failed");
				return "Fail";
			} else {
				System.out.println("Verify the Ships on statement in PIP for the products that will be shipped today test case executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}


	
}
