package wine.autotest.mobile.test;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

class UserCreationWithDesiredState extends Mobile{
static String expectedState=null;
static String actualState=null;
	/*************************************************************************** 
	 * Method Name			: userProfileCreation()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String userProfileCreation() {
		String objStatus=null;
		try {
			log.info("The execution of method create Account started here");
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			expectedState=sel.getFirstSelectedOption().getText();
			System.out.println("expected state:"+expectedState);
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));	
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
						
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.Password, "password"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.webDriverWaitForElement(LoginPage.CreateAccountButton, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(3L);
			/*if(UIFoundation.isDisplayed(driver, "Tooltip")){
			    UIFoundation.clickObject(driver, "Tooltip");
			    UIFoundation.waitFor(2L);
			   }*/
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavTabSignOut));
		/*	objStatus+= String.valueOf(ApplicationDependent.isObjectExistForSignIn(driver));
			UIFoundation.waitFor(3L);*/
			/*driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			UIFoundation.scrollDownOrUpToParticularElement(driver, "MainNavTabSignOut");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "MainNavTabSignOut"));
*/			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignoutLink));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Statename_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavTabSignOut));
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavSignIn"));
			UIFoundation.waitFor(2L);			
			/*if(UIFoundation.isDisplayed(driver, "JoinNowButton")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "JoinNowButton"));
				UIFoundation.waitFor(2L);
			}*/
			
		/*	if(UIFoundation.isDisplayed(driver, "signInLinkAccount")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver,"signInLinkAccount"));
			}*/
			//UIFoundation.waitFor(2L);			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail,"username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			if(expectedState.equalsIgnoreCase(actualState))
			{
				objStatus+=true;
			    String objDetail="Expected state name and actual state name matched";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    System.out.println("Expected satte name and actual state name matched");
			}else{
				/*objStatus+=false;
				ReportUtil.addTestStepsDetails("Expected state name and actual state name doest not match", "", "");*/
				 objStatus+=false;
				 String objDetail="Expected state name and actual state name doest not match";
				  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false") && !(expectedState.equalsIgnoreCase(actualState)))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

}
