package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyEditFunctionalityForHomeAddress extends Mobile {

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
             UIFoundation.waitFor(1L);			
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "editHomeAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: editHomeAddress()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String editHomeAddress() {
	String objStatus=null;
	   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
	   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method editHomeAddress started here ...");
		
			UIFoundation.waitFor(4L);			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientChangeAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnHomeAddressEdit));
			UIFoundation.clearField(FinalReviewPage.btnHomeAddressEditStreetAddressClear);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtHomePhoneNumber));
			UIFoundation.waitFor(1L);
			String expectedEditedAddress=UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtHomeAddressEditStreetAddress, "Address1");
			expectedEditedAddress=expectedEditedAddress.replaceAll(" ", "");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnHomeAddressSaveButton));
			UIFoundation.waitFor(3L);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShipCont, "Invisible", "", 50);
			//VerifyContinueButtonShipRecpt
		//	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "ShipContinueButton"));
			UIFoundation.waitFor(3L);    
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShipContinue, "Invisible", "", 50);
			String actualEditedAddress=UIFoundation.getText(FinalReviewPage.lnkExpectedEditedAddress);
			actualEditedAddress=actualEditedAddress.replaceAll(" ", "");
			if(actualEditedAddress.equalsIgnoreCase(expectedEditedAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified the Edit functionality for Home/ Business address";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the Edit functionality for Home/ Business address");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the Edit functionality for Home/ Business address  is failed";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentSection", objDetail);
			       System.err.println("Verify the Edit functionality for Home/ Business address is failed ");
			}
			log.info("The execution of the method editHomeAddress ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the Edit functionality for Home/ Business address test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the Edit functionality for Home/ Business addresss test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method editHomeAddress "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}
	
}
