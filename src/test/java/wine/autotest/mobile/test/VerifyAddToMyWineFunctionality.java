package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyAddToMyWineFunctionality extends Mobile {


	/***************************************************************************
	 * Method Name			: VerifyAddToMyWineFunctionality(NFP-4253)
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * 						  
	 ****************************************************************************
	 */

	public static String verifyAddToMyWineFunctionality (WebDriver driver)

	{
		String objStatus=null;
		String screenshotName = "VerifyAddToMyWineFunctionality.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method VerifyAddToMyWineFunctionality started here ...");
			String	 actualMyWine= verifyexpectedresult.expectedWineText;	
			String	 actualWinecolor = verifyexpectedresult.expWineColor;	
			String	 actProdName = verifyexpectedresult.prodName;
			
			driver.get("https://qwww.wine.com/product/windy-oaks-estate-cuvee-pinot-noir-2014/244120");
			UIFoundation.waitFor(1L);
			if(UIFoundation.isElementDisplayed(CartPage.spnAddToMyWineFunc))
			{
				objStatus+=true;
				String objDetail="AddToMyWine is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail ="AddToMyWine is not present in product pip page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnAddToMyWineFunc));
			String mywinetextinpip =UIFoundation.getText(CartPage.spnAddToMyWineFunc);
			String actualcolorofmywine=UIFoundation.getColor(CartPage.spnMywinecolor);	
			if(mywinetextinpip.equals(actualMyWine) && (actualcolorofmywine.equalsIgnoreCase(actualWinecolor))){
				objStatus+=true;
				String objDetail="Product is added to My Wine and highligted to purple color";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail ="Product is not added";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			String prodadded=UIFoundation.getText(CartPage.txtProductName);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			if(prodadded.contains(actProdName)) {
				objStatus+=true;
				String objDetail="Product added in pip is displayed in My wine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail ="Product added in pip is not displayed in My wine ";				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method VerifyAddToMyWineFunctionality ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify Add To My Wine Functionality test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Add To My Wine Functionality test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);

			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name			: verifyAddToMyWineFunctionalityinPIP(NFP-4253)
	 * Created By			: Ramesh 
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: 
	 * 						  
	 ****************************************************************************
	 */

	public static String verifyAddToMyWineFunctionalityinPIP ()

	{
		String objStatus=null;
		String screenshotName = "VerifyAddToMyWineFunctionality.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method VerifyAddToMyWineFunctionality started here ...");
			String	 actualMyWine = verifyexpectedresult.expectedWineText;	
			String	 actualWinecolor = verifyexpectedresult.expWineColor;	
		//	String	 actProdName = verifyexpectedresult.prodNameMywine;	
						
	//		String   actProdName= objExpectedRes.getProperty("prodNameMywine");
			driver.get("https://qwww.wine.com/product/j-vineyards-california-pinot-noir-2016/400169");
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(CartPage.spnAddToMyWineFunc))
			{
				objStatus+=true;
				String objDetail="AddToMyWine is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail ="AddToMyWine is not present in product pip page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnAddToMyWineFunc));

			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method VerifyAddToMyWineFunctionality ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify Add To My Wine Functionality test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Add To My Wine Functionality test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);

			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name			: verifyAddToMyWineFunctionalityinPIPforRating(NFP-4253)
	 * Created By			:  Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: 
	 * 						  
	 ****************************************************************************
	 */

	public static String verifyAddToMyWineFunctionalityinPIPforRating ()

	{
		String objStatus=null;
		String screenshotName = "verifyAddToMyWineFunctionalityinPIPforRating.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method verifyAddToMyWineFunctionalityinPIPforRating started here ...");
			
			String	 actualMyWine = verifyexpectedresult.expectedWineText;
			String	 actualWinecolor = verifyexpectedresult.expWineColor;
			String	 actProdName = verifyexpectedresult.prodName;
						
			driver.get("https://qwww.wine.com/product/fel-anderson-valley-pinot-noir-2016/434639");
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(ListPage.imgUserRatings))
			{
				objStatus+=true;
				String objDetail="Star Rating is present in product pip page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail ="Star Rating is not present in product pip page.";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgUserRatingstext));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgUserRatings));
			String strprodadded=UIFoundation.getText(CartPage.txtProductName);
			UIFoundation.waitFor(1L);
			System.out.println("prodadded :"+strprodadded);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			if(strprodadded.contains(actProdName)) {
				objStatus+=true;
				String objDetail="Product added in pip is displayed in My wine";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail ="Product added in pip is not displayed in My wine ";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			driver.manage().window().maximize();
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkfirstProdNameLink));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.imgClearStarRating));
			UIFoundation.waitFor(2L);
			System.out.println("objStatus :"+objStatus);
			log.info("The execution of the method verifyAddToMyWineFunctionalityinPIPforRating ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify Add To My Wine Functionality test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Add To My Wine Functionality test case executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);

			return "Fail";

		}
	}
}
