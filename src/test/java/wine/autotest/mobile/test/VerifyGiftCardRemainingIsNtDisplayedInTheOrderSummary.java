package wine.autotest.mobile.test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary extends Mobile {
	

	/***************************************************************************
	 * Method Name			: applyGiftCode()
	 * Created By			: Chandrashekhar KB  
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */


	public static String applyGiftCode()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_applyGiftCode_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;

		try
		{
			log.info("The execution of the method applyGiftCode started here ...");
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);				
			}
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Gift cards are applied succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Gift cards are not applied";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("The execution of the method applyGiftCode test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("The execution of the method applyGiftCode test case executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method applyGiftCode "+ e);
			return "Fail";

		}
	}
	
		
	/***************************************************************************
	 * Method Name			: verifyGiftCardAmountInOrderSummary()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String verifyGiftCardAmountInOrderSummary() {
	
	String objStatus=null;	
	String giftCardUsed=null;
	String giftCardInOrderHistory=null;
	String screenshotName = "Scenarios_checkoutProcess_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;

		try {
			log.info("The execution of the method checkoutProcess started here ...");			
			UIFoundation.waitFor(4L);
						
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(8L);			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}
			UIFoundation.waitFor(3L);         	
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);				
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);				
			}
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
			   {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
			   }
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtGiftCardUsed);
		    giftCardUsed=UIFoundation.getText(FinalReviewPage.txtGiftCardUsed);
			UIFoundation.getText(FinalReviewPage.txtGiftCardRemaining);
			UIFoundation.waitFor(2L);	
					
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(12L);
			
			 if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
	            {
	                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
	                  UIFoundation.waitFor(5L);                  
	            }                                        
	            objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.txtOrderNum));
	            UIFoundation.waitFor(1L);
	           UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtGiftCardUsed);
				UIFoundation.waitFor(1L);				
				giftCardInOrderHistory=UIFoundation.getText(FinalReviewPage.txtGiftCardUsed);			
				if(giftCardUsed.contains(giftCardInOrderHistory)&&(!UIFoundation.isDisplayed(FinalReviewPage.txtGiftCardRemaining)))
				{
					objStatus+=true;
					String objDetail = "Gift card Remaining is not displayed in the order summary section ";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		    			
				}
				else
				{
					objStatus+=false;
			    	String objDetail="Gift card remaining is displayed in the order summary section ";
			    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
						
			log.info("The execution of the method verifyGiftCardAmountInOrderSummary ended here ...");
			if (objStatus.contains("false")) {
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyGiftCardAmountInOrderSummary "
					+ e);
			return "Fail";
		}
	}

}
