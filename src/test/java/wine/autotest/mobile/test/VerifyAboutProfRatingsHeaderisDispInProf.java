package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyAboutProfRatingsHeaderisDispInProf extends Mobile {

	
	
	
	/***************************************************************************
	 * Method Name   : verifyprofessionalRating
	 * Reviewed By   :  
	 * @throws IOException
	 * TM-4055
	 ****************************************************************************
	 */
	
	public static String verifyprofessionalRating() {
		
		String objStatus=null;
		String screenshotName="VerifyprofessionalRating.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
			     + screenshotName;
		

		try {
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtPinotNoir));
		    UIFoundation.waitFor(4L);
		    if (UIFoundation.isElementDisplayed(ListPage.spnProfessionalRatingR)) {
		    	objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.spnProfessionalRatingR));
		    }
			UIFoundation.waitFor(2L);
			if(UIFoundation.isElementDisplayed(ListPage.spnProfessionalRatingHeader)) {
			String	professionalRatingText=UIFoundation.getText(ListPage.spnProfessionalRatingHeader);
				System.out.println("Text displayed in ProfessionalRating header is: "+professionalRatingText);
				objStatus+=true;
				String objDetail="Proffesional header is displayed in list page:";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Proffesional header is not displayed in list page:";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnModalWindowClose));
		     objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkFirstListProd));
		     UIFoundation.waitFor(5L);
		     if (UIFoundation.isElementDisplayed(ListPage.spnProfessionalRatingHeader)) {
			    	objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.spnProfessionalRatingHeader));
			    }
				UIFoundation.waitFor(2L);
				if(UIFoundation.isElementDisplayed(ListPage.spnProfessionalRatingHeader)) {
				String	professionalRatingText2=UIFoundation.getText(ListPage.spnProfessionalRatingHeader);
					System.out.println("Text displayed in ProfessionalRating header: "+professionalRatingText2);
					objStatus+=true;
					String objDetail="Proffesional header is displayed in Pip page:";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}
				else
				{
					objStatus+=false;
					String objDetail="Proffesional header is not displayed in Pip page:";
					  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
					
				}
		     
				if (objStatus.contains("false")) {
					System.out.println("Verify the 'About Professional ratings' header is displayed in the professional ratings modal test case is failed");
					return "Fail";
				} else {
					System.out.println("Verify the 'About Professional ratings' header is displayed in the professional ratings modal test case is executed successfully");
					return "Pass";
				}

			} catch (Exception e) {
				return "Fail";
			}
		}	
	
}
