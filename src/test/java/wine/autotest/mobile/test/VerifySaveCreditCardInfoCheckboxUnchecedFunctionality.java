package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifySaveCreditCardInfoCheckboxUnchecedFunctionality extends Mobile {
	

	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L); 
		//	UIFoundation.waitFor(4L);
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			WebElement element = wait.until(
			        ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));*/
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernametc"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.homePageTitle;
		//	String expectedTile=objExpectedRes.getProperty("homePageTitle");
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		    //	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
	    	log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: verifySaveCrdeitCardInfo()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifySaveCrdeitCardInfo() {

	String objStatus=null;
	String screenshotName = "Scenarios_CreditCard_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;

		try {
			log.info("The execution of the method verifySaveCrdeitCardInfo started here ...");
			UIFoundation.waitFor(4L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
		   objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
		
			
            UIFoundation.waitFor(8L);
                         // PaymentContinue
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
				UIFoundation.waitFor(2L);
			}
			String creditCardCount=UIFoundation.getText(FinalReviewPage.btnPaymentLinksCount);
			int creditCardCntBefore=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
			UIFoundation.waitFor(2L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			  UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
			   UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			   UIFoundation.waitFor(1L);
			} 
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnUncheckCreditCard));
			if(!UIFoundation.isDisplayed(FinalReviewPage.cboMakePreferedCardCheckBoxHidden))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("verified the Make this card preffered check box is hidden when user unchecked Save card information", "Pass", "");
				System.out.println("verified the Make this card preffered check box is hidden when user unchecked Save card information");
			}else{
				objStatus+=false;
				String objDetail="Make this card preffered check box is not hidden when user unchecked Save card information";
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName+"cboMakePreferedCardCheckBoxHidden", objDetail);
			//	UIFoundation.captureScreenShot(driver, screenshotpath+"MakePreferredCheckBox", objDetail);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			AddProdcutsToCartCaptureOrder.addprodTocrt();
			UIFoundation.waitFor(2L);
			
			
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(6L);
                          objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			
		//	driver.navigate().refresh();						
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
				UIFoundation.waitFor(2L);
			}
			creditCardCount=UIFoundation.getText(FinalReviewPage.btnPaymentLinksCount);
			int creditCardCountAfter=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			log.info("The execution of the method verifySaveCrdeitCardInfo ended here ...");
			
			if(creditCardCntBefore==creditCardCountAfter){
				 objStatus+=true;
			     String objDetail="Credit card count is verified";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			
			}else{
				objStatus+=false;
			    String objDetail="Credit card count is not verified";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			
			}
			
			if (objStatus.contains("false")) {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if unchecked test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the 'Save credit card info for next time' check box functionality, if unchecked  test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifySaveCrdeitCardInfo "
					+ e);
			return "Fail";
		}
	}


}
