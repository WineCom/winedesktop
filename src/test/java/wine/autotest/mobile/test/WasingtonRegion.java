package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class WasingtonRegion extends Mobile {



	/***************************************************************************
	 * Method Name			: VerifyPromoBarInWashington()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * TM-3229
	 ****************************************************************************
	 */

	public static String VerifyPromoBarInWashington()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_PromoBar_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of method productFilteration started here");
			driver.navigate().to("https://qwww.wine.com/list/list-washington-wines/wine/1122-7155#");
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkheroImagePlusSymbol));

			UIFoundation.waitFor(5L);
			String	expPromoBar = verifyexpectedresult.actPromoBarText;		 
			String actPromoBar = UIFoundation.getText(ListPage.lnkPromoBarWashingtonReg);
			if(expPromoBar.contains(actPromoBar))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verifiied the promo bar in washington Region", "Pass", "");
				System.out.println("Try vintage product is  added to the cart");

			}
			else
			{
				objStatus+=false;
				String objDetail="Verify the promo bar in washington Region is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName+"washington", objDetail);

			}

			if (objStatus.contains("false"))
			{

				System.out.println(" Verify the 'Promo Bar' in list page when Washington is selected as Region test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println(" Verify the 'Promo Bar' in list page when Washington is selected as Region test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method VerifyPromoBarInWashington "+e);
			return "Fail";
		}
	}
}

