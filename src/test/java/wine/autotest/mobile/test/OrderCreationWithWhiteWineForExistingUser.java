package wine.autotest.mobile.test;


import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;

public class OrderCreationWithWhiteWineForExistingUser extends Mobile {
		
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkChardonnayWine));
			UIFoundation.waitFor(3L);

			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
			}
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
		
	}
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String checkoutProcess() {
	
	String expected=null;	
	String actual=null;
	String objStatus=null;
	String subTotal=null;
	String shippingAndHandling=null;
	String total=null;
	String salesTax=null;
	String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;

		try {
			log.info("The execution of the method checkoutProcess started here ...");
		
			expected = verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(4L);			
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(8L);	
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);				
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);				
			}
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaSriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
			   {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
			   }
			
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(12L);
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				driver.navigate().refresh();
				UIFoundation.waitFor(4L);
			}

			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnMainNavAccTab);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtYourOrder));
			UIFoundation.waitFor(5L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			System.out.println(orderNum);
			System.out.println(UIFoundation.getText(ThankYouPage.lnkOrderDate));
			UIFoundation.getOrderNumber(orderNum);
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				objStatus+=false;
			    String objDetail="Order creation is not done with white wine products";
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("Order creation with White Wine products for existing user test case is failed");
				return "Fail";
			} else {
				objStatus+=true;
			    String objDetail="Order creation is done with white wine products";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order creation with White Wine products for existing user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			return "Fail";
		}
	}	
}
