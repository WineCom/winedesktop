package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk extends Mobile {
	
	 

	/************************************************************************************************
	 * Method Name : verifyUserIsAbleToEnrollForSubscription
	 * Created By  : Rakesh 
	 * Reviewed By : Chandrashekhar
	 * Purpose     : This method allows user to enroll for subscription flow under picked wines
	 * 
	 ************************************************************************************************/

	public static String verifyDetailedTermsDispOnClickingTermsLnk() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtCity, "City"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnReceiveState,"State"));
			UIFoundation.clickObject(PickedPage.dwnReceiveState);
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtReceipentZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(PickedPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnShipContinue));
			UIFoundation.waitFor(11L);
			if(UIFoundation.isDisplayed(PickedPage.btnVerifyContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(PickedPage.btnVerifyCont, "Invisible", "", 50); 

			}
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(PickedPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(PickedPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(PickedPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(PickedPage.dwnSelectShippingMethod, "Invisible", "", 50);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
					//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(13L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkTermsAndCond));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.lnkCompleteTermsAndCond)) {
				objStatus+=true;
				String objDetail="'Terms & Conditions' popup is displayed.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="'Terms & Conditions' popup is not displayed.";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkCompleteTermsAndCond));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.txtHeaderCompleteTerms)) {
				objStatus+=true;
				String objDetail="Detailed 'Terms & Conditions' page is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Detailed 'Terms & Conditions' page is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Verify Detailed Terms And Conditions Page Is Disp On Clicking Read Terms Lnk test case failed");
				String objDetail="Verify Detailed Terms And Conditions Page Is Disp On Clicking Read Terms Lnk test case failed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				System.out.println("Verify Detailed Terms And Conditions Page Is Disp On Clicking Read Terms Lnk test case executed successfully");
				String objDetail="Verify Detailed Terms And Conditions Page Is Disp On Clicking Read Terms Lnk test case executed successfully test case executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

}
