package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyMyWineSignInNEditLinkForGiftMessage extends Mobile {



	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh,
	 * Purpose     : TM-4132
	 ****************************************************************************
	 */

	public static String signInModalForMyWinePage() {

		String objStatus = null;
		String ScreenshotName = "signInModalForMyWinePage.jpeg";
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;

		try {
			log.info("sign In Modal For My Wine Page method started here........");
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.btnObjMyWine));
			if (UIFoundation.isElementDisplayed(LoginPage.txtSignInTextR)
					&& UIFoundation.isElementDisplayed(LoginPage.btnSignInBtnR)) {
				objStatus += true;
				String objDetail = "Sign modal is displayed when clicks on Mywie";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywie";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);

			}
			log.info("sign In Modal For My Wine Page method ended here........");

			if (objStatus.contains("false")) {

				System.out.println("signInModalForMyWinePage test case is failed");

				return "Fail";
			} else {
				System.out.println("signInModalForMyWinePage test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}
	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : TM-4139
	 ****************************************************************************
	 */
	
	public static String editOptionForGiftMessage() {

		String objStatus = null;
		String ScreenshotName = "editOptionForGiftMessage.jpeg";
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;
		String addToCart1, addToCart2;

		try {
			log.info("edit Option For Gift Message method started here.......");
			driver.navigate().refresh();
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "userGiftMessage"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.btnFirstProductToCart);
			}
			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				UIFoundation.clckObject(ListPage.btnSecondProductToCart);
			}

			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(5L);			
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());

		//	if (UIFoundation.isDisplayed(FinalReviewPage.spnrecipientChangeAddress));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
			UIFoundation.clearField(FinalReviewPage.txtRecipientEmail);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientEmail, "addsaveolyonce"));
			UIFoundation.clearField(FinalReviewPage.txtRecipientGiftMessage);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientGiftMessage, "giftMessage"));	
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboRecipientGiftAdd));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(4L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.btnRecipientedit)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientedit));
				objStatus += true;
				String objDetail = "Edit button is displayed next to gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Edit button is  not displayed next to gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.clickObject(FinalReviewPage.imgNoGiftBag);
			UIFoundation.clickObject(FinalReviewPage.btnReciptContinue);
			log.info("edit Option For Gift Message method ended here.......");
			if (objStatus.contains("false")) {
				System.out.println("editOptionForGiftMessage test case is failed");
				return "Fail";
			} else {
				System.out.println("editOptionForGiftMessage test case  executed succesfully");
				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}
}

