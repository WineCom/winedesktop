package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyRatingOptionIsDisplayedInSliderBar extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: VerifyRatingOptionIsDisplayedInSliderBar()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String verifyRatingOptionIsDisplayedInSliderBar() {
		String objStatus=null;
		   String screenshotName = "Scenarios_SlidingBar_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of method verifyRatingOptionIsDisplayedInSliderBar started here");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtBordexBlends));
			UIFoundation.waitFor(5L);			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.cboSelectRatingAndPrice));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.cboSliderBarRating) && UIFoundation.isDisplayed(CartPage.cboSliderBarPricing))
			{
				  objStatus+=true;
			      String objDetail="Rating option is displayed in slider bar under filter section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				 String objDetail="Rating option is not displayed in slider bar under filter section";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyRatingOptionIsDisplayedInSliderBar ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify Rating option is displayed in slider bar under filter section  test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Rating option is displayed in slider bar under filter section test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Existing customer should be a link in new customer register page  test case is failed");
			log.error("there is an exception arised during the execution of the method verifyRatingOptionIsDisplayedInSliderBar "
					+ e);
			return "Fail";
		}
	}
}
