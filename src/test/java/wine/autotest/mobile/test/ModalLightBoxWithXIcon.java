package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ModalLightBoxWithXIcon extends Mobile {

	
	/***************************************************************************
	 * Method Name			: modalLightBoxWithXIcon()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String modalLightBoxWithXIcon() {
		String objStatus=null;
		   String screenshotName = "Scenarios__lightBox.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtSearchProduct, "lightBoxProduct"));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtSearchTypeListWill));
			UIFoundation.waitFor(7L);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtAllVintages);
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(CartPage.txtAllVintages)){
				 objStatus+=true;
			      String objDetail="'All vintages' Link is displayed in PIP";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				 objStatus+=false;
				 String objDetail="'All vintages' Link is not displayed in PIP";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnCarouselContentItem));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.lnklightBoxCloseIcon)){
				UIFoundation.waitFor(3L);
				UIFoundation.escapeBtn(driver);
				UIFoundation.waitFor(2L);
				if(!UIFoundation.isDisplayed(CartPage.lnklightBoxCloseIcon)){
					 objStatus+=true;
				      String objDetail="Modal/Light Box closes on clicking 'ESC' key when the Modal/Light box has ' X' icon";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					
				}else{
					 objStatus+=false;
					   String objDetail="Modal/Light Box is not closed on clicking 'ESC' key when the Modal/Light box has ' X' icon";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
				}
			}else{
				objStatus+=false;
				   String objDetail="Light Box close icon is not displayed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			       
			}
		
			UIFoundation.scrollUp(driver);
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name			: lightBoxInRecipientPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String lightBoxInRecipientPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios__LightBoxWindow.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;
					//WebDriverWait wait=null;
		try {
			log.info("The execution of the method lightBoxInRecipientPage started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));		
			if(UIFoundation.isDisplayed(FinalReviewPage.spnRecipientChangeAddress)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnRecipientChangeAddress));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShippingAddressEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.clearField(FinalReviewPage.txtShippingAddressFullNameClear);
			UIFoundation.waitFor(2L);
			UIFoundation.clckObject(FinalReviewPage.btnShippingOptionalClk);

			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtShippingAddressFullNameEnter, "firstName"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnModalWindowIcon)){
				UIFoundation.waitFor(3L);
				UIFoundation.escapeBtn(driver);
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkShippingAddressEdit)){
					 objStatus+=true;
				      String objDetail="Modal/Light Box closes on clicking 'ESC' key for the Modal/Light box with  ' X' icon";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else{
					 objStatus+=false;
					   String objDetail="Modal/Light Box does not closed on clicking 'ESC' key for the Modal/Light box with  ' X' icon";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"modalIcon", objDetail);
				}
			}else{
				   objStatus+=false;
				   String objDetail="Light Box close icon is not displayed";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			}

			log.info("The execution of the method lightBoxInRecipientPage ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
		
			log.error("there is an exception arised during the execution of the method lightBoxInRecipientPage "+ e);
			return "Fail";
		}
	}	
}
