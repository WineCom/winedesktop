
package wine.autotest.mobile.test;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;


public class AddProdcutsToCartCaptureOrder extends Mobile {
	

	static boolean isObjectPresent=false;
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		try {
		
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtsearchProduct));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "Wind"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
			UIFoundation.waitFor(12L);
			// System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
			// UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			// System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}
						
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		String addToCart6 = null;
		try {
			UIFoundation.waitFor(1L);
			
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			 {
			    driver.navigate().refresh();
			    UIFoundation.waitFor(4L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
				UIFoundation.waitFor(1L);
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
				UIFoundation.waitFor(1L);
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
				UIFoundation.waitFor(1L);
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
				UIFoundation.waitFor(1L);
			}

			UIFoundation.waitFor(2L);		
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForExistingUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForExistingUser() {
		String products1=null;
		
		try {
			
			System.out.println("=======================Products Added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);

			}
			else
			{
				System.out.println("No products are added into the cart");
			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("=======================Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: productDetailsPresentInCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productDetailsPresentInCart() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 *//*
	
	public static String checkoutProcess(WebDriver driver) {
	
	String expected=null;
	String actual=null;
	String objStatus=null;
	String subTotal=null;
	String shippingAndHandling=null;
	String total=null;
	String salesTax=null;
	
	String screenshotName = "Scenarios_FinalReview_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;

	try {
		log.info("The execution of the method checkoutProcess started here ...");
		expected=objExpectedRes.getProperty("placeOrderConfirmation");
		UIFoundation.waitFor(4L);
		
		if(UIFoundation.isDisplayed(driver, "CheckoutButton"))
		{
		objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CheckoutButton"));
		}else {
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "obj_Checkout"));
		}
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
		UIFoundation.waitFor(12L);
		if(UIFoundation.isDisplayed(driver, "DeliveryContinue"))
		{
			//ShipContinueButton
			UIFoundation.scrollDownOrUpToParticularElement(driver, "DeliveryContinue");
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "DeliveryContinue"));
			UIFoundation.webDriverWaitForElement(driver, "DeliveryContinue", "Invisible", "", 50);
			UIFoundation.waitFor(8L);
		}
		
		if(UIFoundation.isDisplayed(driver, "changePayment"))
		{
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "changePayment"));
			UIFoundation.waitFor(3L);
		//	PaymentContinue
		}
		if(!UIFoundation.isSelected(driver, "SelectPayWithThisCard")){
			  objStatus+=String.valueOf(UIFoundation.clickObject(driver, "SelectCreditCard"));
			  UIFoundation.waitFor(1L);
			
		  }
		  UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
		  objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
		//  objStatus+=String.valueOf(UIFoundation.setObject(driver, "paywithThisCardCvv", "CardCvid"));
        //  UIFoundation.scrollDownOrUpToParticularElement(driver, "paywithThisCardSave");
		
			
			 if(!UIFoundation.isSelected(driver, "SelectPayWithThisCard")){
				  objStatus+=String.valueOf(UIFoundation.clickObject(driver, "SelectCreditCard"));
				  UIFoundation.waitFor(5L);
			  }
			 
          objStatus+=String.valueOf(UIFoundation.clickObject(driver, "paywithThisCardEdit"));
          objStatus+=String.valueOf(UIFoundation.setObject(driver, "paywithThisCardCvv", "CardCvid"));
          UIFoundation.scrollDownOrUpToParticularElement(driver, "paywithThisCardSave");
          UIFoundation.waitFor(1L);
          objStatus+=String.valueOf(UIFoundation.clickObject(driver, "paywithThisCardSave"));
          UIFoundation.waitFor(4L);

          
          if(UIFoundation.isDisplayed(driver, "Subtotal"))
          {
                objStatus+=true;
                String objDetail="User is able to navigate directly to Final review page on entering 'CVV' in 'Edit credit card' popup";
                ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
                System.out.println("User is able to navigate directly to Final review page on entering 'CVV' in 'Edit credit card' popup ");   
          }else{
                 objStatus+=false;
                 String objDetail="User is Not able to navigate directly to Final review page on entering 'CVV' in 'Edit credit card' popup";
                 //ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
                 UIFoundation.captureScreenShot(driver, screenshotpath+"final", objDetail);
          }
        
		if(UIFoundation.isDisplayed(driver, "AddNewCard"))
        {
 			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "AddNewCard"));
        }
          if(UIFoundation.isDisplayed(driver, "NameOnCard"))
          {
             objStatus+=String.valueOf(UIFoundation.setObject(driver, "NameOnCard", "NameOnCard"));
             objStatus+=String.valueOf(UIFoundation.setObject(driver, "CardNumber", "Cardnum"));
             UIFoundation.waitFor(2L);
             objStatus+=String.valueOf(UIFoundation.setObject(driver,"ExpiryMonth","Month"));
             objStatus+=String.valueOf(UIFoundation.setObject(driver,"ExpiryYear","Year"));
             objStatus+=String.valueOf(UIFoundation.setObject(driver, "CVV", "CardCvid"));
             UIFoundation.waitFor(3L);
             
            WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(driver, "BillingAndShippingCheckbox");	
		    UIFoundation.javaScriptClick(driver, "BillingAndShippingCheckbox");
		    UIFoundation.waitFor(3L);
		    UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
	          UIFoundation.waitFor(1L);
	          objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
	          UIFoundation.webDriverWaitForElement(driver, "PaymentContinue", "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(driver, "birthMonth"))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthMonth","birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(driver,"birthDate","birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(driver, "birthYear", "birthYear"));
			}
			
			if(UIFoundation.isDisplayed(driver, "PaymentContinue"))
			{	
			UIFoundation.scrollDownOrUpToParticularElement(driver, "PaymentContinue");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PaymentContinue"));
			UIFoundation.waitFor(14L);
			}
                 
          }
        
		
		System.out.println("============Order summary in the Final Review Page  ===============");
		subTotal=UIFoundation.getText(driver, "Subtotal");
		shippingAndHandling=UIFoundation.getText(driver, "obj_Shipping&Hnadling");
		total=UIFoundation.getText(driver, "TotalBeforeTax");
		salesTax=UIFoundation.getText(driver, "OrderSummaryTaxTotal");
		System.out.println("Subtotal:              "+subTotal);
		System.out.println("Shipping & Handling:   "+shippingAndHandling);
		System.out.println("Sales Tax:             "+salesTax);
		System.out.println("Total:                 "+total);
		UIFoundation.waitFor(3L);
		UIFoundation.scrollDownOrUpToParticularElement(driver, "PlaceOrderButton");
		UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.clickObject(driver, "PlaceOrderButton"));
		UIFoundation.webDriverWaitForElement(driver, "PlaceOrderButton", "Invisible", "", 50);
		String orderNum=UIFoundation.getText(driver,"OrderNum");
		
		if(orderNum!="Fail")
		{
	      objStatus+=true;
	      String objDetail="Order number is placed successfully";
	      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
	      System.out.println("Order number is placed successfully: "+orderNum);   
		}else
		{
	       objStatus+=false;
	       String objDetail="Order number is null and cannot placed successfully";
	       UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
		}
		log.info("The execution of the method checkoutProcess ended here ...");
		
		
		 if(objStatus.contains("false")) {
			System.out.println("Order creation with existing account test case is failed");
			return "Fail";
		} else {
			System.out.println("Order creation with existing account test case is executed successfully");
			return "Pass";
		}
	} catch (Exception e) {
		
		log.error("there is an exception arised during the execution of the method getOrderDetails "
				+ e);
		return "Fail";
	}
}*/


}
