package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyShippingCostAppliedToOrderSummary extends Mobile {


	/***************************************************************************
	 * Method Name : VerifyShippingCostAppliedToOrderSummary
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : TM-4189
	 ****************************************************************************
	 */

	public static String shippingCostAppliedToOrderSummary() {

		String objStatus = null;
		String screenshotName = "shippingCostAppliedToOrderSummary.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("shipping Cost Applied To Order Summary metho started here...");
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboSelectShippingMethodOption));
			UIFoundation.waitFor(3L);
			String overnightpm = UIFoundation.getText(FinalReviewPage.cboShippingmethodPmtext);
			String overNgtShipPrice = (overnightpm.substring(16));
			System.out.println("Over Nigt Pm Shipping method total is "+overNgtShipPrice);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCvvR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "paywithcardR"));
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(2L);
			}	
			String shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			if(overNgtShipPrice.equals(shippingAndHandling)) {
				objStatus += true;
				String objDetail = "Over Nigt PM Shipping total is added to order summary";
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Over Nigt PM Shipping total is not  added to order summary";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboSelectShippingMethodOption2));
			UIFoundation.waitFor(3L);
			String overnightam = UIFoundation.getText(FinalReviewPage.cboShippingmethodAmtext);
			String overNgamtShipPrice = (overnightam.substring(16));
			System.out.println("Over Nigt AM Shipping method total is "+overNgamtShipPrice);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(3L);
			shippingAndHandling =UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			if(overNgamtShipPrice.equals(shippingAndHandling)) {
				objStatus += true;
				String objDetail = "Over Nigt AM Shipping total is added to order summary";
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Over Nigt AM Shipping total is not  added to order summary";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyGiftWrapping ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftWrapping " + e);
			return "Fail";
		}

	}
}
