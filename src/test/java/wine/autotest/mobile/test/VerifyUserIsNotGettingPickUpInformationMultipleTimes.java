package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyUserIsNotGettingPickUpInformationMultipleTimes extends Mobile {

	
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "localpickup"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.shoppingCartPageTitle;			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
		    }
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	

	/*****************************************************************************************
	 * Method Name : VerifyUserIsNotGettingPickUpInformationMultipleTimes() 
	 * Created By : chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose : 
	 * Jira Id : TM-4175
	 *******************************************************************************************
	 */
	/*public static String addFedExAddress(WebDriver Driver) {
		String actualFedexDeliveyMessage= null;
		String expectedFedexDeliveyMessage=null;
		String  objStatus = null;
		String ScreenshotName= "VerifyLocalPickupInformationDisplayed.jpeg";
		String Screenshotpath =System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		try
		{
			log.info("FEDEX Delivery message Execution method started here:");
			if(UIFoundation.isDisplayed(driver, "CheckoutButton"))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "CheckoutButton"));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "obj_Checkout"));
			}
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "ShipToFedex"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "ShipToFedexZip", "ZipCode"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "FedexSearchButton"));
			UIFoundation.waitFor(3L);
			//objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "RecipientShipToaddress"));
			objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "ShipToThisLocarion"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "FedexFirstName", "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "FedexLastName", "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "FedexStreetAddress", "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "FedexCity", "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "FedexZip", "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(Driver, "FedexPhoneNum", "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "FedexSaveButton"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "RecipientContinue"));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(Driver, "DeliveryContinue"));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isElementDisplayed(Driver, "FedEXShippingDeliveryMessage")){
				actualFedexDeliveyMessage=UIFoundation.getText(Driver, "FedEXShippingDeliveryMessage");				
			}
			expectedFedexDeliveyMessage = objExpectedRes.getProperty("FedexDeliveryMessageR");		
			if(actualFedexDeliveyMessage.contains(expectedFedexDeliveyMessage)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the FedexPickup Message in Delivery Section", "Pass", "");
				System.out.println(actualFedexDeliveyMessage);
			}
			else
			{
				objStatus+=false;
				String objDetail="Verified the FedexPickup Message in Delivery Section is not displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("FEDEX Delivery message Excution method started here:");
			if(objStatus.contains("false")){


				System.out.println("Verified the FedexPickup Address Message in Delivery Section testcase failed");

				return "Fail";
			}
			else {


				System.out.println("Verified the FedexPickup Address Message in Delivery Section testcase succesfully");

				return "Pass";

			}

		}catch(Exception e) {
			return "Fail";
		}

	}*/
	
	
	/***************************************************************************
	 * Method Name			: verifyPickUpInformation()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyPickUpInformation() {
		String objStatus=null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method editHomeAddress started here ...");
			UIFoundation.waitFor(2L);		
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}	
			
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkOriginalAddressSelectRadioButton));	
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtAddNewAddressLink))
			{
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkOriginalAddressSelectRadioButton));
			}			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnShipContinue))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));	
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));	
			}

			log.info("The execution of the method editHomeAddress ended here ...");
			if ( objStatus.contains("false")) {
				
				objStatus+=false;
				String  objDetail="User is not prompted to the next step without getting Pick Up Location Dialog displayed";
				ReportUtil.addTestStepsDetails(objDetail, "false", "");
				return "Fail";
			} else {
				
				objStatus+=true;
				String  objDetail="User is prompted to the next step without getting Pick Up Location Dialog : succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editHomeAddress "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";
			 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}	
}
