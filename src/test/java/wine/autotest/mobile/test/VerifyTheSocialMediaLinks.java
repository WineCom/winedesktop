package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheSocialMediaLinks extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyTheSocialMediaLinks()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyTheSocialMediaLinks()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_SocialMedisLink.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			log.info("The execution of the method verifyTheSocialMediaLinks started here ...");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkBlog);
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.lnkBlog))
			{
				objStatus+=true;
				String objDetail="Blog link is displayed ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Blog link is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkFaceBook))
			{
				objStatus+=true;
				String objDetail="Facebook link is displayed ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Facebook link is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkTwitter))
			{
				objStatus+=true;
				String objDetail="Twitter link is displayed ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Twitter link is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkPinterest))
			{
				objStatus+=true;
				String objDetail="PinterestLink link is displayed ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="PinterestLink link is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnkInstagram))
			{
				objStatus+=true;
				String objDetail="Instagram link is displayed ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Instagram link is not displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyTheSocialMediaLinks ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
				String objDetail="All social media links are not displayed in the footer is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
							
				System.out.println("Verify the social media links are not displayed in the footer is failed");
				return "Fail";
			}
			else
			{
				objStatus+=true;
				String objDetail="All The social media links are  displayed succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
							
				System.out.println("Verify the social media links are displayed in the footer is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifyTheSocialMediaLinks "+ e);
			return "Fail";
			
		}
	}	
	
}
