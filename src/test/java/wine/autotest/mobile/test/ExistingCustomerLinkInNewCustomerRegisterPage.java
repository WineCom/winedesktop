package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ExistingCustomerLinkInNewCustomerRegisterPage extends Mobile {
	
	

	
	/***************************************************************************
	 * Method Name			: ExistingCustomerLinkInNewCustomerRegisterPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String existingCustomerLinkInNewCustomerRegisterPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios_SignInNav_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of method existingCustomerLinkInNewCustomerRegisterPage started here");
				
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(5L);
		
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(LoginPage.txtSignIn))
			{
				  objStatus+=true;
			      String objDetail="on clicking on the Sign In link user is navigated to the sign in page.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				 String objDetail="on clicking on the Sign In link user is not navigated to the sign in page.";			
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method existingCustomerLinkInNewCustomerRegisterPage ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Existing customer should be a link in new customer register page  test case is failed");
				return "Fail";
			} else {
				System.out.println("Existing customer should be a link in new customer register page  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Existing customer should be a link in new customer register page  test case is failed");
			log.error("there is an exception arised during the execution of the method existingCustomerLinkInNewCustomerRegisterPage "
					+ e);
			return "Fail";
		}
	}
}
