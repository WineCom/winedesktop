package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyChooseStateDialogPrompted extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		boolean isElementPresent=false;
		
		String screenshotName = "Scenarios_ChooseState_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
			}
			UIFoundation.waitFor(3L);
			boolean elementPresent=UIFoundation.isDisplayed(ListPage.cboChooseYourState);
			if(elementPresent==true){
				   objStatus+=false;
			       String objDetail="Choose state is displayed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}else{
				 objStatus+=true;
			      String objDetail="Choose state is not displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			
			if (objStatus.contains("false")) {
				System.out.println("Verify 'Choose state' dailog is prompted test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify 'Choose state' dailog is prompted test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	


}
