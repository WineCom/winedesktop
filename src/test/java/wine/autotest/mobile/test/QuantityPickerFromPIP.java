package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class QuantityPickerFromPIP extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyQuantityPickerInCartPage() {
        String objStatus=null;
        //String actualQuantity=null;
        //int expectedQuantity=null;
        String screenshotName = "Scenarios_QuantityPick_Screenshot.jpeg";
        String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
        try {
        	log.info("The execution of the method searchProductWithProdName started here ...");
            objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtSearchBarFormInput));
            UIFoundation.waitFor(1L);
            objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtSearchBarFormInput, "Wind"));
            UIFoundation.waitFor(7L);
            objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.lnkSearchTypeListMobile));
            UIFoundation.waitFor(4L);
            UIFoundation.getLastOptionFromDropDown(CartPage.spnPIPAddCartQuantity);
      //    objStatus+=String.valueOf(UIFoundation.SelectObject(driver, "PIPAddCartQuantity", "quantityPIP"));
            UIFoundation.waitFor(4L);
            int expectedQuantity=Integer.parseInt(UIFoundation.getFirstSelectedValue(CartPage.spnPIPAddCartQuantity));
            UIFoundation.waitFor(3L);
            objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnAddToCart));
      //    System.out.println(UIFoundation.getText(driver,"AddAgain"));
            objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCartCount));
            UIFoundation.waitFor(3L);
      //    int qunatity  = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName, "quantityPIP", 1));
            WebElement drop_down =driver.findElement(By.xpath("//select[@class='productQuantity_select']"));
               
            Select se = new Select(drop_down);
          
         //   int actualQuantity=Integer.parseInt(se.getOptions().get(expectedQuantity).getAttribute("productQuantity_selectLength"));
         int actualQuantity=Integer.parseInt(se.getOptions().get(expectedQuantity).getAttribute("value"));
            //actualQuantity=UIFoundation.getFirstSelectedValue(driver, "ProductQuantitySel");
            UIFoundation.waitFor(1L);
       //     if(expectedQuantity==(actualQuantity)+1)
            	  if(expectedQuantity==(actualQuantity))
            {
                  objStatus+=true;
                  String objDetail="Selected quantity in PIP page is matched with the cart page quantity";
                  ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
                  System.out.println("Selected quantity in PIP page is matched with the cart page quantity");
            }
            else
            {
                  objStatus+=false;
            String objDetail="Selected quantity in PIP page does not match with the cart page quantity";
                  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
                  System.out.println("Selected quantity in PIP page does not match with the cart page quantity");
            }
            log.info("The execution of the method searchProductWithProdName ended here ...");
            if(objStatus.contains("false"))
            {
                  
                  return "Fail";
                  
            }
            else
            {
                  
                  return "Pass";
            }
            
      } catch (Exception e) {
            System.out.println(" Search product with product name test case is failed");
            log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
            return "Fail";
      }

	}	

}