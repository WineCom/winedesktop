package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheRecipientSectionUIUnderSubscriptionFlow extends Mobile {



	/*************************************************************************************************
	 * Method Name : addPickedByWineComSubscription() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose     : The purpose of this method is to navigate and 'Proceed to Enrollment' 
	 * 
	 **************************************************************************************************
	 */

	public static String verifyRecipientSectionAfterSubscriptionFlow() {

		String objStatus = null;
		String screenshotName = "Scenarios_addPickedByWineComSubscription_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method addPickedByWineComSubscription started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isEnabled(PickedPage.spnShippingAddrRadioButtonShipToFedex)) {
				objStatus+=true;
				String objDetail="Ship to a Local Pickup Location checkbox is checked by default";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Ship to a Local Pickup Location checkbox is not checked by default";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
								
			UIFoundation.waitFor(5L);
			
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtAdultSig));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtOrderArrival));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.spnShippingAddrRadioButtonShipToHome));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.spnShippingAddrRadioButtonShipToFedex));
		    //skipped 'Wine.com local pickup' Section Should be displayed
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtShipToFedexZip));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.btnFedexSearchButton));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.spnShippingAddrRadioButtonShipToHome));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtAdultSig));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.spnShippingAddrRadioButtonShipToHome));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.spnShippingAddrRadioButtonShipToHome));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtFirstName));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtLastName));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.lblCompName));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtStreetAddress));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtCity));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.dwnReceiveState));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtPhoneNum));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.btnHomeAddrCont));
			
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify The Recipient Section UI Under Subscription Flow test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify The Recipient Section UI Under Subscription Flow test case executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method  "+ e);
			return "Fail";
		}
	}	
}
