package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;



import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyItemWithoutQuantityPicker extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : addStewardshipToCart()
	 * Created By : Chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyQuantityPicker() {
		String objStatus = null;
		
		String screenshotName = "Scenarios_QuantityPicker_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method verifyQuantityPicker started here ...");
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.spnFifthProductSaveFor).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFifthProductSaveFor));
				UIFoundation.waitFor(2L);
			}
			
			if(!UIFoundation.getText(CartPage.spnFourthProductSaveFor).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFourthProductSaveFor));
				UIFoundation.waitFor(3L);
			}
			
			if(!UIFoundation.getText(CartPage.spnThirdProductSaveFor).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnThirdProductSaveFor));
				UIFoundation.waitFor(3L);
				
			}
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtShoppingCartHeader);
			UIFoundation.waitFor(2L);
			if(!UIFoundation.getText(CartPage.spnSecondProductSaveFor).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnSecondProductSaveFor));
				UIFoundation.waitFor(3L);
				
			}
			
			if(!UIFoundation.getText(CartPage.spnFirstProductSaveFor).contains("Fail"))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFirstProductSaveFor));
				UIFoundation.waitFor(2L);
				
			}
			boolean isPresent=UIFoundation.isDisplayed(CartPage.cboProductQuantitySel);
			
			log.info("The execution of the method verifyQuantityPicker ended here ...");
			
			if(isPresent==true){
				 objStatus+=false;
			     String objDetail="Item is without quantity picker is not verified";
			     UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}else{
				objStatus+=true;
			    String objDetail="Item is without quantity picker is verified";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			
			if (objStatus.contains("false")) {
				
				System.out.println("Verify item without quantity picker test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify item without quantity picker test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyQuantityPicker "+ e);
			return "Fail";
		}
	}

}
