package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySortFunctionalitiesUnderMyWine extends Mobile {

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login(WebDriver driver)
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 	
				
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.homePageTitle;
			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		       	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: addMyWineProdTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	/*public static String addMyWineProdTocrt(WebDriver driver) {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "obj_MyWine"));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "myWineSelectSort"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "purchaseDateNewToOld"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(ApplicationDependent.validationForMyWineSortingPurchaseDateNewToOld(driver));
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}*/
	

	/***************************************************************************
	 * Method Name			: sortScannedOldToNew()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	/*public static String sortScannedOldToNew(WebDriver driver) {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "obj_MyWine"));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "myWineSelectSort"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "scannedDateOldToNew"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(ApplicationDependent.validationForMyWineSortingScannedDateOldToNew(driver));
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}*/
	
	/***************************************************************************
	 * Method Name			: sortScannedNewToOld()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	/*public static String sortScannedNewToOld(WebDriver driver) {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "obj_MyWine"));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "myWineSelectSort"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "scannedDateNewToOld"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(ApplicationDependent.validationForMyWineSortingScannedDateNewToOld(driver));
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}*/
	
	/***************************************************************************
	 * Method Name			: sortMyRatingHtoL()
	 * Created By			: Chandra shekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	/*public static String sortMyRatingHtoL(WebDriver driver) {
		String objStatus = null;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "obj_MyWine"));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "myWineSelectSort"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "scannedMyRatingHToL"));
			UIFoundation.waitFor(2L);
		//	objStatus += String.valueOf(ApplicationDependent.validationForMyRatingHighToLow(driver));
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}*/
	/***************************************************************************
	 * Method Name			: addMyWineRecentActivity()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
/*	public static String addMyWineRecentActivity(WebDriver driver) {
		String objStatus = null;
		  String screenshotName = "Scenarios_twoGiftCert_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of the method addMyWineRecentActivity started here ...");
			
			if(!UIFoundation.isDisplayed(driver, "MainNavButton"))
			 {
			    driver.navigate().refresh();
			    UIFoundation.waitFor(2L);
			 }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "CaberNet"));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(driver, "pipProduct");
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "pipProduct"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "rateProduct"));
			UIFoundation.waitFor(2L);
			String pipName=UIFoundation.getText(driver, "productName");
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "AddToCartButton"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "obj_MyWine"));
			UIFoundation.waitFor(4L);
			String pipNameSfterAddToMyWine=UIFoundation.getText(driver, "myWineproductName");
			UIFoundation.waitFor(3L);
			if(pipName.equalsIgnoreCase(pipNameSfterAddToMyWine))
			{
				objStatus+=true;
				String objDetail="Verified product added for the addMyWineRecentActivity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "pipProd"));
				UIFoundation.waitFor(2L);	
				objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "userRating"));
				objStatus += String.valueOf(UIFoundation.mouseHover(driver, "userRating"));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "clearStarRating"));
				UIFoundation.waitFor(2L);	
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "AddToCartButton"));			
		}else{
			objStatus+=false;
			String objDetail="Verified product is not added for the addMyWineRecentActivity";
			//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
			UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
		}
			
			log.info("The execution of the method addMyWineRecentActivity ended here ...");				
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}*/	
}
