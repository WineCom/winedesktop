package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyStewardshipDiscountDisplayedNegetiveInHistory extends Mobile {	
	
	
	/***************************************************************************
	 * Method Name			: stewardLogin()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String stewardLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "passwordStewrd"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: productIconCheck()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: To check product icon attribute in PIP
	 * 				
	 ****************************************************************************
	 */
	
	public static String productIconCheck(WebDriver driver)
	{
		String objStatus=null;
		String screenshotName = "Scenarios_CreditCard_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method productIconCheck started here ...");
			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexBlends));
			UIFoundation.waitFor(2L);
			
			if(!UIFoundation.isDisplayed(ListPage.btnProductAttributeIcon)){
				
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Product Attribute icons are not displayed in list", "Pass", "");
				System.out.println("Product Attribute icons are not displayed in list");
			
			}else{
				objStatus+=false;
				String objDetail="Product Attribute icons are displayed in list";			
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"List", objDetail);
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtProductOne));
			UIFoundation.waitFor(8L);
			
			if(UIFoundation.isDisplayed(ListPage.btnProductAttributeIcon)){
				
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Product Attribute icons are displayed in PIP page", "Pass", "");
				System.out.println("Product Attribute icons are displayed in PIP page");
			
			}else{
				objStatus+=false;
				String objDetail="Product Attribute icons are not displayed in PIP";				
			UIFoundation.captureScreenShot(screenshotpath+screenshotName+"List", objDetail);
			}
			
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgWineLogo));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method productIconCheck ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("productIconCheck test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("productIconCheck test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method productIconCheck "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart(WebDriver driver) {
		
		
		String objStatus=null;
		try {
			log.info("The execution of the method addProductsToCart started here ...");
		//	UIFoundation.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSrt);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			log.info("The execution of the method addProductsToCart ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary(WebDriver driver)
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String stewardshipSaving=null;
		String shippingHandling=null;
		String stewardshipSave=null;
		

		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying Stewardship code===============");
			objStatus+=String.valueOf(subTotal=UIFoundation.getText(CartPage.spnSubtotal));
			objStatus+=String.valueOf(shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling));
			objStatus+=String.valueOf(totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax));
			objStatus+=String.valueOf(stewardshipSaving=UIFoundation.getText(CartPage.txtStewardshipSaving));
			stewardshipSave=stewardshipSaving.replaceAll("[^0-9]", "");
			shippingHandling=shippingAndHandling.replaceAll("[^0-9]", "");
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			
			if(shippingHandling.equals(stewardshipSave))
			{
				System.out.println("Stewardship Savings:      "+stewardshipSaving);
				System.out.println("Stewardship amount is discounted successfully");
				objStatus+=true;
			    String objDetail="Stewardship amount is discounted successfully";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				System.out.println("Stewardship amount is not discounted");
				objStatus+=false;
			    String objDetail="Stewardship amount is not discounted";			   
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	public static String shippingDetails() {
	String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
					
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(3L);
		
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinueShipRecpt);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueShipRecpt)){
		
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueShipRecpt));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueShipRecpt, "Clickable", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
	
		String stewardShipDiscount=null;
		String stewardShipValue="-";
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
    	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
		+ screenshotName;
		try {			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
				UIFoundation.waitFor(1L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(10L);
            if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
            {
                  UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
                  UIFoundation.waitFor(5L);                  
            }                                        
            objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.txtOrderNum));
            UIFoundation.waitFor(1L);
            objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtStewardshipDiscountPrice));
			UIFoundation.waitFor(1L);				
			stewardShipDiscount=UIFoundation.getText(FinalReviewPage.txtStewardshipDiscountPrice);
			if(stewardShipDiscount.contains(stewardShipValue))
			{
				objStatus+=true;
				String objDetail = "Stewardship discount displayed as negetive in order history";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		    			
			}
			else
			{
				objStatus+=false;
		    	String objDetail="Stewardship discount not displayed as negetive in order history";
		    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With Edit Functionality In Final Review Page For New User test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With Edit Functionality In Final Review Page For New User test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
}
