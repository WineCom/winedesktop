package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyOrderHistoryPage extends Mobile {



	/***************************************************************************
	 * Method Name			: OrderHistoryProgressBarValiadtion()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: TM-4143
	 ****************************************************************************
	 */

	public static String OrderHistoryProgressBarValidation() {

		String expected=null;
		String actual=null;
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;

		String screenshotName = "Scenarios_OrderHistoryProgressBarValiadtion_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method OrderHistoryProgressBarValiadtion started here ...");			
			expected = verifyexpectedresult.placeOrderConfirmation;	
			UIFoundation.waitFor(4L);

			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(12L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{

				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
				UIFoundation.waitFor(8L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtAddNewCard));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);

				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
					UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}

				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}

				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.waitFor(14L);
				}
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);

			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: "+orderNum);   
			}else
			{
				objStatus+=false;
				String objDetail="Order number is null and cannot placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtOrderNum));
			UIFoundation.waitFor(20L);
			if(UIFoundation.isElementDisplayed(ThankYouPage.txtProgressBarOrderHistory)) {
				objStatus += true;
				String objDetail = "Order Progress Bar in Order History page is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");

			}else {
				objStatus += false;
				String objDetail = "Order Progress Bar in Order History Page is not Displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method OrderHistoryProgressBarValidation ended here ...");


			if(objStatus.contains("false")) {
				System.out.println("OrderHistoryProgressBarValidation test case is failed");
				return "Fail";
			} else {
				System.out.println("OrderHistoryProgressBarValidation test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method OrderHistoryProgressBarValidation "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: OrderHistoryShippingAmountValidation()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: TM-4190
	 ****************************************************************************
	 */

	public static String OrderHistoryShippingAmountValidation() {

		String expected=null;
		String actual=null;
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String OrderHisShiptotal=null;
		String salesTax=null;

		String screenshotName = "Scenarios_OrderHistoryTotalAmountValidation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method OrderHistoryTotalAmountValidation started here ...");			
			expected = verifyexpectedresult.placeOrderConfirmation;	
			UIFoundation.waitFor(4L);

			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEditOrderHIS());
			UIFoundation.waitFor(12L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{

				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
				UIFoundation.waitFor(8L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(3L);
			}


			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);

				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
					UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}

				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}

				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.waitFor(14L);
				}

			}

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSelectShippingMethodOption));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(6L);

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(3L);
			}


			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);

				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
					UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}

				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}

				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.waitFor(14L);
				}

			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.txtSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.txtShippingHandaling);
			total=UIFoundation.getText(FinalReviewPage.txtTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.txtOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrderButton);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrderButton));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrderButton, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);

			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: "+orderNum);   
			}else
			{
				objStatus+=false;
				String objDetail="Order number is null and cannot placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}

			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkOrderNumber));
			UIFoundation.waitFor(20L);
			OrderHisShiptotal = UIFoundation.getText(ThankYouPage.obj_ShippingAndHnadling);
			System.out.println("Order History Total:                 " + OrderHisShiptotal);
			if(OrderHisShiptotal.equalsIgnoreCase(shippingAndHandling)) {
				objStatus += true;

				String objDetail = "Order History Shipping Amount Matches the Cart Shipping Amount";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");

			}else {
				objStatus += false;
				String objDetail = "Order History Shipping Amount NOT Matches the Cart Shipping Amount";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method OrderHistoryShippingAmountValidation ended here ...");


			if(objStatus.contains("false")) {
				System.out.println("OrderHistoryShippingAmountValidation test case is failed");
				return "Fail";
			} else {
				System.out.println("OrderHistoryShippingAmountValidation test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method OrderHistoryShippingAmountValidation "
					+ e);
			return "Fail";
		}
	}
}
