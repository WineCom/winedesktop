package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyMilitaryOfficeBillingAddress extends Mobile {
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "militaryOfficeUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: addNewCreditCard()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to add the new credit 
	 * 						  card details for billing process
	 ****************************************************************************
	 */
		public static String addNewCreditCard() {
			String objStatus=null;
			   String screenshotName = "Scenarios_militaryOffice_Screenshot.jpeg";
				String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
						+ screenshotName;
			try {
				log.info("The execution of the method addNewCreditCard started here ...");				
			
				if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
				UIFoundation.waitFor(8L);
          //     objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
					UIFoundation.waitFor(1L);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);
				}
				
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Yr"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(2L);			
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(ele.isSelected())					
				{
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillinState, "militaryStateAA"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingZip, "militaryStateAACode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtBirthMonth)){
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthYear, "birthYear"));
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AA' state is displayed when adding new 'BILLING ADDRESS' of credit cards.", "Pass", "");
				}else{
					objStatus+=false;					
						String objDetail="'AA' state is not displayed when adding new 'BILLING ADDRESS' of credit cards.";						
						
						 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"AAState", objDetail);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
					UIFoundation.waitFor(1L);
				}
				
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);
				
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Yr"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(2L);
				UIFoundation.clckObject(FinalReviewPage.btnSaveCreditCard);
				ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(ele.isSelected())
				{
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}			
				
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillinState, "militaryStateAE"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingZip, "militaryStateAECode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtBirthMonth)){
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthYear, "birthYear"));
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AE' state is displayed when adding new 'BILLING ADDRESS' of credit cards.", "Pass", "");
				}else{
					objStatus+=false;				
						String objDetail="'AE' state is not displayed when adding new 'BILLING ADDRESS' of credit cards.";					
						UIFoundation.captureScreenShot(screenshotpath+screenshotName+"APState", objDetail);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
				{
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);					
				}
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Yr"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(2L);
				UIFoundation.clckObject(FinalReviewPage.btnSaveCreditCard);
				ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(ele.isSelected())
				{
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}		
				
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillinState, "militaryStateAP"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingZip, "militaryStateAPCode"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.txtBirthMonth)){
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthYear, "birthYear"));
				}
			
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AP' state is displayed when adding new 'BILLING ADDRESS' of credit cards.", "Pass", "");
				}else{
					objStatus+=false;					
						String objDetail="'AP' state is not displayed when adding new 'BILLING ADDRESS' of credit cards.";						
						UIFoundation.captureScreenShot(screenshotpath+screenshotName+"APState", objDetail);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment)){
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardEdit));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.btnPaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillingAdressState, "militaryStateAA"));
				UIFoundation.clearField(FinalReviewPage.txtBillingAdressZipCode);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingAdressZipCode, "militaryStateAACode"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaywithThisCardSave);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardSave));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaywithThisCardEdit)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AA' state is displayed when editing 'BILLING ADDRESS' of credit cards.", "Pass", "");
				}else{
					objStatus+=false;					
						String objDetail="'AA' state is not displayed when editing 'BILLING ADDRESS' of credit cards.";						
						UIFoundation.captureScreenShot(screenshotpath+screenshotName+"AEStateEditCard", objDetail);
				}
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment)){
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardEdit));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.btnPaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillingAdressState, "militaryStateAE"));
				UIFoundation.clearField(FinalReviewPage.txtBillingAdressZipCode);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingAdressZipCode, "militaryStateAECode"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaywithThisCardSave);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardSave));
				UIFoundation.waitFor(4L);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaywithThisCardEdit) || UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AE' state is displayed when editing 'BILLING ADDRESS' of credit cards.", "Pass", "");
				}else{
					objStatus+=false;					
						String objDetail="'AE' state is not displayed when editing 'BILLING ADDRESS' of credit cards.";						
						UIFoundation.captureScreenShot(screenshotpath+screenshotName+"APState", objDetail);
				}
				UIFoundation.waitFor(2L);
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment)){
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardEdit));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.btnPaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillingAdressState, "militaryStateAP"));
				UIFoundation.clearField(FinalReviewPage.txtBillingAdressZipCode);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingAdressZipCode, "militaryStateAPCode"));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaywithThisCard);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCard));
				UIFoundation.waitFor(4L);
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaywithThisCardEdit) || UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment)){
					objStatus+=true;
					ReportUtil.addTestStepsDetails("'AP' state is displayed when editing 'BILLING ADDRESS' of credit cards.", "Pass", "");
				}else{
					objStatus+=false;				
						String objDetail="'AP' state is not displayed when editing 'BILLING ADDRESS' of credit cards.";						
						UIFoundation.captureScreenShot(screenshotpath+screenshotName+"APState", objDetail);
				}
				log.info("The execution of the method addNewCreditCard ended here ...");	
				if (objStatus.contains("false")) {
					System.out.println("Verify 'AA,AE,AP'  state's are displayed when editing and adding new 'BILLING ADDRESS' of credit cards test case failed.");
					return "Fail";
					
				} else {
					System.out.println("Verify 'AA,AE,AP'  state's are displayed when editing and adding new 'BILLING ADDRESS' of credit cards test case executed successfully.");
					return "Pass";
				}
			} catch (Exception e) {
				log.error("there is an exception arised during the execution of the method addNewCreditCard "
						+ e);
				return "Fail";
			}
			
		}	
}
