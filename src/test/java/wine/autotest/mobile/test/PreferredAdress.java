package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class PreferredAdress extends Mobile {

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 * TM-636,638
	 ****************************************************************************
	 */
	
	public static String login(WebDriver driver)
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.mouseHover(LoginPage.accountBtn));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.lnkSign));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "preferenceAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: preferredAdressFunctionality()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String preferredAdressFunctionality() {
	

	String objStatus=null;
	   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
	   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method checkoutProcess started here ...");			

           if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(10L);					                                  
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.spnRecipientChangeAddress));

			if(UIFoundation.isDisplayed(FinalReviewPage.txtPreferredAddress))
			{
				  objStatus+=true;
			      String objDetail="Verified the address that is preferred has text";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the address that is preferred has text");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the address that is preferred has text test case is failed";
				   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				   System.err.println("Verify the address that is preferred has text");
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtMakePreferred));
			WebElement ele=driver.findElement(By.xpath("(//span[@class='shippingSelectedAddress_group'])[2]/input"));
			String shippingAddressExpectedName=UIFoundation.getText(FinalReviewPage.btnshippingAddressExpectedName);	
			if(!ele.isSelected()){
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnRecipientContinue));
				UIFoundation.waitFor(5L);

			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboShippingSelectedAddress));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			}	
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			UIFoundation.waitFor(1L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
			UIFoundation.waitFor(1L);
			}
			 
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
			UIFoundation.waitFor(2L);
			WebElement ele1=driver.findElement(By.xpath("//input[@name='billingAddrSameAsShip']"));
			if(!ele1.isSelected())
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);

			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtPaywithThisCardCvv))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaywithThisCardCvv, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtPaywithThisCardCvv));
				UIFoundation.waitFor(4L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(10L);
			
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtPaymentCVV))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPaymentCVV, "CardCvid"));
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentSaveButton));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 70);
			}

			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCheckoutButton);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnObjCheckout);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}			
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
			String shippingAddressActualName=UIFoundation.getText(FinalReviewPage.btnShippingAddressActualName);
			if(shippingAddressExpectedName.equalsIgnoreCase(shippingAddressActualName))
			{
				  objStatus+=true;
			      String objDetail="Verified the functionality of 'Make preferred' text button available in the Address cards";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the functionality of 'Make preferred' text button available in the Address cards");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the functionality of 'Make preferred' text button available in the Address cards test case is failed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				   System.err.println("Verify the functionality of 'Make preferred' text button available in the Address cards");
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				//System.out.println("Verify the address that is preferred has text test case is failed");
				return "Fail";
			} else {
				//System.out.println("Verify the address that is preferred has text test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method preferredAdressFunctionality "
					+ e);
			return "Fail";
		}
	}
	

	
}
