package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : shippingDetails()
	 * Created By  : Chandrashekhar. 
	 * Reviewed By : Ramesh.
	 * Purpose : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	public static String verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping(WebDriver driver) {
		String objStatus = null;
		String stewardshipPrice=null;
		String stewardshipDiscount=null;
		   String screenshotName = "Scenarios__shippingMethod.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
						
			UIFoundation.waitFor(2L);			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			UIFoundation.waitFor(2L);			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
		
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			 
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(1L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			
			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnStewardShipAddToCart));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnStewardshipConfirm));
			UIFoundation.waitFor(8L);
			UIFoundation.waitFor(2L);
			stewardshipPrice=UIFoundation.getText(FinalReviewPage.txtStewardshipDiscountPrice);
		//	stewardshipDiscount=stewardshipPrice.replaceAll("\\(|\\)", "");
			stewardshipDiscount=stewardshipPrice.replaceAll("-", "");
			String stewardshipSave=UIFoundation.getText(FinalReviewPage.spnstewardShipSavePrice);
			String shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			if(stewardshipDiscount.equalsIgnoreCase(shippingAndHandling))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the Standard shipping charges are saved  when Stewardship  is enrolled in Final Review Page.", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Verify the Standard shipping charges are saved  when Stewardship  is enrolled in Final Review Page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"warningMsgShip", objDetail);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : verifyShippingAndHandalingChargesAreAppliedToTheNonStewardshipUser() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose : The purpose of this method is to verify the shipping&Handlaingcharges for Non stewardship user.
	 * Jira Id :TM-2200
	 ****************************************************************************
	 */
	
	public static String verifyShippingAndHandalingChargesAreAppliedToTheNonStewardshipUser() {
		String objStatus = null;
		String stewardshipPrice=null;
		String stewardshipDiscount=null;
		   String screenshotName = "Scenarios__shippingMethod.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(1L);
			UIBusinessFlow.recipientEdit();
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSelectStandardShippingMethodOption));
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCvvR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardSave));
			}
			
			if (UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
				}
			}
			
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.spnShippingHnadling))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Shipping and Handling is applied to the Order Totals is succesfully ", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Shipping and Handling is not applied to the Order Totals.";
			 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"warningMsgShip", objDetail);
			}
			
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : verifyUpsellShouldMatchShippingAndHandalingCharges() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh  
	 * Purpose     : The purpose of this method is to verify the upsell shoulmatch shipping&Handlaingcharges.
	 * Jira Id :TM 2201
	 ****************************************************************************
	 */
	
	public static String verifyUpsellShouldMatchShippingAndHandalingCharges(WebDriver driver) {
		String objStatus = null;
		String stewardshipPrice=null;
		String stewardshipDiscount=null;
		   String screenshotName = "Scenarios__shippingMethod.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}         
			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));		
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnSelectShippingMethod));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkSelectStandardShippingMethodOption));
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			expectedShippingMethodCharges= Integer.parseInt(Shipping.replaceAll("[^0-9]",""));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			
			
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
