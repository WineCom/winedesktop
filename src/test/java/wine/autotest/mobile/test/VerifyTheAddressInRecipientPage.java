package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheAddressInRecipientPage extends Mobile {

	
	/***************************************************************************
	 * Method Name			: recipientLogin()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String recipientLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(1L);			
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "recipientEditUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyTheAddressInRecipientPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyTheAddressInRecipientPage() {
		String objStatus=null;
		   String screenshotName = "Scenarios__showOutOfStock.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
					
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(2L);

           if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
                          objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
                          UIFoundation.waitFor(2L);
			/*if(UIFoundation.isDisplayed(FinalReviewPage.spnrecipientChangeAddress)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnrecipientChangeAddress));
				UIFoundation.waitFor(1L);
			}*/
			
			String shippingAddresCountBfr=UIFoundation.getText(FinalReviewPage.lnkShippingAddressCountInRcp);
			if(shippingAddresCountBfr.contains("Fail")){
					objStatus+=false;
				   String objDetail="Unable to read shipping address count is failed";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName+"count", objDetail);
			}
			
			int shippingAddressCountBfr=Integer.parseInt(shippingAddresCountBfr.replaceAll("[^0-9]", ""));
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShippingAddressEdit));
			UIFoundation.waitFor(1L);
			UIFoundation.clearField(FinalReviewPage.txtShippingAddressFullNameClear);
			UIFoundation.clearField(FinalReviewPage.txtShippingAddressStreetAdrsClear);
			UIFoundation.clearField(FinalReviewPage.txtShippingAddressCityClear);
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(FinalReviewPage.lnkShippingOptionalClk);
			String expectedEditedName=UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtShippingAddressFullNameEnter, "firstName");
			expectedEditedName=expectedEditedName.replaceAll(" ", "");
			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtShippingAddressStreetAdrsEnter, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObjectEditShippingAddress(FinalReviewPage.txtShippingAddressCityEnter, "firstName"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtShippingAddressSaveBtn));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShipCont, "Invisible", "", 50);
			UIFoundation.escapeBtn(driver);
		//	UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueButton)){
				 objStatus+=true;
			      String objDetail="Modal/Light Box does not closes on clicking 'ESC' key for the Modal/Light box without  ' X' icon";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueButton));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueButton, "Invisible", "", 50);
			}else{
				 objStatus+=false;
				   String objDetail="Modal/Light Box  closed on clicking 'ESC' key for the Modal/Light box without  ' X' icon";			       
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RecPaddrs", objDetail);
			}
		String actualEditedName=UIFoundation.getText(FinalReviewPage.txtShippingAddressFullNameAfterEdit);
			actualEditedName=actualEditedName.replaceAll(" ", "");
		String shippingAddresCountAftr=UIFoundation.getText(FinalReviewPage.lnkShippingAddressCountInRcp);
		int shippingAddressCountAftr=Integer.parseInt(shippingAddresCountAftr.replaceAll("[^0-9]", ""));
		if(actualEditedName.equalsIgnoreCase(expectedEditedName) && (shippingAddressCountBfr==shippingAddressCountAftr))
			{
				  objStatus+=true;
			      String objDetail="Verified Address does not get duplicated on editing the original address in Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Address should not get duplicated on editing the original address in Recipient page is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"addressBook", objDetail);
			}

			String shippingAddresCountBfrRemove=UIFoundation.getText(FinalReviewPage.lnkShippingAddressCountInRcp);
			int shippingAddressCountBfrRemove=Integer.parseInt(shippingAddresCountBfrRemove.replaceAll("[^0-9]", ""));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShippingAddressEdtRemove));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtRemoveThisAddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtYesremoveThisAddress));
			UIFoundation.waitFor(3L);
			String shippingAddresCountAftrRemove=UIFoundation.getText(FinalReviewPage.lnkShippingAddressCountInRcp);
			int shippingAddressCountAftrRemove=Integer.parseInt(shippingAddresCountAftrRemove.replaceAll("[^0-9]", ""));
			if(shippingAddressCountAftrRemove==(shippingAddressCountBfrRemove-1)){
				  objStatus+=true;
			      String objDetail="Verified the address gets deleted permanently from the Recipient page on clicking 'Remove this address'";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the address gets deleted permanently from the Recipient page on clicking 'Remove this address' is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"addressBook", objDetail);
			}
			UIFoundation.waitFor(1L);
			String shippingAddresCountBeforeAddingNewAddress=UIFoundation.getText(FinalReviewPage.lnkShippingAddressCountInRcp);
			int shippingAddresCountBeforeAddingNewAddres=Integer.parseInt(shippingAddresCountBeforeAddingNewAddress.replaceAll("[^0-9]", ""));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
			objStatus += String.valueOf(UIFoundation.clckObject(FinalReviewPage.SuggestedAddress));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);	
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			UIFoundation.waitFor(1L);
			String shippingAddresCountafterAddingNewAddress=UIFoundation.getText(FinalReviewPage.lnkShippingAddressCountInRcp);
			int shippingAddresCountafterAddingNewAddres=Integer.parseInt(shippingAddresCountafterAddingNewAddress.replaceAll("[^0-9]", ""));
			if(shippingAddresCountafterAddingNewAddres==(shippingAddresCountBeforeAddingNewAddres+1)){
				  objStatus+=true;
			      String objDetail="Verified the address gets saved only once on adding the address in Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				 	objStatus+=false;
				   String objDetail="Verify the address gets saved only once on adding the address in Recipient page is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"addressBook", objDetail);
			}
			
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
}
