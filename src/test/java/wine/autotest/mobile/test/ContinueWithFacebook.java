package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ContinueWithFacebook extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: ContinueWithFacebook()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String continueWithFacebook() {
		String objStatus=null;
		   String screenshotName = "Scenarios_facebook_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of method continueWithFacebook started here");
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaSriptClick(LoginPage.MainNavSignIn));
		
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			if(UIFoundation.isDisplayed(LoginPage.btnContinueWithFacebook))
			{
				  objStatus+=true;
			      String objDetail="'Continue with Facebook' button is dispalyed in 'Sign-In' pages";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				 String objDetail="'Continue with Facebook' button is not dispalyed in 'Sign-In' pages";			
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.createAccountLnk));
			if(UIFoundation.isDisplayed(LoginPage.btnContinueWithFacebook))
			{
				  objStatus+=true;
			      String objDetail="'Continue with Facebook' button is dispalyed in 'Register' pages";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				 String objDetail="'Continue with Facebook' button is not dispalyed in 'Register' pages";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method continueWithFacebook ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("'Continue with Facebook' button must be in 'Sign-In' and 'Register' pages  test case is failed");
				return "Fail";
			} else {
				System.out.println("'Continue with Facebook' button must be in 'Sign-In' and 'Register' pages test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("Existing customer should be a link in new customer register page  test case is failed");
			log.error("there is an exception arised during the execution of the method continueWithFacebook "
					+ e);
			return "Fail";
		}
	}


}
