package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyDeliveryViaEmailDisplayed extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addGiftsToTheCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String addGiftsToTheCart()
	{
		String objStatus=null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try
		{
			log.info("The execution of method addGiftsToTheCart started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.linobj_Gifts));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkAllGiftCard);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkAllGiftCard));
			UIFoundation.waitFor(3L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProdct);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProdct));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProdct);
			if (addToCart2.contains("Add to Cart")) {
			//	UIFoundation.scrollDownOrUpToParticularElement(ListPage. "SecondProdct");
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProdct));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProdct);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProdct));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProdct);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFourthProdct);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProdct));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProdct);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFifthProdct);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProdct));
			}
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addGiftsToTheCart "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to fill the shipping address of the customer
	 * 
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String screenshotName = "deliveryViaEmail.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			UIFoundation.waitFor(2L);
			log.info("The execution of the method shippingDetails started here ...");
			System.out.println("============Order summary in CART page===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
				
			UIFoundation.waitFor(12L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			if(UIFoundation.isDisplayed(FinalReviewPage.txtDeliveryViaEmail))
			{				
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
				objStatus+=true;
				String objDetail="'Delivered via Email' is displayed in delivery section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="'Delivered via Email' is not displayed in delivery section";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);				
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name : addNewCreditCard()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String stawardShipDiscount=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			 expected = verifyexpectedresult.placeOrderConfirmation;			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
				
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(15L);
						
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(10L);			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
			{			
			UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);			
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtOrderNum));
			UIFoundation.waitFor(1L);			
						
			if(UIFoundation.isDisplayed(FinalReviewPage.txtDeliveryTimeLine))
			{	
				objStatus+=true;
				String objDetail = "Delivered via email' is displayed in order history";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		    			
			}else
			{
				objStatus+=false;
		    	String objDetail="Delivered via email' is not displayed in order history";
		    	   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);			
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Order creation with Gifts for new user  test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with Gifts for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}


}
