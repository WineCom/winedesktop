package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class SearchProductByCharacter extends Mobile {
	
	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		String screenshotName = "Scenarios_Cay_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.txtSearchBarFormInput));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtsearchProduct, "cay"));
			UIFoundation.waitFor(4L);
			if(!UIFoundation.getText(ListPage.btnFirstProductInList).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInList);
				if(products1.toLowerCase().contains("cay"))
				{
					  objStatus+=true;
				      String objDetail="First product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("First product contains" + "'Cay'"+ "character , Product name is:"+products1);
				}
				else
				{
					objStatus+=false;
					String objDetail="First product not contains" + "'Cay'"+ "character";
				//    UIFoundation.captureScreenShot(screenshotpath, objDetail);
					  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					System.out.println("First product not contains" + "'Cay'"+ "character , Product name is:"+products1);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.btnSecondProductInList).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInList);
				if(products2.toLowerCase().contains("cay"))
				{
					  objStatus+=true;
				      String objDetail="Second product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Second product contains" + "'Cay'"+ "character , Product name is:"+products2);
				}
				else
				{
					objStatus+=false;
					String objDetail="Second product not contains" + "'Cay'"+ "character";
				 //   UIFoundation.captureScreenShot(screenshotpath, objDetail);
					  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					System.out.println("Second product not contains" + "'Cay'"+ "character , Product name is:"+products2);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.btnThirdProductInList).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInList);
				if(products3.toLowerCase().contains("cay"))
				{
					 objStatus+=true;
				      String objDetail="Third product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Third product contains" + "'Cay'"+ "character , Product name is:"+products3);
				}
				else
				{
					objStatus+=false;
					String objDetail="Third product not contains" + "'Cay'"+ "character";
				//    UIFoundation.captureScreenShot(screenshotpath, objDetail);
					  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					System.out.println("Third product not contains" + "'Cay'"+ "character , Product name is:"+products3);
				}
				
				
				
			}
			if(!UIFoundation.getText(ListPage.btnFourthProductInList).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInList);
				if(products4.toLowerCase().contains("cay"))
				{
					objStatus+=true;
				      String objDetail="Fourth product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Fourth product contains" + "'Cay'"+ "character , Product name is:"+products4);
				}
				else
				{
					objStatus+=false;
					String objDetail="Fourth product not contains" + "'Cay'"+ "character";
				 //   UIFoundation.captureScreenShot(screenshotpath, objDetail);
					  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					System.out.println("Fourth product contains" + "'Cay'"+ "character , Product name is:"+products4);
				}
				
				
			}
			if(!UIFoundation.getText(ListPage.btnFifthProductInList).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInList);
				if(products5.toLowerCase().contains("cay"))
				{
					objStatus+=true;
				      String objDetail="Fifth product contains" + "'Cay'"+ "character";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Fifth product contains" + "'Cay'"+ "character , Product name is:"+products5);
				}
				else
				{
					objStatus+=false;
					String objDetail="Fifth product not contains" + "'Cay'"+ "character";
			//	    UIFoundation.captureScreenShot(screenshotpath, objDetail);
					  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					System.out.println("Fifth product contains" + "'Cay'"+ "character , Product name is:"+products5);
				}
			
				
			}
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
	
}
