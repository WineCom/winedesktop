package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyAddToCartButtonFunctionlityForDryState extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : orderSummaryForNewUser() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String verifyAddToCartButtonFunctionality() {
		String products1 = null;
		String products2 = null;
		String products3 = null;
		String products4 = null;
		String products5 = null;
		String objStatus = null;

		try {
			
/*			UIFoundation.SelectObject(driver, "SelectState", "dryState");
			UIFoundation.waitFor(10L);*/
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);
			System.out.println("=======================verify the  �Add To Cart� button is not displayed when dry state is selected  =====================");
			if (UIFoundation.getText(ListPage.btnAddToCartFirstButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for first product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartSecondButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for second product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartThirdButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for third product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartFourthButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for fourth product");
				

			}

			if (UIFoundation.getText(ListPage.btnAddToCartFifthButton).contains("Fail")) {
				System.out.println("'Add to Cart' button is not displayed in list page for fifth product");
				

			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";
		}
	}



}

