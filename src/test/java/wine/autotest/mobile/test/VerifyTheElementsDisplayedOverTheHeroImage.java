package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheElementsDisplayedOverTheHeroImage extends Mobile {

	
	/***************************************************************************
	 * Method Name			: VerifyTheElementsDisplayedOverTheHeroImage()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyTheElementsDisplayedOverTheHeroImage()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_heroImage.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			log.info("The execution of the method verifyTheElementsDisplayedOverTheHeroImage started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ListPage.txtHeroImageTitle))
			{
				objStatus+=true;
				String objDetail="Varietal name element is displayed over the hero image when learn about content icon is collapsed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Varietal name elements is not displayed over the hero image when learn about content icon is collapsed";
				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			if(UIFoundation.isDisplayed(ListPage.imgHeroImagePlusSymbol))
			{
				objStatus+=true;
				String objDetail="Plus symbol is displayed over the hero image when learn about content icon is collapsed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Plus symbol is not displayed over the hero image when learn about content icon is collapsed";			
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgHeroImagePlusSymbol));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.txtHeroImageTitle))
			{
				objStatus+=true;
				String objDetail="Varietal name element is displayed over the hero image when learn about content icon is expanded";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="Varietal name elements is not displayed over the hero image when learn about content icon is expanded";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(ListPage.lnklistPageContent) && UIFoundation.isDisplayed(ListPage.lnklistPageLink)  )
			{
				objStatus+=true;
				String objDetail="contents and links are displayed with in the hero image when learn about content icon is expanded";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else{
				objStatus+=false;
				String objDetail="contents and links are not displayed with in the hero image when learn about content icon is expanded";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyTheElementsDisplayedOverTheHeroImage ended here ...");
			if (objStatus.contains("false"))
			{
			
				return "Fail";
			}
			else
			{
			
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifyTheElementsDisplayedOverTheHeroImage "+ e);
			return "Fail";
			
		}
	}	
	
}
