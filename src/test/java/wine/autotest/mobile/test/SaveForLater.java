package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class SaveForLater extends Mobile {



	static boolean isObjectPresent=false;

	/***************************************************************************
	 * Method Name			: saveForLaterWithoutSignIn()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String saveForLaterWithoutSignIn(WebDriver driver) {


		String objStatus=null;
		try {
			log.info("The execution of the method saveForLater started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cboSelectState, "State"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSrt);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			objStatus+=String.valueOf(UIBusinessFlow.validationForSaveForLater());
			log.info("The execution of the method saveForLater ended here ...");
			if(objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{	
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name			: saveForLaterWithSignIn()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String saveForLaterWithSignIn(WebDriver driver) {

		String objStatus=null;
		try {
			log.info("The execution of the method saveForLaterWithSignIn started here ...");

			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cboSelectState, "State"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexBlends));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			UIFoundation.waitFor(1L);

			UIFoundation.clckObject(ListPage.lnkSrt);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			objStatus+=String.valueOf(UIBusinessFlow.validationForSaveForLater());
			System.out.println(objStatus);
			log.info("The execution of the method saveForLaterWithSignIn ended here ...");
			if(objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{

				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: verifySaveForLaterOnClickingShipToKY()
	 * Created By			: Chandra Shekhar
	 * Reviewed By			: Ramesh
	 * Purpose				:  
	 *  
	 ****************************************************************************
	 */
	public static String verifySaveForLaterOnClickingShipToKY() {

		String objStatus=null;
		try {
			log.info("The execution of the method verifySaveForLaterOnClickingShipToKY started here ...");

			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cboObj_ChangeState, "dryState"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnContinueShipToKY));			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnContinueShipToKY));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(ListPage.btnYourCartIsEmpty));

			log.info("The execution of the method verifySaveForLaterOnClickingShipToKY ended here ...");
			if(objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{

				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: verifySaveForLaterOnClickingShipToKY()
	 * Created By			: Chandra Shekhar
	 * Reviewed By			: Ramesh
	 * Purpose				:  
	 *  
	 ****************************************************************************
	 */
	public static String VerifyProductMovedToCartFromSaveForLater() {

		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		String products1=null;
		String products2=null;

		String objStatus=null;
		try {
			log.info("The execution of the method verifySaveForLaterOnClickingShipToKY started here ...");

			driver.navigate().refresh();
			UIFoundation.waitFor(3L);			


			System.out.println("=========================Products available in save for later section Before  logout================================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
				actualItemsName.add(products1);				
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnFirstProductSaveFor));

			}

			if(!UIFoundation.getText(ListPage.btnFirstProductInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnFirstProductInSaveFor);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}


			if(products1.contains(products2))
			{
				String objDetail="Verify the functionality of 'Save for later' link in cart section is failed";
			}
			else
			{
				String objDetail="Verify the functionality of 'Save for later' link in cart section is failed";
			}

			System.out.println(objStatus);
			log.info("The execution of the method verifySaveForLaterOnClickingShipToKY ended here ...");
			if(objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{

				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}

}
