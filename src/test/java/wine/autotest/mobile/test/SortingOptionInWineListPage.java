
package wine.autotest.mobile.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.ListPage;

import wine.autotest.mobile.test.Mobile;;

public class SortingOptionInWineListPage extends Mobile {
	


	/***************************************************************************
	 * Method Name : sortingOptionsAtoZ() 
	 * Created By :  Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingOptionsAtoZ() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsAtoZ started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);			
		
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortAtoZ));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIBusinessFlow.validationForSortingAtoZ());
			
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsAtoZ ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsAtoZ "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsZtoA() 
	 * Created By : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose : The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsZtoA() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsZtoA started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortAtoZ));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIBusinessFlow.validationForSortingZtoA());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsZtoA ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsZtoA "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsLtoH() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsLtoH() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsLtoH started here ...");
			driver.navigate().refresh();
		    objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortLtoH));
			UIFoundation.waitFor(4L);

			objStatus += String.valueOf(UIBusinessFlow.validationForSortingPriceLtoH());
			// objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"SortOptions","SortA-Z"));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsLtoH ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsLtoH "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsHtoL() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsHtoL() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsHtoL started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortHtoL));
			UIFoundation.waitFor(4L);
			UIBusinessFlow.validationForSortingPriceHtoL();
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsHtoL ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsHtoL "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : sortingOptionsZtoA() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

	/*public static String sortingOptions(WebDriver driver) {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsZtoA started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortNtoO"));
			
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "linSortOptions"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortOtoN"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortOptions"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortSavings"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortOptions"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortTopRated"));
			UIFoundation.waitFor(3L);
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortOptions"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "SortTopRated"));
			log.info("The execution of the method sortingOptionsZtoA ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsZtoA "
					+ e);
			return "Fail";
		}
	}
*/	/***************************************************************************
	 * Method Name : sortingMostPopular() 
	 * Created By : Chandra Shekhar
	 * Reviewed By : Ramesh, 
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingMostPopular() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingMostPopular started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.linSortOptions))
			{
				 objStatus+=true;
			      String objDetail="Verified the sortingMostPopular is displayed in adding the products";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the sortingMostPopular is displayed in adding the products");
			}
		
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingMostPopular ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingMostPopular "
					+ e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name : sortingMostInteresting() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh. 
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingMostInteresting() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingMostInteresting started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortMostInteresting));
			if(UIFoundation.isDisplayed(ListPage.linSortMostInteresting))
			{
				 objStatus+=true;
			      String objDetail="Verified the sortingMostInteresting is displayed in adding the products";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the sortingMostInteresting is displayed in adding the products");
			}				
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingMostInteresting ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingMostPopular "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingMostInteresting() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh. 
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingCustomerRating() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingMostInteresting started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortCustomerRating));		
			UIFoundation.waitFor(3L);
			UIBusinessFlow.validationForSortingCustomerRating();
			log.info("The execution of the method sortingMostInteresting ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingMostPopular "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : sortingMostInteresting() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh. 
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingProfessionalRating() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingMostInteresting started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortprofissionalRating));		
			UIFoundation.waitFor(3L);
			UIBusinessFlow.validationForProfissionlRating();
			log.info("The execution of the method sortingMostInteresting ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingMostPopular "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : sortingOldToNew() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingOldToNew() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOldToNew started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkSortOldToNew));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortOldToNew));		
			UIFoundation.waitFor(3L);
		
			log.info("The execution of the method sortingOldToNews ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOldToNew "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : sortingOldToNew() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh,
	 * Purpose :
	 ****************************************************************************
	 */

	public static String sortingNewToOld() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingNewToOld started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkSortNewToOld));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSortNewToOld));		
			UIFoundation.waitFor(3L);
		
			log.info("The execution of the method sortingNewToOld ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingNewToOld "
					+ e);
			return "Fail";
		}
	}	
	
	/***************************************************************************
	 * Method Name : sortingOldToNew() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 ****************************************************************************
	 */

	public static String justIn() {
		String objStatus = null;

		try {
			log.info("The execution of the method justIn started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkjustIn));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkjustIn));		
			UIFoundation.waitFor(3L);
		
			log.info("The execution of the method justIn ended here ...");
			if (objStatus.contains("false")) {
				  String objDetail="Just Sort options are not availble in the wine list page ";
				   ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
				 return "Fail";
			} else {
				 String objDetail="JustIn Sort options are availble in the wine list page";
				   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method justIn "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : savings() 
	 * Created By  : Chandra Shekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 ****************************************************************************
	 */

	public static String savings() {
		String objStatus = null;

		try {
			log.info("The execution of the method savings started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.linSortOptions));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.lnkSavings));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkSavings));		
			UIFoundation.waitFor(3L);
		
			log.info("The execution of the method savings ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method savings "
					+ e);
			return "Fail";
		}
	}	
}
