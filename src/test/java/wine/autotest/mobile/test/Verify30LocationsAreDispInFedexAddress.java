package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class Verify30LocationsAreDispInFedexAddress extends Mobile {
	

	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhkar. 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "fedexloactions"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: VerifyAddToMyWineFunctionality(NFP-4253)
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String verify30LocationsAreDispInFedexAddress() {
		String objStatus=null;
		String ScreenshotName= "Verify30LocationsAreDispInFedexAddress.jpeg";
		String Screenshotpath =System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		try
		{
			log.info("Verify 30Locations Are Disp In FedexAddress Method started here........");
			 objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			 UIFoundation.waitFor(3L);
			 if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
			 UIFoundation.waitFor(5L);		
			 objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			 objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkShipToFedex));
			 objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "RecShipZipcode"));
			 objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			 UIFoundation.waitFor(4L);
			 List<WebElement> fedLoc = driver.findElements(By.xpath("//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded ']"));
		     
			 if(fedLoc.size()==30) {
		     objStatus+=true;
		     String objDetail="30 locations are displayed in 'Find Local Pickup Locations' under 'Recipient' section";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		}
		else {
			objStatus+=false;
			String objDetail="30 locations are not displayed in 'Find Local Pickup Locations' under 'Recipient' section";
		      ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
		      UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				 }
		     log.info("Verify 30Locations Are Disp In FedexAddress Method started here..................");
		     if(objStatus.contains("false")) {
		    	 System.out.println("Verify30LocationsAreDispInFedexAddress test case is failed");
					return "Fail";
					}
					else {
					System.out.println("Verify30LocationsAreDispInFedexAddress test case executed succesfully");
					
					return "Pass";
					
				}
					
				}catch(Exception e) {
				return "Fail";
	
			 }
		}
	
	/***************************************************************************
	 * Method Name			: verifyLocalPickupFinderInFooterSection(NFP-4253)
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				:
	 * JiraId               :  TM-4399
	 ****************************************************************************
	 */
	
	public static String verifyLocalPickupFinderInFooterSection() {
		String objStatus=null;
		String ScreenshotName= "Verify30LocationsAreDispInFedexAddress.jpeg";
		
		String Screenshotpath =System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		try
		{
			log.info("Verify 30Locations Are Disp In FedexAddress Method started here........");				
	
			 objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			 UIFoundation.waitFor(4L);
			 List<WebElement> fedLoc = driver.findElements(By.xpath("//button[@class='localPickup_shipTo btn-shipTo btn btn-small btn-rounded ']"));
		     
			 if(fedLoc.size()==30) {
		     objStatus+=true;
		     String objDetail="30 locations are displayed in 'Find Local Pickup Locations' under 'Recipient' section";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		}
		else {
			objStatus+=false;
			String objDetail="30 locations are not displayed in 'Find Local Pickup Locations' under 'Recipient' section";
		      ReportUtil.addTestStepsDetails(objDetail, "Fail", "");		     
		      UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
				 }
		     log.info("Verify 30Locations Are Disp In FedexAddress Method started here..................");
		     if(objStatus.contains("false")) {
		    	 System.out.println("Verify30LocationsAreDispInFedexAddress test case is failed");
					return "Fail";
					}
					else {
					System.out.println("Verify30LocationsAreDispInFedexAddress test case executed succesfully");
					
					return "Pass";					
				}
					
				}catch(Exception e) {
				return "Fail";
	
			 }
		}	
}
