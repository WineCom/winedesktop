package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class ValidateEnrollmentFlowforRedWine extends Mobile {

	

	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWine() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar K B
	 * Purpose     : The purpose of this method is to navigate compass tiles and 'Proceed to Enrollment' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String enrollForPickedUpWine() {
		String objStatus = null;
		String screenshotName = "Scenarios_enrollForPickedUpWine_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method enrollForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage. btnVarietalTypNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPersonalcommntNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSelQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				System.out.println("enrollForPickedUpWine test case failed");
				String objDetail="enrollForPickedUpWine test case failed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				System.out.println("enrollForPickedUpWine test case executed successfully");
				String objDetail="enrollForPickedUpWine test case executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	
	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar K B
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyLocalPickUpForPickedUpWine() {
			
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			//objStatus+=String.valueOf(UIFoundation.clickObject(OMSPage. "VerifyFedexAddress"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocarion));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtFedexSaveButton));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
				//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkAcceptTermsAndConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
				UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}	
}
