package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class ApplyAndRemovePromoCodeForExistingUser extends Mobile {
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String login(WebDriver driver)
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(6L); 
			
		/*	if(UIFoundation.isDisplayed(driver, "JoinNowButton")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "JoinNowButton"));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(driver, "signInLinkAccount")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver,"signInLinkAccount"));
			}
			*/
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "wineemail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			String actualtile=driver.getTitle();
		//	String expectedTile=objExpectedRes.getProperty("shoppingCartPageTitle");
			String expectedTile=verifyexpectedresult.shoppingCartPageTitle;
			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			objStatus+=false;
	    	String objDetail="Failed to login";
	    	 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		WebElement oEle=null;
		boolean isdisplayed=false;
		
		String screenshotName = "Scenarios_PromoCode_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;		
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.txtObjPromoCode)){
				
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnPromoCodeApplyLink));
				UIFoundation.waitFor(5L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.txtObjPromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnPromoCodeApplyLink));
				UIFoundation.waitFor(5L);
			}

			System.out.println("============Order summary after applying promo code==============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			promeCode=UIFoundation.getText(CartPage.spnPromeCodePrice);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Prome Code:            "+promeCode);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
		//	UIBusinessFlow.discountCalculator(CartPage.spnSubtotal, promeCode);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemovePromoCode));
			UIFoundation.waitFor(2L);
			isdisplayed=UIFoundation.isDisplayed(CartPage.spnPromeCodePrice);
			if(isdisplayed)
			{
				System.out.println("The Applied promo code amount is not removed from total amount");
				objStatus+=false;
			    String objDetail="The Applied promo code amount is not removed from total amount";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			else
			{
				System.out.println("Applied Prome code is Removed successfully");
			    objStatus+=true;
			    String objDetail="Applied Prome code is Removed successfully";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			System.out.println("============Order summary after removing promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";			
		}
	}
}
