package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class EmailPreference extends Mobile {



	/***************************************************************************
	 * Method Name			: saveButtonFunctionality()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 * TM-3532
	 ****************************************************************************
	 */

	public static String saveButtonFunctionality( )
	{
		String objStatus=null;
		String screenshotName = "Scenarios_saveButtonFunctionality_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method login started here ...");

			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			//   objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavSignIn"));
			UIFoundation.waitFor(1L); 

			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}

			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}

			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.EmailPreferences));
			UIFoundation.webDriverWaitForElement(LoginPage.btnverifySave, "Clickable", "", 50);
			if(UIFoundation.isDisplayed(LoginPage.btnverifySave))
			{
				objStatus+=true;
				String objDetail=" 'Save' button is displayed in Email Preferences section under user profile";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("'Save' button is displayed in Email Preferences section under user profile");   

			}else{
				objStatus+=false;
				String objDetail=" 'Save' button is not displayed in Email Preferences section under user profile";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the 'Save' button is displayed in Email Preference section under user profile test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the 'Save' button is displayed in Email Preferences section under user profile test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method saveButtonFunctionality "+ e);
			return "Fail";

		}
	}	

}
