
package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyAddGiftWrappingToCartFunctionality extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : verifyGiftWrapping() 
	 * Created By  : chandrashekhar
	 * Reviewed By : Ramesh 
	 * Purpose     : The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

	public static String verifyGiftWrapping() {
		String objStatus = null;
		String finalsubTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		int productCountInCart=0;
		int productCountInShoppingCart=0;
		String screenshotName = "Scenarios_VerifyGiftWrapping_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method verifyGiftWrapping started here ...");
			UIFoundation.waitFor(5L);
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			String cartSubTotal=UIFoundation.getText(CartPage.spnSubtotal);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			UIFoundation.waitFor(2L);
			cartSubTotal = cartSubTotal.replaceAll("[$,]","");
			double cartSubTot=Double.parseDouble(cartSubTotal);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnCartCount);
			productCountInCart=Integer.parseInt(UIFoundation.getText(CartPage.btnCartCount));
			System.out.println("Cart count:"+productCountInCart);
			
			UIFoundation.waitFor(6L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
              /* UIFoundation.waitFor(8L);
               objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
               UIFoundation.waitFor(10L);*/
               
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkShippingAddressEdit))
			{
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkShippingAddressEdit));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(FinalReviewPage.txtStreetAddress);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			UIFoundation.clearField(FinalReviewPage.txtCity);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnShipState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnShipState);
			UIFoundation.clearField(FinalReviewPage.txtZip);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZip, "ZipCode"));
			UIFoundation.clearField(FinalReviewPage.txtPhoneNumbere);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNumbere, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnShippingAddressSave));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueButton));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkReciptEidt)){
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkReciptEidt));
			UIFoundation.waitFor(1L);
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientShipToaddress);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnRecipientShipToaddress));
			UIFoundation.waitFor(1L);
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "RecipientShipToaddress"));
			WebElement gift = driver.findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift.isSelected()){
				UIFoundation.clickObject(FinalReviewPage.radRecipientGiftCheckbox);
			}
			UIFoundation.waitFor(3L);
			
			/*WebElement gift = driver.findElement(By.xpath("(//main/section[@class='checkoutMainContent']//span[@class='formWrap_checkboxSpan'])[2]"));
			UIFoundation.scrollDownOrUpToParticularElement(driver, "RecipientGiftCheckbox");
			if(!gift.isSelected()){
				UIFoundation.javaScriptClick(driver, "RecipientGiftCheckbox");
			}*/
		
			//UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.cboGiftWrapOption));
			UIFoundation.waitFor(1L);
			String giftBag=UIFoundation.getText(FinalReviewPage.cboGiftBag);
			giftBag=giftBag.replace(" ", ".");
			double giftBagValue=Double.parseDouble(giftBag.substring(0,4));
			System.out.println("GiftBag value for each products:"+giftBagValue);
			String giftWrapCnt=UIFoundation.getText(FinalReviewPage.txtGiftWrapOptionCount);
			int giftWrapCount= Integer.parseInt(giftWrapCnt.replaceAll("[^0-9]", ""));
			System.out.println("Gift Wrap added to: "+giftWrapCount + " item");
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnReciptContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnReciptContinue, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
				// ShipContinueButton
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangePayment));
				UIFoundation.waitFor(1L);
			}
				
				 
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
	        {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
	        }
	          if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
	          {
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	             UIFoundation.waitFor(2L);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	             UIFoundation.waitFor(3L);
			}
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth)){
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");

			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+finalsubTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			
			
			/*finalsubTotal=UIFoundation.getText(driver, "Subtotal");
			finalsubTotal = finalsubTotal.replaceAll("[$,]","");
			double finalSubTot=Double.parseDouble(finalsubTotal);
			finalSubTot=(finalSubTot*10/100);
			finalSubTot=Math.round(finalSubTot*100);
			finalSubTot=finalSubTot/100;
			double finalSubtotalPrice=(giftBagValue*giftWrapCount)+cartSubTot;	
			finalSubtotalPrice=(finalSubtotalPrice*10/100);
			finalSubtotalPrice=Math.round(finalSubtotalPrice*100);
			finalSubtotalPrice=finalSubtotalPrice/100;
			productCountInShoppingCart=Integer.parseInt(UIFoundation.getText(driver, "CartHeadLineCount"));
			if((finalSubtotalPrice==finalSubTot) && (productCountInShoppingCart==productCountInCart ))
			{
				 objStatus+=true;
			     String objDetail="Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are matched";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 System.out.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are matched ");
			}
			else
			{
				  objStatus+=false;
				  String objDetail="Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are not matched";
			      UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
				  System.err.println("Verified Final Review page Subtotal with Cart page Subtotal after adding gift wrap price to cart page SubTotal and prices are not matched ");
			}*/
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtOrderNum));
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtGiftBagInOrderHistory))
			{
				  objStatus+=true;
			      String objDetail="Gift wrap is listed in Order Summary as a separate line item";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Gift wrap is listed in Order Summary as a separate line item");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Gift wrap is not listed in Order Summary as a separate line item";
				   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				   System.err.println("Gift wrap is not listed in Order Summary as a separate line item");
			}
			
			
			//objStatus+=String.valueOf(ApplicationDependent.verifyAdGiftWrappingPrice(giftBagValue, giftWrapCount, cartSubTot,productCountInCart));
			log.info("The execution of the method verifyGiftWrapping ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftWrapping "
					+ e);
			return "Fail";
		}
	}
	
}
