package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ForgotPasswordValidation extends Mobile {

	
	/***************************************************************************
	 * Method Name : forgotPassword() 
	 * Created By  : Chandrashekhar  
	 * Reviewed By : Ramesh. 
	 * Purpose :
	 ****************************************************************************
	 */
	public static String forgotPassword()
	{
		

		String objStatus=null;
		String actual=null;
		String expected=null;
		String actualEnterEMailAddMsg=null;
		String expectedEnterEMailAddMsg=null;
		
		String screenshotName = "Scenarios_ForgotPasswordValidation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method Forgot Password started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L);			
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnForgotPasswordLink));
			UIFoundation.waitFor(1L);
			actualEnterEMailAddMsg = verifyexpectedresult.plzEnterEmailMsg;
			expectedEnterEMailAddMsg=UIFoundation.getText(LoginPage.txtEnterEmailAddMsg);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtForgotEmail, "usernameMobile"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotContinue));
			UIFoundation.webDriverWaitForElement(LoginPage.btnForgotContinue, "Invisible", "", 50);
			
			expected = verifyexpectedresult.plzCheckUrMail;
			actual=UIFoundation.getText(LoginPage.txtCheckUrEmail);
			UIFoundation.waitFor(3L);
			if((expected.equalsIgnoreCase(actual)))
			{
				  objStatus+=true;
			      String objDetail="'Please Check Your Email' text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		
				
			}else{
					objStatus+=false;
			       String objDetail="'Please Check Your Email' text is displayed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			if((actualEnterEMailAddMsg.equalsIgnoreCase(expectedEnterEMailAddMsg)))
			{
				  objStatus+=true;
			      String objDetail="'Please enter the email address' text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				   objStatus+=false;
			       String objDetail="'Please enter the email address' text is displayed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName+"email", objDetail);
			       
			}
			log.info("The execution of the method Forgot Password ended here ...");
			if (!objStatus.contains("false"))
			{
				System.out.println("Forgot Password validation test case is executed successfully");
				return "Pass";
				
			}
			else
			{
				System.out.println("Forgot Password validation test case is failed");
				return "Fail";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method Forgot Password "+ e);
			return "Fail";
			
		}
	}
}

