package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyAddAntherTextAndCorporateGiftsLink extends Mobile {


	/***************************************************************************
	 * Method Name			: addaAnotherTextInVariousPages()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: TM-4133
	 ****************************************************************************
	 *//*
	public static String addAnotherTextInVariousPages(WebDriver driver) {

		String objStatus = null;
		String ScreenshotName = "addaAnotherTextInVariousPages.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		try {
			log.info("Add Another Text In Various Pages method started here......");
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "prodInHomePage"));
			String prodText=UIFoundation.getText(driver, "prodInHomePage");
			if(prodText.contains("Add Another"))
			{
				objStatus+=true;
				String objDetail = "Add Another text is verified in Home Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail = "Add Another text is  not displayed for Home page Products";
				 UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
			}
			//UIFoundation.scrollDownOrUpToParticularElement(driver, "WineLogo");
			UIFoundation.scrollUp(driver);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "TxtsearchProduct"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver, "TxtsearchProduct", "bonarda"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "searchedWinery"));
			UIFoundation.waitFor(1L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "FirstProductToCart"));
			prodText = UIFoundation.getText(driver, "FirstProductToCart");
			if (prodText.contains("Add Another")) {
				objStatus+=true;
				String objDetail = "Add Another text is verified in List Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail = "Add Another text is  not displayed for List Page";				
				 UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "thirdListProd"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "FirstProductToCart"));
			UIFoundation.waitFor(2L);
			prodText = UIFoundation.getText(driver, "FirstProductToCart");
			if (prodText.contains("Add Another")) {
				objStatus+=true;
				String objDetail = "Add Another text is verified in PIP Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail = "Add Another text is  not displayed for PIP Page";
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "obj_MyWine"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(driver, "FirstProdct"));
			prodText = UIFoundation.getText(driver, "FirstProdct");
			if (prodText.contains("Add Another")) {
				objStatus+=true;
				String objDetail = "Add Another text is verified in MyWine Page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail = "Add Another text is  not displayed for MyWine Page";				
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
			}
			log.info("Add Another Text In Various Pages method started here......");
			if(objStatus.contains("false")) {
				System.err.println("Add Another Text In Various Pages test case is failed");
				return "Fail";
			}
			else	
			{
				System.out.println("Add Another Text In Various Pages test case is Passed");
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println("Add Another Text In Various Pages test case is failed");
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}*/
	/***************************************************************************
	 * Method Name			: VerifyCorperateLinksInCustomerCareSection()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,
	 * Purpose				: TM-4127
	 ****************************************************************************
	 */
	public static String verifyCorperateLinksInCustomerCareSection() {
		String objStatus = null;
		String ScreenshotName = "VerifyCorperateLinksInCustomerCareSection.jpeg";
		
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		try {
			log.info("Verify Corperate Link In Customer Section methos started here....");

			driver.navigate().to("https://qwww.wine.com/");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.labCustomerCare);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.labCustomerCare));
			if(UIFoundation.isElementDisplayed(CartPage.labCorperateGifts)) {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.labCorperateGifts));
				UIFoundation.waitFor(2L);
				String corperateGift = driver.getCurrentUrl();
			
				if(corperateGift.contains("https://qwww.wine.com/content/landing/corporate-gifting")) {
					objStatus+=true;
					String objDetail = "Corporate Gifts link is displayed in customer care footer section succesfully";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}
				else {
					objStatus+=false;
					String objDetail1 = "Corporate Gifts link is not displayed in customer care footer section";
				    UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail1);
				    
				}		
			}
			log.info("Verify Corporate Link In Customer Section methos started here....");
			if(objStatus.contains("False")) {
				System.out.println("VerifyCorperateLinksInCustomerCareSection test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify Corperate Links I nCustomer Care Section test case is Pass");
				return "Pass";
			}
		}catch(Exception e){
			System.out.println("there is an exception arised during the execution of the method"+ e);
			return "fail";
		}
	}
}
