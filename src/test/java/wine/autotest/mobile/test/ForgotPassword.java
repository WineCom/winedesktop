package wine.autotest.mobile.test;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ForgotPassword extends Mobile {
	
	/***************************************************************************
	 * Method Name			: forgotPassword()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String forgotPassword()
	{
		String objStatus=null;
		String actual=null;
		String expected=null;
		
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method Forgot Password started here ...");
		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
					
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotPasswordLink));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtForgotEmail, "usernameMobile"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotContinue));
			UIFoundation.waitFor(5L);
			expected = verifyexpectedresult.plzCheckUrMail;			
					
		//	actual=UIFoundation.getText(LoginPage.txtCheckUrEmail);
			log.info("The execution of the method Forgot Password ended here ...");
			if(expected.equalsIgnoreCase(actual)){
				   objStatus+=true;
				   String objDetail="Forgot password validation is done";
				   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				    
			}else{
				 objStatus+=false;
			     String objDetail="Forgot Password test case is failed";
			     UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Forgot Password test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Forgot Password test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method Forgot Password "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: forgotPassword()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String forgotPassWordDuringCheckOut()
	{
		String objStatus=null;
		String actual=null;
		String expected=null;
		
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method Forgot Password started here ...");
			
			 objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			 UIFoundation.waitFor(3L);
			 if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
			 UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotPasswordLink));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtForgotEmail, "usernameMobile"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotContinue));
			UIFoundation.waitFor(5L);
			
            expected = verifyexpectedresult.plzCheckUrMail;			
			actual=UIFoundation.getText(LoginPage.txtCheckUrEmail);			
			/*expected=objExpectedRes.getProperty("plzCheckUrMail");
			actual=UIFoundation.getText(driver, "CheckUrEmail");*/
			log.info("The execution of the method Forgot Password ended here ...");
			if(expected.equalsIgnoreCase(actual)){
				   objStatus+=true;
				   String objDetail="Forgot password validation is done";
				   ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				    
			}else{
				 objStatus+=false;
			     String objDetail="Forgot Password test case is failed";
			 //    UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Forgot Password test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Forgot Password test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method Forgot Password "+ e);
			return "Fail";
			
		}
	}
}
