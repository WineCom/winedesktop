package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class RedWhiteButtonPresentinPriceQuantityWidget extends Mobile {
	

	
	/***********************************************************************************************************
	 * Method Name : FunctionalityRedorWhiteButtoninPriceQtyPage() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the functionality of White Wine'/'Red Wine' button present in Price & Quantity widget page' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String FunctionalityRedorWhiteButtoninPriceQtyPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_FunctionalityRedorWhiteButtoninPriceQtyPage_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNtVry));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			String strLiketoMixQuest = verifyexpectedresult.LiketoMixQuest;			
			String strexistLiketoMixQuest = UIFoundation.getText(PickedPage.spnLiketoMixQuest);
			System.out.println("strLiketoMixQuest : "+strLiketoMixQuest);
			System.out.println("strexistLiketoMixQuest : "+strexistLiketoMixQuest);
			if(strexistLiketoMixQuest.contains(strLiketoMixQuest)) {
				String objDetail="Would you like to add to the mix? - Question displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="Would you like to add to the mix? - Question Not displayed";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMixRedWhite));
			UIFoundation.waitFor(4L);
			String strpopupLiketoMixWhite = verifyexpectedresult.HeaderMixWhiteWine;						
			String strPopupexistLiketoMixWhite = UIFoundation.getText(PickedPage.spnHeaderRedWhitePopup);
			System.out.println("strpopupLiketoMixWhite : "+strpopupLiketoMixWhite);
			System.out.println("strPopupexistLiketoMixWhite : "+strPopupexistLiketoMixWhite);
			if(strPopupexistLiketoMixWhite.contains(strpopupLiketoMixWhite)) {
				String objDetail="How do you feel about these white wines? - Model window displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="How do you feel about these white wines? - Model window Not displayed";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}
			if(UIFoundation.isDisplayed(PickedPage.btnCancelMixPopup) && UIFoundation.isDisplayed(PickedPage.btnConfirmMixPopup)) {
				String objDetail="Varietal Preference model displayed with Confirme button and Cancel Link";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="Varietal Preference model Not displayed with Confirme button and Cancel Link";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMixQuestChardonnay));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnConfirmMixPopup));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.spnWhiteCount)) {
				String objDetail="Varietal selected in model PopUP is displayed in  Quantity section";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="Varietal selected in model PopUp is not displayed in Quantity section";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}
			
			if (objStatus.contains("false"))
			{
				String objDetail="Verify  Functionality of White Wine'/'Red Wine' button present in Price & Quantity widget page test case failed";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verify  Functionality of White Wine'/'Red Wine' button present in Price & Quantity widget page test case executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}


}
