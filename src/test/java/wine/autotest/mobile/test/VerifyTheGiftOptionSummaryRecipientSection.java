package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyTheGiftOptionSummaryRecipientSection extends Mobile {


	/***************************************************************************
	 * Method Name : verifyTheGiftOptionSummaryRecipientSection() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose : 
	 * TM-1349,1350
	 ****************************************************************************
	 */

	public static String verifyTheGiftOptionSummaryRecipientSection() {
		String objStatus = null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			
                          UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingType));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
	
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
		    objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
		    UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
	//		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientEidt));
			WebElement gift = driver.findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift.isSelected()){
				UIFoundation.clickObject(FinalReviewPage.cboRecipientGiftCheckbox);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			//	objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "RecipientContinue"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShipContinue, "Invisible", "", 50);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.txtAddGiftMessage))
			{
				objStatus+=true;
				String objDetail="Verified the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message");
			}
			else
			{
				objStatus+=false;
				String objDetail="Verify the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEidt));
			WebElement gift1 = driver.findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift1.isSelected()){
				UIFoundation.clickObject(FinalReviewPage.radRecipientGiftCheckbox);
			}
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.cboGiftWrapOption));
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnShipContinue, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAddGiftMessage))
			{
				objStatus+=true;
				String objDetail="Verified the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message");
			}
			else
			{
				objStatus+=false;
				String objDetail="Verify the gift option summary in collapsed state of Recipient section when only gift wrap is added without gift message is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtGiftMessageTextArea);
			UIFoundation.waitFor(2L); 
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtGiftMessageTextArea, "giftMessage"));
			UIFoundation.waitFor(2L);
			UIFoundation.clckObject(FinalReviewPage.txtObj_RecipientEmail);
			String expectedGiftMessageText=UIFoundation.getAttribute(FinalReviewPage.txtGiftMessageTextArea);
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnReciptContinue);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(12L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Invisible", "", 50);
			}
	
			String actualGiftMessage=UIFoundation.getText(FinalReviewPage.txtGiftMessage);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtGiftMessageSummary) && expectedGiftMessageText.equalsIgnoreCase(actualGiftMessage))
			{
				objStatus+=true;
				String objDetail="Verified the gift option summary in collapsed state of Recipient section when gift wrap and gift message are added to the order";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified the gift option summary in collapsed state of Recipient section when gift wrap and gift message are added to the order");
			}
			else
			{
				objStatus+=false;
				String objDetail="Verify gift option summary in collapsed state of Recipient section when gift wrap and gift message are added to the order is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName+"giftMessage", objDetail);
			}
					
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	
}

