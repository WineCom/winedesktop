package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyLikeSectionNtDisplayedTheVarietalPreferenceReviewPage extends Mobile {

	
	/***********************************************************************************************************
	 * Method Name : verifyLikeSectionInReviewPage() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose     : 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String verifyLikeSectionInReviewPage() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLikeSectionInReviewPage_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyLikeSectionInReviewPage started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRed));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnZinfendaldisLike));					
			UIFoundation.waitFor(1L);					
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(1L);
			if(!UIFoundation.isDisplayed(PickedPage.iconLikdThumsUp)) {
				String objDetail="Like section is not displayed when user does not provide like preference";
							 
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				objStatus+=true;
			}else {
				String objDetail="Like section is displayed when user does not provide like preference";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				objStatus+=false;
			}							
			
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{					
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method verifyLikeSectionInReviewPage"+ e);
			return "Fail";
		}
	}


}
