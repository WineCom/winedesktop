package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheBehaviourOfTheBillingAddresChechBox extends Mobile {	

	static boolean isObjectPresent=true;	

	/***************************************************************************
	 * Method Name : verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : The purpose of this method is to validate the error message displayed
	 * TM-625,
	 ****************************************************************************
	 */
	
	public static String verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected() {


		String objStatus = null;
		String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected started here ...");
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnObjCheckout);
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnReciptContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnReciptContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnReciptContinue, "Invisible", "", 10);				
			}			
			if(UIFoundation.isDisplayed(FinalReviewPage.txtMustSelectAddress))
			 {
			      objStatus+=true;
			      String objDetail="User Must select address before proceeding error message displayed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="select address before proceeding not dsplayed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }
			
			log.info("The execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected " + e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : checkBoxSelectedDefault()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * jira ID	   : TM-673
	 ****************************************************************************
	 */
	
	
	public static String checkBoxSelectedDefault() {
		String objStatus=null;
	
		String screenshotName = "Scenarios_cartPage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

			try {
				log.info("The execution of the method checkBoxSelectedDefault started here ...");
				UIFoundation.waitFor(6L);
				
				if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
				UIFoundation.waitFor(5L);	
				objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());	           				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);
				}				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				}
			
				if(UIFoundation.isDisplayed(FinalReviewPage.chkBillingAndShippingCheckbox))
				 {
				      objStatus+=true;
				      String objDetail="Billing same as Shipping checkbox is selected by default succesfully";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			   
				 }else
				 {					
				       objStatus+=false;
				       String objDetail="Billing same as Shipping checkbox is not selected by default";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				 }
				
				log.info("The execution of the method checkBoxSelectedDefault ended here ...");
				if ( objStatus.contains("false")) {
					System.out.println(" Order creation with existing account test case is failed");
					return "Fail";
				} else {
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method checkBoxSelectedDefault "
						+ e);
				return "Fail";
			}
	
	}
	
	/***************************************************************************
	 * Method Name : unCheckBoxSelected()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * TM-
	 ****************************************************************************
	 */
	
	public static String unCheckBoxSelected() {
		String objStatus=null;
		String screenshotName = "Scenarios_unCheckBoxSelected_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

			try {
				log.info("The execution of the method unCheckBoxSelected started here ...");
				UIFoundation.waitFor(6L);
				
				if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
				UIFoundation.waitFor(1L);
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 70);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkBillPaymentEdit))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkBillPaymentEdit);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkBillPaymentEdit));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
					UIFoundation.waitFor(1L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
					UIFoundation.waitFor(2L);
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
					
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				}
				if(UIFoundation.isSelected(FinalReviewPage.chkBillingAndShippingCheckbox))				{

					objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingAddress));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingSuite));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingCity));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtBillinState));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtobj_BillingZip));
					objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtNewBillingPhone));
					objStatus+=true;
					String objDetail="All fields are displayed when click on BillingAndShippingCheckbox ";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}
			 else
			 {
			       objStatus+=false;
			       String objDetail="All fields are not displayed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }
				log.info("The execution of the method unCheckBoxSelected ended here ...");
				if ( objStatus.contains("false")) {
					System.out.println(" Order creation with existing account test case is failed");
					return "Fail";
				} else {
					return "Pass";
				}
			} catch (Exception e) {
				
				log.error("there is an exception arised during the execution of the method unCheckBoxSelected "
						+ e);
				return "Fail";
			}
	
	}

}
