package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class LoginUsingFacebook extends Mobile {	

	
	/***************************************************************************
	 * Method Name			: loginUsingFacebook()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String loginUsingFacebook()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_facebookLogin_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method loginUsingFacebook started here ...");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnContinueWithFacebook));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFacebookEmail, "usernameFacebook"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtFacebookPass, "passwordFacebook"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.txtFacebookLogin));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEid)){
				  objStatus+=true;
			      String objDetail="Customer is navigated to Recipient section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="Customer is unable to navigated to Recipient section";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method loginUsingFacebook ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the user is able to login using Facebook account test case failed");
				 String objDetail="User is able to login using Facebook account test case failed";
			      ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
				return "Fail";
				
			}
			else
			{
				System.out.println("Verify the user is able to login using Facebook account test case executed successfully");
				 String objDetail="User is able to login using Facebook account  test case executed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
}
