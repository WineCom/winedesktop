package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyErrorDisplayedForInvalidProductId extends Mobile {
	

	

	/***************************************************************************
	 * Method Name			: verifyErrorDisplayedForInvalidProductId()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String verifyErrorDisplayedForInvalidProductId() {
		String objStatus=null;
		   String screenshotName = "Scenarios__ProductInvalidId.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
					//WebDriverWait wait=null;
		try {
			log.info("The execution of the method verifyErrorDisplayedForInvalidProductId started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkAddProductToPIP));
			UIFoundation.waitFor(2L);
			String productURl=driver.getCurrentUrl();
			driver.get(productURl+"james");
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtEror404);
			if(UIFoundation.isDisplayed(ListPage.txtEror404))
			{
				  objStatus+=true;
			      String objDetail="'Error 404' is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="'Error 404' is not displayed";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method verifyErrorDisplayedForInvalidProductId ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyErrorDisplayedForInvalidProductId "+ e);
			return "Fail";
		}

	}
}

