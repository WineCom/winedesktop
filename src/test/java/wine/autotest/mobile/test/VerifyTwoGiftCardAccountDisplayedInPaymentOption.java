package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTwoGiftCardAccountDisplayedInPaymentOption extends Mobile {
	

	/***************************************************************************
	 * Method Name			: verifyTwoGiftCardreedemed()
	 * Created By			: Chandrashekhar  
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */


	public static String verifyTwoGiftCardReedemed()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_twoGiftCert_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;

		try
		{
			log.info("The execution of the method enterGiftCertificate started here ...");
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				UIFoundation.waitFor(20L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(1L);
				/*objStatus+=String.valueOf(UIFoundation.setObject(driver, "GiftNumber", "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "GiftApply"));*/
				UIFoundation.waitFor(20L);				
			}
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Multiple gift cards are applied succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Multiple gift cards are not applied";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify more than 2 gift cards can be redeemed test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify  more than 2 gift cards can be redeemed test case executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";

		}
	}


	/***************************************************************************
	 * Method Name : verifyTwoGiftCardAccountsDisplayed() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * Jira Id     : TM-4604
	 ****************************************************************************
	 */

	public static String verifyTwoGiftCardAccountsDisplayed() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyTwoGiftCardAccountsDisplayed.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method verifyTwoGiftCardAccountsDisplayed started here ...");

			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinueShipRecpt);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueShipRecpt)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueShipRecpt));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueShipRecpt, "Invisible", "", 50);
			}
			UIFoundation.waitFor(5L);
	
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueShipRecpt))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueShipRecpt));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueShipRecpt, "Invisible", "", 50);
				UIFoundation.waitFor(5L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
				UIFoundation.waitFor(5L);
			}

			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);	
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(1L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);		
					UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
					objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
					UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkBillPaymentEdit));
				UIFoundation.waitFor(1L);
			
				if(UIFoundation.isDisplayed(FinalReviewPage.txtRemainingGiftCard1) && UIFoundation.isDisplayed(FinalReviewPage.txtRemainingGiftCard2))
				{
										
					objStatus+=true;
					String objDetail = "2 Gift card Accounts displayed in the payment option along with Gift balance";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");					
				}
				else
				{
					objStatus+=false;
					String objDetail = "2 Gift card Accounts not displayed in the payment option.";	
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
					
				}
			}

			log.info("The execution of the method verifyTwoGiftCardAccountsDisplayed ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}


		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyTwoGiftCardAccountsDisplayed "
					+ e);
			return "Fail";
		}
	}	
}
