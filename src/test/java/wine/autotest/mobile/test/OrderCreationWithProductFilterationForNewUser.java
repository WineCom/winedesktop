package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithProductFilterationForNewUser extends Mobile {

	
	/***************************************************************************
	 * Method Name			: productFilteration()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to filter the product 
	 * 						  list by selecting some product,region,rate and price 
	 * 						  and checking for the count
	 ****************************************************************************
	 */
	
	public static String productFilter()
	{
		String objStatus=null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try
		{
			log.info("The execution of method productFilteration started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.dwnVarietalLink));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtPinotNoir));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.dwnRegionR));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.dwnOregon));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.dwnWillametteValley));
			UIFoundation.waitFor(8L);
		//	objStatus+=String.valueOf(UIFoundation.clickObject(driver, "YamhillCarlton"));
		//	UIFoundation.waitFor(8L);
		/*	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RatingAndPrice"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "Price"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "Rating"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "Done"));*/
			UIFoundation.waitFor(3L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstFilteredProd);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.clickObject(ListPage.btnFirstFilteredProd);
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondFilteredProd);
			if (addToCart2.contains("Add to Cart")) {
	//			UIFoundation.scrollDownOrUpToParticularElement(driver, "SecondFilteredProd");
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondFilteredProd));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdFilteredProd);
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdFilteredProd));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthFilteredProd);
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthFilteredProd));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthFilteredProd);
			if (addToCart5.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthFilteredProd));
			}
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false"))
			{
				
				System.out.println("Product Filteration test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Product Filteration test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method productFilteration "+e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : addNewCreditCard() Created By : Vishwanath Chavan Reviewed
	 * By : Ramesh,KB Purpose : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method addNewCreditCard started here ...");
		//	expected = objExpectedRes.getProperty("placeOrderConfirmation");
			expected = verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(12L);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
		    	objStatus+=true;			
			}else
			{
				objStatus+=false;
			    String objDetail="Order number is null and Order cannot placed";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order creation with Product filteration for new user  test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with Product filteration for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}


	
}
