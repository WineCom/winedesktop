package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class DeletePreferredCreditCardUserProfile extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addPaymentMethod()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 ****************************************************************************
	 */

	public static String addPaymentMethod()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__PaymentMethod.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
		try
		{
			log.info("The execution of the method addAddress started here ...");
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtPaymentMethods));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtAddNewCard));
			UIFoundation.webDriverWaitForElement(ThankYouPage.txtAddNewCard, "Clickable", "", 50);
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtCVV, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtBillingAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtBillingCty, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(ThankYouPage.txtBillingSelState, "dryState"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtBillingZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtBillingPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtMakeThisPreferredCard));			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtBillingSaveButton));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkcreditCardEdit));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.btnemoveThisCard));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.btnYesRemoveThisCard));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtYouDoNotHaveCreditCard)){
				  objStatus+=true;
			      String objDetail="The Preferred Card is removed, user is redirected to the Payment methods page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				 objStatus+=false;
				   String objDetail="The Preferred Card is not removed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") && (!UIFoundation.isDisplayed(ThankYouPage.txtPaymentMethodsHeader)))
			{
				
				return "Fail";
			}
			else
			{
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addPaymentMethod "+ e);
			return "Fail";
		}
	}
}
