package wine.autotest.mobile.test;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated extends Mobile {



	/*****************************************************************************************
	 * Method Name :  VerifyFedexDisplaysProperGoogleAdresses 
	 * Created By  :  Chandrashekhar 
	 * Reviewed By :  Ramesh
	 * Purpose : 
	 * TM-3966
	 *******************************************************************************************
	 */

	public static String verifyAddressSuggestioIsDisplayed() {

		String objStatus = null;
		String suggestedAddress= null;
		String ScreenshotName= "VerifyAddressSuggestioIsDisplayed.jpeg";
		String Screenshotpath =System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;

		try
		{
			log.info("Verify Address Suggestio Is Displayed Method Started Here");

			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}

			UIFoundation.waitFor(10L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.lnkRecipientChangeAddress))
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientChangeAddress));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtAddNewAddressLink));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "AddressR"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipR"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.spnSuggestedAddressR)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("verified that suggested addres is displayed in verify Shopping address", "Pass", "");
				suggestedAddress=UIFoundation.getText(FinalReviewPage.spnSuugestedAddressDisply);
				System.out.println("Address displayed in Verify Shipping Address: " +suggestedAddress);
			}
			else
			{
				objStatus+=false;
				String objDetail="Suggested address is not displayed and address is autoupdated";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				 UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail); 
			}
			log.info("Verify Address Suggestion Is Displayed Method Ended Here");
			if(objStatus.contains("false")) {

				System.out.println("Verification of  Address Suggestion test case failed");
				return "fail";
			}
			else
			{
				System.out.println("Verification of  Address Suggestion test case passed");
				return "Pass";

			}
		}
		catch(Exception e){
			log.error("there is an exception arised during the execution of the method execution " + e);
			return "Fail";

		}

	}
}
