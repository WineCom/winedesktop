
package wine.autotest.mobile.test;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;


public class UserCreationWithCheckoutProcess extends Mobile {
	

	
	
	private static final By SelectState = null;
	public static String expectedState=null;
	public static String actualState=null;
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		boolean isElementPresent=false;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFirstProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnSecondProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnThirdProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductToCart));
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFourthProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFifthProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
			}
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: addProductToCartFromListPage()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

public static String addProductToCartFromListPage() {
	String objStatus=null;
		
		try {
			log.info("The execution of the method addProductToCartFromListPage started here ...");
		//	driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			WebElement ele = driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel = new Select(ele);
			expectedState = sel.getFirstSelectedOption().getText();
			System.out.println("expected state:" + expectedState);
			UIFoundation.SelectObject(SelectState, "State");
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkRhoneBlends));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));

			log.info("The execution of the method addProductToCartFromListPage ended here ...");
			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
			
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method addProductToCartFromListPage "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : userProfileCreation() 
	 * Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to Create
	 * new user account
	 ****************************************************************************
	 */

	public static String userProfileCreation() {
		String objStatus = null;
		try {
			log.info("The execution of method create Account started here");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton))
			{
				objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.JoinNowButton));
				UIFoundation.waitFor(1L);
			}
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email, "email"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.Password, "accPassword"));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(12L);
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "obj_WineHome"));
			UIFoundation.waitFor(4L);
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavAccountTab"));
			/*if(UIFoundation.isDisplayed(driver, "Tooltip")){
			    UIFoundation.clickObject(driver, "Tooltip");
			    UIFoundation.waitFor(2L);
			   }
			*/
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignoutLink));
			UIFoundation.waitFor(4L);
			log.info("The execution of the method create Account ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("User profile creation  test case is failed");
				return "Fail";
			} else {
				System.out.println("User profile creation  test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account " + e);
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_StatesNotMatched_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method login started here ...");
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L);			
			/*if(UIFoundation.isDisplayed(driver, "JoinNowButton")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver, "JoinNowButton"));
				UIFoundation.waitFor(2L);
			}
			*/
			/*if(UIFoundation.isDisplayed(driver, "signInLinkAccount")){
				objStatus+=String.valueOf(UIFoundation.clickObject(driver,"signInLinkAccount"));
			}*/
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.newUsersetObject(LoginPage.LoginEmail,userEmail));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[2]"));
			Select sel=new Select(ele);
			actualState=sel.getFirstSelectedOption().getText();
			System.out.println("actual state:"+actualState);
			log.info("The execution of the method login ended here ...");
			
			if(expectedState.equalsIgnoreCase(actualState)){
				objStatus+=true;
			    String objDetail="States are matched";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				objStatus+=false;
			    String objDetail="States are not matched";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	
}
