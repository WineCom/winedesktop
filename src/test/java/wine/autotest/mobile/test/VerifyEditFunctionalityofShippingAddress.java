
package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyEditFunctionalityofShippingAddress extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: recipientAddressEdit()
	 * Created By			: Chandrashehar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String recipientAddressEdit() {
	String objStatus=null;
	String recipientName1=null;
	String recipientName2=null;
	String recipientName3=null;
	String recipientName4=null;
	
		try {
			log.info("The execution of the method recipientAddressEdit started here ...");

			UIFoundation.waitFor(4L);		

                if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			UIFoundation.waitFor(12L);  
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkRecipientEid);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientEid));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkEditShippingAddress));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtShippingHeaderMsg));
			log.info("The execution of the method recipientAddressEdit ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Verify the edit functionality of shipping address test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the edit functionality of shipping address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method recipientAddressEdit "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: preferredAddressEdit()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: TM-2780
	 ****************************************************************************
	 */
	
	public static String preferredAddressEdit() {
	String objStatus=null;

	
		try {
			log.info("The execution of the method preferredAddressEdit started here ...");

			
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEid))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEid));
				UIFoundation.waitFor(3L);	
			}
			String PreferredAddresstoedit = UIFoundation.getAttribute(FinalReviewPage.spnRadioPrefferedAddress2);
			if(PreferredAddresstoedit.contains("false")) {
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.obj_SecondAddrEdit));
			UIFoundation.waitFor(2L);
			}
			else {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.obj_ThirdAddrEdit));
				UIFoundation.waitFor(2L);
			}
			
			objStatus+=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.txtShippingHeaderMsg));
			
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.ChkPreferredAddressBook));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPreferredSave));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueButton));
				UIFoundation.waitFor(3L);	
			}
			String ShiptoAddress = UIFoundation.getAttribute(FinalReviewPage.btnShipTothisAdrRadion);
			String PreferredAddress = UIFoundation.getAttribute(FinalReviewPage.cboRadioPrefferedAddress);
			if(ShiptoAddress.contains("true") && PreferredAddress.contains("true") )
			{
				objStatus+=true;
				String objDetail = "Preffered Address is Selected as Default Ship to Address";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
			}else {
				objStatus+=false;
				String objDetail = "Preffered Address is NOT Selected as Default Ship to Address";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			
			System.out.println("preferredAddressEdit :"+objStatus);
			log.info("The execution of the method preferredAddressEdit ended here ...");
			if (objStatus.contains("false")) {
				String objDetail = "Verify the edit functionality of preferred shipping address test case is failed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
			} else {
				String objDetail = "Verify the edit functionality of preferred shipping address test case is executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method preferredAddressEdit "
					+ e);
			return "Fail";
		}
	}


}
