package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyStewardshipProgramAddedToCart extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : addStewardshipToCart() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addStewardshipToCart() {
		String objStatus = null;
		String shippingAndHandling=null;
		String stewardshipPrice=null;
		String stewardshipDiscount=null;
		String stewardshipDiscountPrice=null;
		String stewardshipSave=null;
		String stewardshipText=null;
		String expectedStewardshipSave=null;
		String expectedStewardshipText=null;
		String stewardshipSavings=null;
		
		String screenshotName = "Scenarios_Steward_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method addStewardshipToCart started here ...");
			UIFoundation.waitFor(3L);
			expectedStewardshipSave = verifyexpectedresult.stewardshipSave;
			expectedStewardshipText = verifyexpectedresult.stewardshipText;
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			System.out.println("Shipping and handling : "+shippingAndHandling);
			UIFoundation.waitFor(2L);
			stewardshipSavings=UIFoundation.getText(CartPage.btnObj_StewardshipSaving);
			System.out.println("Stewardship savings: "+stewardshipSavings);
			UIFoundation.waitFor(2L);
			stewardshipSave=UIFoundation.getText(CartPage.btnStewardshipSave);
			stewardshipText=UIFoundation.getText(CartPage.txtStewardshipText);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(CartPage.btnAddStewardshipMember));
			UIFoundation.waitFor(2L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.txtStewardshipConfirm));
			UIFoundation.waitFor(4L);
			stewardshipPrice=UIFoundation.getText(CartPage.txtStewardshipDiscountPrice);
			stewardshipDiscountPrice=stewardshipPrice.replaceAll("-", "");
			stewardshipDiscount=stewardshipDiscountPrice.replaceAll("\\(|\\)", "");
			System.out.println("Stewardship Savings : "+stewardshipDiscount);
			log.info("The execution of the method addStewardshipToCart ended here ...");
			
			if(shippingAndHandling.equalsIgnoreCase(stewardshipDiscount))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Shipping and Handling and stewardship discount matched.", "Pass", "");
				System.out.println("Shipping and Handling and stewardship discount matched");
				
			}else
			{
				objStatus +=false;
				String objDetail="Shipping and Handling and stewardship discount did not match";
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("Password Error mssage is validation test cases failed.");
				
			}
			if(stewardshipText.contains(expectedStewardshipText))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Actual stewarship text and expected stewardship text matched", "Pass", "");
				System.out.println("Actual stewarship text and expected stewardship text mathced.");
				
			}else
			{
				objStatus +=false;
				String objDetail="Actual stewarship text and expected stewardship text did not match";
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("Actual stewarship text and expected stewardship text didnot match.");
			}
			log.info("The execution of the method addStewardshipToCart ended here ...");
			
			if (objStatus.contains("false")) {
				
				System.out.println("Verify 'Stewardship program' is added to cart test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify 'Stewardship program' is added to cart test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addStewardshipToCart "+ e);
			return "Fail";
		}
	}

}

