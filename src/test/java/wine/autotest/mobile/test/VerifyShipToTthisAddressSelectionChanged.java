package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyShipToTthisAddressSelectionChanged extends Mobile {

	
	/***************************************************************************
	 * Method Name			: loginPrefferedAddress()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String loginPrefferedAddress()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(2L);			
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "preferenceAddressEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: preferredAdresSelChanged()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				:
	 ****************************************************************************
	 */
	
	public static String preferredAdresSelChanged() {
	

	String objStatus=null;
	   String screenshotName = "Scenarios_preferredAdress_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method preferredAdresSelChanged started here ...");
			
			UIFoundation.waitFor(2L);
		//	
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(2L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkShipAdrEdit));
			String userFullName=UIFoundation.getAttribute(FinalReviewPage.txtFhipAdrFullName);

			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboChipAdrPreferredCardCheckBox));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.bynShipAdrSave));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueButton))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueButton));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueButton, "Invisible", "", 50);
			}
			String shippingAddressExpectedName=UIFoundation.getText(FinalReviewPage.txtShipAdrExpFullName);
			if(UIFoundation.isSelected(FinalReviewPage.btnShipTothisAdrRadion) && (userFullName.equalsIgnoreCase(shippingAddressExpectedName)))
			{
				  objStatus+=true;
			      String objDetail="Preferred address is updated,It is set as 'ship to this address'";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Preferred address is not updated,It is not set as 'ship to this address'";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"preferredSetAddr", objDetail);
				
			}

			log.info("The execution of the method preferredAdresSelChanged ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify ship to this address selection is changed on updating the preferred address. test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify ship to this address selection is changed on updating the preferred address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method preferredAdresSelChanged "
					+ e);
			return "Fail";
		}
	}
	
}
