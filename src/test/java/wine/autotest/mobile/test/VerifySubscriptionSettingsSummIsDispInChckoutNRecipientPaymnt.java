package wine.autotest.mobile.test;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;

import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt extends Mobile {




	/*********************************************************************************************
	 * Method Name : verifySubscriptionSettingsInChckout() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose     : Verifies Subscription Settings is displayed in Recipient & Payment option
	 ********************************************************************************************
	 */

	public static String verifySubscriptionSettingsInChckout() {
		String objStatus=null;
		String screenshotName = "Scenarios_verifySubscriptionSettingsInChckout_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("The execution of the verifySubscriptionSettingsInChckout method strated here");
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			//objStatus+=String.valueOf(UIFoundation.clickObject(OMSPage. "VerifyFedexAddress"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocarion));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtFedexSaveButton));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
					//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsHeader)) {
				objStatus+=true;
				String objDetail="Subscription Settings' summary is  displayed checkout flow";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Subscription Settings' summary is not displayed checkout flow";
				  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.lnkSubscriptionEditOpt));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtTargetPrice));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtWineTypes));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtFrequency));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtDiscoveryLevel));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtFreeShipping));
			objStatus+=String.valueOf(UIFoundation.isDisplayed(PickedPage.txtSatisfactionGurante));
			log.info("The execution of the method verifySubscriptionSettingsInChckout ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="'VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt' Test case Failed ";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				System.out.println("Verified the UI for 'Subscription Settings' in Recipient & Payment section is not displayed ");
				return "Fail";
			}
			else
			{
				String objDetail="'VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt' Test case executed succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified the UI for 'Subscription Settings' in Recipient & Payment sectionis  displayed");
				return "Pass";
			}
		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
