package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class AddressGetsSavedOnceOnAddingNewAddressInUserProfile extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(1L);			
						
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "addAddressUserNameInUserProfile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : UpdateShippingAddressInAddressBook() 
	 * Created By  : Chandrashekhar
	 * Chavan Reviewed By : Ramesh.
	 * Purpose : 
	 * TM-2224,2159
	 ****************************************************************************
	 */

	public static String addAddressInAddressBook() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;

		try {
			log.info("The execution of the method updateShippingAddressInAddressBook started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavTabSignOut"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtAddressBook));
			UIFoundation.webDriverWaitForElement(ThankYouPage.txtAddNewAddressLink, "Clickable", "", 50);
			int addressCountBeforUAddingNewAddress=UIFoundation.listSize(ThankYouPage.spnShippingAddressCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtAddNewAddressLink));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtAddressFullName, "firstName"));
		//	objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage. "LastName", "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(ThankYouPage.dwnObj_State, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtObj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnAddressSave));
			UIFoundation.waitFor(3L);		
			if(UIFoundation.isDisplayed(ThankYouPage.btnDeliveryContinue))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
			int addressCountAfterUAddingNewAddress=UIFoundation.listSize(ThankYouPage.spnShippingAddressCount);
			if(((addressCountBeforUAddingNewAddress+1)==addressCountAfterUAddingNewAddress)){
				objStatus+=true;
			      String objDetail="Verified that address gets saved only once on adding  the new address in Shipping addresses page under user profile";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified that address gets saved only once on adding the new address in Shipping addresses page under user profile");
			}else{
				 objStatus+=false;
				   String objDetail="Verify  that address gets saved only once on adding  the new address in Shipping addresses page under user profile  is failed";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.err.println("Verify that address gets saved only once on adding  the new address in Shipping addresses page under user profile is failed ");
			}
			log.info("The execution of the method updateShippingAddressInAddressBook ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method updateShippingAddressInAddressBook "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : removeAddressInAddressBook() 
	 * Created By  : Chandra shekhar
	 * Reviewed By : Ramesh, 
	 * Purpose : 
	 * TM-1859
	 ****************************************************************************
	 */

	public static String removeAddressInAddressBook() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;

		try {
			log.info("The execution of the method removeAddressInAddressBook started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtAddressBook));
			UIFoundation.webDriverWaitForElement(ThankYouPage.txtAddNewAddressLink, "Clickable", "", 50);
			int addressCountBeforUAddingNewAddress=UIFoundation.listSize(ThankYouPage.spnShippingAddressCount);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.spnEditShippingAddress));
			UIFoundation.waitFor(2L);			
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnRemoveAddress));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnYesremoveThisAddress));
			UIFoundation.waitFor(3L);
					
			int addressCountAfterUAddingNewAddress=UIFoundation.listSize(ThankYouPage.spnShippingAddressCount);
			if(((addressCountBeforUAddingNewAddress-1)==addressCountAfterUAddingNewAddress)){
				objStatus+=true;
			      String objDetail=" Address gets removed from the recepient page under user profile is succesfully ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Address gets removed from the recepient page under user profile is succesfully");
			}else{
				 objStatus+=false;
				   String objDetail="Address gets removed from the recepient page under user profile  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.err.println("Address gets removed from the recepient page under user profile  is failed");
			}
			log.info("The execution of the method removeAddressInAddressBook ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method removeAddressInAddressBook "
					+ e);
			return "Fail";
		}
	}

}
