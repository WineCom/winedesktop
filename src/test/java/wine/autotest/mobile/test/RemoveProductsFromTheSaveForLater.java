package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;


public class RemoveProductsFromTheSaveForLater extends Mobile {
	static ArrayList<String> expectedItemsName=new ArrayList<String>();
	static ArrayList<String> actualItemsName=new ArrayList<String>();
	static String products1=null;
	static String products2=null;
	static String products3=null;
	static String cartCountBeforeLogout=null;
	static int cartCountBefreLogout=0;
	static String cartCountAfterLogin=null;
	static int cartCountAftrLogin=0;
	
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String addProductsToCart(WebDriver driver) {
		
		
		String objStatus=null;
		try {
			log.info("The execution of the method addProductsToCart started here ...");
			driver.navigate().refresh();
		//	UIFoundation.SelectObject(driver, "SelectState", "State");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkBordexBlends));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnThirdProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFourthProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(ListPage.lnkSrt);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCartCount));
			log.info("The execution of the method addProductsToCart ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: moveProductsToSaveForLater()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String moveProductsToSaveForLater() {

		String objStatus=null;
		try {
			log.info("The execution of the method saveForLater started here ...");
			objStatus+=String.valueOf(UIBusinessFlow.validationForRemoveProductsSaveForLater(driver));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveThirdProductFromSave));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnObjRemove));
			
			System.out.println("=========================Products available in save for later section Before  logout================================");
			if(!UIFoundation.getText(CartPage.spnFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.spnFirstProductInSaveFor);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.spnThirdProductInSaveFor).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.spnThirdProductInSaveFor);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			BaseTest.logout();
			log.info("The execution of the method saveForLater ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: productsAvailableInSaveForLater()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String productsAvailableInSaveForLater() {
		String objStatus=null;
		String screenshotName = "Scenarios_ProductMismatched_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try {
			log.info("The execution of the method saveForLater started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			System.out.println("=========================Products available in save for later section after login================================");
			if(!UIFoundation.getText(CartPage.spnFirstProductInSaveFor).contains("Fail"))
			{
				products1=UIFoundation.getText(CartPage.spnFirstProductInSaveFor);
				System.out.println("1) "+products1);
				actualItemsName.add(products1);
			}
			if(!UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor).contains("Fail"))
			{
				products2=UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor);
				System.out.println("2) "+products2);
				actualItemsName.add(products2);
			}
			if(!UIFoundation.getText(CartPage.spnThirdProductInSaveFor).contains("Fail"))
			{
				products3=UIFoundation.getText(CartPage.spnThirdProductInSaveFor);
				System.out.println("3) "+products3);
				actualItemsName.add(products3);
			}
			

			log.info("The execution of the method saveForLater ended here ...");
			if(expectedItemsName.containsAll(actualItemsName)){
				 objStatus+=true;
			     String objDetail="All products are available in save for later section";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}else{
				 objStatus+=false;
			     String objDetail="Mismatched products name in save for later section";
			   //  UIFoundation.captureScreenShot(screenshotpath, objDetail);
			     UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if(objStatus.contains("false"))
			{
				System.out.println("mismatched products name in save for later section");
				return "Fail";
			}
			else
			{	System.out.println("all products are available in save for later section");
				return "Pass";
			}
			
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method saveForLater "+ e);
			return "Fail";
		}
	}
	
}

