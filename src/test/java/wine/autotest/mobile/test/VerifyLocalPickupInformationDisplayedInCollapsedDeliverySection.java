package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection extends Mobile {



	/*****************************************************************************************
	 * Method Name : VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose : 
	 * TM-4027
	 *******************************************************************************************
	 */
	public static String addFedExAddress() {
		String actualFedexDeliveyMessage= null;
		String expectedFedexDeliveyMessage=null;
		String  objStatus = null;
		String ScreenshotName= "VerifyLocalPickupInformationDisplayed.jpeg";
		String Screenshotpath =System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ ScreenshotName;
		try
		{
			log.info("FEDEX Delivery message Execution method started here:");
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(10L);		
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkShipToFedex));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.waitFor(3L);		
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipToThisLocation));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtFedexSaveButton));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isElementDisplayed(FinalReviewPage.spnFedEXShippingDeliveryMessage)){
				actualFedexDeliveyMessage=UIFoundation.getText(FinalReviewPage.spnFedEXShippingDeliveryMessage);				
			}
			expectedFedexDeliveyMessage = verifyexpectedresult.FedexDeliveryMessageR;	
				
			if(actualFedexDeliveyMessage.contains(expectedFedexDeliveyMessage)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the FedexPickup Message in Delivery Section", "Pass", "");
				System.out.println(actualFedexDeliveyMessage);
			}
			else
			{
				objStatus+=false;
				String objDetail="Verified the FedexPickup Message in Delivery Section is not displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				 UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);

			}
			log.info("FEDEX Delivery message Excution method started here:");
			if(objStatus.contains("false")){


				System.out.println("Verified the FedexPickup Address Message in Delivery Section testcase failed");

				return "Fail";
			}
			else {


				System.out.println("Verified the FedexPickup Address Message in Delivery Section testcase succesfully");

				return "Pass";

			}

		}catch(Exception e) {
			return "Fail";
		}

	}

}
