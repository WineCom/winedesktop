package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyStateChangeDialog extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : verifyStateChangrDialog() 
	 * Created By :  chandrashekhar
	 * Reviewed By : Ramesh 
	 * Purpose : 
	 ****************************************************************************
	 */

	public static String verifyStateChangrDialog() {
		String objStatus = null;
        String expectedStateChangeDialogMsg=null;
        String actualStateChangeDialogMsg=null;
           String screenshotName = "Scenarios__stateChangeScreenshot.jpeg";
           String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
        try {
               log.info("The execution of the method verifyStateChangrDialog started here ...");
               UIFoundation.waitFor(3L);
               expectedStateChangeDialogMsg = verifyexpectedresult.stateChangeDailogbox;
       
               objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.imgWineLogo));
               UIFoundation.waitFor(2L);
               UIFoundation.SelectObject(ListPage.cboSelectState, "dryState");
               UIFoundation.waitFor(10L);
              
               actualStateChangeDialogMsg=UIFoundation.getText(ListPage.cboStateChangeDailog);
               System.out.println(actualStateChangeDialogMsg);
               log.info("The execution of the method verifyStateChangrDialog ended here ...");
               if(actualStateChangeDialogMsg.contains(expectedStateChangeDialogMsg)){
            	   objStatus+=true;
                   ReportUtil.addTestStepsDetails("Actual and expected state change message are same", "Pass", "");
             
               }else{
            	   objStatus+=false;
                   String objDetail="Actual and expected state change message are not same";
                   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
               }
               
               if(UIFoundation.isDisplayed(ListPage.cboStateChangeDailog))
               {
                     objStatus+=true;
                     ReportUtil.addTestStepsDetails("Verify the state change dialog test case is executed successfully", "Pass", "");
               }else
               {
                     objStatus+=false;
                     String objDetail="Verify the state change dialog test case is failed";                 
                     UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
               }
               
               if (objStatus.contains("false")) {
                     System.out.println("Verify the state change dialog test case is failed");
                     return "Fail";
               } else {
                     System.out.println("Verify the state change dialog test case is executed successfully");
                     return "Pass";
               }

        } catch (Exception e) {

               log.error("there is an exception arised during the execution of the method verifyStateChangrDialog "
                            + e);
               return "Fail";
        }

	}


}

