package wine.autotest.mobile.test;

import java.io.IOException;
import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyUserIsNavigatedToAppropriateTextUrl extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: itemsNotEligibleForPromoCodeRedemption()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 * TM-1924
	 ****************************************************************************
	 */
	public static String verifyUserIsNavigatedToAppropriateTextUrl() {
		String objStatus = null;

		   String screenshotName = "Scenarios__productNameInURL.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkProductInfo));
			UIFoundation.waitFor(5L);
			String productNameInPIP=UIFoundation.getText(ListPage.txtPIPName);
			productNameInPIP=productNameInPIP.replace(" ", "");
			productNameInPIP=productNameInPIP.replace("&", "");
			productNameInPIP=productNameInPIP.replace("(", "");
			productNameInPIP=productNameInPIP.replace(")", "");
			productNameInPIP=productNameInPIP.replace("-", "");
			productNameInPIP=productNameInPIP.replaceAll("'", "");
			String productNameIn=driver.getCurrentUrl();
			 int index=productNameIn.lastIndexOf('/');
			String productNameInURL=productNameIn.substring(30,index);
			String actualProductNameInURL=productNameInURL.replaceAll("-", "");
		//	actualProductNameInURL=actualProductNameInURL.replaceAll("and", "");
			if(actualProductNameInURL.equalsIgnoreCase(productNameInPIP))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified the user is navigated to appropriate text url on clicking the product in list page", "Pass", "");
			}else
			{
				objStatus+=false;
				String objDetail="Verify the user is navigated to appropriate text url on clicking the product in list page test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
}
