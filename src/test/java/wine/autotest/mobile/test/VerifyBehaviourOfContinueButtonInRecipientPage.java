

package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyBehaviourOfContinueButtonInRecipientPage extends Mobile {	

	
	/***************************************************************************
	 * Method Name : VerifyBehaviourOFContinueButtonInRecipientPage()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose : 
	 * TM-895,
	 ****************************************************************************
	 */

	public static String verifyBehaviourOFContinueButtonInRecipientPage() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
                    
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.txtStreetAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.txtCityRequired) && UIFoundation.isDisplayed(FinalReviewPage.txtStateRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.txtZipRequiredInRecipientPage) && UIFoundation.isDisplayed(FinalReviewPage.txtPhoneRequired) )
			{
				  objStatus+=true;
			      String objDetail="Verified  the behavior of the Continue button, if any mandatory data is not entered in the Recipient page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified  the behavior of the Continue button, if any mandatory data is not entered in the Recipient page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify the behavior of the Continue button, if any mandatory data is not entered in the Recipient page is failed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName+"RecipientAddress", objDetail);
				   System.err.println("Verify the behavior of the Continue button, if any mandatory data is not entered in the Recipient page is failed ");
			}

			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

}
