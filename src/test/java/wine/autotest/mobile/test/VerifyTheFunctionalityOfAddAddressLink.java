package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheFunctionalityOfAddAddressLink extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyTheFunctionalityOfAddAddressLink()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String verifyTheFunctionalityOfAddAddressLink() {
	String objStatus=null;

	   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
	   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method checkoutProcess started here ...");		
			UIFoundation.waitFor(4L);
			

            if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}           
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnRecipientChangeAddress));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientaddressFormHeader))
			{
				  objStatus+=true;
			      String objDetail="Verified the functionality of 'Add Address' link";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Verify the functionality of 'Add Address' link test case is executed successfully");   
			}else{
				objStatus+=false;
				String objDetail="Verify the functionality of 'Add Address' link test case is failed";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the functionality of 'Add Address' link test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the functionality of 'Add Address' link test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyTheFunctionalityOfAddAddressLink "
					+ e);
			objStatus+=false;
			String objDetail="Order number is null.Order not placed successfully";			
			 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			return "Fail";
		}
	}

}
