package wine.autotest.mobile.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyPromoBarIsDisplayedInAccountPages extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyPromoBarIsDisplayedInAccountPages()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String verifyPromoBarIsDisplayedInAccountPages()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_VerifyProMoBarDis_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method login started here ...");

			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccSignIn));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(3L);			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.lnkAccountInfo));
			UIFoundation.waitFor(2L);
			
			if(UIFoundation.isDisplayed(ThankYouPage.txtAccountInfoPromoBar))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is displayed on the main User Account page;";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Promo bar is displayed on the main User Account page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not displayed on the main User Account page";
			     
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"AccountInfo", objDetail);
				   System.err.println("Promo bar is not displayed on the main User Account page");
			}
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage.txtOrdersHistoryNav);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtOrdersHistoryNav));
			UIFoundation.waitFor(5L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtAccountInfoPromoBar) && UIFoundation.isDisplayed(ThankYouPage.txtOrderHistoryPage))
					{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Orders page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Promo bar is present on the Orders page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Orders page";			       
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"orderPage", objDetail);
			     
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtAddressBookNav));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtAccountInfoPromoBar) && UIFoundation.isDisplayed(ThankYouPage.txtAddressBookHeader))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Addresses page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Promo bar is present on the Addresses page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Addresses page";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"AddressPage", objDetail);
				   System.err.println("Promo bar is not present on the Addresses page");
			}
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtPaymentMethodsNav));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtAccountInfoPromoBar) && UIFoundation.isDisplayed(ThankYouPage.txtPaymentMethodsHeader))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Payments page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Promo bar is present on the Payments page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Payments page";			     
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentPage", objDetail);			       
				
			}
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtEmailPreferencesNav));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtAccountInfoPromoBar) && UIFoundation.isDisplayed(ThankYouPage.txtEmailPreferenceHeader))
			{
				  objStatus+=true;
			      String objDetail="Promo bar is present on the Email Preferences page";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Promo bar is present on the Email Preferences page");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Promo bar is not present on the Email Preferences page";
			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"EmailPreference", objDetail);				 
				   System.err.println("Promo bar is not present on the Email Preferences page");
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println(" Verify the promo bar is displayed in account pages test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println(" Verify the promo bar is displayed in account pages test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifyPromoBarIsDisplayedInAccountPages "+ e);
			return "Fail";
			
		}
	}
}

