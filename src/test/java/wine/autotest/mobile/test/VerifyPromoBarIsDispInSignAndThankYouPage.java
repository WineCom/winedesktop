package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyPromoBarIsDispInSignAndThankYouPage extends Mobile {


	/***************************************************************************
	 * Method Name			: VerifyPromoBarIsDispInSignAndThankYouPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: TM-4188
	 ****************************************************************************
	 */

	public static String promoBarInSignPageandCreateAct() {

		String objStatus = null;
		String screenshotName = "promoBarInSignPage.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("Promo Bar In Sign Page method started here......");
			driver.get("https://qwww.wine.com/auth/signin");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(LoginPage.SignInButton) && (UIFoundation.isDisplayed(ThankYouPage.txtPromoBarDisp))){
				String promoBarDisptext =UIFoundation.getText(ThankYouPage.txtPromoBarDisp);
				System.out.println("promo bar is displayed in sign in  page "+ promoBarDisptext);
				objStatus += true;
				String objDetail = "promo bar is displayed in sign in  page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "promo bar is not displayed in sign in  page";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
			if(UIFoundation.isDisplayed(LoginPage.CreateAccountButton) && (UIFoundation.isDisplayed(ThankYouPage.txtPromoBarDisp))){
				String promoBarDisptext =UIFoundation.getText(ThankYouPage.txtPromoBarDisp);
				System.out.println("promo bar is displayed in Create account  page "+ promoBarDisptext);
				objStatus += true;
				String objDetail = "promo bar is displayed in sign in  page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "promo bar is not displayed in Create account  page";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnSignInLnkR));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);

			log.info("Promo Bar In Sign Page method started here......");

			if (objStatus.contains("false")) {

				System.out.println("Promo Bar In Sign Page and create account  test case is failed");

				return "Fail";
			} else {
				System.out.println("Promo Bar In Sign Page and create account  test casee test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}
	/***************************************************************************
	 * Method Name			: promobarDispInThankYouPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-4188
	 ****************************************************************************
	 */
	public static String promobarDispInThankYouPage() {
		String expected = null;
		String actual = null;
		String objStatus = null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method promo bar Disp In ThankYouPage started here ...");			
			expected = verifyexpectedresult.placeOrderConfirmation;	
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}		
			UIFoundation.waitFor(10L);
			objStatus += String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(2L);
			if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));
				//objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "paywithcardR"));
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.txtNameOnCard)) {
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
				}
			}
			if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(1L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal = UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling = UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total = UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax = UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(6L);
			if(UIFoundation.isDisplayed(ThankYouPage.lnkOrderNumber) && (UIFoundation.isDisplayed(ThankYouPage.txtPromoBarDisp))){
				String promoBarDisptext = UIFoundation.getText(ThankYouPage.txtPromoBarDisp);
				System.out.println("promo bar is displayed in Thank you  page "+ promoBarDisptext);
				objStatus += true;
				String objDetail = "promo bar is displayed in Thank you page successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "promo bar is not displayed in Thank you page";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method promo bar Disp In ThankYouPage started here ...");
			if (objStatus.contains("false")) {
				System.out.println("Promo Bar In Sign Page and Thank You test case is failed");
				return "Fail";
			} else {
				System.out.println("Promo Bar In Sign Page and Thank You test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method getOrderDetails " + e);
			objStatus += false;
			return "Fail";
		}
	}

}
