package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection extends Mobile {

	/***************************************************************************
	 * Method Name : verifyGftCrdAccAndBalDispInPaymntOpt() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh
	 * Purpose     : Verifies gift card applied is displayed in payment option
	 ****************************************************************************
	 */

	public static String verifyZeroBalGftCrdIsRemovedInPymntOpt() {

		String objStatus=null;
		String subTotal = null;
		String shippingAndHandling = null;
		String total = null;
		String salesTax = null;
		String orderNum = null;
		String screenshotName = "Scenarios_verifyGftCrdAccAndBalDispInPaymntOpt_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the verifyGiftCardAppliedSuccessMsg method strated here");
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(3L);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("DOB format is in'MM / DD / YYYY'.", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="DOB is not in format 'MM / DD / YYYY'.";			
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"dobFormat", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(6L);			
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.webDriverWaitForElement(CartPage.txtGiftCertificateSuccessMsg, "element", "", 50);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.webDriverWaitForElement(CartPage.txtGiftCertificateSuccessMsg, "element", "", 50);
			}
			if(UIFoundation.isDisplayed(CartPage.txtGiftCrdRemAmtZero)) {
				objStatus+=true;
				String objDetail="Gift card applied and zero remaining is displayed is order summary";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Invalid message for gift code  is displayed");
			}else
			{
				objStatus+=false;
				String objDetail="Gift card applied and zero remaing is not displayed is order summary";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal = UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling = UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total = UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax = UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              " + subTotal);
			System.out.println("Shipping & Handling:   " + shippingAndHandling);
			System.out.println("Sales Tax:             " + salesTax);
			System.out.println("Total:                 " + total);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(20L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnclosePopUp))
			{
				UIFoundation.clickObject(FinalReviewPage.btnclosePopUp);
				UIFoundation.waitFor(5L);             
			} 
			orderNum = UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if (orderNum != "Fail") {
				objStatus += true;
				String objDetail = "Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: " + orderNum);
			} else {
				objStatus += false;
				String objDetail = "Order number is null.Order not placed successfully";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
		//	objStatus+=String.valueOf(UIFoundation.mouseHover(driver, "accountBtn"));
			objStatus+=String.valueOf(UIFoundation.clckObject(ThankYouPage.txtPaymentMethods));
			UIFoundation.waitFor(1L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtPaymentMethodsHeader)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("User is navigated to Credit card info ", "Pass", "");
				System.out.println("Verify user is navigated to Credit card info test case is executed successfully");

			}else{
				objStatus+=false;
				String objDetail="Verify user is navigated to Credit card info  test case is failed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(!UIFoundation.isDisplayed(ThankYouPage.spnGiftCardRemZero)){
				objStatus+=true;
				String objDetail="Gift card with ($0 Remaining) is removed from the Payment methods";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Gift card with ($0 Remaining) is removed from the Payment methods");

			}else{
				objStatus+=false;
				String objDetail="Gift card with ($0 Remaining) is not removed from the Payment methods";
				System.out.println("Gift card with ($0 Remaining) is not removed from the Payment methods");
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);

			}
			log.info("The execution of the method verifyGiftCardAppliedSuccessMsg ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case Failed";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				System.out.println("'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case Failed");
				return "Fail";
			}
			else
			{
				String objDetail="'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case excuted succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("'VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection' Test case excuted succesfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}		
}
