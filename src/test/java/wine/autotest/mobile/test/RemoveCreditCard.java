package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class RemoveCreditCard extends Mobile {
	
	

	/***************************************************************************
	 * Method Name			: removeCreditCard()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String removeCreditCard() {

	String objStatus=null;
	   String screenshotName = "Scenarios__removeCreditCard.jpeg";
				String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
						+ screenshotName;
		try {
			log.info("The execution of the method removeCreditCard started here ...");
			UIFoundation.waitFor(4L);
			

          if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
		//	objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
			UIFoundation.waitFor(5L); 
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
/*			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			 if(!UIFoundation.isSelected(FinalReviewPage.cboSelectPayWithThisCard)){
				  objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboSelectCreditCard));
				  UIFoundation.waitFor(5L);
			  }
			 UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkEditCreditCard);
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkEditCreditCard));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRemoveThisCard));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnYesRemove);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnYesRemove));
			UIFoundation.waitFor(5L);
			String creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentLinksCount);
			int creditCardCntBefore=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			 UIFoundation.waitFor(2L);
			if(!UIFoundation.isSelected(FinalReviewPage.cboSelectPayWithThisCard)){
				  objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.cboSelectCreditCard));
				  UIFoundation.waitFor(5L);
			  }
			 UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkEditCreditCard));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRemoveThisCard));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnYesRemove);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnYesRemove));
			UIFoundation.waitFor(5L);
			creditCardCount=UIFoundation.getText(FinalReviewPage.lnkPaymentLinksCount);
			int creditCardCountAfter=Integer.parseInt(creditCardCount.replaceAll("\\(|\\)", ""));
			if(((creditCardCntBefore-1)==creditCardCountAfter))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("The credit card is removed from the payment list.", "Pass", "");
		
			}else
			{
				objStatus+=false;
				String objDetail="The credit card is not removed from the payment list";				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method removeCreditCard ended here ...");
			if (objStatus.contains("false")) {
			
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method removeCreditCard "
					+ e);
			return "Fail";
		}
	}
}
