package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class AlphabeticalOrderMovingFromSaveForLaterToCart extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: validationForSaveForLater()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String alphabeticalOrderMovingFromSaveForLaterToCart()
	{
		String strStatus=null;
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		ArrayList<String> missingProductName=new ArrayList<String>();
		ArrayList<String> allProductName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		   String screenshotName = "Scenarios_SaveForLater_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			String totalItemsBeforeSaveForLater=UIFoundation.getText(CartPage.btnCartCount);
			System.out.println("Number of Products Added to the CART :"+totalItemsBeforeSaveForLater);
			System.out.println("Number of Products in Saved For Later Section :"+UIFoundation.getText(CartPage.btnSaveForLaterCount));
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			
			boolean saveForLaterTitle=UIFoundation.isDisplayed(ListPage.txtSaveForLaterHeadline);
			boolean noItemsInSaveForLater=UIFoundation.isDisplayed(ListPage.txtNoItemsInSaveForLater);
			boolean saveForLaterAfterCheckout=UIFoundation.isDisplayed(ListPage.txtSaveForLaterAfterCheckout);
			if(saveForLaterTitle==true && noItemsInSaveForLater==true && saveForLaterAfterCheckout==true)
			{
				System.out.println("==========Verify save for later section, if the list empty===============");
				System.out.println("Title 'Saved for Later (0)' is displayed");
				System.out.println("'You have no items saved' text is available");
				System.out.println("Save for later section is available below the Checkout button");
				
				strStatus+=true;
			      String objDetail="Verified  save for later section, if the list empty ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Verified save for later section, if the list empty");
				
			}else{
					strStatus+=false;
			       String objDetail="Verify save for later section, if the list empty is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.out.println("Verify save for later section, if the list empty is failed");
			}
			System.out.println("=======================================================================");
			System.out.println("=======================CART Order Summary Before Moving to Saved for Later =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total price of all items before moving :"+totalPriceBeforeSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Name moving from CART to �Saved For Later�:"+expectedItemsName);
			if(!UIFoundation.getText(ListPage.btnThirdProductSaveFor).contains("Fail"))
			{
				strStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductSaveFor));
				
			}
			if(!UIFoundation.getText(ListPage.btnSecondProductSaveFor).contains("Fail"))
			{
				String saveForLaterLink=String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductSaveFor));
				strStatus+=saveForLaterLink;
				boolean saveForLaterLnk =String.valueOf(saveForLaterLink) != null;
				if(saveForLaterLnk){
						strStatus+=true;
				      String objDetail="Verified the functionality of 'Save for later' link in cart section";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
					System.out.println("Verified the functionality of 'Save for later' link in cart section");
				}else{
						strStatus+=false;
				       String objDetail="Verify the functionality of 'Save for later' link in cart section is failed";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					   System.out.println("Verify the functionality of 'Save for later' link in cart section is failed");
				}
				
			}
			UIFoundation.waitFor(2L);
			String totalItemsAfterSaveForLater=UIFoundation.getText(CartPage.btnCartCount);
			String totalPriceAfterSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Below Products are moved from CART to �Saved For Later� Successfully =================");
			if(!UIFoundation.getText(CartPage.spnFirstProductInSaveFor).contains("Fail"))
			{
				String actualProducts1=UIFoundation.getText(CartPage.spnFirstProductInSaveFor);
				System.out.println("1) "+actualProducts1);
				actualItemsName.add(actualProducts1);
			}
			if(!UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor).contains("Fail"))
			{
				String actualProducts2=UIFoundation.getText(CartPage.spnSecondProdcutInSaveFor);
				System.out.println("2) "+actualProducts2);
				actualItemsName.add(actualProducts2);
			}
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("=======================================================================");
			System.out.println("Product Names in �Saved for Later�: "+actualItemsName);
			System.out.println("=======================Order Summary After Moving =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total price of all items after moving :"+totalPriceAfterSaveForLater);
			System.out.println("Numbers of Products remain in the CART after moving to the �Saved For Later� :"+totalItemsAfterSaveForLater);
			System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.btnSaveForLaterCount));
			System.out.println("=======================================================================");
			if(UIFoundation.isDisplayed(CartPage.btnSaveForLaterCount))
			{
					strStatus+=true;
			      String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.btnSaveForLaterCount);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.btnSaveForLaterCount));
			}else{
					strStatus+=false;
			       String objDetail="Number of Products in save for later section after moving: "+UIFoundation.getText(CartPage.btnSaveForLaterCount);;
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				   System.out.println("Verify save for later section, if the list non empty is failed");
			}
			strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtMoveToCartFirstLink));
			UIFoundation.waitFor(2L);
			strStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtMoveToCartSecondLink));
			UIFoundation.waitFor(2L);
			if(strStatus.contains("false"))
			{
				System.out.println("Product Name "+missingProductName+"is not matched or missing.");
				return "Fail";
				
			}
			else
			{
				System.out.println("Product Names are matched before moving and after moving to the �Saved For Later�");
				return "Pass";
			}
			
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}
}

