package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyErrorMsgForAddingNewCreditCard extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : addNewCreditCard()
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 * TM-2190,2192,686
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
	//		objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.nameRequiredErrorMsg) && UIFoundation.isDisplayed(FinalReviewPage.creditCardRequiredErrorMsg) && UIFoundation.isDisplayed(FinalReviewPage.expirtDateRequiredErrorMsg) && UIFoundation.isDisplayed(FinalReviewPage.cvvRequiredErrorMsg))
			{
				  objStatus+=true;
			      String objDetail="Verified appropriate error messages are displayed in Payment section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified appropriate error messages are displayed in Payment section");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify appropriate error messages are displayed in Payment section  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentSection", objDetail);
				   System.err.println("Verify Verified appropriate error messages are displayed in Payment section is failed ");
			}
			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "inValidCreditCardNum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtPleaseSpeciftCardProvider))
			{
				  objStatus+=true;
			      String objDetail="Verified error message is displayed when invalid credit card values are entered.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified  error message is displayed when invalid credit card values are entered.");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify error message is displayed when invalid credit card values are entered is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"inValidCredit", objDetail);
				   System.err.println("Verify error message is displayed when invalid credit card values are entered is failed ");
			}
			UIFoundation.clearField(FinalReviewPage.txtCardNumber);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			//WebElement ele1=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			//ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(ele.isSelected())
			{
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
		//	if(UIFoundation.isDisplayed(FinalReviewPage. "streetAddressRequired") && UIFoundation.isDisplayed(FinalReviewPage. "cityRequired") && UIFoundation.isDisplayed(FinalReviewPage. "stateAddressRequired") && UIFoundation.isDisplayed(FinalReviewPage. "ZipRequired") && UIFoundation.isDisplayed(FinalReviewPage. "phoneRequired") )
			if(UIFoundation.isDisplayed(FinalReviewPage.StreetAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.CityRequired) && UIFoundation.isDisplayed(FinalReviewPage.StateAddressRequired) && UIFoundation.isDisplayed(FinalReviewPage.ZipRequired))

			{
				  objStatus+=true;
			      String objDetail="Verified appropriate error messages are displayed in Billing address fields in Payment section";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified appropriate error messages are displayed in Billing address fields in Payment section");
			}
			else
			{
				   objStatus+=false;
				   String objDetail="Verify appropriate error messages are displayed in Billing address fields in Payment section is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"BillingAddress", objDetail);
				   System.err.println("Verify Verified appropriate error messages are displayed in Billing address fields in Payment section is failed ");
			}
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				return "Fail";
			} else {
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}


}
