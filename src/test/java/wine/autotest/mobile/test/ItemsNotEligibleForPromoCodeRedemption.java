package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ItemsNotEligibleForPromoCodeRedemption extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: itemsNotEligibleForPromoCodeRedemption()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 * TM-1924
	 ****************************************************************************
	 */
	public static String itemsNotEligibleForPromoCodeRedemption() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		   String screenshotName = "Scenarios__promocodeRedemption.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtSearchProduct, "notEligibleForPromocode"));
			UIFoundation.waitFor(10L);
			UIFoundation.hitEnter(ListPage.btnNotEligiblepromocodeRedemptionProduct);
		//	objStatus+=String.valueOf(UIFoundation.javaSriptClick(ListPage.btnNotEligiblepromocodeRedemptionProduct));
			UIFoundation.waitFor(12L);
			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnFirstProductToCart));
			}

			/*addToCart2 = UIFoundation.getText(driver, "SecondProductToCart");
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(driver, "SecondProductToCart"));
			}*/
			
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lblShowOutOfStock);
			  UIFoundation.scrollUp(driver);
			//UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.txtPromoCode))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtObj_PromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(ListPage.txtMinimumOfferAmount))
			{
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption", "Pass", "");
			}else
			{
				objStatus+=false;
				String objDetail="Verify appropriate error message is displayed ,when none of the items in the cart are eligible for the promo code redemption test case is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}


}
