package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyStewardshipMember extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: TM-622
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
		    objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "Stewardusername"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50);
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.shoppingCartPageTitle;			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
		    	String objDetail="Failed to login";
		    	 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}

	/***************************************************************************
	 * Method Name			: verifyStewardshipMessageNotDisplayed()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-622
	 ****************************************************************************
	 */
	
	
	public static String verifyStewardshipPromoNotDisplayed() {
		String objStatus=null;
		try {
			log.info("The execution of the method verifyStewardshipMessageNotDisplayed started here ...");
			
			 UIFoundation.waitFor(3L);
			 if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}
			 UIFoundation.waitFor(5L);	
			if(!UIFoundation.isDisplayed(CartPage.spnlabstewardShipSection)) {
				objStatus+=true;
				String objDetail = "Stewardship Promo is not dispalyed for existing Stewardship User ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");				
			}else {
				objStatus+=false;
				String objDetail = "Stewardship Promo is dispalyed for existing Stewardship User";	
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				
			}
			System.out.println(objStatus);
			log.info("The execution of the method verifyStewardshipPromoNotDisplayed ended here ...");
			if(objStatus.contains("false"))
			{
				String objDetail = "Verify Stewardship Promo Not dispalyed test case is failed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
				
			}
			else
			{
				String objDetail = "Verify Stewardship message not dispalyed test case is executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
			
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyStewardshipMonthlyWineClub "+ e);
			return "Fail";
		}

	}
	
	/***************************************************************************
	 * Method Name			: VerifyDefaultstewardshipStdShipping()
	 * Created By			: Ramesh S
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: TM-3223
	 ****************************************************************************
	 */
	
	public static String VerifyDefaultstewardshipStdShipping() {
	
	String expected=null;
	String actual=null;
	String objStatus=null;
	String Shipping=null;
	String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("The execution of the method VerifyDefaultstewardshipStdShipping started here ...");			
			expected = verifyexpectedresult.placeOrderConfirmation;	
			UIFoundation.waitFor(4L);
			 if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
				{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
				}else {
					objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
				}			
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			if(UIFoundation.isDisplayed(FinalReviewPage.dwnChangeDeliveryDate))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnChangeDeliveryDate));
			}
			Shipping=UIFoundation.getFirstSelectedValue(FinalReviewPage.dwnSelectShippingMethod);
			if(Shipping.contains("0.00")) {
				objStatus+=true;
				String objDetail = "The Standard Shipping is $0";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else {
				objStatus+=false;
				String objDetail = "The Standard Shipping is NOT $0";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false")) {
				String objDetail = "VerifyDefaultstewardshipStdShippingd in delivery section test case is failed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
			} else {
				String objDetail = "VerifyDefaultstewardshipStdShipping in delivery section test case is executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method VerifyDefaultstewardshipStdShipping "
					+ e);
			return "Fail";
		}
	}

}
