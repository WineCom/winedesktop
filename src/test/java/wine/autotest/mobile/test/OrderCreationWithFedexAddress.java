package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithFedexAddress extends Mobile {
	

	
static boolean isObjectPresent=true;
	

/***************************************************************************
 * Method Name			: shippingDetails()
 * Created By			: Chandrashekhar
 * Reviewed By			: Ramesh.
 * Purpose				:  The purpose of this method is to fill the shipping 
 * 						  address of the customer
 ****************************************************************************
 */

public static String shippingDetails() {
	String objStatus=null;
	 String screenshotName = "Scenarios_fedexPickUpLocation_Screenshot.jpeg";
	 String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
	try {
		UIFoundation.waitFor(2L);
		log.info("The execution of the method shippingDetails started here ...");
		
		if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
		{
		objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
		}else {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
		}
		UIFoundation.waitFor(4L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkShipToFedex));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "fedExInvalidZipcode"));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
		UIFoundation.waitFor(10L);
		if(UIFoundation.isDisplayed(FinalReviewPage.txtfedexLocalPickUpIsNotAvailable))
		{
			 objStatus+=true;
		      String objDetail="Verified the error message is displayed on adding the new FedEx address with invalid zipcode";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			  System.out.println("Verified the error message is displayed on adding the new FedEx address with invalid zipcode");
		}else{
			 	objStatus+=false;
			   String objDetail="Verify the error message is displayed on adding the new FedEx address with invalid zipcode  is failed";
			   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			   System.err.println("Verify the error message is displayed on adding the new FedEx address with invalid zipcode is failed ");
		}
		UIFoundation.clearField(FinalReviewPage.txtFedexZipCode);
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
		UIFoundation.waitFor(1L);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnFedexSearchButton);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtFinalReviewPage));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.chkSelectRecipientAdr, "Clickable", "", 50);
		
			
		objStatus+=String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkSelectRecipientAdr));
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkSelectRecipientAdr));
		UIFoundation.waitFor(1L);
//		objStatus+=String.valueOf(UIFoundation.clickObject(driver,"FedexAddressContinueButton"));
//		UIFoundation.waitFor(5L);	
	//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "VerifyFedexAddress"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtFedexSaveButton));
		UIFoundation.waitFor(4L);
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnFedexContinueButton);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexContinueButton));
		UIFoundation.waitFor(6L);
		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Clickable", "", 50);
		if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
		objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
		UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
		
		}
		log.info("The execution of the method shippingDetails ended here ...");

		if (objStatus.contains("false")) {
			
			return "Fail";
		} else {
			
			return "Pass";
		}
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method shippingDetails "
				+ e);
		return "Fail";
	}

}


/***************************************************************************
 * Method Name			: addNewCreditCard()
 * Created By			: Chandrashekhar
 * Reviewed By			: Ramesh.
 * Purpose				: The purpose of this method is to add the new credit 
 * 						  card details for billing process
 ****************************************************************************
 */
	
	public static String addNewCreditCard() {
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "fedExNameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "fedExCardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			/*objStatus+=String.valueOf(UIFoundation.setObject(driver,"BillingAddrss", "fedExShipName"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"BillingSuite", "fedExAddress"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"BillingCity", "fedExCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(driver,"BillingState", "fedExState"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"obj_BillingZip", "fedExZipcode"));
			objStatus+=String.valueOf(UIFoundation.setObject(driver,"obj_BillingPhone", "PhoneNumber"));*/
		//	if(!UIFoundation.isSelected(driver, "billingAddress"))				
		
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected()) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingAddrss, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnFedexBillingState, "BillingState"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingZip, "BillingZipcode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexBillingPhoneNum, "PhoneNumber"));
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtBirthMonth)){
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBirthYear, "birthYear"));
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			{
				System.out.println("Order Number :"+orderNum);
				UIFoundation.getOrderNumber(orderNum);
		    	objStatus+=true;		
		    	String objDetail="Order number is placed successfully";
		    	ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null and Order cannot placed";
		    	 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			
			}
			log.info("The execution of the method addNewCreditCard ended here ...");	
			if (objStatus.contains("false")) {
				System.out.println("Order creation with Fedex address test case is failed");
				return "Fail";
				
			} else {
				System.out.println("Order creation with Fedex address test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard "
					+ e);
			return "Fail";
		}
	}

}

