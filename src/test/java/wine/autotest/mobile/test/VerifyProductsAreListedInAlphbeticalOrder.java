package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyProductsAreListedInAlphbeticalOrder extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyProductsAreListedAlbhabeticalOrder()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyProductsAreListedAlbhabeticalOrder()
	{
		
		ArrayList<String> expectedItemsName=new ArrayList<String>();
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		boolean isSorted=true;
		String objStatus=null;
		String screenshotName = "Scenarios_Sorting_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
		
			System.out.println("============Below Products are added to the CART ===================================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
				expectedItemsName.add(products1);
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);
				expectedItemsName.add(products2);
			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);
				expectedItemsName.add(products3);
			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);
				expectedItemsName.add(products4);
			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);
				expectedItemsName.add(products5);
			}
			
			
			 for(int i = 0; i < expectedItemsName.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if((expectedItemsName.get(i).compareToIgnoreCase(expectedItemsName.get(i + 1)) > 0) && (expectedItemsName.get(i).compareToIgnoreCase(expectedItemsName.get(i + 1))<0)) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	System.out.println("Verify products are listed in Alphabatical order test case executed successfully");
			    	objStatus+=true;
			        String objDetail="Products are listed in Alphabatical order";
			        ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    }
			    else
			    {
			    	System.err.println("Products are not listed in Alphabatical order");
			        objStatus+=false;
			        String objDetail="Products are not listed in Alphabatical order";
			        UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					
			    }
			return "Pass";
			
		
        } catch (Exception e) {
            
        	e.printStackTrace();
        	return "Fail";
        }
	}

}
