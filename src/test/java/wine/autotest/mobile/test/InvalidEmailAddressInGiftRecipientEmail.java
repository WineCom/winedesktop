package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class InvalidEmailAddressInGiftRecipientEmail extends Mobile {

	
	/***************************************************************************
	 * Method Name : editRecipientPlaceORder() 
	 * Created By  : chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String editRecipientPlaceORder() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;

		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			

         if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
         objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
                       UIFoundation.waitFor(2L);
			UIFoundation.waitFor(8L);  
			String expectedEmailErrorMsg = verifyexpectedresult.InvalidEmailErrorMsgInGiftRecipientEmail;
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEid));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientShipToaddress));
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.radRecipientGiftCheckbox);
			UIFoundation.waitFor(2L); 
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtRecipientEmail))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtRecipientGiftCheckboxInValidEmail));
				UIFoundation.clearField(FinalReviewPage.txtRecipientEmailField);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientEmailField, "InValiRecipientEmail"));
							
			}else
			{
				UIFoundation.clearField(FinalReviewPage.txtRecipientEmailField);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtRecipientEmailField, "InValiRecipientEmail"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnReciptContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnReciptContinue);
			String actualInValidEmailErrorMsg=UIFoundation.getText(FinalReviewPage.txtInvalidEmailAddressInGiftRecipientPage);
			if (expectedEmailErrorMsg.equalsIgnoreCase(actualInValidEmailErrorMsg)) {
				
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Verified error message for invalid email format", "Pass", "");
		
				
			} else {
			
				objStatus+=false;
				String objDetail="Verified error message for invalid email format is failed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName+"InvalidEmailInGiftRecipient", objDetail);
				
			}
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}


}
