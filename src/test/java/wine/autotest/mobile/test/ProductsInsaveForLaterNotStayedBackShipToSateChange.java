package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ProductsInsaveForLaterNotStayedBackShipToSateChange extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: productsInsaveForLate()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productsInsaveForLate() {
		String objStatus = null;
		String screenshotName = "Scenarios_ OutBound_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		String state=null;
		try {
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgWineLogo));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[3]"));
			org.openqa.selenium.support.ui.Select sel=new org.openqa.selenium.support.ui.Select(ele);
			sel.selectByVisibleText("DE");
		//	sel.selectByVisibleText("DC");
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnContinueShipToDE));
			UIFoundation.waitFor(10L);			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.btnYourCartIsEmpty)){
				objStatus+=true;
			      String objDetail="Your cart is empty text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Your cart is empty text is displayed");
				  if(UIFoundation.isDisplayed(ListPage.btnCurrentlyUnavailable)){
						objStatus+=true;
					      String objDetail1="Cart page is be displayed and the products are  moved to Save for Later and marked as currently unavailable";
					      ReportUtil.addTestStepsDetails(objDetail1, "Pass", "");
						  System.out.println("Cart page is be displayed and the products are  moved to Save for Later and marked as currently unavailable");
						
					}else
					{
						objStatus+=false;
						String objDetail1="Cart page is be not displayed and the products are not marked as currently unavailable";
						UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					}				
			}else
			{
				objStatus+=false;
				String objDetail="Your cart is empty text is not displayed";			
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgWineLogo));
			UIFoundation.waitFor(3L);
			WebElement ele1=driver.findElement(By.xpath("(//select[@class='state_select state_select-promptsCartTransfer'])[3]"));
			org.openqa.selenium.support.ui.Select sel1=new org.openqa.selenium.support.ui.Select(ele1);
			sel1.selectByVisibleText("CA");
			UIFoundation.waitFor(12L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(ListPage.btnYourCartIsEmpty)){
				objStatus+=true;
			      String objDetail="Your cart is empty text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Your cart is empty text is displayed");
				  if(UIFoundation.isDisplayed(ListPage.btnMoveToCart)){
						objStatus+=true;
					      String objDetail1="Products are displayed under save for later with 'Move to cart' button";
					      ReportUtil.addTestStepsDetails(objDetail1, "Pass", "");
						  System.out.println("Products are displayed under save for later with 'Move to cart' button");
						
					}else
					{
						objStatus+=false;
						String objDetail1="Products are not displayed under save for later with with 'Move to cart' button";			
						UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
					}
				
			}else
			{
				objStatus+=false;
				String objDetail="Your cart is empty text is not displayed";
		    	  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
	
			
			
		} catch (Exception e) {
			String objDetail="OutBound Gregg override link failed: ";
			  UIFoundation.captureScreenShot( screenshotpath+screenshotName, "");
			return "Fail";
		}
	}


}
