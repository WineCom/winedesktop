package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyMultipleGiftCardCanBeReedemed extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar  
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(1L);			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "giftCertificateUn"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";			
		}
	}
	
	/***************************************************************************
	 * Method Name			: addNewCreditCard()
	 * Created By			: Chandrashekhar  
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	
	public static String addNewCreditCard() {
		String objStatus = null;
		   String screenshotName = "Scenarios_addNewCreditCard_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
		
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(5L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(1L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Add new credit card is failed");
				return "Fail";
			} else {
				System.out.println("Add new credit card is failed is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyMoreThanTwoGiftCardreedemed()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String verifyMoreThanTwoGiftCardReedemed()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_twoGiftCert_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method enterGiftCertificate started here ...");
			String giftCert1=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert1);
			String giftCert2=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate2", 1, giftCert2);
			String giftCert3=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert3);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate3"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate3"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
			}
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Multiple gift cards are applied succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Multiple gift cards are not applied";
				  UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify more than 2 gift cards can be redeemed test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify  more than 2 gift cards can be redeemed test case executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyOlyTwoGftCrdSelForOrder()
	 * Created By			: Chandrashekhar  
	 * Reviewed By			: Ramesh
	 * Purpose				: Checks gift card added is checked for two cards only
	 ****************************************************************************
	 */
	
	public static String verifyOlyTwoGftCrdSelForSingleOrder()
	
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_TwoGiftCert_Selected_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method verifyOlyTwoGftCrdSelForOrder started here ...");
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkBillPaymentEdit));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtChckBoxGiftCard1);
			UIFoundation.waitFor(1L);
			if(UIFoundation.isCheckBoxSelected(FinalReviewPage.txtChckBoxGiftCard1) && UIFoundation.isCheckBoxSelected(FinalReviewPage.txtChckBoxGiftCard2)){
				objStatus+=true;
				String objDetail="Two Gifts are checked for single order";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Two Gifts are not checked for single order";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyOlyTwoGftCrdSelForOrder ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verified only 2 gift cards checked method failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verified only 2 gift cards checked method Passed");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
	
	
	/*****************************************************************************************************
	 * Method Name			: verifyOlyTwoGftCrdSelForOrder()
	 * Created By			: Chandrashekar 
	 * Reviewed By			: Rakesh
	 * Purpose				: Checks gift card added additionally is unchecked
	 ******************************************************************************************************
	 */
	
	
	public static String verifyAdditionalGftCrdIsUnChked()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_GiftCert_UnChecked_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method verifyAdditionalGftCrdIsUnChked started here ...");
			UIFoundation.waitFor(1L);
			if(!UIFoundation.isEnabled(FinalReviewPage.txtChckBoxGiftCard3)) {
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.txtChckBoxGiftCard2));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.txtChckBoxGiftCard3));
				if(!UIFoundation.isEnabled(FinalReviewPage.txtChckBoxGiftCard2)) {
				objStatus+=true;
				String objDetail="Additional gift card is unchecked for single order";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			}
			else{
				objStatus+=false;
				String objDetail="Additional gift card is not unchecked for single order";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyAdditionalGftCrdIsUnChked ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="Customer is not allowed to enter multiple gifts cards and gifts cards are not displayed under payment section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Customer is not allowed to enter multiple gifts cards and gifts cards are not displayed under payment section");
				return "Fail";
			}
			else
			{
				String objDetail="Customer is allowed to enter multiple gifts cards and gifts cards are displayed under payment section";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Customer is allowed to enter multiple gifts cards and gifts cards are displayed under payment section");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
		}
	}	
	
}
