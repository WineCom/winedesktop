package wine.autotest.mobile.test;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;


public class OrderCreationWihExistingAccountBillingAddress extends Mobile {
	

	static boolean isObjectPresent=false;
	

/***************************************************************************
 * Method Name			: shippingDetails()
 * Created By			: Vishwanath Chavan
 * Reviewed By			: Ramesh,KB
 * Purpose				:  The purpose of this method is to fill the shipping 
 * 						  address of the customer
 ****************************************************************************
 */

public static String shippingDetails() {
	String objStatus=null;
	try {
		log.info("The execution of the method shippingDetails started here ...");
		
		if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
		{
		objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
		}else {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
		}
		

		UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
		objStatus += String.valueOf(UIBusinessFlow.recipientEdit());
		
		if (UIFoundation.isDisplayed(FinalReviewPage.radShiptoaddressOne)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShiptoaddressOne));
			UIFoundation.waitFor(10L);
		}
		UIFoundation.waitFor(10L);
		if (UIFoundation.isDisplayed(FinalReviewPage.btnContinuetostate)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnContinuetostate));
			UIFoundation.waitFor(10L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)) {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(10L);
		}
	
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			UIFoundation.waitFor(10L);
		}
		if (UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)) {
			/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkPaymentEdit);	
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkPaymentEdit));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVVR, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnpaywithThisCardSave));*/
			
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
		}
		if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard)) {
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
		    UIFoundation.clickObject(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			}
		if(UIFoundation.isElementDisplayed(FinalReviewPage.btnPaymentContinue)) {
		UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
		objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		UIFoundation.waitFor(1L);
		}
		
	//	objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
	
    /*                  UIFoundation.waitFor(2L);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(NewAccountBillingAddressPage.radShippingTyp));
		UIFoundation.waitFor(1L);
		objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage.txtFirstName, "firstName"));
		objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage.txtLastName, "lastName"));
		objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage.txtStreetAddress, "Address1"));
		objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage.txtCity, "City"));
		objStatus+=String.valueOf(UIFoundation.SelectObject(NewAccountBillingAddressPage.dwnReceiveState,"State"));
		UIFoundation.javaScriptClick(NewAccountBillingAddressPage. dwnReceiveState);
		
		objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage.txtZipCode, "ZipCode"));
		objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage.txtPhoneNum, "PhoneNumber"));
		UIFoundation.waitFor(1L);
		UIFoundation.scrollDownOrUpToParticularElement(NewAccountBillingAddressPage.btnShipContinue);
		objStatus+=String.valueOf(UIFoundation.clickObject(NewAccountBillingAddressPage.btnShipContinue));
		UIFoundation.webDriverWaitForElement(NewAccountBillingAddressPage. btnShipContinue, "Invisible", "", 50);
		objStatus+=String.valueOf(UIFoundation.javaScriptClick(NewAccountBillingAddressPage. "SuggestedAddress"));
		UIFoundation.waitFor(3L);
		if(UIFoundation.isDisplayed(NewAccountBillingAddressPage. "VerifyContinueButton")){
		UIFoundation.scrollDownOrUpToParticularElement(NewAccountBillingAddressPage. "VerifyContinueButton");
		objStatus+=String.valueOf(UIFoundation.clickObject(NewAccountBillingAddressPage. "VerifyContinueButton"));
		UIFoundation.waitFor(10L);
		}
		
		if(UIFoundation.isDisplayed(NewAccountBillingAddressPage.btnVerifyContinueButtonShipRecpt))
		{
			UIFoundation.scrollDownOrUpToParticularElement(NewAccountBillingAddressPage.btnVerifyContinueButtonShipRecpt);
			objStatus += String.valueOf(UIFoundation.clickObject(NewAccountBillingAddressPage. btnVerifyContinueButtonShipRecpt));
			UIFoundation.webDriverWaitForElement(NewAccountBillingAddressPage.btnVerifyContinueButtonShipRecpt, "Invisible", "", 50);
		}
		if(UIFoundation.isDisplayed(NewAccountBillingAddressPage.btnDeliveryContinue)){
		UIFoundation.scrollDownOrUpToParticularElement(NewAccountBillingAddressPage.btnDeliveryContinue);
		objStatus+=String.valueOf(UIFoundation.clickObject(NewAccountBillingAddressPage.btnDeliveryContinue));
		UIFoundation.webDriverWaitForElement(NewAccountBillingAddressPage.btnDeliveryContinue, "Invisible", "", 50);
		}*/
		log.info("The execution of the method shippingDetails ended here ...");

		if (objStatus.contains("false")) {
			
			return "Fail";
		} else {
			
			return "Pass";
		}
	} catch (Exception e) {
		log.error("there is an exception arised during the execution of the method shippingDetails "
				+ e);
		return "Fail";
	}

}
/***************************************************************************
 * Method Name			: addNewCreditCard()
 * Created By			: Vishwanath Chavan
 * Reviewed By			: Ramesh,KB
 * Purpose				: The purpose of this method is to add the new credit 
 * 						  card details for billing process
 ****************************************************************************
 */
	public static String addNewCreditCard() {
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		/*String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\Screenshots\\"
				+ screenshotName;*/
			
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			
			UIFoundation.waitFor(5L);
			if (UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment)) {
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(5L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage. lnkAddNewCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShipping);
		    UIFoundation.waitFor(3L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingAddress, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingSuite, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingCity, "BillingCity"));
			objStatus+=String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillinState, "BillingState"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingZip, "BillingZipcode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNewBillingPhone, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.txtSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.txtShippingHandaling);
			total=UIFoundation.getText(FinalReviewPage.txtTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage. txtOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(NewAccountBillingAddressPage. "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(NewAccountBillingAddressPage. "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(NewAccountBillingAddressPage. "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(NewAccountBillingAddressPage. "PaymentSaveButton"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(NewAccountBillingAddressPage. "PaymentContinue"));
			UIFoundation.waitFor(5L);*/
		//	UIFoundation.waitFor(NewAccountBillingAddressPage. By.xpath("(//a[text()='Place Order']"), "element", "",10);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrderButton);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrderButton));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrderButton, "Invisible", "", 50);
		//	UIFoundation.waitFor(NewAccountBillingAddressPage. By.xpath("(//button[@class='mainNavBtn']"), "element", "",10);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
		//	orderNum=UIFoundation.getText(NewAccountBillingAddressPage."OrderNum");
			if(orderNum!="Fail")
		   {
		      objStatus+=true;
		      String objDetail="Order number is placed successfully";
		      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
		      System.out.println("Order number is placed successfully: "+orderNum);   
		   }else
		   {
		       objStatus+=false;
		       String objDetail="Order number is null and cannot placed successfully";
		  //     UIFoundation.captureScreenShot(NewAccountBillingAddressPage. screenshotpath, objDetail);
		   }
			log.info("The execution of the method addNewCreditCard ended here ...");	
			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard "
					+ e);
			return "Fail";
		}
		
	}
}
