package wine.autotest.mobile.test;


import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class ValidateCompassEnrollmentFlowforBothRedandWhiteWine extends Mobile {
	

	
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWine() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar K B
	 * Purpose     : The purpose of this method is to Verify Likeed and Disliked Red/White Wine in Anything else section' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String enrollForPickedUpWine() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRedWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypWhiteNxt));
			UIFoundation.waitFor(4L);
		
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
				objStatus+=true;
				String objDetail="User is navigated to adventurous quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to adventurous quiz next page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));
			UIFoundation.waitFor(1L);
			
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedWhiteQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				String objDetail="Enrollment flow for red wine is completed successfully";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Enrollment flow for red wine is not completed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

	
	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar K B
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyLocalPickUpForPickedUpWine() {
			
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);			
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocarion));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage. txtFedexSaveButton));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
				//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkAcceptTermsAndConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
				UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
		/***********************************************************************************************************
		 * Method Name : EditPaymentMethodinPickedSettings() 
		 * Created By  : Ramesh S
		 * Reviewed By :  Chandrashekhar K B
		 * Purpose     : The purpose of this method is to Verify the Change Payment in Picked Setting' 
		 * 
		 ************************************************************************************************************
		 */
		
		public static String EditPaymentMethodinPickedSettings() {
			String objStatus = null;
			String screenshotName = "Scenarios_EditPaymentMethodinPickedSettings_Screenshot.jpeg";
			String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
			try {
				log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
				String strExistingPymtName = UIFoundation.getText(PickedPage.spnExistingPymtName);				
				String VrfyPymtName = verifyexpectedresult.PaymentName;	
				System.out.println("strExistingPymtName :"+strExistingPymtName);
				System.out.println("VrfyPymtName :"+VrfyPymtName);
				UIFoundation.waitFor(2L);
				if(strExistingPymtName.contains(VrfyPymtName)){
					objStatus+=true;
					String objDetail="The Card Added in Enrollment Page is displayed in Picked Setting Page : "+strExistingPymtName;
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="The Card Added in Enrollment Page is not displayed Picked Setting Page.";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSettingChangePymt));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetNameonCard, "PickedSetNameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetBillAddressCardNumber, "CardnumM1"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetExpMonth,"MonthM1"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetExpYear,"YearM1"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetExpCvv, "CardCvidM1"));
				UIFoundation.waitFor(3L);
				
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetBillAddressName, "Address1"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetBillAddressCity, "BillingCity"));
				objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.txtPickedSetBillAddressState, "BillingState"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetBillAddressZipCode, "BillingZipcode"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtPickedSetBillAddressPhone, "PhoneNumber"));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPickedSetPymtSave));
				
				String strChangedPymtName = UIFoundation.getText(PickedPage.spnExistingPymtName);
				System.out.println("strChangedPymtName :"+strChangedPymtName);				
				String VrfyChangedPymtName = verifyexpectedresult.ChangedPaymentName;
				System.out.println("VrfyChangedPymtName :"+VrfyChangedPymtName);
				UIFoundation.waitFor(1L);
				if(strChangedPymtName.contains(VrfyChangedPymtName)){
					objStatus+=true;
					String objDetail="The Card Added in Edit Payment Page is displayed Picked Setting Page : "+strChangedPymtName;
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="The Card Added in Edit Payment Page is not displayed Picked Setting Page.";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				
				if (objStatus.contains("false"))
				{
					String objDetail="Verification of Change Payment in Picked Settings page is not completed successfully";
					System.out.println(objDetail);
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return "Fail";
				}
				else
				{
					String objDetail="Verification of Edit Subscription in Picked Settings page is completed successfully";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
		/***********************************************************************************************************
		 * Method Name : EditSubscriptioninPickedSettings() 
		 * Created By  : Ramesh S
		 * Reviewed By :  Chandrashekhar K B
		 * Purpose     : The purpose of this method is to Verify the edit option for subscription Setting' 
		 * 
		 ************************************************************************************************************
		 */
		
		public static String EditSubscriptioninPickedSettings(WebDriver driver) {
			String objStatus = null;
			String screenshotName = "Scenarios_EditSubscriptioninPickedSettings_Screenshot.jpeg";
			String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
			try {
				log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.imgPickedLogo));
				UIFoundation.waitFor(10L);
				if(UIFoundation.isDisplayed(PickedPage.spnPickedSettingHeader)){
					objStatus+=true;
					String objDetail="User is successfully Navigated to Picked Settings Page";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="User is not successfully Navigated to Picked Settings Page";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkSubSettingsEdit));
				if(UIFoundation.isDisplayed(PickedPage.spnSubSettingEditText) && (UIFoundation.isDisplayed(PickedPage.btnSubSettSaveChange))){
					objStatus+=true;
					String objDetail="Subscription settings is editable in Picked Settings Page";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="Subscription settings is not-editable in Picked Settings Page";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				
				
				
				if (objStatus.contains("false"))
				{
					String objDetail="Verification of Edit Subscription in Picked Settings page is not completed successfully";
					System.out.println(objDetail);
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return "Fail";
				}
				else
				{
					String objDetail="Verification of Edit Subscription in Picked Settings page is completed successfully";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
		/***********************************************************************************************************
		 * Method Name : ChangedDeliveryDateinPickedSettings() 
		 * Created By  : Ramesh S
		 * Reviewed By :  Chandrashekhar K B
		 * Purpose     : The purpose of this method is to Verify the Delivery date changes in picked settings' 
		 * 
		 ************************************************************************************************************
		 */
		
		public static String ChangedDeliveryDateinPickedSettings() {
			String objStatus = null;
			String screenshotName = "Scenarios_ChangedDeliveryDateinPickedSettings_Screenshot.jpeg";
			String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
			try {
				log.info("Execution of the method ChangedDeliveryDateinPickedSettings started here .........");
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.imgPickedLogo));
				UIFoundation.waitFor(10L);
				String ExistingDate = UIFoundation.getText(PickedPage.spnPickedSetPickedDate);
				String[] SelDate = ExistingDate.split("t");
				System.out.println("SelDate : "+SelDate[0]);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetChangeDate));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.radDelayDateOneWeek));
				String DelayDate = UIFoundation.getText(PickedPage.radDelayDateOneWeek);
				String[] picDate = DelayDate.split("/");
				System.out.println("picDate : "+picDate[1]);
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnDelaydateConfirm));
				UIFoundation.waitFor(1L);
				String ChangeDate = UIFoundation.getText(PickedPage.spnPickedSetPickedDate);
				String[] ChangedDate = ChangeDate.split("t");
				System.out.println("ChangedDate : "+ChangedDate[0]);
				if(!(ChangedDate[0].trim()).contains(SelDate[0].trim())){
					objStatus+=true;
					String objDetail="The Existing date and Changed delay are not matching in Picked Setting page.";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="The Existing date and Changed delay are matching in Picked Setting page.";
					System.out.println(objDetail);
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				
				if((ChangedDate[0].trim()).contains(picDate[1].trim())){
					objStatus+=true;
					String objDetail="The Changed Delay date is displayed in Picked Setting page.";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="The Changed Delay date is Not displayed in Picked Setting page.";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					System.out.println(objDetail);
				}				
				
				if (objStatus.contains("false"))
				{
					String objDetail="Verification of Change Delay date in Picked Settings page is not completed successfully";
					System.out.println(objDetail);
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return "Fail";
				}
				else
				{
					String objDetail="Verification of Change Delay date in Picked Settings page is completed successfully";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
		/***********************************************************************************************************
		 * Method Name : ChangeRecipientAddressinPickedSettings() 
		 * Created By  : Ramesh S
		 * Reviewed By :  Chandrashekhar K B
		 * Purpose     : The purpose of this method is to Verify the Recipient address change in picked settings' 
		 * 
		 ************************************************************************************************************
		 */
		
		public static String ChangeRecipientAddressinPickedSettings() {
			String objStatus = null;
			String screenshotName = "Scenarios_ChangeRecipientAddressinPickedSettings_Screenshot.jpeg";
			String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
			try {
				log.info("Execution of the method ChangeRecipientAddressinPickedSettings started here .........");
				
				String strExistingRecpAddr = UIFoundation.getText(PickedPage.spnRecipientNamePickedSettings);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkChangedRecipientAddress));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickedSetZipCode, "ZipCodePicked"));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnLocalPickupSearch));
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnLocalPickupAddressselect));
				UIFoundation.waitFor(1L);
				if(UIFoundation.isDisplayed(PickedPage.txtpickupInfoFirstName)) {
					UIFoundation.waitFor(1L);
					
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickupInfoFirstName, "firstNamepickedSet"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickupinfoLastName, "lastNamepickedSet"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickupinfoAddress1, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickupinfoCity, "City"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnpickupinfoState, "State"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickupinfoZip, "ZipCodePicked"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtpickupinfoPhone, "PhoneNumber"));
					objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpickupinfoSave));
					UIFoundation.waitFor(1L);
				}
				String strChangedRecpAddr = UIFoundation.getText(PickedPage.spnRecipientNamePickedSettings);
				System.out.println("strExistingRecpAddr : "+strExistingRecpAddr);
				System.out.println("strChangedRecpAddr : "+strChangedRecpAddr);
				if(!(strExistingRecpAddr.contains(strChangedRecpAddr))){
					objStatus+=true;
					String objDetail="The user is able to change the recipient address in Picked Setting page.";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="The user is Not able to change the recipient address in Picked Setting page.";
					System.out.println(objDetail);
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				
				
				if (objStatus.contains("false"))
				{
					String objDetail="Verification of Change Recipient Address in Picked Settings page is not completed successfully";
					System.out.println(objDetail);
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return "Fail";
				}
				else
				{
					String objDetail="Verification of Change Recipient Address in Picked Settings page is completed successfully";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
		/***********************************************************************************************************
		 * Method Name : ValidateContentforYourSubscriptioninPickedSettings() 
		 * Created By  : Ramesh S
		 * Reviewed By :  Chandrashekhar K B
		 * Purpose     : The purpose of this method is to Verify the Your Subscription Content in picked settings' 
		 * 
		 ************************************************************************************************************
		 */
		
		public static String ValidateContentforYourSubscriptioninPickedSettings() {
			String objStatus = null;
			String screenshotName = "Scenarios_ValidateContentforYourSubscriptioninPickedSettings_Screenshot.jpeg";
			String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
			try {
				log.info("Execution of the method ValidateContentforYourSubscriptioninPickedSettings started here .........");
				
				String strYourSubTitle = UIFoundation.getText(PickedPage.spnPickedSetYourSubscription);
				ReportUtil.addTestStepsDetails("Your Subscription Title : "+strYourSubTitle, "Pass", "");	
				String strTargetPrice = UIFoundation.getText(PickedPage.spnPickedsetTargetPrice);
				ReportUtil.addTestStepsDetails("Target Price : "+strTargetPrice, "Pass", "");
				String strMinPrice = UIFoundation.getText(PickedPage.lnkPickedsetMinPrice);
				ReportUtil.addTestStepsDetails("Minimum Price : "+strMinPrice, "Pass", "");
				String strMaxPrice = UIFoundation.getText(PickedPage.lnkPickedsetMaxPrice);
				ReportUtil.addTestStepsDetails("Maximum Price : "+strMaxPrice, "Pass", "");
				String strRedBottleQty = UIFoundation.getText(PickedPage.spnPickedsetRedQty);
				ReportUtil.addTestStepsDetails("Red Bottle Quantity : "+strRedBottleQty, "Pass", "");
				String strRedBottlePrice = UIFoundation.getText(PickedPage.spnPickedsetRedPrice);
				ReportUtil.addTestStepsDetails("Red Bottle Price : "+strRedBottlePrice, "Pass", "");
				String strWhiteBottleQty = UIFoundation.getText(PickedPage.spnPickedsetWhiteQty);
				ReportUtil.addTestStepsDetails("White Bottle Quantity : "+strWhiteBottleQty, "Pass", "");
				String strWhiteBottlePrice = UIFoundation.getText(PickedPage.spnPickedsetWhitePrice);
				ReportUtil.addTestStepsDetails("White Bottle Price  : "+strWhiteBottlePrice, "Pass", "");
				String strFreqDesc = UIFoundation.getText(PickedPage.spnPickedsetFreqDesc);
				ReportUtil.addTestStepsDetails("Frequency Description : "+strFreqDesc, "Pass", "");
				String strDiscLevelDesc = UIFoundation.getText(PickedPage.spnPickedsetDiscDesc);
				ReportUtil.addTestStepsDetails("Discovery Level : "+strDiscLevelDesc, "Pass", "");
				String strFreeShipDesc = UIFoundation.getText(PickedPage.spnPickedsetFreeShip);
				ReportUtil.addTestStepsDetails("Free Shipping : "+strFreeShipDesc, "Pass", "");
				String strSatisfactGuatantDesc = UIFoundation.getText(PickedPage.spnPickedsetSatisfactGuarant);
				ReportUtil.addTestStepsDetails("Satisfaction guaranteed : "+strSatisfactGuatantDesc, "Pass", "");
				if(strYourSubTitle!=null){
					objStatus+=true;
					String objDetail="Your Subscription Content Found : "+strYourSubTitle;
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="Your Subscription Content Not Found : "+strYourSubTitle;
					System.out.println(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
								
				if (objStatus.contains("false"))
				{
					String objDetail="Verification of content for Your Subscription in Picked Settings page is not completed successfully";
					System.out.println(objDetail);
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return "Fail";
				}
				else
				{
					String objDetail="Verification of content for Your Subscription in Picked Settings page is completed successfully";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}	
}
