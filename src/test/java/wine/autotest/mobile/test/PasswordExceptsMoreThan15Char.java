package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class PasswordExceptsMoreThan15Char extends Mobile {



	static String NewUseremailR = null;

	/*******************************************************************
	 * Method name: VerifyPasswordExceptsMoreThan15Char
	 *  TM-4071 :
	 ******************************************************************
	 */

	public static String verifyPasswordExceptsMoreThan15Char() {

		String objStatus=null;
		//String NewUseremailR=null;
		String screenshotName="PasswordExceptsMoreThan15Char";
		String screenshotpath=System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("account creation method started here:");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));

			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.JoinNowButton));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email,"email"));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.Password, "grt15char"));
			UIFoundation.waitFor(2L);
			NewUseremailR=UIFoundation.getAttribute(LoginPage.Email);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(7L);
			log.info("account creation method ended here:");
			if(objStatus.contains("false")){
				String objDetail="User profile creation  test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			} else {
				String objDetail="User profile creation  test case is executed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method create account "
					+ e);
			return "Fail";
		}
	}

	public static String loginusingSamepassword() {

		String objStatus=null;
		String screenshotName="LoginusingSamepassword";
		String screenshotpath=System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("login method started here:");	
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.SignoutLink));
			driver.findElement(By.xpath("//input[@name='email']")).sendKeys(NewUseremailR);
			//objStatus += String.valueOf(UIFoundation.setObject(LoginPage. "LoginEmail", NewUseremailR));
			objStatus += String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "grt15char"));
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccountTab));
			objStatus += String.valueOf(UIBusinessFlow.isObjectExistForSignIn());
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				String objDetail="User able to login  successfully";
				ReportUtil.addTestStepsDetails(objDetail, "fail", "");				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="user is able to login";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}
}
