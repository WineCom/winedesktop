package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.verifyexpectedresult;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class RemoveAllProdcutsFromCartContinueShopingWithoutSignIn extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	static int countProducts=0;
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		String addToCart6 = null;
		String addToCart7 = null;

		boolean isElementPresent=false;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(driver, "FirstProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
				countProducts++;
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
		//		UIFoundation.scrollDownOrUpToParticularElement(driver, "SecondProductToCart");
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
				countProducts++;
			}

			addToCart3 = UIFoundation.getText(ListPage.btnThirdProductToCart);
			if (addToCart3.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(driver, "ThirdProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnThirdProductToCart));
				countProducts++;
			}

			addToCart4 = UIFoundation.getText(ListPage.btnFourthProductToCart);
			if (addToCart4.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(driver, "FourthProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFourthProductToCart));
				countProducts++;
			}

			addToCart5 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart5.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(driver, "FifthProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
				countProducts++;
			}
			addToCart6 = UIFoundation.getText(ListPage.btnSixthProductToCart);
			if (addToCart6.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(driver, "FifthProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSixthProductToCart));
				countProducts++;
			}
			addToCart7 = UIFoundation.getText(ListPage.btnSeventhProductToCart);
			if (addToCart7.contains("Add to Cart")) {
				/*UIFoundation.scrollDownOrUpToParticularElement(driver, "FifthProductToCart");
				UIFoundation.waitFor(1L);*/
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSeventhProductToCart));
				countProducts++;
			}

			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkShowOutOfStock);
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			isElementPresent=UIFoundation.isDisplayed(LoginPage.LoginEmail);
			if(isElementPresent)
			{
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "usernameMobile"));
				objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
				UIFoundation.waitFor(3L);
			}
			//objStatus += String.valueOf(UIFoundation.javaScriptClick(driver, "CartBtnCount"));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: removeProductsFromTheCart()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String removeProductsFromTheCart()
	{
		String objStatus=null;
		String expected=null;
		String actual=null;
		
		String screenshotName = "Scenarios_HomeTitle_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			
			for(int i=0;i<=countProducts;i++)
			{
				UIFoundation.clckObject(CartPage.spnRemove);
				UIFoundation.waitFor(2L);
				UIFoundation.clckObject(CartPage.spnObjRemove);
				UIFoundation.waitFor(1L);
			}
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnContinueShopping));
			UIFoundation.waitFor(2L);
					
			expected = verifyexpectedresult.homePageTitle;
			actual=driver.getTitle();
	
			log.info("The execution of the method removeProductsFromTheCart started here ...");
			if(expected.equalsIgnoreCase(actual)){
				  objStatus+=true;
			      String objDetail="Actual and expected title is same";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			    String objDetail="Actual and expected title is not same";
			//    UIFoundation.captureScreenShot(screenshotpath, objDetail);
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}

}