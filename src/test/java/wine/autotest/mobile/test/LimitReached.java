package wine.autotest.mobile.test;


import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class LimitReached extends Mobile {

	
	/***************************************************************************
	 * Method Name			: limitReached()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String limitReached()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios__limitReached.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;		
		
		try
		{
			
			log.info("The execution of the method limitReached started here ...");
			UIFoundation.waitFor(1L);
            objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtSearchBarFormInput));
            objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtsearchProduct, "Wind"));
            UIFoundation.waitFor(8L);
            objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkSearchTypeList));
            UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.selectLastValueFromDropdown(ListPage.btnFirstProdQuantitySelect));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			
			if (UIFoundation.isDisplayed(ListPage.txtLimitReachedInPIP)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Limit reached button is displayed in PIP page.", "Pass", "");
				
				
			} else {
				objStatus+=false;
				String objDetail="Limit reached button is not displayed in PIP page.";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}		
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.spnRatingProd));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.imgObj_MyWine));
			UIFoundation.waitFor(3L);
			
			if (UIFoundation.isDisplayed(ListPage.spnlimitReachedInMyWine)) {
				objStatus+=true;
				ReportUtil.addTestStepsDetails("Limit reached button is displayed in MyWine page.", "Pass", "");
				
				
			} else {
				objStatus+=false;
				String objDetail="Limit reached button is not displayed in MyWine page.";				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method limitReached ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Style 'Limit Reached' Button test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Style 'Limit Reached' Button test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method limitReached "+ e);
			return "Fail";
			
		}
	}	
}
