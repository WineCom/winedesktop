package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard extends Mobile {




	/***************************************************************************
	 * Method Name			: VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: TM-4268
	 ****************************************************************************
	 */
	public static String addingNewCardInPaymentSection() {

		String screenshotName = "addingNewCardInPaymentSection.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		String objStatus = null;
		try {
			log.info("Adding New Card In Payment Section method started here");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ThankYouPage.txtPaymentMethods));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.txtAddNewCard));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtExpiryYear, "Yr"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtNewBillingAddress, "NameOnCard"));			
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtNewBillingCity, "BillingCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(ThankYouPage.txtBillinState, "militaryStateAA"));
			objStatus += String.valueOf(UIFoundation.setObject(ThankYouPage.txtObj_BillingZip, "militaryStateAACode"));
			objStatus+=String.valueOf(UIFoundation.setObject(ThankYouPage.txtNewBillingPhone, "PhoneNumber"));
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnBillingSave));
			UIFoundation.waitFor(3L);
			UIFoundation.clickObject(ThankYouPage.obj_WineLog);
			if (objStatus.contains("false")) {
				System.out.println("New credit card is not added in the payment section user profile");
				return "Fail";

			} else {
				System.out.println("New credit card is  added in the payment section user profile");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addingNewCreditCard "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: TM-4268
	 ****************************************************************************
	 */

	public static String searchProductWithProdName() {
		String objStatus = null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtsearchProductMobile));
			objStatus += String.valueOf(UIFoundation.setObject(ListPage.txtSearchProduct, "vinye"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtSearchTypeListMobile));
			UIFoundation.waitFor(12L);
			// System.out.println(UIFoundation.getText(driver,"obj_AddToCart"));
			// UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnAddToCart));
			UIFoundation.waitFor(4L);
			// System.out.println(UIFoundation.getText(driver,"AddAgain"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";

			} else {

				return "Pass";
			}

		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName " + e);
			return "Fail";
		}

	}	

	/***************************************************************************
	 * Method Name			: verifyDOBFiledPresent()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: TM-4268
	 ****************************************************************************
	 */

	public static String verifyDOBFiledPresent() {

		String screenshotName = "verifyDOBFiledIsPresentInPaymentSection.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		String objStatus = null;
		try {
			log.info("verify DOB Filed Is Present In Payment Section method started here");
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckout));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.obj_StateMobile, "State"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtZipCode, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(6L);			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnShipContinue)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
				UIFoundation.waitFor(10L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(6L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtDOBFormat)){
				objStatus+=true;
				ReportUtil.addTestStepsDetails("DOB field is present after adding card in userprofile", "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="DOB field is not present after adding card in userprofile";				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"dobFormat", objDetail);

			}
			if (objStatus.contains("false")) {
				System.out.println("Verify DOB Field Is Present In Paymnt After Adding New Card test case failed");
				return "Fail";

			} else {
				System.out.println("Verify DOB Field Is Present In Paymnt After Adding New Card test case executed succesfully ");
				return "Pass";
			}

		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method"
					+ e);
			return "Fail";
		}
	}
}
