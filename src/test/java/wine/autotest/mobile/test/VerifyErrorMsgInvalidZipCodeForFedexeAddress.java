package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyErrorMsgInvalidZipCodeForFedexeAddress extends Mobile {
	

	/***************************************************************************
	 * Method Name			: invalidZipCodeInFedexAddress()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				:  The purpose of this method is to fill the shipping 
	 * 						  address of the customer
	 * TM-1852
	 ****************************************************************************
	 */

	public static String invalidZipCodeInFedexAddress() {
		String objStatus=null;
		   String screenshotName = "Scenarios_fedexPickUpLocation_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
	        UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}			
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkShipToFedex));
			UIFoundation.waitFor(1L);

			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.chkSelectRecipientAdr, "Clickable", "", 50);
			
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkSelectRecipientAdr));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "InvalidZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtFedexSaveButton));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnZipCodeErrorMsg))
			{
				 objStatus+=true;
			      String objDetail="Error message is displayed on adding the new Fedex address with invalid zip code.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Error message is displayed on adding the new Fedex address with invalid zip code.");
			}else{
				 	objStatus+=false;
				   String objDetail="Error message is not displayed on adding the new Fedex address with invalid zip code.";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentSection", objDetail);
				   System.err.println("Error message is not displayed on adding the new Fedex address with invalid zip code.");
			}
		     UIFoundation.clearField(FinalReviewPage.txtFedexZip);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtFedexSaveButton));
			UIFoundation.waitFor(5L);			
	//		if(UIFoundation.isDisplayed(FinalReviewPage. "verifyPickUpLocation"))
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewAddress))
			{
				 objStatus+=true;
			      String objDetail="Verified  adding the new FedEx address with valid zipcode";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified  adding the new FedEx address with valid zipcode");
			}else{
				 	objStatus+=false;
				   String objDetail="Verified  adding the new FedEx address with valid zipcode  is failed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentSection", objDetail);
				   System.err.println("Verified  adding the new FedEx address with valid zipcode is failed ");
			}			
			
		
			UIFoundation.waitFor(5L);
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails "
					+ e);
			return "Fail";
		}

	}

}

