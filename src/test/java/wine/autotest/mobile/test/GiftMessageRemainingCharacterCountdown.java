package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class GiftMessageRemainingCharacterCountdown extends Mobile {
	


	/***************************************************************************
	 * Method Name : editRecipientPlaceORder() 
	 * Created By  : chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	public static String editRecipientEnterGiftMessage() {
		String objStatus = null;
		String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");


			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(5L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkChangeAddress))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkChangeAddress));
				UIFoundation.waitFor(3L);	
			}
		//	objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(5L);

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkaddGiftMessage))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkaddGiftMessage));
			}

			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.radRecipientGiftCheckbox);
			UIFoundation.waitFor(2L);
			if(!UIFoundation.isDisplayed(FinalReviewPage.txtGiftMessageTextArea))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.radRecipientGiftCheckbox));
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.txtMaximumCharacter)){
				objStatus+=true;
				String objDetail="'240 characters remaining' is displayed.";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="character countdown starting in a state of (240 characters remaining) is not displayed.";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			String giftMessageInptTextCount= XMLData.getTestData(testScriptXMLTestDataFileName, "giftMessageCharacter", 1);
			int giftMessageInputTextCount=giftMessageInptTextCount.length();
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtRecipientEmailField));
			//UIFoundation.scrollDownOrUpToParticularElement(driver, "giftMessageTextAreaChar");
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtgiftMessageTextArea, "giftMessageCharacter"));
			UIFoundation.waitFor(1L);
			String giftMessage=UIFoundation.getAttribute(FinalReviewPage.txtgiftMessageTextArea);
			int giftMessageOutputTextCount=giftMessage.length();
			//objStatus += String.valueOf(UIFoundation.clickObject(driver, "RecipientContinue"));
			if(UIFoundation.isDisplayed(FinalReviewPage.txtLimitCharacter) &&(giftMessageOutputTextCount<giftMessageInputTextCount) ){
				objStatus+=true;
				String objDetail="'0 characters remaining' is displayed.";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="0 character is not displayed.";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	
}
