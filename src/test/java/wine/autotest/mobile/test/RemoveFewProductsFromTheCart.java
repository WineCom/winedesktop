package wine.autotest.mobile.test;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;





public class RemoveFewProductsFromTheCart extends Mobile {
	

	static boolean isObjectPresent=false;
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Vishwanath Chavan 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotalBefore=null;
		String subTotalAfter=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String secondProductPrice=null;
		String thirdProductPrice=null;

		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before removing products from the cart===============");
			subTotalBefore=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalBefore);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			secondProductPrice=UIFoundation.getText(CartPage.spnSecondProductPrice);
			thirdProductPrice=UIFoundation.getText(CartPage.spnThirdProductPrice);
			UIFoundation.waitFor(3L);
			if(!UIFoundation.getText(CartPage.spnRemoveThirdProductFromSave).contains("Fail"))
			{
				UIFoundation.waitFor(2L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "RemoveThirdProduct");
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveThirdProduct));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));
			}
			if(!UIFoundation.getText(CartPage.spnRemoveSecondProduct).contains("Fail"))
			{
				UIFoundation.waitFor(2L);
				//UIFoundation.scrollDownOrUpToParticularElement(driver, "RemoveSecondProduct");
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveSecondProduct));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.spnObjRemove));
			}
		/*	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RemoveThirdProduct"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "obj_Remove"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RemoveSecondProduct"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "obj_Remove"));*/
			UIFoundation.waitFor(3L);
			subTotalAfter=UIFoundation.getText(CartPage.spnSubtotal);
		//	ApplicationDependent.removerProductPrice(driver, subTotalBefore, subTotalAfter, secondProductPrice, thirdProductPrice);
			UIFoundation.waitFor(3L);
			System.out.println("============Order summary after removing products from the cart===============");
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotalAfter);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			log.info("The execution of the method captureOrdersummary started here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Remove products from the cart test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Remove products from the cart test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method addProductsToCart "+ e);
			return "Fail";
		}
	}



}
