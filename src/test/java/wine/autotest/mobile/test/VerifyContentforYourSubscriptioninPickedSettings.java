package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyContentforYourSubscriptioninPickedSettings extends Mobile {

	public static String strRedBottleCount ;
	public static String strWhiteBottleCount ;
	public static String strTargetPrice ;
	public static String strAdvenTypeVery ;
	public static String strFrequency3Month ;

	/***********************************************************************************************************
	 * Method Name : RedWhiteWineSubscriptionflow() 
	 * Created By  : Ramesh S
	 * Reviewed By :Chandrashekhar
	 * Purpose     : The purpose of this method is to do RedWhite wine subscription flow' 
	 * 
	 ************************************************************************************************************
	 */

	public static String RedWhiteWineSubscriptionflow() {
		String objStatus = null;
		String screenshotName = "Scenarios_RedWhiteWineSubscriptionflow_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method RedWhiteWineSubscriptionflow started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRedWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypWhiteNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));
			strAdvenTypeVery = UIFoundation.getText(PickedPage.spnAdventurTypeVery);
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoFrequency3Months));
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			strFrequency3Month = UIFoundation.getText(PickedPage.rdoFrequency3Months);
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAddRedBottleCount));
			UIFoundation.waitFor(1L);
			strRedBottleCount = UIFoundation.getText(PickedPage.spnRedCount);
			if(strRedBottleCount!=null) {
				ReportUtil.addTestStepsDetails("Increased Red Bottle Count"+strRedBottleCount, "Pass", "");
			}else {
				ReportUtil.addTestStepsDetails("Red Bottle Count Not Increased"+strRedBottleCount, "Fail", "");
			}
			//White Wine calculation
			UIFoundation.waitFor(1L);
			strWhiteBottleCount = UIFoundation.getText(PickedPage.spnWhiteCount);
			if(strWhiteBottleCount!=null) {
				ReportUtil.addTestStepsDetails("Decreased  White Bottle Count "+strWhiteBottleCount, "Pass", "");
			}else {
				ReportUtil.addTestStepsDetails("White Bottle Count Not Decreased "+strWhiteBottleCount, "Fail", "");
			}


			strTargetPrice = UIFoundation.getText(PickedPage.spnTargetPrice);
			System.out.println("strRedBottleCount :"+strRedBottleCount);
			System.out.println("strWhiteBottleCount :"+strWhiteBottleCount);
			System.out.println("strTargetPrice :"+strTargetPrice);
			System.out.println("strAdvenTypeVery :"+strAdvenTypeVery);
			System.out.println("strFrequency3Month :"+strFrequency3Month);
			if(strTargetPrice!=null) {
				ReportUtil.addTestStepsDetails("Target Price Exist in Quantity Selection page "+strTargetPrice, "Pass", "");
			}else {
				ReportUtil.addTestStepsDetails("Target Price Not-Exist in Quantity Selection page "+strTargetPrice, "Fail", "");
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedWhiteQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				String objDetail="Subscription flow for red/White wine test case failed";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Subscription flow for red/White wine test case executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/

	public static String verifyLocalPickUpForPickedUpWine() {

		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			//objStatus+=String.valueOf(UIFoundation.clickObject(OMSPage. "VerifyFedexAddress"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocarion));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtFedexSaveButton));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
					//	objStatus+=String.valueOf(UIFoundation.setObject(OMSPage."NewBillingAddress", "NameOnCard"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
				UIFoundation.waitFor(3L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkAcceptTermsAndConditions));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
			UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
			if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
				objStatus+=true;
				String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

	/***********************************************************************************************************
	 * Method Name : ValidateContentforYourSubscriptioninPickedSettings() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the Your Subscription Content in picked settings' 
	 * 
	 ************************************************************************************************************
	 */

	public static String ValidateContentforYourSubscriptioninPickedSettings() {
		String objStatus = null;
		String screenshotName = "Scenarios_ValidateContentforYourSubscriptioninPickedSettings_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method ValidateContentforYourSubscriptioninPickedSettings started here .........");
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.imgPickedLogo));
			UIFoundation.waitFor(10L);
			String strYourSubTitle = UIFoundation.getText(PickedPage.spnPickedSetYourSubscription);
			ReportUtil.addTestStepsDetails("Your Subscription Title : "+strYourSubTitle, "Pass", "");	
			String strTargetPrice = UIFoundation.getText(PickedPage.spnpickedsetTargetPrice);
			ReportUtil.addTestStepsDetails("Target Price : "+strTargetPrice, "Pass", "");
			String strMinPrice = UIFoundation.getText(PickedPage.lnkpickedsetMinPrice);
			ReportUtil.addTestStepsDetails("Minimum Price : "+strMinPrice, "Pass", "");
			String strMaxPrice = UIFoundation.getText(PickedPage.lnkpickedsetMaxPrice);
			ReportUtil.addTestStepsDetails("Maximum Price : "+strMaxPrice, "Pass", "");
			String strRedBottleQty = UIFoundation.getText(PickedPage.spnpickedsetRedQty);
			ReportUtil.addTestStepsDetails("Red Bottle Quantity : "+strRedBottleQty, "Pass", "");
			String strRedBottlePrice = UIFoundation.getText(PickedPage.spnpickedsetRedPrice);
			ReportUtil.addTestStepsDetails("Red Bottle Price : "+strRedBottlePrice, "Pass", "");
			String strWhiteBottleQty = UIFoundation.getText(PickedPage.spnpickedsetWhiteQty);
			ReportUtil.addTestStepsDetails("White Bottle Quantity : "+strWhiteBottleQty, "Pass", "");
			String strWhiteBottlePrice = UIFoundation.getText(PickedPage.spnpickedsetWhitePrice);
			ReportUtil.addTestStepsDetails("White Bottle Price  : "+strWhiteBottlePrice, "Pass", "");
			String strFreqDesc = UIFoundation.getText(PickedPage.spnpickedsetFreqDesc);
			ReportUtil.addTestStepsDetails("Frequency Description : "+strFreqDesc, "Pass", "");
			String strAdvenLevelDesc = UIFoundation.getText(PickedPage.spnpickedsetDiscDesc);
			ReportUtil.addTestStepsDetails("Discovery Level : "+strAdvenLevelDesc, "Pass", "");
			String strFreeShipDesc = UIFoundation.getText(PickedPage.spnpickedsetFreeShip);
			ReportUtil.addTestStepsDetails("Free Shipping : "+strFreeShipDesc, "Pass", "");
			String strSatisfactGuatantDesc = UIFoundation.getText(PickedPage.spnpickedsetSatisfactGuarant);
			ReportUtil.addTestStepsDetails("Satisfaction guaranteed : "+strSatisfactGuatantDesc, "Pass", "");
			if(strYourSubTitle!=null){
				objStatus+=true;
				String objDetail="Your Subscription Content Found : "+strYourSubTitle;
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Your Subscription Content Not Found : "+strYourSubTitle;
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			System.out.println("strRedBottleQty : "+strRedBottleQty);
			System.out.println("strWhiteBottleQty : "+strWhiteBottleQty);
			System.out.println("strTargetPrice : "+strTargetPrice);
			System.out.println("strFreqDesc : "+strFreqDesc);
			System.out.println("strDiscLevelDesc : "+strAdvenLevelDesc);

			if(strTargetPrice.contains(strTargetPrice)){
				objStatus+=true;
				String objDetail="The Picked Setting Target Price matched the selected Price : "+strTargetPrice;
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="The Picked Setting Target Price Not matched the selected Price : "+strTargetPrice;
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(strRedBottleQty.contains(strRedBottleCount)){
				objStatus+=true;
				String objDetail="The Picked Setting Red Bottle count matched the selected Count : "+strRedBottleQty;
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="The Picked Setting Red Bottle count Not matched the selected Count : "+strRedBottleQty;
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(strWhiteBottleQty.contains(strWhiteBottleCount)){
				objStatus+=true;
				String objDetail="The Picked Setting White Bottle count matched the selected Count : "+strWhiteBottleQty;
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="The Picked Setting White Bottle count Not matched the selected Count : "+strWhiteBottleQty;
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(strFreqDesc.contains(strFrequency3Month)){
				objStatus+=true;
				String objDetail="The Picked Setting Frequemcy matched the selected Frequency : "+strFreqDesc;
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="The Picked Setting Frequemcy matched the selected Frequency : "+strFreqDesc;
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(strAdvenLevelDesc.contains(strAdvenTypeVery)){
				objStatus+=true;
				String objDetail="The Picked Setting Adventurous Level matched the selected Adventure Level : "+strAdvenLevelDesc;
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="The Picked Setting Frequemcy matched the selected Frequency : "+strAdvenLevelDesc;
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verification of content for Your Subscription in Picked Settings page is not completed successfully";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verification of content for Your Subscription in Picked Settings page is completed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
}
