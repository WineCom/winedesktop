package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyGrayBackgroundDisplayed extends Mobile {	
	
	/***************************************************************************
	 * Method Name			: contentfulLoadingPageWithoutBrokensearchBar()
	 * Created By			: chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String contentfulLoadingPageWithoutBrokensearchBar() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(3L);				
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.txtVerifySearchBar));
			
							
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	
	/***************************************************************************
	 * Method Name			: verifyMenuMenuAndHomePageHeaderAreIdentical()
	 * Created By			: chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String verifyMenuMenuAndHomePageHeaderAreIdentical() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(3L);				
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.txtVerifySearchBar));
			UIFoundation.waitFor(1L);		
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.imgSelectWineLogo));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.txtVerifySearchBar));
													
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForNewUser() {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("============Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total Before Tax :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String orderSummaryForExistingUser(WebDriver driver) {
		String products1=null;
		
		try {
			
			System.out.println("=======================Products Added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);

			}
			else
			{
				System.out.println("No products are added into the cart");
			}
			
			String totalPriceBeforeSaveForLater=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("=======================Order Summary in the Cart Page  =====================");
			System.out.println("SubTotal : "+UIFoundation.getText(CartPage.spnSubtotal));
			System.out.println("Shipping and handling : "+UIFoundation.getText(CartPage.spnShippingHnadling));
			System.out.println("Total price of all items :"+totalPriceBeforeSaveForLater);
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: orderSummaryForNewUser()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String productDetailsPresentInCart(WebDriver driver) {
		String products1=null;
		String products2=null;
		String products3=null;
		String products4=null;
		String products5=null;
		
		try {
			
			System.out.println("=======================Products added in to the cart  =====================");
			if(!UIFoundation.getText(ListPage.btnFirstProductInCart).contains("Fail"))
			{
				products1=UIFoundation.getText(ListPage.btnFirstProductInCart);
				System.out.println("1) "+products1);
	
			}
			
			if(!UIFoundation.getText(ListPage.btnSecondProductInCart).contains("Fail"))
			{
				products2=UIFoundation.getText(ListPage.btnSecondProductInCart);
				System.out.println("2) "+products2);

			}
			
			if(!UIFoundation.getText(ListPage.btnThirdProductInCart).contains("Fail"))
			{
				products3=UIFoundation.getText(ListPage.btnThirdProductInCart);
				System.out.println("3) "+products3);

			}
			
			if(!UIFoundation.getText(ListPage.btnFourthProductInCart).contains("Fail"))
			{
				products4=UIFoundation.getText(ListPage.btnFourthProductInCart);
				System.out.println("4) "+products4);

			}
			
			if(!UIFoundation.getText(ListPage.btnFifthProductInCart).contains("Fail"))
			{
				products5=UIFoundation.getText(ListPage.btnFifthProductInCart);
				System.out.println("5) "+products5);

			}
			return "Pass";

		} catch (Exception e) {
			return "Fail";
		}
	}
	
}
