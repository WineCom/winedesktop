package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class InvalidGiftCertificate extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: invalidPromoCode()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String invalidGiftCardValidation()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String expectedErrorMsg=null;
		String actualErrorMsg=null;
		
		String screenshotName = "Scenarios_ErrorMessage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method invalidGiftCardValidation started here ...");
			System.out.println("============Order summary ===============");
						
			expectedErrorMsg = verifyexpectedresult.inValidGiftCardmsg;
			
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.chkGiftCheckbox));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "InvalidGiftCertificate"));
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.btnGiftApply);
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
		//	UIFoundation.webDriverWaitForElement(driver, "GiftCardApplyng", "Invisible", "", 70);
		
			UIFoundation.waitFor(2L);
			actualErrorMsg=UIFoundation.getText(CartPage.txtGiftErrorCode);
			UIFoundation.waitFor(2L);
			if(actualErrorMsg.equalsIgnoreCase(expectedErrorMsg))
			{
				System.out.println(expectedErrorMsg);
			    objStatus+=true;
			    String objDetail="Actual and expected error message are same";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				System.err.println(actualErrorMsg);
			    objStatus+=false;
		        String objDetail="Actual and expected error message are not same";
		       
		        UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method invalidGiftCardValidation ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method invalidGiftCardValidation "+ e);
			return "Fail";
			
		}
	}


	
	
	
	
}