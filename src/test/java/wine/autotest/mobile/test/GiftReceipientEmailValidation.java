package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class GiftReceipientEmailValidation extends Mobile {
	

	static int orderNumber=0;
	/**************************************************************************
	 * Method Name : editRecipientPlaceORder() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to add the products to the cart
	 ****************************************************************************
	 */

	public static String editRecipientPlaceORder(WebDriver driver) {
		String objStatus = null;

		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try {
			log.info("The execution of the method editRecipientPlaceORder started here ...");
			

           if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}
                          UIFoundation.waitFor(2L);                      
                    
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkEditShippingAddress))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.imgGiftBagR);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.imgGiftBagR));
				UIFoundation.waitFor(2L);	
			}
			
			String recEmailAddress=UIFoundation.getAttribute(FinalReviewPage.txtRecipientEmail);
			System.out.println(recEmailAddress);
			if(recEmailAddress.isEmpty())
			{
				objStatus+=false;
			    String objDetail="email id is  empty by default";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.err.println("email id is empty by default");
			}
			else
			{
				System.out.println("email id is displayed by default");
				objStatus+=true;
				String objDetail="email id is displayed by default";
				 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtRecipientGiftCheckbox);
			UIFoundation.waitFor(1L); 
		//	UIBusinessFlow.isGiftCheckboxSelected(FinalReviewPage.txtRecipientGiftCheckbox);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.cboRecipientGiftAdd));
						
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);		
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Clickable", "", 50);
			}
			
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnReciptContinue);
			UIFoundation.waitFor(2L); 
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(3L);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
	        {
	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCardEdit));
	          objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.btnPaywithThisCardCvv, "CardCvid"));
	          UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaywithThisCard);
	          UIFoundation.waitFor(1L);
	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaywithThisCard));
	          UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaywithThisCard, "Invisible", "", 50);

	          UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
	          UIFoundation.waitFor(1L);
	          objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
	          UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
	         
	        }
	        else{
	          if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
	          {
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	             UIFoundation.waitFor(2L);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	             UIFoundation.waitFor(3L);
	             
	            WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
	                 
	          }
	        }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			 }
			driver.navigate().refresh();
			UIFoundation.waitFor(4L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnContinueShopping));
			UIFoundation.webDriverWaitForElement(CartPage.btnContinueShopping, "Invisible", "", 50);
			log.info("The execution of the method editRecipientPlaceORder ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	
	
	/*************************************************************************
	 * Method Name : verifyGiftRecipientEmail() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh 
	 * Purpose     : The purpose of this method is to add the products to the cart
	 *****************************************************************************/
	 

	public static String verifyGiftRecipientEmail() {
		String objStatus = null;
		
		String screenshotName = "Scenarios_Email_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method verifyGiftRecipientEmail started here ...");
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkSelectRecipientAdr));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkRecipientEidt));
			UIFoundation.waitFor(2L);
			
			/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtRecipientGiftCheckbox);
			if(giftCheckbox==true){
				UIFoundation.javaScriptClick(FinalReviewPage.txtRecipientGiftCheckbox);
				UIFoundation.waitFor(3L);
			}*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtRecipientEmail);
			UIFoundation.waitFor(3L);
			String recEmail=UIFoundation.getAttribute(FinalReviewPage.txtRecipientEmail);
			System.out.println(recEmail);
			if(!recEmail.isEmpty())
			{
				objStatus+=true;
			    String objDetail="email id used in previous order is displayed by default";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("email id used in previous order is displayed by default");
			}
			else
			{
				System.err.println("email id used in previous order is not displayed by default :");
				objStatus+=false;
				String objDetail="email id used in previous order is not displayed by default";
			  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method verifyGiftRecipientEmail ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyGiftRecipientEmail "
					+ e);
			return "Fail";
		}
	}	

}