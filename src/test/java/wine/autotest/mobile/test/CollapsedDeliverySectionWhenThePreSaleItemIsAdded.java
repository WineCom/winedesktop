package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class CollapsedDeliverySectionWhenThePreSaleItemIsAdded extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addPreSaleprodTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addPreSaleprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
						
	       addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
			}
			
			UIFoundation.waitFor(1L);
			UIFoundation.scrollUp(driver);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFeaturedMainTab));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexFeatures));
			UIFoundation.waitFor(3L);
		    addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
					if (addToCart1.contains("Add to Cart")) {
						objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
					}

					addToCart2 = UIFoundation.getText(ListPage.btnSecondProductToCart);
					if (addToCart2.contains("Add to Cart")) {
						objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnSecondProductToCart));
					}
			UIFoundation.scrollUp(driver);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifyDeliverySection()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String verifyDeliverySection() {
		   String screenshotName = "Scenarios_DeliverySection_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
			String objStatus = null;
		try {
			

               if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
            UIFoundation.waitFor(2L);          
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
		//	UIFoundation.clickObject(FinalReviewPage. "obj_State");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
			
			UIFoundation.waitFor(5L);		
			if(UIFoundation.isDisplayed(FinalReviewPage.txtEstimatedArival))
			{
				  objStatus+=true;
			      String objDetail="estimated arrival dates is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				String objDetail="estimated arrival dates is not displayed";				
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtAllPreSaleItemsDetails))
			{
				  objStatus+=true;
			      String objDetail="Your order contains a pre-sale item, including shipping & handling and   applicable taxes, will be billed when the order is placed is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			     
			}else{
				objStatus+=false;
				String objDetail="Your order contains a pre-sale item, including shipping & handling and   applicable taxes, will be billed when the order is placed is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"final", objDetail);
				
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtCancellingThePreSale))
			{
				  objStatus+=true;
			      String objDetail="canceling this Pre-sale order is subject to a 20% cancellation fee, with the remaining balance applied as a credit in your wine.com  account is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			        
			}else{
				objStatus+=false;
				String objDetail="canceling this Pre-sale order is subject to a 20% cancellation fee, with the remaining balance applied as a credit in your wine.com  account is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"final", objDetail);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
}
