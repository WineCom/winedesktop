package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ForgotPasswordInvalidEmailMsgValidation extends Mobile {
	/***************************************************************************
	 * Method Name : forgotPasswordInvalidEmailMsgValidation() 
	 * Created By : Chandrashekhar
	 * Reviewed By: Ramesh, 
	 * Purpose    :
	 ****************************************************************************
	 */
	public static String forgotPasswordInvalidEmailMsgValidation()
	{
		String objStatus=null;
		String actual=null;
		String expected=null;
		String actualEnterEMailAddMsg=null;
		String expectedEnterEMailAddMsg=null;
		
		String screenshotName = "Scenarios_forgotPasswordInvalidEmailMsgValidation_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method Forgot Password started here ...");
			//objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L);
					
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.btnForgotPasswordLink));
			UIFoundation.waitFor(2L);
			actualEnterEMailAddMsg = verifyexpectedresult.plzEnterEmailMsg;
			expectedEnterEMailAddMsg=UIFoundation.getText(LoginPage.txtEnterEmailAddMsg);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.txtForgotEmail, "invalidEmail"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.btnForgotContinue));
			UIFoundation.waitFor(3L);
			expected = verifyexpectedresult.InvalidEmailErrorsMsg;
			actual=UIFoundation.getText(LoginPage.txtinvalidEmailMsg);//invalidEmailMsg			
			if((expected.equalsIgnoreCase(actual)))
			{
				  objStatus+=true;
			      String objDetail="Email not found. Please enter a valid email address.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				
			}else{
					objStatus+=false;
			       String objDetail="'Sorry, we do not have an account with that email.' text is displayed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			if((actualEnterEMailAddMsg.equalsIgnoreCase(expectedEnterEMailAddMsg)))
			{
				  objStatus+=true;
			      String objDetail="'Please enter the email address' text is displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				   objStatus+=false;
			       String objDetail="'Please enter the email address' text is displayed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName+"email", objDetail);
			}
			log.info("The execution of the method Forgot Password ended here ...");
			if (!objStatus.contains("false"))
			{
				System.out.println("Forgot Password validation for Invalid email  test case is executed successfully");
				return "Pass";
				
			}
			else
			{
				System.out.println("Forgot Password validation for Invalid email  test case is failed");
				return "Fail";
			}
			
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method Forgot Password "+ e);
			return "Fail";
			
		}
	}

}
