package wine.autotest.mobile.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class SortFunctionalityOfGiftCards extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchGiftCard(WebDriver driver) {
		String objStatus=null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
					
			
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtSearchBarFormInput));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtSearchProduct, "sortGiftCard"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtGiftCardsTab));
			UIFoundation.waitFor(2L);
		
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				
				return "Fail";
				
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
	/***************************************************************************
	 * Method Name : sortingOptionsAtoZ() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsAtoZ() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsAtoZ started here ...");
			
			driver.navigate().to("https://qwww.wine.com/search/Gift%20Cards/0");
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.linSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkGiftSortAtoZ));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlow.validationForGiftSortingAtoZ());
			
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsAtoZ ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsAtoZ "
					+ e);
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name : sortingOptionsZtoA() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsZtoA() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsZtoA started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.linSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkGiftSortZtoA));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlow.validationForGiftSortingZtoA());			
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsZtoA ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsZtoA "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsLtoH() 
	 * Created By  : Chandrashekhar.
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsLtoH() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsLtoH started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.linSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkGiftSortLtoH));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlow.validationForGiftSortingPriceLtoH());
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsLtoH ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsLtoH "
					+ e);
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name : sortingOptionsHtoL() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh,KB Purpose : The purpose of this method is to add
	 * the products to the cart
	 ****************************************************************************
	 */

	public static String sortingOptionsHtoL() {
		String objStatus = null;

		try {
			log.info("The execution of the method sortingOptionsHtoL started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.linSortOptions));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkGiftSortHtoL));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlow.validationForGiftSortingPriceHtoL());
			UIFoundation.waitFor(3L);
			log.info("The execution of the method sortingOptionsHtoL ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method sortingOptionsHtoL "
					+ e);
			return "Fail";
		}
	}	
}
