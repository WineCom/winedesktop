package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyOptionTryVinatageAndRemoveButton extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String searchProductWithProdName() {
		String objStatus=null;
        String screenshotName = "Scenarios__tryVintage.jpeg";
        String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
		
			int countOfProductsInCartBefore=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			
			driver.get("https://qwww.wine.com/list/wine/red-wine/cabernet-sauvignon/7155-124-139");
			UIFoundation.waitFor(12L);
	//		objStatus+=String.valueOf(UIFoundation.clickObject(driver, "VintageLink"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnAddToCart));
			UIFoundation.waitFor(2L);
			int countOfProductsInCartAfter=Integer.parseInt(UIFoundation.getText(ListPage.btnCartCount));
			if(countOfProductsInCartBefore+1==countOfProductsInCartAfter)
			{
				System.out.println("Try vintage link is dispayed on PIP page and product is added to the cart");
				objStatus+=true;
                ReportUtil.addTestStepsDetails("Try vintage product is  added to the cart", "Pass", "");

			}else
			{
				System.out.println("Try vintage link is dispayed on PIP page");
				objStatus+=false;
                String objDetail="Not able to add Try vintage product is  added to the cart";
                UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);

			}
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnCartCount);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemove));
			UIFoundation.waitFor(1L);
			boolean isDisp=UIFoundation.isDisplayed(CartPage.spnObjRemove);
			if(isDisp==true)
			{
				System.out.println("Remove button is displayed");
			}
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.txtShoppingCartHeader));
			UIFoundation.waitFor(1L);
			boolean isHidden=UIFoundation.isDisplayed(CartPage.spnObjRemove);
			if(isHidden==false)
			{
				System.out.println("'Remove' icon is hided, Once the user clicks anywhere in the application");
			}
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}


	
}
