package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithGiftCardForExistinguser extends Mobile {
	

	
	
	/***************************************************************************
	 * Method Name			: addProductsToCart()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String addProductsToCart()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method orderCreationWithProme started here ...");
			driver.navigate().refresh();
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.SelectObject(ListPage.cboSelectState,"State"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexBlends));
			UIFoundation.waitFor(3L);
	/*		objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "FirstProductToCart"));
			UIFoundation.waitFor(1L);*/
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSecondProductToCart));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			log.info("The execution of the method orderCreationWithProme ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method orderCreationWithProme "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String giftAccount=null;
		
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying Gift card code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.txtGiftNumber);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
				UIFoundation.webDriverWaitForElement(CartPage.btnGiftCardApplyng, "Invisible", "", 50);

			}else
			{
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.chkGiftCheckbox);
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.chkGiftCheckbox));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate2"));
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnGiftApply));
				UIFoundation.webDriverWaitForElement(CartPage.btnGiftCardApplyng, "Invisible", "", 50);

			}

			System.out.println("============Order summary after applying Gift card code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			giftAccount=UIFoundation.getText(CartPage.txtGiftCertificatePrice);
			if(giftAccount.equalsIgnoreCase("fail"))
			{
				System.err.println("There was an error redeeming your gift card. Please be sure you haven't redeemed it already and that you entered the correct code");
				System.out.println("Subtotal:              "+subTotal);
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				System.out.println("Total Before Tax:      "+totalBeforeTax);
			}
			else
			{
				System.out.println("Subtotal:              "+subTotal);
				System.out.println("Shipping & Handling:   "+shippingAndHandling);
				System.out.println("Gift Account:            "+giftAccount);
				System.out.println("Total Before Tax:      "+totalBeforeTax);
			}
			//ApplicationDependent.discountCalculator(driver, subTotal, promeCode);
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String checkoutProcess()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(1L);
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(3L);
			}			       
	        
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)) {
				
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
			}
	          if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
	          {
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
	             UIFoundation.waitFor(2L);
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
	             objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
	             UIFoundation.waitFor(3L);
	             
	            WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);	
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				{
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				}
				
				if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
				{	
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
				}
	                 
	          }       
	
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);

			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order is placed successfully: "+orderNum);
			}else
			{
				objStatus+=false;
				String objDetail="Order  is null and Order cannot placed successfully";
			
				  UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (objStatus.contains("false"))
			{
				return "Fail";
			}
			else
			{
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method checkoutProcess of order creation with "
					+ "prome code test cases "+ e);
			return "Fail";
			
		}
	}


}
