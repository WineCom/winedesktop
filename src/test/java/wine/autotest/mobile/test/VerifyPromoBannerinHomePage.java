package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyPromoBannerinHomePage extends Mobile {
	


	/***************************************************************************
	 * Method Name : VerifypromoBanner()
	 * Created By  : Ramesh
	 * Reviewed By : Chandashekhar
	 * Purpose     : TM-405
	 ***************************************************************************
	 */
	public static String VerifypromoBanner() {
		
		String objStatus = null;
		 String screenshotName = "promoBarInSignPage.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
					+ screenshotName;
			
			try {
				log.info("Promo Banner In Home Page method started here......");
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(ThankYouPage.txtPromoBarDisp)){
				System.out.println("Promo Banner is displayed in Home  page ");
				objStatus += true;
				String objDetail = "Promo Banner is displayed in Home  page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Promo Banner is not displayed in sign in  page";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			 				
			 log.info("Promo Banner In Home Page method started here......");

				if (objStatus.contains("false")) {

					System.out.println("Promo Banner In Home Page test case is failed");

					return "Fail";
				} else {
					System.out.println("Promo Banner In Home Page test case executed succesfully");

					return "Pass";
				}
			} catch (Exception e) {
				return "Fail";

			}
	}

}
