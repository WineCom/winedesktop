package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyMenuVarietalFilters extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyVarietalMenu()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String verifyVarietalMenu() {
		 String objStatus=null;

	//	String expectedVarietalFilter[]={"Red Wine","White Wine","Champagne & Sparkling","Dessert, Sherry & Port","Ros� Wine","Sak�"};
		 String expectedVarietalFilter[]={"Pinot Noir","Cabernet Sauvignon","Other Red Blends","Syrah/Shiraz","Rh�ne Blends"};
		 ArrayList<String> expectedVarietalFilters=new ArrayList<String>(Arrays.asList(expectedVarietalFilter));
		 ArrayList<String> actualVarietalFilters=new ArrayList<String>();
		 String varietalFilter1=null;
		 String varietalFilter2=null;
		 String varietalFilter3=null;
		 String varietalFilter4=null;
		 String varietalFilter5=null;
		 String varietalFilter6=null;
		 
		 String screenshotName = "Scenarios_SubMenu_Screenshot.jpeg";
		 String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;
		try {
			log.info("The execution of method verifyVarietalMenu started here");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(6L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.txtBordexBlends));
			UIFoundation.waitFor(4L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.dwnFilterMenuIcon));
			UIFoundation.waitFor(4L);
		//	driver.findElement(By.partialLinkText("VARIETAL")).click();
		//	driver.findElement(By.linkText("VARIETAL")).click();selVerietal
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "selVarietal"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.dwnselectVarital));
			UIFoundation.waitFor(6L);
			varietalFilter1=UIFoundation.getText(ListPage.btnFirstRefinementWidget);
			varietalFilter2=UIFoundation.getText(ListPage.btnSecondRefinementWidget);
			varietalFilter3=UIFoundation.getText(ListPage.btnThirdRefinementWidget);
			varietalFilter4=UIFoundation.getText(ListPage.btnFourthRefinementWidget);
			varietalFilter5=UIFoundation.getText(ListPage.btnFifthRefinementWidget);
			varietalFilter6=UIFoundation.getText(ListPage.btnSixthRefinementWidget);
			actualVarietalFilters.add(varietalFilter1);
			actualVarietalFilters.add(varietalFilter2);
			actualVarietalFilters.add(varietalFilter3);
			actualVarietalFilters.add(varietalFilter4);
			actualVarietalFilters.add(varietalFilter5);
		//	actualVarietalFilters.add(varietalFilter6);
			System.out.println("exp:"+expectedVarietalFilters);
			System.out.println("act:"+actualVarietalFilters);
			log.info("The execution of the method verifyVarietalMenu ended here ...");
			if(expectedVarietalFilters.containsAll(actualVarietalFilters))
			{
				System.out.println("Verify the sub menu elements under varietal filters test case is executed successfully");
				objStatus+=true;
			    String objDetail="Sub menu elements under varietal filters are verified";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
				
			}else
			{
				
				System.out.println("Verify the sub menu elements under varietal filters test case is failed");
				objStatus+=false;
				String objDetail="Sub menu elements under varietal filters are not verified";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
				//ReportUtil.addTestStepsDetails("Not Verified  the sub menu elements under varietal filters", "", "");
				return "Fail";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method verifyVarietalMenu "
					+ e);
			ReportUtil.addTestStepsDetails("Not Verified  the sub menu elements under varietal filters", "", "");
			return "Fail";
		}
	}


	
}
