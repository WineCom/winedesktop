package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class CreateNewAccountWhileRatingTheProduct extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: createNewAccountWhileRatingTheProduct()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String createNewAccountWhileRatingTheProduct()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method productAttributeDescriptionInListPage started here ...");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkFirstListProd));
			UIFoundation.waitFor(2L);
		//	objStatus += String.valueOf(UIFoundation.clickObject(ListPage.ingRateProduct));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(LoginPage.createAccountLnk))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.createAccountLnk));
			}			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email,"email"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.Password, "accPassword"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavTabSignOut));
			log.info("The execution of the method productAttributeDescriptionInListPage ended here ...");
			if (objStatus.contains("false"))
			{
			System.out.println("Verify user is able to create New Account while Rating the Product test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify user is able to create New Account while Rating the Product test case executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method productAttributeDescriptionInListPage "+ e);
			return "Fail";
			
		}
	}
	
}

