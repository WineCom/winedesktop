package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ShowOutOfStocksAndRecommenderProdutcs extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: searchProductWithProdName()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to search for product 
	 * 						  with name and adding to the cart
	 ****************************************************************************
	 */
	public static String showOutOfStocksAndRecommenderProdutcs() {
		String objStatus=null;
		   String screenshotName = "Scenarios__showOutOfStock.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
							+ screenshotName;
					//WebDriverWait wait=null;
		try {
			log.info("The execution of the method searchProductWithProdName started here ...");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtPinotNoir));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.lblShowOutOfStock));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(ListPage.lblCurrentlyUnavailableProduct))
			{	
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lblCurrentlyUnavailableProductName));
				UIFoundation.waitFor(5L);
				if(UIFoundation.isDisplayed(ListPage.lblCurrentlyUnavailableProductInPIP))
				{
					objStatus+=true;
					ReportUtil.addTestStepsDetails("Product is currnetly unavailable", "Pass", "");
					
				}else{
					objStatus+=false;
					String objDetail="Product is currnetly available";
					UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					
				}
			}else{
				objStatus+=false;
				String objDetail="Product is currnetly available in List page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				
			}
			//
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnRecommendedProductsList);
			UIFoundation.waitFor(2L);
			String status=String.valueOf(UIBusinessFlow.recommendedProductsAddToCartBtn());
			if(status.contains("false")){
				objStatus+=false;
				String objDetail="'Add to Cart button' is not displayed for Recommended Products'";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"recommended", objDetail);			
			}else{

			objStatus+=true;
			ReportUtil.addTestStepsDetails("'Add to Cart button' is displayed for Recommended Products'", "Pass", "");
			}
			log.info("The execution of the method searchProductWithProdName ended here ...");
			if(objStatus.contains("false"))
			{
				return "Fail";
				
			}
			else
			{	
				return "Pass";
			}
			
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method searchProductWithProdName "+ e);
			return "Fail";
		}

	}
}
