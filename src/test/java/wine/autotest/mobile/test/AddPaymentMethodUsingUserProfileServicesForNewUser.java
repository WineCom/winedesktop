package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class AddPaymentMethodUsingUserProfileServicesForNewUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: addPaymentMethod()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 ****************************************************************************
	 */

	public static String addPaymentMethod()
	{
		String objStatus=null;
		try
		{
			log.info("The execution of the method addAddress started here ...");
			driver.navigate().refresh();
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnMainNavAccTab);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkPaymentMethods));
			UIFoundation.waitFor(6L);			
			if(UIFoundation.isDisplayed(ThankYouPage.txtAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtAddNewCard));
				UIFoundation.waitFor(2L);
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingAddress, "dryAddress"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingCty, "dryCity"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.txtBillingSelState, "dryState"));
			UIFoundation.javaScriptClick(FinalReviewPage.txtBillingSelState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtBillingZipCode, "dryZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_BillingPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnBillingSave));
			UIFoundation.waitFor(2L);
			log.info("The execution of the method addAddress ended here ...");	
			if (objStatus.contains("false") && (!UIFoundation.isDisplayed(FinalReviewPage.txtPaymentMethodsHeader)))
			{
				System.out.println("Adding payment method using user profile services for new user test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Adding payment method using user profile services for new user test case is executed successfully");
				return "Pass";
			}
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method addPaymentMethod "+ e);
			return "Fail";
		}
	}
	
}
