package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class ResponsiveHeaderMenu extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: varietalResponse()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String varietalResponse()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_listView_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
		//	objStatus += String.valueOf(UIFoundation.mouseHover(driver, "varietal"));
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtBordexBlends));
			UIFoundation.waitFor(5L);
			String listUrl=driver.getCurrentUrl();
			if(listUrl.contains("list/wine")){
				objStatus+=true;
			      String objDetail="User is landed on the list view on selecting Varietal ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("User is landed on the list view on selecting Varietal ");
			}else{
				 objStatus+=false;
				   String objDetail="User is not landed on the list view on selecting Varietal";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"var", objDetail);
				   System.err.println("User is not landed on the list view on selecting Varietal");
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlow.validationForResponseHeader("Bordeaux Red Blends"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnVarietalFilterClose));
			UIFoundation.waitFor(5L);
			if(!UIFoundation.isDisplayed(ListPage.btnVarietalFilterClose)){
				objStatus+=true;
			      String objDetail="Varietal filters are removed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Varietal filters are removed");
			}else{
				objStatus+=false;
				   String objDetail="Varietal filters are not removed";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName+"varFil", objDetail);			      
				   System.err.println("Varietal filters are not removed");
			}
			
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(5L);
			
			objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.lnkRegionTab));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtNapaValley));
					
			UIFoundation.waitFor(5L);
			String listUrlRegion=driver.getCurrentUrl();
			if(listUrlRegion.contains("list/wine/napa-valley")){
				objStatus+=true;
			      String objDetail="User is landed on the list view on selecting Region";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("User is landed on the list view on selecting Region");
			}else{
				 objStatus+=false;
				   String objDetail="User is not landed on the list view on selecting Region";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"Reg", objDetail);
				   System.err.println("User is not landed on the list view on selecting Region");
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIBusinessFlow.validationForResponseHeader("Napa Valley"));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnVarietalFilterClose));
			UIFoundation.waitFor(2L);
			if(!UIFoundation.isDisplayed(ListPage.btnVarietalFilterClose)){
				objStatus+=true;
			      String objDetail="Varietal filters are removed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Varietal filters are removed");
			}else{
				objStatus+=false;
				   String objDetail="Varietal filters are not removed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"RegFil", objDetail);
				   System.err.println("Varietal filters are not removed");
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(6L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFeaturedMainTab));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtTopRated));
			UIFoundation.waitFor(5L);
			String listUrlFeatured=driver.getCurrentUrl();
			if(listUrlFeatured.contains("list/wine/7155?ratingmin=94")){
				objStatus+=true;
			      String objDetail="User is landed on the list view on selecting featured";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("User is landed on the list view on selecting featured");
			}else{
				 objStatus+=false;
				   String objDetail="User is not landed on the list view on selecting featured";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"fet", objDetail);
				   System.err.println("User is not landed on the list view on selecting featured");
			}
			if(UIFoundation.isDisplayed(ListPage.lnkListProdName)){
				objStatus+=true;
			      String objDetail="Verified the content of the list and data is dispalyed for Feautures option";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the content of the list and data is dispalyed for Feautures option");
			}else{
				objStatus+=false;
				   String objDetail="Verified the content of the list test case if failed for Feautures option";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"fetFil", objDetail);
			       
				   System.err.println("Verified the content of the list test case if failed for Feautures option");
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnVarietalFilterClose));
			UIFoundation.waitFor(2L);
			if(!UIFoundation.isDisplayed(ListPage.btnVarietalFilterClose)){
				objStatus+=true;
			      String objDetail="Varietal filters are removed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Varietal filters are removed");
			}else{
				objStatus+=false;
				   String objDetail="Varietal filters are not removed";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"fetFil", objDetail);
				   System.err.println("Varietal filters are not removed");
			}
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnGfts));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtGiftsBottttles));
			UIFoundation.waitFor(5L);
			String listUrlGifts=driver.getCurrentUrl();
			if(listUrlGifts.contains("list/gifts")){
				objStatus+=true;
			      String objDetail="User is landed on the list view on selecting Gifts";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("User is landed on the list view on selecting Gifts");
			}else{
				 objStatus+=false;
				   String objDetail="User is not landed on the list view on selecting Gifts";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"Gifts", objDetail);
				   System.err.println("User is not landed on the list view on selecting Gifts");
			}
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(ListPage.lnkListProdName)){
				objStatus+=true;
			      String objDetail="Verified the content of the list and data is dispalyed for Gifts option";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Verified the content of the list and data is dispalyed");
			}else{
				objStatus+=false;
				   String objDetail="Verified the content of the list test case if failed for Gifts option";
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"fetFil", objDetail);
				   System.err.println("Verified the content of the list test case if failed for Gifts option");
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnVarietalFilterClose));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(ListPage.btnVarietalFilterClose)){
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnVarietalFilterClose));
				UIFoundation.waitFor(2L);
			}

			if(!UIFoundation.isDisplayed(ListPage.btnVarietalFilterClose)){
				objStatus+=true;
			      String objDetail="Varietal filters are removed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Varietal filters are removed");
			}else{
				objStatus+=false;
				   String objDetail="Varietal filters are not removed";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"GiftsFil", objDetail);
				   System.err.println("Varietal filters are not removed");
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}	
}
