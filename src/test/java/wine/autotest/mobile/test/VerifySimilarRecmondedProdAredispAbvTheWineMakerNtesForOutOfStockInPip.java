package wine.autotest.mobile.test;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip extends Mobile {
	
	
	/***************************************************************************
	 * Method Name : RecomendedProdDisplayedForOutOfStock() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : TM-4314
	 ****************************************************************************
	 */

	public static String recomendedProdDisplayedForOutOfStock() {
		String objStatus = null;
		String screenshotName = "recomendedProdDisplayedForOutOfStock.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of method productFilteration started here");
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtCaberNet));
			UIFoundation.waitFor(2L);			
		//	objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkmoreFilters));
			UIFoundation.waitFor(2L);	
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.chkOutOfStockCheckbox);		
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.chkOutOfStockCheckbox));	
			UIFoundation.waitFor(4L);			
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkPipProduct);		
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.lnkPipProduct));
			UIFoundation.waitFor(2L);		
			UIFoundation.scrollDownOrUpToParticularElement(CartPage.spnAddRecommendedProducts);
			objStatus += String.valueOf(UIFoundation.isDisplayed(CartPage.spnAddRecommendedProducts));
			UIFoundation.waitFor(2L);	

			if (objStatus.contains("false"))
			{			
				String objDetail="Recommended product section is not displayed above the winemaker note" ;
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");							
				return "Fail";
			}
			else
			{
				String objDetail="Recommended product section is displayed above the winemaker succesfully" ;
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");			
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println(" Search product with product name test case is failed");
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}

}
