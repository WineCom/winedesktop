package wine.autotest.mobile.test;


import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;

import wine.autotest.mobile.test.Mobile;;





public class OrderCreationWithPromoCodeForNewUser extends Mobile {
	

	static boolean isObjectPresent=false;
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */

	
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		
		try
		{
						
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCodeText))
			{
				
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnPromoCodeApplyLink));
				UIFoundation.waitFor(5L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtObjPromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnPromoCodeApplyLink));
				UIFoundation.waitFor(5L);
			}

			System.out.println("============Order summary after applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			promeCode=UIFoundation.getText(CartPage.spnPromeCodePrice);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Prome Code:            "+promeCode);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
		//	ApplicationDependent.discountCalculator(CartPage.subTotal, promeCode);
			
			if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}
                         
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingType));
			UIFoundation.waitFor(2L);
			
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);  //obj_State,obj_Zip
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);//ShipContinueButton
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(4L);
			
			UIFoundation.waitFor(6L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
		
			UIFoundation.waitFor(3L);
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : addNewCreditCard()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			log.info("The execution of the method addNewCreditCard started here ...");
			System objExpectedRes = null;
			expected = objExpectedRes.getProperty("placeOrderConfirmation");
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(3L);
			}
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
/*			UIFoundation.javaScriptClick(driver, "BillingAndShippingCheckbox");
			UIFoundation.waitFor(1L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtbirthMonth);
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			 {
			      objStatus+=true;
			      String objDetail="Order number is placed successfully";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Order number is placed successfully: "+orderNum);   
			 }else
			 {
			       objStatus+=false;
			       String objDetail="Order number is null and Order cannot placed";
			  //     UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			 }
			log.info("The execution of the method addNewCreditCard ended here ...");
		//	if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				if (objStatus.contains("false")) {
				System.out.println("Order creation with promo code for new user  test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with promo code for new user test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}


}
