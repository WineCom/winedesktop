package wine.autotest.mobile.test;


import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.PickedPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyRescheduleSubscriptioninPickedSetting extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh
	 * Purpose				: The purpose of this method is to Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.accountBtn));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "compassuser"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(LoginPage.QAUserPopUp)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.QAUserPopUp));
				UIFoundation.webDriverWaitForElement(LoginPage.QAUserPopUp, "Invisible", "", 50);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***********************************************************************************************************
	 * Method Name : enrollForPickedUpWine() 
	 * Created By  : Ramesh S
	 * Reviewed By : Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify Likeed and Disliked Red/White Wine in Anything else section' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String enrollForPickedUpWine() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyUserIsAbleToEnrollForSubscription_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyUserIsAbleToEnrollForSubscription started here .........");
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedCompassTiles));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnSignUpPickedWine));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizNovice));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineJourneyNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizWineTypRedWhite));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnWineTypeNxt));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoZinfandelLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoMalbecDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypNxt));
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoChardonnaylLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoSauvignonBlancDisLike));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnVarietalTypWhiteNxt));
			UIFoundation.waitFor(4L);
		
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnpersonalcommntNxt));
			UIFoundation.waitFor(4L);	
			if(UIFoundation.isDisplayed(PickedPage.rdoPickedQuizNtVry)) {
				objStatus+=true;
				String objDetail="User is navigated to adventurous quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to adventurous quiz next page";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.rdoPickedQuizVery));
			UIFoundation.waitFor(1L);
			
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnAdventuTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.btnMnthTypNxt)) {
				objStatus+=true;
				String objDetail="User is navigated to Month select quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Month select quiz nexr page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnMnthTypNxt));
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(PickedPage.txtSubscriptionSettingsPge)) {
				objStatus+=true;
				String objDetail="User is navigated to Subscription Settings quiz next page";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="User is not navigated to Subscription Settings quiz next page";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnRedWhiteQtyNxt));
			UIFoundation.waitFor(4L);
			if (objStatus.contains("false"))
			{
				String objDetail="Enrollment flow for red/White wine is not completed successfully";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Enrollment flow for red/White wine is completed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}

	
	/******************************************************************************************
	 * Method Name : verifyLocalPickUpForPickedUpWine
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : This method validates local pickup address with picked up wine
	 * 
	 ******************************************************************************************/
		
		public static String verifyLocalPickUpForPickedUpWine() {
			
		String objStatus = null;
		String screenshotName = "Scenarios_verifyLocalPickUpFlowForPickedUpWine_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method verifyLocalPickUpFlowForPickedUpWine started here .........");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(PickedPage.btnRecipientHeader)) {
				objStatus+=true;
				String objDetail="Subscription flow 'Recipient' page is displayed";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Subscription flow 'Recipient' page should is not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZipCode, "ZipCodeFedEx"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnShipToThisLocarion));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexFirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexLastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexStreetAddress, "Address1"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexCity, "City"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexZip, "ZipCode"));
			objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexPhoneNum, "PhoneNumber"));
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.txtFedexSaveButton));
			UIFoundation.waitFor(15L);
			if(UIFoundation.isDisplayed(PickedPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				if(UIFoundation.isDisplayed(PickedPage.txtFedexBillingAddrss)) {
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingAddrss, "Address1"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingCity, "BillingCity"));
					objStatus+=String.valueOf(UIFoundation.SelectObject(PickedPage.dwnFedexBillingState, "BillingState"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingZip, "BillingZipcode"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtFedexBillingPhoneNum, "PhoneNumber"));}
					UIFoundation.waitFor(3L);
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthMonth,"birthMonth"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthDate,"birthDate"));
					objStatus+=String.valueOf(UIFoundation.setObject(PickedPage.txtBirthYear, "birthYear"));
			}
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(PickedPage.chkAcceptTermsAndConditions));
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnEnrollInPicked));
				UIFoundation.webDriverWaitForElement(PickedPage.txtHeadlinePicked, "element", "", 50);
				if(UIFoundation.isDisplayed(PickedPage.txtHeadlinePicked)){
					objStatus+=true;
					String objDetail="User is successfully enrolled to compass and Glad to meet you screen is displayed.";
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail="User is not successfully enrolled to compass and Glad to meet you screen not displayed";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
				if (objStatus.contains("false"))
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case failed");
					return "Fail";
				}
				else
				{
					System.out.println("Validate The Subscription Flow For Local Pickup Address test case executed successfully");
					return "Pass";
				}
			}catch(Exception e)
			{
				log.error("there is an exception arised during the execution of the method"+ e);
				return "Fail";
			}
		}
		
	
	/***********************************************************************************************************
	 * Method Name : ChangedDeliveryDateinPickedSettings() 
	 * Created By  : Ramesh S
	 * Reviewed By :  Chandrashekhar
	 * Purpose     : The purpose of this method is to Verify the Delivery date changes in picked settings' 
	 * 
	 ************************************************************************************************************
	 */
	
	public static String ChangedDeliveryDateinPickedSettings() {
		String objStatus = null;
		String screenshotName = "Scenarios_ChangedDeliveryDateinPickedSettings_Screenshot.jpeg";
		String screenShotPath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\" +screenshotName ;
		try {
			log.info("Execution of the method ChangedDeliveryDateinPickedSettings started here .........");
			objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.imgPickedLogo));
			UIFoundation.waitFor(10L);
			String ExistingDate = UIFoundation.getText(PickedPage.spnPickedSetPickedDate);
			
			String strCurrentDate = UIFoundation.getDateTime("dd");
			System.out.println("ExistingDate : "+ExistingDate);
			System.out.println("strCurrentDate : "+strCurrentDate);
			if(strCurrentDate.contains(ExistingDate)) {
				objStatus+=true;
				String objDetail="The Scheduled Delivery date is current Date";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.lnkPickedSetChangeDate));
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.radDelayDateOneWeek));
				String DelayDate = UIFoundation.getText(PickedPage.radDelayDateOneWeek);
				String[] picDate = DelayDate.split("/");
				System.out.println("picDate : "+picDate[1]);
				UIFoundation.waitFor(5L);
				objStatus+=String.valueOf(UIFoundation.clickObject(PickedPage.btnDelaydateConfirm));
				UIFoundation.waitFor(1L);
				String ChangeDate = UIFoundation.getText(PickedPage.spnPickedSetPickedDate);
				String[] ChangedDate = ChangeDate.split("t");
				System.out.println("ChangedDate : "+ChangedDate[0]);
				if((ChangedDate[0].trim()).contains(picDate[1].trim())){
					objStatus+=true;
					String objDetail1="The Current delivery date is Rescheduled to delay delivery in Picked Setting page.";
					System.out.println(objDetail1);
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				}else{
					objStatus+=false;
					String objDetail2="The Current delivery date is Not Rescheduled to delay delivery in Picked Setting page.";
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					System.out.println(objDetail2);
				}	
			}else {
				objStatus+=false;
				String objDetail="The Scheduled Delivery is not Current date in Picked Setting page.";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			if (objStatus.contains("false"))
			{
				String objDetail="Verification of Reschedule Delivery date in Picked Settings page is not completed successfully";
				System.out.println(objDetail);
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				return "Fail";
			}
			else
			{
				String objDetail="Verification of Change Delay date in Picked Settings page is completed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
				return "Pass";
			}
		}catch(Exception e)
		{
			log.error("there is an exception arised during the execution of the method"+ e);
			return "Fail";
		}
	}
	
}
