package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyConfirmationBannerDisplayedWhenAddingProductToCart extends Mobile {


	
	/***************************************************************************
	 * Method Name			: confirmationBannerDisplayedForAddedProducts
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static String confirmationBannerDisplayedForAddedProducts () {

		String objStatus=null;
		String screenshotName = "confirmationBannerDisplayedForAddedProducts.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;

		try {
			log.info("verify confirmationBannerDisplayedForAddedProducts started here");
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFifthProductToCart);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			UIFoundation.waitFor(3L);
					
			if(UIFoundation.isDisplayed(CartPage.txtItemAdded) && UIFoundation.isDisplayed(CartPage.dwnShipToState)) {
				objStatus+=true;
				String objDetail="Confirmation banner displayed upon adding the product to the cart is excecuted succesfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				objStatus+=false;
				String objDetail="Confirmation banner is not displayed upon adding the product to the cart is failed";
				ReportUtil.addTestStepsDetails(objDetail, "Failed", "");
			}
			log.info("verify confirmationBannerDisplayedForAddedProducts ended here");

			if(objStatus.contains("false")) {
				System.out.println("verify confirmation Banner Displayed For Added Products failed");
				return "fail";
			}
			else {
				System.out.println("confirmation Banner Displayed For Added Products executed succesfully");
				return "Pass";
			}
		}
		catch (Exception e) {
			log.error("there is an exception arised during the execution of the method  " + e);
			return "Fail";
		}
	}
	

	
}
