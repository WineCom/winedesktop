package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose     :The purpose of this method is to fill the
	 * shipping address of the customer
	 ****************************************************************************
	 */
	static int expectedShippingMethodCharges;
	static String Shipping=null;
	
	public static String shippingDetails() {
		String objStatus = null;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			
	//		objStatus+=String.valueOf(ApplicationDependent.recipientEdit(driver));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.javaScriptClick(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.waitFor(13L);
			/*objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "SuggestedAddress"));
			UIFoundation.waitFor(6L);*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinue);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinue)){
		//	UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage. "VerifyContinueButton");
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinue, "Clickable", "", 50);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}

	}

	/***************************************************************************
	 * Method Name : addNewCreditCard() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh.
	 * Purpose     : The purpose of this method is to add the new
	 * credit card details for billing process
	 ****************************************************************************
	 */
	public static String addNewCreditCard() {
		String objStatus = null;
		String expected = null;
		String actual = null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		int cartConutBeforeRemovingProduct=0;
		int cartConutAfterRemovingProduct=0;

		try {
			
			log.info("The execution of the method addNewCreditCard started here ...");
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			UIFoundation.waitFor(3L);
			WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
		    UIFoundation.waitFor(3L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			{
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			}
			
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			UIFoundation.waitFor(8L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentCheckoutEdit"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentEdit"));
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage. "PaymentCVV", "CardCvid"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentSaveButton"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. "PaymentContinue"));
			UIFoundation.waitFor(5L);*/
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnCartEditButton));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			{
				
				UIFoundation.waitFor(1L);
				System.out.println("Navigated successfully to cart section");
				UIFoundation.waitFor(1L);
				cartConutBeforeRemovingProduct=UIBusinessFlow.cartCount();
				if(!UIFoundation.getText(CartPage.spnRemoveFirstProduct).contains("Fail"))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveFirstProduct));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnObjRemove));
					UIFoundation.waitFor(2L);
				}
				cartConutAfterRemovingProduct=UIBusinessFlow.cartCount();
				if((cartConutBeforeRemovingProduct-1)==cartConutAfterRemovingProduct)
				{
					System.out.println("One product is removed from the cart");
				}else
				{
					System.err.println("Product is not removed the cart");
				}
				UIFoundation.waitFor(2L);
				
				
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout));
				UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Invisible", "", 50);
			}else
			{
				System.err.println("Not able to navigated  Cart section");
			}
			
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "RecipientContinue"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. "DeliveryContinue"));
			UIFoundation.waitFor(4L);		*/	
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				System.out.println("Navigated successfully to Final review section");
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
				
			}else
			{
				System.err.println("Not able to Navigate Final review section");
			}
			if(!UIFoundation.isDisplayed(ListPage.btnMainNavButton))
			{
				driver.navigate().refresh();
				UIFoundation.waitFor(4L);
			}
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "MainNavButton"));
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnMainNavAccTab);
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavAccTab));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtYourOrder));
			UIFoundation.waitFor(5L);
			String orderNum=UIFoundation.getText(ThankYouPage.lnkOrderNumber);
			System.out.println(orderNum);
			System.out.println(UIFoundation.getText(ThankYouPage.lnkOrderDate));
			UIFoundation.getOrderNumber(orderNum);
			log.info("The execution of the method addNewCreditCard ended here ...");
			if (objStatus.contains("false")) {
				System.out.println("Order Creation With Edit Functionality In Final Review Page For New User test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With Edit Functionality In Final Review Page For New User test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method addNewCreditCard " + e);
			return "Fail";
		}
	}
}
