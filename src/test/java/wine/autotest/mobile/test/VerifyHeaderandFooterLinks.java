package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyHeaderandFooterLinks extends Mobile {
	


	/***************************************************************************
	 * Method Name			: VerifyCustomerCareHeader()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: TM-2818
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String VerifyCustomerCareHeader() {
		String objStatus = null;
		String screenshotName = "Scenarios_CustomerCareHeader.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		try {
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkCustCareHeader));
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.titleContactUs))
			{
				objStatus+=true;
				String objDetail = "Customer Care Page is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				String objDetail = "Customer Care Page is NOT displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
	
			if (objStatus.contains("false")) {
				String objDetail = "Verify Customer care header test case is failed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				return "Fail";
			} else {
				String objDetail = "Verify Customer care header test case is executed successfully";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyLinksinFooter()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: TM-2818
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String VerifyLinksinFooter() {
		String objStatus = null;
		String screenshotName = "Scenarios_CustomerCareHeader.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		try {
		
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.labCustomerCare))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.labCustomerCare));
				objStatus += String.valueOf(UIFoundation.VerifyText(CartPage.txtMobContactUs, "Contact Us", "label"));
				String objDetail = "Contach US is Displayed while Clicking on Customer Care Link";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				String objDetail = "Contach US is NOT Displayed while Clicking on Customer Care Link";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");	
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.lnkEmailPref))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkEmailPref));
				UIFoundation.waitFor(8L);
				String objDetail = "Email Preferences Page Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else {
					objStatus+=false;
					String objDetail = "Email Preferences Page is NOT Displayed";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.lnkAboutWine))
			{
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.lnkAboutWine);
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkAboutWine));
				UIFoundation.waitFor(8L);
				objStatus += String.valueOf(UIFoundation.VerifyText(CartPage.titleAboutUs, "Why Shop with Wine.com?", "label"));
				String objDetail = "About Us Page Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else {
					objStatus+=false;
					String objDetail = "About Us Page is NOT Displayed";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.lnkCareers))
			{
//				UIFoundation.scrollDownOrUpToParticularElement(CartPage. "linkCareers");
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkCareers));
				UIFoundation.waitFor(8L);
				objStatus += String.valueOf(UIFoundation.VerifyText(CartPage.titleCareers, "Attention Wine Lovers", "label"));
				String objDetail = "Careers Page Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else {
					objStatus+=false;
					String objDetail = "Careers Page is NOT Displayed";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.labHowtoWorkWithUs))
			{
				UIFoundation.waitFor(8L);
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.labHowtoWorkWithUs);
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.labHowtoWorkWithUs));
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.labHowtoWorkWithUs));
				UIFoundation.waitFor(8L);	
				String objDetail = "How to Work With Us Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				String objDetail = "How to Work With Us Label is NOT Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.lnkSellUsYourWine))
			{
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.lnkSellUsYourWine);
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkSellUsYourWine));
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkSellUsYourWine));
				UIFoundation.waitFor(8L);
				objStatus += String.valueOf(UIFoundation.VerifyText(CartPage.titleSellUsYourWine, "Sell Us Your Wine", "label"));
				String objDetail = "Sell Us Your Wine Page Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else {
					objStatus+=false;
					String objDetail = "Sell Us Your Wine Page is NOT Displayed";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.lnkAffiliatedProgram))
			{
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.lnkAffiliatedProgram);
				UIFoundation.waitFor(2L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkAffiliatedProgram));
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkAffiliatedProgram));
				UIFoundation.waitFor(8L);
				objStatus += String.valueOf(UIFoundation.VerifyText(CartPage.lnkAffiliatedProgram, "Affiliate Program", "label"));
				String objDetail = "Affiliate Program Page Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else {
					objStatus+=false;
					String objDetail = "Affiliate Program Page is NOT Displayed";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(CartPage.lnkPartnerships))
			{
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkPartnerships));
				UIFoundation.waitFor(5L);
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.lnkPartnerships));
				UIFoundation.waitFor(3L);
//				objStatus += String.valueOf(UIFoundation.VerifyText(CartPage. "titlePartnerships", "Partnerships", "label"));
				String objDetail = "Partnerships Page Label is Displayed";
				System.out.println(objDetail);
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				}else {
					objStatus+=false;
					String objDetail = "Partnerships Page is NOT Displayed";
					System.out.println(objDetail);
					ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
			}
			System.out.println("objStatus :"+objStatus);
			if (objStatus.contains("false")) {
				System.out.println("Verify Customer care header test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify Customer care header test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}
	
	/***************************************************************************
	 * Method Name			: VerifyCopyrightsinFooter()
	 * Created By			: Ramesh S
	 * Reviewed By			: Chandrashekhar
	 * Purpose				: TM-4203
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String VerifyCopyrightsinFooter() {
		String objStatus = null;
		String screenshotName = "Scenarios_VerifyCopyrightsinFooter.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
		     + screenshotName;
		String ExpCopyRighttxt = null;
		
		String actualCopyRighttxt = verifyexpectedresult.CopyRightText;
		try {
			
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.labCopyRights))
			{
				objStatus+=true;
				ExpCopyRighttxt = UIFoundation.getText(CartPage.labCopyRights);
				System.out.println("CopyRighttxt :"+ExpCopyRighttxt);
				UIFoundation.waitFor(3L);	
			}else {
				objStatus+=false;
				System.out.println("CopyRighttxt not foud");
			}
			if(ExpCopyRighttxt.contains(actualCopyRighttxt)) {
				objStatus+=true;
				String objDetail = "Copy Right Text Matches the Expected Text";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else {
				objStatus += false;
				UIFoundation.scrollDownOrUpToParticularElement(CartPage.labCopyRights);
				String objDetail = "Copy Right Text Not Matches the Expected Text";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
	
			if (objStatus.contains("false")) {
				System.out.println("Verify VerifyCopyrightsinFooter test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify VerifyCopyrightsinFooter test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
