package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class CreateAccountButtonInactive extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: createButtonInActive()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to Create new user account
	 ****************************************************************************
	 */
	
	public static String createButtonInActive() {
		String objStatus=null;
		
		String screenshotName = "Scenarios_CreateButton_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		String expectedErrormsg  = verifyexpectedresult.WithoutPwdError;
		String ActualErrorMsg=null;		
				
		try {
			
			log.info("The execution of method createButtonInActive started here");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));	
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(2L);			
			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.JoinNowButton));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.FirstName, "firstName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LastName, "lastName"));
			objStatus+=String.valueOf(UIFoundation.setObjectCreateAccount(LoginPage.Email,"email"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.CreateAccountButton));
			UIFoundation.waitFor(2L);
			
			ActualErrorMsg = UIFoundation.getText(LoginPage.txtPwdErrorMessage);
			//boolean status=UIFoundation.isEnabled(driver, "CreateAccountButton");
			
			log.info("The execution of the method createButtonInActive ended here ...");
			
			if(expectedErrormsg.equalsIgnoreCase(ActualErrorMsg)){
				   objStatus+=true;
			       String objDetail="Actual and expected error message are same";
			       ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      
			}else{
				   objStatus+=false;
				   String objDetail="Actual and expected error message are not same";
				   UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				   
			}
			
			if(objStatus.contains("false"))
			{
				System.out.println("Verify the create account button is inactive until mandatory fields are filled test case is failed");
				return "Fail";
			}else
			{
				System.out.println("Verify the create account button is inactive until mandatory fields are filled test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			System.out.println("User profile creation  test case is failed");
			log.error("there is an exception arised during the execution of the method createButtonInActive "
					+ e);
			return "Fail";
		}
	}
	
}
