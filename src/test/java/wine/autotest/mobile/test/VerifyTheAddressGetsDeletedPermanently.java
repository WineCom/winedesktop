package wine.autotest.mobile.test;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.pages.ThankYouPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheAddressGetsDeletedPermanently extends Mobile {
		

	/***************************************************************************
	 * Method Name : shippingDetails() 
	 * Created By  : Chandrashekhar 
	 * Reviewed By : Ramesh
	 * Purpose     : The purpose of this method is to fill the
	 * shipping address of the customer
	 * TM-2197,
	 ****************************************************************************
	 */

	public static String shippingDetails() {
		String objStatus = null;
		   String screenshotName = "Scenarios_DeleteAddress.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of the method shippingDetails started here ...");
			UIFoundation.waitFor(3L);

            if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
         	objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.radShippingType));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
		//	UIFoundation.clickObject(FinalReviewPage. "obj_State");
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
			
		   UIFoundation.waitFor(1L);
		  
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.imgWineLogoRecPage));
			UIFoundation.waitFor(2L);

			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.txtAddressBook));
			
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.lnkUserDataEdit));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.clickObject(ThankYouPage.btnRemoveThisAddressUserProfile));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ThankYouPage.btnYesremoveThisAddress);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ThankYouPage.btnYesremoveThisAddress));
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(ThankYouPage.txtYouDoNotHaveAnyAddress)){
				  objStatus+=true;
			      String objDetail="Address is deleted permanently from the user profile.";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				 objStatus+=false;
				   String objDetail="Address is not deleted permanently from the user profile.";
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method shippingDetails ended here ...");

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method shippingDetails " + e);
			return "Fail";
		}
	}	
}
