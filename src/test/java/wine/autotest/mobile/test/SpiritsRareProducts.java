package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class SpiritsRareProducts extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: validateRareProductIcon()
	 * Created By			: Chandrashekha
	 * Reviewed By			: Ramesh,
	 * Purpose				: The purpose of this method is to Validate the rare product attribute icon displayed.
	 * Jira Id         		: TM-4404				  
	 ****************************************************************************
	 */
	
	public static String validateRareProductIcon()
	{
		String objStatus=null;
		String actual=null;
		String expected=null;
		
		String screenshotName = "Scenarios_validateRareProductIcon_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{
			log.info("The execution of the method validateRareProductIcon started here ...");
			
			objStatus += String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(1L);			
			objStatus += String.valueOf(UIFoundation.clickObject(LoginPage.MainNavAccSignIn));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnRareProducts));
			UIFoundation.waitFor(1L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.spnRareProducts));
			UIFoundation.waitFor(2L);	
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.lnkPipProduct));		
			
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(ListPage.spnRareProductAttribute));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.isDisplayed(ListPage.spnRareProductAttribute));
			
			log.info("The execution of the method validateRareProductIcon ended here ...");
			if (objStatus.contains("false"))
			{
				objStatus+=false;
				   String objDetail="Rare' product attribute icon is not displayed";
				   ReportUtil.addTestStepsDetails(objDetail, "fail", "");
				   UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				System.out.println("validateRareProductIcon test case is failed");
				return "Fail";
			}
			else
			{
				 objStatus+=true;
			     String objDetail="Rare' product attribute icon is displayed successfully";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("validateRareProductIcon test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{			
			log.error("there is an exception arised during the execution of the method validateRareProductIcon "+ e);
			return "Fail";
			
		}
	}	
}
