package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class EnteredGiftCardGiftCertificateSection extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: enterGiftCertificate()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: Entering the gift card
	 ****************************************************************************
	 */
	
	
	public static String enterGiftCertificate()
	{
		String objStatus=null;
		   String screenshotName = "Scenarios_giftCert_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method enterGiftCertificate started here ...");

			String giftCert=UIFoundation.giftCertificateNumber();
			XMLData.updateTestData(testScriptXMLTestDataFileName, "GiftCertificate1", 1, giftCert);
			if(UIFoundation.isDisplayed(CartPage.txtGiftNumber))
			{
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(20L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.chkGiftCheckbox));
				UIFoundation.clearField(CartPage.txtGiftNumber);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtGiftNumber, "GiftCertificate1"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnGiftApply));
				UIFoundation.waitFor(10L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "giftCertificateUn"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			if(UIFoundation.isDisplayed(CartPage.txtGiftCertificateSuccessMsg)){
				objStatus+=true;
				String objDetail="Gift card is applied to the cart total and the appropriate message is displayed above the gift certificate field.";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");		
			}else{
				objStatus+=false;
				String objDetail="Gift certificate has been expired.Please enter new gift certificate";
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
					
			log.info("The execution of the method enterGiftCertificate ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Verify the entered gift card is applied to the cart total when the user logs in to the application in 'Gift certificate' section test case failed");
				return "Fail";
			}
			else
			{
				System.out.println("Verify the entered gift card is applied to the cart total when the user logs in to the application in 'Gift certificate' section test case executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}	
}
