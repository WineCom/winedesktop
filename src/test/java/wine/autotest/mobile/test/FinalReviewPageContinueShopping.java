package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;



import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class FinalReviewPageContinueShopping extends Mobile {


	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String verifyContinueShopping() {
	
	String expectedTitle=null;
	String actualTitle=null;
	String objStatus=null;
	
	String screenshotName = "Scenarios_TitleMatched_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;

		try {
			log.info("The execution of the method checkoutProcess started here ...");
			
			expectedTitle = verifyexpectedresult.homePageTitle;
			UIFoundation.waitFor(5L);
			
			
			if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}
                          UIFoundation.waitFor(2L);
		//	driver.navigate().refresh();
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.waitFor(3L);
			}
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard)){
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(2L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtNameOnCard);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			}
		//	WebElement ele=driver.findElement(By.xpath("//input[@name='billingAddrSameAsShip']"));
			/*if(!UIFoundation.isSelected(driver, FinalReviewPage.chkBillingAndShippingCheckbox))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(3L);
			}*/
			if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
			   {
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
			   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
			   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
			   }
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPaymentContinue))
			   {
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			   }
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnFnlRvwContinueShoppingBtn));
			UIFoundation.waitFor(1L);
			actualTitle=driver.getTitle();
			log.info("The execution of the method checkoutProcess ended here ...");
			
			if(expectedTitle.equalsIgnoreCase(actualTitle)){
				
				 objStatus+=true;
			     String objDetail="Actual and expected title are same";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				
			   objStatus+=false;
		       String objDetail="Actual and expected title are not same";
		       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
		      
			}
			
			if (objStatus.contains("false")) {
				System.out.println("Final review page continue shopping test case is failed");
				return "Fail";
			} else {
				System.out.println("Final review page continue shopping test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifyContinueShopping "
					+ e);
			return "Fail";
		}
	}



}
