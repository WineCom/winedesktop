package wine.autotest.mobile.test;

import org.apache.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import wine.autotest.mobile.test.ProductListFilteration;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.Initialize;
import wine.autotest.mobile.test.BaseTest;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.GlobalVariables;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.XMLData;

public class Mobile  extends GlobalVariables{

	
	static int numberOfTestCasePassed=0;
	static int numberOfTestCaseFailed=0;
	public static String TestScriptStatus="";
	public static String teststarttime=null;
	
	/***************************************************************************
	 * Method Name			: loadFiles()
	 * Created By			: Ramesh S
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method  is to load .properties 
	 * 						   files.
	 ****************************************************************************
	 */
	
	@BeforeClass
	@Parameters({"browser", "testEnvironment", "teststate"})
	public static void loadFiles(String browser, String testEnvironment, String teststate)
	{ 
		String teststarttime=null;
		try
		{
				log=Logger.getLogger("wine Automation ...");
				teststarttime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				environmentUrl=testEnvironment;
				state=teststate;
				ReportUtil.createReport(MobileReportFileName, teststarttime,environmentUrl);
				UIBusinessFlows.deleteFile(driver);
				xmldata=new XMLData();
				url=UIFoundation.property(configurl);
				UIFoundation.screenshotFolderName("screenshots");
				ReportUtil.startScript("Scenarios");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: ExtendReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method  is to generate extend report
	 ****************************************************************************
	 */
	@BeforeTest
	public void ExtendReport()
	{
		try
		{
			//Extend Report
			ExtentHtmlReporter reporter =new ExtentHtmlReporter(ExtendMobileReportFileName);
	        report =new ExtentReports();
	        report.attachReporter(reporter);
	        reporter.config().setDocumentTitle("Platform Automation");
	        reporter.config().setReportName("Mobile Execution Report");
	       
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	}
	
	/***************************************************************************
	 * Method Name			: LaunchBrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: This method is used to launch the browser.
	 ****************************************************************************
	 */
	@BeforeMethod
	@Parameters({"browser"})
	public static void Launchbrowser(String browser)
	{ 
			try
		{
				log=Logger.getLogger("Launch Browser ...");
				ReportUtil.deleteDescription();
				driver=Initialize.launchBrowser(browser);
				objStatus = null;
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/***************************************************************************
	 * Method Name			: TC01_BaseTest
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: this method is responsible for execution of all the 
	 * 						  scenarios.
	 ****************************************************************************
	 */
	public static String startTime=null;
	public static String endTime=null;
	public static String objStatus = null;
	
	@Test
	public static void TC01_BaseTest()
	{
			
			try
			{				
				startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(BaseTest.logout());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC02_productFilteration()
	{
			
			try
			{
				startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
			
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(ProductListFilteration.verifyOnlyThreeFiltersAreVisible());
					objStatus += String.valueOf(ProductListFilteration.verifyPagination());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC03_orderCreationWithExistingAccount()
	{			
			try
			{
               startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(OrderCreationWithExistingAccount.searchProductWithProdName());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
					objStatus += String.valueOf(OrderCreationWithExistingAccount.checkoutProcess());					
				//	objStatus += String.valueOf(ProductListFilteration.verifyPagination());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC04_orderCreationWithNewAccount()
	{
		
			try
			{
                startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithNewAccount.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
					
			}
				catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC05_saveForLaterWithoutSignIn()
	{
		
			try
			{
                startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(UIBusinessFlow.validationForSaveForLater());
				//	objStatus += String.valueOf(Initialize.closeApplication(driver));
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC06_saveForLaterWithSignIn()
	{
		
			try
			{
                startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(UIBusinessFlow.validationForSaveForLater());
				//	objStatus += String.valueOf(Initialize.closeApplication(driver));					
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC07_orderCreationWihNewAccountBillingAddress()
	{
		
			try
			{
               startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
				Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
                String[] m1=  Method.split("_");
                testCaseID = m1[0];
                methodName = m1[1];
                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(BaseTest.userProfileCreation());										
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser(driver));
					objStatus += String.valueOf(OrderCreationWihNewAccountBillingAddress.shippingDetails());
					objStatus += String.valueOf(OrderCreationWihNewAccountBillingAddress.addNewCreditCard());
				//	objStatus += String.valueOf(Initialize.closeApplication(driver));
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC08_orderCreationWihExistingAccountBillingAddress()
	{		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.login());								
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser(driver));
					objStatus += String.valueOf(OrderCreationWihExistingAccountBillingAddress.shippingDetails());
					objStatus += String.valueOf(OrderCreationWihExistingAccountBillingAddress.addNewCreditCard());
				//	objStatus += String.valueOf(Initialize.closeApplication(driver));
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");							
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC09_orderCreationWithPromoCodeForExistingUser()
	{		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);		
					
					objStatus += String.valueOf(Initialize.navigate());
					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForExistingUser.checkoutProcess());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC10_orderCreationWithPromoCodeForNewUser()
	{		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
	                
					objStatus += String.valueOf(Initialize.navigate());	
					objStatus += String.valueOf(BaseTest.userProfileCreation());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithPromoCodeForNewUser.addNewCreditCard());
									
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC11_removeFewProductsFromTheCart()
	{	
		try
		{
		 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
         String[] m1=  Method.split("_");
         testCaseID = m1[0];
         methodName = m1[1];
         logger=report.createTest(Method);	
		
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
					objStatus += String.valueOf(RemoveFewProductsFromTheCart.captureOrdersummary());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC12_productsInCartAfterRemove()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(ProductsInCartAfterRemove.captureOrdersummary());
					//objStatus += String.valueOf(BaseTest.logout(driver));
					objStatus += String.valueOf(ProductsInCartAfterRemove.productsAvailableInTheCart());				
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC13_removeProductsFromTheSaveForLater()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(ProductsInCartAfterRemove.captureOrdersummary());																		
					objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.moveProductsToSaveForLater());
					objStatus += String.valueOf(RemoveProductsFromTheSaveForLater.productsAvailableInSaveForLater());
								
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC14_forgotPassword()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(ForgotPassword.forgotPassword());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
							
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	@Parameters({"browser"})
	public static void TC16_userProfileCreationWithDesiredState(String browser)
	{	
		
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserCreationWithDesiredState.userProfileCreation());
					objStatus += String.valueOf(Initialize.closeApplication());					
					driver = Initialize.launchBrowser(browser);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserCreationWithDesiredState.login());
					objStatus += String.valueOf(Initialize.closeApplication());		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	@Parameters({"browser"})
	public static void TC17_userProfileCreationWithCheckoutProcess(String browser)
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus+=String.valueOf(UserCreationWithCheckoutProcess.addProductToCartFromListPage());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(UserCreationWithCheckoutProcess.userProfileCreation());
					objStatus += String.valueOf(BaseTest.logout());
					objStatus += String.valueOf(Initialize.closeApplication());
					driver = Initialize.launchBrowser(browser);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserCreationWithCheckoutProcess.login());
					objStatus += String.valueOf(Initialize.closeApplication());		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	@Parameters({"browser"})
	public static void TC18_userProfileCreationMyWine(String browser)
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UserProfileCreationWithMyWine.userProfileCreation());
					objStatus += String.valueOf(BaseTest.logout());
					objStatus += String.valueOf(Initialize.closeApplication());
					driver=Initialize.launchBrowser(browser);
					objStatus+=String.valueOf(Initialize.navigate());
					objStatus+=String.valueOf(UserProfileCreationWithMyWine.login());
					objStatus+=String.valueOf(Initialize.closeApplication());		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC19_orderCreationWithGiftCardForExistinguser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);			
						
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithGiftCardForExistinguser.checkoutProcess());			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC20_orderCreationWithGiftCardForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
	                
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//  	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.captureOrdersummary());
					objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC21_verifyStewardshipDiscount()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
				
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyStewardshipDiscount.stewardLogin());
					objStatus += String.valueOf(VerifyStewardshipDiscount.productIconCheck());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyStewardshipDiscount.captureOrdersummary());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC22_sortingOption()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
				
					/*objStatus += String.valueOf(Initialize.navigate(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingMostPopular(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingMostInteresting(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingCustomerRating(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingProfessionalRating(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsAtoZ(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsZtoA(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsLtoH(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingOptionsHtoL(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingOldToNew(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.sortingNewToOld(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.savings(driver));
					objStatus += String.valueOf(SortingOptionInWineListPage.justIn(driver));*/
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
							
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC23_verifyDiscountPriceInRedColorAndGrandTotalInBold()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					/*objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyDiscountPriceInRedColorAndGrandTotalInBoldColor.captureOrdersummary());*/
								
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC24_searchProductWithCharacter()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(SearchProductByCharacter.searchProductWithProdName());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC25_removeAllProductsFromTheCartContinueShoppingWithoutSignIn()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithoutSignIn.addprodTocrt());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithoutSignIn.removeProductsFromTheCart());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC26_removeAllProductsFromTheCartContinueShoppingWithSignIn()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithSignIn.addprodTocrt());
					objStatus += String.valueOf(RemoveAllProdcutsFromCartContinueShopingWithSignIn.removeProductsFromTheCart());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
										
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC28_giftRecipientEmailValidation()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
										
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
			//		objStatus += String.valueOf(GiftReceipientEmailValidation.editRecipientPlaceORder(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//		objStatus += String.valueOf(GiftReceipientEmailValidation.verifyGiftRecipientEmail(driver));
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
										
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	 @Test
	public static void TC29_applyAndRemoveromoCodeForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
				    objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(ApplyAndRemovePromoCodeForExistingUser.captureOrdersummary());		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC30_applyAndReovePromoCodeForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(ApplyAndRemovePromoCodeForNewUser.captureOrdersummary());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC31_quantityPickerInCartPage()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(QuantityPicker.quantityPick());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
							
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC32_invalidPromoCode()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());
				//	objStatus += String.valueOf(ApplicationDependent.login(driver));
					objStatus += String.valueOf(BaseTest.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(InvalidPromoCode.invalidPromoCode());				
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC33_verifySalesTaxInCartandFinalReviewPage()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
				//	objStatus += String.valueOf(ApplicationDependent.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(VerifySalesTaxInCartAndFinalReviewPage.verifySalesTaxInCartPage());
					objStatus += String.valueOf(VerifySalesTaxInCartAndFinalReviewPage.verifySalesTaxInFinalReviewPage());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC34_quantityPickerInPIP()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(QuantityPickerFromPIP.verifyQuantityPickerInCartPage());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC35_finalReviewPageContinueShopping()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(FinalReviewPageContinueShopping.verifyContinueShopping());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC36_invalidGiftCardValidation()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(InvalidGiftCertificate.invalidGiftCardValidation());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC37_VerifyWarningMsgaddShipTodayProductsAndListProductsToCart()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyWarningMessageForShipTodayProductsAndListProducts.addShipTodayProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC38_verifyWarningMsgForDryStateShippingAddress()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
				
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyFunctionlityOfDryStateStayInPreviousState.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(VerifyFunctionlityOfDryStateStayInPreviousState.dryStateShippingDetails());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");						
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC39_verifyWarningMsgForOnlyPreSaleProducts()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyWarningMsgForOnlyPreSaleProductsToCart.addPreSaleProdTocrt());					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC40_verifyErrorMsgForUsedGiftCard()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);				
					  
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(VerifyErrorMsgForUsedGiftCard.usedGiftCardValidation());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
			
	@Test
	public static void TC41_verifyWarningMsgForDryStateSaveForLater()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					/*objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.login(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(VerifyFunctionalityOfDryStateSaveItemsForLater.dryStateShippingDetails(driver));
							*/
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}	
			
	@Test
	public static void TC42_verifyDryStateProductsUnavailableFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
	                
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyDryStateProductsUnavailableFunctionality.verifyproductUnavailabileFunctionality());
		
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
			
	@Test
	public static void TC43_verifyAddToCartButtonFunctionlityForDryState()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyAddToCartButtonFunctionlityForDryState.verifyAddToCartButtonFunctionality());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");							
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC44_verifyWarningMsgForRemoveShipTodayProductsFromCart()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyWarningMsgForRemoveShipTodayProductFromCart.addShipTodayProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC45_HandelsXBottelsPerCustome()
	{	
		 
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(HandelsXBottelsPerCustomer.addLimitProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC46_CreateAccountButtonInActive()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(CreateAccountButtonInactive.createButtonInActive());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC47_VerifyInvalidEmailInCreateAccountForm()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyInValidEmailInCreateAccountForm.inValidEmail());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC48_HandleSoldInIncrementOfXProducts()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(HandleSoldInIncrementOfXProducts.addIncrementProdTocrt());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");							
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC49_SortOptionsOfGiftCard()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					//	objStatus += String.valueOf(TM_318_SortFunctionalityOfGiftCards.searchGiftCard(driver));					
					/*objStatus += String.valueOf(TM_318_SortFunctionalityOfGiftCards.sortingOptionsAtoZ(driver));			
					objStatus += String.valueOf(TM_318_SortFunctionalityOfGiftCards.sortingOptionsZtoA(driver));
					objStatus += String.valueOf(TM_318_SortFunctionalityOfGiftCards.sortingOptionsLtoH(driver));
					objStatus += String.valueOf(TM_318_SortFunctionalityOfGiftCards.sortingOptionsHtoL(driver));
					*/
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC50_VerifyOutOFStockCheckboxChecked()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyOutOFStockCheckBoxChecked.searchProductWithProdName());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");						
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC51_VerifyProductsAreListedInAlphbeticalOrder()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyProductsAreListedInAlphbeticalOrder.verifyProductsAreListedAlbhabeticalOrder());
								 
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC52_PasswordValidationLessThanSixChar()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
	                
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(PasswordValidationLessThanSixChar.passwordValidationLessThanSixChar());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		
								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC53_verifyEditFunctionalityofShippingAddress()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);				
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyEditFunctionalityofShippingAddress.recipientAddressEdit());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");								
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC54_orderCreationWithFedExAddress()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
					//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
					objStatus += String.valueOf(OrderCreationWithFedexAddress.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithFedexAddress.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC55_verifyAddGiftWrappingToCartFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());					
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyAddGiftWrappingToCartFunctionality.verifyGiftWrapping());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC56_addProductsToCartWithoutSelectingState()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
				objStatus += String.valueOf(Initialize.navigateWithoutSelectinState());
			    objStatus += String.valueOf(VerifyChooseStateDialogPrompted.addprodTocrt());
			    endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC57_verifyStewardshipProgramAddedToCart()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyStewardshipProgramAddedToCart.addStewardshipToCart());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC58_verifyItemWithoutQuantityPicker()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyItemWithoutQuantityPicker.verifyQuantityPicker());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC59_verifyUnderVarietalFilters()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyMenuVarietalFilters.verifyVarietalMenu());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC60_verifyTitleAndTotalOrderAndImageInShoppingCartPage()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(VerifyTitleAndTotalOrderAndImageInShoppingCartPage.addprodTocrt());
					objStatus += String.valueOf(VerifyTitleAndTotalOrderAndImageInShoppingCartPage.verifyTotalOrderSummary());
					objStatus += String.valueOf(VerifyTitleAndTotalOrderAndImageInShoppingCartPage.verifyImageIsAvailable());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC61_verifySaveCreditCardInfoCheckboxUncheckedFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxUnchecedFunctionality.verifySaveCrdeitCardInfo());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
									
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC62_verifySaveCreditCardInfoCheckboxCheckedFunctionality()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					/*objStatus += String.valueOf(ApplicationDependent.login(driver));*/
					objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxCheckedFunctionality.addprodTocrt());
					objStatus += String.valueOf(VerifySaveCreditCardInfoCheckboxCheckedFunctionality.verifySaveCrdeitCardInfo());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
												
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC63_verifyCollapsedPanelInShippingForm()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
				//	objStatus += String.valueOf(ApplicationDependent.login(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyCollapsedPanelInShippingForm.verifyCollapsedPanel());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}

	
	@Test
	public static void TC64_verifyStateChangeDialog()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifyStateChangeDialog.verifyStateChangrDialog());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC65_verifySuggestedShippingAddressWidget()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
					objStatus += String.valueOf(VerifySuggestedShippingAddressWidget.verifySuggestedAddress());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");									
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC66_verifyOptionTryVinatageAndRemoveButton()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(VerifyOptionTryVinatageAndRemoveButton.searchProductWithProdName());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	
	@Test
	public static void TC67_orderCreationWithRegionForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithRegionForExistingUser.checkoutProcess());
					
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC68_orderCreationWithRegionForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(OrderCreationWithRegionForNewUser.addprodTocrt());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC69_orderCreationWithProductFilterationForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					
					objStatus += String.valueOf(Initialize.navigate());
				//	objStatus += String.valueOf(OrderCreationWithFilteringTheProductsForExistingUser.productFilter());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
				//	objStatus += String.valueOf(OrderCreationWithFilteringTheProductsForExistingUser.checkoutProcess());
										
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC70_orderCreationWithProductFilterationForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
			//		objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(OrderCreationWithProductFilterationForNewUser.productFilter());
			//		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithProductFilterationForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");														
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC71_orderCreationWithSpecialProductsForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(OrderCreationWithSpecialProductsForExistingUser.specialProducts());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithSpecialProductsForExistingUser.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");																			
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC72_orderCreationWithSpecialProductsForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.specialProducts());
			   //   objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC73_orderCreationWithTopRatedProductsForExistingUser()
	{	
			try
			{
				   startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);				
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(OrderCreationWithTopRatedForExistingUser.sortingOptionsTopRated());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithTopRatedForExistingUser.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");										
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC74_orderCreationWithTopRatedProductsForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(OrderCreationWithTopRatedForNewUser.sortingOptionsTopRated());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithTopRatedForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");										
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	@Test
	public static void TC75_orderCreationWithGiftsForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.addGiftsToTheCart());
				//	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithGiftsForExistingUser.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC76_orderCreationWithGiftsForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(OrderCreationWithGiftsForNewUser.addGiftsToTheCart());
			//		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithRegionForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithGiftsForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC77_orderCreationWithMyWineForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.login());
					objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.addMyWineProdTocrt());
		//      	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithMyWineForexistingUSer.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");														
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC78_orderCreationWithShippingMethodForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(OrderCreationWithShippingMethodForExistingUser.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithShippingMethodForExistingUser.checkoutProcess());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");															
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC79_orderCreationWithShippingMethodForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);				
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//  	objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser(driver));
					objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithShippingMethodForNewUser.addNewCreditCard());
										
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC80_orderCreationWithEditFunctionalityInFinalReviewPageForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart(driver));
					objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser.checkoutProcess());
										
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC81_orderCreationWithEditFunctionalityInFinalReviewPageForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
			//		objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			//		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser(driver));
					objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");															
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
    @Test
	public static void TC82_addAddressUsingUserProfileServicesForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);				
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddAddressUsingUserProfileServicesForExistingUser.addAddress());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC83_addAddressUsingUserProfileServicesForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);				
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
				//	objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddAddressUsingUserProfileServicesForNewUser.addAddress());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC84_orderCreationUsingUserProfileServicesWithStewardshipSettings()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
			//		objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.addprodTocrt());
					objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.productDetailsPresentInCart());
					objStatus += String.valueOf(OrderCreationWithEditFunctionalityInFinalReviewPageForNewUser.shippingDetails());
					objStatus += String.valueOf(OrderCreationUsingUserProfileServiceWithStewardshipSettings.addNewCreditCard());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					
					
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC85_addPaymentMethodUsingUserProfileServicesForExistingUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);					
					
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(UIBusinessFlow.login());
					objStatus += String.valueOf(AddPaymentMethodUsingUserProfileServicesForExistingUser.addPaymentMethod());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC86_addPaymentMethodUsingUserProfileServicesForNewUser()
	{	
			try
			{
				 startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
					Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
	                String[] m1=  Method.split("_");
	                testCaseID = m1[0];
	                methodName = m1[1];
	                logger=report.createTest(Method);	
					objStatus += String.valueOf(Initialize.navigate());
					objStatus += String.valueOf(BaseTest.userProfileCreation());
			//		objStatus += String.valueOf(ApplicationDependent.userProfileCreation(driver));
					objStatus += String.valueOf(AddPaymentMethodUsingUserProfileServicesForNewUser.addPaymentMethod());
					endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
					
			}catch(Exception e)
			{
				e.printStackTrace();
			}
	}
	
	@Test
	public static void TC87_orderCreationWithWhiteWineForNewUser()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(OrderCreationWithWhiteWineForNewUSer.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());									
			objStatus += String.valueOf(OrderCreationWithWhiteWineForNewUSer.addNewCreditCard());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC88_orderCreationWithWhiteWineForExistingUser()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			
			objStatus += String.valueOf(OrderCreationWithWhiteWineForExistingUser.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(OrderCreationWithWhiteWineForExistingUser.checkoutProcess());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC89_vverifyGiftWrappingSectionNtDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(VerifyGiftWrappingSection.addGiftsTocrt());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC90_forgotPasswordValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ForgotPasswordValidation.forgotPassword());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC91_forgotPasswordInvalidEmailErrorMsgValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ForgotPasswordInvalidEmailMsgValidation.forgotPasswordInvalidEmailMsgValidation());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/*@Test(priority=92, enabled=false)
	public static void TC92_outBoundGreggOverrideLink()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(OutBound_GreggOverrideLink.addprodTocrt());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.orderSummaryForNewUser());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.shippingDetails());
			objStatus += String.valueOf(OutBound_GreggOverrideLink.addNewCreditCard());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");	


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/

	@Test
	public static void TC93_emailPrefernce()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(EmailPreference.saveButtonFunctionality());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	/*@Test(priority=94, enabled=false)
	public static void TC94_verifyPromoBarInWashingtonRegion()
	{	
		try
		{

			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(WasingtonRegion.VerifyPromoBarInWashington());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}*/

	@Test
	public static void TC95_ratingStarsHiddenInPIPForGifts()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					
			objStatus += String.valueOf(Initialize.navigate());
			//	objStatus += String.valueOf(ApplicationDependent.login(driver));
			objStatus += String.valueOf(RatingStarsHiddenInPIPForGifts.VerifyRatingStarsHiddenInPIPForGifts());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC96_InvalidEmailGiftRecipientValidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					
						
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(InvalidEmailAddressInGiftRecipientEmail.editRecipientPlaceORder());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void T97_verifyPromoBarIsDisplayedInAccountPages()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);		

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyPromoBarIsDisplayedInAccountPages.verifyPromoBarIsDisplayedInAccountPages());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC98_verifyTheFunctionalityOfAddAddressLink()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(VerifyTheFunctionalityOfAddAddressLink.verifyTheFunctionalityOfAddAddressLink());
			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC99_verifyTheContentsOfVerifyAddressDialog()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());		
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyTheContentsOfVerifyAddressDialog.shippingDetails());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC100_preferredAdressFunctionality()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.loginPrefferedAddress());		
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(PreferredAdress.preferredAdressFunctionality());			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC101_verifyErrorMsgForAddingNewCreditCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());								
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyErrorMsgForAddingNewCreditCard.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC102_verifyEditFunctionalityForHomeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyEditFunctionalityForHomeAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityForHomeAddress.editHomeAddress());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC103_verifyEditFunctionalityForFedexeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheEditFunctionalityForFedExAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheEditFunctionalityForFedExAddress.editFedexAddress());															

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC104_verifyBehaviourOFContinueButtonInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			String Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			{					

				objStatus += String.valueOf(Initialize.navigate());
				objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
				objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
				objStatus += String.valueOf(VerifyBehaviourOfContinueButtonInRecipientPage.verifyBehaviourOFContinueButtonInRecipientPage());

				endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC105_verifyTheGiftOptionSummaryRecipientSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheGiftOptionSummaryRecipientSection.verifyTheGiftOptionSummaryRecipientSection());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");					

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC106_newArrivalAlertsInUserProfile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(NewArrivalAlertsInUserProfile.newArrivalAlertsInUserProfile());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");										

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC107_updateShippingAddressInAddressBook()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);            

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UpdateShippingAddressInAddressBook.login());
			objStatus += String.valueOf(UpdateShippingAddressInAddressBook.updateShippingAddressInAddressBook());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC108_productsInsaveForLaterNtStayedBackShipToSateChange()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ProductsInsaveForLaterNotStayedBackShipToSateChange.productsInsaveForLate());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC109_stewardshipUpsellInFinalReviewPAgeForStandardShipping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(StewardshipUpsellInFinalReviewPAgeForStandardShipping.shippingDetails());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");		


		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC110_itemsNtEligibleForPromoCodeRedemption()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);				

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ItemsNotEligibleForPromoCodeRedemption.itemsNotEligibleForPromoCodeRedemption());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC111_verifyUserIsNavigatedToAppropriateTextUrl()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyUserIsNavigatedToAppropriateTextUrl.verifyUserIsNavigatedToAppropriateTextUrl());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC112_verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.verifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping(driver));

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC113_verifyErrorMsgInvalidZipCodeForFedexeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyErrorMsgInvalidZipCodeForFedexeAddress.invalidZipCodeInFedexAddress());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC114_verifyProductsAreListedInAlphabeticalOrder()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphabeticalOrder.verifyProductsAreListedInAlphabeticalOrder());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC115_verifyErrorMessageInTheLoginComponent()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyErrorMsgInLoginComponent.verifyErrorMsgInLoginComponent());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC116_updateAccountInfoWithOutDate()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UpdateAccountInfoWithOutDate.login());	    			
			objStatus += String.valueOf(UpdateAccountInfoWithOutDate.updateAccountInfoWithOutDate());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC117_aboutProffessionalandProductAttributeDescriptionInPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AboutProffessionalandProductAttributeDescriptionInPIP.aboutProffessionalandProductAttributeDescriptionInPIP());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC118_errorMessageDisplayedForTheUserBelow21Years()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);		

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(ErrorMessageDisplayedForTheUserBelow21Years.errorMessageDisplayedForTheUserBelow21Years());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC119_verifyTheSocialMediaLinks()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheSocialMediaLinks.verifyTheSocialMediaLinks());	    			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC120_stewardshipAutoRenewalOption()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipDiscount.stewardLogin());
			objStatus += String.valueOf(StewardshipAutoRenewalOption.stewardshipAutoRenewalOption());									
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC121_newAlertSectionForCurrentlyUnavailable()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(NewAlertSectionForCurrentlyUnavailable.newAlertSectionForCurrentlyUnavailable());									
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC122_promoCodesThatDntMeetMinimumAmount()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.addprodTocrt());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.enterPromoCode());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC123_giftMessageRemainingCharacterCountdown()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login()); 
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(GiftMessageRemainingCharacterCountdown.editRecipientEnterGiftMessage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC124_productAttributeDescriptionInListPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ProductAttributeDescriptionInListPage.productAttributeDescriptionInListPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC125_viewAllWineForDifferentStates()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ViewAllWineForDifferentStates.viewAllWineForDifferentStates());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC126_superscriptInProductPrice()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(SuperscriptInProductPrice.superscriptInProductPrice());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC127_ABVandFoundLowerPrice()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ABVandFoundLowerPrice.aBVandFoundLowerPrice());								
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC128_collapsedDeliverySectionWhenThePreSaleItemIsAdded()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(CollapsedDeliverySectionWhenThePreSaleItemIsAdded.addPreSaleprodTocrt());
			objStatus += String.valueOf(CollapsedDeliverySectionWhenThePreSaleItemIsAdded.verifyDeliverySection());					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC129_removeCreditCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemoveCreditCard.removeCreditCard());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC130_promoBannerBehaviourForDryState()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(PromoBannerBehaviourForDryState.promoBannerBehaviourForDryState());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC131_sortOrderInNewArrivalAlerts()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(SortOrderInNewArrivalAlerts.sortedArrivalLogin());
			objStatus += String.valueOf(SortOrderInNewArrivalAlerts.verifyTheSortingInNewArrival());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC132_fedexLocationForUPSStatesInRecipientSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(FedexLocationForUPSStatesInRecipientSection.fedexLocationForUPSStatesInRecipientSection());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC133_creditCardInfoIsUpdatedInPaymentMethods()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(CreditCardInfoIsUpdatedInPaymentMethods.addPaymentMethod());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC134_showOutOfStocksAndRecommenderProdutcs()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ShowOutOfStocksAndRecommenderProdutcs.showOutOfStocksAndRecommenderProdutcs());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC135_verifyTheAddressInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.verifyTheAddressInRecipientPage());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC136_VerifyContinueButtonIfAddressNtSelectedInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC137_verifyErrorDisplayedForInvalidProductId()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyErrorDisplayedForInvalidProductId.verifyErrorDisplayedForInvalidProductId());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC138_filterIsHighlightedInBlueInListPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(FilterIsHighlightedInBlueInListPage.filterIsHighlightedInBlueInListPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC139_verifyTheAddressGetsDeletedPermanently()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheAddressGetsDeletedPermanently.shippingDetails());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC140_addressGetsSavedOnceOnAddingNewAddressInUserProfile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.login());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.addAddressInAddressBook());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC141_deletePreferredCreditCardUserProfile()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(DeletePreferredCreditCardUserProfile.addPaymentMethod());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC142_removePreferredCreditCardDuringCheckout()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(RemovePreferredCreditCardDuringCheckout.shippingDetails());
			objStatus += String.valueOf(RemovePreferredCreditCardDuringCheckout.addNewCreditCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}



	@Test
	public static void TC144_verifyShipToTthisAddressSelectionChanged()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(VerifyShipToTthisAddressSelectionChanged.loginPrefferedAddress());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShipToTthisAddressSelectionChanged.preferredAdresSelChanged());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC145_enteredGiftCardGiftCertificateSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(EnteredGiftCardGiftCertificateSection.enterGiftCertificate());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC146_VerifyMultipleGiftCardCanBeReedemed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.verifyMoreThanTwoGiftCardReedemed());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.addNewCreditCard());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.verifyOlyTwoGftCrdSelForSingleOrder());
			objStatus += String.valueOf(VerifyMultipleGiftCardCanBeReedemed.verifyAdditionalGftCrdIsUnChked()); 

			objStatus += String.valueOf(Initialize.navigate());
			//	objStatus += String.valueOf(ItemsNotEligibleForPromoCodeRedemption.itemsNotEligibleForPromoCodeRedemption());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC147_userSelectsToPayWithDifferentCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UserSelectsToPayWithDifferentCard.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(UserSelectsToPayWithDifferentCard.userSelectsToPayWithDifferentCard());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");			

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC148_responsiveHeaderMenu()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ResponsiveHeaderMenu.varietalResponse());					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC149_verifySortFunctionalitiesUnderMyWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			/*objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.login());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.addMyWineProdTocrt());		
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.sortScannedOldToNew());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.sortScannedNewToOld());
			objStatus += String.valueOf(VerifySortFunctionalitiesUnderMyWine.addMyWineRecentActivity());*/

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC150_wireUpCalender()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(WireUpCalender.wireUpCalender());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC151_multipleTrackingOrderNumber()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.login());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.addprodTocrt());
			objStatus += String.valueOf(MultipleTrackingOrderNumber.multipleTrackingOrderNumber());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC154_verifyLinksDisplayedInPIPWhensectionScrolled()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);					

			objStatus += String.valueOf(Initialize.navigate());
			//	objStatus += String.valueOf(ItemsNotEligibleForPromoCodeRedemption.itemsNotEligibleForPromoCodeRedemption());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC155_giftBagIsDisplayedAsLineItemInShoppingCartPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(GiftBagIsDisplayedAsLineItemInShoppingCartPage.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
		//	objStatus += String.valueOf(GiftBagIsDisplayedAsLineItemInShoppingCartPage.addGiftBag());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC156_createNewAccountWhileRatingTheProduct()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(CreateNewAccountWhileRatingTheProduct.createNewAccountWhileRatingTheProduct());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC157_existingCustomerLinkInNewCustomerRegisterPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ExistingCustomerLinkInNewCustomerRegisterPage.existingCustomerLinkInNewCustomerRegisterPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC158_verifyRatingOptionIsDisplayedInSliderBar()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyRatingOptionIsDisplayedInSliderBar.verifyRatingOptionIsDisplayedInSliderBar());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC159_continueWithFacebook()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ContinueWithFacebook.continueWithFacebook());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC160_verifyTheShipsOnStatementInPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());	    			
			objStatus += String.valueOf(VerifyTheShipsOnStatementInPIP.verifyTheShipsOnStatementInPIP());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	} 

	@Test
	public static void TC161_contentfulLoadingPageWithoutBrokensearchBar()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyGrayBackgroundDisplayed.contentfulLoadingPageWithoutBrokensearchBar());										
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC162_verifyMenuMenuAndHomePageHeaderAreIdentical()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());	    			
			objStatus += String.valueOf(VerifyGrayBackgroundDisplayed.verifyMenuMenuAndHomePageHeaderAreIdentical());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC163_verifyRedWarningMessageAppearInCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());	    			

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC164_modalLightBoxWithXIcon()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheAddressInRecipientPage.recipientLogin());
			objStatus += String.valueOf(ModalLightBoxWithXIcon.modalLightBoxWithXIcon());
			objStatus += String.valueOf(ModalLightBoxWithXIcon.lightBoxInRecipientPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC166_verifyRecommendedProductsOnRating()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(Initialize.closeApplication());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC167_limitReached()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(LimitReached.limitReached());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC168_ratingStarsDisplayedProperlyInMyWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(RatingStarsDisplayedProperlyInMyWine.login());
			objStatus += String.valueOf(RatingStarsDisplayedProperlyInMyWine.ratingStarsDisplayedProperlyInMyWine());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC169_verifyTheFourAddressAndCreditcards()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(OrderCreationWithExistingAccount.searchProductWithProdName());
			objStatus += String.valueOf(VerifyTheFourAddressAndCreditcards.verifyTheFourAddressAndCreditcards());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC171_verifyMilitaryOfficeBillingAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);	                
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(VerifyMilitaryOfficeBillingAddress.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(VerifyMilitaryOfficeBillingAddress.addNewCreditCard());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC172_verifyShipToDateUpdated()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyShipToDateUpdated.verifyShipToDateUpdated());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC173_verifyOrderOfProductsIconInPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyOrderOfProductsIconInPIP.verifyOrderOfProductsIconInPIP());
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC174_shipSoonCheckBox()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(ShipSoonCheckbox.shipSoonCheckbox());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC175_verifyLocalPickupInformationDisplayedInCollapsedDeliverySection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);   

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyLocalPickupInformationDisplayedInCollapsedDeliverySection.addFedExAddress());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC176_verifyFedexDisplaysProperGoogleAdresses()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyFedexDisplaysProperGoogleAdresses.verifyFedexAddressInGoogleMaps());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	@Test
	public static void TC177_verifyBillingPhoneNumberIsRequiredField()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyBillingPhoneNumberIsRequiredField.addnewcreditCardWithoutPhoneNumberInPaymentMethod());
			objStatus += String.valueOf(VerifyBillingPhoneNumberIsRequiredField.addnewcreditCardWithoutPhoneNumberInPaymentMethod());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC178_verifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginR());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyAddressSuggestioIsDisplayedAndAddressNtAutoUpdated.verifyAddressSuggestioIsDisplayed());


			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC179_verifyAboutProfRatingsHeaderisDispInProf()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);  

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAboutProfRatingsHeaderisDispInProf.verifyprofessionalRating());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC180_passwordExceptsMoreThan15Char()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);   

			objStatus += String.valueOf(Initialize.navigate());	    	
			objStatus += String.valueOf(PasswordExceptsMoreThan15Char.verifyPasswordExceptsMoreThan15Char());
			objStatus += String.valueOf(PasswordExceptsMoreThan15Char.loginusingSamepassword());


			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC181_verifyDiffGiftBagTot()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDiffGiftBagTot.verifyDiffGiftBagTotInOrderSummary());


			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC182_verifyProductsAreRemovedInMyWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyProductsAreRemovedInMyWine.login());
			objStatus += String.valueOf(VerifyProductsAreRemovedInMyWine.verifyProductsAreRemovedInMyWine());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC183_verify30LocationsAreDispInFedexAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.login());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.verify30LocationsAreDispInFedexAddress());	                
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC184_verifyDefaultGiftBagButtonIsUnchecked()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyDefaultGiftBagButtonIsUnchecked.verifyNoGiftBagButtonIsUnchecked());
			objStatus += String.valueOf(VerifyDefaultGiftBagButtonIsUnchecked.verifyGiftbagName());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC185_verifyMyWineSignInNEditLinkForGiftMessage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);             

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyMyWineSignInNEditLinkForGiftMessage.signInModalForMyWinePage());
			objStatus += String.valueOf(VerifyMyWineSignInNEditLinkForGiftMessage.editOptionForGiftMessage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC186_VerifyPromoBarIsDispInSignAndThankYouPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);         

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promoBarInSignPageandCreateAct());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyPromoBarIsDispInSignAndThankYouPage.promobarDispInThankYouPage());

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Test
	public static void TC187_VerifyShippingCostAppliedToOrderSummary()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingCostAppliedToOrderSummary.shippingCostAppliedToOrderSummary());		                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC188_verifyAlertDisplayedWhenProdAdded()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInHomePage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInListPage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInPiptPage());
			objStatus += String.valueOf(VerifyAddToCartAlertIsDispWhenProdIsAdded.verifyAddToCartAlertInMyWinePage());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC189_VerifyAddToCartButtonDisplayedForRecommendedProducts()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddToCartForRecomondedProducts.verifyAddToCartButtonForRecommendedProducts());                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC190_VerifyRecommendedProductsAfterRatingTheStars()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddToCartForRecomondedProducts.rateTheStarsAndVerifyRecommendedProducts()); 			                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC191_VerifySaveForLaterOnClickingContinue_ShipToKY()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(SaveForLater.verifySaveForLaterOnClickingShipToKY());					
	                       

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC192_VerifyProductMovedToCartFromSaveForLaterOnClickingMoveToCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());			
			objStatus += String.valueOf(OrderCreationWithSpecialProductsForNewUser.specialProducts());			
			objStatus += String.valueOf(SaveForLater.VerifyProductMovedToCartFromSaveForLater());
			                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC193_verifyCustomerShipstateRevertedToPreviousStateInsideShoppingCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());					
			objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateInSideShoppingCart());				
			                         

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC194_verifyCustomerShipstateRevertedToPreviousStateOutsideShoppingCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());		
			objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateOutSideShoppingCart());				
		                  
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
		
	@Test
	public static void TC195_verifyTheBehaviorOfTheContinueButtonIfAddressNtSelectedInRecipientPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
     		objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.verifyTheBehaviorOfTheContinueButtonIfAddressNtSelected());
		//	objStatus += String.valueOf(VerifyErrorMsgForAddingNewCreditCard.addNewCreditCard(driver));                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC196_verifyTheBillingAddressCheckboxIsCheckedByDefault()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.checkBoxSelectedDefault());
		//	objStatus += String.valueOf(VerifyCustomerShipStateRevertedToPreviousState.customerShipstateRevertedToPreviousStateOutSideShoppingCart(driver));				
			                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC197_verifyBillingAddressDisplayedOnUncheckingTheBillingSameAsShipping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyTheBehaviourOfTheBillingAddresChechBox.unCheckBoxSelected());	                               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC198_verifyAddressGetsDeletPermanentlyFromTheShippingAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.login());
			objStatus += String.valueOf(AddressGetsSavedOnceOnAddingNewAddressInUserProfile.removeAddressInAddressBook());                       

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC199_verifyContentInTheOrderPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.shippingDetails());
			objStatus += String.valueOf(OrderCreationWithGiftCardForNewUser.addNewCreditCard());
			objStatus += String.valueOf(VerifyTheContentsInTheOrderPage.verifyTheContentOntheOrderPage());                              

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC200_verifyShippinAndHandalingChargesAppliedToTheNtStewardshipUser()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForExistingUser());
			objStatus += String.valueOf(VerifyStewardshipChargesAreSavedInFinalReviewPAgeForStandardShipping.verifyShippingAndHandalingChargesAreAppliedToTheNonStewardshipUser());                              

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC201_stewardUpsellshouldMatchShippingAndHandalingCharges()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(StewardshipUpsellInFinalReviewPAgeForStandardShipping.shippingDetails());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC202_verifyGoBackButtonIsDispInFedexWindow()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyFedexDisplaysProperGoogleAdresses.fedexLocationGoBackButtonDisplayed());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
	
	@Test
	public static void TC204_validatePromoCodeAppliedMessageDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.addprodTocrt());
			objStatus += String.valueOf(PromoCodesThatDntMeetMinimumAmount.validatePromoCodeMessageDisplayed());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace(); 
		}
	}
	
	@Test
	public static void TC205_verifyTheRareProductAttributeIconDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(SpiritsRareProducts.validateRareProductIcon());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC206_verifyPromoCodeAndGiftCardErrorMassage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyInvalidPromoCodeAndGiftCard.invalidPromoCode());
			objStatus += String.valueOf(VerifyInvalidPromoCodeAndGiftCard.invalidGiftCardValidation());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC207_verifyCorporateGiftLinkDisplayedUnderCustomerCareSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAddAntherTextAndCorporateGiftsLink.verifyCorperateLinksInCustomerCareSection());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC208_VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTatVarietalNRegOriginTextInPIPAreTheLinksToListPages.varietalNRegionLinkInPip());                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC209_VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifySimilarRecmondedProdAredispAbvTheWineMakerNtesForOutOfStockInPip.recomendedProdDisplayedForOutOfStock());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC210_verifyTheConfirmationBannerForAddedProduct()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());	
			objStatus += String.valueOf(VerifyConfirmationBannerDisplayedWhenAddingProductToCart.confirmationBannerDisplayedForAddedProducts());                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC211_verifyFilterByLinkDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyFilterByLinkDisplayed.verifyOnlyThreeFiltersAreVisible());
			objStatus += String.valueOf(VerifyFilterByLinkDisplayed.verifyMoreFiltersElementsAreVisible());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC212_verifyUserIsNtGettingPickUpInformationMultipleTimes()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyUserIsNotGettingPickUpInformationMultipleTimes.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyUserIsNotGettingPickUpInformationMultipleTimes.verifyPickUpInformation());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC213_loginUsingFacebook()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(LoginUsingFacebook.loginUsingFacebook());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC214_verifyLovalPickUpFinderInFooterMobileSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.login());
			objStatus += String.valueOf(Verify30LocationsAreDispInFedexAddress.verifyLocalPickupFinderInFooterSection());                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC215_forgotPasswordDuringCheckOut()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(ForgotPassword.forgotPassWordDuringCheckOut());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC216_verifyEditFunctionalityofPreferredAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyEditFunctionalityofShippingAddress.preferredAddressEdit());  
			

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC217_verifyCustomerCareLinkinHeader()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyCustomerCareHeader());			                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC218_verifyLinkinFooter()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyLinksinFooter());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC219_VerifyStewardshipStdShiping()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipMember.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipMember.VerifyDefaultstewardshipStdShipping());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC220_reportingAddressPopUpforNewPickUpLocation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyShippingaddress.verifyHomePickUpLocationAddress());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC221_VerifyUserNtabletoSelectSaturdayForShippinginAlaska()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginOtherState("StateAK"));
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.AlaskaStateSaturdayCalenderSelect());                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
		
	@Test
	public static void TC223_VerifyUserNtabletoSelectSaturdayForShippinginHawaii()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.loginOtherState("StateHI"));
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.HawaiiStateSaturdayCalenderSelect());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC224_verifyAddToMyWineFunctionalityinPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionalityinPIP());                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC225_verifyAddToMyWineFunctionalityinPIPforRating()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyAddToMyWineFunctionality.verifyAddToMyWineFunctionalityinPIPforRating());			                             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC226_VerifySigninModelinMyWineforListandPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method);

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifySigninModelinMyWineforListandPIP.signInModalForMyWineforListandPIP());                        

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC227_ValidateOrderHistoryProgressBar()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyOrderHistoryPage.OrderHistoryProgressBarValidation());			                            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC228_OrderHistoryShippingAmountValidationn()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyOrderHistoryPage.OrderHistoryShippingAmountValidation());                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC229_verifyCopyRighttxtinFooter()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyHeaderandFooterLinks.VerifyCopyrightsinFooter());                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC230_VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.addingNewCardInPaymentSection());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.searchProductWithProdName());
			objStatus += String.valueOf(VerifyDOBFieldIsPresentInPaymntAfterAddingNewCard.verifyDOBFiledPresent());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC231_verifyTheElementsDisplayedOverTheHeroImage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyTheElementsDisplayedOverTheHeroImage.verifyTheElementsDisplayedOverTheHeroImage());			                       

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC232_calenderDispalyedSixMonth()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(CalenderDispalyedSixMonth.calenderSixMnthInDeliverySection());                          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC233_alphabeticalOrderMovingFromSaveForLaterToCart()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AlphabeticalOrderMovingFromSaveForLaterToCart.alphabeticalOrderMovingFromSaveForLaterToCart());
			objStatus += String.valueOf(VerifyProductsAreListedInAlphabeticalOrder.verifyProductsAreListedInAlphabeticalOrder());                     

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC234_VerifyPromoBannerInHomePage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyPromoBannerinHomePage.VerifypromoBanner());	
	
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC235_VerifyStewardshipMessageNtDisplayedinCartPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipMember.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyStewardshipMember.verifyStewardshipPromoNotDisplayed());           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC236_verifyGiftCardAccountDisplayedInThePaymentOption()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());			
			objStatus += String.valueOf(VerifyTwoGiftCardAccountDisplayedInPaymentOption.verifyTwoGiftCardReedemed());			
			objStatus += String.valueOf(VerifyTwoGiftCardAccountDisplayedInPaymentOption.verifyTwoGiftCardAccountsDisplayed());     

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC237_VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress.verifyTheUserAbleToAddGiftAddress());    

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC238_verifyTheOfferPageIsDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyTheOfferPageIsDisplayedWhenNewUerClickOnUserProfile.offerPageDisplayed());
	                           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC239_verifyTheElementsDisplayedInThankyouPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.offerPageDisplayed());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.shippingDetails());
			objStatus += String.valueOf(VerifyElementDisplayedInThankYouScreen.addNewCreditCard());                    

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC240_verifyStewardshipDiscountDisplayedNegetiveInHistory()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.stewardLogin());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.shippingDetails());
			objStatus += String.valueOf(VerifyStewardshipDiscountDisplayedNegetiveInHistory.addNewCreditCard());                    

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC241_verifyDeliveryCalenderDisplayedWhenUserSelectsDeliveryAnyway()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus+=String.valueOf(UIBusinessFlow.userProfileCreation());			
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(VerifyDeliveryCalenderDisplayedWhenUserSelectsDeliveryAnyway.shippingDetails());			             

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC242_VerifyGftCardBalDispOrdrSummaryUponRedeem()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.addProductToCartLessThnGftCrdPrice());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.verifyGftCardBalDispInOrdrSummaryUponRedeem());                 

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	@Test
	public static void TC243_VerifyGftCrdAccIsDispInPymntOpt()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCrdAccIsDispInPymntOpt.verifyGftCrdAccAndBalDispInPaymntOpt());
			                

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC244_VerifyGftCrdSucessMsgDispAftrGftCrdApplied()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGftCrdSucessMsgDispAftrGftCrdApplied.verifyGiftCardAppliedSuccessMsg());		                 

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC245_VerifyTheRecipientSectionUIUnderSubscriptionFlow()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(VerifyTheRecipientSectionUIUnderSubscriptionFlow.verifyRecipientSectionAfterSubscriptionFlow());
		
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC246_ValidateTheSubscriptionFlowForLocalPickupAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(ValidateTheSubscriptionFlowForLocalPickupAddress.verifyLocalPickUpFlowForPickedUpWine());
			                

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC247_VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(VerifySubscriptionSettingsSummIsDispInChckoutNRecipientPaymnt.verifySubscriptionSettingsInChckout());
			                

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC248_VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyGftCardBalDispInOrdrSummaryUponRedeem.addProductToCartLessThnGftCrdPrice());
			objStatus += String.valueOf(OrderCreationWithNewAccount.shippingDetails());
			objStatus += String.valueOf(VerifyGiftCardWithZeroBalanceIsRemovedInPaymentSection.verifyZeroBalGftCrdIsRemovedInPymntOpt());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC249_ValidateTheSubscriptionFlowForHomeAddress()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifySignUpFunctionalitiesFlow.verifyUserIsAbleToEnrollForSubscription());
			objStatus += String.valueOf(ValidateTheSubscriptionFlowForHomeAddress.verifyHomeAddrFlowForPickedUpWine());			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC250_VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(PickedWineGetStarted.enrollForPickedUpWine());
			objStatus += String.valueOf(VerifyDetailedTermsAndConditionsPageIsDispOnClickingReadTermsLnk.verifyDetailedTermsDispOnClickingTermsLnk());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC251_verifyDeliveryViaEmailDisplayed()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.addGiftsToTheCart());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.productDetailsPresentInCart());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.shippingDetails());
			objStatus += String.valueOf(VerifyDeliveryViaEmailDisplayed.addNewCreditCard());
			           

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC252_verifyCompassUserSubscription()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.login());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.verifySubscriptions());			
	
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC253_verifyGiftCardRedeemedDisplayedInTheOrderSummary()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRedeemedDisplayedInOrderSummary.verifyGiftCardAmountInOrderSummary());					               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC254_verifyGiftCardRemainingIsNtDisplayedInTheOrderSummary()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.addprodTocrt());
			objStatus += String.valueOf(AddProdcutsToCartCaptureOrder.orderSummaryForNewUser());			
			objStatus += String.valueOf(VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary.applyGiftCode());			
			objStatus += String.valueOf(VerifyGiftCardRemainingIsNtDisplayedInTheOrderSummary.verifyGiftCardAmountInOrderSummary());            

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC255_verifyCompassUserNtAbleToEnrollForDryState()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.offerPageDisplayed());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.shippingDetails());
			objStatus += String.valueOf(VerifyCompassUserNotAbleToEnrollForDryState.addNewCreditCard());               

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC256_VerifyLikedandDisLikedRedWhiteWineinAnythingElseSectionAndTotalPricewithQtyvalidation()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateRedWhiteVarietalPreffinAnythingElseSec.VerifyLikeandDislikeinAnythingElseSec());
			              

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC257_ValidateCompassEnrollmentFlowforWhiteWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforWhiteWine.verifyUserIsAbleToEnrollForWhiteWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforWhiteWine.verifyLocalPickUpForPickedUpWine());         

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC258_ValidateCompassEnrollmentFlowforRedWine()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateEnrollmentFlowforRedWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateEnrollmentFlowforRedWine.verifyLocalPickUpForPickedUpWine());                   

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC259_ValidateCompassEnrollmentFlowforBothRedandWhiteWineWithSubscriptionSettingsandPymtMethodEdit()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.EditPaymentMethodinPickedSettings());          

			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC260_VerifyChangeDelvyDateandRecipientAddressinPickedSettingpage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.enrollForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.ChangedDeliveryDateinPickedSettings());
			objStatus += String.valueOf(ValidateCompassEnrollmentFlowforBothRedandWhiteWine.ChangeRecipientAddressinPickedSettings());
		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC261_VerifyMySommPageDisplayedthroughPickedSettingNav()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyMySommPageDisplayedthroughNav.login());
			objStatus += String.valueOf(VerifyMySommPageDisplayedthroughNav.VerifyMySommPageDisplayedthroughNavigation());
		
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	@Test
	public static void TC262_verifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup.offerPageDisplayed());
			objStatus += String.valueOf(VerifyDescriptionDisplayedInRecipientSectionWhenSelectLocalPickup.shippingDetails());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC263_verifyContentDisplayedUnderMySOMMSection()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyContenDisplayedUnderMySOMMSection.login());
			objStatus += String.valueOf(VerifyContenDisplayedUnderMySOMMSection.verifySubscriptions());			
						
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC264_VerifyContentforYourSubscriptioninPickedSetting()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.RedWhiteWineSubscriptionflow());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(VerifyContentforYourSubscriptioninPickedSettings.ValidateContentforYourSubscriptioninPickedSettings());
						
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC265_VerifyRescheduleSubscriptioninPickedSetting()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.enrollForPickedUpWine());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.verifyLocalPickUpForPickedUpWine());
			objStatus += String.valueOf(VerifyRescheduleSubscriptioninPickedSetting.ChangedDeliveryDateinPickedSettings());
			
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC266_VerifyUserAbletoSetRedWhiteBottleCountto0or6()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyRedWhiteQtyAllowedto0or6withMultipleClasses.VerifyRedWhiteQtySetto0or6());
					
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC267_VerifyRedWhiteButtonPresentinPriceQtyWidgetPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(RedWhiteButtonPresentinPriceQuantityWidget.FunctionalityRedorWhiteButtoninPriceQtyPage());
		 
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC268_VerifyLikeDislikeNtSelectedinVeritalpreferencePage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyLikeDislikeSectionNotDisplayedinVaritalPreference.LikeDisLikeNotSelectedinVarietalPage());
		    			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC269_verifyDislikeSectionNtDisplayedInReviewPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyDislikeSectionNtDisplayedTheVarietalPreferenceReviewPage.verifyDisLikeSectionInReviewPage());
		   		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC270_verifyLikeSectionNtDisplayedInReviewPage()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());
			objStatus += String.valueOf(VerifyLikeSectionNtDisplayedTheVarietalPreferenceReviewPage.verifyLikeSectionInReviewPage());
		  
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC271_verifyErrorMessageDisplayedOnClickingConfirmButton()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyCompussUsersSubscriptions.login());
			objStatus += String.valueOf(VerifyErrorMessageDisplayedUponClickingConfirmButton.verifyErrorMessageDisplayed());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC272_verifyVaritalPreferenceSectionAvailable()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.userProfileCreation());			  		
			objStatus += String.valueOf(VerifyVaritalPreferenceSectionAvailableWhenSommIsNotAssigned.offerPageDisplayed());
			objStatus += String.valueOf(VerifyVaritalPreferenceSectionAvailableWhenSommIsNotAssigned.shippingDetails());
			objStatus += String.valueOf(VerifyVaritalPreferenceSectionAvailableWhenSommIsNotAssigned.addNewCreditCard());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC273_verifyTheDisplayOfSpiritMenu()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyTheDisplayOfSpiritsMenuInTheWineHomePage.validateSpiritMenu());	
		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC274_verifySpiritsShowsAsOutOfStock()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifySpiritsShowsOutOfCartWhenSelectedOutSideState.verifyOutOfStockSpirits());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC275_verifyFiltersSupportingSpirits()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 
			
			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(UIBusinessFlow.login());
			objStatus += String.valueOf(VerifyFilterSupportingSpirits.validateTheSpiritsFilters());
			
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Test
	public static void TC276_verifyUserAbleToAddSpiritProductViaPIP()
	{	
		try
		{
			startTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");				
			Method = new Object() {} .getClass().getEnclosingMethod().getName(); 
			String[] m1=  Method.split("_");
			testCaseID = m1[0];
			methodName = m1[1];
			logger=report.createTest(Method); 

			objStatus += String.valueOf(Initialize.navigate());
			objStatus += String.valueOf(VerifyUserAbleToAddPIPViaSpirit.login());
			objStatus += String.valueOf(VerifyUserAbleToAddPIPViaSpirit.ratingStarsDisplayedProperlyInMyWine());
		    
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");

		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: Closebrowser()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser.
	 ****************************************************************************
	 */
	@AfterMethod
	public static void Closebrowser()
	{ 
			try
		{
				if (objStatus.contains("Fail"))
				{
				 	numberOfTestCaseFailed++;
				 	ReportUtil.writeTestResults(testCaseID, methodName, "Fail", startTime, endTime);
				}
				else
				{
					numberOfTestCasePassed++;
					ReportUtil.writeTestResults(testCaseID, methodName, "Pass", startTime, endTime);
				}
				log=Logger.getLogger("Close Browser ...");
				Initialize.closeApplication();
				
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/***************************************************************************
	 * Method Name			: EndReport()
	 * Created By			: Ramesh S
	 * Reviewed By			: 
	 * Purpose				: The purpose of this method is to close the browser and end the report.
	 * ****************************************************************************
	 */
	 @AfterTest
     public void CloseBrowserAndEndReport()
     {
		 try {
		
			 	report.flush(); 
		 }catch(Exception e)
			{
				e.printStackTrace();
			}
           }

	/***************************************************************************
	 * Method Name			: endScenariosExecution()
	 * Created By			: Vishwanath Chavan
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: This method responsible for updating the end time to 
	 * 						  customized html report
	 ****************************************************************************
	 */
	
	@AfterSuite
	public static void endScenariosExecution()
	{
		String endTime=null;
		try
		{
			ReportUtil.endScript(numberOfTestCasePassed,numberOfTestCaseFailed);
			endTime=UIFoundation.getDateTime("dd-MMM-yyyy hh:mm:ss z");
			ReportUtil.updateEndTime(endTime);
			UIFoundation.waitFor(3L);
			System.out.println("=======Mobile Cart test case details==============");
			System.out.println("Total no test case Passed:"+numberOfTestCasePassed);
			System.out.println("Total no test case Failed:"+numberOfTestCaseFailed);
			//**********************Sending Reort via Mail***************************************************
		//  MonitoringMail.sendEmail("drinkwinecom@gmail.com","drinkwinecom@gmail.com",ReportFileName,"PFA");
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
