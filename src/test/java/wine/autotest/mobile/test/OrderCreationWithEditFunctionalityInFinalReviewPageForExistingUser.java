package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithEditFunctionalityInFinalReviewPageForExistingUser extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhkar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String checkoutProcess() {
	
	String expected=null;
	String actual=null;
	String objStatus=null;
	String subTotal=null;
	String shippingAndHandling=null;
	String total=null;
	String salesTax=null;
	int cartConutBeforeRemovingProduct=0;
	int cartConutAfterRemovingProduct=0;
	String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
	String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
			+ screenshotName;

		try {
			log.info("The execution of the method checkoutProcess started here ...");
			expected = verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(4L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}			
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(4L);
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "ShippingTyp"));
			UIFoundation.waitFor(2L);*/
/*			if(UIFoundation.isDisplayed(driver, "shippingAddressEdit")){
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "shippingAddressEdit"));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(driver, "StreetAddress");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "StreetAddress", "Address1"));
			UIFoundation.clearField(driver, "City");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(driver, "shipState", "State"));
			UIFoundation.javaScriptClick(driver, "shipState");
			UIFoundation.clearField(driver, "ZipCode");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "ZipCode", "ZipCode"));
			UIFoundation.clearField(driver, "PhoneNum");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "shippingAddressSave"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButton"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientContinue"));
			UIFoundation.waitFor(8L);
			}*/
			 UIFoundation.waitFor(0);
	            driver.navigate().refresh();
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(10L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkAddNewCard);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "AddPayment"));
			UIFoundation.waitFor(2L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.waitFor(8L);
			}
			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(5L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			UIFoundation.waitFor(2L);
			/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage. "CartEditButton");
			UIFoundation.waitFor(1L);*/
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnCartEditButton));
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckout))
			
			{
				
				UIFoundation.waitFor(1L);
				System.out.println("Navigated successfully to cart section");
				UIFoundation.waitFor(1L);
				cartConutBeforeRemovingProduct=UIBusinessFlow.cartCount();
				if(!UIFoundation.getText(CartPage.spnRemoveFirstProduct).contains("Fail"))
				{
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnRemoveFirstProduct));
					UIFoundation.waitFor(1L);
					objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.spnObjRemove));
					UIFoundation.waitFor(2L);
				}
				cartConutAfterRemovingProduct=UIBusinessFlow.cartCount();
				if((cartConutBeforeRemovingProduct-1)==cartConutAfterRemovingProduct)
				{
					System.out.println("One product is removed from the cart");
				}else
				{
					System.err.println("Product is not removed the cart");
				}
				UIFoundation.waitFor(2L);
				
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.btnCheckout));
				UIFoundation.waitFor(6L);
			}else
			{
				System.err.println("Not able to navigated  Cart section");
			}
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "selectRecipientAdr"));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "RecipientContinue"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage. "DeliveryContinue"));
			UIFoundation.waitFor(4L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.btnPlaceOrder))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
				UIFoundation.waitFor(1L);
				System.out.println("Navigated successfully to Final review section");
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
				UIFoundation.waitFor(8L);
				String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
				//String orderNum=UIFoundation.getText(driver,"OrderNum");
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: "+orderNum);
				
			}else
			{
				
				System.err.println("Not able to Navigate Final review section");
				objStatus+=false;
		    	String objDetail1="Not able to Navigate Final review section";
		    	 UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail1);
			}
//			if(!UIFoundation.isDisplayed(driver, "MainNavButton"))
//			{
//				driver.navigate().refresh();
//				UIFoundation.waitFor(4L);
//			}
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavButton"));
			
			UIFoundation.scrollDownOrUpToParticularElement(driver, "MainNavAccountTab");
			UIFoundation.waitFor(1L);*/
		// 	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "MainNavAccountTab"));
		// 	UIFoundation.waitFor(3L);
		//	objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "YourOrder"));
		//	UIFoundation.waitFor(5L);
			
			/*if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: "+orderNum);
			}else
			{
				objStatus+=false;
		    	String objDetail="Order number is null and cannot placed successfully";
		    	UIFoundation.captureScreenShot(driver, screenshotpath, objDetail);
			}*/
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				System.out.println("Order Creation With Edit Functionality In Final Review Page For Existing User test case is failed");
				return "Fail";
			} else {
				System.out.println("Order Creation With Edit Functionality In Final Review Page For Existing User test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			return "Fail";
		}
	}
	


}
