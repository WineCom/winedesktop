package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyWarningMsgForOnlyPreSaleProductsToCart extends Mobile {
	


	/***************************************************************************
	 * Method Name : addPreSaleProdTocrt()
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh.
	 * Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addPreSaleProdTocrt() {
		String objStatus = null;

		try {
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtSearchProduct));
			objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtSearchProduct, "preSale"));
			UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.txtPreSaleProduct));
			UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnAddToCart));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCartCount));
			UIFoundation.waitFor(3L);
			UIBusinessFlow.validationForOnlyPreSaleProducts(driver);
		//	ApplicationDependent.verifyClockIconPresent(driver);
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

}
