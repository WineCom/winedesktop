package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyFedexDisplaysProperGoogleAdresses extends Mobile {


	/*****************************************************************************************
	 * Method Name : VerifyFedexDisplaysProperGoogleAdresses 
	 * Created By  : Chandrashekhar
	 * Reviewed By : 
	 * Purpose     : TM-4027
	 *******************************************************************************************
	 */

	public static String verifyFedexAddressInGoogleMaps() {	
		String AddressOnmap = null;
		String objStatus = null;
		String ScreenshotName = "VerifyFedexDisplaysProperGoogleAdresses.jpeg";		
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;

		try {
			log.info("Fedex address verification Execution method started here:");
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			
			UIFoundation.waitFor(10L);
			if (UIFoundation.isElementDisplayed(FinalReviewPage.spnRecipientChangeAddress))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.spnRecipientChangeAddress));
			if (UIFoundation.isElementDisplayed(FinalReviewPage.lnkAddNewAddressLink))
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewAddressLink));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.chkShipToFedex));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.chkShipToFedex, "ZipCodeR"));
			UIFoundation.waitFor(4L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.dwnlocationIcon));
			String addrOnlist = UIFoundation.getText(FinalReviewPage.lnkActAddrOnList);
			objStatus += String.valueOf(UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.lnkActAddrOnMap));
			String oddrOnMap = UIFoundation.getText(FinalReviewPage.lnkActAddrOnMap);
			if (addrOnlist.equals(oddrOnMap)) {
				objStatus += true;
				String objDetail = "Address displayed on PickupLoaction and Map are Same:";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Address displayed on PickupLoaction and Map are not Same";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");				
				UIFoundation.captureScreenShot(screenshotpath+ScreenshotName, objDetail);
			}
			log.info("FedEx address verification mehtod ended here:");
			if (objStatus.contains("false")) {
				System.out.println("Verified the FedexPickup Address on pickup loacation and map failed");

				return "Fail";
			} else {

				System.out.println("Verified the FedexPickup Address on pickup loacation and map executed succesfully");

				return "Pass";

			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: fedexLocationGoBackButtonDisplayed()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,
	 * Purpose				: TM-3373
	 ****************************************************************************
	 */

	public static String fedexLocationGoBackButtonDisplayed() {
		String objStatus=null;
		   String screenshotName = "fedexLocationGoBackButtonDisplayed.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		try {
			log.info("The execution of the method fedexLocationGoBackButtonDisplayed started here ...");
			UIFoundation.waitFor(2L);
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}			
            UIFoundation.waitFor(4L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.chkShipToFedex));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFedexZipCode, "fedExUPSZipcode"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFedexSearchButton));
			UIFoundation.waitFor(10L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.txtShipToThisLocation));
			UIFoundation.waitFor(2L);		
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnGoBackButton))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnGoBackButton));
				 objStatus+=true;
			      String objDetail="Go Back button is displayed in the FedEx location and user is navigated back to the local pickup location succesfully ";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				  System.out.println("Go Back button displayed in the FedEx location is succesfully ");
			}else{
				
				 	objStatus+=false;
				   String objDetail="Go Back button is not displayed in the FedEx location and user is Not navigated back to the local pickup location";			      
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PaymentSection", objDetail);
				   System.err.println("Go Back button is not displayed in the FedEx location ");
			}
			log.info("The execution of the method fedexLocationGoBackButtonDisplayed ended here ...");

			if (objStatus.contains("false")) {
				
				return "Fail";
			} else {
				
				return "Pass";
			}
		} catch (Exception e) {
			log.error("there is an exception arised during the execution of the method fedexLocationGoBackButtonDisplayed "
					+ e);
			return "Fail";
		}

	}	
}
