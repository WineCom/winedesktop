package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class GiftBagIsDisplayedAsLineItemInShoppingCartPage extends Mobile {
	
	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String login()
	{
		String objStatus=null;
		
		try
		{
			
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(1L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
            UIFoundation.waitFor(2L);	
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "giftBagUserName"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(8L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name : addGiftBag() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh. 
	 * Purpose     : The purpose of this method is to
	 * add the products to the cart
	 ****************************************************************************
	 */

	/*public static String addGiftBag(WebDriver driver) {
		String objStatus = null;
		   String screenshotName = "Scenarios_giftBagIcon_Screenshot.jpeg";
					String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
							+ screenshotName;

		try {
			log.info("The execution of the method addGiftBag started here ...");			
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(15L);  		
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEid));
			UIFoundation.waitFor(4L);
		//	objStatus += String.valueOf(UIFoundation.clickObject(driver, "RecipientShipToaddress"));
			WebElement gift = driver.findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift.isSelected()){
				UIFoundation.clickObject(FinalReviewPage.radRecipientGiftCheckbox);
			}
			//ApplicationDependent.isGiftCheckboxSelected(FinalReviewPage. "RecipientGiftCheckbox");
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.cboGiftWrapOption));
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
	//		objStatus += String.valueOf(UIFoundation.javaSriptClick(FinalReviewPage. "RecipientContinue"));
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
	        {
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkAddNewCard));
	        }
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
			    UIFoundation.waitFor(3L);
				}
				if(UIFoundation.isDisplayed(FinalReviewPage.txtbirthMonth))
				   {
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthMonth,"birthMonth"));
				   objStatus+=objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthDate,"birthDate"));
				   objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtbirthYear, "birthYear"));
				   }
				
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				UIFoundation.waitFor(1L);
				//PaymentContinue
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(8L);
			}
			
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnViewCart)){
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewCart));
			}

			int prodcutIcon=UIFoundation.listSize(FinalReviewPage.imgProdItemgiftIcon);
	    	String giftBagInCart[]={"firstProdItemgiftIcon","secondProdItemgiftIcon","thirdProdItemgiftIcon","fourthProdItemgiftIcon","fifthProdItemgiftIcon"};
		
			for(int i=0;i<prodcutIcon;i++){
			
				if(UIFoundation.isDisplayed(driver, giftBagInCart[i])){
		//		if(UIFoundation.isDisplayed(giftBagInCart[i])){
					
					  objStatus+=true;
				      String objDetail="Gift Bag is displayed for "+giftBagInCart[i];
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				}else{
					objStatus+=false;
					String objDetail="Gift Bag is not displayed "+giftBagInCart[i];
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
			}
			UIFoundation.waitFor(2L);
		
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnFnlRvwContinueShopping));
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnCartCount));
			UIFoundation.waitFor(3L);
			for (int i = 0; i < prodcutIcon; i++) {

				if (UIFoundation.isDisplayed(FinalReviewPage. giftBagInCart[i])) {

					objStatus += true;
					String objDetail = "Gift Bag is displayed for " + giftBagInCart[i];
					ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				} else {
					objStatus += false;
					String objDetail = "Gift Bag is not displayed " + giftBagInCart[i];					
					 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
				}
			}
			log.info("The execution of the method addGiftBag ended here ...");
		  String cartCount = UIFoundation.getText(CartPage.txtCartBtn);
			int count =Integer.parseInt(cartCount);	
			if(count>=9) {
			for(int i=0;i<=count;i++) {
			UIFoundation.clckObject(CartPage.spnRemoveFirstProduct);
			UIFoundation.waitFor(1L);
			UIFoundation.clckObject(CartPage.spnObjRemove);
			UIFoundation.waitFor(1L);
			}
			}	
			
			if (objStatus.contains("false")) {
				System.out.println("Verify the gift bag is displayed as line item in shopping  cart page test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the gift bag is displayed as line item in shopping  cart page test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method addGiftBag "
					+ e);
			return "Fail";
		}
	}*/
}
