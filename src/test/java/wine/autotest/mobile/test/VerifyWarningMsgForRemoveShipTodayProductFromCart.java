package wine.autotest.mobile.test;



import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class VerifyWarningMsgForRemoveShipTodayProductFromCart extends Mobile {

	/***************************************************************************
	 * Method Name : addShipTodayProdTocrt() Created By : Vishwanath Chavan
	 * Reviewed By : Ramesh,KB Purpose :
	 * 
	 * @throws IOException
	 ****************************************************************************
	 */
	public static String addShipTodayProdTocrt() {

		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String expectedWarningMsg = null;
		String actualWarningMsg = null;

		String screenshotName = "Scenarios_WarningMessage_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try {
			UIFoundation.waitFor(3L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.txtPinotNoir));
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnShipToday);
			UIFoundation.waitFor(2L);
			/*addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFirstProductToCart));

			}*/
			UIFoundation.waitFor(1L);
			addToCart2 = UIFoundation.getText(ListPage.btnFifthProductToCart);
			if (addToCart2.contains("Add to Cart")) {
				UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnFifthProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnFifthProductToCart));
			}

			UIFoundation.waitFor(1L);
			addToCart3 = UIFoundation.getText(ListPage.btnSixthProductToCart);
			if (addToCart3.contains("Add to Cart")) {
			//	UIFoundation.scrollDownOrUpToParticularElement(ListPage.btnSixthProductToCart);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnSixthProductToCart));
			}
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.lnkSrt);
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.waitFor(3L);
			expectedWarningMsg = verifyexpectedresult.onlyShipTodayWarningMsg;
			actualWarningMsg = UIFoundation.getText(ListPage.txtShipTodayWarningMsg);
			if (actualWarningMsg.contains(expectedWarningMsg)) {
				System.out.println(actualWarningMsg);
				System.out.println(
						"Verify warning message for only shiptoday products  test case is executed successfully ");
				objStatus+=true;
				String objDetail="Actual and expected Warning message are same";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");

			} else {
				System.out.println(actualWarningMsg);
				System.err.println("Verify warning message for only shiptoday products  test case is failed ");
				objStatus+=false;
				String objDetail="Actual and expected Warning message are not same";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			return "Fail";
		}
	}

}