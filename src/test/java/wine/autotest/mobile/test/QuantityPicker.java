
package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class QuantityPicker extends Mobile {




	/***************************************************************************
	 * Method Name			: quantityPick()
	 * Created By			: Chandrashekar 
	 * Reviewed By			: Ramesh
	 * Purpose				: 
	 ****************************************************************************
	 */


	public static String quantityPick()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		WebElement oEle=null;
		String productPriceBefore=null;
		double prodPriceBefore=0.0;
		boolean isdisplayed=false;
		String screenshotName = "Scenarios_quantityPick_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try
		{
			log.info("The execution of the method quantityPick started here ...");
			System.out.println("============Order summary before increasing quantity===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(4L);


			if(UIFoundation.isDisplayed(CartPage.txtProductPriceHasStrike)){
				productPriceBefore=UIFoundation.getText(CartPage.txtProductPriceHasSale);
				productPriceBefore=productPriceBefore.replaceAll("[ ]","");
				prodPriceBefore=Double.parseDouble(productPriceBefore);
		//		prodPriceBefore=ApplicationDependent.productPrice(driver, prodPriceBefore);
				UIFoundation.waitFor(2L);
			}else{
				productPriceBefore=UIFoundation.getText(CartPage.txtProductPrice);
				productPriceBefore=productPriceBefore.replaceAll("[ ]","");
				prodPriceBefore=Double.parseDouble(productPriceBefore);
			//	prodPriceBefore=ApplicationDependent.productPrice(driver, prodPriceBefore);
				UIFoundation.waitFor(2L);
			}
			objStatus+=String.valueOf(UIFoundation.SelectObject(CartPage.txtProductQuantity, "quantity"));
			UIFoundation.waitFor(2L);
			System.out.println("============Order summary after increasing quantity===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			String productPriceAfter=UIFoundation.getText(CartPage.txtProductPriceTotal);
			productPriceAfter=productPriceAfter.replaceAll("[ ]","");
			double prodPriceAfter=Double.parseDouble(productPriceAfter);
			//prodPriceAfter=ApplicationDependent.productPrice(driver, prodPriceAfter);
			int Quantity = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName,"quantity", 1));
			
			if(prodPriceBefore*Quantity==prodPriceAfter)
				//     	  if(prodPriceBefore==prodPriceAfter)

			{
				System.out.println("Product price is verified after increasing the quantity ");
				objStatus+=true;
				String objDetail="Product price is verified after increasing the quantity";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				System.out.println("Product price is not matching after increasing the quantity ");
				objStatus+=false;
				String objDetail="Product price is not matching after increasing the quantity ";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				System.out.println("The Applied promo code amount is not removed from total amount");
			}
			log.info("The execution of the method quantityPick ended here ...");
			if (objStatus.contains("false"))
			{

				return "Fail";
			}
			else
			{

				return "Pass";
			}

		}catch(Exception e)
		{

			log.error("there is an exception arised during the execution of the method quantityPick "+ e);
			return "Fail";

		}
	}

}
