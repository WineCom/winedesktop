package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyUserAbleToAddGifrWrappingMessageAndEmailAddress extends Mobile {
	


	/***************************************************************************
	 * Method Name : verifyTheUserAbleToAddGiftAddress() 
	 * Created By  : Chandrashekhar
	 * Reviewed By : Ramesh, 
	 * Purpose     : 
	 * Jira id     : TM-4187
	 ****************************************************************************
	 */

	public static String verifyTheUserAbleToAddGiftAddress() {
		String objStatus = null;
		String screenshotName = "Scenarios_verifyTheUserAbleToAddGiftAddress_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method verifyTheUserAbleToAddGiftAddress started here ...");
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
                          UIFoundation.waitFor(2L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.radShippingTyp));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtFirstName, "firstName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtLastName, "lastName"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtStreetAddress, "Address1"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCity, "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(FinalReviewPage.dwnState, "State"));
			UIFoundation.clickObject(FinalReviewPage.dwnState);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtobj_Zip, "ZipCode"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtPhoneNum, "PhoneNumber"));
			UIFoundation.waitFor(1L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnshippingSaving, "Invisible", "", 50);
			/*objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage. "SuggestedAddress"));
			UIFoundation.waitFor(3L);*/
			UIFoundation.waitFor(3L);
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnVerifyContinueShipRecpt);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnVerifyContinueShipRecpt)) {
			UIFoundation.waitFor(3L);
		    objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnVerifyContinueShipRecpt));
		    UIFoundation.webDriverWaitForElement(FinalReviewPage.btnVerifyContinueShipRecpt, "Invisible", "", 50);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEid));
			WebElement gift = driver.findElement(By.xpath("//main/section[@class='checkoutMainContent']//fieldset[@class='formWrap_group checkoutForm_checkboxGroup giftOptions']/label/input[@class='formWrap_checkbox giftOptions_checkbox']"));
			if(!gift.isSelected()){
				UIFoundation.clickObject(FinalReviewPage.radRecipientGiftCheckbox);
			}
		
			if(UIFoundation.isDisplayed(FinalReviewPage.txtGiftMessageTextArea) && UIFoundation.isDisplayed(FinalReviewPage.txtObj_RecipientEmail))
			{				
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.txtObj_RecipientEmail);
				UIFoundation.waitFor(2L); 
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtObj_RecipientEmail, "giftBagUserName"));
				objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtGiftMessageTextArea, "giftMessage"));
				UIFoundation.waitFor(1L);
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.cboGiftWrapOption);
				UIFoundation.waitFor(1L);
				objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.cboGiftWrapOption));
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnShipContinue);
				objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnShipContinue));
				
				objStatus+=true;
				String objDetail="User is able to add the gift email,Gift message and select Gift wrap";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("User is able to add the gift email,Gift message and select Gift wrap");
			}
			else
			{
				objStatus+=false;
				String objDetail="User is not able to add the gift email,Gift message and select Gift wrap is failed";				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
					
			log.info("The execution of the method verifyTheUserAbleToAddGiftAddress ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else { 

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method verifyTheUserAbleToAddGiftAddress "
					+ e);
			return "Fail";
		}
	}
}
