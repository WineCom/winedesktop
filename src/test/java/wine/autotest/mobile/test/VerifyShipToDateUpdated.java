package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyShipToDateUpdated extends Mobile {
	


	/***************************************************************************
	 * Method Name : VerifyShipToDateUpdated()
	 * Created By  : Chandrashekhara
	 * Reviewed By : Ramesh.
	 * Purpose :
	 ****************************************************************************
	 */

	public static String verifyShipToDateUpdated() {
		String objStatus = null;
		String screenshotName = "Scenarios__shipToDate.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ screenshotName;

		try {

			log.info("The execution of the method verifyShipToDateUpdated started here ...");
			driver.get("https://qwww.wine.com/product/pahlmeyer-jayson-cabernet-sauvignon-2016/500713");	
		
			UIFoundation.waitFor(3L);
			String shipDateBeforIncQuantity = UIFoundation.getText(CartPage.txtProdItemShippingDate);
			objStatus += String.valueOf(UIFoundation.selectLastValueFromDropdown(CartPage.cboPIPAddCartQuantity));
			UIFoundation.waitFor(2L);
			String shipDateAfterIncQuantity = UIFoundation.getText(CartPage.txtProdItemShippingDate);
			UIFoundation.waitFor(2L);
			if (!shipDateBeforIncQuantity.equalsIgnoreCase(shipDateAfterIncQuantity)) {
				objStatus+=true;
				String objDetail="Ship to date is updated when user modifies quantity to add to cart in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");	
				System.out.println("Ship to date is updated when user modifies quantity to add to cart in PIP");
			} else {
				objStatus+=false;
				String objDetail="Ship to date is not updated when user modifies quantity to add to cart in PIP";					
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"PIPDate", objDetail);
				System.out.println("Ship to date is not updated when user modifies quantity to add to cart in PIP");
			}
			log.info("The execution of the method verifyShipToDateUpdated ended here ...");
			if (objStatus.contains("false")) {
				
				System.out.println(
						"Verify ship to date is updated when user modifies quantity to add to cart in PIP and List page test case is failed");
				return "Fail";
			

			} else {
				System.out.println(
						"Verify ship to date is updated when user modifies quantity to add to cart in PIP and List page test case is executed successfully");
				return "Pass";
			}

		} catch (Exception e) {

			log.error(
					"there is an exception arised during the execution of the method ratingStarsDisplayedProperlyInMyWine "
							+ e);
			return "Fail";

		}
	}
}
