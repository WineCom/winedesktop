package wine.autotest.mobile.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySalesTaxInCartAndFinalReviewPage extends Mobile {
	

	

	/***************************************************************************
	 * Method Name			: verifySalesTax()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String verifySalesTaxInCartPage()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String status="";
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_SalesTaxInCart_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		
		try
		{
			log.info("The execution of the method verifySalesTax started here ...");
			System.out.println("============Cart Page Order summary ===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(2L);
			status=String.valueOf(UIFoundation.isDisplayed(CartPage.txtOrderSummarySalesTax));
			if(status.contains("false"))
			{
				 objStatus+=true;
			     String objDetail="Sales Tax is not dispalyed in Cart Page";
			     ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				 System.out.println("Sales Tax is not dispalyed in Cart Page");
			}
			else
			{
				  System.err.println("Sales Tax is  dispalyed in Cart Page");
				  objStatus+=false;
			      String objDetail="Sales Tax is  dispalyed in Cart Page";
			      UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(2L);
			
			
			if(UIFoundation.isDisplayed(CartPage.btnObjCheckout))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}
                          UIFoundation.waitFor(2L);
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "selectRecipientAdr"));
			UIFoundation.waitFor(4L);*/
/*			if(UIFoundation.isDisplayed(driver, "shippingAddressEdit")){
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "shippingAddressEdit"));
			UIFoundation.waitFor(4L);
			UIFoundation.clearField(driver, "StreetAddress");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "StreetAddress", "Address1"));
			UIFoundation.clearField(driver, "City");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "City", "City"));
			objStatus += String.valueOf(UIFoundation.SelectObject(driver, "shipState", "State"));
			UIFoundation.javaScriptClick(driver, "shipState");
			UIFoundation.clearField(driver, "ZipCode");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "ZipCode", "ZipCode"));
			UIFoundation.clearField(driver, "PhoneNum");
			objStatus += String.valueOf(UIFoundation.setObject(driver, "PhoneNum", "PhoneNumber"));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "shippingAddressSave"));
			UIFoundation.waitFor(5L);
			objStatus += String.valueOf(UIFoundation.clickObject(driver, "VerifyContinueButton"));
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(driver, "RecipientContinue"));
			UIFoundation.waitFor(5L);
			}*/
			
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());
			UIFoundation.waitFor(1L); 
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue)){
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}
			
			log.info("The execution of the method verifySalesTax ended here ...");
			
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method verifySalesTax "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: verifySalesTaxInFinalReviewPage()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */
	
	public static String verifySalesTaxInFinalReviewPage() {
	
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String status="";
		
		String screenshotName = "Scenarios_SalesTax_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method verifySalesTaxInFinalReviewPage started here ...");
			UIFoundation.waitFor(3L);
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth, "Month"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear, "Year"));
			objStatus += String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
			WebElement ele=driver.findElement(By.xpath("//input[@name='billingAddrSameAsShip']"));
			if(!ele.isSelected())
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
				UIFoundation.waitFor(3L);
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
			objStatus += String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
			UIFoundation.waitFor(3L);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			
			status=String.valueOf(UIFoundation.isDisplayed(FinalReviewPage.spnOrderSummarySalesTax));
			System.out.println("============Final Review Page Order summary ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			if(status.contains("true"))
			{
				System.out.println("Sales Tax is  dispalyed in Final Review Page");
			    objStatus+=true;
			    String objDetail="Sales Tax is  dispalyed in Final Review Page";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}
			else
			{
				System.err.println("Sales Tax is  not dispalyed in Final Review Page");
				objStatus+=false;
			    String objDetail="Sales Tax is  not dispalyed in Final Review Page";
			    UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
				
			}
			log.info("The execution of the method verifySalesTaxInFinalReviewPage ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method verifySalesTaxInFinalReviewPage "
					+ e);
			return "Fail";
		}
	}
	
}
