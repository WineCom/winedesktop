package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class PromoCodesThatDntMeetMinimumAmount extends Mobile {
	
	
	/***************************************************************************
	 * Method Name			: addprodTocrt()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addprodTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {

			UIFoundation.waitFor(5L);
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtSangviovese));
			UIFoundation.waitFor(2L);

			addToCart1 = UIFoundation.getText(ListPage.btnFirstProductToCart);
	//		addToCart2 = UIFoundation.getText(ListPage.btnFifthProductToCart);
	//		addToCart3 = UIFoundation.getText(ListPage.btnSixthProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFirstProductToCart));
				
			}else if(addToCart2.contains("Add to Cart")){
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnFifthProductToCart));
			}

			else if (addToCart3.contains("Add to Cart")){
				objStatus += String.valueOf(UIFoundation.javaSriptClick(ListPage.btnSixthProductToCart));
			}
				
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtCaberNet);
			 UIFoundation.scrollUp(driver);
			//UIFoundation.waitFor(3L);			 
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		
		} catch (Exception e) {
			return "Fail";
		}
	}
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String enterPromoCode()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		   String screenshotName = "Scenarios_PromoCodeMinimum_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			
							
			if(UIFoundation.isDisplayed(ListPage.txtPromoCode))
			{	
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.webDriverWaitForElement(CartPage.spnRemovePromoCode, "Clickable", "", 50);
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtObj_PromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.webDriverWaitForElement(CartPage.spnRemovePromoCode, "Clickable", "", 50);
			}

			if(UIFoundation.isDisplayed(ListPage.txtMinimumOfferAmount)){
				objStatus+=true;
			      String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed.";
			      System.out.println(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectLastValueFromDropdown(CartPage.cboIncreaseQuantity);
			UIFoundation.waitFor(9L);
			if(UIFoundation.isDisplayed(CartPage.spnPromeCodePrice)){
				objStatus+=true;
			      String objDetail="Promo code is immediately applied and displayed in the Order Summary";
			      System.out.println(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="promo code is not applied ";
			       System.out.println(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectValueByValue(CartPage.cboIncreaseQuantity, "1");
			UIFoundation.waitFor(9L);
			if(UIFoundation.isDisplayed(ListPage.txtMinimumOfferAmount) && (!UIFoundation.isDisplayed(CartPage.spnPromeCodePrice))){
				objStatus+=true;
			      String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed and promo credit isn't applied.";
			      System.out.println(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
	/***************************************************************************
	 * Method Name			: validatePromoCodeMessageDisplayed()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh.
	 * Jira Id              : TM - 3921
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	
	public static String validatePromoCodeMessageDisplayed()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		   String screenshotName = "Scenarios_PromoCodeMinimum_Screenshot.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
					+ screenshotName;
		
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			if(UIFoundation.isDisplayed(ListPage.txtPromoCode))
			{	
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.webDriverWaitForElement(CartPage.spnRemovePromoCode, "Clickable", "", 50);
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtObj_PromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(ListPage.txtPromoCode, "PromecodeMinimumAmount"));
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkPromoCodeApply));
				UIFoundation.webDriverWaitForElement(CartPage.spnRemovePromoCode, "Clickable", "", 50);
			}

			if(UIFoundation.isDisplayed(ListPage.txtMinimumOfferAmount)){
				objStatus+=true;
			      String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed.";
			      System.out.println(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectLastValueFromDropdown(CartPage.cboIncreaseQuantity);
			UIFoundation.waitFor(9L);
			if(UIFoundation.isDisplayed(CartPage.spnPromeCodePrice)){
				  objStatus+=true;
			      String objDetail="Promo code is immediately applied and displayed in the Order Summary";
			      System.out.println(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="promo code is not applied ";
			       System.out.println(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(1L);
			UIFoundation.selectValueByValue(CartPage.cboIncreaseQuantity, "1");
			UIFoundation.waitFor(9L);
			if(UIFoundation.isDisplayed(ListPage.txtMinimumOfferAmount) && (!UIFoundation.isDisplayed(CartPage.spnPromeCodePrice))){
				objStatus+=true;
			      String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is displayed and promo credit isn't applied.";
			      System.out.println(objDetail);
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
			       String objDetail="'Your cart does not currently meet the requirements for this code. Please review the promo details.' is not displayed.";
			       System.out.println(objDetail);
			       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
}
