package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import wine.autotest.desktop.library.UIBusinessFlows;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyDiscountPriceInRedColorAndGrandTotalInBoldColor extends Mobile {
	
	
	
	/***************************************************************************
	 * Method Name			: captureOrdersummary()
	 * Created By			: Chandrashekhar.
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
		
	public static String captureOrdersummary()
	{
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String totalBeforeTax=null;
		String promeCode=null;
		String actualColor=null;
		String screenshotName = "Scenarios_NotInRedColour_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		
		try
		{
			log.info("The execution of the method captureOrdersummary started here ...");
			System.out.println("============Order summary before applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			if(UIFoundation.isDisplayed(CartPage.txtPromoCodeText))
			{
				
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnPromoCodeApplyLink));
				UIFoundation.waitFor(5L);
			}else
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(CartPage.txtObjPromoCode));
				objStatus+=String.valueOf(UIFoundation.setObject(CartPage.txtPromoCodeText, "Promecode"));
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnPromoCodeApplyLink));
				UIFoundation.waitFor(5L);
			}
			System.out.println("============Order summary after applying promo code===============");
			subTotal=UIFoundation.getText(CartPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(CartPage.spnShippingHnadling);
			totalBeforeTax=UIFoundation.getText(CartPage.spnTotalBeforeTax);
			promeCode=UIFoundation.getText(CartPage.spnPromeCodePrice);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Prome Code:            "+promeCode);
			System.out.println("Total Before Tax:      "+totalBeforeTax);
			UIFoundation.waitFor(3L);
			String fontWeight = driver.findElement(By.xpath("//td[@class='orderSummary_price orderSummary_totalPrice']")).getCssValue("font-family");
			if(fontWeight.contains("BentonSansMed"))
			{
				System.out.println("Grand Total is displayed in the Bold in order summary page");
			}
			UIBusinessFlows.discountCalculator(subTotal, promeCode);
			actualColor=UIFoundation.getColor(CartPage.spnPromeCodePrice);
			if(actualColor.equalsIgnoreCase("#d61d2c"))
			{
				System.out.println("Discount price is in Red color");
				objStatus+=true;
			    String objDetail="Discount price is in Red color";
			    ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				
			}
			else
			{
				System.out.println("Discount price is not in Red color");
				objStatus+=false;
			    String objDetail="Discount price is not in Red color";
			    UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}	
			log.info("The execution of the method captureOrdersummary ended here ...");
			if (objStatus.contains("false"))
			{
				
				return "Fail";
			}
			else
			{
				
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method captureOrdersummary "+ e);
			return "Fail";
			
		}
	}
}
