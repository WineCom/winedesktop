package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.fw.utilities.XMLData;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifyTheFourAddressAndCreditcards extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: verifyTheFourAddressAndCreditcards()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: 
	 ****************************************************************************
	 */
	
	public static String verifyTheFourAddressAndCreditcards() {
	

	String objStatus=null;

	   String screenshotName = "Scenarios_availableAddressCreditCard_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"  
				+ screenshotName;
		try {
			log.info("The execution of the method checkoutProcess started here ...");
			UIFoundation.waitFor(4L);
			
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else{
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
			UIFoundation.waitFor(8L);
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkRecipientEid))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEid));
				UIFoundation.waitFor(3L);	
			}
			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnRecipientShipToaddress);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.btnRecipientShipToaddress));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnReciptContinue));
			UIFoundation.waitFor(8L);
			int AddressCount = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName, "AddressCreditCardCount", 1));
			int getAddressCount=UIFoundation.listSize(FinalReviewPage.lnkAddressCountInRecipient);
			//int getAddressCount=Integer.parseInt(UIFoundation.getText(FinalReviewPage. "addressCountInRecipient"));
			if(AddressCount==getAddressCount){
				  objStatus+=true;
			      String objDetail="4 address cards are displayed by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="4 address cards are not displayed by default";
				//ReportUtil.addTestStepsDetails("Order number is null.Order not placed successfully", "", "");				
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"addressCard", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkRecipientEidt));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkViewAllAllAddress));
			if(UIFoundation.isDisplayed(FinalReviewPage.btnModalWindowIcon)){
				  objStatus+=true;
			      String objDetail="All the available address cards  are displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="All the available address cards  are not displayed";						
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"allCreditCard", objDetail);
			}
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnModalWindowIcon));
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnRecipientContinue));
			UIFoundation.waitFor(10L);
			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.waitFor(1L);
			}
			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
			}
			int creditCardCount = Integer.parseInt(XMLData.getTestData(testScriptXMLTestDataFileName, "AddressCreditCardCount", 1));
			int getcreditCard=UIFoundation.listSize(FinalReviewPage.txtCreditCardCountInRecipient);
			//int getcreditCard=Integer.parseInt(UIFoundation.getText(FinalReviewPage. "creditCardCountInRecipient"));
			if(creditCardCount==getcreditCard){
				  objStatus+=true;
			      String objDetail="4 credit cards are displayed by default";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="4 credit cards are not displayed by default";					
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName+"creditCard", objDetail);				
			}
			UIFoundation.waitFor(5L);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnViewAllPaymentMethods));
			
			if(UIFoundation.isDisplayed(FinalReviewPage.btnModalWindowIcon)){
				  objStatus+=true;
			      String objDetail="All the available credit cards are displayed";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			}else{
				objStatus+=false;
				String objDetail="All the available credit cards are not displayed";
				UIFoundation.captureScreenShot(screenshotpath+screenshotName+"allCreditCard", objDetail);	
				
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if ( objStatus.contains("false")) {
				System.out.println("Verify the 4 Address/Credit cards is displayed by default in Recipient/Payment methods page during checkout test case is failed");
				return "Fail";
			} else {
				System.out.println("Verify the 4 Address/Credit cards is displayed by default in Recipient/Payment methods page during checkout test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {
			
			log.error("there is an exception arised during the execution of the method getOrderDetails "
					+ e);
			return "Fail";
		}
	}	
}
