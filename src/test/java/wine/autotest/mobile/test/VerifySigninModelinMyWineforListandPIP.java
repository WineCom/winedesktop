package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class VerifySigninModelinMyWineforListandPIP extends Mobile {
	


	/***************************************************************************
	 * Method Name : VerifyMyWineSignInNEditLinkForGiftMessage 
	 * Created By  : Ramesh S 
	 * Reviewed By : Chandrashekhar
	 * Purpose     : TM-4132
	 ****************************************************************************
	 */

	public static String signInModalForMyWineforListandPIP() {

		String objStatus = null;
		String ScreenshotName = "signInModalForMyWinePage.jpeg";
		String screenshotName = "Scenarios_ForgotPassword_Screenshot.jpeg";
		String Screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\DesktopResults\\Screenshots\\"
				+ ScreenshotName;

		try {
			log.info("sign In Modal For My Wine Page method started here........");
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			if (UIFoundation.isElementDisplayed(CartPage.txtSignInTextR)
					&& UIFoundation.isElementDisplayed(CartPage.txtSignInBtnR)) {
				objStatus += true;
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnAllLinkCloseIcon));
				String objDetail = "Sign modal is displayed when clicks on Mywine ";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywine";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.imgObj_MyWine));
			if (UIFoundation.isElementDisplayed(CartPage.txtSignInTextR)
					&& UIFoundation.isElementDisplayed(CartPage.txtSignInBtnR)) {
				objStatus += true;
				objStatus += String.valueOf(UIFoundation.clickObject(CartPage.btnAllLinkCloseIcon));
				String objDetail = "Sign modal is displayed when clicks on Mywine badge in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			} else {
				objStatus += false;
				String objDetail = "Sign modal is not displayed when clicks on Mywine badge in PIP";
				ReportUtil.addTestStepsDetails(objDetail, "Fail", "");
				 UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
			}
			log.info("sign In Modal For My Wine Page method ended here........");

			if (objStatus.contains("false")) {

				System.out.println("signInModalForMyWineforListandPIP test case is failed");

				return "Fail";
			} else {
				System.out.println("signInModalForMyWineforListandPIP test case executed succesfully");

				return "Pass";
			}
		} catch (Exception e) {
			return "Fail";

		}
	}

}
