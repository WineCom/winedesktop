package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;

import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.FinalReviewPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;

import wine.autotest.mobile.test.Mobile;;

public class OrderCreationWithMyWineForexistingUSer extends Mobile {
	


	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */

	public static String login()
	{
		String objStatus=null;
		String screenshotName = "Scenarios_Login_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;
		try
		{

			log.info("The execution of the method login started here ...");
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(1L); 

			/*if(UIFoundation.isDisplayed(LoginPage. "JoinNowButton")){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage. "JoinNowButton"));
				UIFoundation.waitFor(2L);
			}*/

			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			//	UIFoundation.waitFor(4L);
			/*WebDriverWait wait = new WebDriverWait(LoginPage. 10);
			WebElement element = wait.until(
			        ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='email']")));*/

			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "username"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.SignInButton));
			UIFoundation.webDriverWaitForElement(LoginPage.SignInButton, "Invisible", "", 50); 
			String actualtile=driver.getTitle();
			String expectedTile = verifyexpectedresult.homePageTitle;
			
			if(actualtile.equalsIgnoreCase(expectedTile))
			{
				objStatus+=true;
			}else
			{
				objStatus+=false;
				String objDetail="Failed to login";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				System.out.println("Login test case is executed successfully");
				return "Pass";
			}

		}catch(Exception e)
		{
			objStatus+=false;
			String objDetail="Failed to login";
			UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";

		}
	}

	/***************************************************************************
	 * Method Name			: addMyWineProdTocrt()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 * @throws IOException 
	 ****************************************************************************
	 */
	public static String addMyWineProdTocrt() {
		String objStatus = null;
		String addToCart1 = null;
		String addToCart2 = null;
		String addToCart3 = null;
		String addToCart4 = null;
		String addToCart5 = null;
		try {
			UIFoundation.waitFor(2L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.imgObj_MyWine));
			UIFoundation.webDriverWaitForElement(ListPage.lnkmyWneSortWd, "Clickable", "", 50);

			addToCart1 = UIFoundation.getText(ListPage.btnMyWineFirstProductToCart);
			if (addToCart1.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMyWineFirstProductToCart));
			}

			addToCart2 = UIFoundation.getText(ListPage.btnMyWineSecondProductToCart);
		//	UIFoundation.scrollDownOrUpToParticularElement(ListPage. "MyWineSecondProduct");
			UIFoundation.waitFor(1L);
			if (addToCart2.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMyWineSecondProductToCart));
			}

			/*addToCart3 = UIFoundation.getText(ListPage. "MyWineThirdProduct");
			if (addToCart3.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "MyWineThirdProduct"));
			}

			addToCart4 = UIFoundation.getText(ListPage. "MyWineFourthProduct");
			UIFoundation.scrollDownOrUpToParticularElement(ListPage. "MyWineFourthProduct");
			if (addToCart4.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "MyWineFourthProduct"));
			}

			addToCart5 = UIFoundation.getText(ListPage. "MyWineSixthProduct");
			if (addToCart5.contains("Add to Cart")) {
				objStatus += String.valueOf(UIFoundation.clickObject(ListPage. "MyWineSixthProduct"));
			}*/
			UIFoundation.waitFor(2L);
			UIFoundation.scrollDownOrUpToParticularElement(ListPage.txtMyWine);
			UIFoundation.waitFor(1L);
			objStatus += String.valueOf(UIFoundation.javaScriptClick(ListPage.btnCartCount));
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckout, "Clickable", "", 50);

			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {
			return "Fail";
		}
	}

	/***************************************************************************
	 * Method Name			: checkoutProcess()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh.
	 * Purpose				: The purpose of this method is to capture the order 
	 * 						  number and purchased id
	 ****************************************************************************
	 */

	public static String checkoutProcess() {

		String expected=null;
		String actual=null;
		String objStatus=null;
		String subTotal=null;
		String shippingAndHandling=null;
		String total=null;
		String salesTax=null;
		String screenshotName = "Scenarios_OrderNotPlaced_Screenshot.jpeg";
		String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
				+ screenshotName;

		try {
			log.info("The execution of the method checkoutProcess started here ...");
		//	expected=objExpectedRes.getProperty("placeOrderConfirmation");
			expected = verifyexpectedresult.placeOrderConfirmation;
			UIFoundation.waitFor(2L);			
						
			if(UIFoundation.isDisplayed(CartPage.btnCheckoutButton))
			{
			objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnCheckoutButton));
			}else {
				objStatus+=String.valueOf(UIFoundation.clickObject(CartPage.btnObjCheckout));
			}
	
			UIFoundation.webDriverWaitForElement(CartPage.btnCheckoutButton, "Invisible", "", 50);
			objStatus+=String.valueOf(UIBusinessFlow.recipientEdit());

			if(UIFoundation.isDisplayed(FinalReviewPage.btnDeliveryContinue))
			{
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnDeliveryContinue);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnDeliveryContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnDeliveryContinue, "Invisible", "", 50);
			}

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkchangePayment))
			{
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.lnkchangePayment));
				UIFoundation.waitFor(1L);
			}
			
		//	objStatus+=String.valueOf(ApplicationDependent.existingUserOrderCreation(driver));

			if(UIFoundation.isDisplayed(FinalReviewPage.lnkAddNewCard))
			{
				objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage.lnkAddNewCard));
				UIFoundation.waitFor(1L);
			}
			/*			objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "PaymentCheckoutEdit"));
			UIFoundation.waitFor(1L);*/
			/*objStatus+=String.valueOf(UIFoundation.javaScriptClick(FinalReviewPage. "AddPayment"));
			UIFoundation.waitFor(2L);*/
			if(UIFoundation.isDisplayed(FinalReviewPage.txtNameOnCard))
			{
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtNameOnCard, "NameOnCard"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCardNumber, "Cardnum"));
				UIFoundation.waitFor(2L);
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryMonth,"Month"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.dwnExpiryYear,"Year"));
				objStatus+=String.valueOf(UIFoundation.setObject(FinalReviewPage.txtCVV, "CardCvid"));
				UIFoundation.waitFor(3L);
				WebElement ele=driver.findElement(By.xpath("//form[@class='paymentForm_form']/fieldset[@class='paymentForm_billingFieldset formWrap_group checkoutForm_checkboxGroup paymentForm_sameAsShip']/label/input[@name='billingAddrSameAsShip']"));
				if(!ele.isSelected())
				{
					UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.javaScriptClick(FinalReviewPage.chkBillingAndShippingCheckbox);
					UIFoundation.waitFor(3L);
				}
				UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPaymentContinue);
				UIFoundation.waitFor(1L);
				objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPaymentContinue));
				UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPaymentContinue, "Invisible", "", 50);
			}

			System.out.println("============Order summary in the Final Review Page  ===============");
			subTotal=UIFoundation.getText(FinalReviewPage.spnSubtotal);
			shippingAndHandling=UIFoundation.getText(FinalReviewPage.spnShippingHnadling);
			total=UIFoundation.getText(FinalReviewPage.spnTotalBeforeTax);
			salesTax=UIFoundation.getText(FinalReviewPage.spnOrderSummaryTaxTotal);
			System.out.println("Subtotal:              "+subTotal);
			System.out.println("Shipping & Handling:   "+shippingAndHandling);
			System.out.println("Sales Tax:             "+salesTax);
			System.out.println("Total:                 "+total);
			UIFoundation.waitFor(2L);
			/*UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage. "PlaceOrderButton");
			UIFoundation.waitFor(1L);*/

			UIFoundation.scrollDownOrUpToParticularElement(FinalReviewPage.btnPlaceOrder);
			objStatus+=String.valueOf(UIFoundation.clickObject(FinalReviewPage.btnPlaceOrder));
			UIFoundation.waitFor(5L);
			UIFoundation.webDriverWaitForElement(FinalReviewPage.btnPlaceOrder, "Invisible", "", 50);
			String orderNum=UIFoundation.getText(FinalReviewPage.txtOrderNum);
			if(orderNum!="Fail")
			{
				objStatus+=true;
				String objDetail="Order number is placed successfully";
				ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				System.out.println("Order number is placed successfully: "+orderNum);
			}else
			{
				objStatus+=false;
				String objDetail="Order number is null and Order cannot placed";
				UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			log.info("The execution of the method checkoutProcess ended here ...");
			if (!expected.equalsIgnoreCase(actual) && objStatus.contains("false")) {
				System.out.println("Order creation with My Wine products test case is failed");
				return "Fail";
			} else {
				System.out.println("Order creation with My Wine products test case is executed successfully");
				return "Pass";
			}
		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method checkoutProcess "
					+ e);
			return "Fail";
		}
	}


	
}
