package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class NewArrivalAlertsInUserProfile extends Mobile {
	
	
	/***************************************************************************
	 * Method Name : newArrivalAlertsInUserProfile() 
	 * Created By : Chandrashekhar
	 * Chavan Reviewed By : Ramesh.
	 * Purpose : 
	 * TM-2224,2159
	 ****************************************************************************
	 */

	public static String newArrivalAlertsInUserProfile() {
		String objStatus = null;
		   String screenshotName = "Scenarios_OrderCreation_Screenshot.jpeg";
		   String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"
					+ screenshotName;

		try {
			UIFoundation.waitFor(8L);
			log.info("The execution of the method newArrivalAlertsInUserProfile started here ...");
			if(!UIFoundation.isDisplayed(ListPage.btnAddProduct)){
				driver.navigate().refresh();
				UIFoundation.waitFor(7L);
			}
			
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.btnMainNavButton));
			objStatus += String.valueOf(UIFoundation.clickObject(ListPage.txtCaberNet));
			UIFoundation.waitFor(4L); 
			objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.lnkproductInfo));
			UIFoundation.webDriverWaitForElement(ListPage.txtVintageAlert, "Clickable", "", 50);			
			/*objStatus+=String.valueOf(UIFoundation.clickObject(driver, "addProduct"));
			UIFoundation.waitFor(2L);*/
			if(UIFoundation.isDisplayed(ListPage.txtsetAlert)){
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsetAlert));
				UIFoundation.webDriverWaitForElement(ListPage.btnAlertSaving, "Invisible", "", 50);
				
			}else{
			
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtVintageAlert));
				UIFoundation.webDriverWaitForElement(ListPage.txtsetAlert, "Clickable", "", 50);
				objStatus+=String.valueOf(UIFoundation.clickObject(ListPage.txtsetAlert));
				UIFoundation.webDriverWaitForElement(ListPage.btnAlertSaving, "Invisible", "", 50);
			}

			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			//objStatus+=String.valueOf(UIFoundation.clickObject(driver, "accountBtn"));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.txtEmailPreferences));
			UIFoundation.webDriverWaitForElement(LoginPage.btnDeleteAlerts, "Clickable", "", 50);
			if(UIFoundation.isSelected(LoginPage.btnEmailPreferenceRadio))
			{
				  objStatus+=true;
			      String objDetail="Include me on all Wine.com emails Radio button is selected by deafault";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Include me on all Wine.com emails Radio button is selected by deafaul");
				
			}else{
				  objStatus+=false;
			       String objDetail="Include me on all Wine.com emails Radio button is not selected by deafaul";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName+"radioButton", objDetail);
			      
			}
			if(UIFoundation.isDisplayed(ListPage.txtNewArrivalAlertHeader) && UIFoundation.isDisplayed(ListPage.txtNewArrivalAlertListItem))
			{
				  objStatus+=true;
			      String objDetail="Verified the 'New arrival alerts' in User profile are updated on setting the alert for the product in PIP";
			      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			      System.out.println("Verified  the 'New arrival alerts' in User profile are updated on setting the alert for the product in PIP");
			}
			else
			{
				   objStatus+=false;
			       String objDetail="Verify the 'New arrival alerts' in User profile are updated on setting the alert for the product in PIP is failed";
			       UIFoundation.captureScreenShot( screenshotpath+screenshotName, objDetail);
			}
			UIFoundation.waitFor(3L);
			if(UIFoundation.isDisplayed(ListPage.txtDeleteAlerts))
			{
				String statusDeleteALert=String.valueOf(UIFoundation.clickObject(ListPage.txtDeleteAlerts));
				boolean status=Boolean.valueOf(statusDeleteALert);
				 objStatus+=status;
				 if(status)
				 {
					 objStatus+=true;
				      String objDetail="Alert is deleted";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
				      System.out.println("Alert is deleted successfully");
				 }else{
					 objStatus+=false;
				       String objDetail="Failed to delete alert";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName+"alert", objDetail);
				 }
			}
			
			log.info("The execution of the method newArrivalAlertsInUserProfile ended here ...");
			if (objStatus.contains("false")) {

				return "Fail";
			} else {

				return "Pass";
			}

		} catch (Exception e) {

			log.error("there is an exception arised during the execution of the method editRecipientPlaceORder "
					+ e);
			return "Fail";
		}
	}
	
}
