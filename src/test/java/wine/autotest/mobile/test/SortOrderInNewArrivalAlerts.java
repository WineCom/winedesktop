package wine.autotest.mobile.test;

import java.util.ArrayList;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wine.autotest.fw.utilities.ReportUtil;
import wine.autotest.fw.utilities.UIFoundation;
import wine.autotest.mobile.library.UIBusinessFlow;
import wine.autotest.mobile.library.verifyexpectedresult;
import wine.autotest.mobile.pages.CartPage;
import wine.autotest.mobile.pages.ListPage;
import wine.autotest.mobile.pages.LoginPage;
import wine.autotest.mobile.test.Mobile;;

public class SortOrderInNewArrivalAlerts extends Mobile {
	

	
	/***************************************************************************
	 * Method Name			: login()
	 * Created By			: Chandrashekhar 
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: The purpose of this method is Login into the Wine.com
	 * 						  Application
	 ****************************************************************************
	 */
	
	public static String sortedArrivalLogin()
	{
		String objStatus=null;
		
		try
		{
			log.info("The execution of the method login started here ...");
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccountTab));
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavSignIn));
			UIFoundation.waitFor(3L);
			

			if(UIFoundation.isDisplayed(LoginPage.JoinNowButton)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.JoinNowButton));
				UIFoundation.waitFor(2L);
			}
			
			if(UIFoundation.isDisplayed(LoginPage.signInLinkAccount)){
				objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.signInLinkAccount));
			}
			
			
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginEmail, "newArrivalAlertEmail"));
			objStatus+=String.valueOf(UIFoundation.setObject(LoginPage.LoginPassword, "password"));
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.SignInButton));
			UIFoundation.waitFor(6L);
			log.info("The execution of the method login ended here ...");
			if (objStatus.contains("false"))
			{
				//System.out.println("Login test case is failed");
				return "Fail";
			}
			else
			{
				//System.out.println("Login test case is executed successfully");
				return "Pass";
			}
			
		}catch(Exception e)
		{
			
			log.error("there is an exception arised during the execution of the method login "+ e);
			return "Fail";
			
		}
	}
	
	/***************************************************************************
	 * Method Name			: validationForSortZtoA()
	 * Created By			: Chandrashekhar
	 * Reviewed By			: Ramesh,KB
	 * Purpose				: 
	 ****************************************************************************
	 */
	public static boolean verifyTheSortingInNewArrival()
	{
		WebElement ele=null;
		String objStatus=null;
		ArrayList<String> arrayList=new ArrayList<String>();
		ArrayList<String> actualItemsName=new ArrayList<String>();
		   boolean isSorted = true;
			String screenshotName = "Scenarios_verifyTheSortingInNewArrival.jpeg";
			String screenshotpath = System.getProperty("user.dir") + "\\src\\test\\resources\\PlatformResults\\Screenshots\\"  
			     + screenshotName;
		try
		{
			
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavButton));
			UIFoundation.waitFor(3L);
			objStatus+=String.valueOf(UIFoundation.javaScriptClick(LoginPage.MainNavAccTabAfterSignIn));
			UIFoundation.waitFor(1L);			
			objStatus+=String.valueOf(UIFoundation.clickObject(LoginPage.EmailPreferences));
			UIFoundation.waitFor(5L);
			String products1=UIFoundation.getText(ListPage.btnNewArrivalAlertFirstProduct);
			String products2=UIFoundation.getText(ListPage.btnNewArrivalAlertSecondProduct);
			arrayList.add(products1.replaceAll("[0-9]", ""));
			arrayList.add(products2.replaceAll("[0-9]", ""));
			    for(int i = 0; i < arrayList.size()-1; i++) {
			       // current String is < than the next one (if there are equal list is still sorted)
			       if(arrayList.get(i).compareToIgnoreCase(arrayList.get(i + 1)) < 0) { 
			    	   isSorted=false;
			           break;
			       }
			      
			    }
			    if(isSorted)
			    {
			    	  objStatus+=true;
				      String objDetail="New arrival alerts are sorted in descending order by default.";
				      ReportUtil.addTestStepsDetails(objDetail, "Pass", "");
			    	
					return true;
			    }
			    else
			    {
			    		objStatus+=false;
				       String objDetail="New arrival alerts are not sorted in descending order by default.";
				       UIFoundation.captureScreenShot(screenshotpath+screenshotName, objDetail);
					return false;
			    }
		}catch(Exception e)
		{
			return false;
			
		}
	
	}

}
